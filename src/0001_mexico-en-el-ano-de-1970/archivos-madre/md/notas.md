Parece ridículo decir que a las doce y media de la noche la sobrina
saludase a su tío a cincuenta varas de distancia y que él la viese;
pero esto alude a un proyecto que tiene en París un francés, y es
producir una luz tal y colocada de modo que desempeñe perfectamente
en la noche las funciones de sol en cuanto a luz. Parece
descabellado a primera vista el proyecto; pero no lo es, pues
lo primero casi se ha logrado dirigiendo una corriente de hidrógeno
bicarbonado inflamable sobre cal viva; la luz que resulta
es tan intensa que a trescientas varas de distancia se puede leer
una carta. La dificultad, pues, de producir el rayo solar ---así llama
su autor a su feliz pensamiento--- consiste en colocar el aparato
que da la luz a una altura en que, sin dañar demasiado la vista
de los que están cerca de él, pueda alumbrar a grandes distancias.
Como antes dijimos, el proyecto no nos parece desatinado,
y creemos que si su autor imita a Daguerre en su asidua laboriosidad,
llegará a ver coronados sus esfuerzos.
