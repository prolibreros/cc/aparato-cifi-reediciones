<section epub:type="foreword" role="doc-foreword">

# Advertencia @ignore

_Este libro no pretende ser una anticipación; es solo un libro
optimista._ {.espacio-arriba3 .sangria}

_Como era menester el autor ha usado en esta novela nombres y
apellidos criollos de origen español; pero cualquier homonimia
con personas vivas o muertas es meramente casual._ {.espacio-arriba3}

_Lo mismo debe decirse de alguna otra identidad en caracteres
y sucesos, pues el presente relato es imaginario._

</section>
<section epub:type="epigraph">

# Epígrafes @ignore

_Porque el Señor es el Espíritu, y donde hay el Espíritu del
Señor, allí hay libertad._ \
++San Pablo++ {.epigrafe}

_For hatred does not cease by hatred at any time; hatred ceases
by love----this is an old rule._ \
++The Dhammapada++ \
(_traducción inglesa_). {.espacio-arriba3 .epigrafe}

</section>
<section epub:type="preface" role="doc-preface">

# Exordio

++Los++ meses de febrero, marzo y abril de 1938 los pasé en el
pueblo de San Miguel Allende en el Estado de Guanajuato. Había
ido en busca de descanso para recuperar mi equilibrio mental
después de un tremendo choque nervioso que puso en peligro mi
razón. Tuve síntomas alarmantes: ataques pasajeros de amnesia
y una parafasia que me trastornó durante varias semanas. Pero
gracias a Dios los médicos me declararon fuera de peligro, prescribiéndome
un reposo absoluto y el abandono temporal de mis ocupaciones
habituales para completar mi cura. Fue entonces cuando un sanmigueleño
amigo mío muy querido me ofreció su casa y su compañía para convalecer.

Las caminatas diarias por el campo, las subidas y bajadas a las
colinas sobre las que esa ciudad está construída, el sol, el
aire libre y el olvido absoluto del automóvil, obraron milagros
en mi naturaleza. Pero fueron también factores decisivos la comida
saludable, el ambiente familiar, las charlas larguísimas y el
bullir de los chiquillos.

Al cabo de seis semanas me sentía fuerte y dispuesto a reanudar
el trajín cotidiano de mi vida; pero la idea de regresar a mi
escritorio de burócrata, de preparar los acuerdos, de ver al
Oficial Mayor y al Secretario, me llenaba de espanto. Así fui
prolongando esas vacaciones usando y abusando de la licencia
que me habían concedido.

Las conversaciones con mi amigo llenaban varias horas del día,
pues las entablábamos en las mañanas, en las sobremesas y después
en la velada, hasta bien entrada la noche. Yo, con mi espíritu
metódico de cagatinta crónico, sabía que los tópicos podían
clasificarse siempre en tres categorías:

Primeramente hablábamos mal del gobierno, del fracaso revolucionario,
de la venalidad de los hombres públicos, de la miseria de nuestros
pobres indios que han servido solo de estandarte para encumbrar
a los políticos, pero que siguen sumidos en la desgracia.

Nuestro segundo tema era la situación del mundo, la probabilidad
de una guerra europea, las engañifas de Hitler, las mentiras
del comunismo, las rectificaciones de Gide y de Citrine, y por
supuesto comentábamos a fondo los sucesos de la revolución española,
que todos los mexicanos tomaron tan a pecho.

Y por fin hablábamos de San Miguel: de sus casas llenas de dignidad,
de sus patios labrados, de su sabor colonial ---(de la mejor
época de la Colonia)--- de sus gentes acogedoras y de su pueblo
industrioso. Conocí multitud de consejas y de pequeños secretos
que corren de boca en boca entre los vecinos iniciados. Uno de
ellos es el de los túneles que van de un lado a otro de la población.
Tomé en serio esta pesquisa hasta obtener un plano antiguo por
demás curioso; pero nunca tuve oportunidad de hacer las exploraciones
que habrían confirmado los datos que recogí.

Parece sin embargo que cuando los apaches se aventuraban hasta
el Bajío, los señores de San Miguel construyeron una red de comunicaciones
subterráneas como medio estratégico de ataque y defensa contra
esas correrías.

Esto nos llevó de la mano a hablar de los tesoros escondidos.
Mi amigo me paseó entonces por toda su casa y me contó historias
fidedignas que establecían presunciones acerca de cantidades
en oro que seguramente estaban ocultas en algún sitio. Su bisabuelo
falleció muy anciano; recibía frecuentemente la venta de sus
cosechas en buenas onzas amarillas que nadie sabía dónde iban
a parar. El mismo había escrito una carta a su notario hablándole
de sus ahorros y asegurándole que, si sus hijos eran prudentes,
la educación de todos los nietos estaba asegurada. Pero una buena
mañana el viejo se quedó muerto, con los ojos abiertos, sentado
en el salón de su casona, mientras hablaba de la defensa que
hizo de un cañón en la Angostura, cuando la invasión del 47.
Durante algún tiempo nadie había recordado la fábula aquella,
pero a la postre se hicieron sondeos en los lugares donde juzgaron
probable que estuviera el guardado: en el despacho, en la recámara,
en un desván, mas los resultados fueron nulos.

Mi buen amigo me propuso que nos ocupásemos en el asunto y yo
acepté resuelto a contribuir con mi escrupulosidad y mi espíritu
metódico. Comencé por hacer un levantamiento del edificio; establecí
gruesos de muros, encontré curiosas asimetrías y algunos inexplicables
dispositivos de construcción. Esto me tomó dos semanas de un
trabajo asiduo e investigador. Me decía a mí mismo que si los
empleados del Estado aplicasen esa asiduidad y esa inteligencia
a sus labores, México sería el espejo del mundo. Pero yo no podía
traicionar la gloriosa tradición del burócrata y me apegaba estrictamente
a las tres reglas de oro de mi oficio: la primera tratar mal
al público, la segunda despachar los asuntos con el máximo de
lentitud y la tercera dejarme cohechar cada vez que se me presentaba
una ocasión sin peligro.

Veo que estoy haciendo digresiones inútiles y por lo mismo vuelvo
a la médula de mi relato. Una vez en posesión de los planos discutimos
un programa resolviendo comenzar con las posibilidades más remotas.
En realidad, aunque no nos lo confesábamos, lo que pretendíamos
era prolongar nuestro pasatiempo. Llevábamos ya diez días de
hurgar aquí y allá; el uso del pico y la pala me tenía en magnífico
estado de salud y de humor excelente.

El 13 de marzo era un domingo. Lo recuerdo porque nos habíamos
levantado más tarde que de costumbre y porque los mocosos, que
no fueron a la escuela, se pusieron a jugar frente a mí, perturbando
la lectura de mi periódico fechado el sábado, que había llegado
de México. Lo recuerdo también porque esa tarde se instaló una
mesa de póquer barato, juego que me encanta porque un tío mío,
cuando yo era pequeño, me enseñó unas cuantas trampas bastante
ingenuas, que no podrían pasar entre tahures de fuste, pero que
son infalibles en las partidas familiares, sobre todo con las
señoras. A las siete me levanté con cincuenta pesos.

Después de la cena mi amigo y yo conectamos el radio y nos pusimos
a oír música mala hasta la diez. A esa hora buscamos los noticiarios.
Entonces supimos que el Primer Ministro austriaco había proclamado
la unión de Alemania y Austria. Comentamos el asunto sin gran
conocimiento de causa. Solo recuerdo que mi amigo me dijo:

---Yo creo que el apetito de Hitler será insaciable. Esto es
solo el principio.

Como a las once mi amigo me dijo que tenía sueño y que nuestras
investigaciones sobre el tesoro las reanudaríamos el siguiente
día, lunes, que era de trabajo.

Le contesté que me quedaba todavía un rato allí; que me había
asaltado una idea y que si él no tenía inconveniente iba a ensayar
su validez.

---Por supuesto, Diego ---me contestó---. Ya me contará mañana
si resulta algo interesante.

Efectivamente yo estaba obsesionado con ciertas anormalidades
de la chimenea que no podía explicarme; las observé al levantar
los planos de la casa. En el angostamiento del tiro, al salir
del hogar, existían dos recesos simétricos cuya presencia me
intrigaba y habiéndome quedado solo me resolví a aclarar el
enigma.

La chimenea había estado encendida, pero la leña se había agotado
y solo quedaban las cenizas grises salpicadas de pequeños puntos
que brillaban como corindones color de rosa. Las hice a un lado
con el pie y me introduje para ver si la temperatura no era excesiva
y si se podría permanecer allí un buen cuarto de hora, sin gran
sacrificio. En seguida saqué mi lámpara eléctrica de bolsillo
y me puse a revisar todas las junturas. Allí, en uno de los desvíos
a que me he referido, el del lado derecho, comencé a rascar las
uniones con un cincel hasta que noté que una de las piedras cedía.
Pude levantarla sin mucha dificultad y me encontré un lugar hueco,
bien enjarrado con esa mezcla teñida de rojo y bruñida, que era
tan común en las fábricas de antaño. Evidentemente se había construido
allí una caja con objeto de depositar algo en ella, pero estaba
vacía. Pensé que después valdría la pena continuar mis investigaciones,
pero que de momento lo lógico era ver si del lado izquierdo encontraba
una oquedad semejante. En efecto, a la luz de mi antorcha eléctrica,
pude ver una piedra que hacía oficio de tapa; destruí la argamasa
de las juntas y palanqueando con el cincel logré desprenderla.
El enlucido interior era semejante al que ya había encontrado;
al mismo tiempo enfoqué el haz luminoso hacia el fondo.

Entonces, con enorme sorpresa, vi un paquete envuelto en papel
de periódico y atado con un cordón; lo saqué y ya a la luz de
los focos de la estancia donde me encontraba, vi que era un envoltorio
plano; estaba ligeramente húmedo y el papel del forro, trigueño,
quizás por la vejez. Al tacto se adivinaba que el contenido consistía
también en papeles.

Con el corazón sobresaltado por la emoción y sin ponerme a pensar
que lo indicado era despertar a mi amigo para darle cuenta del
descubrimiento, comencé a desatar el cordel. Me sorprendió el
hecho de que este era de un rojo vivo y lustroso y de una contextura
como celuloide. En mi ansia por satisfacer mi curiosidad no reparaba
en nada y lo tiré a la chimenea. A los pocos segundos escuché
el crepitar de algo que arde y vi cómo se consumía rápidamente
con una llama amarilla.

Despojé el paquete de su cubierta; esta consistía en un ejemplar
del «Excelsior». Leí maquinalmente el encabezado y abajo: «El
periódico de la vida nacional». Pero todo esto era fútil; lo
había leído cotidianamente millares de veces. Arrojé entonces
ese papel inservible sobre el montón de cenizas que, en un rincón
del hogar, despedían un vaho caliente y acariciador. Anoto estas
circunstancias, aparentemente banales, porque después iba a repasarlas
en mi mente una y otra y otra vez con el propósito de desentrañar
el misterio que estaba a punto de envolverme.

Encontréme entonces con un manuscrito. Encendí una lámpara de
pie junto a un sillón y me senté en este para enterarme de la
importancia de mi hallazgo. En la primera página había dos renglones
escritos con letra menuda; como tengo la vista cansada puse las
cuartillas sobre mis rodillas, saqué mis anteojos, me los calé
con parsimonia y me dispuse a leer. Esas dos líneas decían así:

«Terminé de escribir esta historia el miércoles siete de junio
del año de mil novecientos sesenta y uno».

Comencé a hojearlo, sin fijar mi atención. Eran trescientas y
tantas cuartillas de una escritura fina. En esos momentos lo
absurdo del rótulo hirió mi mente: novecientos sesenta y uno
---me dije---. Y volví a fijar mi vista en él, esta vez con mayor
cuidado. No cabía duda; allí estaba la frase escrita con firmeza:
«Terminé de escribir esta historia el miércoles siete de junio
del año de mil novecientos sesenta y uno».

Púseme a conjeturar que estaba frente a una fantasía, aunque
no me explicaba por qué el manuscrito había sido depositado en
ese lugar. Y probablemente me habría conformado con esa explicación,
a no ser porque en esos momentos mis miradas se dirigieron hacia
la chimenea.

Allí, sobre las cenizas, estaba el diario que había servido de
envoltura. El suave calor lo había tostado poco a poco, pero
sin transformarlo en llama. Todo el fondo del papel tenía color
de cacao y en un tono más oscuro se destacaban ligeramente las
letras: «Excelsior» ---«El periódico de la vida nacional»---.
Las orillas comenzaban a consumirse con una combustión lenta,
sin flama; una delgada orla de fuego las iba royendo como un
ratoncillo diminuto. Abajo de las letras que podía percibir desde
mi asiento había unas líneas y otras palabras que ya no alcanzaba
a leer.

No sé qué impulso me hizo levantarme de pronto y arrodillarme
junto a la fogata extinguida. Con mi linterna enfoqué todo ese
encabezado. Y allí vi claramente, sin que hubiese lugar a la
más ligera equivocación: México, D. F., domingo 11 de junio de
1961.

El cerebro me dio vueltas y cometí una imprudencia; pretendí
salvar del fuego esa prueba fehaciente de mi extraña aventura.
(En realidad eso era: una aventura). Pero al tocarla se deshizo
en partículas diminutas e impalpables; el tenue tiro de aire
producido por el calor las hizo ascender hacia la chimenea; otras
partículas flotaron en el ambiente de la pieza. Quise salvar
los restos de aquella catástrofe, pero todo fue inútil; los pedazos
de letras se deshacían apenas los tocaba. Entonces me levanté
y fuíme a sentar nuevamente en el sillón.

No se me ocurrió, en ese instante, emprender la lectura de esa
novela, o crónica, o documento, o como quiera que se le llame.
Mis pensamientos tomaron otro sesgo.

En primer lugar lo acaecido era tan extraordinario que estaba
fuera de lo normal. Empecé yo mismo a dudar de mi memoria y del
testimonio de mis sentidos; recordé mis trastornos mentales y
la idea me asaltó de que había sido víctima de una alucinación.
Es cierto que las cuartillas estaban allí, en mis manos; pero
la envoltura ya no existía; quizá todo había sido una fugaz recurrencia
de mis achaques.

Entonces pensé que era preciso enterar a mi huésped de lo acaecido;
pero comprendí inmediatamente que si en ese momento, apenas unos
minutos después del desconcertante acontecimiento yo mismo me
sentía incierto y admitía la posibilidad de una ilusión, con
mucho mayor motivo los demás pensarían que todo era el producto
de mi mente enferma; nadie dejaría de diagnosticar una recaída
de mi salud. No me quedaba otro remedio sino mostrar las páginas
que, esas sí, eran una realidad en mis manos; pero aun en ese
caso no faltaría quien calificara lo del hallazgo como una broma
mía, bastante inocente. Podrían conjeturar que intentaba, con
mi fábula, despertar el interés hacia un relato salido de mi
caletre, pues era sabido que fui un literato fracasado y que
cuando joven publicaba cuentos bastante malos en las revistas
ilustradas.

Decidí, por lo mismo, guardar el secreto sobre el suceso: los
escondrijos, la envoltura, el escrito mismo. Más tarde renovaría
la búsqueda en la chimenea, pues ahora, más que nunca, sentía
que el misterio me rodeaba y después de lo acontecido el encuentro
de un tesoro era algo natural o si se quiere mucho menos maravilloso.

Me quedé leyendo aquí y allá algunas páginas del manuscrito,
pero estaba cansado y el sueño me sorprendió sentado a la luz
de la lámpara. Desperté cerca de las dos. Entonces me levanté,
caminé a mi cuarto, metíme en la cama y dormí como un lirón hasta
las nueve del día siguiente.

Al despertar, después de una noche sin imágenes, pensé que mi
aventura era un sueño y como quien teme haber sido despojado
de una reliquia, busqué ávidamente con mis manos, bajo la almohada.
Los papeles seguían allí. Entonces me quedé largo rato absorto;
pensé en la cuarta dimensión, en el espacio-tiempo, en la teoría
de la relatividad; pero cualquier exégesis no pasaba de ser una
fantasía. En cambio yo estaba sentado en una cama que era real,
cobijado con unas sábanas que mis manos palpaban; y tenía ante
mis ojos el rubro inquietante y conciso: ---«Terminé de escribir
esta historia el miércoles siete de junio de mil novecientos
sesenta y uno».

Esto amerita una investigación ---me dije--- y comencé a hacer
planes.

Abrevié mi estancia en San Miguel; exploramos los recesos de
la chimenea, pero con resultados nulos. Mientras tanto había
estado leyendo las cuartillas en cuestión; no sabía qué pensar
de ellas. El hecho es que mi amigo y yo dejamos a medias nuestros
sondeos y nos prometimos reanudarlos en mis vacaciones de invierno.

En México todos mis propósitos se concretaron únicamente a adquirir
una certeza: la de que nunca se había publicado un número de
«Excelsior» ---ni siquiera un título--- que dijese: domingo 11
de junio de 1961. Eso solo podía averiguarlo con los cajistas
del periódico.

Me valí de uno de los reporteros, quien era mi amigo porque abrevaba
en la fuente de mi Secretaría ---para usar la jerga periodística---
donde yo le proporcionaba noticias. Cuando interrogamos a los
obreros nos vieron con ojos burlones y contestaron a nuestras
preguntas como si tuviesen frente a ellos a un par de orates;
pero quedó bien dilucidado que nunca, ni en broma, habían impreso
aquella fecha. En seguida invité a mi amigo ---cuyo nombre no
menciono por elemental discreción--- a cenar en un restorán cercano.
Sobre una carne asada echamos una garrafa de vino y más tarde
rociamos una langosta con jerez; estaba yo gastando la espléndida
propina que esa mañana había recibido de un abogado a cambio
del pequeño servicio de esconder un expediente, lo que para mí
no significaba ningún esfuerzo extraordinario. El hecho fue que
para terminar pedí una botella de coñac y pescamos una borrachera
fantástica. Entonces recuerdo que me puse a hablar de la situación
europea y del futuro de aquel continente; predije las conferencias
de Múnich, la ocupación de Checoslovaquia, la caída de la república
en España y la toma de París en 1940. Mi amigo reía sonoramente,
me llamaba mal agorero; me dijo que el día que París se rindiese
él se colgaría de un farol y comenzó a cantar _La Marsellesa_.
Yo lo acompañé, otros trasnochados se nos reunieron y a las dos
de la mañana la juerga estaba en su apogeo. A las cuatro salí
con el propósito de dormir unas horas para dirigirme en seguida
a mi trabajo.

El incidente parecía cerrado y preferí olvidarlo; la prueba de
lo definitivo de mi curación puede verse en el hecho de que efectivamente
olvidé. En lugar de analizar enigmas me propuse continuar mi
existencia cotidiana, escéptica y desencantada, pero sin preocupaciones.
Mi manuscrito dormía en un rincón de mi armario; ya casi no lo
recordaba.

Pasaron los meses y uno de los primeros días de octubre atendía
a un solicitante quien tenía pendiente un asunto cuantioso y
de obvia justicia, iniciado hacía año y medio; esa mañana me
estuvo esperando unas dos horas mientras terminaba mis acuerdos;
en seguida le hice entrar y le expliqué que era menester anular
su último escrito, pues contenía un error grave; en lugar de
invocar el reglamento de diciembre de 1937, se acogía a las adiciones
de enero de 1938 y para colmo de descuidos, al mencionar el Diario
Oficial, había trastocado su número.

Imponíase rehacer la tramitación.

Mi visitante me contemplaba con ojos desfallecidos, cuando sin
anunciarse penetró mi amigo el periodista quien usaba del derecho
de picaporte, como dicen los políticos. Al punto me levanté y
lo llevé al balcón; esas conferencias en los balcones son características
de los funcionarios y constituyen la peor afrenta para los hombres
de trabajo que prefieren arreglar las cosas sentados frente a
los escritorios.

---Diego, necesito hablar contigo con urgencia; vamos a tomar
un trago. Te espero.

---Bueno ---le contesté--- déjame desembarazarme de este tipo.

Me llegué a donde estaba mi hombre y le endilgué:

---Perdóneme, señor Padilla. Me manda llamar el Secretario; no
puedo detenerme. Vuelva a verme a principios de la semana entrante.

Y salí de mi oficina sin sombrero, pues todos los empleados del
gobierno somos sinsombreristas cuando vamos al bar próximo a
tomar la copa.

Una vez que estuvimos sentados frente a dos tequilas dobles,
que era nuestra bebida reglamentaria cuando no teníamos delirios
de grandeza, mi amigo el reportero me interrogó con precisión:

---Oye, ¿tú te acuerdas bien de lo que dices cuando estás borracho?

Verdaderamente no supe qué contestarle.

---¿Por qué me lo preguntas?

---¿Recuerdas aquella noche en que cantamos _La Marsellesa_ con
tanto entusiasmo?

---Hombre, claro que sí.

---Pues escúchame: mi memoria se afina cuando bebo y ahora puedo
decirte palabra por palabra todo lo que vociferaste. Hablaste
mucho de la situación de Europa y entre otras cosas me anunciaste
que en septiembre habría una conferencia en Múnich. Eso fue ¿cuándo?
¿cuándo?

Yo lo sabía muy bien; las fechas en que se recibe dinero son
inolvidables.

---Fue a principios de mayo ---le dije.

---Pues bien ---replicó--- a principios de mayo presagiaste que
en septiembre, Mussolini, Daladier, Chamberlain y Hitler se reunirían
en Múnich y condenarían a Checoslovaquia. ¿Cómo lo supiste? ¿De
cuándo acá te has convertido en vidente? Y también hablaste de
que habría guerra en Europa y de que caería París. Vamos, habla.

Yo recordaba perfectamente que esas profecías las había leído
en mi famoso manuscrito de mil novecientos sesenta y uno. Lo
más sencillo era haber relatado la verdad, lisa y llana; pero
tomé un aire de misterio, como si mi aventura en sí no fuera
ya bastante misteriosa, y le dije que en ocasiones, como una
ráfaga, veía desfilar ante mi imaginación sucesos futuros.

---¿Y necesitas embriagarte para eso? ---me preguntó mi amigo.

---No siempre, le respondí.

Entonces se acercó hacia mí, a través de la mesa y me dijo:

---Escúchame Diego; me guía un interés exclusivamente periodístico.
Vamos a publicar una revista que saldrá cada semana. Nos hemos
juntado varios del oficio y estamos preparando el primer número.
Quiero hacer un reportazgo sensacional; voy a referir nuestra
cena y tus vaticinios y además te voy a formular una serie de
preguntas y tú las vas a contestar.

---No tengo inconveniente, si es que eso te puede servir.

Recogí el cuestionario y al llegar a mi casa saqué las cuartillas
del ropero donde estaban entre mis camisas; redacté entonces
las respuestas.

Dije que se declararía la guerra en 1939, que Polonia sería invadida
y destrozada, que Bélgica, Holanda, Noruega y Dinamarca serían
ocupadas, que Italia se aliaría con Alemania, que Londres sería
bombardeado, que los nazis invadirían Rusia y finalmente que
los Estados Unidos lucharían por la posesión del Pacífico y perderían
las Filipinas.

Había en todos aquellos augurios bastante dinamita para volar
el planeta.

Pero esta entrevista, con un retrato mío, pasó inadvertida. Se
publicó a fines de octubre de 1938 en el único número de un semanario
rotograbado, llamado «Vita». Casi nadie la leyó; a mí me obsequiaron
con algunos ejemplares que sirvieron para atizar mi baño. Sus
escasos lectores deben haber supuesto que mis declaraciones eran
un alarde vulgar de sensacionalismo descabellado, pues aquellas
anticipaciones no podían caber en ninguna cabeza sensata. La
revista murió de inanición y el segundo número jamás vio la luz
pública.

La existencia continuó su marcha acostumbrada. La clase privilegiada,
que comienza con los políticos enriquecidos y se extiende a los
hombres de negocios, a los banqueros, a los empleados públicos
de categoría, a los líderes proletarios y después a todas las
otras especies, géneros, órdenes y grupos de parásitos, siguió
cabalgando implacablemente sobre los riñones de la indiada que
constituye en México, por antonomasia, la clase sin privilegios.

El país continuó su marcha lenta, pero ascendente, hacia la civilización
o lo que se ha dado en llamar civilización donde hay millares
de automóviles, donde se viaja en aeroplano y donde los ferrocarriles,
aunque manejados por un gobierno inepto, representan una pequeña
ventaja sobre las antiguas diligencias.

La agricultura ha continuado decadente; la gran industria comienza
a surgir y así hemos principiado a incrustar en nuestras carnes
los grandes problemas que son el cáncer de los países industriales.
Los campesinos contemplan sus ejidos improductivos; una entidad
enigmática que se llama el Banco les entrega salarios de miseria
y les hace firmar papeles misteriosos.

Es cierto que los hombres que tratan de poner un poco de orden
en este caos social van a veces a Acapulco en automóvil y usan
en sus casas cocinas eléctricas, pero en cambio viven en la inquietud
y la zozobra y están siempre a merced de un Estado arbitrario,
omnipotente y transgresor. Por eso a menudo mueren de un sincope
cardíaco, de diabetes o de una úlcera nerviosa.

Mientras tanto
se hicieron las elecciones: la voluntad popular fue burlada y
gracias a ese fraude nos libramos en México de un gobierno de
mercachifles sin escrúpulos. Más tarde, hemos entrado a la guerra…
Tenemos siquiera un presidente bueno, aunque…

¿Pero a dónde voy; a dónde me conduce esta logorrea desenfrenada?

¿Yo, que debería callar, porque pertenezco a la casta omnipotente;
a la burocracia que exprime con sus garras las ubres providentes
de este país maravilloso?

Una noche del presente año, 1943, se presentó en mi casa, calle
de Morelia número 97, en la colonia Roma Sur, un caballero alto,
de cabellos blancos y vestido con pulcritud impecable. Hizo pasar
su tarjeta, lo introduje a mi pequeño despacho y allí ---a invitación
mía--- comenzó a hablarme; su español era correcto, con ligero
acento extranjero. {.espacio-arriba1}

Aseguróse de mi nombre y apellido y en seguida sacó de su bolsillo
un recorte de periódico; lo reconocí desde luego; eran mis profecías
de «Vita».

---¿Usted escribió esto? ---No, señor, ---respondí--- yo me limité
a contestar un cuestionario---. Mi interlocutor sonrióse y continuó:

---Tiene usted razón; y eso es lo único que me interesa---. Siguió
haciéndome multitud de preguntas hasta que decidí mostrarle mis
memorables cuartillas del año de sesenta y uno. Las tomó en sus
manos y se puso a hojearlas parsimoniosamente con actitud de
experto. Al poco rato me interrogó:

---¿Desde cuándo están en sus manos?

---Pues… verá usted; desde abril de 1938.

---¿Me puede dar usted algunos detalles?

Entonces, no sé por qué causa, después de haber guardado mi secreto
a mis mejores amigos, hice una confesión completa ante ese desconocido.
El me miraba con evidente interés. Cuando terminé me habló pausadamente,
en un tono que no admitía réplica:

---Voy a hacerle una proposición: Soy editor y deseo publicar
este manuscrito. Solicito de usted una pequeña introducción,
digamos dos o tres páginas, en las que explique cómo llegó a
su poder. Después de vendidos los primeros cinco mil ejemplares
le daré a usted alguna participación por cada uno que exceda
ese número. ¿Qué me responde?

Yo no era tan lerdo para ignorar que nunca, en todos los años
que me restan de existencia, se agotaría una edición de cinco
mil ejemplares. Sin embargo, acepté en el acto; me halagaba extraordinariamente
el que esta introducción o prólogo, o preámbulo, o introito,
o como quiera llamársele, fuese publicado.

Era una oportunidad sin precedente de darme a conocer y estipulé
únicamente que no se restringiría su extensión. Mis condiciones
fueron aprobadas.

Y en seguida tienen ustedes lo que un desconocido escribió ---(¿o
escribirá?)--- el año de mil novecientos sesenta y uno.

</section>
<section epub:type="chapter" role="doc-chapter">

# I

++En++ el mes de diciembre de 1960 comienzo a escribir estas
líneas; estoy sentado ante una mesa en la recámara que el ingeniero
Teodoro Uranga me ha destinado en su casona de San Miguel Allende.
La mesa, a su vez, está colocada ante una gran ventana que ve
al sur; por ella entra el sol de invierno. Veo ante mí el rimero
de blancas cuartillas que debo ir llenando una a una. ¿Cuántas?
No lo sé; quizás cien, o doscientas o más. Voy a escribirlas
con lo que refieren muchas gentes que vieron los tristes días
de la invasión. Durante esos años el dolor envolvió la tierra,
como una mortaja; parecía que la especie humana quería desaparecer
a toda costa, y que, para preparar el advenimiento de otra pareja
edénica el planeta entero debía someterse a la esterilización
por el hierro y el fuego.

Conocemos los hechos escuetos que culminaron con la guerra, la
competencia de los mercados, los errores del convenio de Versalles;
también sabemos ahora cómo se modificó la estrategia y se mecanizaron
los ejércitos; conocemos los choques de tanques y el último raid
sobre Alemania en que los aviones aliados recordaban las flechas
de los ejércitos persas: que eran tantas que oscurecían la luz
del sol.

¿Para qué entrar a escudriñar sucesos que otros mejor preparados
han estudiado ya? Lo único que puedo decir es que nos sentimos
ufanos de vivir en este mundo nuevo.

¿Nuevo? Yo diría que nuevo porque es una novedad que los hombres
hayan encontrado un camino de sensatez; pero no lo es si consideramos
que nos regimos por principios eternos que hace dos mil años
Dios mismo o un Hombre Divino reafirmó con su sangre.

En la lucha que todavía a mí me tocó presenciar ---aunque entonces
no medía su alcance--- fueron estrujadas las dos cosas más caras
a los hombres: el amor y la libertad. Las llamo cosas en su alto
sentido de entidades espirituales, sin las cuales la vida es
imposible. Entonces México luchó por conservarlas y hasta los
mismos equivocados de buena fe contribuyeron con su desengaño
y muchas veces con su sangre a hacer surgir otro país.

¿Cómo podré acometer esta tarea? A falta de experiencia estoy
lleno de buena voluntad. Me consuelo pensando que esto no es
una novela, sino una sucesión de acontecimientos en los cuales
alientan seres vivos. Tengo además mis veinticinco años, y el
optimismo de la vida que ha sido mi lote. Pero ya es tiempo de
poner manos a la obra.

Esta historia comienza en San Miguel Allende, en 1912, cuando
Verónica Arriaga, una muchacha de ese pueblo, dio promesa de
matrimonio a Rodrigo Guerrero, abogado del mismo lugar. En realidad
he mencionado una fecha para situar en el tiempo a aquellos que
merecieron la ventura y que al final de sus vidas encontraron
solo el dolor; pero podría decirse que la historia empieza antes,
cuando Verónica y Rodrigo se conocieron, y comprendieron por
primera vez que estaban destinados a identificarse en una existencia
común. Aunque ellos con su carne y sus huesos pisaron las baldosas
de San Miguel, respiraron su aire, bebieron su agua y recibieron
su sol, no han sido quizás sino la reencarnación de otros Rodrigos
y otras Verónicas, igualmente jóvenes, bien hechos de sus cuerpos
y limpios de sus almas, que también se prometieron cariño, se
amaron, se casaron y tuvieron hijos, los que a su vez, después
de cerrar para siempre los ojos de sus padres, fueron Rodrigos
y Verónicas; y así sucesivamente por generaciones sin cuento.
Debería en rigor decir que Rodrigo Guerrero y Verónica Arriaga
pudieron venir al mundo en cualquier otro lugar de México, lo
mismo en Durango que en la caliente Veracruz. Quiero hacer entender
con estos circunloquios que la vida que ahora vivimos en 1960
solo se ha hecho posible porque está construída y amasada con
los corazones de todos los Rodrigos y todas las Verónicas que
han nacido y alentado en este suelo.

Pero al idealizarlos no les quito realidad; al hablar de ellos
como de la savia que todavía nos nutre, o de la sustancia que
se transmite incorruptible de unas almas a otras en el correr
de los tiempos, no quiero convertirlos en un símbolo frío.

Rodrigo y Verónica vivieron como vivo yo ahora; en San Miguel
los conocieron las gentes con las cuales hablo y todavía abrazo
a sus hijos y a sus nietos. Esta historia data de hace apenas
unos lustros. Se entra a la casa de Don Rodrigo y allí está aún
el ancho sillón donde leía durante las veladas, la mesa de roble
en el comedor, los árboles en el huerto, los rosales del jardín
y los anaqueles de la biblioteca, que la barbarie nazi vació
casi por completo.

Quien habla mucho de él es Miguel Torres, el herrero. Cuando
lo invito a comer y abro una o dos botellas de vino, se pone
locuaz desde el segundo vaso, y cae en este tema. Dice que Don
Rodrigo era pocos años mayor que él, pues Miguel cuenta a la
fecha sesenta y tantos. Lo describe, por supuesto en su juventud,
como un muchacho no muy alto, que tendía ligeramente a la gordura;
se peinaba echándose el pelo hacia atrás con una raya en medio
y usaba un bigotillo arriscado que iba cambiando con la edad
y la moda.

---Para mí fue el mejor amigo; ---dice Miguel---. Aunque yo he
sido siempre herrero, y herrero me he de morir, y él era notario
y licenciado, pues le gustaba venir a la herrería y verme machacar
el fierro caliente. Se sentaba en este banquito de mezquite que
ve usted aquí; se quitaba el sombrero, se secaba el sudor y se
ponía a decirme cosas. ---¿Conque es cierto que te emborrachaste
el domingo en la noche, Miguel? Me contaron que te orinaste en
los calzones porque no atinaste a abrirte la bragueta---. Yo
en aquel tiempo tomaba mucho aguardiente y es cierto que era
borracho; pero no me gustaba que me lo dijera nadie; ni siquiera
Praxedes el carnicero que era tan borracho como yo. Pero con
Don Rodrigo no podía disgustarme; me decía esas cosas chanceándose
para que se me quitara el vicio y a él le debo que ya no me embriague.
Cuando lo mataron hice una promesa y la he cumplido, pues una
de las cosas que me decía era que si se moría antes que yo, iba
a venir del otro mundo a darme a oler amoníaco. Claro que nunca
he creído en los difuntos, aunque les tengo mucho respeto; pero
cuando supimos que lo habían matado lloré junto con mi mujer
y juré no volverme a embriagar. Ahora tomo hasta una botella
de vino o cerveza, allá muy de vez en cuando y eso no me hace
mal; pero nunca me ha vuelto a suceder que se me levanten las
losas de la calle para pegarme en la frente. Quería decirle que
esa tarde que supimos lo de su muerte se armó una tremolina y
salieron los alemanes tras Doña Verónica. Yo hice allí otro juramento:
que por cada año de vida que le habían quitado a Don Rodrigo
yo acabaría con uno de ellos. Y se me hizo mi gusto porque llegó
una noche en que me puse con una ametralladora, allí en la calle
de la Canal y estuve matando nazis hasta que me dieron un balazo
que me atravesó el pulmón.

---Bueno, Miguel, ¿y te acuerdas de Verónica?

---Claro que sí; pero déjeme acabarle de decir. A mí me gustaban
mucho las mujeres y un año, durante las fiestas, vinieron unas
familias de San Luis de la Paz y traían una muchacha, la verdad
muy bonita. La anduve siguiendo y antes de que terminara el «borlote»
aquí en San Miguel ya me había largado con ella rumbo a Celaya
y por fin a Irapuato. Allí me puse a trabajar y anduve en ese
enredo como siete meses, pero esa rancherita era ---con perdón
de usted--- muy puta, y una noche se me fue con un sargento de
la guarnición. Entonces me volví a San Miguel, con la cola entre
las piernas, sin saber cómo estarían mi vieja y mis dos chamacos,
pues no les mandé ni un peso en todo ese tiempo y es que la otra
me tenía «ahuitado». Llego y los encuentro muy bien y la casa
muy limpia. Yo al principio creí que me habían encontrado reemplazo;
pero no, era que Don Rodrigo había estado mandando el diario
a mi casa y sin decirle nada a nadie porque ni Doña Verónica
lo sabía.

---Ahora sí, dime algo de Verónica, Miguel.

---Yo no sé usted qué se propone haciéndome hablar; pero le diré
que todos la queríamos mucho. Ella hizo que mis chamacos fueran
a la escuela y cuando los veía los ponía limpios y les cambiaba
ropa. Los llevaba a su casa y los metía en la tina de agua caliente,
donde bañaba a sus niños: a Remigio que usted conoce y a los
otros; y todos salían a jugar juntos y cuando venía la Noche
Buena invitaba a muchos chamacos de San Miguel, unos pobres,
como los míos y otros más pobres, como los de Isidoro Reyna,
y otros ricos y les daba una gran merienda con tamales que le
hacían las monjitas de la Concepción. También le diré que una
vez que mi chamaco el más grande tuvo… bueno, no sé; pero se
estaba ahogando, ella trajo al Doctor Bernáldez y pusieron un
telegrama a México y al día siguiente lo picaron con una jeringa
y se alivió como de milagro.

Hasta aquí lo que me cuenta Miguel el herrero. El hecho es que
Rodrigo y Verónica se casaron pobres. Es decir que él ejercía
su profesión y ya había conseguido que lo hicieran notario, pero
al principio tenía a lo sumo en su casa el pan de una semana;
el pan de la siguiente lo enviaba la Divina Providencia. Chole,
que fue nana de Verónica, que la había cargado cuando era solo
un montoncito de carne rosada, que le cambiaba los calzoncitos
zurrados y le contaba cuentos para que se durmiera, se fue a
vivir con ellos. Ganaba seis pesos al mes, pero no había que
preocuparse, podía recibirlos con atraso pues en realidad Chole
era de la familia.

El santo de Rodrigo le compraba una caja de tabacos en la que
se gastaba el sueldo de mes y medio; cuando Rodrigo estaba joven
y ya con Remigio nacido, esos eran los únicos tabacos buenos
que fumaba de cuaresma a cuaresma. Los días del cumpleaños de
Verónica le regalaba unas botellas de agua florida y gastaba
en ellas su salario de dos meses; el agua florida era una loción
que trascendía a flores del campo; las botellas traían un tapón
de corcho cubierto con papel de estaño. En aquella época en que
no se conocían los perfumes sintéticos extraídos del petróleo
con etiquetas de Schiaparelli o de Woolf, el agua florida era
el lujo de las mujeres.

Rodrigo se levantaba temprano, a pesar de que era inclinado a
la molicie, pues su primitiva clientela fue de gente pobre que
iba a verlo antes de entrar al trabajo. Muchos no podían pagarle
en dinero y le traían un huacal de pollos o una canasta con huevos.
Praxedes, el carnicero, le llevaba el filetillo de los lomos.
Entonces Verónica lo abría con cuidado y lo convertía en una
extensa lámina de carne delgada; le quitaba con escrupulosidad
los pellejos y se los daba al gato; un animal enorme y soñoliento
que vivía cerca del fogón y que se llamaba Don Tadeo. Después
ponía a asar esa carne en una gran charola llena de brasas coruscantes,
sazonada con sal, pimienta y limón, con un vago gusto de ajo
y chile ---casi un aroma--- y ella misma le llevaba al comedor
en una inmensa bandeja de plata, uno de sus muy contados lujos,
que heredara de su madre y esta de su abuela. Don Rodrigo cortaba
ese manjar y hacía porciones muy liberales para el padre Videgaray,
un mocetón que después fue cura de la parroquia y para Nicanor
Aceves, que era una especie de escritor de provincia, ejemplo
vívido de que Dios cuida de sus criaturas, pues la pasaba con
decoro aunque con extrema parquedad y nadie le conocía ninguna
ocupación estable o productiva.

Rodrigo cultivó siempre con Praxedes una amistad estrecha. En
una ocasión en que Praxedes riñó con un diputado de pistola y
sombrero tejano, Rodrigo lo defendió hasta verlo libre y mientras
estuvo en la cárcel ---unos tres meses--- Verónica mandábale
diariamente la comida envuelta en una servilleta limpia.

Don Rodrigo tenía así tratos con artesanos, con tejedores, con
talabarteros y con pequeños rancheros del Estado. Su bufete se
hizo famoso porque los clientes pagaban lo que podían y cuando
podían, y si jamás podían eran siempre bien recibidos. Cuando
llegaba la época de las cosechas le traían sacos de frijol, de
maíz o de nueces; en el otoño su casa se llenaba con el perfume
de los duraznos y de las chirimoyas. Rodrigo vio entonces que
para un hombre sencillo no devorado por la ambición, la vida
era fácil.

Pero tenía también relaciones con gentes de más fuste; profesionalmente
lo consultaban los ricachos: le llevaban sus escrituras, lo hacían
redactar sus testamentos, lo ponían al cabo de sus dificultades.
En son de amistad lo visitaban Paco Améndola y Don Estanislao
Gutiérrez; también Don José María, el viejo dueño de la fábrica
y Don Martín, el hijo único que la heredó y que fue amigo inseparable
de Rodrigo, desde que se escapaban de la escuela ---cuando apenas
levantaban lo que un becerro añojo--- para ir a bañarse a las
albercas de Ezcuinapan que les parecían inmensas.

Sin embargo, el bienestar tardó en llegar a aquel hogar recién
fundado. Verónica recosía la ropa y ayudaba a plancharla, pues
Chole no se daba a basto. Pero se veía muy bonita con sus vestidos
de percal, que llevaba con garbo, y cuando iba a la calle se
terciaba su rebozo de largos flecos. Todos la conocían y la saludaban:
Adiós, Verónica. Ella respondía sonriente.

Para las solemnidades pueblerinas tenía un vestido de seda amarillo,
que le sentaba muy bien a su piel blanca y a sus cabellos negros.
Lo modificaba para cada ocasión: a veces con olanes en la falda
o cambiándole la forma del escote, según la moda, y a sus amigas
que habían estrenado la ropa que les cosía Crucita Valdés, Verónica
las elogiaba burlándose con donaire de sus propios arreos de
fiesta. Pero los lucía con prestancia y con tanta naturalidad
y buen humor que nadie murmuraba. En resumen, todo esto acontecía
porque Rodrigo era un notario pobre y abogado con pleitos de
pobres; pero eso no era para entristecerse. En cambio era el
mejor de los hombres, siempre tranquilo y dicharachero; ambos
tenían una salud de lobos y una hambre también de lobos y además
la satisfacían con largueza. A Verónica no le gustaba mucho la
cocina, pero de vez en cuando hacía cosas con sus manos, con
objeto de escuchar los elogios de Rodrigo que metía los dedos
en la salsa de nogada para darse el gusto de chupárselos y rebañaba
los platos hasta dejarlos limpios, aunque sabía que eso era un
signo de muy mala educación.

Todavía yo alcancé a Praxedes y cuando comenzaba a hablar de
Rodrigo no había manera de pararlo. También Praxedes había dejado
la bebida, pero fue por una cirrosis del hígado de la que al
fin murió.

Este Rodrigo siempre se andaba chanceando conmigo y cuando veía
a mi hijo Apolinar, que jamás ha tomado una copa y que entonces
era ya un muchachón de veintitantos años, ---(muy amigo de Patricio
---que en paz descanse--- el hermano de Remigio)--- me decía
que me iba a pasar lo que a un viejo de su conocencia y me hacía
broma con un cuentecito que se lo oí cien veces.

---A ver, Praxedes, cómo era ese cuento.

---Pues a mi modo de ver no tiene mucha gracia. Pero así era
Rodrigo: agarraba un chascarrillo y lo repetía y se reía de él
aunque nadie le hiciera coro. Tenía un gran repertorio de cuentos
de borrachos, casi todos para hombres solos, pero él los refería
delante de las señoras y las hacía reír, porque decía las cosas
con un aire muy inocente. Yo le oí contar a las hermanas de Don
Estanislao Gutiérrez, que eran zopilotes de iglesia, aunque muy
buenas personas, una historia de un borrachito que entra a un
templo; y acabaron por sonreírse.

---Bueno, Praxedes. Pero ¿cómo era ese cuento del borracho y
su hijo?

---Ya se me olvidaba; y conste que lo refiero solo en recuerdo
de Rodrigo, que Dios ha de tener en su gloria. El adornaba el
chiste con lo de la prohibición, cuando a los yanquis les dio
la ventolera de que nadie había de beber en los Estados Unidos;
una de esas locuras que no tenían ni pies ni cabeza ni para los
hombres rudos, como yo. Ustedes, los muchachos, ya no alcanzaron
eso…

---A ver el cuento, Praxedes.

---Perdóneme. Decía Rodrigo que fueron a ver a un viejo, así
como yo, de bien cumplidos los setenta; creo que lo visitó un
periodista. El viejo estaba fuerte y duro como un mezquite: montaba
a caballo, subía los cerros y después de enviudar se había casado
con una jamona de treinta años y le regalaba un muchacho cada
dos. Pues ese viejo era como Apolinar, que nunca había tomado
una copa. Y entonces le pregunta el periodista: ---¿Y a usted
le gusta la bebida? ---Ni por pienso; el alcohol ni lo huelo.
---¿Y está usted sano? ---Ya lo ve usted;--- y mientras habla
casca nueces con sus dientes. El del periódico siguió cuestionándolo.
---¿Y usted atribuye su salud, cuando menos en parte, a su abstinencia?
---No en parte; se la debo en todo.

Rodrigo decía que esa plática la tenían en la sala de la casa
y que de pronto escucharon insultos en un cuarto vecino y ruido
de trastos que se rompen con un gran fracaso. Entonces aquel
setentón le dijo al periodista: ---no se preocupe; es mi padre
que se emborracha todos los días y le da por romper la vajilla.
Eso es todo. Como verá usted no tiene chiste; pero Rodrigo decía
que eso le iba a pasar a Apolinar conmigo y él mismo se festejaba
la historieta con grandes carcajadas.

En el pueblo había vivido un tipo original, siempre inconforme.
Se llamaba Maclovio Otamendi y era el último retoño de una vieja
familia guanajuatense. Lo introduzco aquí por las hondas ligas
de afecto que lo unían a Rodrigo y los suyos. Había comenzado
a estudiar para cura, pero abandonó el seminario porque dijo
que se sentía con fuerzas para guardar todos los votos: el de
obediencia, el de pobreza, pero no el de castidad, y él no quería
ser un fraile prevaricador, corruptor del clero o de sus hijas
espirituales. Era buen católico cuando entró al seminario y salió
de allí buen católico, a diferencia de otros que cuando destripan
de la carrera del sacerdocio se convierten en jacobinos come-curas.
Fue antiporfirista sincero y despotricaba contra esa dictadura;
allá por mil novecientos siete u ocho hizo un discurso para una
repartición de premios, echó pestes contra el gobierno y se dirigió
a los niños que lo escuchaban haciendo una fábula en que comparaba
al «círculo de amigos del General Díaz» con un rebaño de borregos.
Entonces el jefe político del pueblo lo mandó aprehender y lo
tuvo por varios días en un cuartucho oscuro y lleno de chinches.
Rodrigo comenzó a gestionar su libertad y tuvo que ir a México.
El caso llegó hasta el mismo dictador, quien leyó el discurso
y parece que estuvo bastante de acuerdo con las opiniones de
Maclovio, porque dio instrucciones de que lo soltaran y Rodrigo
regresó a San Miguel con la orden de libertad. Pero Maclovio
continuó antiporfirista de hueso colorado y al final de cuentas,
para no seguir chocando con el jefe político, se fue a México.
Allí, después de algunos años de lucha se casó y tuvo dos hijos:
Armando y Guadalupe. Enviudó cuando ambos eran pequeños y dedicóse
a educarlos lo mejor que pudo; como él decía: ---en el amor a
la libertad y el temor de Dios.

Su máxima favorita era: ---Primero hay que enseñarles el amor
a la libertad y después a temer a Dios, pues el que no es libre,
no puede sentir el santo temor de Dios. Esto lo encontraba muy
dentro de la ortodoxia. En México estudió Filosofía, Derecho
y aprendió a encuadernar libros; puso un taller, pero los clientes
le llevaban sus encargos y no volvían más. Entonces con esos
rezagos inició un comercio de libros viejos y al final de cuentas
abrió una librería de usado en la avenida Hidalgo, por la alameda;
pero nunca mezcló el tráfico de libros ---que él alegaba era
noble--- con la usura ni con la explotación de los estudiantes
pobres; por eso jamás hizo dinero ni adquirió fama de protector
de las letras.

Tuvo amistad muy estrecha con el padre de Verónica y a esta la
cargó cuando era un rollito de manteca; y más tarde, cuando iba
a la escuela, le contaba el cuento de la Caperucita y la enseñó
a cuidarse de los lobos, aunque a la postre nadie supo defenderla
contra aquellos que se la habían de comer, porque penetraron
a su casa por las rendijas de las ventanas y por debajo de las
puertas, arrastrándose como sombras.

Maclovio era también muy amigo del padre de Rodrigo y acabó por
apadrinar a este en su matrimonio; pero intimó más con la pareja
recién casada en el transcurso de los años, al llegar los hijos,
tanto para unos como para otros.

Al estallar la Revolución se unió a Madero, pero cuando este
fue Presidente lo criticaba con acritud porque decía que era
un inepto y había hecho contubernio con la mierda porfirista.
Acabó por escribir en un periodiquito de oposición que se llamaba
«El Mañana», en donde hacía chistes mordaces contra los ministros
del régimen. Pero cuando el asesino Victoriano Huerta apuñaló
a Madero por la espalda y de paso pisoteó los sentimientos de
decencia de las gentes, sentimientos que en aquella época se
tenían en mucho, entonces, digo, Maclovio fue el primero en protestar
airadamente.

Las cámaras seguían sesionando; pero Maclovio, el mismo día del
crimen, se fue a los diarios pretendiendo que le publicaran un
artículo candente contra el general Huerta. Como no lo consiguió,
tiró unos millares de hojitas sueltas repartiéndolas en la calle.
Tuvo que ocultarse y su amigo Alonso Quintanar le dio asilo.
Por fin fue a reunirse con los revolucionarios; anduvo con las
fuerzas de un general que se llamaba Pablo González, pero cuando
entraron a Monterrey y quemaron los confesonarios de las iglesias
e hicieron ejercicios de tiro al blanco fusilando las imágenes
de los santos ---algunas que vinieron de España en el siglo ++xvi++---
llamó cafres a los que eran sus correligionarios y les dijo que
esos excesos no lo escandalizaban como católico, sino como hombre
civilizado y de sentido común. Estuvieron a punto de pasarlo
por las armas.

Más tarde Veracruz fue ocupado por los americanos. Los jefes
de aquellos que peleaban contra el gobierno de Huerta vieron
en los barcos yanquis el instrumento de la justicia divina que
se esgrimía en su favor; en los Estados Unidos, para justificar
esa torpeza criminal, se trató de envenenar el espíritu del pueblo
americano haciéndonos aparecer a los mexicanos como un país de
bandidos desalmados.

Maclovio hizo oír su voz iracunda. Dijo que esa ocupación había
sido un acto imbécil y que se había asesinado a muchos americanos
y mexicanos por el capricho de un vesánico. De paso llamó traidores
a los revolucionarios y repitió sus denuestos contra Victoriano
Huerta.

El resultado fue que habiendo insultado a todas las facciones
se vio obligado a huir y se refugió en los Estados Unidos; allí
se le dejó vegetar en paz algún tiempo. Desde esa época solía
decir que ese era un país de gente libre.

Diré ahora quién fue Alonso Quintanar, ese amigo de Maclovio.
Si bien ya no se cocía del primer hervor, nadie lo llamaba don;
hasta los hijos de Rodrigo le decían Alonso a secas. Aunque Don
Rodrigo le profesaba gran cariño, daba la impresión de que no
era sino un señor que hacía teorías, alejado de la lucha; quizá
soy injusto juzgándolo así, pues cuando fue necesario arriesgó
su bienestar y su vida; mi concepto proviene quizás de que, en
una época en que los sacrificios no se escatimaban, él murió
en su cama. Sin embargo, no fue por su culpa, pues dio motivos
de sobra para ser enviado al patíbulo, como muchos otros.

Tenía una pequeña fábrica de juguetes que manejaba con un reducido
grupo de obreros, apenas unos treinta. Su sistema de trabajo
era muy estrafalario: no había horas de entrada ni de salida
y les decía siempre que ese pequeño negocio era una lotería,
la cual les daba para vivir bien en un mundo desbarajustado,
donde las gentes aptas no sufrían ninguna competencia. Cuando
sobraba dinero, pues el negocio era muy pingüe, lo repartía
según las necesidades de cada quien, obligando a sus trabajadores
a que enviasen a sus muchachos a buenas escuelas.

En México conoció a Maclovio y este lo llevó a San Miguel donde
intimó con Rodrigo y Verónica.

Era huésped obligado cuando había bautizos o se celebraban cumpleaños.
Se entendía bien con Maclovio, pero junto con este vivía en pugna
con Rodrigo; discutía interminablemente sobre muchas nociones
que ahora nos aparecen sin sentido. Rodrigo basaba la existencia
en tres o cuatro virtudes que en su conversación eran como un
ritornelo: el orden, la disciplina, el trabajo. Debo decir
que son también un ritornelo en este relato. Alonso le replicaba
que era un campeón de virtudes ajenas, pues que Rodrigo apenas
si trabajaba y era el prototipo de la indisciplina y del desorden.
Rodrigo, además, alimentaba una pasión violenta contra los yanquis;
en este sentido su actitud no podía calificarse de excepcional
pues abundaban aquellos que entonces profesaban al tío Sam un
odio sincero. Ahora, en 1960, este sentimiento es para nosotros
incomprensible; pero en esa época constituía la moneda corriente
y fue necesaria la dura prueba de 1946 y 1947 para que los dos
pueblos se acercasen tendiéndose los brazos por encima de una
montaña de papeles oficiales que por millones y millones vomitaban
las oficinas públicas de ambos países, tanto en inglés como en
español.

A Rodrigo lo visitaban otros muchos de la villa. Su médico era
el Doctor Bernáldez, quien había vivido antes en Guanajuato.
Don Rodrigo hacía chascarrillos a sus costillas y le decía que
había emigrado de la capital del Estado porque los cementerios
estaban ya llenos con su clientela. Un día que en una tertulia
le preguntaban a Bernáldez si creía en los espíritus, Rodrigo
se apresuró a contestar por él diciendo: ---desde luego aseguro
que no cree en ellos; si creyera ya habría cambiado de profesión.

Bernáldez se defendía riendo; decía que había tenido que abandonar
Guanajuato, no por las razones que alegaba su amigo el notario,
sino porque nadie le pagaba. ---Tomé la costumbre de ir a todos
los entierros haciendo correr la voz de que yo había sido el
médico del difunto. De esa manera me desprestigiaba un poco para
que no se me acercase tanto enfermo indigente.

---Quizá sea cierto eso ---le replicaba Rodrigo---. Pero lo que
sucede, mi querido doctor, es que usted está en la primera etapa
de su carrera: cura a sus pacientes de balde. ---¿Y cuál será
la segunda? ---preguntaba Bernáldez---. Cuando esté ya bien reputado
los matará por mucho dinero---. Esta cuchufleta, que Rodrigo
leyó tal vez en alguna revista, la festejaba después con risotadas
homéricas y la sacaba a colación con frecuencia.

Mientras tanto Verónica lo aprisionaba en las redes de su amor;
las iba tejiendo con paciencia infinita por medio de sus palabras,
de sus caricias, de sus gestos. En las noches deshacía con suavidad
las tertulias donde Rodrigo discutía o jugaba dominó para llevarlo
a su despacho a estudiar los negocios de su clientela; él aparentaba
una pereza que no sentía plenamente para representar su papel
de marido subyugado por su mujer; pero subyugado con un yugo
tan dulce que lo ostentaba con alegría.

Verónica se arrellanaba a su lado en un sillón y cosía o leía,
mientras Rodrigo consultaba los códigos casi maquinalmente. En
medio de este trabajo y mientras tomaba notas que habrían de
servirle para preparar un alegato o una defensa, charlaba con
Verónica de temas triviales; muy a menudo de los amigos de la
casa: ---Oye, mujer, tú que le tienes más confianza, porque según
él dice lo orinabas cada vez que te sacaba de la cuna, dile a
Maclovio que se corte el pelo; esas melenas están bien en la
adolescencia, cuando le da a uno por la bohemia o por hacer versos;
pero ahora me traen a la memoria un libro de fábulas que tuve
en mi niñez. Tenía unos litograbados muy siglo ++xix++; en uno de
ellos un león de antiparras pintaba su autoretrato. A través
de mis recuerdos le encuentro un gran parecido con Maclovio.

---¿Pero cómo quieres que vaya yo a decirle esas cosas? Lo que
pasa es que ahora murmuras de él, como en ocasiones del cura
Manrique o de Alonso o de Don Aurelio. ---Bueno, mujer; pero
los critico sin veneno. ¿Qué es lo que digo? Que el cura viene
a comer aquí siguiendo un sistema de su invención con objeto
de aparentar que su presencia es casual; pero yo le tengo agarrada
la cábula y puedo predecir sus visitas. ¿No te has fijado que
se mosquea un poco cuando le digo al llegar que ya tiene su asiento
en la mesa? ¿Y qué digo de Alonso? Que se pedorrea con disimulo,
alza una nalga y se figura que nadie se percata, y yo me divierto
observándolo. De Don Aurelio digo que usa corsé para aparecer
más derecho y le pico la barriga con el dedo para gozar viéndolo
ponerse colorado; digo también que es como las cebollas, pues
tiene la cabeza blanca y el rabo verde; pero en nada de eso hay
dolo. Además son mis amigos y me quitaría la camisa por ellos.

Fue durante los años borrascosos de la revolución, cuando Verónica
y Rodrigo tuvieron que echar las bases de su hogar. Llegó un
momento en que se sintieron muy aislados porque muchas gentes
abandonaron San Miguel; los invitaban a marcharse a Guanajuato
o a la ciudad de México, pero Rodrigo se resistió siempre. Pensaba
que como él no le hacía daño a nadie, estaba a salvo de cualquier
atropello. Seguía atendiendo a sus clientes que llegaban de los
ranchos a exponerle sus querellas; se multiplicaban los delitos
de sangre y Rodrigo abordaba defensas peligrosas escudado en
su misma bondad; cuando naufragaron todas las nociones que su
mente de jurista consideraba indispensables para el buen orden
social, él veía a generales y a políticos. Entablaba con ellos
conversaciones salpicadas de dichos picarescos y más de una ocasión
se salía con la suya. El jefe de las fuerzas de la plaza era
el Coronel Estanislao Fonseca. Rodrigo tenía que verlo casi a
diario para solicitar algo a favor de sus clientes: uno estaba
en la cárcel, otro pedía una indemnización porque las fuerzas
del coronel le habían llevado una yegua y matado una ternera;
otro quería un salvoconducto para ir al Norte. Este militar murió
en una cacería, por un accidente, aunque las malas lenguas decían
que lo habían mandado asesinar. Como una de sus pasiones era
la caza, Rodrigo lo lisonjeaba refiriendo que les tiraba a los
venados con rifle, pero que para no estropearles el cuero les
daba en las patas. El coronel se reía satisfecho y agregaba:
---Licenciado, algo le ha faltado decirles a los señores. ---¡Ah,
sí! ---terminaba diciendo Rodrigo, como si efectivamente se le
hubiese olvidado lo más importante del cuento;--- cuando ando
con mi coronel, nunca dejo de decirle: ---a ver, coronel, péguele
a la pata trasera izquierda, o a la que se me ocurre; y nunca
la yerra.

Así fue Rodrigo capeando el temporal revolucionario. Él se daba
cuenta de que esa sacudida venía del hambre y sed de justicia
jamás apaciguadas, a las cuales habíase referido uno de los mejores
hombres del porfirismo; pero todo se veía tan confuso y las pasiones
andaban tan revueltas que prefirió quedarse en su casa entregado
a su mujer y a su hijo mayor, que comenzaba a balbucir sílabas
sin sentido.

Pero la revolución lo marcó para siempre con un dejo de escepticismo.
Nosotros ahora, cincuenta años después de que Madero acabó con
la leyenda de la paz porfiriana, estamos demasiado ocupados en
construir para ponernos a hacer una revisión de ese pasado inmediato;
pero comprendemos bien que sobre los espíritus como el de Rodrigo
Guerrero, ese período agitado tuvo una repercusión triste. A
la distancia de diez lustros lo que sí puede asegurarse es que
los que se llamaban a sí mismos revolucionarios cometieron el
crimen de hacer burla de una gran ansia de generosidad que embargaba
a las almas; y tenemos que decir que esa revolución que encubrió
tanta podredumbre la estamos haciendo nosotros ahora, en 1960.
La estamos haciendo sin gritos libertarios, sin políticos y sin
líderes; nos bastan el sentido común y la buena fe. Pero hablar
de esto me llevaría muy lejos y mi tarea es otra: seguir a Verónica
y a Rodrigo por las calles de San Miguel, entrar con ellos a
su casa, oírlos charlar y aspirar con delicia ese ambiente de
dicha bien habida. Sin embargo, es cierto que Rodrigo perteneció
a una generación desencantada; por eso ha llegado a ser un axioma
que la invasión nazi obró como un cauterio que cura una pústula
e hizo surgir de sus cenizas una patria nueva.

</section>
<section epub:type="chapter" role="doc-chapter">

# II

++Rodrigo++ tuvo suerte ---me dice Ceferino Carrión--- que cuida
y ordena el archivo en la Casa Municipal, porque cuándo entraron
los villistas él se encontraba en Guanajuato arreglando unos
negocios. Venía al frente de las fuerzas un general que creo
se apellidaba Martínez; tomaron Dolores Hidalgo y pasaron por
Atotonilco, pues entonces la presa de Begoña no estaba construída
y estos lugares se comunicaban con San Miguel por un mal camino.
Cuando la soldadesca de Martínez entró a la ciudad traían las
casullas como sudaderos para los caballos y yo vi a la gente
de tropa sesteando en las bancas del jardín, embozados con las
capas pluviales que se habían robado en el Santuario, y también
usarlas por las noches a guisa de frazadas para dormir con las
soldaderas. El General Martínez estuvo aquí una semana y en un
solo día fusiló en el jardín a trece vecinos.

Carrión continúa relatando atrocidades; lo cierto es que el pueblo
nunca se sintió tan maltratado y necesitóse la invasión nazi
para que se repitieran esos actos de barbarie. Rodrigo regresó
cuando las cosas estaban un poco más tranquilas.

Así llegaron los años de diecisiete y dieciocho y las gentes
se pusieron a trabajar.

Remigio nació en tiempos todavía muy revueltos; los curas eran
perseguidos y no se celebraban actos de culto, pero el padre
Manrique echó sobre la mollera del recién nacido las aguas bautismales
en presencia del Coronel Fonseca.

Para Verónica se abrió una nueva existencia. Su ternura de mujer
inteligente púsose al servicio de ese muñequito indefenso hecho
de una sustancia tan suave; lo amamantaba, cosía para él y le
fue haciendo hábitos exactos. Cuando Remigio tenía cinco meses
se despertaba a las seis y media en punto de la mañana; Verónica
alegaba que era una hora ideal para cualquier ocupación de la
vida. Al escuchar Rodrigo el chillido matutino, imprescindible
como el canto de un gallo, la veía saltar del lecho y desaparecer
con el niño en la pieza vecina; en seguida se escuchaba, en la
quietud del ambiente, el chupetear de la boquita. Rodrigo permanecía
amodorrado una media hora más; echaba a un lado las mantas haciendo
un esfuerzo y se iba a la ducha. Con los años, cuando las cosas
fueron mejor, se levantaba más tarde.

Verónica ponía al chico al sol en la mañana, lo bañaba dos veces
al día y lo sacaba a dormir al jardín. Fuera de eso procuraba
que la ropa de Rodrigo estuviese impecablemente recosida, la
comida bien sazonada y equilibrar el presupuesto de la casa.

Para combatir las tendencias de Rodrigo a la inactividad del
cuerpo, Verónica lo levantaba algunas mañanas y lo invitaba a
caminar. Trepaban por la calle real y en ocasiones daban vuelta
por el Caracol, bordeando el cerro de la Bola para entrar de
regreso por la parte baja de San Miguel. En algunos de estos
paseos Verónica llevaba una tortilla de huevos con patatas, panes
recién salidos del horno, duraznos priscos y una botella termos
con café caliente. Sentados en alguna ladera engullían ese desayuno.
Hablaban entonces de sus problemas presentes y futuros y hacían
planes para la vida de Remigio. Rodrigo tuvo durante mucho tiempo
el proyecto de trasladarse a México; como tenía su legítima vanidad
de buen abogado que conoce las leyes y sabe manejar los códigos,
pensaba por una parte que en la metrópoli haría buen papel y
podría sacudirse la monotonía de la provincia. Además deseaba
ese cambio por Verónica; estaba seguro de ganar lo necesario
para mantener su casa con decoro y dar a su mujer una vida con
mayores satisfacciones.

Pero esta lo disuadió siempre; lo desengañaba del espejismo de
las urbes donde la lucha es dura porque se emprende con armas
desiguales; donde es difícil guardar intacta la integridad y
el medro impone el trato de gentes indignas. Rodrigo insistió
muchas veces alegando la educación de Remigio, pero ella invocaba
razones sólidas para demostrar que podía educarse en el pueblo
y si se veía acorralada decía que por lo pronto el niño se desarrollaba
muy bien y que durante diez años no tendrían que preocuparse
más que por el sol, el buen aire, el silabario y el catecismo,
todo lo cual lo tenían a la mano.

Verónica era inteligente y como la quietud le daba muchas oportunidades
procuró mejorar su instrucción, que era muy incompleta. Rodrigo,
con su bonhomía habitual, se burlaba amorosamente de ella cuando
la encontraba leyendo el libro de Mme. Calderón de la Barca o
alguna otra cosa de ese estilo. Pero en el fondo le halagaba
mucho este esfuerzo de su mujer y lo fomentaba por cuantos medios
tenía a su alcance. Así adquirió Verónica un bagaje suficiente
para ocuparse de los hijos en sus primeros años y enseñarlos
a leer. Con su carácter tranquilo, que hacía el efecto de un
sedante sobre quienes la rodeaban, los esfuerzos cotidianos de
la existencia eran para ella un juego. Por eso las lecciones
de lectura la distraían y distraían al chiquillo; la forma de
las letras le sugería movimientos y muecas; las sílabas se juntaban
en onomatopeyas llenas de comicidad. Un buen día, después de
tres o cuatro meses de esa tarea, Remigio se soltó deletreando
todos los rótulos de la calle. Le llamaba la atención uno sobre
la casa del Dr. Bernáldez: Médico, Cirujano y Partero. Rodrigo
solía preguntarle: ---A ver, hijito ¿tú qué vas a ser? ----Cirujano
y partero ---contestaba Remigio cuando todavía no cumplía cinco
años; y pronunciaba las erres, pues todos los chicos de Rodrigo
tuvieron magnifica dicción desde muy pequeños. Por una coincidencia,
de hombre ha llegado a ser un excelente cirujano.

Pero además de leer, Remigio aprendió también a compenetrarse
con ella. En su cabecita infantil incrustóse la idea de que Verónica
era la mejor de las criaturas; sería muy fácil decir que la veía
como una camarada o una hermana al mismo tiempo que una madre;
pero esta explicación no correspondería exactamente a la esencia
de su afecto. Verónica era un compendio de las cosas que despiertan
el amor; en su almita de niño le prestaba un carácter casi divino.
Para él todas las imágenes de la Virgen que había en la casa
tenían algo de su madre, pero con una divinidad tangible; con
unos labios carnosos que sabían besar y unas manos tan sutiles
y suaves que curaban con su contacto el dolor de los magullones.
Al mismo tiempo era un ser que reía, sabía jugar y conocía un
sinnúmero de maneras maravillosas de tenerlo contento. A esta
adoración contribuía Rodrigo que hablaba con él de Verónica como
si fuese un ángel protector de ambos. Cuando le contaba el cuento
de la princesa Jimijíjimijá, añadía que era tan bonita como su
madre.

Verónica, a su vez, sentía que allí tenía un corazón y una cabecita
que podía modelar a su antojo. Le preocupaba aquel chiquillo
que desde su edad más tierna ponía en juego su iniciativa de
acción; recordaba que, a los siete u ocho meses, cuando apenas
gateaba, se perdió una mañana y a la postre encontrólo en un
ángulo del corredor, pegado a una teta de Venus, la perra, que
criaba a la sazón a tres cachorros. Después se convirtió en un
chiquillo turbulento que tenía salidas desconcertantes. Verónica
las refería orgullosa, aunque nunca dejaba de agregar que el
amor ciega el entendimiento de las madres, lo que la hacía juzgar
a Remigio como un niño precoz.

Cuando pasaron los tiempos difíciles de la Revolución, la vida
se organizó en esa casa con un ritmo tranquilo. Todo estaba limpio;
los muebles relucían, los pisos de los corredores que eran de
piedra pulimentada, reflejaban vagamente las columnas del patio.
El escritorio de Rodrigo era un modelo de orden porque Verónica
arreglaba por sí misma los libros, ponía en su sitio los papeles
y llenaba los tinteros; la cocina parecía un laboratorio, donde
los frascos tenían sus letreros y cada cazuela su uso especial;
había una grande para los moles, otra para meterla al horno con
los asados y una sartén destinada a las tortillas de huevo.

Verónica, después de estar en la cocina, iba a su escritorio,
junto a una ventana por donde entraba el sol. Allí ponía en orden
la contabilidad de la casa, escribía algunas cartas y también
notas en una libreta acerca de la educación de sus hijos. Por
desgracia esas notas se perdieron.

Mientras tanto fue necesario enviar a Remigio a la escuela de
las monjas. Desde el primer día sintióse a sus anchas; encontró
allí algunos amigos con los cuales jugaba en las casas del pueblo.
El llegaba con una ventaja: leía de corrido mientras los otros
apenas conocían las letras; sumaba y restaba cantidades grandes;
sabía las tablas de multiplicar y efectuaba algunas divisiones
sencillas. Distinguía todos los colores y aplicaba sus nombres
correctos a las partes de una flor; sabía cuando se podían coger
sin peligro las abejas del panal que había en el huerto y conocía
a fondo la vida y costumbres de los gatos y de la perra de la
casa.

Además era fuerte y trepaba a las ramas más bajas de los árboles.
Nadaba bien, sobre todo cuando se sentía animado por la presencia
de Rodrigo, que lo llevaba a las albercas de los baños del Chorro.
Sabía también ponerse de coronilla y hacer algunas cabriolas
complicadas.

Pero antes de que creciera Patricio, ya su mejor compañera en
la casa era Venus, la perra. Cuando estaba muy pequeño y lo sentaban
en el jardín, a tomar el sol, en el centro de un sarape y rodeado
de su parafernalia infantil: ---carretes sin hilo, huesos de
mamey, una gran pelota y un muñeco de hule--- la Venus se acercaba
hasta colocarse al alcance de sus bracitos que hacían movimientos
desacordes. Remigio le rasguñaba el hocico y le metía las manitas
en las orejas colgantes o agarraba con sus deditos los carnosos
belfos.

La Venus, que era una gran perra cruzada de mastín, de cabeza
enorme, lo miraba con sus ojos dulces y azotaba suavemente con
su rabo el sarape extendido. Fue entonces cuando tuvo una cría
y tomando a sus cachorritos con el hocico los llevaba, hoy uno
y mañana otro, hasta donde estaba Remigio, seguramente con el
propósito de estrechar una amistad que era ya muy íntima.

Nació Patricio y revivieron los cuidados al diminuto ser que
chupaba ávidamente los pechos henchidos. Renacieron los comentarios
cada vez que, con un gesto o una palabra, daba un paso para introducirse
en la vida de la familia. Empezó a hablar más tarde y era mentalmente
menos precoz, pero se crió fuerte y a los once meses comenzó
a incorporarse sobre las dos piernitas agarrándose de los muebles.

Rodrigo seguía trabajando; a toda costa trataba de construir
el bienestar para los suyos. Comenzó a hacerse una reputación
de abogado penalista y salía con frecuencia a los pueblos cercanos
para ver a la clientela: a San José Iturbide, a San Luis de la
Paz, a Dolores Hidalgo. En una ocasión que iba a Monterrey decidió
llevar consigo a Remigio; ese contacto estrechaba la camaradería
que según él debía ser la base de las relaciones con sus hijos.
El chiquillo sentía la solemnidad del acontecimiento: subir a
uno de esos trenes que él había visto pasar jadeantes por la
estación de San Miguel. Y después dormir allí encima de esos
vagones inmensos. Rodrigo lo aleccionó: ---Hijito, vas a dejar
unos días a tu mamá; es preciso decirle que estás triste, que
no quisieras separarte de ella; dile que tienes que acompañar
a tu padre a sus negocios.

El chico tenía la intuición de los discursos fáciles. Ya en el
estribo del tren abrazaba a Verónica y le hacía demostraciones
de su pesar por dejarla, hasta que ella llevada por su espíritu
de broma tan cordial, le dice: ---Hijito, mejor quédate; no quiero
que te vayas tan triste. Tu papá puede marcharse solo---. Remigio
la contempló azorado; ciertamente estaba triste, pero prefería
ir con Rodrigo. Ahora sus aspavientos lo echaban todo a perder.
Y como Verónica insistía diciéndole: ---Bájate, hijito; quédate
entonces conmigo--- Remigio le contestó mientras los pucheros
le temblaban en la barbita: ---Sí, estoy triste mamacita; pero
no es para tanto.

Esa salida se transmitía después en las charlas familiares; eran
las cosas que ella escribía en aquella libreta de notas ahora
extraviada.

Rodrigo y Verónica estaban confundidos en el mismo ideal; un
ideal sin complicaciones. Querían educar a sus hijos y ser buenos
cristianos; cumplían sin esfuerzo los mandamientos de Dios y
otros que les hacían la vida fácil. Eran buenos católicos, pero
eran tolerantes; creían en la bondad de gentes que no iban a
misa, ni cumplían con la Iglesia; trataban a todo el mundo con
igual llaneza sin sentirse superiores a nadie. A medida que Rodrigo
se iba acercando a la plenitud de la vida, y después en el dintel
de la vejez ---cuando lo sorprendió la muerte--- solía decir
que la única preeminencia que respetaba era la de la bondad.
Para él los hombres se dividían en dos grandes clases: los buenos
y los malos: él procuraba estar con los primeros, al lado de
Verónica y aspiraba a que sus hijos estuvieran también allí.
Lo demás, se decía, nos será dado por añadidura. Era su cita
predilecta de los evangelios.

Verónica educó a todos sus hijos dentro de la Iglesia, como buenos
católicos. Cuando muy pequeños los enseñaba a orar y a pedir
únicamente por las cosas espirituales. Para satisfacer las necesidades
del cuerpo nos basta con rezar el Padre Nuestro ---solía decir---.
Rodrigo estaba de acuerdo con ella y nunca se hacían oraciones
para mejorar los negocios ni remediar los aprietos. Verónica
decía con frecuencia que ella deseaba para los suyos que sus
faltas las pagaran en esta vida, con objeto de que llegasen puros
a la presencia del Señor. Cuando Remigio hizo su primera comunión
en la parroquia fue una fiesta para todo el pueblo, porque cien
chiquillos comulgaron al mismo tiempo. A Verónica le pareció
muy bien ese contacto popular. Juzgaba sinceramente que todos
eran iguales ante Dios y quería hacer sentir a su hijo que por
la gracia de los sacramentos se borran las diferencias humanas.

Cuando Patricio se acercaba a los cuatro años, Verónica se aplicó
a enseñarlo a leer, como lo había hecho con el mayor. Pero aquel
era menos despierto y las nociones penetraban con más dificultad
en aquella cabecita dura; sin embargo, una vez adquiridas, quedaban
allí para siempre. Verónica tenía sus ideas y cuando charlaba
con Rodrigo alegaba que Patricio era el más inteligente. Su cuerpecito
era una maravilla de armonía; iba adquiriendo una robustez extraordinaria
y aun cuando era menor que su hermano, al crecer lo iba igualando
rápidamente en todo lo que era destreza y agilidad.

La casa se completó al llegar Ana. Con el tiempo comenzó a ser
un fenómeno nuevo la tiranía de esa criaturita que cuando apenas
sabía andar trastornaba ya el buen orden de las cosas. Chole
se sentía rejuvenecida; revivían aquellos años cuando Verónica
era pequeña; ahora se doblegaba con un amor incondicional ante
la voluntad de Ana que hurtaba del costurero de su madre las
bolas de estambre, para jugar con ellas, o entraba a saco en
el gran baúl donde sus hermanos guardaban sus juguetes. Pero
Verónica era demasiado inteligente para dejarse arrastrar por
su pasión maternal y logró doblegar con dulzura esa almita rebelde.
Ana supo muy pronto que el peor castigo a sus faltas era sentirse
temporalmente excluída de los privilegios del pacto familiar.
A los cuatro años sabía también ya que en esa casa existía una
ley de respeto mutuo que era indispensable acatar, y que cuando
se violaba se perdía el amor de quienes formaban esa cofradía
de cinco seres unidos para siempre por una voluntad que estaba
por encima de todo.

La vida en el país comenzaba a normalizarse y aunque se resentía
de los tiempos revueltos que acababan de transcurrir era tal
el ansia de trabajo que las gentes, alucinadas con cualquier
promesa de los que manejaban la cosa pública, se entregaban a
sus labores. Todavía entonces se juzgaba que la corrupción era
transitoria y el resultado inevitable de una época sangrienta;
pero se creía que con la paz los detritus serían al fin eliminados.
Nadie hubiese pensado que esas sustancias fétidas pudieran incorporarse
a las corrientes vitales hasta trastocar los valores, que era
lo que pasaba cuando vino la invasión.

El hecho fue que ese espejismo de prosperidad trajo el bienestar
a Rodrigo. Hubo negocios en su notaría; se escrituraban transacciones
de importancia; las minas reanudaban sus trabajos y era preciso
dar fe de contratos cuantiosos. Con las transacciones vinieron
las controversias y con ellas los honorarios; a veces se encontraba
con el milagro de que tenía quinientos o mil pesos en el cajón
de su escritorio. Entonces podía acercarse cualquiera con la
certeza de que era fácil despojarlo de ellos si llegaba con una
historia de un niño enfermo o de una esposa cardíaca que necesitaba
bajar al mar. Pero si esas ocasiones de dispendio no se presentaban,
él buscaba otras escribiendo a México por cosas para los chicos
o Verónica. En ocasiones esta última, que sabía lo que pasaba
al ver llegar los paquetes, acudía a tiempo para salvar del naufragio
unos cientos de pesos. Se acercaba a él y Rodrigo bajaba los
ojos sobre los papeles de su mesa, como un muchacho cogido en
falta; ella se sentaba a su lado y le echaba el brazo sobre el
cuello.

---Hijo mío, ¿cuándo vas a tener juicio? ¿No crees que gastando
así el dinero se tienta a la Providencia? Comprarme de una vez
seis pares de zapatos es un despilfarro; pero, en fin, tarde
o temprano tendré que gastarlos, así se compren de uno en uno.
Lo que es una locura es ese perfume, ¿cuánto gastaste en él?
No me respondes; claro, te remuerde la conciencia. ¿Y esa muñeca
para Ana? La vas a echar a perder con esos mimos. ¡Una muñeca
que trae un baúl con una docena de vestidos, como una recién
casada! Lo que compraste para los chicos no tiene igual, ¡relojes!
¿Qué no sabes la edad que tienen? Rodrigo mío, ¿es que no tienes
remedio? ¿Y lo de Chole? Mira que traerle una pulsera; lo que
necesita es un rebozo.

Rodrigo levantaba al fin la vista de sus legajos y se quedaba
contemplando a Verónica; quería aparecer serio y hasta un poco
malhumorado.

---Hija de mi vida, ¿es que no soy ya bastante crecido para hacer
mi voluntad? Yo sé que tú estás bien oliendo a jabón; pero ¿por
qué no he de halagarte a mi antojo? Además ¿cuándo voy a poder
hacer algo insensato sin que me hagas un llamado a la cordura?
Hijita mía, es mi obligación proveer para lo preciso, en cambio
es un solaz comprarles lo superfluo; déjame, cuando pueda, seguir
mi fantasía.

---Rodrigo, Rodrigo, eres incorregible---. El la atraía hacia
sí y la besaba en la boca; ella sentía los latidos agitados de
su corazón y cerraba suavemente los ojos.

En aquella familia cuyos miembros crecían armoniosamente como
un árbol frondoso, la grave enfermedad de Patricio, apenas de
siete años, alteró el ambiente de paz. Cuando el Dr. Bernáldez
hizo el terrible diagnóstico y anunció que se trataba de un caso
muy serio de escarlatina, Remigio y Ana fueron enviados a la
casa de las hermanas de Don Estanislao Gutiérrez para evitar
el contagio. En tres o cuatro días el niño se puso a la muerte.
Verónica estaba triste, callada, sin dejar la cabecera del enfermito;
este quería a toda costa apretar entre las suyas la mano de su
madre y la obligaba a permanecer inmóvil. Cuando Verónica quería
desprenderse un poco para descansar y trataba de abrir las manitas
crispadas, se escuchaba una queja leve; tal parecía que esa mano,
con su contacto, era el áncora que lo anclaba a la vida. Verónica
estaba allí horas enteras, con el rostro contraído por la pena,
la mirada perdida e impasible; no rezaba, había puesto ya la
vida de su hijo en el regazo de Aquel que distribuye según sus
designios, la alegría y la angustia. Sabía que aquella muerte
no la volvería rebelde, ni tampoco iba a flaquear su fe en la
voluntad providente que hasta entonces los había protegido; sabía
que iba a ser una madre resignada; pero sufría, sufría hondamente.
Eso estaba fuera de su dominio sobre sí misma, de la firmeza
de sus creencias, de su confianza en Dios. No lloraría; no lloraría
cuando llevaran al panteón la cajita blanca; no lloraría ni cuando
regresaran Remigio y Ana y le pidieran cuentas, y ella tuviera
que confesarles que se lo habían arrebatado. Podía también evitar
las lágrimas y mostrarse estoica; pero no evadir el sufrimiento.
Sufría, sufría hondamente mientras esos deditos contraíanse en
los suyos y escuchaba la respiración jadeante. Allí, en el cuarto
penumbroso, junto a la camita de Patricio, la misteriosa química
del dolor trasmutó en hilos de plata muchos de sus cabellos.

Pero aquel cuerpecito exangüe se aferraba con energía a la vida;
sin saber si en ella le esperaba el infortunio o la ventura,
anhelaba sentir de nuevo el sol caliente y contemplar las rosas
del jardín, comer los duraznos jugosos y volver a hundirse en
aquel pecho en donde había libado en otro tiempo un néctar incomparable.
Por su cabecita pasaban imágenes extrañas, flotaban en el aire
y se acercaban a él apretándole el cuello y obligándolo a abrir
la boquita para respirar mejor. Pero no estaba solo; estaba allí
Verónica, estaba allí su mano, con sus dedos que sabían prodigar
caricias. Sentía un contacto fresco sobre su frente y de cuando
en cuando entre las fantasmagorías de la fiebre, veía a Rodrigo
que se inclinaba hasta él.

Si hubiese tenido fuerzas se habría abrazado a su cuello para
sentirse más seguro.

Rodrigo envejeció; estaba ojeroso, demacrado, sucio. Se sentaba
al lado de Verónica durante esas horas interminables; a veces
atraía hacia sí a su mujer y la besaba en los ojos resecos. De
improviso se levantaba y se iba al huerto, por cuyas calzadas
caminaba sin tregua; él también sufría, pero además tenía miedo
por Verónica. La muerte del hijo iba a dejarla con el espíritu
lacrado para siempre, inválido por la amputación de uno de sus
pedazos. Cuando llegaba el Doctor Bernáldez, Rodrigo quería adivinar
en su rostro una esperanza o una sentencia y al verlo salir de
la pieza del enfermo alcanzábalo ansioso: ---Doctor, dígame la
verdad. Cuando esto no tenga remedio, dígamelo. Quiero preparar
a Verónica. ¿Cómo lo encuentra?---. ---Grave, Rodrigo; muy grave.
Pero tal vez salgamos adelante.

Una mañana Patricio sintió que esos cocos que rondaban alrededor
de su almohada perdían su forma y que él iba cayendo en un vacío
insondable; él mismo se fundió en una nada donde no existía el
sufrimiento. Entonces comenzó a dormir; cuando la luz del día
entró a esa estancia, Patricio respiraba profundamente y sus
manitas se habían aflojado sobre la de Verónica.

Esta y su marido recordaban algunos meses más tarde aquellos
días angustiosos. Siento, hijo mío ---decía Verónica--- que la
dura prueba pasó ya; era preciso sentir de cerca que uno de nosotros
podía abandonarnos para cuidar mejor nuestra felicidad y aferrarnos
a ella.

Esperemos que sea así ---contestaba Rodrigo---. Creo que vamos
sacando adelante esta prole. Remigio me da la impresión de ser
más libre; parece como que no toma las cosas en serio. Patricio
es más hondo, más persistente en el esfuerzo; Ana es una chiquilla
voluntariosa pero tú la has domeñado con tu cariño. Juzgo que
ahora como buenos jardineros no nos queda más que regar y ver
crecer a nuestros árboles.

He aquí esta familia; he aquí esta casa. He pretendido revivirlos
con su calor humano. Un hombre y una mujer, buenos, que nunca
hicieron mal a nadie, unidos por un amor tierno que funde las
vidas. Nacieron y crecieron en este pueblo y mientras más transcurren
los años más se identifican con el paisaje, con los retablos
de las iglesias, con los nichos que, en las fachadas de las casas,
alojaban las santas figuras del viacrucis. En las noches, cuando
recorro las calles, creo que los voy a encontrar; murieron tan
jóvenes y tan llenos de vida que todavía se escucha el eco de
sus palabras. Las gentes me dicen lo que hablaban, cómo vestían
y cómo se prodigaban a todos en una ansia de caridad de buena
ley. Todavía me parece ver a sus hijos, llenos de salud, ir al
colegio con los libros a la espalda; a Ana correr por el parque
detrás de su aro. Contemplo a Chole que cose, con la cabeza llena
de canas, y que después de cada puntada alza la vista vigilante
cuidando a esos chiquillos, que son como suyos. Y veo también
a Venus, la perra que pasó la enfermedad de Patricio sin querer
abandonar ni un solo momento esa camita donde acechaba la muerte.
La veo cuando murió a su vez y se la enterró en el jardín. Parecía
que había muerto una persona de la casa; en realidad Verónica,
aunque muy ortodoxa en sus creencias, consideraba a la Venus
como un miembro de la cristiana comunidad de la familia. Los
niños asistieron a esos funerales con los ojos llorosos. Se envolvió
el cuerpo de pelo lustroso en una sábana limpia y Verónica y
Rodrigo estuvieron presentes hasta que la última palada de tierra
cubrió la fosa.

Pero mi relato es solo una sucesión deshilvanada, sin esa corriente
profunda de ternura que podría hacer que estos seres se levantasen
de estas páginas para vernos con ojos gozosos o tristes. Y sin
embargo, me es preciso continuar.

Corrieron los años y la vida siguió su curso. Cada región de
la república soportaba a sus gobernantes; cada ciudad o pueblo
a sus presidentes municipales analfabetos. Pero este ha sido
un país resuelto a vivir. Después de cada disparate se ha levantado
siempre maltrecho, pero con un vigor renovado. Unos presidentes
nos hicieron aparecer ateos, come-curas y materialistas; entonces
hubo varios años de lucha estériles, en los cuales se derramó
mucha sangre; sucedía que esas campañas enriquecían a los generales
y se prolongaban sin piedad. Otros presidentes simularon ansias
comunistas en el corazón del pueblo, siendo así que las gentes
han suspirado únicamente por el pedazo de tierra suyo, al que
ponerle una cerca de piedras para abarcarlo con un gesto amplio
de los brazos y decir a los cuatro vientos: esta tierra es mía.
Otros pretendieron inculcar en los cerebros de los niños un concepto
racional y exacto del universo, anticipándose así a la sabiduría
de milenios. Se editaron textos repletos de un socialismo trasnochado
que ahora, apenas a unos años de distancia, nos parecen la obra
de un insensato. {.espacio-arriba1}

Pero el país quería vivir, o mejor dicho, estaba resuelto a sobrevivir.
Resistió el saqueo de funcionarios venales, las inepcias de los
que a toda costa se hacían aparecer como revolucionarios para
cimentar en ese título su medro. El cohecho, para vergüenza
de México, se convirtió en una institución nacional; la verba
del pueblo lo marcó con un nombre sarcástico que no menciono
por pudor. Así fue México arrastrando los grilletes de sus gobiernos;
pero existían virtudes tan hondas en el corazón de sus hombres
y el suelo era tan rico y el sol tan tibio que, por gracia de
Dios, pudimos preservar nuestro decoro y nuestra existencia.

Y encontramos a Rodrigo Guerrero, allá por los treintas, hace
apenas veintitantos años, en el mismo San Miguel Allende. Verónica
había llegado a una hermosa madurez; era más que nunca la ancha
roca del hogar sobre el cual se asentaba ese edificio de armonía
y dicha que parecía destinado a durar para siempre, al mismo
tiempo que con el calor de sus brazos los cobijaba a todos, como
cuando eran pequeños. Remigio terminaba su preparatoria y había
comenzado su carrera de cirujano; Patricio estudiaba leyes. Ana
estudiaba también, bajo el cuidado solícito de su madre, que
antes que nada quería hacer de ella una mujer. Y Rodrigo continuaba
soportando con alegría ese peso que el cielo cargó sobre sus
hombros. No había acumulado riquezas, en parte porque ejercía
su profesión en un medio modesto; pero sobre todo porque no era
inclinado a la avaricia. Pensaba que si Remigio se hacía médico
y Patricio notario, la vida les sería fácil, como lo era para
él. Hizo algunos ahorros que Verónica guardaba, en previsión
de algún día sombrío; pero pensaba que los problemas más graves
en la vida no consisten en el dinero, sino en esas cosas intangibles
que se albergan en la mente y en el corazón.

El año de 1936, la familia se había reunido para pasar las Navidades.
Como San Miguel ha sido siempre un pueblo acogedor, las gentes
de la capital comenzaban a ver en él un lugar de paz; iban con
frecuencia escritores, arquitectos, músicos o simplemente hombres
de trabajo que encontraban un sedante en la soledad de sus calles.
La peste del turismo no se conocía y la ciudad pudo, como hasta
ahora, conservar su recato urbano. Por eso Maclovio Otamendi
y Alonso Quintanar estaban allí en aquella ocasión. Verónica
nunca les permitía alojarse en los hoteles: Guadalupe, la hija
de Maclovio, se arreglaba en el cuarto de Ana; Armando Otamendi
con Remigio, y Maclovio dormía en un gran sofá, en la sala. Quintanar
tenía su cuarto en la azotea, suficientemente amplio para contener
una cama, un armario y una mesa grande de madera blanca, que
él habilitaba de tocador.

En la estancia de la casa charlaban los cinco muchachos después
de comer. ---Ana ---dice Remigio--- ¿por qué no has invitado
a tu enamorado? Podía haber venido para la Nochebuena.

Patricio contestó por ella: ---Nos tiene miedo, Remigio; la Saeta,
para hacerse interesante, le cuenta que somos dos hermanos celosos.

---No seas idiota; no viene porque en las vacaciones trabaja
en la fábrica con su padre, en León. Además, aunque tú presumas
de deportista no quisiera verte en sus manos. Y finalmente, no
es mi enamorado; es mi amigo y eso basta.

Guadalupe intervino: ---Patricio, por qué te empeñas en llamarle
a Ana la Saeta, cuando tiene un nombre tan bonito y tan corto.
Ana… suena muy bien. La Saeta me da la idea de un título de periódico.
Además se disgusta.

---No; no me enfado ---dijo Ana---. Cuando este tonto comenzó
a decirme Saeta me enfurecía tanto que le habría sacado los ojos;
pero después me acostumbré. A veces le digo alguna majadería
porque quiero mantener viva mi protesta.

Patricio se acercó a Ana y la abrazó por detrás besándola en
los cabellos. ---¿No te gusta que le diga Saeta, Guadalupe? Pues
es un nombre bien aplicado porque Ana, hace algunos años, era
flaca como un otate; y además tenía sus buenas cualidades y se
me ocurrió llamarla así. Debería sentirse orgullosa.

---Si -dice Guadalupe---, ya sé que la llamabas Saeta por impetuosa.
El hecho es que no va a tener quien la lleve a la mesa la noche
de Navidad.

---Estás equivocada ---dijo Ana---, aquí tengo a Armando. Este,
que era un muchacho tímido, apenas de la edad de Ana, enrojeció
hasta la raíz de los cabellos.

La conversación se deslizaba en ese tono cuando entraron a la
sala Don Rodrigo, seguido de Quintanar, del doctor Bernáldez
y de Maclovio Otamendi, quien venía gesticulando. Los cinco muchachos
se vieron entre sí. ¿Nos quedamos? ---dijo Guadalupe---. Ya sabemos
que hablan de política o de la paz de la tierra.

Maclovio escuchó la murmuración porque interpeló a su hija inmediatamente:
---¿Quieres irte? Vete, pero deberías sentir la curiosidad natural
por oír comentarios sobre las cosas que pasan en el mundo. La
cultura no está toda en los libros de Química. Algo aprenderás
si te quedas.

---Yo salgo en defensa de Guadalupe ---dijo Alonso---; Maclovio,
cualquiera diría que es usted un ogro.

---Usted lo ha dicho, Alonso ---replicó Guadalupe---. Es una
paloma con plumaje de gavilán. ¡Dígame si no lo conoceré! He
lidiado con él desde los cinco años, cuando murió mi madre.

---Consuélate, hija ---arguyó entonces Rodrigo---. Hay otras
palomas más temibles y conservan sus plumas blancas. La tía Casilda
tenía una.

---Vas a salir con alguno de tus chascarrillos ---dijo Maclovio---.
Si lo vas a contar que sea pronto y sin circunloquios. Así el
mal rato será más corto.

---Pues lo contaré sin circunloquios. La tía Casilda era una
viejita casi ciega; su último amor consistía en una paloma blanca
que se posaba en su hombro y le hacía rucutucutú junto a la oreja.
Cuando alguien llegaba a visitarla y la encontraba sola, sin
ese animalito símbolo de la paz, le preguntaba: ---Tía Casilda,
dónde está su palomita---. Y ella contestaba: ---Pobrecita, anda
cogiendo algún gavilancito para comer.

Los muchachos, sobre todo, rieron el chiste. Remigio observó:
---Ese cuento es nuevo; si no lo fuese ya te lo habríamos oído
una docena de veces. ¿Quién te lo contó?

---Vamos, muchachos; más respeto para su padre ---dijo el Doctor
Bernáldez--- interviniendo. Y usted, Maclovio, continúe su perorata.

---Yo decía únicamente ---dijo entonces Maclovio---, que Rodrigo
habla de memoria. ¡Mire usted que venirnos ahora con que Hitler
es un defensor de la religión!

---Pues no atestiguo con muertos ---contestó Rodrigo---. Lo dice
«Mi Lucha». Y conste que la estoy leyendo por encargo especial
de Alonso, quien me regaló este ejemplar---. Al mismo tiempo
agitaba parsimoniosamente un volumen. Continuó diciendo: ---Aquí
tengo marcados dos pasajes y voy a leérselos con satisfacción:
---«El que arranca la religión o la fe a un pueblo, le abre las
puertas al veneno devorador del bolchevismo»---. Y aquí hay otra
frase… Bueno, no la encuentro, pero Hitler apostrofa a los que
quieren abolir la fe religiosa del pueblo llamándolos criminales
y locos. ¿Qué me dice usted a eso?

---Te digo sencillamente que Hitler es un maestro del engaño
---contestó Maclovio---. Lee también en ese libraco una apología
de la mentira; si mal no recuerdo afirma que una mentira impresionante,
a fuerza de repetirse, acaba por ser aceptada como verdad irrefutable.
Si me lo prestas podré señalarte los párrafos que prueban mi
aserto y también docenas de tiradas contra el cristianismo.

Tomó entonces el libro de las manos de Rodrigo y comenzó a hojearlo.
A los pocos instantes se dirigió a sus amigos: ---Aquí tienen
una muestra, y para muestra basta un botón. Comenzó a leer: «El
cristianismo, por una intolerancia fanática, echó por tierra
los altares del paganismo».

Sin embargo ---argumentó Rodrigo--- no cabe duda que ese hombre
está transformando Alemania. Por el trabajo, el orden y la disciplina
está haciendo milagros. Lo que aquí en México necesitamos es
un Hitler.

---Jamás ---dijo Alonso exaltado contra su costumbre---. No sabe
usted lo que está diciendo---. En seguida se calmó. ---Perdóneme
Rodrigo, no pude contenerme. Pero usted tiene un concepto erróneo
de la mística alemana. La reduce a un humilde evangelio de trabajo,
de orden y de disciplina, para usar sus palabras. Estas virtudes
son el instrumento, pero otras las finalidades. Lea algo más
_Mein Kampf_ que para los nazis es como la revelación; creen
en ese libro, Rodrigo, del mismo modo que usted cree en el Nuevo
Testamento; ni más ni menos. Allí se habla de algo así como de
que «no se podrían concebir los grandes cambios en el mundo,
si se hubiesen apoyado en la virtud burguesa del orden, en lugar
de brotar de pasiones fanáticas y hasta histéricas». ---Quizás
estas frases no sean exactamente las mismas, pues hace ya meses
que leí la obra, pero le respondo de su significado esencial.
Allí el orden que usted preconiza es tratado despectivamente
por Hitler como una virtud burguesa.

---Todo esto son palabras y palabras ---dijo Rodrigo---. Yo me
atengo a los hechos. Hay que hablar con la gente que viene de
Alemania. Entre otras cosas ha acabado con la plaga judía. ¿Usted,
Maclovio, defiende a los judíos?

Maclovio se quedó un momento silencioso. ---En eso, Rodrigo,
quizás tengas razón. Yo, como buen católico, estoy en primer
lugar contra los masones y en consecuencia contra los judíos,
pues la masonería es, en resumen, una obra de judíos. El comunismo
es también un engendro de judíos, y el comunismo es la amenaza
de nuestra religión.

---¿Me permiten hablar? ---preguntó Alonso---. Pues al escucharlos
diría que escucho a Hitler. Los voy a ofender a ustedes con un
símil: me parecen dos fariseos que discuten primeramente sobre
las jerarquías de los ángeles o la luz del Talmud y que después
se ponen de acuerdo para clavar en la cruz al Salvador, que era
judío. Yo no soy católico y por lo mismo no estoy muy versado
en la casuística católica; pero me parece que si hacemos una
consulta al Santo Padre nos va a decir que los judíos son nuestros
hermanos en Nuestro Señor Jesucristo.

Ahora ténganme paciencia. Voy a esgrimir el sentido común. Ustedes,
muchachos, si no están aburridos, pongan atención: ---Detesto
a los judíos tanto como el que más, cuando no contribuyen al
bienestar de sus semejantes, cuando en lugar de hacer con sus
manos o pensar con sus cerebros algo que coadyuve a la felicidad
común, acumulan dinero y hacen de este un instrumento de opresión
y poder. Detesto a los judíos que introdujeron el dinero prestado
a rédito, contra los preceptos de la Santa Iglesia Católica,
la cual consideraba este medro, en otros tiempos, como un grave
pecado. Estoy contra el judío que vende y compra, que presta
y recibe, que toma con la mano derecha para entregar con la izquierda
y que en cada una de esas maniobras trafica con el sudor y la
sangre de sus prójimos. Entrando más a fondo en la cuestión,
diré también que abomino de los judíos si conglomerados como
una raza elegida pretenden dominar al mundo con el arma del bolchevismo
o la de las finanzas internacionales. Estoy contra esos judíos.
Pero nunca haré causa contra el judío que trabaja, y que obtiene
de su esfuerzo una honesta retribución; tampoco estaré nunca
contra un Einstein. Si acabásemos con los sabios, los músicos,
los novelistas, los cómicos y los poetas judíos, daríamos un
golpe tremendo a la cultura humana. Cualquier país que ataque
a un hombre útil que cumple su destino, únicamente porque pertenece
a una cierta raza, se deshonra.

Ahora les cedo la palabra a los muchachos; ellos son más generosos
que nosotros los viejos y juzgan las cosas sin prejuicios.

Entonces todos tomaron parte en la discusión. Remigio daba sus
puntos de vista a favor de Alonso; Patricio alegaba que la conducta
de Hitler había que juzgarla por sus resultados. A los pocos
minutos todas las prácticas de un buen debate se habían perdido
y nadie se entendía. Por fin Rodrigo se levantó a sacar el dominó
para hacer una partida mientras decía:

---Alonso, admiro su estrategia. Nos hizo usted su pequeño discurso
y como se sintió satisfecho de él, decidió quitarnos la oportunidad
de replicarle. ¿Y qué hace usted? Desbozalar a sus aliados, esa
partida de chachalacas sin sesos. Ese no es juego limpio, Alonso---.
Y al mismo tiempo echó sobre la mesa las veintiocho fichas.

---

Los años siguieron transcurriendo. Los chirimoyos del huerto
sobrepasaron con mucho la altura de las azoteas; la higuera daba
una gran sombra y en verano se colocaba bajo ella la mesa para
el desayuno; el laurel de la india tenía un tronco enorme que
apenas se alcanzaba a abrazar. {.sangria}

Patricio terminó su carrera de abogado y se casó con Eligia Santiesteban;
fue un matrimonio hecho por amor que auguraba felicidad. Remigio
hacía su práctica de hospital y estaba próximo a recibirse. Ana
era novia de Carlos Andrade, su amigo de León. Rodrigo era un
poco más viejo, pero estaba sano y tenía trabajo; lo seguía buscando
su clientela ranchera y en la mesa había siempre huevos frescos,
pollos capones y, cuando era la temporada, frutas de la región.
Había ahorrado dinero y comprado la casa en que vivían. Seguía
discutiendo con Maclovio, con Nicanor Aceves, con el padre Videgaray
---que era ya cura de la parroquia--- con Alonso Quintanar. Rodrigo
tenía la pasión de la controversia.

Verónica tenía bastantes canas, pero se veía más hermosa. Su
cutis se conservaba fresco y sus ojos parecían más negros que
nunca. La monotonía de su existencia no la fatigaba. Como disponía
de mucho tiempo libre iba a las casas y ayudaba a los enfermos
y a los niños. Enseñaba a leer a los nietos de sus amigas lo
mismo que enseñó a sus hijos. Con estos entablaba largas conversaciones
y les hablaba del porvenir.

A veces, cuando estaban solos, Rodrigo y Verónica mencionaban
el fin que se acercaba. Se sentían todavía jóvenes, pero su labor
casi había terminado. Hablaban de que la muerte los sorprendería
en paz.

Sin embargo, no era lícito hablar de paz. Europa era un volcán.
En septiembre de 1938 Hitler se reunió en Múnich con Mussolini,
Deladier y Chamberlain y condenaron a muerte a Checoslovaquia,
la cual cayó en poder de Alemania.

En 1939 se declaró la guerra. Bélgica, Holanda, Noruega y Dinamarca
fueron invadidas; Polonia crucificada; Francia había sido derrotada
y en 1940 los alemanes entraron a París. Italia se alió a Alemania
y esta, rompiendo su alianza con Rusia la invadió en 1941. Grecia
fue destrozada. A fines de ese año Japón y los Estados Unidos
entraron a la lucha. En mayo de 1942 México declaró también que
nos encontrábamos en estado de guerra. La noche de ese día, el
28, Rodrigo, Verónica, Remigio y Ana cenaban en la casa de San
Miguel Allende. Estaban también Patricio y Eligia, su mujer,
la cual se encontraba embarazada y en espera de su criatura.
Además, Remigio recibiría más o menos al mismo tiempo, su título
de Médico. Estos hechos ponían en aquella casa un ambiente de
expectación feliz; se hacían planes, se discutían detalles: Ana
y Remigio serían los padrinos. Rodrigo proyectaba la comida:
encargaría los pavos y los cabritos por docenas. Praxedes metería
dos terneras en barbacoa; se invitaría a todo el mundo; de un
momento a otro llegarían varias barricas de vino. Como esperaban
el nacimiento en julio, harían el convite en agosto coincidiendo
con las fiestas del cuarto centenario de la fundación de San
Miguel.

Nadie habló de la guerra y de la participación de México; como
si ese fuera un suceso sin importancia. Todos suponían que la
vida seguiría su ritmo normal.

</section>
<section epub:type="chapter" role="doc-chapter">

# III

++A pesar++ de que era aquella una reunión de gente campechana,
sentíase que vivían un momento solemne. Maclovio levantó con
gravedad su copa y se dirigió a Remigio en un silencio familiar:
---Remigio, esto no es una fiesta, sino un rito. Hoy te armamos
caballero. Y hablo así en plural porque yo, y Alonso y el cura
y todos los que hemos comido aquí pan y sal a manteles, formamos
parte de tu lar. Te han tocado tiempos duros y nadie sabe lo
que el porvenir te depare, pero sé que serás un buen médico,
y también que serás un buen católico como lo es tu padre y lo
fue tu abuelo y tus antepasados por generaciones incontables.
Bueno, Remigio, hijo mío; cuando discuto con tu padre, que es
para mí otro hijo, soy elocuente y me fluyen las palabras; pero
ahora la emoción me embarga y no sé decirte más.

Todos quedaron silenciosos. Había un ambiente demasiado íntimo
a pesar de los treinta o cuarenta comensales, para que se oyese
un solo aplauso. Don Rodrigo dijo desde su asiento: ---Es cierto,
esto es un rito, y no sería para mí perfecto si dejase de escuchar
la voz de Alonso Quintanar.

---Ya lo esperaba ---dijo este último, pero no me había resuelto
a poner mis ideas en orden---. Se levantó a su vez alzando su
copa: En primer lugar, hijo mío ---dijo dirigiéndose a Remigio---
debemos dar gracias a tu madre por su guiso de naranjas agrias;
esto es parte del rito, como dice Maclovio, y yo lo he comido
con devoción. Pienso también como él que los tiempos son duros;
te diré con el poeta: ---«¿Ha nacido el apocalíptico anti-Cristo?---
Se han sabido presagios y prodigios se han visto ---y parece inminente
el retorno del Cristo». Pero no soy pesimista; sobre todo no
soy pesimista por ti, a cuyo alrededor nos hemos congregado.
Te incorporaste a las filas de una profesión que es hermosa porque
predispone al apostolado y porque te alejará del parasitismo,
ese cáncer de la vida de ahora. Tú formarás siempre con esa parte
de la humanidad que cumple con el precepto bíblico: ganarás el
pan con el sudor de tu frente. De ellos debe ser el mundo, sin
distinción de razas, ni de credos, ni de continentes. Y tú tendrás
tu parte. Amén.

Todos bebieron una buena sidra del país. Rodrigo dijo en voz
queda, pero con palabras que todos escucharon: ---Aun cuando
nunca voy de acuerdo con Alonso, si muero deseo que él sea el
padre de mis hijos. Siempre parece estar por encima de las querellas.

Esto era el final de una comida en San Miguel, a mediados de
agosto de 1942. Remigio había recibido su título de Médico y
Don Rodrigo Guerrero, juntó a sus amigos, es decir, a medio pueblo.
Además, había nacido el primer nieto, el hijo de Patricio. Maclovio
decía lo cierto; todos formaban parte de esa casa. Reinó una
alegría ruidosa, un poco primitiva; Martín Pérez, un montañés
cincuentón, dueño de la fábrica de hilados, hizo traer unas docenas
de botellas de un Valdepeñas viejo y Verónica había pasado dos
días en la cocina, con Juana y Dorotea, haciendo masas y adobando
carnes. Era la misma escena de los bautizos, de los matrimonios,
de las navidades, de aquellas cosas que, en esa familia, apretaban
los lazos del cuerpo y del espíritu. Cuando esto acaecía las
comilitonas se idealizaban y esas gentes claras, al gustar los
manjares aromados y al escanciar en abundancia el buen vino ---cuando
sus vapores aligeraban la imaginación y hacían ingrávida la carne---
abrían las compuertas de sus buenas pasiones y las derramaban
sin tasa.

---En realidad ---dijo Alonso--- no merecemos este festín. No sé
qué hemos hecho para ser tan felices. ---Este comentario brotaba
de sus labios más tarde, cuando los amigos de siempre se habían
quedado rumiando la sobremesa; era un domingo y todos se sentían
con derecho al descanso.

Don Rodrigo, el cura Joaquín, Martín Pérez y Maclovio Otamendi,
jugaban al dominó. Nicanor Aceves leía en un rincón. Alonso resolvía
crucigramas; Ana y su marido, con Patricio y Eligia, habían salido;
Verónica estaba en la huerta con las hermanas de Don Estanislao
Gutiérrez y en un sofá de la ancha sala, Remigio hablaba con
Guadalupe, la hija de Maclovio, la cual estudiaba química en
México. Remigio quería casarse, pero ella lo refrenaba: ---déjame
acabar mi carrera y además hay que esperar a que los tiempos
sean menos inquietos.

De pronto, Remigio y Guadalupe se levantaron, arrastraron dos
sillas y fueron a sentarse frente a Alonso. ---Alonso, aquí venimos
a charlar; abandone su crucigrama; esa manera de perder el tiempo
es indigna de usted ---dijo Guadalupe.

---Espera un momento, mujer ---replicó Alonso sin levantar los
ojos de su revista.

Después se quitó las gafas y dejó a un lado el crucigrama comentando:
---Bueno, ya continuaremos más tarde fumando este opio de los
tontos---. Nicanor Aceves acercó entonces su sillón hasta ellos
y guardó su libraco en un bolsillo.

---Oiga, Alonso ---comenzó Remigio--- usted sabe que tengo que
permanecer en Guanajuato este año y parte del entrante. Guadalupe
dice que después debo ir a México, pero preferiría marcharme
a los Estados Unidos; quisiera alistarme para trabajar en un
hospital de campaña; me fascina esa cirugía.

---En ese caso vete a los Estados Unidos. Siendo médico mexicano
no veo por qué no has de poder darte de alta allá; después de
todo somos enemigos del Eje y estarás en tu papel.

Don Rodrigo, que parecía concentrado en su juego, prestaba sin
embargo oído a todo lo que se hablaba, y exclamó: ---No te dejes
engatusar, hijo; además, la guerra terminará en la primavera
próxima y la ganará Hitler. Para nosotros es un juego del «quien
pierde gana», porque la ganaremos perdiéndola.

Remigio se sonrió. Alonso dijo entonces con voz más apagada:
---el caso de tu padre es extraordinariamente frecuente y yo,
que vivo en México, tropiezo con ellos todos los días. En sus
hogares son hombres modelos, como ciudadanos son intachables:
pagan sus contribuciones, respetan las leyes y se niegan heróicamente
a cohechar a la pestilente burocracia del Estado, aunque en ello
les vaya el pan cotidiano. Además, son gentes de trabajo y constituyen
celdillas sanas en el organismo social. Sin embargo, desean el
triunfo nazi; creen que los agravios que tenemos con los Estados
Unidos desde los tiempos de Poinset serán borrados para nuestro
provecho.

Ofuscados por su odio olvidan que, hoy por hoy, tenemos junto
con los yanquis, un patrimonio que defender.

A la sazón escuchóse en la mesa de dominó un ruido de fichas
y Maclovio se levantó hablando en voz alta y discutiendo con
Rodrigo: ---Nos cerraron el juego por torpeza tuya---. El cura
Videgaray dijo entonces: ---de cualquier manera yo tenía ya que
irme; tengo en la parroquia algunos quehaceres dominicales.

Rodrigo y Maclovio hicieron corro alrededor de Alonso y Martín
Pérez abandonó la estancia diciendo: ---Yo voy a fumar al jardín.

Rodrigo reanudó la eterna polémica: ---Ya lo escuché, Alonso,
haciendo sus discursos de propaganda; parece usted un líder comunista.

---Nunca he comulgado con los regímenes de opresión ---replicó
Alonso--- pero admiro al pueblo ruso por su heroicidad. Además,
el comunismo no debe inspirarnos tantos temores; se ha modificado
profundamente y está ya muy lejos de Lenin y más lejos aún de
Marx. Stakanof demostró que los rusos abandonaron la ortodoxia.
Los comunistas que se dicen ortodoxos viven fuera de Rusia. El
comunismo es una etiqueta de exportación; sirve o sirvió para
agitar en Francia, en España o en México. También para hacer
propaganda en los Estados Unidos; pero en realidad Rusia es la
versión moderna de lo que ha sido siempre: una tiranía Oriental.
Totalitaria porque totalitarios fueron los imperios de los zares.

---De cualquier modo, nunca estaré con ellos ---dijo Rodrigo---.
Usted mismo admite que viven bajo un despotismo que tratan de
extender al mundo entero. Además, ya conoce usted la frasecita:
la religión es un opio.

---Bueno, Rodrigo, me alegra oírlo hablar así. Son exactamente
las mismas razones para odiar a los nazis. Solo hay una diferencia:
los rusos tratan de propagar sus doctrinas fomentando gobiernos
de carácter bolchevique; así lo han hecho con nosotros; pero
contra ese veneno y contra nuestros comunistoides hemos sabido
defendernos, pues a pesar de los líderes y de los agitadores
de todas las categorías nos encontramos, por fortuna, muy lejos
del comunismo; en cambio los nazis hacen su propaganda con los
cañones de sus tanques, con la esclavitud de los países ocupados
y con los fusilamientos de rehenes.

---La verdad es que el mundo vive una era de confusión ---dijo
Nicanor Aceves---. Yo añoro los tiempos de Don Porfirio. ¡Todo
tan estable; los estratos sociales tan definidos; la vida tan
segura!

---¡Ah, no! ---protestó Alonso---. Es que esa época, la clásica
del capitalismo y de los noventas, pasó para no volver. El capitalismo
cumplió ya su misión, si es que tenía alguna; y para ser imparcial
hay que decir que sí la tuvo. Pero el capitalismo es una cosa
del pasado; y digo eso porque estamos en guerra y al terminar,
el mundo va a vivir una nueva era que no será la de Marx, pero
tampoco la de una plutocracia dominadora.

---Sí ---dijo Rodrigo--- viviremos el nuevo orden; Hitler lo está
preparando y yo creo que hacia allá apunta el porvenir.

---Pero Rodrigo, usted es el de siempre. Afortunadamente aquí
está Maclovio; él se encargará de llenarlo de denuestos.

---¡Usted también, Alonso! Pero vamos a ver: usted no es bolchevique;
es antinazi furibundo; está contra el capitalismo ¿cuál es pues,
su credo? ¿Acaso es usted nihilista?

---Mire, Rodrigo, tiene usted razón; mi credo, hasta la fecha,
es casi todo negativo. Si me constriñe usted a darle una respuesta
concreta le contestaré una perogrullada: mi credo es el de un
mundo justo; y si he de agregar algo más diría un mundo cristiano.
Y conste, Rodrigo, que digo cristiano, y no católico, porque
el catolicismo es intolerante por esencia, y el mundo que yo
deseo no puede ser intolerante.

---¡Alonso, no nos vaya usted ahora a resultar protestante! ---exclamó
Nicanor Aceves.

---Esa observación es una inepcia, querido Aceves ---replicó
Alonso Quintanar visiblemente molesto---. Es claro que si abogo
por los fueros de la tolerancia no puedo ser protestante. Le
diré algo más: ansío que la libertad del espíritu sea un hecho
después de esta guerra; pero, aunque parezca paradójico, anhelo
también que mi patria reafirme su unidad católica.

---Voy a terciar como la única mujer en este corrillo ---dijo
Guadalupe---. Yo creo que ---con perdón de ustedes--- todas estas
son palabras inútiles. Cuando llegue el momento que cada quien
actúe según sus convicciones.

---Hija mía, estás diciendo tonterías ---saltó Maclovio---. Si
tuvieras diez años menos les enseñaría tus nalgas a los que formamos
este corrillo, como tú nos llamas, y te daría unos manazos hasta
ponértelas coloradas. Esta plática o discusión y aún disputa,
si la quieres calificar así, es un fruto de cultura, es una controversia
de ideas; con ella se prepara esa acción que preconizas. Quizá
algún día recordemos estas conversaciones y nos daremos cuenta
de que, a pesar de todo, hemos tenido la fortuna de vivir en
un gran país libre.

---Ya tengo veintitantos años, padre, y tendría derecho a protestar
por lo que me has dicho; tal parece que soy una mocosa. Y haces
alardes falsos, porque nunca me has nalgueado.

---Ya lo sabemos; ya lo sabemos, Guadalupe. Pero en todo lo demás
Maclovio nos ha dicho un puñado de verdades; y las ha dicho bien
----dijo Alonso.

---Bueno, pero el asunto es otro ---interrumpió Don Rodrigo que
gustaba provocar con su tono bonachón las explicaciones de sus
huéspedes, haciéndoles observaciones impertinentes---. El asunto
es otro, Maclovio. No comprendo su antinazismo. Por una parte
se desgañita contra el comunismo, por otra está contra los nazis,
pero nunca ha ocultado sus simpatías por Franco. Si no lo conociese
tan bien diría que es usted un farsante; pero como ese no es
el caso sí digo que su actitud es contradictoria.

---Te la voy a explicar bien; y también quiero que usted me escuche
---dijo dirigiéndose a Alonso--- pues deseo conocer su opinión.
Soy antinazista y anticomunista porque el nazismo y el comunismo
no son antitéticos; tienen, por el contrario, grandes semejanzas.
Unos hablan de la lucha de clases, los otros de la lucha de razas;
unos quieren la dictadura del proletariado y los otros la dictadura
de los arios. Ambos son enemigos de la Iglesia Católica; ambos
practican el capitalismo de estado, son totalitarios en sus métodos
y enemigos de la libertad. A ver, Alonso, ¿tengo o no tengo razón?

Rodrigo dijo entonces: ---usted, Maclovio, busca siempre el apoyo
de Alonso porque sabe que con él nunca peleo. Pero ha eludido
usted la segunda parte de mi interpelación, la que se refiere
a que, al mismo tiempo que es antinazista es usted franquista.

---No eludo nada, Rodrigo; te voy a explicar esa paradoja. He
sido franquista porque soy católico y el triunfo de los rojos
habría acabado con la Iglesia en España. ¿No es bastante? Pero
además, no es cierto que la república fuese popular; el pueblo
la odiaba porque era una tiranía y el español siempre ha estado
contra las tiranías. Si Franco se convierte en un tirano, el
pueblo estará contra él.

---Es que dicen que ya es un tirano ---observó con burla Rodrigo.

---Pues no me asusto de mis palabras; ---replicó vivamente Maclovio---
si Franco es ya un tirano, puedes jurar que el pueblo español
está ya contra él. ¿Estás satisfecho?

---Yo creo que sobre el caso de España puede hacerse un símil
clínico, con perdón de Remigio, cuyo campo invado ---dijo entonces
Alonso---. Cuando la espiroqueta pálida ataca el cerebro produce
la parálisis general; esta afección equivalió durante mucho tiempo
a una sentencia de muerte, sin indulto posible. Alguien, creo
que un médico vienés, descubrió que cuando el enfermo sufría
un ataque de fiebre muy alta mejoraba su estado y muchas veces
se obtenía una curación absoluta. Entonces produjo esa fiebre
artificialmente inyectando el microbio de la malaria. El enfermo
sanaba de la paresis, pero quedaba palúdico y muchas veces gravemente
palúdico. Pues bien, España sufría la parálisis general del comunismo;
ahora sufre de un grave paludismo nazista. Es indudable que en
aquella época la parálisis comunista parecía más grave que el
paludismo nazi, aunque a la fecha la proposición inversa sea
más sostenible. Quizás la analogía sea confusa, pero no encuentro
mejor manera de expresar lo que pienso.

---Su comparación, Alonso ---replicó Rodrigo--- tiene la ventaja
para usted de que no lo compromete. Me gusta oírle opiniones
más concluyentes. Por ejemplo, España está ahora con el Eje y
los comentaristas dicen que si la república hubiese triunfado
estaría ahora con las Naciones Unidas. Yo ni pongo ni quito rey;
repito lo que oigo.

---Cuando a usted le conviene, Rodrigo, tiene usted catarro,
como la zorra del cuento ---contestó Alonso---. Pero le diré
lo que pienso, aunque me comprometa. Pienso que la victoria de
la república habría convertido a España, como enemiga de Hitler,
en una nación ocupada más. ¿De qué le sirvió a Francia su frente
popular? Si España hubiese sido otra guarida bolchevique, los
nazis habrían saltado los Pirineos y tendríamos allí otro Pétain
y otro Laval. Entre paréntesis, Serrano Súñer sería un magnífico
candidato para este último puesto. Mire, Rodrigo, mi símil puede
usted calificarlo de pedante, lo admito; pero si lo analiza verá
que no es muy inexacto.

---No les falta a ustedes ---dijo Rodrigo--- sino aplaudir la
importación de refugiados españoles.

---Pues le diré ---replicó Alonso--- yo sí la aplaudo. Se equivocó
usted si creyó que iba yo a saltar al piquete de su aguijón.
La inmensa mayoría de ellos son hombres de trabajo y al acogerlos
aquí hemos obrado como nación civilizada. Claro que vienen también
algunos que son líderes o carne de presidio, pero noventa y nueve
entre cien serán aquí excelentes ciudadanos.

---Me deja usted estupefacto; ---contestó el notario--- decididamente
su pensamiento es una maraña.

---Por el contrario; mi pensamiento es bien claro. Y en este
punto el tiempo me dará la razón. Si la república con sus mismos
hombres llegase a restaurarse en España, vería usted que estos
refugiados permanecen aquí en México. Los casados tendrán hijos
mexicanos y los solteros se casarán con muchachas mexicanas.

La charla pareció terminar y Guadalupe y Remigio se levantaron
iniciando la desbandada. Habían fracasado en sus propósitos de
discutir con Alonso Quintanar algunos de sus problemas y decidieron
dejarlo para mejor ocasión. Al día siguiente Alonso y Maclovio
muy temprano salieron a andar por las calles. El cielo estaba
azul, con nubes que se amontonaban en el horizonte anunciando
un aguacero para en la tarde; el aire transparente; las colinas
se veían verdes, como si las hubiesen restregado con agua. Alonso
y Maclovio subieron por la calle del Camino Real y se sentaron
a contemplar el pueblo tendido a sus pies. Cerca de ellos, los
chiquillos salían de las casas y jugaban en el arroyo empedrado;
las carretas tiradas por bueyes descendían lentamente, mientras
las recuas de burros trepaban hacia la carretera para ir a los
ranchos. Bajaron después por el callejón de las Piedras Chinas
y tomaron por la calle del Chorro; desde afuera podía verse el
patio lleno de macetas de las muchachas Huacuja; allí estaban
ellas, frente a sus telares, en tanto que una chica quinceañera,
en el fondo, hilaba la lana; encontraron abierto el tendejón
de Doña Gertrudis; en su estantería se alineaban los frascos
de burbujeante vidrio azul, los pilones de panocha, las sodas
multicolores y los cigarrillos de hoja de maíz. Las muchachas
iban a la fuente con el cántaro en la cadera o sobre la cabeza.
Alonso y Maclovio siguieron por la calle del Correo; a través
de una ventana baja columbraron a Ramona, en su taller de costura,
rodeada de sus aprendizas que se inclinaban sobre los bastidores.
Martín el carpintero los saludó al pasar con un gesto cordial
del brazo, pues tenía la boca llena de clavos.

Los dos paseantes no se habían dicho una palabra. Al llegar a
la calle de Josefa Ortiz, Alonso sugirió: ---Iremos al mercado---.
En él encontraron a gentes que iban a sus compras. Los detuvo
Jesusita para charlar con Maclovio: ---Estás muy buen mozo ---le
dijo---. No te veía hacía siglos---. Usted es la buena moza,
Jesusita, ---contestó Maclovio---. Y siguieron charlando.

Mientras tanto, Alonso sopesaba los tomates, preguntaba los precios,
discutía sobre la clase de las cebollas, mordía los chiles. Se
pararon con Praxedes y arguyeron acerca de la manteca. Eran las
once cuando llegaron de vuelta a la casa de Rodrigo.

En la noche, en el tren rumbo al sur, comentaban el paseo.

---Esa gente vive en otro planeta ---decía Maclovio--- o en otro
siglo; no parece que habitan un mundo en conmoción. ¿Se ha fijado
usted que nadie nos ha hablado de la guerra ni nos ha dicho nada
de la campaña en Rusia o del discurso de Roosevelt? Esto pasa
más o menos, dentro de nuestro país, en todas partes ---contestó
Alonso--- hasta en la capital. Allí se habla mucho y se lee mucho,
pero es inútil, porque tampoco sabemos que estamos en guerra,
ni lo que es la guerra.

México seguía cambiándose de traje. Se despojaba rápidamente
de sus galas del novecientos, que le daban un aspecto de señora
provinciana ataviada con ropas cursis y costosas, para llenarse
de edificios funcionales que también le caían muy mal. Perdía
su aspecto recatado de beata católica para adquirir el de una
vieja descocada que se cuelga collares de neón y se tiñe el cabello.
Los edificios llenos de dignidad del tiempo de la Colonia estaban
inconocibles por la mugre y la incuria. Pero crecía, crecía a
ojos vistas. Ya había pasado del millón de habitantes, del millón
y medio; quizá alcanzaba los dos. Se abrían nuevos cabarets para
turistas; los bares estaban repletos: se llamaban Papillon, Rosignol,
Escargot, Grillon, Souris, Cigogne; se bebía _scotch_, manhattan,
brandy, gin, cobbler, derby y old Tom.

En los hoteles pululaba una muchedumbre cosmopolita; sonaban
títulos más o menos falsos de marquesas y de condes; llegaban
americanas con abrigos de pieles en _coupés_ convertibles y viejos
de cabellos grises que viajaban con una libreta de banco y una
bolsa llena de palos de golf.

En los periódicos aparecían nuevos nombres: polacos, rusos, griegos
o húngaros; mujeres con escotes interminables iban a los conciertos
y estrellas de Hollywood nos hacían visitas en avión. Se multiplicaban
las galerías y salones de arte dudoso; la población burocrática
de las oficinas públicas crecía como una plaga en forma que Malthus
jamás habría sospechado.

En cada manzana de casas se abría un banco que prestaba al veinticuatro por ciento
con hipoteca de la vida; las taquígrafas usaban medias de nailon
y se pintaban las uñas de color rojo-obispo, rojo-sangre, rojo-violeta,
rojo-solferino: toda la gama de los rojos; los _bellboys_ fruncían
el ceño cuando las propinas no llegaban a un peso y se hacía
indispensable fumar cigarrillos Lucky Strike o Phillip Morris.

Llegaba ya el momento en que las gentes se encontraban en las
calles y no se conocían y en que los hombres que habían vivido
cuarenta o cincuenta años en la ciudad tenían la sensación extraña
de que algún genio de Aladino los iba transportando, durante
el sueño, a una cosmópolis desconocida.

La urbe crecía chupando la savia de los campos donde las milpas
levantaban con esfuerzo hacia el cielo sus mazorcas anémicas;
los peones seguían alimentándose con las mismas tortillas embarradas
de chile que comían sus ancestros; en los ejidos miserables,
que los líderes pintaban como jirones de un paraíso redivivo,
los niños morían por racimos de infecciones en las tripitas;
el país se apretaba la cabeza después de veinte años de borrachera.
Pero la urbe crecía, y los políticos y los financieros se mostraban
ufanos por ello; hacían continuamente declaraciones optimistas.

Cuando se declaró la guerra al Eje las gentes citadinas comentaban
el suceso con una curiosidad desinteresada; cuando más lo juzgaban
desde el punto de vista de su bienestar más inmediato.

Tendrá que venir mucho dinero yanqui ---se decían---; los ingenieros
famélicos soñaban en contratos fabulosos; otros hablaban de los
metales raros; los industriales aumentaban sus precios. La vida
subía de costo en consonancia con ese delirio de grandeza. Pero
pocos pensaban que la guerra significaba para México algo más
que el lucro; pocos se daban cuenta de que el destino nos había
conducido fatalmente a ser actores, tal vez preponderantes, en
el gran drama de la humanidad. Pocos penetraban el sentido esencial
de las cosas que los rodeaban y apenas si unos cuantos comprendían
que si los nazis llegaban a dominar la tierra solo podría pensarse
en emigrar a otro planeta o en elevar una inmensa oración al
Todopoderoso para que una gran catástrofe cósmica lavase para
siempre las culpas de los hombres. Por el contrario en México
se suponía que éramos ajenos a la contienda, que no teníamos
cuentas pendientes con Alemania, que estábamos en esa guerra
por congraciarnos con el tío Sam.

A la gente le sonaba a moneda falsa que, para lanzarnos a la
lucha, se invocase a la Señora Democracia la cual entre nosotros
había sido siempre escarnecida y vapuleada. Nadie le decía al
hombre de la calle que el amago no era contra la democracia,
sino contra la libertad de hacer un hogar; de casarse, si así
le venía en gana, con una mujer a su gusto, mestiza como la Virgen
de Guadalupe, o rubia como una hija de _viking_, o con una negra
olorosa a clavo; la libertad de abandonar su trabajo y cambiarlo
por otro, la de vender su alma al diablo por un vaso de vino,
o la de ponerse de nuca en la mesa del comedor para divertir
a los chiquillos. La libertad de decirles a nuestros hijos que
debemos adorar al gran Señor Buda, repitiendo cotidianamente:
«yo me refugio en ti» o a un totem policromo traído de los confines
del círculo Artico; o la de leerles el «Ta-hio» del venerable Khoung-Fou-Tsen,
o la libertad de ser suficientemente tontos para predicarles,
en tono doctoral, que todo se hizo de la nada y que Dios es una
hipótesis superflua. O finalmente, la de enseñarles que hay un
Gran Padre Omnipotente y Justo, que cuida con amor de sus criaturas
y que murió en una Cruz.

Ese hombre simple, engreído con su universo personal, ignoraba
sin embargo que podía perderlo y que ningún beneficio del orden
nuevo, ni los hospitales, ni la salubridad, ni los aviones, ni
los _ersatz_ ni las fábricas, ni la economía científica, ni siquiera
el imperio del orden y del sentido común podrían resarcirlo.

Es muy difícil escribir de estas cosas, a veinte años de distancia,
cuando no se han presenciado, cuando solo se obtienen las noticias
aisladas de las gentes que vivían en aquellos días. Pero parece
ser que entonces comenzó a perfilarse el milagro. En esa costra
de incomprensión y de pensamientos egoístas, comenzaron a brotar
corrientes filiformes de una agua clara. Eran los indicios inequívocos
de un gran río subterráneo.

La juventud principió a darse cuenta de que tenía que entrar
a la lucha; no era posible eludirlo. El instinto los guiaba;
con una seriedad inesperada acometieron su enseñanza militar.
Lo mismo en México, que en Monterrey, que en los villorrios,
se marchaba en las calles desde las seis de la mañana; no eran
los ejercicios de las centrales obreras que en años anteriores,
con una actitud de amago hacia la burguesía trabajadora, se preparaban
para desfiles oropelescos de atletas ventrudos; tampoco eran
las marchas matutinas de empleados públicos que madrugaban durante
un mes por temor al cese. Ahora no; los hombres acudían espontáneamente
a sus lugares de instrucción, congregados por barrios, mezclándose
los obreros con los profesionistas, los burócratas con los de
vivir independiente.

Por primera vez se daban cuenta todos, ante un peligro vago e
indefinido, que la vida era una tarea de cooperación, impuesta
a los hombres por Aquel-que-todo-ata-y-desata.

Así se fue despertando ocultamente la conciencia colectiva; los
hombres se hacían a la idea de que tarde o temprano iban a necesitar
enfrentarse a una contienda real. A la hostilidad para los yanquis,
que era como un _leit-motif_ de la psicología nacional, se fue
superponiendo lentamente un sentimiento más constructivo, de
mayor anchura y también más hondo; empezó a vislumbrarse que
la era de las reyertas por territorios, por el dominio de mercados
y por antagonismos políticos pasaba a planos de menor importancia
ante la pugna por el destino del hombre.

Fue aquella juventud la que, cuando llegó el momento, opuso un
valladar a la invasión.

Ahora, en 1960, esos hombres tienen cuarenta y cinco, cincuenta
o cincuenta y cinco años y no hablan de su hazaña; están entregados
a la tarea de reconstruir. Trabajan en las cooperativas, en los
laboratorios o en las granjas; prefieren olvidar el pasado. Todavía
no ha llegado quien haga una interpretación de esos tiempos,
que sentimos tan próximos y que están sin embargo, tan lejanos.
Años confusos, en los que zozobraba la buena fe.

El Gobierno Americano creyó entonces que bastaba el formulismo
de las relaciones oficiales para crear un ambiente de cordialidad.
Eso se llamaba la política del Buen Vecino. Consistía en hacer
discursos, en gritar «¡Viva México!», en agasajar a los burócratas
mexicanos que iban a los Estados Unidos con objeto de visitar
Hollywood y hacer compras en las tiendas de five and ten.

No comprendían que lo único que podía borrar una desconfianza
tradicional era poner esa labor de acercamiento, en ambos países,
en manos de las gentes de buena voluntad, ayunas de la leche
de los presupuestos. Esas gentes abundaban, aquí y allá. Nada
hay tan parecido a un hombre de trabajo mexicano, como un hombre
de trabajo americano. Ninguno de los dos saben gran cosa de la
doctrina Monroe o de las conferencias de Río Janeiro; pero ambos
tienen una casa, una mujer que trajina, que protesta por los
precios del mercado, que les arregla las camisas, que los cuida
si están enfermos, que los besa cuando salen en las mañanas y
manda los chicos a la escuela. Ambos tienen bebés que chupan
el biberón y hacen pipí en la cama y juegan en los jardines,
rompen los zapatitos y después aprenden a leer, a escribir, a
contar; más tarde estudian en los libros y van a las universidades.
Esos muchachitos son los mismos aquí y allá: unos prietuzcos,
de ojos negros; y los otros rubios, con grandes ojos de un azul
transparente; pero unos y otros viven por las mismas cosas y
cuando crecen están dispuestos a morir por ellas; mientras sus
padres, aquí y allá, profesan un idéntico credo de amor, aman
la libertad y tienen fe en el mismo Dios-que-no-se-nombra, ---como
dijo el poeta.

</section>
<section epub:type="chapter" role="doc-chapter">

# IV

++El++ hecho era que los alemanes tenían puestos los ojos en
México desde hacía muchos años. Los que habían leído _Mein Kampf_
habían encontrado las frases precisas. En la república formaban
una típica colonia de raza aria: inasimilable y superior; en
sus fincas de Chiapas vivían como si estuviesen en un jirón de
África. La mayor parte de ellos no venían nunca al centro del
país; tomaban año tras año un barco en Puerto México para ir
a sus casas de Hamburgo o Berlín. Cuando les hicimos la declaración
de guerra los que no pudieron evadirse fueron concentrados en
la capital; allí tenían evidente desahogo económico, seguían
haciendo transacciones en las oficinas públicas, comían en Prendes,
iban a los teatros, su mesa era espléndida, fumaban tabacos habanos
y aunque su casino fue clausurado se juntaban en las casas de
uno o de otro y bebían, pagándolas a escote, las barricas de
cerveza o las cajas de coñac. Muchos de ellos seguían en sus
ocupaciones habituales. Maclovio contaba indignado, con su vehemencia
acostumbrada (y por supuesto llenando de improperios al Gobierno)
que había encontrado en un banco a un alemán su conocido, traficante
en medicinas y al preguntarle cómo iban sus cosas, le contestó:
---Un poco difíciles; pero no hay que alarmarse, se vive bien.

De cualquier manera aquellas reuniones de boches eran en realidad
juntas de partido: se planeaban sabotajes, se colectaban fondos
para pagar espías, se proporcionaba dinero a aventureros que
se internaban en los Estados Unidos con pasaportes falsos. Mantenían
relaciones estrechas con mexicanos oposicionistas cuya aversión
al estado de guerra era bien conocido e iban efectuando una labor
de zapa.

Uno de estos boches comerciaba en maquinaria, pero lo mismo podía
ser tratante en fornituras o en barnices. Lo importante para
él había sido siempre tener a mano algo que venderle al gobierno,
pues allí era donde obtenía pingües sobreprecios. Cuando llegó
a México, allá por los años de novecientos ocho, se llamaba Hugo
Hütten, pero a medida que acrecentaba su cuenta de banco adquiría
pujos de aristócrata. Al regreso de uno de sus viajes a Alemania
vino usando monóculo y pasando en los ministerios unas tarjetas
con letra realzada que decían: Barón Hugo von Hütten; desde entonces
le llamaban el Barón, de lo cual se sentía muy ufano. Su vanidad
más grande había sido siempre el contacto que mantenía con prohombres
del régimen en el poder, desde el asesino Victoriano Huerta hasta
los más flamantes revolucionarios.

Cuando estalló la guerra los triunfos nazis le producían vértigos;
su instinto de superioridad racial sobre los pobres mexicanos
se afirmaba y soñaba en lo que podía significar para él la dominación
teutona en América. En un principio aquello eran solo sueños;
pero el _blitzkrieg_ que arrasó al mundo como una tormenta les
fue dando en su espíritu consistencia y materia. Ya a raíz de
la ocupación de Creta, del profundo avance en territorio ruso
y de la toma de Tobruk, los mensajes que para los nazis de México
llegaban de Alemania, por los múltiples conductos secretos que
con años de anticipación habían creado, contenían promesas definidas
del asalto al Nuevo Continente.

El plan teutón se perfiló desde el principio con toda su perfidia.
Había que fomentar el odio a los yanquis, hacer impopular la
guerra y despertar entre los mexicanos un secreto deseo de tener
un gobierno que pudiera enfrentarse al tío Sam. Desgraciadamente
existía un sector grande donde esta ideología tenía arraigo:
era la generación desencantada de principios de siglo, la que
había sido sinceramente antiporfirista y había sufrido más tarde
el desengaño de la Revolución; la que no olvidaba la afrentosa
y estéril ocupación de Veracruz en la que un puñado de buenos
muchachos mexicanos y americanos habían perdido la existencia.
Muchas entre esas gentes de espíritu patriota y abnegado formaban
una magnífica tierra de siembra para la maquiavélica maniobra
nazi. Pero sabían también los agentes de Hitler que ninguna de
ellas cometería la traición de ponerse al lado de un ejército
invasor y por lo tanto dirigieron toda su actividad a encontrar
los canales adecuados para inyectar el virus, dando así al movimiento,
llegado el caso, una apariencia de agitación doméstica.

Visitaba a Von Hütten un individuo claramente simpatizador de
Hitler; era un panfletista que publicaba artículos y folletos
con dinero alemán. Por los años de treinta daba conferencias
para explicar el sentido de la obra de Mussolini y hacía su exégesis
con una voz pastosa de protagonista de capa y espada. Para «epatar»
a su auditorio le hacía citas en italiano: «Quando due elementi
sono in lotta e sono irreducibili, la soluzione e nella forza»
y en seguida traducía con lentitud, agregando campanudamente:
palabras de Mussolini.

Era escultor y cuando joven estuvo en Europa; pero la materia
se rebelaba en sus manos; le faltó consistencia espiritual para
luchar con esa sustancia indómita que le hacía rebotar el cincel.
Bastante inteligente para conocer su impotencia, no tuvo el valor
de seguir otro camino y continuó medrando como artista. Otro
motivo lo obligaba a ello: vivía de una beca que le pagaba un
prócer del porfirismo; el abandono de sus actividades era también
decir adiós a la pensión que le llegaba puntualmente cada mes.
Era decir adiós a París, a Montparnasse, al aperitivo en «La
Closerie des Lilas» y a las reuniones de pintores melenudos que
no pintaban, pero hablaban de arte bebiendo café y ajenjo.

El sabía que era un fracasado; pero a falta de entereza espiritual,
su inteligencia despierta de hombre vivo le prestaba ropajes
nuevos. Desde luego tenía que seguir siendo escultor; eso le
daba ya un asiento. Entonces inventó una materia plástica a la
que llamaba siddarthita, la cual endurecía al contacto de un
buril caliente. Más tarde conectó su utensilio a un enchufe eléctrico
y después, ya en México, le agregó un aditamento que era como
un bulbo de rayos catódicos, el que, según decía, al bañar la
siddarthita con corrientes electrónicas, prestaba una calidad
incomparable a las superficies.

Consiguiendo unas líneas de propaganda aquí y allá preparaba
su regreso a la patria; llegaría a México como un innovador,
como un artista más allá de la crítica. Miguel Angel había creado
la forma, pero el mármol venía de la dolorosa gestación de las
edades. El creaba la forma, y creaba también la materia. Pero
eso no era bastante, necesitaba otras muletas para su pobre espíritu
derrotado y se cambió el nombre. Sus padres lo bautizaron católicamente
como Serafín Rafael Pérez; él se hizo llamar Siddartha, y más
tarde doctor. Abominó de su niñez, de la vida provinciana en
su casa cuando era joven y su madre lo llevaba a misa.

Había, sin embargo, que buscar otras vías; ni la siddarthita
ni el doctorado eran bagaje bastante para vivir en México; ni
siquiera sus aptitudes de oceanógrafo.

Estalló la guerra de 1914; había que salir de Francia que en
esa crisis arrojaba de su seno a todos los parásitos. Y llegó
a México, a la retaguardia de la Revolución; pero fue desafortunado
porque se inclinó siempre por los regímenes perdidosos. Adquirió
un énfasis persuasivo y rimbombante.

Se hizo bastante conocido por sus trajes de pana, sus largas
melenas y sus barbas espesas, que le daban un aspecto de un Darwin
criollo; además, había pasado su vida descendiendo a dos o tres
brazas de profundidad con escafandra de buzo, en las costas de
Yucatán y escrito varias monografías sobre la vida del fondo
de los mares; alegaba que cerca de Campeche existió una gran
ciudad ahora sumergida y publicó un plano en el cual precisaba
el sitio exacto donde los habitantes iban a comer pescado frito
en aceite de oliva. En realidad, sus aficiones submarinas venían
del tiempo en que decidió dedicarse a la pesca de esponjas; fundó
una compañía y vendió algunos miles de pesos de acciones con
los que vivió varios meses.

Tenía una colección de caracoles que decía haber encontrado en
sus someras exploraciones y aseguraba que valía dos millones
de dólares; pero alardeaba de su desprecio por el dinero. Sin
embargo, la amargura de ese hombre fue siempre no tener el poder;
el poder político, o el militar, o el poder del oro. De inteligencia
despierta, pero demasiado disperso para imaginar algo constructivo,
era un ambicioso fracasado. Frecuentaba a la gente de letras
y por eso era conocido de Maclovio Otamendi, a cuya librería
de viejo, frente al lado norte de la Alameda, acudía con frecuencia.
En el correr del tiempo desempeñó un papel importante de propagandista
del Eje y murió al fin ahogado ensayando un aparato sumergible
de su invención.

Este hombre proporcionó servicios apreciables a los nazis por
el poder persuasivo que tienen los grandes contadores de mentiras,
pues lo mismo decía que acababa de recibir una carta de Hitler
o que tenía una oferta de cincuenta mil dólares por su escultura
del Prometeo Cósmico. Pero le fue sobre todo útil a Hütten porque
lo puso en contacto con varios ejemplares de la fauna mexicana.

Uno de ellos era el ingeniero Esteban Cabral, quien había actuado
en la revolución desde 1910, uniéndose a Madero. Era el tipo
del hombre inteligente, pero de mentalidad caótica. Pudo, sin
embargo, seguir su camino ascendente porque en la marejada de
la lucha civil un cerebro constructivo era casi un lujo estorboso.
Nunca conoció, en realidad, la parte noble y creadora de su oficio;
cuando fue Ministro de Obras Públicas ---las fechas se me embrollan---
allá entre 1920 y 1930, en lugar de aplicarse a colocar honestamente
un ladrillo sobre otro con la concienzuda sabiduría de un buen
albañil, escribía, hacía conferencias y buscaba una popularidad
continental. Se fue rodeando así de un séquito de jóvenes; enviaba
mensajes al mundo entero. Muchos creyeron en él; le escribían,
le contaban sus cuitas, sus incertidumbres; le pedían una luz
para su oscuridad. Pero le faltaba la abnegación cristiana del
que nada quiere para sí. Con aquella juventud que creía en él
hizo carne de propaganda política.

En 1940 se sentía vencido. La vida se iba con rapidez vertiginosa
y sus sueños no se realizarían nunca. Entonces traicionó a las
juventudes de las cuales había pretendido ser un guía y se echó
en brazos de los nazis. En sus noches de insomnio, a medida que
Alemania avanzaba en sus conquistas, el espejismo se veía más
cercano hasta parecer cosa real; él sería el _gauleiter_.

Iba a la casa del Barón con un digno y honorable abogado, hombre
de sesenta años, jefe de un bufete bien reputado, pero que pertenecía
a ese grupo escéptico que ya no creía en nada. Tuvo quizá, razón
para ello: formó parte de varios gobiernos de la Revolución y
tropezó solo con la incompetencia y la codicia; vio cómo, a su
alrededor, sus amigos más allegados, que con él habían luchado
por un ideal en sus años mozos, improvisaban fortunas; todos
ante sus ojos se rindieron a los halagos del dinero y a la postre
compraban coches y mantenían queridas costosas. Sentíase aislado;
como tantos otros culpaba a los yanquis de esa podredumbre, razonando
que solo con su apoyo pudieron encumbrarse y sostenerse tantos
gobernantes pícaros. Cuando comenzó a frecuentar a Von Hütten
y lo veía derrochar en comilitonas y bacanales, él se tranquilizaba
pensando que ese dinero venía del trabajo limpio de un hombre
de empresa y no de los rateros de dineros públicos con los cuales
tuvo contacto y aún amistad. Pensaba de buena fe que la declaración
de guerra había sido, por parte de México, un acto de vergonzosa
sumisión al tío Sam y aceptaba sin repugnancia los agasajos de
aquellos teutones que, cuando tomaban cerveza en los tarros enormes,
se le antojaban honrados burgueses, encarnación de las virtudes
familiares. Aprendió a despertar su sed con _brezeln_, a comer
una _pressack_ con mostaza y a cantar a voz en cuello «Was ist
des Deutschen Vaterland».

Los nazis iban así creando al amparo de la despreocupación del
gobierno una trama para sostener el tejido de sus futuras maniobras;
dirigieron sus actividades hacia todos los descontentos, los
despechados, los ambiciosos, agitando las pasiones buenas y malas
que podían servir sus intereses. En sus listas de amigos figuraban
Martín Cordero, Guilebaldo Ordóñez, Guillermo Abrego, Timoteo
Ramírez, Atanasio Cifuentes y otros. Muchos, entre ellos, eran
escritores y todos gente conocida. Estos hombres que ensayaban
entonces sus papeles, eran los fantoches del _guignol_ que habría
de ensangrentar la república.

Así, ensartando los hilos de la intriga con una paciencia admirable
y prodigando el dinero a espuertas, los alemanes iban preparando
la acción futura.

Mientras tanto la lucha seguía y llegó el final de 1942. Hitler
trataba, a toda costa, de apoderarse del petróleo ruso; los ingleses
hablaban de abrir un segundo frente, pero hicieron un ensayo
de incursión en Dieppe y sus pérdidas fueron tales que los Estados
Mayores pospusieron sus proyectos de desembarco en Francia. Los
Estados Unidos, a pesar de que un reducido número de escritores
hacían una campaña vigorosa para despertar a las gentes a la
realidad, ignoraban aún lo que era la guerra. Las fábricas americanas
e inglesas, es cierto, producían aviones por centenares y millares
y las estadísticas de fabricación se publicaban en el mundo entero;
llegaban por supuesto hasta Berlín, donde esos datos eran cuidadosamente
comprobados o rectificados. Mientras tanto Alemania, en secreto
y sin dejar traslucir la verdad, se aplicaba a una producción
gigantesca de materiales de guerra; los aviones y los submarinos
salían de las manufacturas en cifras fabulosas. Atestaban los
hangares y puertos escondidos y los campos de aterrizaje desparramados
por millares en toda Europa; sin embargo, como no contestaban
los golpes de la Real Fuerza Aérea ni aquellos de los bombarderos
yanquis, los más optimistas hablaban del próximo colapso teutón.

</section>
<section epub:type="chapter" role="doc-chapter">

# V

++Lo++ que voy a referir ahora a grandes rasgos, es perfectamente
conocido y puede leerse en multitud de periódicos de la época,
no solo publicados en el país, sino en el mundo entero.

Lo más interesante en la materia es la obra de un historiador,
cuyo nombre se me escapa, pero que fue un periodista conocidísimo
hace veinte años. Sus «Crónicas mexicanas de la invasión nazi»,
en tres volúmenes, pueden considerarse por su claridad, su construcción
y su doctrina lo mejor sobre el tema.

A esas páginas debe acudirse para conocer con exactitud el fondo
sobre el cual se mueven mis personajes desde 1940 hasta la expulsión
de los alemanes.

A mediados de 1943 un ambiente de optimismo envolvía a las naciones
aliadas. La ocupación del norte de África, el empuje denodado
de los rusos que luchaban, no por el comunismo, sino por su suelo,
y finalmente los golpes de la Real Fuerza Aérea y de la aviación
norteamericana sobre el territorio alemán, hacían concebir esperanzas.
En México las gentes llevaban esa vida febril de los países inundados
de dinero, pero los hombres tranquilos, que no eran presa de
ansias de riqueza, veían el porvenir oscurecido. Se decían que
la miseria producida por la inflación iba a abatirse sobre una
masa despreocupada cuyo metabolismo seguían un ritmo sin freno.
Y se decían también que eso no iba a ser el resultado del desgaste
de la guerra, ni de las privaciones, sino precisamente de lo
contrario. El número de automóviles crecía, la gasolina y las
llantas se despilfarraban y las playas no podían contener a los
visitantes. Alonso Quintanar solía recordar una ingenua ficción
que leyó en su infancia, cuyo título era «La ciudad oxihidrogenada»,
en la que un tranquilo villorrio flamenco de vecinos reposados
cambia de la noche a la mañana: aumenta el número de pulsaciones,
sube la presión arterial, se aceleran todos los procesos fisiológicos
y hasta los enamorados se demuestran el amor al estilo de los
países tropicales. Concluía Alonso que México parecíale sujeto
a un experimento semejante.

La capital se llenó de refugiados que venían huyendo de la guerra
en otros países. Unos eran pobres y como llegaban de lugares
donde la lucha es más áspera apuraban su inventiva, se ponían
a trabajar y al cabo de algunos meses vivían con holgura. Los
que eran ricos alquilaban por una bicoca casas amuebladas y se
rodeaban de un ejército de criados. Unos y otros se radicaron
en las ciudades, principalmente en la capital, y considerándose
en el paraíso, llenaban los tanques de sus automóviles hasta
derramarlos, dejaban el fondo de sus tazas embarrado de azúcar
y bebían sin ponerse coto un café caracolillo que es el mejor
del mundo.

Maclovio Otamendi y Alonso Quintanar charlaban a menudo en el
expendio de libros viejos situado en la Avenida Hidalgo. Todavía
ahora existe allí una librería; se llama Pahlas y tiene un tecolote
pintado junto a la puerta, lo que quiere decir que hay símbolos
que no envejecen. Adentro la luz difusa de los muros y de los
mostradores, que se ven pletóricos de volúmenes y están construidos
de cavolita transparente, da una claridad suave. El actual propietario
tiene allí mesas donde los clientes hojean las obras y sillones
donde leen los periódicos; todo esto en un ambiente policromo
de azules y amarillos. Pero en aquellos lejanos tiempos, que
son lejanos no por el número de lustros transcurridos ---apenas
cuatro--- sino porque las cosas eran distintas, esa tienda, en
lugar de los rótulos luminosos, ostentaba uno de manta restirada
que decía: Libros ---compra y venta---. Aquello no tenía aliciente
sino para los lectores empedernidos que formaban sus bibliotecas
en los expendios de viejo. El interior estaba oscuro, lleno
de polvo; los tomos mugrientos se apilaban sobre las estanterías
de ocote, pero este desorden formaba parte del goce de los bibliófilos
y bibliómanos. Y sobre todo, allí estaba Maclovio, con su palabra
elocuente, sus conceptos agresivos y su crítica mordaz iluminando
aquel antro con el chisporroteo de su inteligencia.

Alonso hacía sus teorías con una voz tranquila que parecía desinteresarse
de las cosas. Se había sentado en una silla incómoda, detrás
del mostrador y hablaba pausadamente; Maclovio estaba de pie,
recargado en los libreros, metidos los pulgares en los bolsillos
del chaleco, en una actitud que le era muy peculiar. Se encontraban
solos.

---Los Estados Unidos ---decía Alonso Quintanar--- no se dan
cuenta aún de que están en guerra, y si ellos no lo comprenden,
menos nosotros. Creen que la guerra está lejos porque los defiende
el océano; también en 1914 el Canal de la Mancha defendía a los
ingleses. Es un país de tecnólogos, pero parecen ignorar el avance
acelerado de la tecnología.

---Estoy de acuerdo con usted ---observó Maclovio.

---Casi siempre estamos de acuerdo ---continuó Alonso--- y llegamos
a las mismas conclusiones, aunque por distintos caminos. Pero
quiero seguir criticando la actitud yanqui. Desconocen también
la geografía y parecen ignorar que el día que sean atacados lo
serán a través de nuestro territorio. Y después de un año de
guerra, en 1943, seguimos indefensos. Creen y creemos que veinte
tanques, cien camiones, unas cuantas ametralladoras y media docena
de piezas antiaéreas van a protegernos y a protegerlos a ellos.
Eso apenas sirve para los noticiarios cinematográficos.

---Está usted en un error ---saltó Maclovio--- en un craso error.
Los yanquis saben muy bien que estamos inermes y que con lo que
nos han mandado apenas si se arrancan aplausos en las salas de
cine. Pero no nos arman porque no tienen fe en nosotros; nos
han visto tan de cerca que desconfían de nuestra capacidad para
organizarnos; se imaginan que si algún día hay que hacer la defensa
de América, ellos la harán.

---Quizá eso sea cierto, Maclovio; pero de ser así cometen dos
desaciertos. En primer lugar las defensas no se improvisan; hay
que construirlas.

Alonso calló mientras Maclovio lo contemplaba en actitud expectante.
El primero dijo entonces:

---Por lo visto hoy está usted de vena para escuchar, veo que
puede usted resistir un discursillo y voy a aprovecharme. Mire,
Maclovio, la defensa de México debe prepararse con rapidez; pero
esto requiere tiempo. Requiere tiempo hacer la carretera hasta
Panamá; lo requiere hacer obras en Acapulco y Tampico y también
acondicionar nuestro Istmo y convertirlo en un medio decente
de comunicación interoceánica; y se necesitan muchos meses para
construir algunos centenares de campos de aterrizaje y varias
docenas de enormes cuarteles. Es preciso comenzar; debimos comenzar
desde el día en que admitimos que existía un estado de guerra
y no hemos hecho nada. Yo no soy estratega, mas hay cosas de
sentido común que se leen en los libros y en las revistas y para
estas fechas miles y miles de hombres deberían estar realizando
estas labores. Pero en primer lugar, ¿qué quieren que haga un
país como el nuestro que apenas cuenta cada año con cien millones
de dólares? Disponemos de buenos técnicos, pero no forman parte
de la burocracia gubernamental que lo ahoga todo, aquí y allá.
Por la burocracia estuvo a punto de perderse definitivamente
esta guerra; por ella sucedió lo de Pearl Harbor y se perdieron
las Filipinas. ¿Y aquí qué ha sucedido? Que nadie habla de Acapulco
si no es para ir a tumbarse a las playas y beber cocteles; ni
del ferrocarril del Istmo, y en cuanto a esa carretera Panamericana
la acabaremos dentro de diez años y el día que quieran usarla
para movilizar un ejército los pavimentos van a estrellarse como
cáscaras de huevo y los puentes se hundirán con el peso de las
cargas de guerra.

---Bueno, Alonso ---comentó Maclovio Otamendi---. Tal vez tenga
usted razón. Admiro la fuerza industrial de los Estados Unidos,
pero soy un librero sin sentido práctico y reconozco que hay
cosas que prefiero no juzgar. Lo escucho sin embargo con interés.

---Ya que está usted en tan buena disposición, voy a terminar,
---dijo Alonso---. Se equivocan también creyendo que no podemos
hacer uso de los elementos que nos proporcionen. Lo que nos dan
es con cuentagotas y a través de docenas y docenas de oficinas
y cientos y cientos de papeles firmados. Ese error lo cometen
porque ignoran dónde está el pueblo de México y también de lo
que es capaz. Este pueblo, Maclovio, puede un día salvar a la
América. Pero creen que el pueblo de México es el gobierno de
México; y no hablo del gobierno de ahora que está en manos de
un hombre cuya bondad y buena fe nos hacen olvidar a los mexicanos
muchas porquerías, sino de todos los gobiernos. Identifican al
pueblo de México con quienes en las oficinas públicas redactan
oficios idiotas y se ofrecen al cohecho. Ojalá y no llegue el
momento en que la suerte del planeta dependa de las gentes de
este país; pero si ese momento llega, Maclovio, verá usted producirse
lo inesperado: cada hombre lo sacrificará todo, el bienestar,
la familia, los pequeños y grandes placeres y todas las cosas
que nos hacen existir y luchar, e inclusive la vida, por defender
algo más que nuestro territorio: la libertad del mundo.

Desde 1943 a 1946 los nazis desarrollaron en la república mexicana
su vasto plan. Comenzaron a formarse partidos políticos sin fuerza
ni arraigo; este fenómeno era tan frecuente aquí que nadie reparó
en él, pero los hombres observadores encontraban esos grupos
fuera de estación, como los tomates o los mangos que se dan en
invierno. Los partidos se forman cuando se acercan los elecciones
presidenciales; tres individuos, se declaran revolucionarios,
escogen un nombre rimbombante: Partido Evolucionista Revolucionario
de Izquierdas o Partido Revolucionario Futurista o Partido Proletario
Revolucionario y desde ese momento se llaman a sí mismos el Perri,
el Perrefe o el Peperre. {.espacio-arriba3}

Sin embargo, aunque en aquellos momentos no había candidaturas
en puerta, surgían grupos con nombres anodinos e inexpresivos.
Se denominaban «Amigos de un México Mejor», «Partido pro-libertad»
o «Club político de ciudadanos». Lo extraño era que disponían
de dinero; mantenían oficinas limpias y bien amuebladas y pagaban
sus compromisos. Pertenecían a la oposición, pero sin hacer aspavientos
contra el gobierno y aparentemente lo que deseaban era vivir
sin molestias.

Este galimatías debe ser incomprensible para quienes no hayan
vivido en aquellas épocas. Ahora las cosas son mucho más sencillas
y aunque a nuestros gobiernos no se les llama ampulosamente democracias,
en realidad han reivindicado ese sustantivo tan desprestigiado
entre nosotros pues ahora todo tiende inflexiblemente al bienestar
de las mayorías. Cuando estos sucesos acaecían yo era un mocoso,
jugaba al trompo, estudiaba apenas la geometría euclidiana y
me daba de golpes si creía que mis derechos eran atropellados.
Pero ignoraba en absoluto lo que era un diputado y menos aún
un pistolero a sus órdenes pagado con los dineros públicos. Después,
con el tiempo, y sobre todo ahora para escribir este relato he
tenido que hurgar en los periódicos y hablar con los viejos para
poder dar así una impresión vivida de entonces.

Lo que sí recuerdo con precisión fue la sacudida que produjo
el reajuste, al implantarse las primeras medidas del Mundo Nuevo,
cuando las fortunas de muchos políticos fueron sometidas a un
juicio de residencia y quedó demostrado hasta la certidumbre
que habían sido forjadas al amparo de la venalidad. Los timoratos
alegaban que los intereses creados eran tan poderosos que se
iba a alterar la economía entera del país; pero eso no era sino
un temor vano, pues ante los recientes trastornos de la guerra,
cualquier medida, por drástica que fuese, aparecía benigna. El
pánico cundió entonces entre centenares de nuevos ricos que habiendo
pasado sus vidas pegados a las ubres del fisco chupando raquíticos
sueldos, adquirieron haciendas, industrias y mansiones, amén
de casas de apartamientos que constituían para muchos políticos,
el medio clásico de convertir en una realidad tangible sus aspiraciones
supremas.

El hecho fue que se rescataron así, a favor de una colectividad
ansiosa de seguir un nuevo camino, centenares de millones de
pesos que sirvieron para aliviar no pocas miserias. De paso muchos
de los hijos de aquellos hombres desposeídos de esos bienes espurios
viéronse obligados a trabajar y así se obtuvo un doble beneficio.

Pero me estoy desviando lamentablemente de la médula del relato.
El año de 1945 sorprendió al país acongojado por la situación
de la guerra y por las primeras agitaciones políticas internas.

La felicidad paradisíaca de los años anteriores había desaparecido
y aunque se vivía mejor que en los Estados Unidos, ya los turistas
no consideraban a México como el nuevo El dorado. En las ciudades
se racionaban la comida, la gasolina y los zapatos; las comodidades
burguesas se convirtieron en un recuerdo de tiempos mejores.
Mas en donde la miseria alcanzaba fabulosos extremos era en el
campo, entre la gente proletaria. Se vivía en plena inflación
y, como de rigor, la indiada era la que padecía con mayor dureza
el azote de la escasez. Esto no lo podían remediar ni las leyes
ni los decretos; subían las curvas del costo de la vida, subían
las estadísticas de las enfermedades y los índices de mortalidad.
Aquellas gráficas rojas y verdes, parecían trazadas por la cola
del diablo.

Los pequeños partidos incoloros comenzaron a dar muestras de
actividad; ocuparon grandes casas viejas en el centro de la urbe.
Manadas interminables de hombres ociosos entraban y salían de
ellas sin tregua.

En la avenida del Cinco de Mayo, en una finca de la época porfiriana,
se encontraba el hígado de esa efervescencia. Su figura más destacada
era el ingeniero Cabral, pero quien más hablaba y pronunciaba
frases campanudas, era el doctor Siddartha. Este publicaba además
un periódico llamado «Orden futuro», desde cuyas columnas lanzaba
ataques al gobierno.

Con esa propaganda tratábase de envenenar al pueblo insinuando
que su miseria se debía a nuestra participación en la guerra;
se hacía hincapié en la hegemonía americana y el periódico de
Siddartha hablaba en términos despectivos de esa nueva anfictionía
congregada alrededor de Washington.

Dentro del país todos los movimientos de oposición han estado
siempre condenados a la derrota, pero en el caso era evidente
que se gastaba a espuertas. Los volantes y panfletos se distribuían
por millones; se efectuaban desfiles francamente subversivos
y se pedía el fin de la lucha. Por otra parte las naciones del
eje, después de consolidar una situación de resistencia que duró
dos años, habían reanudado su ofensiva.

En la Metrópoli se respiraba un ambiente de inquietud; era claro
que algo se preparaba. Aunque el dinero corría en abundancia
muchos negocios estaban cerrados por los racionamientos. Centenares
de ciudadanos estadounidenses, ante las cargas fiscales de su
país, trasladaron sus fortunas al sur del Bravo. El Banco de
México había tenido que modificar su tipo de cambio y el dólar
se cotizaba a tres pesos mexicanos; pero el costo de la vida
era alrededor de cinco veces el de 1940. Los indigentes padecían
hambre con el salario mínimo.

Estos hechos escuetos son la historia de entonces; están fuera
de la literatura y de las figuras retóricas. Las mujeres hacían
colas interminables para obtener las tortillas o los frijoles;
el pan había desaparecido casi de los menús caseros; las madres
atribuladas se apiñaban en las antesalas de algunas oficinas
en busca de tarjetas con las que conseguir zapatos para sus chicos,
pues estos iban a la escuela mostrando los dedos desnudos como
cabecitas asustadas.

Una enorme burocracia pesaba como plomo sobre los hombres de
trabajo. Los empleados se habían cuadruplicado, quintuplicado;
eran diez veces más que los de 1925 y cincuenta veces los de
los tiempos porfirianos. Los edificios públicos mostraban sus
zaguanes en todas las calles, como fauces de antediluvianos monstruos
listos siempre para dar una tarascada. Cinco millones de seres
vivían del presupuesto del Estado, es decir, del sudor de los
que laboraban y también de los fondos que llegaban día a día
del extranjero a inflar más el mercado. El parasitismo se intensificó,
los intermediarios de todas las categorías se multiplicaron como
ratas, el soborno era la transacción más socorrida. Quienes pescaban
en ese río revuelto se proveían en la bolsa negra, allí obtenían
desde excelentes tabacos de Huimanguillo y dulcísima azúcar de
caña, hasta medias de seda para sus queridas y gasolina para
sus coches. Uno de los personajes más influyentes se hizo el
rey de un gran _trust_ clandestino y especulaba con el maíz, con
la manta, con el café, con los huaraches y con la miseria y la
sangre del pueblo. Resucitó los grandes trenes de caballos y
exhibía por las avenidas sus _landaus_ ingleses con cocheros trajeados
a la mexicana cuajados de botonaduras áureas y arrastrados por
jacas herradas de plata.

A mediados de 1945, el licenciado Rodrigo Guerrero hizo un viaje
a México; contra su costumbre llegó solo, pues Verónica prefirió
quedarse en San Miguel. Esto constituyó una contrariedad para
este hombre que no podía vivir fuera de su casa y para el cual
la presencia de su mujer representaba la ternura hogareña. En
realidad en aquella época los viajes eran difíciles; el servicio
ferrocarrilero era desastroso, pues la casta privilegiada de
los obreros de ese gremio continuaba considerando las líneas
como un patrimonio personal y el mangoneo seguía a todo trapo.
En años anteriores, ya declarada la guerra, los Estados Unidos
pretendieron proporcionar dinero y elementos para corregir una
situación caótica, pero estas buenas intenciones se estrellaron
ante la burocracia, los sindicatos, la ratería inveterada y la
debilidad del gobierno. Se protegía entonces, so pretexto de
mantener invictas las conquistas revolucionarias, a líderes sin
escrúpulos aunque así se contribuyese a aumentar el malestar
de las grandes masas miserables.

Todo esto nos parece absurdo ahora; no concebimos cómo el México
de entonces vivía encadenado a una colección de clisés verbales
divorciados de la realidad. La Revolución, el bienestar de los
campesinos, las conquistas obreras y otros por el estilo. Nada
de eso se había logrado; en cambio todos aspiraban a incorporarse
a la burguesía; los funcionarios públicos dedicaban su ingenio
y su actividad a formar parte del grupo de nuevos ricos; los
líderes entraban a saco en las tesorerías de las uniones obreras
o se ofrecían al soborno de los empresarios para burlar la ley.
Estos hechos eran el secreto de todos; se conocían con detalles
y hasta existían pruebas de ellos, pero nadie los denunciaba
en voz alta. Los que resentían más cruelmente estos privilegios
inmorales no tenían fuerzas para protestar: eran las enormes
multitudes de harapientos que vivían en chozas primitivas, semidesnudos
y mal comidos.

Es necesario abandonar estas digresiones aunque me siento inclinado
a ellas, pues constituyen el fondo de la existencia de entonces.
Si me he dejado arrastrar por la secuela de mis pensamientos
es porque esta trama de circunstancias impidió a Verónica acompañar
a Rodrigo a México. Los billetes eran caros; había que justificar
el objeto del viaje; era difícil conseguir dos pasajes utilizables
el mismo día. Lo cierto es que Rodrigo llegó solo a la capital
para atender algunos procedimientos de un complicado juicio de
sucesión.

Después de dos días de andar en juzgados se presentó en la librería
de Maclovio. Este discutía con un cliente ocasional sobre el
futuro de la guerra. Rodrigo, al entrar, quedóse en la penumbra
de un rincón, ante un rimero de libros a la rústica. Aparentando
examinarlos dio la espalda a Maclovio, sin saludarlo; en realidad
tenía el oído atento a las palabras de su amigo. Lo escuchaba
con gesto burlón y esperaba quedarse a solas para hacerle, con
su socarronería habitual, una serie de observaciones cáusticas.

Cuando el comprador salió, Rodrigo volvió hacia Maclovio su rostro
iluminado por su gran sonrisa bonachona. Los dos amigos se abrazaron
y comenzaron a hacerse preguntas sobre sus familias. Rodrigo
se enteró de que Guadalupe y Armando seguían sus estudios y a
su vez le habló de Verónica y de sus hijos; de Remigio que seguía
cada día más empeñado en su trabajo de médico, y de Patricio,
que aunque era abogado, no pensaba sino en sus _boy scouts_.

---¿Y qué me dice usted de Alonso Quintanar? ---dijo Rodrigo---.
Es preciso que lo veamos; quiero discutir con él y también con
usted. Tengo alguna esperanza de que el desastre del país los
tenga convertidos.

---A mí me encontrarás tan intransigente como siempre ---replicó
Maclovio---. Estoy dispuesto a insultarte hasta que entres por
el sendero de la decencia. Ojalá la lección te la enseñen mis
palabras, aunque te parezcan amargas y no vayas a aprenderla
por el dolor y la desgracia. En cuanto a Alonso te hablará reposadamente,
pero su pífano toca el mismo son.

---Bueno, bueno, Maclovio ---contestó Rodrigo--- no comencemos
tan pronto. Quiero convidarlos a comer. Sobre el café emprenderemos
la polémica.

---¿Sobre el café, Rodrigo? No seas optimista. En fin voy a telefonear
a Alonso.

Era pasado ya el mediodía y una vez hecha la cita con Quintanar,
Rodrigo y Maclovio se echaron a la calle. La gran avenida frente
a la Alameda veíase solitaria; no transitaban sino bicicletas,
contados automóviles y uno que otro camión de pasajeros repleto
hasta los topes. Algunos carricoches arrastrados por mulas o
caballos hambrientos iban también pletóricos de gentes; en sus
costados, con letras toscas, se veían letreros que anunciaban
el destino: Santa María, San Lázaro, San Rafael; los mismos nombres
celestiales que recordaban la urbe despreocupada y feliz de años
atrás. Con cifras llamativas en rojo o amarillo se consignaba
el valor del pasaje: cincuenta centavos, un peso, según la distancia.
Al cruzar hacia la Alameda un automóvil pasó cerca de ellos con
una velocidad de carretera; Rodrigo y Maclovio alcanzaron a ver
la placa: Poder Legislativo. El último, que nunca pudo refrenar
su lengua, observó:

---Esto me hace sentirme comunista. Desearía vivir en una sociedad
sin clases.

Caminaron a pie, hacia el noreste de la ciudad, rumbo al viejo
barrio de Peralvillo, donde Alonso Quintanar tenía su fábrica.
Llegaron cerca de las dos. Alonso apretó a Rodrigo en sus brazos,
estrechó la mano de Maclovio y les anunció que comerían allí
mismo, con él.

---¿A dónde podríamos ir? ---dijo Quintanar---. Para comer bien
tendríamos que ir a los sitios frecuentados por los políticos,
los banqueros y los judíos especuladores. Eso haría un agujero
considerable en su bolsillo, mi querido notario. Además va contra
mis convicciones pues creo que debemos aceptar sin evadirlos
los sacrificios que nos impone esta guerra. Tengo un poco de
café legítimo, un magnífico piloncillo rubio, frijoles negros
y un buen asado de carne de caballo.

Maclovio encontró esta carne un poco dulzona, pero dio cuenta
de ella. Comieron en un gran salón destartalado, pintado a la
cal; los obreros de Alonso ---unos treinta--- se repartieron
en tres mesas largas. Para celebrar la presencia de sus amigos
el anfitrión hizo abrir unos garrafones de vino, restos de otras
épocas de abundancia y bien pronto reinó una alegría cordial.
Con el café Alonso pasó subrepticiamente a Rodrigo un par de
tabacos. ---Usted es el único que fuma aquí. No los muestre pues
no quiero provocar envidias por esta preferencia; pero como al
fin y al cabo viene usted a vernos tan de tarde en tarde, no
me parece injusta.

Los trabajadores se levantaron para volver a sus tareas y los
tres amigos se quedaron solos.

---Maclovio ---dijo Rodrigo--- lo encuentro extraordinariamente
ecuánime y esto me sorprende. Se ha privado usted de su placer
habitual: insultarme delante de Alonso.

---Ha sido por respeto a esos muchachos ---contestó Maclovio---.
Por eso ves que me he limitado a comentar este menú de guerra
y a preguntarte por la situación en San Miguel. Me alegra saber
que por allá las cosas son más tolerables.

---Y lo serían más aún si no fuese por la testarudez del gobierno
---replicó Rodrigo---. Llevamos cuatro años bien corridos de
estas danzas, sin provecho para nadie. Miseria y hambre. Y esto
lo resienten todos; una tortilla vale cinco centavos, de manera
que ya supondrá usted lo que come una familia con un salario
mínimo de un peso. Le hablo de lo que sucede en Guanajuato.

---A mí no me tacharás de gobiernista ---saltó Maclovio--- todo
menos eso; pero esta miseria de que hablas no es el resultado
directo de la guerra. Se debe a una política económica desacertada;
se debe también a la abundancia de dólares.

---Es decir ---interrumpió Rodrigo--- a nuestra sumisión a los
yanquis.

---Calma, calma, Rodrigo ---dijo Alonso---. Es preciso ir a la
médula de las cosas. Estamos defendiendo una causa que tiene
más importancia que el precio de las tortillas y la escasez transitoria
del calzado. Esto podrá remediarse en un mundo pacífico con un
poco de sensatez por parte de los hombres, pero lo que duraría
siglos sería la degradación del alma humana si triunfasen los
nazis; Hitler mismo ha dicho que está cimentando el poderío milenario
de Alemania. Es decir, mil años de opresión, de seres deformados
mentalmente desde la infancia, de imperio de la violencia, de
racismo ario. Mil años de _Mein Kampf_, mi querido Rodrigo. En
esta amenaza nada tienen que ver los Estados Unidos; existe y
es preciso defenderse de ella. Eso es todo.

---Pero Alonso ---respondió Rodrigo Guerrero--- usted no destruye
mis argumentos. He dicho que la miseria y el hambre son el cortejo
de la guerra.

---Tiene usted razón; pero es que hacemos la guerra para librarnos
de cosas peores que la miseria y el hambre. Usted debe comprender
esto porque, como católico, tiene que ser espiritualista.

---¡Ay, Alonso! Se me está usted volviendo retórico ---replicó
Rodrigo---. Eso es retórica; y se lo digo sin creer que con esto
falto a mi credo; soy católico, pero mis creencias nada tienen
que ver con su propaganda judaizante.

---Por Dios, Rodrigo ---exclamó Maclovio con exaltación---. Sé
que no eres imbécil y tampoco un malvado. Me devano los sesos
y no sé cómo clasificarte.

---Amigos míos ---concluyó Alonso--- dejemos esta discusión.
No nos llevará a nada. Conformémonos con seguir siendo en lo
futuro, como siempre lo hemos sido, gentes sinceras y amantes
del bien, aunque por vías diferentes. ---Ahora les voy a mostrar
algunas cosas de esta pequeña fábrica.

Se levantaron y comenzaron a recorrer los locales pletóricos
de muñecos, payasos y fieras. Alonso alegaba que su único mérito
consistía en perfeccionar los juguetes populares haciéndolos
de una materia mejor y poniendo más devoción en el acabado. Pero
carecía del ansia del lucro y una vez que vio satisfechas sus
necesidades y las de sus obreros desistió de seguir aumentando
su producción. Cuando un trabajador creaba un nuevo juguete que
halagaba el sentido estético de Alonso, este le hacía un regalo
de un precio exorbitante; o si la Divina Providencia mandaba
a uno de esos hogares un par de mellizos, los dotaba de ropa
para años enteros. Los niños iban a buenas escuelas y los enfermos
a los mejores hospitales, sobre todo si su vida dependía de la
intervención de un cirujano de fama.

---Pero dígame, Alonso ---interrogó Rodrigo--- ¿se venden juguetes
ahora?

---Nadie lo creería, ---contestó Quintanar--- pero se venden
y quizás más que antes. Esto es para mí una cosa consoladora;
la humanidad, después de todo, es mejor de lo que parece. Mientras
los hombres se preocupen por hacer felices a los niños se puede
conservar la esperanza.

---Y he notado ---observó Maclovio--- que usted prescinde aquí
de toda la parafernalia bélica. No veo fusiles, ni soldados,
ni tanques.

---Es cierto; ---contestó Alonso--- no quiero contribuir al envenenamiento
de las almas infantiles. Nunca he fabricado un juguete que sugiera
ideas de violencia o de muerte. Solo por razones muy poderosas,
que no puedo prever, abandonaría esta línea de conducta.

Continuaron la visita a la fábrica. Rodrigo salió de allí prometiendo
regresar antes de su vuelta a San Miguel y con un cargamento
de chucherías para su nieto, el hijo de Patricio, que vivía en
México en el otro extremo de la ciudad.

</section>
<section epub:type="chapter" role="doc-chapter">

# VI

++Entonces++ los acontecimientos se precipitaron. Se intensificó
la agitación política. Los partidos de oposición lanzaron la
candidatura del ingeniero Cabral; los grupos gobiernistas anunciaron
que no hablarían de candidatos sino hasta principios de 1946.
Comenzaron a presenciarse hechos alarmantes: en un pueblo de
los Altos, en Jalisco, y en Irapuato, dos manifestaciones políticas
habían dado un saldo considerable de muertos y heridos. Pero
hubo víctimas en los dos bandos, rompiendo así la tradición de
que, en estas coyunturas, pertenecían siempre al grupo desafecto
al gobierno. Casi simultáneamente, en ambos lugares, los oposicionistas
tomaron una actitud subversiva y se hicieron fuertes con ametralladoras
portátiles; aquello parecía el principio de una lucha civil.

El gobierno, para no dar el espectáculo triste de una reyerta
interna que daría al traste con la unidad nacional cuando el
continente entero se enfrentaba a problemas tan arduos, pactó
con sus enemigos en la forma que le pareció mejor: aparentando
ignorar la gravedad del desafío.

En Aguascalientes, mientras Cabral hacía un discurso en el quiosco
de la plaza, un aeroplano sin matrícula, cuyo campo de aterrizaje
fue siempre un misterio, arrojó sobre la ciudad miríadas de volantes
que decían con grandes letras: «Paz» y otros: «Mexicanos, el
ingeniero Cabral nos dará una paz honrosa».

Comenzó a perderse la seguridad pública y para fines de 1945
era indudable que operaba en el país una vasta organización de
saboteadores nazistas. Los descarrilamientos se hicieron más
frecuentes que nunca y cuando el gobierno hizo un llamamiento
público al patriotismo de los ferrocarrileros, publicando planas
enteras en los diarios, que eran como un S.O.S. de la república
atacada de arterioesclerosis, ellos contestaron que no podían
traicionar a la revolución.

En Guadalajara se produjo un motín por la carestía de los granos
y el populacho saqueó el mercado y los principales comercios
a la luz del día. Grupos numerosos de gentes continuaron recorriendo
las calles durante la tarde y al oscurecer cayeron como langostas
sobre las residencias de los gobernadores que se habían enriquecido
chupando la sangre de los tapatíos. Al desvanecerse la última
claridad del sol, grandes piras, alimentadas con tapetes y muebles
costosos, alzaban sus flamas por encima de las copas de los árboles.

Era indudable que las quintas columnas nazistas que durante años
operaron en México, habían envenenado el espíritu público. En
la misma capital una procesión de más de mil mujeres, Cada una
con un chiquillo en los brazos, se presentó en el Palacio Nacional.
A gritos pedían leche para sus hijos. El Presidente decidió recibirlas;
las congregó en uno de los patios y desde un corredor dirigióles
algunas palabras reposadas de confianza en el porvenir. Pero
mientras hablaba se escucharon voces que salían de aquella multitud
andrajosa: pedimos pan. Al final una hembra de aspecto feroz,
con los cabellos blancos sobre el rostro y que cargaba una criatura
de pecho, clamó con tono destemplado: queremos paz y comida.

Cuando el jefe del Estado se retiró a sus oficinas, un murmullo,
primero suave y que después fue creciendo como cuando uno se
acerca a la mar bravía o como el ruido que va aumentando de una
flota de aviones, levantóse de aquella multitud, y al cabo de
unos minutos se convirtió en una algazara de la que partían dichos
procaces.

Estos acontecimientos ocurrían por docenas y diariamente por
doquiera y era indudable que vivíamos sobre un fondo de inquietud
y de expectación angustiosa. Por otro lado las noticias presagiaban
nuevas catástrofes. Hasta que una mañana los periódicos anunciaron
que seis aviones alemanes habían arrojado unas bombas sobre Nueva
York. Las seis máquinas fueron abatidas por las defensas americanas
y en realidad los perjuicios que causaron fueron insignificantes,
pero una de las bombas destruyó una gran casa de apartamientos
en Park Avenue.

Los mexicanos principiamos a gustar el mismo sabor amargo, pues
unos cuantos aeroplanos japoneses volaron impunemente sobre Acapulco
bombardeando las playas y los hoteles de turistas. Como ese puerto
estaba indefenso todos los proyectiles dieron en el blanco y
la matanza de civiles fue espantosa. Cuando la provisión de bombas
se agotó los pilotos ametrallaron las calles y arrojaron también
miles y miles de volantes prediciendo la ruina y la destrucción
a menos que se firmase una paz inmediata con el Eje.

Mientras tanto los nazis hicieron llegar a diversos puertos grandes
cantidades de armas destinadas a los grupos opositores al gobierno.
Más tarde, cuando los boches alardearon de sus hazañas, se supo
que enormes submarinos de transporte, provistos de aeroplanos,
trajeron a nuestras costas esos elementos de guerra y también,
para organizar a los descontentos, a oficiales que hablando a
la perfección el inglés se hacían pasar como americanos.

Los japoneses residentes en el país invadieron los Estados de
Nayarit y Sinaloa. Pretextaban fomentar la producción agrícola
y protestaron su lealtad al Gobierno; pero en realidad se hicieron
fuertes en puntos estratégicos y pusieron en jaque las comunicaciones
de todo el Occidente. Se pertrecharon con armas que llegaban
de lugares ocultos y sus campamentos se convirtieron en verdaderas
fortalezas.

Las relaciones de estos hechos las he recogido aquí y allá y
como es natural no se concadenan para formar un todo y dar la
impresión vívida de las peripecias de esos meses. Además si me
pusiese a referir lo que cuentan muchas gentes acerca de lo que
pasaron y presenciaron llenaría, con eso solo, cientos de cuartillas.
Yo era un chiquillo, pero recuerdo que mi escuela estuvo clausurada
por una epidemia de tifoidea la que según se descubrió al fin,
debióse a una contaminación de los tanques de agua; se pretendía
así minar la moral colectiva. Entonces nos escapábamos de nuestras
casas y en pandillas de ocho o diez recorríamos la ciudad; en
las calles solitarias nos poníamos a jugar al burro sin ser molestados
por nadie. Andábamos hambrientos por la severa dieta a que nos
condenaba la escasez y organizábamos incursiones predatorias
a los lugares donde había víveres, abusando de la impunidad de
nuestros años. Cuando teníamos suerte nos refugiábamos en los
jardines públicos, principalmente el de San Martín, y ocultos
en el follaje de los arbustos nos repartíamos el producto de
nuestras rapiñas. En una ocasión logramos robarnos una cabra
que llevamos a esconder cerca del bosque de Chapultepec a unos
lotes vacíos bardeados con altos paredones. Allí ordeñábamos
el animalito alimentándolo como Dios nos daba a entender. De
este modo bebíamos cada uno un buen vaso de leche al día; pero
una noche la cabra desapareció misteriosamente.

En nuestras casas todo era escaso y malo. Mi madre me daba, una
vez por semana, un pequeño trozo de jabón que apenas me alcanzaba
para un baño. Los demás días yo me echaba la ducha y me restregaba
el cuerpo con las manos hasta que poco a poco fui perdiendo el
buen hábito de la ablución cotidiana. Cuando se hizo casi imposible
obtener zapatos llevábamos huaraches con suela de hule, que se
obtenía de los desperdicios de neumáticos y tubos de automóviles.
Aprendimos pronto a no usar ese calzado sino en las circunstancias
más necesarias y generalmente lo guardábamos al llegar a nuestras
casas. Estos sacrificios los soportábamos con la alegría de la
niñez; pero a nuestros padres los veíamos preocupados y con los
ceños fruncidos. El mío, ingeniero consultor en un banco, llegaba
cada quince días con su paga; recuerdo bien cómo extraía de sus
bolsillos los billetes arrugados que con un gesto brusco entregaba
a mi madre. Esta, que era el espíritu mismo de la economía, los
contaba y extendía haciendo sus planes para invertirlos inmediatamente,
antes de que cambiasen de valor. Mi padre había abandonado su
indumentaria de universitario de la clase media y usaba unos
pantalones de pana con una chaqueta de dril azul; llevaba al
aire, sin sombrero, su cabellera gris.

Es una gran cosa que la memoria sea tan frágil y también que
las inquietudes no tengan cabida en los cerebros de los niños.
Esos tiempos tan duros no me dejaron lacra física ni mental,
y además casi los he olvidado, de tal manera que para refrescar
mi memoria tengo que apelar a charlas con los viejos. Lo que
sé es que nos ingeniábamos para completar el magro menú doméstico
y generalmente nuestras pancitas se sentían satisfechas; pero
esta situación era el privilegio de los chamacos entre los diez
y los quince años, pues los más pequeños no tenían el vigor físico
necesario para nuestras correrías y los ya mayores, si tomaban
parte en ellas, corrían el riesgo de aparecer como delincuentes.
En cuanto a las mujercitas, de cualquier edad, no las admitíamos
como compañeras de aventuras.

Por mi parte recuerdo aquellas épocas como un largo tiempo al
margen de la autoridad paterna, sin obligaciones precisas, sin
lecciones qué aprender ni estrictas prácticas de aseo que observar.
Pero cuando llegaron los alemanes y los japoneses nos sentimos
maduros de la noche a la mañana y aprendimos a odiarlos como
si fuésemos hombres hechos.

El ingeniero Berrueta me ha relatado muchos sucesos de entonces.
Según él los partidos oposicionistas de tendencias nazis obraban
en absoluto acuerdo con las quintas columnas pagadas por Hitler.
Hace observar que eso no significaba que la oposición estuviera
confinada en esos partidos que indudablemente representaban solo
una minoría despreciable, si es que algo representaban. Según
él la oposición era la mayoría del país, como lo fue cuando el
presidente Calles emprendió la estéril querella religiosa, o
como cuando se implantó una ley llamada Artículo 3% que Berrueta
califica de estulta. A principios de 1946 también la mayoría
del país era oposicionista, porque estábamos cansados de tanto
latrocinio, tanta burocracia y tanta incapacidad. Sin embargo,
esa opinión pública había renunciado a organizarse y guiada por
un patriotismo inmanente habíase resuelto no provocar disensiones
en momentos tan graves.

Pero convenía a los politicastros hacer creer que esos pequeños
grupos cuyo portavoz era el periódico del Doctor Siddartha representaban
la oposición genuina del país. Por eso cuando un hombre constituía
un estorbo para el bienestar de aquella mafia corrompida, se
le llamaba fascista. Anteriormente, durante los veinticinco años
que precedieron a estos acontecimientos, el apelativo era otro:
se les calificaba de reaccionarios, pero como esta palabreja
se aplicaba en general a ciudadanos inteligentes, los hombres
sensatos llegaron a considerarla como un elogio, a tal punto
que perdió su significado prístino para adquirir otro circunstancial.

Cuando esos mismos hombres fueron llamados fascistas o nazistas
protestaron con tremenda energía considerando el término infamante.
No quiero adelantar los acontecimientos de esta historia, pero
Berrueta dice que esas maniobras calumniosas fueron sobre todo
empleadas por los funcionarios que huyeron a los Estados Unidos
durante la invasión y que pretendían representar la médula del
pueblo de México. Llamaban nazistas a muchos patriotas por el
solo hecho de haberse quedado en el territorio invadido; mas
la heroica muerte de un gran número de estos últimos vino a demostrar
la mendacidad de estas aseveraciones. Aquí se jugaban la vida
todos los días mientras sus detractores intrigaban en Washington
preparando su regreso al país.

Todo esto yo mismo lo encuentro confuso, pero es lo que el ingeniero
Héctor Berrueta ha procurado explicarme muchas veces. Quizás
mi interpretación es errónea.

Los sucesos posteriores pertenecen a la Historia. Una flotilla
enorme de submarinos ---se calcula que de varios miles---, atacó
las costas brasileñas. A pesar de la tenaz resistencia de sus
habitantes, la sorpresa los aplastó. La invasión derramóse hasta
Panamá y la destrucción del canal por un gigantesco torpedo que
cargaba miles de toneladas de explosivos y que según se dijo
era guiado por una tripulación suicida, fue un golpe que parecía
mortal para las naciones unidas. El Tío Sam se replegó violentamente
a su territorio y comenzó a prepararse febrilmente para la defensa.

Los alemanes invadieron Centro América hasta la frontera de Guatemala
con Honduras y Salvador, donde tomaron unos días de tregua. Aquello
recordaba la primavera de 1940, cuando ocuparon los Países Bajos
y Francia.

El 9 de febrero de 1946 los habitantes de la metrópoli que vivían
presas del sobresalto y la curiosidad, comenzaron a escuchar
en la madrugada un ruido sordo de bombardeos. Muchos abandonaron
el lecho y subieron a las azoteas; en el aire frío y claro el
estrépito de las explosiones se hacía más retumbante y llenaba
el espacio; como a las siete se escuchó el ronronear lejano de
aviones. El conserje del edificio ocupado entonces por los ferrocarriles
en la esquina del Paseo de la Reforma y la calle del Ejido, trepó
hasta la terraza más alta y pudo contemplar centenares de máquinas
que llegaban por el sur; procuró contarlas, pero su vista no
podía agruparlas para hacer un cálculo rápido. Indudablemente
pasaban de quinientas. Cuando se hicieron perfectamente visibles
empezaron a soltar de sus barrigas bultitos oscuros que se precipitaban
al vacío y que de pronto flotaban colgados de sombrillas que
se abrían como gigantescas flores blancas. Poco a poco esos bultos
iban adquiriendo forma humana y se discernían, en la claridad
del ambiente, las cabezas, las piernas y los brazos. Mientras
tanto de casas desparramadas en muchos rumbos de la población
salían grupos muy numerosos con toda clase de armas y se dirigían
hacia el Palacio Nacional y hacia los cuarteles. En las afueras,
donde los paracaidistas aterrizaban, muchos vehículos estaban
en su espera para transportarlos al centro. Las oficinas de los
partidos de oposición se erizaron de ametralladoras y las bocacalles
se llenaron de patrullas armadas.

Simultáneamente comenzaron a escucharse tiroteos por rumbos muy
diversos. El vecindario temeroso y expectante al mismo tiempo
no sabía a ciencia cierta lo que pasaba; pero como a las ocho
una columna de populacho, de cuatro o cinco mil hombres, recorrió
el Paseo de la Reforma gritando vivas al ingeniero Cabral y mueras
al gobierno; otros grupos llevaban grandes cartelones con esta
sola palabra: «Paz».

Los elementos leales organizaron la resistencia en diversos sitios:
en el cuartel de Peralvillo, en el palacio nacional, en la ciudadela,
en Chapultepec y comenzó la lucha por posesionarse de las avenidas
más estratégicas.

En el aeródromo de Balbuena unos treinta o cuarenta aviones se
elevaron para librar una batalla suicida y heroica; pero uno
a uno fueron cayendo. Hicieron, sin embargo, bastantes bajas
entre los enemigos y uno de los bombarderos invasores cayó panza
arriba en la Alameda, mostrando las alas con las insignias nazis
y provocando un gran incendio de árboles que crepitaban con el
fuego.

Ese mismo día, en las primeras horas de la tarde, una radio gobiernista,
que seguía transmitiendo, anunció que un transporte enemigo había
logrado atracar en Veracruz rodeado por centenares de submarinos
que lo escoltaban como delfines gigantescos. El pueblo y los
muchachos conscriptos del puerto trataron de hacer resistencia,
pero no existía una sola pieza ni un asomo de preparación y todo
terminó en una carnicería de héroes. Entonces los invasores organizaron
columnas de tanques y hombres motorizados que se dirigieron sobre
México. Al día siguiente, que era domingo, estaban ya sobre el
camino a sesenta kilómetros de las goteras.

Los grupos facciosos se apoderaron fácilmente de Tampico y Acapulco.
En esta última bahía desembarcó también una fuerte columna alemana.
Varios transportes cayeron sobre Puerto México y los invasores
avanzaron a través del Istmo; traían plétora de cañones, tanques,
locomotoras y carros. En esa ruta las tropas del gobierno se
batieron con bravura ejemplar, pero nada podían contra hombres
veteranos de la guerra en Europa, equipados con el armamento
más moderno.

Mientras tanto las fuerzas nazis habían atravesado Guatemala,
donde se les hizo una oposición desesperada, y se internaban
por nuestro territorio.

Los japoneses que juraron lealtad al país se convirtieron de
pronto en una fuerte columna que se avalanzó contra Guadalajara.
Otros saltaron desde las costas de Sonora, a través del Golfo
de Cortés, sobre la Baja California, mientras varios centenares
de submarinos nipones hacían un desembarco por la costa del Pacífico.

Estos hechos no forman propiamente parte de mi relato, que debería
ceñirse a unas cuantas gentes unidas por una trama casi familiar.
Pero no puedo desprender a mis protagonistas del cuadro de la
invasión y de la guerra que dominaba y regía los intereses, las
pasiones y hasta las circunstancias más nimias en las vidas de
cada quien.

El recuerdo que guardo de esas semanas es que los vecinos se
sentían poseídos por el pánico y la sorpresa. Mis padres se encerraron
en nuestra pequeña vivienda y a mí me encerraron con ellos mientras
en las calles ocurrían escaramuzas y sobre nuestras cabezas se
escuchaba el zumbido de los aviones. Los periódicos dejaron de
publicarse y durante muchos días conocíamos lo ocurrido por los
rumores que se propalaban de una calle a otra. Nos dábamos cuenta
de los progresos nazis por las radiodifusoras que al principio
transmitían mensajes llenos de esperanza para después irse callando
una a una. Entonces comenzaron a difundir discursos hablando
de la caída del gobierno y de la formación de una asamblea que
debería elegir a un presidente provisional.

El Presidente legítimo, con su ministro de la guerra y unos tres
o cuatro mil hombres se defendían obstinadamente en el viejo
convento del Carmen en San Angel. Finalmente, ante el empuje
enemigo, tuvieron que echarse a pie de noche por los pedregales
buscando una salida hacia la serranía del Ajusco; fueron alcanzados
y conminados para rendirse, pero ellos contestaron con el fuego
de sus ametralladoras. Las tropas de invasores los cercaron al
fin y los exterminaron uno a uno, sin hacer un solo prisionero.
Así murió un presidente que aunque escaló el poder por el fraude
electoral en contra de la opinión casi unánime del país, se hizo
respetable después por sus virtudes de hombre de bien, tan escasas
entonces entre los políticos. Todavía ahora los textos de historia
hablan de él llamándolo El Bueno, y con este adjetivo, en el
cual no hay ni una sombra de ironía, rinden un tributo a su memoria.
Fue un hombre bueno y murió como un valiente y un patriota. ¡Cuántos
habrían querido hacer otro tanto!

Lo anterior ocurrió con la rapidez de un relámpago; era en realidad
la misma _blitzkrieg_ de Bélgica, de Francia, de Grecia. Para
el 19 de febrero las tropas que se llamaban a sí mismas del Nuevo
Gobierno, pero que en realidad estaban formadas por nazis invasores,
desfilaron por la capital. Cantidades fantásticas de armamentos,
que parecían haber bajado del cielo por milagro, llegaban en
flotillas interminables de camiones que como líneas puntuadas
rayaban los caminos por distancias inmensas. Para mediados de
marzo los nazis habían consolidado sus posiciones hasta el norte
de la república. Los japoneses dominaban todo el Occidente. Algunos
jefes militares, aplastados por la rudeza del ataque, se rindieron;
unos cuantos, nazistas de corazón, hicieron proclamas y alegando
que el bien del país lo representaba el gobierno recién constituído,
entraron a colaborar con el Nuevo Orden.

El resto de nuestro ejército, entre ellos un gran número de muchachos
que se batieron con denuedo, se salvó replegándose hasta los
jirones del territorio que el enemigo no pudo invadir. Entre
esos hombres se conservó siempre intacta la esperanza de recuperar
un día la patria perdida.

Los alemanes obraron con gran sagacidad y cuando se presentaron
en la metrópoli con lujo de aeroplanos, paracaidistas, tanques
y ametralladoras, hacían la comedia de que venían a prestar su
ayuda a un movimiento popular que restauraba la dignidad del
país sojuzgado por el poderío americano. Así, la lucha en México
se enmascaró con la careta de la guerra civil.

Al principio muchos hombres de buena fe, que ante el conflicto
habían perdido el sentido de la proporción, cayeron en esa trampa
hábilmente tendida. Más tarde, cuando el lobo disfrazado con
la piel del cordero empezó a gruñir y a mostrar los colmillos,
casi todos esos hombres dieron un paso atrás, arrostrando el
peligro y aún la muerte, y se unieron a los que luchaban por
arrojar al nazi. La invasión fue una prueba dura que a la postre
tuvimos que bendecir. Bajo la bota nazi México inició su primera
hora de pueblo libre y unido. Desde ese momento las gentes comenzaron
a luchar y a morir por algo más alto que las engañifas de los
líderes.

Se reunió una gran asamblea y fue electo presidente provisional
Onésimo Cabañas. Existía el plan de convocar a una elección definitiva;
pero estos sueños nunca llegaron a realizarse. Cabañas fue el
primero y el último de la Dinastía de los _Quislings_ Mexicanos.

</section>
<section epub:type="chapter" role="doc-chapter">

# VII

++Pues++ digas lo que digas ---dijo Maclovio exaltado--- si este
gobierno se consolida será la perdición de México; porque no
solo nos borrarán del mapa convirtiéndonos en un feudo teutón,
sino que aniquilarán nuestra esencia misma: nuestra hispanidad,
nuestra catolicidad, nuestra latinidad en una palabra. Y te lo
digo ahora aquí, a gritos, apostrofándote y llamándote imbécil,
en primer lugar porque te quiero como a un hijo; pero además
porque estoy haciendo uso de los últimos vestigios de libertad
que le quedan a este pobre país. Dentro de uno, dos o seis meses,
cuando estas promesas almibaradas que hacen ahora los alemanes
se hayan desvanecido, me fusilarán por la mitad de lo que ahora
estoy diciendo.

Mientras así vociferaba recorría a grandes trancos el corredor.
Sus brazos parecían las aspas de un molino; sus ojos brillaban
tras los espejuelos.

Don Rodrigo, en actitud beatífica y repantigado en un sillón,
escuchaba a Maclovio Otamendi con evidente placidez, fumando
un tabaco. Sacudía las cenizas con pulcritud. Con voz blanda
interrumpió a su amigo:

---Maclovio; usted sabe que en mi casa siempre podrá decir lo
que guste. Esta es una casa de gentes civilizadas, y al decir
civilizadas también quiero decir libres. Usted no piensa como
yo y eso es todo. Y conste que sin ser demócrata, sino simpatizador
del nuevo régimen, no lo insulto a usted y en cambio me dejo
insultar.

---Mira, Rodrigo; los tiempos no están para bromas. No tienes
derecho a insinuar que soy demócrata. Estoy con los Estados Unidos
e Inglaterra y aún a mis años pienso seriamente en tomar un fusil,
pero no soy demócrata.

Esto lo decía con una actitud un poco más tranquila, como dispuesto
a debatir. Y continuó: ---Además, tú no me insultas en voz alta,
pero dices para tu coleto: ¡oigan a este viejo idiota!--- Después
volvió a gritar, sacudiendo las mechas con el movimiento de cabeza
que le era habitual:

---Eres tan ciego que me estás ofreciendo tu casa como un refugio
sagrado para que despotrique a mi sabor. Pues bien, Rodrigo;
estás ofreciendo lo que ya casi no tienes. Con los alemanes no
tendrás casa, ni podrás decir dentro de ella lo que te venga
en gana; las paredes tendrán oídos y estas mismas paredes te
delatarán.

---Voy a hablarle en serio Maclovio ---dijo Rodrigo---. Mi casa
será siempre mi casa: el hogar de mi mujer y de mis hijos; el
_sancta-sanctorum_ de nuestros afectos, de nuestras emociones,
de nuestras ideas. A él solo entran mis amigos, como ustedes.
Eso nada tiene que ver con los presidentes que van y vienen,
ni siquiera con las guerras, aunque sean mundiales.

---Que Dios te conserve esa confianza, Rodrigo. ¡Qué más puedo
desear para ustedes: para ti y Verónica y para tus muchachos!
Paz para ti y los tuyos.

Y en seguida agregó exaltado: ---Pero eres un idiota y no sabes
lo que dices. Ha sido la práctica alemana profanar esos santuarios
en todos los países que ocupan.

Alonso Quintanar que había sido hasta ese momento testigo mudo
de la escena, terció entonces:

---No nos salgamos del tema; yo opino lo mismo que Maclovio,
aunque sea por razones ligeramente diferentes. Lo que pasa ahora
aquí es lo mismo que ha pasado en Europa; en Noruega, digamos.
Nada más que nuestro _Quisling_ ha sido quizás más sumiso. Pero
el establecimiento del orden nazi en nuestra patria significa
el sacrificio de la libertad; y ya le he dicho muchas veces,
Rodrigo, lo que entiendo por libertad: la libertad de forjar
nuestra propia vida.

Don Rodrigo se dirigió entonces a Alonso:

---Con usted discuto más a mis anchas porque no me increpa al
estilo de Maclovio. Yo lo que le sé decir es que esa libertad
la tendremos siempre. No sean ustedes escépticos; piensen que
hemos disfrutado de ella hasta viviendo bajo la égida de los
Estados Unidos.

---Mira, Rodrigo, ojalá no llegue el día en que le pidas a Dios
que los americanos te protejan ---arguyó Maclovio---. Yo lo único
que sé decirte es que si en lugar de ser ellos nuestros vecinos
lo hubiesen sido los alemanes, México no existiría: se lo habrían
engullido en calidad de espacio vital, y tú y yo y todo este
pueblo donde hay gentes tan buenas, estaríamos trabajando para
nuestros conquistadores arios en calidad de ejemplares despreciables
de una raza inferior. Y eso será el fin de esta aventura si Dios
no nos tiende su mano.

Esta conversación tenía lugar en la casa del licenciado Rodrigo
Guerrero, en San Miguel, a mediados de marzo de 1946.

Al apoderarse los nazis de la ciudad de México e instalar a Onésimo
Cabañas en el Palacio, como Presidente de paja, quisieron dar
la impresión de que nada se rompía en el orden legal y también
de que la vida cotidiana de la población seguiría su curso acostumbrado.
Pero muchas gentes abandonaron la Metrópoli y entre ellas Alonso
Quintanar y Maclovio Otamendi. Encontraron congestionada la carretera
a Querétaro, pues centenares de coches se dirigían al norte,
no precisamente con el propósito de huir de una invasión que
era ya un hecho consumado, sino porque consideraban que en la
provincia los riesgos que se corrían eran menores.

Este tráfico intenso estorbaba seriamente las operaciones bélicas
de los nazis, pero como no tenían de momento ningún problema
militar grave prefirieron crear una apariencia de normalidad
inspirando confianza en el nuevo gobierno pelele. Maclovio y
Alonso llegaron a San Miguel después de casi tres jornadas de
camino en las que recorrieron los trescientos kilómetros que
median entre ese pueblo y la Capital de la república. Tuvieron
que dormir dos noches dentro de su Ford desvencijado; la primera
en Tepeji del Río y la segunda en Querétaro; desde este lugar
encontraron la carretera más expedita, pues el grueso de la migración
siguió hacia el bajío, dirigiéndose por el rumbo de Celaya e
Irapuato sobre la línea troncal de Ciudad Juárez.

Pasaron unos veinte días en la casa de Rodrigo Guerrero, que
veían como suya. Con Otamendi llegaron sus hijos Guadalupe y
Armando. Todos encontraron un buen dormir y buen yantar en aquella
casa no muy espaciosa, pero donde la hospitalidad era ancha y
cobijadora.

Quintanar y Otamendi discutieron sus situaciones. El primero
tenía sesenta y cuatro o sesenta y cinco años. Decidió volver
a México y participar en la lucha. Se dedicó a la confección
de artificios de sabotaje, pero en lugar de morir como un héroe,
murió más tarde en su cama, de un tifo fulminante.

Maclovio, que tenía muy pasada la cincuentena se entregó a actividades
de agitación, pero decir lo que hizo y cómo lo hizo y también
lo que hicieron Armando y Guadalupe, son cosas que tengo, para
seguir un cierto orden, que colocar en otra parte de mi relato.

Mientras tanto la existencia se desarrolló con ciertos visos
de orden. Es cierto que por breves días los periódicos dejaron
de llegar, pues al parecer no se arreglaba todavía la publicación
de otros nuevos que confeccionaran las noticias para beneficio
de los invasores; pero en cambio estos hicieron difusiones continuas,
utilizando las transmisoras confiscadas. Los locutores, con pronunciación
gutural la mayor parte de las veces, hablaban de las excelencias
del nuevo régimen y hacían hincapié sobre los términos en que
Cabañas se había dirigido al pueblo a raíz de su toma de posesión
y también encomiaban las frases lacónicas con las que el Barón
Von Virchow había respaldado ese mensaje. Esas cosas las escuchaban
en San Miguel todos los miembros de la casa ---incluso los criados---
reunidos en la gran estancia, con excepción de Patricio que vivía
en México.

Hubo, sin embargo, durante esos días, un incidente que puso la
primera nube en el azul optimismo de Rodrigo Guerrero. Cierta
mañana recibió una comunicación del Cuartel General de las fuerzas
nazis en San Miguel; venía firmada por su jefe, un Coronel Alejandro
Körner. En ella se ordenaba a Don Rodrigo que a las doce se presentase
en el Cuartel ubicado en el Convento de las Monjas; el objeto
era instruir a los citados sobre determinaciones importantes
que era menester cumplir. El documento estaba redactado en correcto
español oficinesco, pero con expresiones ayunas de cortesía:
«se servirá usted», «sin excusa ni pretexto», «severas sanciones»
y otras. Allí aparecía la lista de todos los notificados; quince
o veinte: aparte de Rodrigo, Don Estanislao Gutiérrez, el Doctor
Bernáldez, Don Aurelio Manterola, Nicanor Aceves y algunos más.
Se veía bien que el jefe invasor se había dirigido a lo más conspicuo
del vecindario.

Rodrigo sintió una molestia desagradable en su amor propio. Después
de todo Körner no representaba ninguna potestad; había ya una
civil, la del presidente de la Junta Directora, y aunque consideraba
a Melquiades de la Peña como un mentecato, su espíritu de jurista
lo investía con una autoridad que era respetable; en cambio,
le humillaba tener que obedecer a la soldadesca. Sin embargo,
meditando el caso pensó que la situación era anormal y que además
a De la Peña no podía pedírsele entereza para defender sus derechos
ni al coronel teutón finura de inteligencia para comprender dónde
principia el agravio a la dignidad de un hombre.

Sus deseos de cooperación y su sentido de orden se sobrepusieron
a su orgullo herido y decidió acudir al llamado sin demandar
explicaciones.

Cuando llegó al convento estaban ya allí los demás; Nicanor Aceves
bromeaba haciendo comentarios sobre la parafernalia guerrera
del ambiente: cascos, ametralladoras, cajas de municiones, motocicletas,
sables; era un conjunto abigarrado y pintoresco. Se les hizo
pasar entonces a una antesala desnuda y allí, de pie, esperaron
un rato largo hasta que la puerta del fondo abrióse y apareció
el Coronel Alejandro Körner: alto, macizo, con uniforme de campaña,
con el compás abierto sobre sus botas negras. Su cráneo pelado
surgía de un cuello apoplético; sus ojillos azules, inexpresivos,
se hundían en promontorios de carne rubicunda. Körner se dirigió
al grupo sin circunloquios; en su rostro había un desagradable
gesto agresivo. A su lado un oficial imberbe, que hablaba español
a la perfección con un inconfundible acento mexicano, traducía.

Körner hizo una serie de advertencias amenazadoras: que se estaba
en estado de guerra y que en consecuencia el interés de la población
civil quedaba supeditado a las necesidades militares; que los
agricultores deberían continuar sus cultivos y que si los campesinos
se rehusaban a trabajar, usaría la fuerza; que obligaría al vecindario
a comportarse con un orden estricto. Así habló por unos minutos
para terminar anunciando que de ser necesario publicaría una
serie de disposiciones de carácter policial y que los allí presentes,
como representativos del pueblo, deberían hacer que se respetaran
y serían considerados como responsables por cualquier desobediencia.

Rodrigo salió silencioso y profundamente resentido. Le había
chocado el fondo de la amonestación, el tono, el vocabulario.
Le había asqueado el aspecto del hombre, su gesto, su actitud
deliberadamente brutal. En el fondo su humorismo se sobreponía
a estos sentimientos de repugnancia y Körner se le antojaba un
ogro que intimida a un grupo de niños atemorizados; sonreía a
su pesar pensando que probablemente allá, en Múnich o en Berlín
o en Colonia, Frau Körner lo apostrofaba y le gritaba ---¡viejo
idiota!--- mientras él, con las babuchas puestas y en bata leía
el periódico junto a la chimenea. Pero estos pensamientos no
impidieron que llegase a su casa sombrío y malhumorado.

Alonso Quintanar y Maclovio Herrera, este último con Guadalupe
y Armando, partieron para México; Ana volvió a León con su marido;
Remigio pasaba en Guanajuato la mayor parte de su tiempo, pues
su práctica de hospital lo absorbía. Las noticias de Patricio
eran escasas. Don Rodrigo y Verónica se sentían un poco abandonados,
pero ellos sabían que eso era natural y se reconfortaban con
su mutuo cariño. Verónica pensaba que nada podría destruir los
lazos que ella había atado entre todos, lazos que se entretejían
con nudos tan embrollados que nada podría deshacerlos. Cada uno
estaba ligado a los demás y así habíase creado, en el correr
de los años, una red de pensamientos, emociones y afectos comunes.
Verónica pensaba que la ausencia era solo un accidente en esa
vida familiar; lejos o cerca cada uno de ellos era un pedazo
del gran espíritu de la casa.

Leía con ansia las cartas de los hijos, pero cuando no llegaban
o cuando tardaban, nunca supuso que eso debilitara o destruyera
la gran corriente de amor que los unía. Las exigencias cotidianas
los separaban; es cierto. Uno en México, el otro soñaba con ir
a trabajar a uno de los grandes hospitales americanos; la hija
hacía un hogar con un hombre afectuoso y trabajador. Todo eso
estaba en el buen orden de las cosas: tenían ambiciones nobles
y querían realizarlas; era lo justo. Nadie heredaría la notaria
del Licenciado Guerrero que les había dado bienestar y había
permitido educarlos como gentes de bien. Patricio era el avocado
a ella, pero había preferido correr el mundo entregado a sus
_boy scouts_; con frecuencia, antes de la ocupación, el «Excelsior»
le traía noticias gratas; cada domingo venía una sección entera
dedicada a los muchachos mexicanos y dirigida por Patricio y
ella la leía con un orgullo inocente que la hacía sonreír, como
burlándose un poco de sí misma, de su cariño acendrado de madre.
El alejamiento corporal de ahora la entristecía, pero no la desconsolaba;
sabía que todos estaban unidos y como era una mujer idealista
se replegaba en sus recuerdos y eso le bastaba para tener en
paz el corazón y sentirse feliz.

Estas ideas bullían en su cabeza mientras tejía, en la noche;
Rodrigo leía.

Dos o tres semanas después llegó, dirigida a ambos, a Don Rodrigo
y a Verónica, una voluminosa carta de Patricio. Allí les refería
cosas estupendas; su nombramiento como Jefe de las Juventudes
Hitlerianas de México, su entrevista con Von Virchow, la iniciación
de su trabajo. En cinco páginas manuscritas de caligrafía apretada
les relataba cómo había iniciado su tarea, la asombrosa cantidad
de elementos que habían puesto en sus manos, los centenares de
empleados que trabajaban para él. Les enviaba recortes de los
nuevos periódicos que se publicaban en la capital ---La Raza
y la Suástica--- en los que aparecían entrevistas con Patricio,
fotografías de Patricio, proclamas de Patricio. Y todo esto en
quince días.

La carta los llenó de regocijo; su tono filial, afectuoso, alegre,
hizo pensar a Verónica que el edificio de su dicha estaba construído
con la argamasa de las cosas eternas.

Hasta ese momento nada extraordinario había ocurrido en San Miguel.
Las ordenanzas policiales de Körner no se habían publicado; es
cierto que ese pueblo estaba un poco desvinculado de la vida
activa del país, pero parecía, en verdad, que allí los invasores
estaban resueltos a no dejarse sentir.

Una tarde un soldado llegó a la casa de Rodrigo con una comunicación.
Se le ordenaba que se presentase inmediatamente en el cuartel
nazi. Don Rodrigo trató de explicar al soldado que estaba en
una conferencia y que al concluirla iría a ver a Körner; pero
el emisario no hablaba español y limitóse a plantarse en la antesala
del despacho, en actitud de espera.

Cuando Rodrigo terminó, una media hora después, tomó su sombrero
y su bastón y echó a caminar por las calles de San Francisco,
rumbo al Convento de las Monjas.

Al llegar, Körner hizo pasar a su enviado; inmediatamente después
a Don Rodrigo Guerrero.

El jefe militar leía un periódico con una atención simulada y
no levantó los ojos, ni ofreció una silla a su visitante. A un
lado del escritorio el oficial intérprete hacía guardia de pie.
Pasaron así dos, tres, cuatro minutos y la situación fue haciéndose
cada momento más embarazosa. Por fin, Don Rodrigo dirigióse al
oficial y preguntóle:

---¿Habla usted español?

---Sí, señor ---fue la respuesta.

---Pues tenga la bondad de decir al señor coronel que he acudido
a su llamado y que estoy a sus órdenes; que le ruego me indique
en qué puedo servirlo---. El oficial hizo la traducción y escuchó
la respuesta de Körner, la cual, a su vez, le fue vertida al
español:

---El Coronel Körner me dice que de momento no tiene nada que
decirle; que se sirva usted esperar aquí.

Y en seguida Körner agitó una campanilla, llamó a un taquígrafo
y púsose a dictar rápidamente en alemán por diez o quince minutos.
Rodrigo sintió que la cólera lo invadía y que iba a cegarlo;
hizo un llamado supremo a todo su dominio sobre sí mismo hasta
que el jefe nazi se dirigió de pronto a él, en tanto que veía
su reloj.

---Cuando yo doy una orden debe ejecutarse al instante; la autoridad
militar es aquí suprema. No toleraré ni la más ligera rebeldía.
Tuvo usted a mi asistente veintisiete minutos esperándolo; yo
lo he tenido a usted aquí únicamente diecinueve.

Don Rodrigo iba a contestar, pero Körner lo detuvo con un gesto
imperioso.

---No tengo tiempo que perder y solo deseo que escuche usted
lo siguiente: recién llegado aquí llamé a varios de ustedes y
los hice responsables del orden de la población. Ustedes representan
para mí una garantía tangible y colectiva y no deben abandonar
la ciudad sin mi consentimiento; se lo digo porque el señor Nicanor
Aceves ---pronunció este nombre consultando unos apuntes que
tenía ante sí--- ha salido.

---Ha ido a Querétaro, señor Körner. Fue a estudiar unos archivos.

---Lo sé, señor Guerrero. Conozco todo lo que ustedes hacen;
pero no deseo que este caso se vuelva a repetir. De hoy en adelante
vendrá usted dos veces al día a este edificio a firmar una lista
especial; en la mañana a las diez; en la noche a las siete.

---¿Y si me rehuso a acatar esta orden arbitraria?

---Señor Guerrero, estamos en tiempos de guerra y mis mandatos
deben ser acatados sin discusión. Si usted los desobedece tendré
el derecho a considerarlo como indócil y mi primera providencia
será confinarlo en este cuartel para estar seguro de que a las
diez y a las siete firma usted.

En seguida pareció dulcificar la voz y terminó diciendo:

---Le recomiendo que no haga las cosas difíciles para los vecinos
de San Miguel. _Aufwiedersehen Herr_ Guerrero.

La vida reasumió su cotidiana monotonía. Los uniformes nazis
llenaban las calles, aunque no con la profusión que en otras
ciudades. Apenas si unos mil hombres ocupaban la plaza; pero
este número era bastante para quitar a San Miguel su risueño
aspecto de pueblo dormido.

Las amas de casa, en cambio, comenzaron a resentir alteraciones
hondas en sus costumbres. Para adquirir comestibles era indispensable
obtener de la comandancia militar tarjetas sin las cuales nada
se les expendía. En las tiendas, guardias de soldados nazis vigilaban
meticulosamente el curso de las compras. El mercado presentaba
un aspecto triste; las verduleras abandonaron sus lugares habituales;
los pequeños almacenes de madera permanecían cerrados. Los invasores,
como primera providencia, redujeron a una cuarta parte el aprovisionamiento
normal del pueblo; se autorizaba el de carne para el vecindario,
primeramente dos veces a la semana, más tarde solo una. Las entradas
de comestibles que llegaban de México o del Bajío cesaron por
completo y el azúcar y el café se convirtieron en artículos raros.

Un cierto grupo de familias bastante numeroso comenzó entonces
a abastecerse en las huertas del lugar, aunque muy limitadamente,
de algunos artículos: verduras, leche, huevos, fruta. Pero pronto
la soldadesca dióse cuenta de ello y esos sitios fueron esquilmados
de sus cosechas. En apariencia la moneda eran los mismos viejos
billetes del Banco de México, pero debían llevar un resello del
nuevo gobierno. Como el canje de unos por otros fue lento y se
efectuaba únicamente en la Presidencia Municipal, el vecindario
sufrió toda clase de molestias y para obtener los artículos más
indispensables las dueñas de casa operaban clandestinamente con
la moneda no sellada.

Hay que decir que nadie traficaba para su provecho y solo Melquiades
de la Peña y algunos de los suyos exigían enormes porcentajes
por recibir el monto de las contribuciones sin el requisito del
resello.

Por otro lado, en la estación de San Miguel se cargaban carros
de ferrocarril destinados a las tropas que los alemanes tenían
en muchos puntos del país, principalmente donde los refuerzos
ofensivos y defensivos alcanzaban cifras enormes.

Sin embargo, a pesar de esas limitaciones, en el cuartel se comía
con abundancia. A las cocinas de los hoteles, que funcionaban
todos para la oficialidad teutona, llegaban diariamente canastos
de verduras, frutas y carnes. En sus mesas no escaseaba el azúcar,
el café y el buen vino. Esos hechos conocidos en el pueblo crearon
en corto tiempo un mortal antagonismo hacia los invasores. Se
delineaban claramente dos castas: la de los que comían bien y
la de los que comían mal o no comían. La primera estaba formada
por los nazis alemanes y por un reducido número de nazis mexicanos;
es decir, Melquiades de la Peña y un centenar de sus paniaguados.

Como es natural este saqueo sistemático a favor del invasor principió
a comentarse acremente en los corrillos de vecinos. Se juntaban,
como siempre, en la botica de Antonio Millán. Allí alzaban su
voz Nicanor Aceves y el Doctor Bernáldez.

Don Rodrigo Guerrero los escuchaba al principio en silencio,
pero por fin exclamó una noche: ---el responsable de la situación
es Körner, está desprestigiando al régimen. Estas cosas las sabrán
en México.

Al llegar a su casa escribió una larga carta a Patricio y allí
le refería los atropellos, los abusos, las iniquidades de la
soldadesca alemana y de su jefe, diciéndole que se estaba minando
ostensiblemente la popularidad del Presidente, de su gobierno
y del supremo jefe nazi, que tantas promesas había hecho al pueblo.
Don Rodrigo cargaba toda la responsabilidad sobre Körner considerándolo
como un troglodita, indigno representante en San Miguel de un
país civilizado.

Pero esa carta no tuvo una respuesta definida; Patricio se refirió
a ella lacónicamente diciéndole que ciertas cosas no podían evitarse,
que otras eran males necesarios del momento y que todo eso lo
discutirían en una próxima visita que pretendía hacerles y que
venía planeando desde hacía semanas.

Entretanto todos esos lugares de comadreo pueblerino recibieron
orden de cerrar a las seis y se previno a los propietarios que
evitaran con la clientela cualquier conversación que no tuviese
directa relación con su comercio. Y así se cerraron las dos boticas,
la cantina de Mario, la de Pepe Muñoz, en el portal. Como en
los frontones se seguían reuniendo los amigos y allí también
se hacían críticas acres, fueron confiscados a favor de la soldadesca
teutona como una necesidad militar.

Una mañana, a las once, Nicanor Aceves llegó a la notaría de
Don Rodrigo. Se le veía excitado y aunque era prieto traía encendida
la faz.

Rodrigo lo hizo pasar a su despacho, cerró las puertas y le dijo:

---Nicanor, a usted le ha pasado algo; dígamelo, pero no alce
la voz.

Nicanor, de ordinario tan tranquilo e irónico, comenzó a hablar
descompasadamente:

---Pues imagínese, Rodrigo, estos mentecatos acaban de estar
en mi casa. Como a las nueve Herrmann, ese sodomita asqueroso,
entró con cuatro esbirros a mi biblioteca; yo me estaba desayunando.
Cuando acudí ya habían bajado de mis estantes dos centenares
de libros y formaban con ellos un rimero en una esquina del patio.
Ciego me abalancé sobre este cabrón y lo agarré por la guerrera;
debe haber visto mi ira, porque percibí el miedo en sus ojos.
Desprendióse violentamente de mí y desenfundó el revólver que
traía colgado de la cintura.

Ya comprenderá usted, Rodrigo, que me detuve. Entonces díjome
que obraba así porque había recibido mandatos precisos y que
podía matarme sin que eso le ocasionase molestias ulteriores;
le bastaría dar cuenta de mi resistencia. Mientras tanto, esos
rufianes regaron con petróleo los volúmenes apilados y les prendieron
fuego.

---¿Y sus manuscritos, Nicanor?

Nicanor contestó en voz baja:

---Los tenía guardados en otro lugar, afortunadamente. Dios quiera
que no den con ellos ahora, mientras puedo ponerlos a salvo.
Pero, Rodrigo, ¿qué no puede hacerse nada? ¿Una queja, un amparo?
¿Vamos a dejarnos atropellar por esta canalla?

Don Rodrigo le contestó con un tono desencantado:

---Temo que todo es inútil. Atribuyo estas cosas personalmente
a Körner; parece que en otras ciudades han tenido más suerte
y están bajo una férula más llevadera. He pensado seriamente
trasladarme a México.

---No es ese el remedio, Rodrigo. La radio yanqui dice que están
cometiendo atrocidades en todas partes. En Monterrey han hecho
cosas inauditas.

---Creo que esa es propaganda interesada, Nicanor.

En esos precisos momentos se escucharon tumultuosos golpes en
el ancho portón.

---Esto no es nada bueno ---murmuró Rodrigo---. Temo que mi turno
ha llegado.

No era Herrmann. Era un oficial teutón de unos treinta y cinco
años, de andar pesado y gruesas antiparras de miope. Se inclinó
ante Don Rodrigo:

---¿_Herr_ Guerrero?

---Servidor de usted.

---Mi nombre es Goffine, Hans Goffine. Traigo ciertas instrucciones
que debo cumplir y que se refieren a su biblioteca.

Hablaba español lentamente, con pronunciación gutural; extraía
las erres desde lo hondo de su garganta y abría las vocales mostrando
una dentadura orificada.

---Espero tener facilidades.

Su actitud era mecánica, como la de un muñeco de _ballet_.

Don Rodrigo, que quizás ante Herrmann habría llegado al paroxismo
de la cólera, se mostró tranquilo. Parecía que ese nuevo golpe
lo dejaba indiferente.

---Está usted en su casa, señor oficial; puede hacer lo que guste.
Nicanor, le ruego que acompañe a los señores. Yo me retiro.

Y lentamente fue caminando hacia el otro lado de la casa, rumbo
a su recámara.

---Gracias, señor Guerrero, contestó el militar, inclinándose.
Al mismo tiempo hacía ademán a Nicanor Aceves para que le indicase
el camino y penetró a la biblioteca, seguido de sus hombres.
Por la puerta que daba al jardín, el sol pintaba sobre el piso
un gran rectángulo dorado y el aroma de los naranjos invadía
la estancia.

La técnica destructora de Goffine difería de la de Herrmann.
Comenzó a curiosear los títulos con un interés de bibliófilo.
A veces tomaba un volumen, abríalo y se enteraba de los pies
de imprenta; otras leía los colofones. De cuando en cuando extraía
del bolsillo de su guerrera un pequeño tomo y lo consultaba.
Después comenzó a dar órdenes y sus corchetes iban retirando
los libros de las estanterías apilándolos ordenadamente en una
rotonda del jardín, bajo la sombra de los laureles. Lo mejor
que los hombres habían pensado y sentido en el correr de los
siglos formaba rimeros multicolores; diez tomos de Shakespeare,
un Byron completo, una edición Palgrave de las novelas de Walter
Scott; la edición Chesterton de Dickens; casi todos ellos libros
que Rodrigo había recibido de su padre y este de su abuelo. Después
aumentaron el montón los ensayos de Bergson, los cuentos de Edgar
Allan Poe, la autobiografía de Franklin, una biblia…

Nicanor Aceves contemplaba la escena atónito; su curiosidad de
cronista se sobreponía a su ira. Al fin interpeló a Goffine:

---Teniente (creo que ese es su grado ¿no es eso?). ¿Puede usted
decirme cuál es el criterio que lo guía para ejecutar este auto
de fe? Quiero decir ¿qué procedimiento sigue para conocer las
obras nefandas? ¿Cuáles son los culpables?

Goffine respondió con una seriedad cómica:

---No debo dejar un solo libro inglés ni americano; la cultura
enfermiza de nuestros enemigos debe desaparecer. Y también los
libros judíos.

Cientos de libros fueron así condenados a la hoguera. El teniente
Goffine seguía abriendo con amor los volúmenes, acariciando las
pastas y consultando el pequeño tomo que guardaba en el bolsillo.
Nicanor Aceves, al ver su actitud voluptuosa, pensó para sus
adentros que tenía ante sí un caso patológico de sadismo.

Al poco rato comenzaron a arder; las llamas se elevaban y las
hojas más bajas de los árboles crepitaban con el calor. Aceves
se sentía como un hombre hambriento que ve arrojar a un estercolero
los manjares que nunca ha podido gustar.

Al salir el oficial nazi tomó un ejemplar de _Mein Kampf_. Era
el obsequio de Alonso Quintanar. Al percibir la mirada interrogadora
de Aceves, explicó con una entonación en la cual parecía extremarse
su acento germano:

---Prefiero que este libro esté en poder de un ario; de un hombre
que lo comprenda y lo respete.

Ante tamaña pedantería Aceves estuvo a punto de soltar la carcajada
en sus narices. Goffine llegó al zaguán, inclinóse, alzó el brazo,
pronunció el ritual «Heil Hitler» y salió a la calle.

Nicanor dirigióse a buscar a Don Rodrigo. Pensaba que allí el
atropello físico había sido menos violento. Casi reía al recordar
al sabiohondo teutón disfrazado de militar.

Rodrigo se encontraba en su recámara. Estaba sentado, inclinado
sobre una mesa, con la frente apoyada en un puño, su otra mano
descansaba sobre la cubierta; sollozaba quedamente. A su lado
Verónica se encontraba también sentada, con la cabeza sobre el
hombro del marido. Tenía empañados sus ojos abiertos; lloraba
en silencio y sus lágrimas rodaban por su rostro, cayendo en
gotas sobre el dorso de la mano que Don Rodrigo tenía extendida.

Nicanor Aceves no se atrevió a entrar; dio media vuelta y abandonó
rápidamente la casa.

Decididamente un ambiente opresor se abatía sobre San Miguel.
En esos días un grupo de soldados borrachos requisó las bodegas
de algunos sanmigueleños que gustaban de agasajar a sus amigos
con un vaso de buen vino. Cargaron las existencias en un camión
militar y las llevaron al cuartel; allí iban trescientas botellas
de un Chablis de la cosecha del año 27, que el Doctor Bernáldez
había traído consigo en su último viaje a Francia. También penetraron
a la casa de Don Rodrigo, encontrando solamente unas barricas
de Santo Tomás. Entonces, para no cargarlas llenas hasta los
camiones, las fueron vaciando en cubetas de zinc; los regueros
de vino iban desde la bodega hasta el portón. La soldadesca celebraba
el pillaje con risotadas escandalosas. Al día siguiente, a cada
uno de los dueños de esos caldos, se les impuso una multa de
mil pesos por acaparamiento delictuoso de artículos alimenticios.

Don Rodrigo estaba de un humor taciturno; se le veía enfermo.
Verónica, de suyo tan animosa, sentíase en peligro. Pensaba que
si el infortunio los abatía no los iba a encontrar unidos y esa
era su congoja; la posición de Patricio la llenaba de inquietudes.
Por primera vez en su vida se sintió sola y tuvo miedo a su soledad.
Mandó llamar a Remigio sin darle detalles, pues las cartas eran
censuradas por el cuartel nazi.

Remigio arribó un mediodía; venía de Guanajuato. Su padre no
estaba en casa; mientras llegaba, Verónica le refirió las vejaciones
sufridas. Remigio las escuchó con el ceño fruncido, sin hacer
comentarios.

Sorprendióse dolorosamente al ver a Don Rodrigo; en pocas semanas
de ausencia lo encontró envejecido; marchaba agobiado; su habitual
aspecto feliz habíase borrado de su rostro.

La comida fue triste. Remigio procuró estar festivo. Les contó
anécdotas de su vida de interno; pero a su padre le faltaba la
jovialidad de otros tiempos. Apenas si sonreía. Cuando terminaron
Remigio le propuso un paseo.

---Vamos a caminar; el ejercicio te hará bien.

---Tiene razón Remigio ---comentó Verónica---, eso ayudará a
tu digestión; no te mueves, estás apoltronado. Anda, a la calle
---y le puso en sus manos un bastón y el sombrero.

Don Rodrigo y su hijo bajaron en silencio por la calle de San
Francisco, prosiguieron por la de La Canal, torcieron por la
de Zacateros y pasando frente a la ruinosa casa solariega de
don Manuel de la Canal, llegaron a la iglesia de San Antonio.
Allí se sentaron en los escalones del atrio. A su izquierda se
extendía el pueblo; la Parroquia, la cúpula de las monjas y al
fondo la de San Francisco.

Las torres se recortaban sobre el cielo sobresaliendo por encima
de las colinas. A su derecha se desplegaba la serranía de color
verde. Ambos estaban poseídos de una sensación de descanso; les
parecía que contemplaban el San Miguel de otros tiempos, desde
allí solo veían las cosas inmutables: el cielo, los cerros, las
iglesias levantadas con tanto amor, las casas construídas con
la sabiduría de generaciones, los abetos casi centenarios. Desde
allí, desde el atrio de San Antonio, Körner no existía, ni los
_Quislings_, ni las platitudes de _Mein Kamp_, ni el orden nuevo.

Remigio fue el primero en hablar:

---Padre, ya sé los atropellos que se cometen en San Miguel;
sé también lo que han hecho contigo.

---Sí, hijo; el jefe nazi aquí es un tipo incomprensivo y grosero;
desprestigia al gobierno y anula sus buenas intenciones.

---Padre, quiero decirte que todavía vives engañado. En todo
el país se repiten los mismos ultrajes; los Körner son la fruta
del tiempo. En Guanajuato recibimos informaciones precisas.

---Hijo mío, ya no quiero ni discutir el punto; no sé ni qué
pensar.

---Desgraciadamente ---continuó Remigio--- lo único que puede
pensarse es que somos un país conquistado y que recibimos el
trato de esclavos de guerra. En Morelia las escuelas se han cerrado
en las tardes y los niños están obligados a hacer la pepena en
los basureros de la ciudad; salen en parvadas a recoger papeles,
trapos y latas para llevarlos a la escuela de artes y oficios
en donde estos desechos son clasificados y empacados con destino
a las fábricas nazis. Lo más triste es que esos chicos van por
las calles custodiados por esbirros del invasor, como si fuesen
delincuentes.

---¡Pero eso no es posible, hijo!

---Es posible, padre; y también ha sido posible el fusilamiento
apenas ayer de siete vecinos principales en Toluca, por publicar
una protesta contra los procedimientos del _gauleiter_ local. Podría
contarte muchas otras cosas, pero no es el momento. Ahora quise
traerte aquí para hablarte con libertad; no quiero muros que
me escuchen y tampoco quiero que me oiga mi madre.

He decidido alistarme con un grupo de patriotas que pretenden
libertar a México del yugo alemán. Cómo y de qué manera, todavía
no podemos decirlo; pero estamos resueltos a que ningún soldado
invasor sienta su vida segura mientras pise suelo de México.
De momento no sabemos más. No deseo que lo sepa mi madre, cuando
menos por ahora. He pensado hondamente y mi resolución es definitiva.

---Hijo mío ---contestó Don Rodrigo después de un silencio largo---,
eres un hombre y tienes derecho a seguir el camino que te plazca.
Si crees que así cumples con tu deber no tengo nada que objetar.
Sin embargo, estoy obligado a decirte, como padre, lo que por
lo pronto se me ocurre.

En esta lucha tendrán ustedes que coligarse con los Estados Unidos;
son sus únicas probabilidades de triunfo; aun así su buen éxito
es dudoso, pero sin esa alianza van al fracaso más rotundo. El
mecanismo del invasor, como tú le llamas, es demasiado poderoso.

---Y bien, Remigio, yo veo para mi patria una disyuntiva desgraciada.
Si Körner, como dices, representa el tipo del protectorado hitlerista,
entonces México está perdido; pero sabes que temo a los yanquis
tanto como tú a los alemanes. En fin, no tengo humor de chancearme,
pero hay que decir que estamos ante las dos sopas del cuento---.
Y sonrió ligeramente.

---Bueno, padre; habría mucho que discutir. Solo sé decirte que
aun concediendo todo lo que tú dices contra los americanos, nuestro
camino está bien claro. Solo con ellos tiene México una probabilidad
de ser un sitio donde nuestros hijos puedan vivir. Sus gobiernos
nos han agraviado, es cierto, pero ahora nuestro movimiento liberador
viene de las entrañas mismas del pueblo de México. Y yo espero
en Dios que ante el peligro común podamos establecer contacto
con el pueblo americano. Quizá cuando llegue la victoria tendremos
el respeto del mundo y nos habremos hecho dignos de modelar nuestro
destino; esperemos que entonces ellos mismos nos reconozcan ese
sagrado derecho.

---Hijo mío, acato tu decisión; tu madre no sabrá nada mientras
tú no lo quieras. Ojalá tus romanticismos se realicen. Tienes
mi bendición.

---Padre ---contestó Remigio---, tú has dicho la palabra; vamos
a pelear por el romanticismo de la libertad.

Se levantaron y lentamente subieron hacia el centro del pueblo.
Remigio le daba el brazo a su padre y canturreaba una canción.
Se sentían casi alegres.

</section>
<section epub:type="chapter" role="doc-chapter">

# VIII

++Patricio++ se fue a vivir a México desde antes que el Gobierno
pelele se apoderara del país. El esperaba que el trabajo, como
los nazis lo entendían, salvaría a su patria; y Patricio creía
en el trabajo metódico, disciplinado, austero. Cuando en febrero
de 1946 (a través de los años las fechas de usurpación habían
coincidido) los paracaidistas nazis descendieron en la ciudad
de México y surgió de la nada ese gobierno bastardo, Patricio
creyó de buena fe que un movimiento popular había arrojado del
Palacio a quienes llevaban las riendas de la cosa pública.

El razonaba en forma simplista, como tantas veces había oído
razonar a su padre: ---el gran enemigo de México es el yanqui,
porque invariablemente nos ha impuesto los regímenes que mejor
se pliegan a sus tendencias imperialistas jamás desmentidas.
Esta ha sido la tradición Poinsetiana. Mi padre, que es un hombre
educado y ecuánime me enseñó que el Gobierno del General Díaz,
con todos sus graves defectos, habría evolucionado hacia una
estructura genuinamente nacional a no ser por los temores infundados
de los yanquis, quienes suponían que el viejo dictador anudaba
demasiados lazos con la entonces omnipotente Europa. Y derrocaron
al caudillo, escudándose en una opinión pública ingenua e inexperta,
para encumbrar a un hombre bondadoso, que creía en la fraternidad
humana; un espiritista cristiano que se ganaba el cielo observando
la ley natural, como dicen los textos canónicos, y que llevó
a México de traspiés en traspiés, convirtiéndolo en una presa
cada vez más indefensa para los Estados Unidos.

Mi padre me enseñó también ---se decía Patricio--- que un Presidente
americano, un autócrata teorizante llamado Woodrow Wilson, nos
convirtió a los mexicanos en sujetos de un gran experimento
sociológico, poniéndose al habla con Francisco Villa ---un troglodita
sediento de sangre y de poder--- y enviando a México a un embajador
personal, un analfabeto saturado de prejuicios, llamado Lind,
quien decidió que Villa y sus hordas eran la esencia pura de
las aspiraciones de los pobres indios.

Ese mismo Wilson ensanchó después sus experiencias, convirtiendo
al mundo entero en un gran laboratorio en 1918.

Mi padre me decía que era cierto que México tuvo un gobernante
que fue un chacal dipsómano, un tal Victoriano Huerta, pero que
los Estados Unidos solo supieron arrancarnos de las fauces del
chacal para arrojarnos en las garras del tigre.

Estos ejemplos de la acción norteamericana venían a la mente
de Patricio porque los conocía a través de las charlas paternas.
Se habían incrustado en su cerebro desde niño, pues Don Rodrigo,
en las sobremesas, era locuaz y hacía ante la taza de café y
con el cigarro puro en la mano largas disertaciones para justificar
su yankofobia. Este era su credo.

Por eso, cuando los nazis organizaron y apoyaron su gobierno
pelele, Patricio juzgó patriótico aliarse con los enemigos del
yanqui. Juzgaba que había que aprovechar la ayuda desinteresada
de los invasores. Pero la verdad nunca llegó, o llegó demasiado
tarde a su entendimiento. Antes de la ocupación alemana él vivía
entregado a sus _boy scouts_. Había puesto tanta pasión en esa
tarea, se había sacrificado con tan hondo desinterés, que el
árbol comenzaba a dar sombra y aun a florecer; el fruto no podía
tardar mucho.

Con qué plenitud de vida había salido muchas mañanas por las
colinas que rodeaban a San Miguel, con sus _boy scouts_ del Estado
de Guanajuato, el grupo embrionario que más tarde se había extendido
a la mesa central y después a toda la república. Recordaba aquella
caminata hasta Guadalajara, con ochocientos muchachos de doce
a quince años. Se sentía con un corazón tan grande como si fuera
el padre de todos.

Salieron un quince de abril. La noche anterior habían dormido
en una troje abandonada del rancho de Mexiquito. A las cuatro
de la madrugada despertó a sus jefes de patrulla y poco a poco
el silencio se fue llenando de una algarabía matinal. El aire
estaba fresco; las estrellas comenzaban a esconderse en la luz
de la aurora; sobre el cielo principiaron a recortarse las siluetas
de los cerros y allí abajo los faroles del pueblo se apagaban
uno a uno. Recordaba la torre del Convento de las Monjas, la
Parroquia, con su arquitectura gótica, absurda, pero que le hablaba
tanto al corazón, y las cúpulas del Oratorio y de la Santa Casa,
de siluetas estrambóticas y suaves tonos rosados que le evocaban
cuentos de las mil y una noches.

Allí estaba él, Patricio, con su indumentaria juvenil de _boy scout_,
su mochila terciada y un gran cayado sobre el cual se apoyaba
al contemplar el paisaje de ensueño.

Se encendieron las fogatas para calentar el café negro, después
los pequeños montones de cenizas y brasas se cubrieron cuidadosamente
con tierra y aquel ejército emprendió la marcha cuando los gallos
anunciaban la salida del sol.

¡Cuánto había disfrutado en aquellos días de fatiga! ¡Las noches
que acamparon en los cerros; sus pequeños centinelas haciendo
guardia y rasgando el viento con sus voces de soprano! Y todo
tan exacto, con una disciplina tan espontánea.

Después la llegada a Guanajuato, más tarde a Silao y finalmente
su entrada casi épica en Guadalajara. Le parecía que su proeza
solo podía compararse a la de una gran campaña militar. Llegó
sin un enfermo, sin un extenuado. Todos limpios, frescos, rebosantes
de vida. Este era su primer triunfo como líder de la juventud;
el fruto de meses de trabajo, de enseñanzas, de cuidados solícitos,
de amor pródigo. Y cuando llegó el momento de la desbandada,
antes de ir a los trenes que habían de devolverlos a las casas,
sus _boy scouts_ entonaron para él un canto que se antojaba la
despedida a un héroe.

Para los veintidós años obtuvo su titulo de abogado. El no sabía
hasta qué punto gustaba de su profesión; desde luego no era un
polemista y veía con antagonismo un porvenir de juzgados y expedientes.
Su carácter serio y hondo se sentía más a su sabor explorando
el sentido filosófico del derecho. Pero su padre quería un abogado
en la familia y el cumplimiento de ese deber le satisfacía. Recordaba
su vida estudiantil, la casa de huéspedes provinciana, el Colegio
del Estado con sus corredores oscuros y sus aulas destartaladas.

No había sido un alumno brillante; le faltaba el ingenio fácil,
la respuesta rápida, el desparpajo que todo lo suple. El entraba
a la médula de sus textos con un esfuerzo casi doloroso, como
el encarcelado que busca la luz de la libertad desintegrando
los muros con una paciencia infinita y sin otras herramientas
que sus uñas ensangrentadas. Pero él, por orgullo, ocultaba esta
gestación y se presentaba en las clases escudado en una sencillez
que no dejaba adivinar la dureza del trabajo previo.

Contestaba con un decoro que revelaba la estricta honestidad
de su espíritu; sin aparentar nunca lo que no sabía, sin sustituciones
dolosas, sin palabrería vacua. Cuando ignoraba algo, era un ---«no
lo sé»--- sin ambages ni disculpas. No lo sabía y eso era todo;
había que reparar la falta sobre los libros.

En la casa de huéspedes era el preferido de Doña Amelia, una
viuda ya vieja, menudita y tan frágil que parecía que iba a troncharse.
Patricio guardaba de ella un recuerdo grato; había sido para
él como la sombra de una madre (le faltaba el alma y la carne),
pero no podía olvidar la solicitud con que se sentaba a poner
botones nuevos a las camisas y le servía los molletes con mantequilla.
El también había sido para ella como la sombra de un hijo. Después
de que el hijo de verdad había muerto en una riña de cantina,
Patricio despertó en Doña Amelia sus adormecidos instintos maternales;
era una dulzura esa actividad física rutinaria: limpiarle los
trajes, contarle la ropa, marcarle los pañuelos, llevarle un
vaso con jugo de naranja cuando Patricio saltaba del lecho para
la clase de las ocho.

Ante el recuerdo del hijo turbulento que llegaba ebrio, que contraía
deudas, que faltaba al trabajo, que hacía una existencia desdichada
de animal demente, se refugió después en la realidad consoladora
de ese muchacho de otra mujer, era cierto, pero que tenía tantas
cosas que habría deseado para el suyo.

Patricio fue así haciéndose hombre en el Colegio del Estado.
No era el estudiante más fácil, pero, con el tiempo, fue quizá
el más sólido. Su carácter no lo predisponía a formar amistades
con estudiantes de su edad que cortejaban a las costureritas
de los barrios, las llevaban al cine y después de dejarlas los
sábados en las noches iban a los prostíbulos. Cuando urgido por
el instinto los acompañó algunas veces, salió siempre de esos
lugares con una honda contrición. Adivinaba desde entonces que
tendría que casarse joven.

Al mismo tiempo agradecía a su padre el culto que había sabido
fomentarle por la fatiga corporal, por la lucha abierta, por
la carrera a campo traviesa. Recordaba sus primeros guantes de
boxeo, cuando Remigio y él aprendieron desde pequeños que no
hay nada denigrante en los golpes que se dan y se reciben según
las reglas de un juego equitativo. Patricio era alto, delgado,
ancho de hombros y de músculos largos y se había hecho nervudo,
resistente, incansable. Ponía en los deportes el mismo empeño
consciente que en los libros. Su padre los hizo _boy scouts_, tanto
a Remigio como a él, desde que apenas podían empinar una cantimplora.
Remigio abandonó a los grupos a los diez u once años; prefería
las excursiones solo y a veces llegaba de noche, cuando Verónica
estaba ya angustiada y Don Rodrigo tenía el rostro severo. Pero
Patricio se sentía en su elemento cuando con sus compañeros acampaba,
dormía bajo el cielo y almorzaba con ellos a la sombra de los
árboles. Escuchaba con devoción las lecturas colectivas y se
sabía de memoria las reglas que había que respetar.

Por eso en la Preparatoria y después en Leyes comenzó a organizar
a los chicos pequeños; niños de diez, doce o trece años que lo
admiraban cuando jugaba básquetbol y que lo querían porque los
había defendido más de una vez.

Abandonó la compañía de los estudiantes de su edad y dedicó todos
sus ocios a esos _boy scouts_ a los que predicaba ideas de caballero
andante y a quienes contaba historias de viajes, de exploraciones
y de conquistas justas.

Conoció a Eligia en unas vacaciones en San Miguel. Es decir,
la conoció mujer, porque de chiquillos habían retozado juntos
en las casas del pueblo. Ella llevaba una muñeca de trapo vestida
de azul que dejaba a un lado para ponerse a jugar a Doña Blanca
o al pan y queso; la hacían llorar a menudo porque era prietita
y los demás se burlaban de su color. Al crecer sus facciones
se afinaron, su piel se aclaró y con sus ojos y cabellos negros
era un tipo de mestiza. Huérfana de padre, su madre había sacado
adelante una casa numerosa. El retoño único era Eligia, pero
al calor de Doña Concha se habían acogido dos sobrinos y la hija
de una amiga que se la legó en su lecho de muerte; y para todos
trabajaba con el propósito de hacerlos gentes de bien.

Cuando la encontró en la casa de los Veytia, recién llegado de
Guanajuato, Patricio se sorprendió: ---¿Eres tú Eligia? ---Sí,
yo soy Eligia ¿qué te sorprende? ---Que estás muy crecida y que
si te encuentro en la calle no te reconozco. ---Pues tú eres
el mismo, Patricio; nada más que te han alargado los pantalones.

En esa temporada se vieron con frecuencia. Descubrieron que tenían
cosas en común. Ella ayudaba a su madre en una multitud de pequeños
artificios con los cuales ganaba su vida; pero se había propuesto
aprender inglés y hacer un curso de enfermería. Estas tareas
las tomaba muy en serio y pasaba ante sus libros cotidianamente
varias horas. Eligia encontró inusitado que Patricio, lejos de
hacer mofa de sus estudios le ayudara a descifrar sus textos.
Por fin, decíase, hay un muchacho para el cual no soy ojos y
boca y cuerpo; por fin hay alguien aquí que piense que una muchacha
puede hacer otra cosa distinta a esperar un marido. Pero en realidad
se engañaba y estas reflexiones eran el fruto de una ingenua
pedantería.

Patricio se fijaba bien en lo suave de su tez, en sus cejas firmes,
en su boca grande y en todo su cuerpo bien construído. Quizás
si ella no hubiese sido tan llena de gracia él no le habría prodigado
con tanta generosidad sus consejos e interés. Eligia por su parte
comenzó a hacerle preguntas sobre los _boy scouts_. ---Bueno, Patricio,
pero, ¿cuál es el objeto de esas excursiones? ¿Por qué no te
limitas a que hagan ejercicio? ¿Para qué enseñas a un chico de
diez años a destripar un pescado y a asarlo en las brasas?---
Patricio no pedía otra cosa sino desarrollar sus teorías: ---Mira,
Eligia, a los niños hay que ponerlos en contacto con la naturaleza;
es preciso que conozcan la salida del sol, la lluvia, el arco iris---.
Y se enfrascaba en explicaciones interminables. Le llevó
a Eligia «El libro de las tierras vírgenes» y decidieron leerlo
juntos. Patricio se entusiasmaba buscando el sentido filosófico
de la ley de la selva y de la lucha entre Mowgli y Shere-Khan,
el tigre. Eligia lo que no encontraba bien era por qué Mowgli
volvía a los hombres cuando en la selva era el rey y era feliz.
Muerto el tigre, aquella ranita indefensa y desnuda vivía rodeada
de amigos como el milano, la boa, el oso y la pantera negra.
¿Para qué volver a la aldea? ¿Cuál era la esencia de ese último
cuento «Correteos en Primavera»? ---Eligia, te voy a explicar
eso. Mowgli observaba cómo cada primavera los lobos lo dejaban
solo y se iban a buscar sus parejas y cuando después, al cabo
de nueve o diez semanas, llegaba una noche a su cueva, allí estaba
mamá loba con una nueva cría de lobeznos. Tú comprendes, Eligia,
que… ---Esos son cuentos chinos ---le interrumpía levantándose---.
Mowgli dejó la selva por tonto.

En el pueblo se comenzó a hablar de ellos. La cosa era tan natural
que hasta las solteronas comentaban ese noviazgo con simpatía.
---Este muchacho le quitará de la cabeza el inglés y la medicina---.
Rodrigo pensaba que la muchacha era bonita, sana y que sabía
de la casa y el cuidado de los chicos. ¿Qué más podía desear
Patricio? ¿Que este era muy joven? También él, Don Rodrigo, se
había casado pobre y joven, sin más patrimonio que su carrera
y su notaría y había sido feliz con Verónica.

Nunca, como entonces, en esas vacaciones, saboreó Patricio la
tranquilidad paradisíaca del pueblo. Durante el día, mientras
Eligia ayudaba a Doña Concha, él charlaba con los vecinos amigos
de su padre que lo habían visto nacer o con sus camaradas de
la escuela de las monjas que ahora trabajaban ya; unos en los
ranchos, otros en los comercios o en las oficinas del Ayuntamiento.

En las tardes iba por el periódico que llegaba de México y comentaba
con don Rodrigo las noticias de Europa. La guerra acababa de
estallar. Transcurrían justamente esos meses de tregua engañosa,
cuando Alemania trabajaba veinticuatro horas al día terminando
sus preparativos, mientras de unas trincheras a otras se hacían
chistes y se cambiaban buenos trozos de cena. Los franceses pensaban
que esos mocetones alemanes que se bromeaban en francés y tocaban
_La Madelon_ eran incapaces de hacer ningún mal; por su parte,
los alemanes obraban así obedeciendo las consignas.

Don Rodrigo comentaba sorprendido el lento desarrollo de los
acontecimientos. Esta Alemania no era la que él esperaba: la
de los triunfos en Austria, la de las conquistas de Checoslovaquia
y de Polonia. No por eso era menor su admiración por Hitler.

---Hay que aprender, hijo, lo que quiere decir la disciplina;
de un país postergado se hace una potencia de primer orden. Aquí
hablamos mucho de libertad y sin embargo no la tenemos ni para
elegir a un presidente. Y si la tuviéramos ¿qué haríamos con
ella? ¡Elegir a Almazán! Y es que esa libertad por la que clamamos
no es la que hace grandes a los pueblos.

Aquí confundimos la libertad con el desorden. La libertad no
puede estar en pugna con el bien colectivo o hay que suprimirla.
Imagínate, hijo, que cuando sales con tus _boy scouts_ cada quien
fuese libre de alejarse, de hacer fogatas a deshora, de dormir
en cualquier sitio; tú tienes que imponerte, esgrimir la ley
y hacerla respetar. Y cuando es necesario la única ley es tu
voluntad. Eso es lo que ha hecho Hitler. ¡Y hay que ver a las
Juventudes Alemanas! ¡Qué espléndida organización! Todo por el
orden.

Por eso estoy en pugna con los yanquis. Les falta el sentido
de la especie. Ellos no aspiran a la fuerza, sino a que sean
débiles los que lo rodean; es la razón por la que han hecho de
nosotros lo que han hecho. Sus fuerzas negativas son Canadá y
México, pero México sobre todo, porque los ojos del yanqui están
apuntados hacia el Sur. Recuerda lo que te digo, Patricio, ellos
nunca nos dejarán hacer un gobierno fuerte y si alguna vez esa
ocasión se presenta, tu deber de mexicano es aprovecharla o ayudar
a que el país la aproveche.

Patricio lo escuchaba con afecto. Su padre solo podía decir cosas
justas. Además esas mismas cosas resonaban con exactitud en su
interior. El también quería el orden, la libertad limitada al
bien colectivo, la formación de una juventud poseída del sentido
de la especie, como Don Rodrigo decía.

Patricio volvió al colegio con una nueva ilusión. Eligia no era
su novia, pero se había despedido de él mirándole a los ojos.
Se enfrascó más que nunca en sus estudios, quería terminar cuanto
antes y echarse a la vida en su compañía. La ausencia la hizo
a sus ojos más mujer y los recuerdos comenzaron a torturarlo.
Allí en Guanajuato el olor de su cuerpo lo envolvía, evocaba
inconscientemente la plenitud de la carne que llenaba los vestidos
recién planchados, la piel morena que se asomaba a través de
las camisas y las flores en los cabellos. Esto lo tenían muchas
mujeres; pero en Eligia todo era perfecto. Ella misma era perfecta,
con su tono afable y su sonrisa afectuosa. ¿Por qué no le dijo
que la quería? ¿Por qué no la ligó a él con promesas precisas?

Patricio se entregó con gran ahínco a sus _boy scouts_. Los chiquillos
de los barrios iban afiliándose a esa hermandad que para ellos
representaba otra existencia. No tenían equipos, ni botas claveteadas,
ni sombreros de fieltro. Salían a caminar descalzos muchos de
ellos, con sus trajes usados de mezclilla; no tenían infiernillos
para cocinar, ni cañas de pesca, ni tiendas de campaña. Pero
desde los sábados en la tarde una agitación infantil recorría
las callejuelas de Guanajuato; era que se preparaban para una
excursión. Los muchachitos armaban conciliábulos en los rincones
de las plazas, juntaban los centavos que habían logrado ganar
limpiando zapatos, sacudiendo coches o haciendo mandados. Con
eso bastaba para los gastos estrictos y el domingo en la madrugada
o mucho antes estaban prestos para la marcha.

Patricio salía así con su enjambre; algunos eran de un mejor
estrato social, pero todos se confundían en una misma ansia
de aventura. Cuando hacían el primer alto, al salir el sol, Patricio
les contaba historias extraordinarias y les llenaba las cabecitas
con fantasías románticas que disipaban la tristeza de la vida
y les hacían olvidar los cuartuchos sórdidos y las comidas malas.

Poco a poco aquello comenzó a repercutir. Se hablaba de aquel
mozalbete, casi un adolescente, que ejercía esa fascinación sobre
los chicos. Las escuelas particulares lo llamaban para organizar
a sus muchachos. Los _boy scouts_ principiaron a multiplicarse
por Guanajuato, después por Jalisco, Michoacán y México. Se juraba
por el honor, por la Patria y por Dios y como por milagro en
todas partes brotaban patrullas de afiliados. Las gentes de calidad
se interesaron poco a poco; las organizaciones de _boy scouts_
que anteriormente en la capital habían arrastrado una existencia
precaria, respiraron un soplo de esperanza; finalmente, el periódico
«Excelsior» decidió apoyar un gran movimiento nacional de _boy scouts_,
considerándolo como una gran promesa de redención y convirtiéndose
en su desinteresado órgano de publicidad.

Patricio fue llamado para acometer la tarea y estuvo en México
repetidas veces. Pero este trabajo agobiador pesaba demasiado
sobre sus hombros de estudiante. O eran los estudios o eran sus
muchachos. Por fortuna estaba en el último año de la carrera
y haciendo un esfuerzo supremo la terminó con decoro. Cuando
regresó a su casa, a fines de 1940, era abogado y tenía veintidós
años. Lo más saliente de esas semanas que pasó en San Miguel
fue su noviazgo con Eligia; ella lo recibió como a un pequeño
triunfador. Al verlo le echó los brazos al cuello y lo besó en
la boca; y así quedó sellado su compromiso, sin explicaciones
literarias. ¡Cuánto le agradeció él esa acogida! Su gratitud
fue de toda la existencia. ---Si tú supieras ---le decía más
tarde--- cuántas palabras vacías y tontas había yo preparado
para esa entrevista.

Fijó pronto fecha para casarse. La fatiga y el esfuerzo no eran
bastantes para acallar la pasión que lo invadía sin tregua; pero
él refrenaba su deseo porque se había propuesto hacer de su amor
una cosa limpia y austera. Pasaba horas de lucha con su instinto
desbocado y solo el sueño, a veces, le concedía un desahogo bienhechor.

El día de la boda fue de regocijo hogareño. Estaban don Rodrigo,
Verónica, Remigio y Ana; todos los pedazos de su carne. Había
ido don Maclovio Otamendi, con él iban Guadalupe su hija y Armando,
que a los quince años parecía un Apolo adolescente. Naturalmente
los acompañaba el doctor Bernáldez, el cura Videgaray y don Aurelio
Manterola, un ranchero este último tan viejo que con su barba
larga era como un patriarca. Tampoco podía faltar Alonso Quintanar.
Fuera de estas gentes que eran como la prolongación de la familia,
estaba el pueblo entero. En San Miguel, donde no hay plutocracia
ni grandes desigualdades de fortuna, don Rodrigo con su notaría
y su decente pasar era casi un símbolo; las nupcias de Patricio
les atañían a todos.

Aquellas fueron las bodas de Camacho. Allí en San Miguel, como
en toda la provincia mexicana, la vida era tan fácil y la providencia
tan pródiga, que una comilitona como esa era obligada en casos
así. Se pensaba que México era el paraíso de la tierra mientras
en Polonia ancianos y niños morían de inanición; o se leían las
noticias de racionamiento en toda Europa, donde un kilo de carne,
si la había, era la porción semanaria de una familia. Entretanto
allí en San Miguel un notario modesto invitaba a su mesa a ciento
cincuenta de sus amigos. ¡Y qué mesa! Cabritos asados, lechones
al horno, pavos en barbacoa. Canastos de pan caliente, de tortillas
perfumadas. Los mejores higos y duraznos del mundo. Y buen vino;
vino mexicano al fin, que salía sin tasa de las espitas alineadas
en el patio. El cura Videgaray bendijo aquellos dones y don Maclovio
Otamendi, sacudiendo sus mechas grises, hizo augurios de felicidad
para Patricio y Eligia. Estos salieron esa tarde para Guanajuato;
iban rumbo a Torreón en donde una sociedad de padres de familia
lo solicitaban para poner en sus manos la suerte de un millar
de muchachos. En San Miguel quedaba esa sensación melancólica
y dulce del tiempo gozado y la que en tierra dejan los marinos
que se embarcan en un velero, hacia el ignoto mar.

Al día siguiente Rodrigo y Verónica, con Remigio y el cura Videgaray,
don Maclovio Otamendi y Alonso Quintanar, tomaban el café en
el huerto de la casa. El aire era tibio y olía a azahares; sobre
el cielo de puro azul se destacaba la cúpula de San Francisco.
Un ambiente de felicidad sensual envolvía la escena. Don Rodrigo
inició su charla predilecta: {.espacio-arriba1}

---La caída de Francia estaba escrita ---dijo---. No en vano
se inyecta en un pueblo el virus bolchevique. Donde no hay disciplina
ni trabajo no puede existir la fuerza, y lo que necesitan los
pueblos es ser fuertes. Además, los alemanes luchan por el espacio
vital que es una causa justa. Los ingleses ---que por algo son
pérfidos por antonomasia--- dominan y explotan el mundo entero
para su provecho y ya es tiempo de que su imperio acabe. Yo deseo
hondamente el triunfo nazi porque será un golpe al comunismo,
a los ingleses y de paso a los americanos. A ver si así llega
nuestra hora y sacudimos la tutela yanqui.

---Rodrigo ---le interrumpió Maclovio--- te llevo algunos años
y casi te vi nacer. Tengo el derecho de hablarte, si no como
a un hijo, cuando menos como a un hermano menor. Desbarras lastimosamente.
Eso de la perfidia inglesa es una generalización vulgar; yo podría
probarte que esa explotación de que hablas es una fábula: allí
están el Canadá y Australia. Despotricas contra los rusos; pero
debes recordar que son aliados de Hitler y quizá por eso cayeron
como buitres sobre una Polonia destrozada. Si Alemania triunfase
ahora, se repartiría con Rusia a la pobre Europa mientras llega
el momento de que ambas riñan por el predominio final. Además,
las persecuciones a los católicos…

---Maclovio ---replicó Rodrigo--- acepto sus amistosas injurias,
pero en este terreno quien puede hablar es el señor cura. Es
cierto que su opinión no la tomaré como infalible; pero tiene
más autoridad que usted.

---De acuerdo. ¿Usted qué piensa, padre?

El cura Videgaray era un hombre joven, impetuoso, buen conductor
de su grey. Le importaba sobre todo la moral de sus feligreses,
la fidelidad en los matrimonios, la caridad de los ricos y la
dicha de los pobres.

---El problema es tan complejo que es muy difícil emitir un juicio,
pero yo, como sacerdote, no puedo estar con Hitler. Quizás este
triunfe; tiene una máquina guerrera formidable, pero es un enemigo
de la Iglesia y mientras más poderoso sea es más temible.

---Padre ---dijo Rodrigo--- estas son patrañas. De quien Hitler
es enemigo es de los judíos que crucificaron a Nuestro Señor.

---Don Rodrigo, Nuestro Señor ha perdonado ya a los judíos y
no es Hitler el llamado a liquidar esas cuentas. Pero sí puedo
decirle que esas persecuciones son contra el espíritu cristiano.
Respecto a la Iglesia Católica no es preciso hablar de memoria:
Hitler firmó un concordato en el verano de 1933; esa fecha debió
haber sido de fiesta para los católicos, pero no había pasado
una semana cuando el gobierno alemán promulgó la ley de esterilización.
El concordato se violó en todas sus partes; se entabló la lucha
y hubo asesinatos en masa; entonces perdió la vida el Jefe de
la Acción Católica de Berlín. Mire, don Rodrigo, con algo más
de memoria y una poca de pedantería le podría relatar cosas que
ponen los pelos de punta.

---Todo eso es ---dijo Don Rodrigo--- propaganda inglesa y americana.
Y en seguida: ---Aquí el que no dice nada es Alonso.

Este se había extendido en un sillón y dormitaba. Al oírse interpelado
habló con lentitud y desencanto.

---Rodrigo, pierdo mi tiempo con usted; ya conoce mi sentir.
Pero cada razón sólida contra la barbarie nazista es para usted
propaganda aliada. ¿Para qué discutir? Lo único que sostengo
es que no puede usted ser buen católico y estar por Hitler. No
le citaré hechos y fechas; le dejo esa tarea al padre Videgaray.
Pero para mí es evidente que el nazismo está contra lo más grande
y sagrado del cristianismo: la integridad del alma humana. Su
religión, Rodrigo, le enseña que Dios hizo al hombre a su imagen
y semejanza, y esto no tiene un sentido de grosero antropomorfismo,
sino que quiere decir que en el alma de cada hombre existe el
universo con toda la grandeza divina y que, por eso solo, es
inviolable. Bueno, Rodrigo, yo tampoco soy pedante, pero eso
es lo que pienso. Y no necesito probar que Hitler ha atropellado
el espíritu humano porque esos ultrajes son ya materia juzgada.
Mis palabras tienen mayor fuerza porque yo no soy católico, lo
cual no es un misterio para nadie.

El eco de sus palabras lo había despejado; se incorporó en su
sillón y continuó: ---Pero voy a atacar también uno de sus argumentos
torales, aquel que para usted, como mexicano, juzga que es un
baluarte inexpugnable. Me refiero a su antiyanquismo.

---En ese terreno voy a derrotarlo ---dijo don Rodrigo---. Soy
todo oídos.

---No seré yo quien rompa ahora lanzas por la política americana
respecto a nosotros. Es cierto: hace mucho que son los más fuertes,
pero no siempre lo fueron y durante los siglos ++xvii++ y ++xviii++ nosotros
éramos la nación más próspera y adelantada de América. Lo que
sucedió fue que ellos proclamaron su independencia y se dedicaron
a construir un país; nosotros consumamos la nuestra y nos aplicamos
a destruir el legado de la Colonia. Tenemos por lo tanto nuestra
parte de responsabilidad en el desarrollo de nuestra historia.
Yo sé que me va usted a hablar del 47; naturalmente no puedo
aplaudir esa conquista bárbara, pero nosotros no estamos exentos
de culpa; ese desastre fue un resultado de la podredumbre de
nuestros gobiernos que hacían revoluciones y peleaban por tener
el poder, mientras los yanquis trabajaban. Ahora, que gracias
a Dios nos dejaron intacta nuestra nacionalidad, nuestro legado
espiritual y también un porvenir para modelarlo a nuestro antojo.
Todavía podemos ser un gran pueblo libre.

---Alonso, no me hable usted de libertad, cuando menos de libertad
a la mexicana ---interrumpió don Rodrigo.

---Rodrigo, está usted violando las buenas reglas de la polémica
al romper la cadena de mis razonamientos. Además usted mismo
me invitó a hablar cuando yo sesteaba, por cierto muy sabrosamente.
Va usted a tener que esucharme lo que pienso de la libertad.

La libertad de que hablo no es la que nos permite elegir un alcalde
o insultar a un gendarme; ni siquiera es la libertad de la revolución
francesa, la del lema: libertad, igualdad, fraternidad. La libertad,
Rodrigo ---por la que daría mi vida--- consiste en ser libre
para forjar mis propios ideales y en ser libre para modelar mi
vida de acuerdo con ellos.

Pero me desvió usted con su interrupción inoportuna; tengo que
volver a su yankofobia para decirle que tendremos que estar con
los Estados Unidos si se trata de defender esa libertad y el
sentido cristiano de la vida, y la dignidad del espíritu humano;
que estas tres cosas, como en el misterio de la Trinidad, son
tres y son una sola. Tendremos que estar con ellos porque esas
cosas son eternas y ante ellas hay que olvidar las reyertas pasadas,
que fueron por bienes transitorios. Ahora, Rodrigo, le cedo la
palabra.

Pero Verónica intervino: ---Rodrigo, ofreciste ayer que iríamos
hoy a primera tarde a ver a mi nana Chole. Ya son las cinco.

Don Rodrigo se puso de pie y todos hicieron otro tanto.

Remigio comentó en ese momento: ---Padre, yo pienso también como
Alonso; creo que lo que dice es la verdad.

---Allá tú ---contestó don Rodrigo---. Si esa es tu convicción
haces bien en expresarla.

Alonso afirmó entonces: ---Bendita sea esa tolerancia y ojalá
la conserve usted siempre, Rodrigo---. Y en seguida a Remigio:
---Y a ti, hijo, te agradezco lo que dices; ya verás que tarde
o temprano, tu padre será de los nuestros.

Patricio repasaba su existencia. Estaba echado boca arriba con
los ojos abiertos. Estos recuerdos hacían su pena más lancinante.
{.espacio-arriba3}

</section>
<section epub:type="chapter" role="doc-chapter">

# IX

++Cuando++ los alemanes ocuparon México, el Barón Von Virchow
fue designado Jefe Supremo Civil y Militar. Tenía instrucciones
de fomentar el odio contra los americanos, de aparentar respeto
para el _Quisling_ Mexicano y de hacer una propaganda intensa de
las doctrinas hitleristas. Los invasores se equivocaron entonces
concediendo un valor excesivo al odio por el yanqui. Supusieron
cohechar también a las masas creyentes del país aparentando
una actitud amistosa para con la Iglesia Católica, haciendo alardes
artificiosos de su respeto hacia los cultos e invitando al Arzobispo
de México a celebrar con ellos conferencias en favor de la religión.
El prelado contestó que iba a recabar instrucciones de Roma y
que mientras llegaban agradecía que los fieles no fuesen perturbados
en sus prácticas. En el fondo lo que quería Von Virchow era establecer
una Iglesia Mexicana; Hitler mismo se lo había recomendado en
una larga entrevista que le concedió en Berchtesgaden. A este
respecto el Führer cometía otro craso error: muchos mexicanos
son agnósticos, en otros casos católicos tibios; tal vez sea
también exacto que las masas indígenas practican una especie
de idolatría; mas nunca cambiarán su fe por ninguna otra y menos
traicionarán al Vaticano.

Esta era la situación en 1946 y los alemanes se dieron al fin
cuenta de su error cuando ya era para ellos demasiado tarde.
La verdad es que sus quintas columnas los habían engañado respecto
a todos los puntos fundamentales en que querían basar su melosa
política de acercamiento: engañado respecto al antiyanquismo,
que si bien es cierto que existía y muy hondo, borróse como por
encanto ante la amenaza teutona; engañado respecto al sentimiento
religioso nacional y engañado finalmente en lo que al patriotismo
mexicano se refería, pues este brotó entonces de fuentes desconocidas
y profundas: fue una corriente subterránea que reveló a México,
ante el mundo, como un gran pueblo insospechado.

Los alemanes, y especialmente Von Virchow, recibieron informes
falsos, en primer lugar, de sus conterráneos residentes de larga
fecha en la república, quienes habían gozado una situación excepcional
para ser tiempos de guerra; pues mientras los escasos mexicanos
que habitaban en Alemania o en la Europa ocupada fueron despojados
de sus bienes y enviados a trabajar forzadamente en los campos
de concentración ---allí está el caso del doctor Zavalza, tan
cruelmente tratado y muerto en Prusia--- los alemanes en México,
y con ellos los muy contados japoneses e italianos establecidos
también aquí, gozaban de una vida fácil; se les había dado tiempo
de limpiar sus cuentas de Banco, de llevarlas a sitio seguro
y aún algunos ferreteros seguían realizando un comercio subrepticio
vendiendo a precios fantásticos las existencias de bodegas clandestinas.
Estos teutones hacían banquetes en sus casas en ocasión de cada
triunfo de sus ejércitos; seguían visitando a sus amigos e iban
a tomar la copa al bar del Ritz en donde se reunía la flor y
nata de los espías del Eje. Pero en cambio no conocían todavía
el espíritu del país que los había acogido y donde tan fácilmente
habían labrado sus fortunas; por eso al llegar Von Virchow llevaron
a su ánimo la convicción de que estaba en un foco de podredumbre,
en donde la traición se obtenía siempre por un puñado de monedas
o a cambio de un adarme de poder. Quizá lo afirmaban así porque
la colonia teutona medraba principalmente en las esferas donde
la prevaricación era la práctica invariable; pero a la postre
el conquistador fue a estrellarse ante una muralla que surgió
milagrosamente de las entrañas mismas de la tierra.

Los otros que dieron a los alemanes informes totalmente equivocados
fueron los infelices componentes del gobierno pelele. Y es que
los que lo formaban eran individuos que procedían de las más
pestilentes cloacas políticas, donde habían pasado la vida en
una ansia perpetua de poder. Ellos, en parte por ignorancia estúpida,
en parte por inspirar confianza a sus protectores y hacerles
creer que su tarea de dominación era sencilla con tal que tuviera
el apoyo de las bayonetas, hicieron también creer a Von Virchow
y a sus prosélitos que en el odio a los Estados Unidos estribaba
la mejor garantía de sujeción del país. Y fueron también ellos
los que aseguraron la posibilidad de una conversión total hacia
esa Iglesia Católica Mexicana, enemiga del Papa y rendida a los
pies de Hitler como un instrumento de la omnipotente Alemania.

Von Virchow, típico prusiano, engreído de vanidad, empezó a dictar
medidas de gobierno como si después del zarpazo que le permitió
apoderarse de una gran parte del territorio de México se iniciase
una era de paz. Tenía hasta cierto punto motivos para esperarlo
así. El Presidente Onésimo Cabañas pudo lograr el acatamiento
político de gran número de Estados; en otros se hizo sentir la
garra guerrera teutona. El mundo estaba atónito.

Hablábase ya de un viaje de Hitler a México. Von Virchow aspiraba
a consolidarse como el amo de América y se dispuso a poner en
obra todas aquellas empresas que él sabía eran gratas a su Führer.

Fue entonces cuando ante el sumiso Presidente Cabañas y su Secretario
de Educación Pública, el doctor Belgrano, el jefe invasor mencionó
la necesidad urgente de fundar las «Juventudes Hitlerianas de
México».

Von Virchow habíase adueñado del edificio de Relaciones Exteriores,
donde despachaba. Hacía venir a verlo a Cabañas, quien se presentaba
con lujo de aparato militar. Pero en realidad, a pesar de la
prosopopeya de esas entrevistas, iba a recibir órdenes que era
necesario ejecutar con diligencia.

Aquella mañana estaban sentados en la oficina de Von Virchow,
con este, Cabañas y Belgrano. Además se encontraba como intérprete
el teniente Seybel y un taquígrafo que iba tomando nota de la
conferencia, en alemán y en español. Seybel traducía con impecable
exactitud. La conversación tenía un ritmo lento, para dar tiempo
a las traducciones; pero Von Virchow suplía esta lentitud con
la precisión de sus órdenes.

---Señor Presidente, vamos a terminar nuestra plática tratando
un asunto importante. He deseado la presencia de su Ministro
para evitar demoras; el señor Ministro puede así tomar directamente
nota de mis instrucciones. Es absolutamente preciso proceder
desde luego a la organización de la juventud. Necesitamos un…
_bund_. Yo lo llamaría… Juventudes Hitlerianas de México, es
decir… vamos a llamarlo así.

---Todo mi trabajo ---replicó Belgrano--- consiste precisamente
en eso, en la educación de la juventud o en su organización,
para usar las mismas palabras del señor Barón.

---Entonces no nos entendemos, o mejor dicho, vamos a entendernos
sin más demoras ---contestó Von Virchow---. _Herr President_, va
usted a ordenar al señor Ministro aquí presente que nombre una
persona que proceda a la formación de un grupo juvenil semejante
al que tenemos en Alemania. Necesito un hombre joven, con cierto
prestigio. _Herr Minister_ ¿tiene usted un candidato?

---Tendría que pensarlo; de momento no se me ocurre. Se lo diré
cuando tenga el honor de verlo nuevamente, señor Barón.

---No podemos perder el tiempo ---contestó este secamente---.
Nosotros, al llegar aquí, conocemos mejor que ustedes mismos
su país y sus gentes. Yo sí tengo un candidato. Va usted a nombrar
a…

El teniente Seybel iba traduciendo frase por frase, pero al llegar
a este punto, Von Virchow lo interpeló directamente.

---¿_Wie heisst die Person, über welche ich die detaillierte
Erkundigung erhielt, die Sie gestern übersetzen_?

Seybel contestó:

---_Sie heisst_ Patricio Guerrero.

El Barón continuó dirigiéndose al doctor Belgrano:

---_Herr Doktor_, va usted a nombrar para este puesto al señor
Patricio Guerrero. Al salir el teniente Seybel le dará a usted
informes sobre quién es y dónde puede encontrarlo. Asígnele usted
un sueldo atractivo, digamos unos dos mil pesos. Y envíemelo
cuanto antes; deseo hablar con él.

Cuando se quedó solo el Barón Von Virchow tomó unas páginas que
estaban sobre su mesa y repasó algunos de los párrafos. Era la
traducción del teniente Seybel:

«Su padre es el licenciado don Rodrigo Guerrero, notario de San
Miguel, uno de los pueblos con más arraigada tradición en el
Estado de Guanajuato. El licenciado Guerrero nunca ha ocultado
sus simpatías por nuestro Führer ni su odio por los ingleses
y los americanos».

Más adelante se leía:

«Patricio Guerrero se ha educado en este ambiente de admiración
por Alemania su obra como fomentador de las organizaciones de
_boy scouts_ es notable. Se calcula que ha podido reunir, casi
sin ayuda pecuniaria, más de cincuenta mil asociados en toda
la república. Patricio Guerrero puede transformar este grupo
en un _bund_ hitleriano si se logra convencerlo de que lo haga,
pues su prestigio es indiscutible y ante los _boy scouts_ mexicanos
su voluntad es ley».

Había algunos datos sobre la vida íntima de Patricio:

«Está casado con una muchacha de su mismo pueblo, llamada Eligia
Santiesteban, de raza inferior e indudablemente contaminada con
sangre indígena. Tienen un hijo de cuatro años llamado Arturo.
Patricio está absolutamente consagrado a su familia. No se le
conocen ningunas relaciones femeninas y parece estar profundamente
enamorado de su mujer».

Estas eran las informaciones de las quintas columnas que venían
trabajando en México de tiempo atrás. Von Virchow, después de
echar una ojeada rápida a muchas de esas frases, murmuró entre
dientes:

---_Wir werden sehen ob dieser unser Mann ist_.

Al mismo tiempo tocó el timbre y continuó su trabajo.

A los dos días, y por instrucciones del doctor Belgrano, Patricio
Guerrero se presentó en el edificio de la Avenida Juárez.

Aquello era un hormiguero de uniformes. A la entrada se le interrogó
con detenimiento. ¿Quién era? ¿A quién buscaba? Patricio mostró
la tarjeta que le había entregado el Secretario de Educación.
Se le condujo entonces, por la escalera, al primer piso, tropezando
con militares que subían y bajaban. Los pasillos hervían con
toda clase de gentes que formaban corrillos y llenaban con el
rumor de sus pláticas el oscuro patio cubierto. Un ujier recibió
instrucciones de anunciarlo.

Patricio esperó en los sombríos corredores con una ansia casi
angustiosa; pero no se le hizo esperar mucho. A los pocos minutos
un ayudante pronunció su nombre y se le condujo entonces por
una antesala iluminada artificialmente hasta el despacho mismo
de Von Virchow.

Este lo recibió sentado, con la espalda casi hacia la ventana;
la luz ponía grandes sombras en su duro semblante. A su lado
se encontraba un muchacho como de treinta años, alto y bien hecho,
con cabellos rubios clarísimos y transparentes ojos grises; vestía
el uniforme de los oficiales nazis y estaba de pie, en actitud
militar.

Patricio se detuvo ante la mesa de trabajo que el Jefe del Ejército
de ocupación tenía atestada de papeles.

Von Virchow no lo invitó a sentarse. Comenzó a hablar en alemán
con un tono firme de tranquila deliberación. El oficial uniformado
de rostro nórdico iba traduciendo, con voz impersonal, y en el
más claro español, frase por frase.

---¿Usted es Patricio Guerrero?

---Sí, señor.

---¿Hijo del señor licenciado Rodrigo Guerrero, de San Miguel?

---Sí, señor.

---¿Usted es jefe de una organización juvenil?

---Sí, señor. De los Boy-Scouts Mexicanos ---y agregó con orgullo:
---Tengo cincuenta y tres mil ochocientos veinticinco afiliados.

---¡_Gut_! ¿Habla usted alemán?

---Ni una palabra.

---¿Otros idiomas?

---Inglés e italiano. Algo de francés.

---¡_Gut_!

Después, como dirigiéndose a su intérprete. ---_Man kann nicht
sagen, dass das itälinische vom nützlichstem sei_---. Y en seguida:

---¿Sabe usted que los _boy scouts_ son una organización de una
democracia degenerada y que con ella se corrompe a la juventud?

Patricio frunció el ceño, herido en lo más hondo de sus sentimientos,
y no contestó.

---¡_Gut_! ¡_Gut_! ---siguió diciendo Von Virchow como complacido
en extremo por la reacción de Patricio---. Vamos a convertir
ese grupo en una institución gigantesca; primero en todo México,
más tarde en la América latina. Se le ha hecho a usted el honor
de designarlo para dirigir este movimiento, pues goza usted de
mi confianza y de la de nuestro Führer. Nuestra institución se
llamará Juventudes Hitlerianas de México.

Le proporcionarán a usted los escritos de Von Schirach; ellos
le servirán de guía precisa para orientar su labor; el espíritu
de los _boy scouts_ será sustituído por otro infinitamente superior.

Ahora, hablemos de dos o tres pequeños detalles. Tomará usted
un profesor de alemán y dedicará usted al aprendizaje de nuestro
idioma todo el tiempo que su trabajo le deje libre. ¿Entendido?

---Sí, señor.

---¡_Gut_! Tiene usted fondos ilimitados; no se detenga ante ningún
gasto para equipo, locales, personal. Óigalo usted; fondos ilimitados.

Patricio se sintió transportado. ---Barón, es lo que necesito.
Con esta ayuda yo le respondo de mi organización.

---¿Cuánto le asignó de sueldo el señor doctor Belgrano?

---Mil pesos---. Von Virchow rió ligeramente y murmuró:

---¡_Der dumme alte Geizhals_! Bueno, señor Guerrero; de mis oficinas
le entregarán a usted otra cantidad igual cada mes. Y quiero
decirle que si responde usted a mi confianza ese subsidio personal
también será ilimitado.

---Gracias ---contestó Patricio---. Esos dos mil pesos es más
que lo que puedo gastar.

---¡_Gut_! ---exclamó Von Virchow sonriendo---. Finalmente ---agregó
el jefe nazi, señalando al intérprete--- le presento al subteniente
Wilfrido Müller. Va a ser su colaborador; le va a ser muy útil
para que conozca usted el espíritu del nuevo orden que vamos
a implantar en América; en ese Nuevo Orden las Juventudes Hitlerianas
de México serán un factor de gran importancia. Müller nació en
México, pero ha crecido en el amor a nuestro Führer y está educado
en el credo nazi. Espero que sean buenos camaradas.

Wilfrido Müller fue traduciendo con meticulosa fidelidad todas
las palabras de Von Virchow. Al final este ordenó: ---Pueden
ustedes retirarse.

Müller alzó el brazo diciendo ¡Heil Hitler!; Patricio se inclinó
en silencio y los dos muchachos abandonaron el despacho, en tanto
que por una puerta en el fondo un oficial penetraba para recibir
órdenes del Barón.

El muchacho alemán y Patricio salieron juntos sin dirigirse la
palabra y así llegaron hasta la acera, en la Avenida Juárez.
Fue Wilfrido quien rompió el silencio con un tono cordial y alegre.

---¡Bien! Parece que vamos a vivir casi juntos. Tenemos que ser
hermanos siameses. Mi primera proposición es que nos hablemos
de tú---. Y extendió su mano, que Patricio estrechó con efusión.
Wilfrido continuó: ---Ahora estás anonadado como es natural.
Vete y piensa en tu dicha; puedes echar tu imaginación a volar,
hay razón para ello. Te buscaré en tu casa---. Y dando un último
apretón a la mano de Patricio, que había conservado en la suya,
tomó hacia el centro de la ciudad encendiendo un cigarrillo.

Patricio echó a andar lentamente, sin rumbo. Se dio cuenta de
que Wilfrido no conocía la dirección de su casa y volvió la cabeza,
a ver si lo alcanzaba, pero ya aquel se había perdido entre los
transeúntes.

Entonces siguió por el Paseo de la Reforma. Eran las once de
la mañana y hacía sol. Por todas partes encontraba soldados alemanes;
los automóviles no eran muy numerosos, pero se racionaba la gasolina
menos que los alimentos y la avenida presentaba un aspecto de
relativa animación. Su primer impulso fue correr para ver a Eligia;
después pensó que era necesario poner en orden sus ideas y fue
caminando paso a paso por la ancha acera norte.

Una indescriptible elación lo dominaba; ¡sus _scouts_, sus _scouts_!
Después un pensamiento lo ensombreció; recordaba su entrevista
con Von Virchow. ¡Cambiar el nombre de sus patrullas! ¡Juventudes
Hitlerianas de México! Pero, después de todo, eso era lógico.
Patricio se sentía seguro de que mientras él fuese la cabeza
no se desvirtuaría su obra. Y sería una obra mexicana, porque
la influencia yanqui había terminado para siempre; los alemanes
lo habían ofrecido así.

La ocasión de que le habló siempre su padre había llegado; la
ocasión de sacudir la tutela del Tío Sam. En realidad ---se decía---
nuestra soberanía está ahora más intacta que nunca; nos hemos
librado de un gobierno que no contaba sino con el poder de los
Estados Unidos logrado a fuerza de concesiones vergonzosas. Los
alemanes han llegado justamente a tiempo para evitarnos la humillación
de ver a los soldados del vecino ocupar nuestras ciudades con
un solapado pretexto de defensa. Es cierto que Cabañas, el Presidente,
no puede satisfacernos; hasta hoy había sido un perpetuo fracasado
en política y todos sabían que su periódico se sostenía con el
dinero alemán. Pero la ley ha sido respetada y según ella habrá
elecciones y nos daremos el presidente que México merece.

Ahora a trabajar. Las frases de Von Virchow le danzaban ante
los ojos. Fondos ilimitados. Tendremos equipos; buenos vestidos
para las excursiones: esquís que daremos a las patrullas del Popo,
bastones y cables destinados a los alpinistas; chaquetones de
cuero para los días fríos, sacos en los que los chicos dormirán
a la intemperie, navajas de monte, machetes para abrir las malezas.
Pensaba en los guantes de box, en los zapatos de fútbol, en
las raquetas, en las pelotas. Podría ahora construir albercas,
campos de juego y frontones.. ---Y volvía a recordar a Von Virchow---.
Es cierto… tengo que leer la propaganda de ese tipo… ¿Cómo dijo
que se llamaba?… Von. ¿qué? Bueno ya me lo dirá Müller, pero
con certeza será excelente; los resultados en Alemania lo demuestran;
gracias a eso han dominado al mundo… ¿Y Wilfrido? ¿Quién sería
Wilfrido? Parecía un buen mozo, afable y cordial. En realidad
no hay por qué desconfiar de él. Volvían a su memoria las palabras
de Von Virchow: ---«que sean buenos camaradas».

Estos pensamientos revoloteaban en su mente, una y otra y otra
vez. El seguía caminando. Pasó la glorieta de Cuauhtémoc y llegó
a la columna de la Independencia. Tomó por las calles del Tíber
y después por las de Lerma; allí, cerca de la Calzada de la Verónica,
vivía en una pequeña vivienda.

Eligia misma salió a recibirlo. ---Patri, llegas muy temprano
---le dijo al tiempo que lo besaba en los labios. Y añadió sonriente:
---¿malas noticias? No te veo alegre. Dime.

---No, no son malas, Eligia. Pero son tan extraordinarias que
vengo anonadado; ni siquiera puedes ver si vengo contento. ¿Y
el niño?

---Lo mandé al parque; la mañana está tan linda. Pero a ver,
cuéntame.

---Vas a tener paciencia. Vamos por Arturo, comeremos y después
te contaré esta historia de las mil y una noches.

---¿Así es de maravillosa?

---No sé si será maravillosa, ---replicó Patricio---, pero sí
es increíble.

---No, no te dejo salir, ---le dijo Eligia, excitada---. Me voy
a morir si no me cuentas algo ahora mismo.

Entonces él rió apaciblemente, abrió la puerta a la calle y sacó
a su mujer sujetándola dulcemente por una muñeca.

---Pero hijo, estoy hecha un desastre; mira que estaba en la
cocina y traigo el delantal.

Patricio le desanudó el delantal, hizo con él un pequeño lío
que metió en un bolsillo de su pantalón y rodeándola con su brazo
por sobre el hombro echó a andar con ella diciéndole: ---Anda,
vamos a que respires el buen aire de Dios.

Después de comer Patricio se acercó a Eligia, la tomó en sus
brazos y fue a sentarse en el sofá de la estancia, arrullándola.
---Ahora escúchame y pon atención; no pierdas una sola palabra.

Y relatóle su entrevista con Von Virchow; el aspecto de la antigua
Secretaría de Relaciones; los uniformes que cruzaban el patio,
los automóviles que llegaban sin cesar hasta el pie de los ascensores,
los mexicanos a quienes él no conocía ---que se distinguían de
los demás por sus trajes de civiles y su aire desconcertado---
esperando audiencias en las antesalas. Le refirió en seguida
con fidelidad su conversación con Von Virchow y finalmente le
describió a Wilfrido Müller.

Entonces calló. Eligia estaba silenciosa. El la interrogó con
la mirada. Ahora me toca a mí sentirme anonadada ---dijo ella---.
Espero que esto sea la gran oportunidad de tu vida; mejor dicho,
de nuestra vida… Sin embargo… ---y quedó suspensa… ---Dilo, dilo
---saltó Patricio---dime lo que piensas.

---Bueno, no es nada, pero te lo diré. Estoy pensando que esa
conversación me habría gustado mucho más si la hubieses tenido
con el Presidente; me refiero a Cabañas.

Patricio replicó con excitación. ---¿Qué quieres que se hable
con Cabañas? La idea de un movimiento juvenil no puede caberle
en el magín. Es un idiota. Afortunadamente las elecciones se
harán pronto.

---Está bien, Patri. Tú vas a trabajar, como siempre, y yo voy
a pedirle a Dios que cuando tengamos un nuevo Presidente él comprenda
lo que haces y que tú te entiendas con él.

---Hijita mía ---concluyó Patricio--- con mi primer mes de sueldo
te vas a comprar ropa y vas a buscar otra casa.

---Me compraré ropa y te compraré ropa, pero nos quedaremos aquí;
yo vivo muy bien.

Y con eso cerraron su charla sobre el inesperado acontecimiento.
Patricio no se sentía decepcionado por la acogida de Eligia;
toda su actitud era de ternura. Pero una extraña incertidumbre
lo inquietaba.

Esa noche Wilfrido Müller lo llamó por teléfono. La cita que
hicieron para la mañana siguiente fue como el tirón al llamador
de un arma. La energía latente de Patricio púsose en acción;
encontróse en un mundo desconocido, en el que sus órdenes eran
acatadas sin réplica y en el cual los cosas surgían de la nada,
como en un nuevo cuento de Aladino. Hubo fábricas enteras trabajando
para él, flotillas de camiones cargados de equipo, rotativas
que lanzaban cientos de miles de folletos. Le entregaron para
su tarea las oficinas de Chapultepec, perchadas encima del cerro.
Allí permanecía dieciséis o dieciocho horas diarias. En ocasiones
el trabajo lo absorbía en tal forma que a deshora telefoneaba
a Eligia que no lo esperase y dormía en un catre de campaña dentro
de su despacho. Dos veces al día, en la mañana y en la tarde,
se presentaba Elsa, la profesora de alemán recomendada por Wilfrido,
una hermosa muchacha que parecía la heroína de un cuento de Grim
y Patricio se aplicaba duramente a las declinaciones. Wilfrido
lo secundaba con una eficacia impersonal; discutía con él los
problemas, lo acompañaba a ver a funcionarios alemanes, estaba
siempre presente en las conferencias con Cabañas o Belgrano y
sobre todo lo imbuía de la filosofía nazista.

Patricio se hizo con rapidez a las ideas sobre la pureza de la
sangre y el destino de la raza. Comenzó a creer que una predestinación
lo había llevado al lugar de los escogidos; leyó sobre el nuevo
cristianismo que iba a cobijar al mundo: un cristianismo ario,
bajo la divina égida de Hitler, su Cristo; se acrecentó su odio
por el bolchevismo y las democracias: todo estaba siendo sustituído
por el Nuevo Orden. Al mismo tiempo trabajaba con energía extrahumana:
gigantescas cantidades de material se despachaban para las Juventudes
Hitlerianas de México a las ciudades y pueblos dominados por
los nazis. Patricio salía muchas madrugadas en un avión que estaba
siempre presto y pasaba una revista a patrullas de Michoacán
o de Tabasco, para regresar a las once a despachar frente a su
mesa. Mientras tanto los carros de ferrocarril iban derramando
por todas partes millares y millares de insignias y panfletos;
los rifles y las pistolas de aire comprimido se repartían pródigamente
a los chiquillos de ocho a doce años; se abrían concursos de
tiro, se organizaban grandes maniobras infantiles, se simulaban
tomas de villorrios por batallones de muchachitos que recorrían
en bicicleta los caminos, manejaban ametralladoras fabricadas
ex profeso con cartuchos de salva y se lanzaban unos contra otros
con ferocidad de adultos. Patricio preparó también la jura de
la bandera, una nueva bandera con los colores nacionales y el
águila venerable sobre el campo blanco, pero con dos suásticas
negras en el rojo y el verde. A esa jura vinieron doscientos
mil niños del territorio que ellos llamaban libre, pero que en
realidad era el ocupado por el invasor.

Ante un gran templete, levantado en la Plaza de la Constitución,
desfiló ese inmenso ejército infantil. En el templete estaba
Cabañas; a su derecha se sentaba Von Virchow, a su izquierda
Patricio; atrás todos los miembros de ese gobierno pelele. Aquel
día fue la fugaz apoteosis de las Juventudes Hitlerianas de México
y quizá también el momento culminante de esa dominación nazi.

En su embriaguez de actividad Patricio no podía analizar ni el
alcance ni la calidad del esfuerzo. Por otra parte tampoco se
daba cuenta de lo electrizado que estaba el ambiente; las hazañas
insurgentes iban convirtiéndose día a día en un serio problema.
Los Estados Unidos, repuestos un poco de la sorpresa del desembarque
en América, dedicaron íntegramente su potencia industrial a la
fabricación de armamentos ---aviones de preferencia--- y los
ataques a los convoyes nazis eran inexorables. El salto al Brasil
se hacía cada día más eventual y aunque se calculaba que los
invasores trasladaban al continente aparatos en número ilimitado,
era cierto también que las pérdidas por los ataques de la aviación
americana y canadiense no se cubrían con la acostumbrada eficacia.

Patricio, entregado en cuerpo y alma a su propaganda y a su trabajo,
leía apenas los escasos periódicos ---todos los que se publicaban
era de ideología nazi y ocultaban las noticias adversas al invasor---
y cuando escuchaba las radiodifusiones eran siempre las de fuentes
alemanas. Estaba por lo mismo tranquilo y ni por un momento podía
imaginarse que la situación del gobierno pelele pudiera peligrar.
Al ver los elementos que habían puesto en sus manos para las
Juventudes de México, al ver cómo en meses y aun en semanas se
erigían campos y edificios donde antes nada existía, y también
los millones que sin cortapisa se gastaban en esa obra, privaba
en su mente la impresión de una fuerza ilimitada.

Los invasores procuraron envenenar a las masas con noticias ad
hoc; con fiestas populares, con eventos deportivos y con películas
ilustrativas de la grandiosidad de esa civilización que aportaría
la raza dominadora. Las gentes acudían, es cierto; pero en la
oscuridad de los cines se siseaba al Führer y en los encuentros
de fútbol los equipos alemanes eran silbados implacablemente.
Un poco más tarde comenzaron en la misma ciudad de México los
actos de sabotaje.

Pero Patricio estaba embriagado; los tapujos del triunfo no le
permitían ver sino su estrecho sendero. Trabajaba sin cesar y
después de la titánica labor desarrollada se disponía a hacer
una serie de visitas a ciudades de la provincia; quería ver por
sí mismo cómo crecían esos grupos juveniles a los que todo les
había proporcionado.

Wilfrido era como su otro yo; un muchacho extraño que parecía
ajeno a los halagos del éxito clamoroso y al acicate de la ambición.
Colaboraba con una actitud desprendida, carente de iniciativas,
pero sin que ningún detalle se le escapase. Era la suya una amistad
asidua, pero recatada. De cuando en cuando charlaba largamente
con Patricio; este, de carácter menos reservado, le mostraba
su pensamiento al desnudo, mientras Wilfrido oía con atención
y le hacía comentarios. Patricio era ingenuo y en su avidez de
encontrar un espíritu amigo que compartiese un poco su responsabilidad,
se entregaba sin ambages al criollo teutón.

Iban con frecuencia a beber a una cervecería recién abierta en
la Avenida Insurgentes. La entrada solo era permitida a los oficiales
alemanes, pero el rango de Patricio les franqueaba las puertas.
Allí pasaban una hora cuando el trabajo era de tal modo abrumador
que un breve descanso se hacía imperioso.

Wilfrido abordaba sus temas de filosofía nazi. Después de haber
bebido un poco ponía su mano sobre la de Patricio con un ademán
fraternal y le decía:

---¡Qué fortuna para ti, Patricio, el estar con nosotros!---
Y en seguida, bromeando: ---Lo único que harás será cambiarte
ese nombre de patrón de Irlanda; necesitas un apelativo fuerte:
Federico o Wilfrido ---como yo--- o Hugo. Es menester que te
sientas alemán; recuerda que los alemanes «somos la sal de la
tierra».

La cerveza le prestaba una ligera borrachera alegre.

---A ver, usted -dirigiéndose al mozo--- otros tarros.

Patricio contestaba con tono apacible: ---Pero Wilfrido, si tú
apenas si eres alemán. Tu padre lo es, pero aquí hizo su fortuna.
Y tu madre nació en México, aunque tus dos abuelos maternos hayan
sido alemanes. Y tú naciste aquí.

---Es cierto, Patricio. Pero por eso soy alemán; porque mis padres
lo son; y mis abuelos y antepasados lo fueron también durante
generaciones sin número. Soy ario, Patricio---. Y sus ojos despedían
fulgores. ---Soy ario---. Hacía una pausa como para aspirar esa
palabra y llenarse con ella y proseguía en un tono sosegado:
---Ustedes, los que vienen de razas mestizas, no pueden comprender
esa fuerza oculta de la sangre. A ti puedo decírtelo, porque
dentro de la relatividad de las cosas eres aquí en México de
una casta superior. Te has ganado nuestra confianza y tienes
ante ti un porvenir espléndido. Cuando hayamos acabado con la
última resistencia yanqui y la paz se haga, no habrá nada en
este país que tú no puedas conseguir. Nosotros dejaremos México
para los mexicanos que nos han sido fieles. A quien vamos a humillar
es al cochino Tío Sam.

---Creo que fundamentalmente tienes razón ---replicaba Patricio---.
Aunque los mexicanos no somos militaristas no por eso dejamos
de admirar la conquista obtenida por el sacrificio de tu pueblo.

---No, ---interrumpía Wilfrido, con los ojos iluminados---. Estás
hablando como un burgués demócrata. Obtenida por la predestinación,
por la sangre y por la raza.

Al escuchar a Wilfrido, que le dispensaba un trato de amigo protector,
Patricio resentía en ocasiones una especie de humillación escondida.
¡Se proclamaba de tal modo por encima de él! No se decidía, en
su espíritu, a articular las palabras justas, pero estas venían
a su mente fustigándolo, como reminiscencias dolorosas y fugaces:
conquista e invasión, conquista e invasión. Cierto; el lenguaje
de Müller era el del invasor y del conquistador. Mas por otro
lado lo trataban tan espléndidamente y delegaban en él una autoridad
tan indiscutible, que estos análisis mentales se esfumaban ante
la magnificencia de los sueños realizados.

A veces ese orgullo lo contagiaba. ---¡Estoy con ellos!--- se
decía. A París le llamaban la Ciudad Luz, pero esta guerra nos
ha demostrado que eso era solo una exterioridad. Hay una Nación
Luz y es Alemania. Al lado de ella México va a transformarse;
acabaremos con el yanqui y después de la paz seremos la avanzada
del continente.

Sin embargo, algunas discusiones con Wilfrido lo llenaban de
confusión. El le decía: ---tendrás que aprender a desechar tu
culto por la paz burguesa y el trabajo bíblico. A veces hablas
como un judío del Talmud. El mundo no volverá nunca al trabajo
como las democracias lo entienden; los arios seguiremos luchando
siempre hasta que el último terrón de tierra de este planeta
sea un terrón alemán.

---¿Y entonces? ---interpeló Patricio subrayando las frases---
¿cuando el último terrón de tierra del planeta sea un terrón
alemán?

---¡Ah! Entonces… entonces. ¡Quién sabe!. ---Somos la Raza Universal.

Patricio salía de la cervecería sacudiendo la cabeza y asentando
bien las plantas de los pies sobre el pavimento de las aceras.

En todo este tiempo, desde su primera visita a Von Virchow, solo
una ocasión había estado en San Miguel. Deseaba hacer algo excepcional
por sus antiguos _boy scouts_. Fue a alojarse a su pieza, donde
todo lo encontró meticulosamente intacto, como cuando era estudiante.
Pero allí no se le acogió como un héroe, ni siquiera como un
personaje.

Encontró un ambiente de opresora tristeza. Sin previo acuerdo
hubo una conspiración de silencio y nadie le mencionó las vejaciones
que el pueblo había sufrido. Nadie le habló de las listas del
cuartel, ni del incendio de los libros. Verónica lo abrazó y
besó con la efusión de siempre, quizá con mayor ternura, como
al hijo que está en peligro. Ana pasaba unos días allí, mientras
su marido trabajaba en León, y tejía ropa para su primera criatura;
Patricio la apretó contra su pecho y la besó en la frente y en
los negros cabellos. Remigio estaba fuera y nadie hablaba de
él; pero su ausencia era el secreto de todos.

Patricio estuvo a ver a Körner, le habló del arreglo de un campo
de aterrizaje a un lado de la carretera a Querétaro y trató de
interesarlo en sus otros propósitos, pero al oficial alemán solo
le preocupaba mantener el orden en medio de un vecindario cuya
oculta hostilidad adivinaba.

Fue a la Parroquia a charlar con el cura Videgaray quien lo recibió
afablemente, le recomendó que cuidara a su padre, a quien veía
delicado, pero no le mencionó ninguna de las inquietantes cuestiones
que estaban en la mente de todos.

Fue a visitarlo únicamente Melquiades de la Peña, el virulento
_quisling_ de San Miguel quien le pidió su apoyo cerca de los jefes
en la Capital. Se puso en contacto con sus muchachos de otrora,
pero no encontró en ellos el entusiasmo que esperaba: seguían
llamándose los _boy scouts_ de San Miguel y haciendo sus marchas
por los alrededores. Patricio les repartió cuchillos de monte,
cantimploras y brújulas; eso deshizo un poco el hielo y entró
a detalles de organización hasta dejar cimentado el grupo de
Juventudes Hitlerianas. Pero los chicos de San Miguel continuaron
siendo siempre un poco anárquicos e indisciplinados, aún después
de los subsecuentes viajes de Patricio. Ninguno de sus viejos
camaradas estuvo a saludarlo y la actitud del pueblo pesó sobre
su ánimo, no obstante que él trataba de explicársela por razones
simples, entre otras la de que nadie es profeta en su tierra.
Pero era un hecho que esa acogida contrastaba con la de muchos
otros lugares, en donde siendo un desconocido se le recibía con
homenajes como si fuese un gran señor.

En la noche charló con su padre. Había en su voz un dejo de amargura.
---Parece como si yo no fuera hijo de este lugar. Encuentro a
las gentes reservadas. No se me habla con franqueza. He venido
a hacer un bien a San Miguel aprovechando medios de acción que
están en mis manos; no me habría perdonado echar en olvido este
pueblo donde nací. Y por supuesto que mi obra se realizará y
volveré cuantas ocasiones sea preciso. Pero me voy un poco desencantado.

Don Rodrigo le contestó: ---Hijo mío, vivimos tiempos desconcertantes.
La gente no sabe a ciencia cierta dónde está el bien y dónde
está el mal, y aquí no hay simpatías por los alemanes; quizá
sea porque el jefe militar ---ese tal Körner--- es un tipo brutal
y autoritario. Pero yo me limito a referirte los hechos. Mis
opiniones no te son desconocidas; de continuo me oyeron ustedes
hablar del orden y de la disciplina como las bases de cualquier
gobierno. Lo que tú has podido hacer se lo debes a esas dos virtudes.
También sabes que he alimentado siempre en mí un sentimiento
de aversión hacia los yanquis. Pero ese es mi caso y no puedo
hacer generalizaciones. Los hombres viven confundidos. Yo lo
único que deseo es que se haga la paz, que las conciencias se
tranquilicen y que Dios proteja a este país.

Patricio encontró esta actitud de su padre un poco elusiva, pero
no la comentó. Se proponía volver a San Miguel y hacer sentir
toda la fuerza de su posición. Será para hacerles el bien a su
pesar ---se decía.

Al siguiente día, después del almuerzo, salió en automóvil a
Querétaro, donde tomó un avión que lo condujo a México.

Patricio tendido en el diván hubiera querido llorar; pero su
corazón estaba reseco. Su mirada vagaba en la media luz del
estudio. Eligia abrió la puerta y se acercó a él suavemente,
deslizándose igual que una sombra. Patricio cerró los ojos como
si durmiera. {.espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# X

++Las++ gentes de San Miguel antes de la ocupación estaban habituadas
a una vida libre y sin cuidados. José el carpintero y Pancho
el mecánico cuando se sentían de vena para el trabajo tenían
abiertos sus talleres hasta la medianoche; pero ambos eran pescadores
y si se les antojaba echábanse las cañas al hombro y se iban
a pescar a la presa de Begoña que se prolonga hasta las orillas
del pueblo. En esos días nada podría obligarlos a dar un martillazo,
ni para enderezar un guardafango ni para meter un clavo. Durante
las fiestas anuales los indios venían a bailar ante la estatua
de Fray Juan de San Miguel, en el atrio de la parroquia, y entonces
no solo José y Pancho, sino todos los otros carpinteros, Miguel
el herrero y los otros herreros, los tejedores y tejedoras, los
zapateros, los alfareros y Francisco Ordóñez, el sastre, cerraban
sus obradores para ir a las misas, seguir a los danzantes por
las calles y charlar en el mercado. Unicamente Eulogio el panadero
y sus aprendices mantenían el amasijo siempre abierto porque
eran del oficio por atavismo y a través de muchas generaciones
sabían que a un poblado no se le puede dejar sin pan de sal.

Pero al llegar los nazis los sanmigueleños se sintieron atacados
en su libre albedrío. Se hizo obligatoria una jornada de doce
horas pues el país entero debería conquistar con el trabajo de
sol a sol el privilegio de sostener a los ejércitos boches y
a la corrompida burocracia del gobierno pelele.

En la fábrica de la Aurora se establecieron dos turnos y las
labores se realizaban ante el ojo avizor de la soldadesca. De
vez en cuando el mismo Körner visitaba esta manufactura y daba
órdenes con voz estentórea. El primer incidente que enajenó a
los alemanes las voluntades de un cierto número de trabajadores
que sinceramente creían en el odio a los yanquis, suscitóse durante
una de esas visitas. Le pareció a Körner que una de las obreras
no atendía su telar con la debida atención y en lugar de hablarle
la fustigó ligeramente con la punta del fuete.

La mujer se volvió, rápida como un animal que tira un zarpazo,
se lo arrebató de las manos y lo arrojó a lo lejos, por encima
de las máquinas en marcha. Entonces Körner mandó suspender el
trabajo, hizo alinear en la gran explanada frente al edificio
a los doscientos operarios de ese turno y a la vista de todos
mandó dar veinticinco azotes sobre su espalda desnuda a la obrera
rebelde. En seguida ordenó que se reanudaran las labores.

Allí se producía dril para las tropas de ocupación; la filatura
era considerada una industria de guerra y los alemanes habían
confiscado todas las fábricas. En San Miguel los trabajadores
pretendieron desde un principio echarse de lleno al sabotaje,
pero Francisco Toledo, uno de los jefes mecánicos, pudo reprimir
esas ansias prematuras y en apariencia la producción se continuaba
normalmente. Un hecho insólito vino sin embargo a desquiciar
esa tranquilidad ficticia: cierta tarde, al terminar el turno
de día, todos los encargados de telares fueron quintados y con
los elegidos por el sorteo se formó un grupo de veintitantos.
Inmediatamente se les hizo subir a tres camiones, los cuales,
evitando hasta donde fue posible pasar por las calles más céntricas,
tomaron la carretera rumbo a Querétaro.

Estos obreros iban custodiados por esbirros y se les advirtió
que un grito o una protesta les costaría la vida. De manera que
sus familias se quedaron ignorantes respecto a la suerte que
correrían.

Más tarde súpose que fueron trasladados a Orizaba en donde habiéndose
ejecutado a cincuenta trabajadores como castigo por varios actos
de sabotaje, hubo que sustituirlos con otros de distintas partes
del país. Esta fue la primera de una serie de deportaciones que,
al enconarse la lucha, tuvieron caracteres de inaudita crueldad.

Don Rodrigo seguía presentándose dos veces diarias en el cuartel
del Convento de las Monjas. A veces se le hacía esperar más de
una hora para que firmara la lista.

Después de la primera visita que Patricio hizo a San Miguel,
la firma de la tarde se fijó a las seis, porque nadie, después
de las siete podía salir a la calle si no era con permiso especial.
Comenzó a tener serias inquietudes respecto a su porvenir; el
trabajo escaseaba y en la existencia del orden nuevo su clientela
como penalista había disminuído, pues nadie reprimía los delitos
de sangre, a no ser que fuesen contra el invasor y en estos casos
la justicia era expedita y unilateral.

Don Rodrigo veía disminuir sus ahorros y pedía a la Providencia
que la vida recobrase su ritmo ordinario antes de que la pobreza
tocase a sus puertas.

San Miguel tenía un aspecto de desusada soledad. Don Rodrigo,
en la noche, se encerraba en la que fue su biblioteca y conectaba
su aparato para escuchar el noticiario de las once. Así supo
que de momento los propósitos del invasor estribaban únicamente
en consolidar sus posiciones. El gobierno pelele, a su vez, hablaba
de preparativos para un avance gigantesco hacia territorio del
Tío Sam; podía colegirse que los adversarios medían sus fuerzas
para el choque que había de venir.

Después de la partida de Remigio, don Rodrigo pensó que su actitud
con Patricio no había sido franca, pues la charla que sostuvieron
durante su visita al pueblo fue superficial y no penetró al fondo
de las cosas.

Cuando Patricio pedía una explicación clara y se quejaba del
ambiente de reserva que había encontrado, él, Rodrigo, eludió
mencionar las verdaderas causas de ese malestar. Se propuso subsanar
su falta en la primera coyuntura; pero antes de ver nuevamente
a Patricio tenía que sufrir otras vejaciones.

Primeramente fue despojado de su radio, el procedimiento era
de sobra conocido: allanamiento de su casa, de esa casa que hacía
apenas unos meses él había ofrecido a Maclovio como un recinto
inviolable. En esa ocasión increpó con dureza a los esbirros:
los llamó ladrones y llegó hasta decir que nunca, ni siquiera
de los yanquis, habían recibido los mexicanos semejantes agravios.
Pero lo dejaron desahogar su indignación y se quedó solo rumiando
sus propias palabras; como en un destello fugaz vino a su espíritu
la idea de que estaba sufriendo una metamorfosis.

Tenía aún que soportar una privación, que en su insignificancia
le fue excesivamente dolorosa. Una mañana que entró al express
a recoger la remesa periódica de tabacos que recibía de Jalapa,
se le anunció que había sido confiscada por el cuartel general.
Era su reserva de dos meses. Logró ver al capitán Herrmann, pero
lo único que obtuvo fue el informe de que la orden provenía de
Körner y que ese tabaco se destinaba a los soldados en el frente.
Rodrigo salió con una cólera mal contenida, cierto de que la
ratería era en provecho personal del jefe invasor.

Comentaba con sus amigos estos sucesos y se expresaba en términos
inequívocos: los sanmigueleños no podían seguir tolerando esa
tiranía. Cuando hablaba con el cura Videgaray este le recomendaba
calma; su psicología era la de los hombres sencillos, que no
conocen el mal, pero que van acumulando energías cuando son acosados.
Las dificultades siempre en aumento para llenar las rutinarias
necesidades de la vida diaria lo enervaban y la actitud apacible
de Verónica parecía excitarlo en lugar de proporcionarle quietud.

Fue entonces cuando Patricio regresó y el disentimiento fue inevitable.
Patricio venía agobiado; el trabajo lo arrastraba como una corriente
incontenible. Cada día se le exigía más, cada día, con mayor
ahínco, el invasor trataba de disfrazar con actividades de tiempo
de paz un estado de intensa desazón; y por eso hacía de las Juventudes
Hitlerianas de México un movimiento de actividad gigantesca.
Pero Patricio comenzaba a notar un oculto antagonismo y por otra
parte se interrogaba y en muchas ocasiones, en lo íntimo de su
espíritu, no podía contestarse.

Las observaciones de su padre lo irritaron; no quería admitir
que sus palabras tuviesen un fondo de justicia. Embriagado de
poder burocrático no se confesaba que su vida era un enorme error
y menos lo confesaba a su padre. Había en su actitud una mezcla
confusa de vanidad, de energía, de cansancio, de orgullo, de
obstinación. Era la máquina que lo trituraba. Recordaba las frases
del Doctor Ley, uno de los clásicos de la pedagogía nazi, y las
juzgaba abominables: «comenzamos con el niño cuando tiene tres
años… de ahí pasa a la escuela, a la Juventud Hitleriana, a
las tropas de asalto, al adiestramiento militar. No lo dejamos
escapar… el _arbeitsfront_ no le abandona, quiera o no quiera,
hasta su muerte».

A él tampoco lo dejarían escapar, hasta su muerte.

Don Rodrigo le refirió todas las opresiones sufridas por el pueblo,
el racionamiento, las raterías, las requisas; la prohibición
de reunirse, los azotes a los obreros, los robos de lana a los
telares domésticos, las confiscaciones de sarapes a cambio de
sumas ridículas. Le contó cómo el vecindario entero se moría
de hambre, cómo no se encontraban ni verduras, ni carne, ni huevos;
cómo las huertas eran saqueadas y relatóle las medidas de tiranía
que se ponían en práctica para obligar al vecindario a trabajar;
y cómo, también, en el Convento de las Monjas, convertido en
cuartel, se comía y se bebía sin ninguna tasa.

---Y mis libros, hijo mío, mis libros. ¿Por qué despojarme de
mi Shakespeare, de mi Dickens, de mi Homero? ¿Recuerdas que cuando
eras pequeño te leía Ivanhoe, Oliverio Twist o les relataba la
aventura de Odiseo escapando de la cueva de Polifemo bajo las
panzas de los carneros? ¿Recuerdas a Nausica? ¿Y me puedes explicar
por qué me quemaron mi Quijote? Que te lo cuente todo Nicanor
Aceves; él lo presenció. Y no fui el único vejado; cinco o seis
amigos nuestros sufrieron igual atropello.

Rodrigo seguía relatando y excitándose con sus propias palabras.
Al escucharse no se reconocía; no era él quien hablaba. Era un
hombre que no quería ni orden, ni disciplina, ni trabajo, sino
solo libertad. ---Y hay que buscarla donde se encuentre ---dijo---
y hay que ir al último rincón de la tierra donde se pueda vivir
como un hombre libre.

Patricio, igual que si se tratase de tomar una oculta venganza,
contestó con hosquedad: ---El orden nuevo no puede conquistarse
sin sacrificio; los hombres protestan porque se les exige el
abandono de una vida muelle; los mexicanos nunca han sabido lo
que es el trabajo y ahora se les insta para que trabajen. Te
oigo, padre, y comprendo que San Miguel es un pueblo podrido.

---Patricio ---interrumpió Rodrigo--- el podrido es Körner, tiene
todas las podredumbres; es servil con sus amos y cruel con sus
esclavos. El choque se ha producido porque se imagina que nosotros
lo somos.

---Körner es un buen jefe; se le ha enviado aquí para que descanse,
pero hizo una gran campaña en Polonia ---contestó Patricio---.
Lo que pasa es que San Miguel necesita una mano de hierro. Los
beneficios de las Juventudes Hitlerianas de México son inmensos
y aquí parece que nadie quiere recibirlos. Pero los impondré
---concluyó Patricio casi gritando--- los impondré aunque sea
con sangre.

Patricio partió y Rodrigo comprendió que una desgracia grande
se había abatido sobre su casa. No lloraba por Remigio, dispuesto
a cambiar su vida por la de un invasor; lloraba al otro.

Entonces lo abandonó toda prudencia. Comenzó a vociferar sin
tregua. Se mezclaba entre los obreros y les hablaba de sabotaje;
en el cuartel se presentaba y negábase a firmar. Otras ocasiones
hablaba por teléfono avisando que seguía en el pueblo, demasiado
ocupado.

Un día fue al jardín, sentóse en una banca, sacó un libraco del
bolsillo y comenzó a declamar en inglés. Los soldados de tropa
lo miraban azorados; pero la vez siguiente que repitió esta comedia
se presentó el teniente Goffine y le arrebató el tomo de las
manos; era una libreta en blanco.

---Esto es una burla y una provocación, _Herr_ Guerrero ---dijo
el teutón con su acento inconfundible---. ¿Puedo saber lo que
está usted diciendo?

---Señor oficial ---dijo Rodrigo--- son los versos de un inglés;
de uno de esos ingleses que ustedes odian, pero que murió por
la libertad; de Byron, que luchó por la Grecia que ustedes han
pisoteado; escúcheme. Y Rodrigo recitó enfáticamente en su inglés
martajado:

---«Eternal Spirit of the chainless Mind! Brightest in dungeons,
Liberty, thou art, For there thy habitation is the heart--- The
heart which love of Thee alone can bind». {.poesia}

---_Herr_ Guerrero, tendré que rendir un informe sobre sus palabras.

Varios vecinos presenciaron la escena. El cura Videgaray fue
a verlo inmediatamente y le hizo reflexiones:

---Don Rodrigo, va usted a un sacrificio estéril. Si quiere usted
luchar váyase de aquí; llévese a Verónica.

---Querido amigo, contestó Rodrigo; estoy viejo para esos trotes,
déjeme seguir mi camino.

Körner no sabía que pensar; después de haber cometido tantos
atropellos inútiles e impunes, la actitud de ese hombre indefenso
lo preocupaba. Las instrucciones de Von Virchow habían sido,
al principio de la invasión, en el sentido de que se crearan
lazos amistosos con los mexicanos y don Rodrigo era un vecino
respetado. Pero ahora ---pensaba Körner--- las condiciones son
distintas. México no es un país sumiso; de hecho estamos en guerra
con su pueblo.

Una mañana, como a las once, don Rodrigo se presentó a ver al
Presidente de la Junta Directora; iba a protestar por una multa
arbitraria. Esta pequeñez fue la gota que derramó el vaso; la
discusión se agrió y entonces llamó a de la Peña traidor a su
patria, lame-culo de Körner, vendido al invasor. ---Usted puede
asesinarme; usted no ha sido otra cosa que un asesino, pero los
hijos de este pueblo lo verán a usted colgado de la torre más
alta---. Y se salió dejando a de la Peña perplejo por unos minutos;
después el pequeño _quisling_ tomó su sombrero y se dirigió hacia
el Convento de las Monjas a hablar con el jefe teutón.

En la tarde don Rodrigo salió de su casa. Al llegar a la posada
de San Francisco dos esbirros lo tomaron por los brazos y lo
llevaron al cuartel; a las cuatro lo introdujeron en un automóvil
y vigilado por un oficial y varios soldados lo trasladaron a
Querétaro. Allí fue puesto en manos del jefe de las fuerzas invasoras
y los vecinos de San Miguel perdieron toda huella de su existencia.

Este acontecimiento produjo estupor y consternación. Era el primer
atentado que hería al pueblo en lo que tenía de mejor; en un
hombre bueno, inteligente y caritativo; un hombre trabajador
que había sabido educar a sus hijos. Estos elogios son triviales
y se escriben en dos líneas, pero la existencia de una ciudad
depende de que en ella vivan muchas gentes así y por eso San
Miguel sintió como si le hubiesen hecho una amputación dolorosa.

Al llegar don Rodrigo a Querétaro lo cambiaron de automóvil y
de guardia y para las diez de la noche estaba en México. Llevósele
al ex convento de Churubusco, el cual era un museo antes de la
ocupación; pero más tarde fue tomado por el Servicio Secreto
Mexicano, especie de Gestapo que trabajaba bajo la vigilancia
de los oficiales nazis, según la fantasía de los esbirros del
gobierno de paja. El ex convento se había convertido en una especie
de sucursal destinada a ciertos detenidos como don Rodrigo. Lo
que se pretendía era que revelase dónde estaba Remigio y cuáles
eran sus planes, pues el sabotaje que este dirigía y los primeros
golpes de los insurgentes comenzaban ya a constituir para el
invasor un problema. Pero el sufrimiento físico no era para Rodrigo
la tortura más grande; lo patético era la amargura de su yerro.
Los días que estuvo allí recluido lloraba de rabia; el recuerdo
de las disputas con Maclovio lo torturaba. Durante las noches,
agotado por los interrogatorios, febril por el martirio físico,
veía en fugaces apariciones las figuras de sus amigos; «tu casa
no es tu casa… tu casa no es tu casa», repetía Maclovio interminablemente,
sacudiendo su cabeza gris cuyos mechones le caían sobre los espejuelos.

En su pesadilla escuchaba voces que le decían: _Mein Kampf_, _Mein
Kampf_, _Mein Kampf_. Y primero eran tenues, como un susurro, e
iban creciendo y creciendo dentro de su cráneo hasta que parecía
que iba a estallar. Cuando despertaba, en las sombras, incorporábase
sobre su camastro; pero el cansancio y el sufrimiento lo aletargaban
nuevamente y volvía la visión de Maclovio y sus palabras: «tu
casa… tu casa… tu casa»; y así hasta el amanecer.

A los tres días lo sacaron de su tugurio oscuro, lo metieron
a un camión cerrado y después de una media hora de marcha abrieron
las puertas, se le hizo salir y encontróse de pronto en un furgón.
Era el procedimiento típico para embarcar reses. Por las rendijas
entraba una luz tenue y poco a poco sus pupilas dilatadas se
hicieron a esa penumbra. El carro era muy estrecho; en un ángulo
había un montón de serrín; el resto estaba casi lleno de materiales
disímbolos: enormes engranes de maquinaria, viguetas de fierro,
varillas corrugadas, varias docenas de cajas de distintos tamaños,
láminas de zinc, llantas de automóvil o camión.

Rodrigo estaba solo y a pesar de que ignoraba su destino sentíase
contento de haber salido de la asquerosa mazmorra de Churubusco.
Se echó sobre el aserrín y procuró sosegarse; al poco rato sintió
cómo la fiebre lo invadía y volvieron a torturarlo las alucinaciones
de las noches anteriores. A ellas se mezclaba Verónica; le parecía
sentir su mano fresca sobre su frente; y venía Patricio golpeando
con sus botas las tablas del piso y todas las cajas se cimbraban
y las vigas de fierro se entrechocaban; Maclovio seguía diciendo
con ritmo monótono: «tu casa ---tu casa; tu casa ---tu casa»;
sus palabras tenían un acompañamiento metálico. Entonces Don
Rodrigo se dio cuenta de que iba en un convoy en marcha; hacía
frío y llovía; las gotas tamborileaban el techo, el viento helado
penetraba por las rendijas, un hilillo de agua caía sobre su
cama. Comenzó a amanecer y más tarde rayitos de sol se colaron
por las hendiduras; el polvo flotante les prestaba materialidad.
Rodrigo recordaba aquellos rayos que bajaban de las altas ventanas
en la parroquia de San Miguel.

Más tarde abrieron la puerta y unas manos introdujeron una escudilla
y un jarro; la escudilla contenía una sopa de arroz y un trozo
de pan. El jarro estaba lleno de agua; Rodrigo la bebió con avidez
y en seguida, como el que cumple un deber, comenzó a mordisquear
sus mendrugos ablandándolos en el caldo magro del arroz. Dióse
cuenta de que el tren se había detenido; por los intersticios
apenas si divisaba las paredes de otros furgones y la cinta de
unos rieles. Afuera se escuchaban voces confusas; palabras sueltas
en español y gritos en alemán.

Rodrigo pensó que la tarde estaba entrada cuando el convoy volvió
a ponerse en movimiento. Se hizo de noche y empezó a llover;
se marchaba con lentitud y con paros frecuentes. De pronto abrióse
la puerta del carro y una voz gritó con imperio: ¡Abajo, abajo
inmediatamente! Se incorporó en su rincón y de afuera penetró
el haz de luz de una antorcha eléctrica que le dio en los ojos;
se le hizo descender y caminar bajo la lluvia copiosa hacia un
resplandor tenue que indicaba el lugar donde la máquina resoplaba.
Rodrigo, en la oscuridad, sentía cómo para hacerlo marchar de
prisa lo empujaban con la culata de un fusil; percibía el contacto
duro contra sus riñones. Cuando llegó a la vía, iluminada en
cien o doscientos metros por los faros de la máquina, muchas
piedras la obstruían. Varios hombres, cuyos rostros no podía
distinguir, se inclinaban y las iban quitando arrojándolas a
un lado, mas como los taludes estaban muy próximos rebotaban
y volvían a caer sobre los durmientes.

Entonces una voz gritaba: ¡Con cuidado, no sean pendejos! A don
Rodrigo se le ordenó que se aplicara a la misma tarea; se agachaba
e iba retirando los pedruzcos. Cuando habían despejado unos doscientos
metros, la máquina comenzó a caminar, con lentitud. Don Rodrigo
---acostumbrado a su trabajo de escritorio consultando sus códigos---
sentía un dolor en la cintura; pero cada vez que se incorporaba
y se arqueaba hacia atrás buscando un alivio, una voz salía de
las sombras gritándole:

---¡_Gehen wir rasch_!--- Y aunque sin comprender su significado
sabía que lo fustigaban; del mismo modo una bestia escucha un
jarre! y el tronar del látigo en las orejas.

Sus fuerzas lo abandonaban; la lluvia le mojaba las espaldas
y la estrecha faja entre los rieles, a la luz de los faros, veíase
poblada de piedras. Las mismas palabras de mando se seguían escuchando
en la noche:

---¡_Schnell, schnell_!

El las oía como en un sueño mientras levantaba las piedras, hasta
que en una de las ocasiones en que se inclinaba, dio con su cuerpo
en tierra. Un soldado se acercó y lo golpeó rudamente con el
duro zapatón claveteado gritándole: ---_Stehen Sie auf, werfen_.

Después ordenó: ---_Tun sie diesen Mann auf den Wagen_.

Tomaron a Don Rodrigo por los pies y los hombros. Lo levantaron
en vilo; los últimos días de sufrimiento le habían quitado kilos
de peso y habíanlo convertido en un cuerpecito desmirriado e
insignificante. Lo arrojaron a un carro de carga entre canastos
de verdura, huacales de fruta y de pollos, becerros y sacos de
patatas. Logró arrastrarse a un rincón y allí creyó, con una
sensación apagada de bienestar, que iba a morir. Por primera
vez en muchas noches durmió con un sueño hondo, sin alucinaciones.
Cuando se despertó tenía hambre. De los huacales sacó unos tomates
y unas manzanas; como a mediodía un hombre entró al carro y en
seguida otro le trajo la escudilla con el arroz, el pan y el
jarro de agua.

---Prepárese, vamos a llegar dentro de una hora---. ---¿A dónde?
---preguntó Don Rodrigo---. ---No puedo contestarle; solo puedo
decirle que va usted a un campo.

La noticia lo dejó indiferente. Nada podía ser peor que los últimos
días. Recordaba que hacía una semana hablaba todavía en San Miguel
con el viejo Améndola y con Aceves. Sí, se contaban historias
de los campos de concentración; pero a ellos les parecían algo
irreal. Hacía unos meses, él, Rodrigo Guerrero, habría negado
su existencia; se puso a elucubrar: lo llevaban en un convoy
de vía angosta y aunque con desesperante lentitud habían dejado
México hacía muchas horas. Quizá lo llevaban a Oaxaca; quizá
al Istmo.

El tren se detuvo con un ruido infernal de fierros. Hacía un
sol espléndido pero el calor era agradable. Lo hicieron bajar
y atravesando siete u ocho vías lo llevaron frente a una pequeña
estación destartalada; quiso ver el nombre pero lo habían embadurnado
intencionalmente y no podía leerse.

Algunos individuos estaban ya allí y otros fueron llegando; eran
sin duda confinados como él. Don Rodrigo contó ocho; andrajosos,
barbudos, de ojos hundidos, con los rostros marcados por el sufrimiento
y el hambre. Se les advirtió que no debían hablar y se les hizo
caminar a un patio enorme, rodeado de grandes bodegas y talleres;
escuchábase el ruido de los motores y el aserrar de la madera.
Grandes camiones entraban a los garajes y galeras, y salían de
ellos. Varios cientos de hombres se veían ocupados en actividades
múltiples: carga y descarga, manejo de máquinas, recuento de
cajones, arreglo de anaqueles.

Don Rodrigo contemplaba la escena con indiferencia; se sentía
fatigado y quería sentarse; pero tenía que continuar de pie,
en silencio, alineado con los otros presos.

Varios soldados los hicieron subir y se sentaron junto a ellos
en una camioneta descubierta que echó a rodar por una amplia
calzada. El día estaba luminoso y era una delicia recibir el
viento fresco; atravesaron un pequeño poblado, pasaron frente
a una casa con portales; un gran letrero colgaba de ella «Haus
der Ingenieure». Dejaron a un lado un gigantesco laurel de la
India y una iglesita pueblerina llena de poesía. Salieron del
villorrio y comenzaron a ascender una sierra. A los cuatro o
cinco kilómetros el trabajo se desarrollaba febrilmente. Docenas
de palas mecánicas mordían los costados de los cerros y colmaban
con tierra rosarios de vagonetas que pequeñas locomotoras arrastraban
hasta la orilla de las barrancas; se veía desde luego que se
estaba ensanchando la carretera existente y los terraplenes de
ampliación crecían a ojos vistas.

Más adelante potentes arados levantaban el pavimento ya construido
y cuando todo estaba limpio, innumerables grupos de hombres tendían
una cuadrícula de varillas; después otras máquinas enormes, con
un andar lento y pesado de megaterios, iban vomitando por sus
fauces un gran chorro de concreto que como la lava de un volcán
cubría pesadamente las varillas formando sobre ellas una lámina
tersa.

Centenares de camiones iban y venían cargados con fierro, bolsas
de cemento, arena y combustibles. Otros se alineaban, numerosos
como hormigas, bajo las palas e iban a voltear después su contenido
sobre los terraplenes.

En los puentes se erigían nuevas estructuras y las grúas alzaban
hasta el cielo sus cuellos, como jirafas prehistóricas, de cuyas
mandíbulas pendían cadenas con viguetas y arcos de hierro. Los
remachadores, en sus trajes de buzos, hacían oír su martilleo
incesante de ametralladoras; en otros sitios las chispas brotaban
de las máquinas de soldar y los operarios, cubiertos con escafandras
de vidrios oscuros, parecían los superhombres de un mundo inhumano,
sin corazón ni piedad.

Después de quince o veinte kilómetros el movimiento cesaba como
por encanto y la camioneta se deslizó por una vía anchísima y
lustrosa. Don Rodrigo no se daba casi cuenta del panorama; el
cansancio y el hambre lo agobiaban. Por primera vez en su vida
sentía la tortura física de la mugre; pasábase las manos por
su barba de una semana y al encajarle la uña arrancaba de su
piel la costra de tierra apelmazada con el sudor. Y veía después
sus manos, todavía suaves, con reminiscencias del jabón que Verónica
le ponía en su baño; ahora estaban pringadas, llenas de moretones,
rasguños y desolladuras por el trabajo de la noche anterior.

Y al recordar a Verónica quería llorar, pero un pudor de su alma
viril lo detenía.

Continuaban subiendo por la montaña; las faldas verdes, tenían
la gama entera de los verdes. Los montes azules en lontananza
y después los cerros oscuros de un verde azuloso cuyo perfil
se recortaba con los dientes diminutos de sus árboles: los verdes
tranquilos de las milpas, los verdes esmeraldas de los pastos,
los amarillentos de los trigales. Aquí y allá veíanse inmensas
manchas de flores silvestres, moradas o color de naranja. En
los términos más cercanos los encinares mostraban sus follajes
lustrosos y las casuarinas, al sacudir sus agujas de un tono
verde tierno, daban al viento un murmullo musical.

La cinta de la carretera de concreto seguía trepando, ancha,
blanca, perfecta. Las juntas de asfalto la partían en cuadrángulos
y tres listas amarillas, a lo largo, dividíanla en cuatro zonas.
Las curvas se sucedían unas a otras e iban rebanando los cerros
con cortes altos y atrevidos; a veces se atravesaban túneles
y entonces se veía cómo se había abandonado el trazo de la carretera
vieja, estrecha, con su pavimento bituminoso. En las tangentes
largas había grandes signos para aterrizajes de emergencia, pues
toda la región es abrupta, con barrancas y montañas que se extienden
hasta el horizonte. Se cruzaban con camiones pintados de gris
oscuro y llenos de uniformes. En algunos montículos cañones que
salían de la espesura apuntaban al cielo; los soldados, bajo
toldos de ramas, se agrupaban alrededor de las piezas.

La camioneta seguía rodando; llegaron a un lugar en donde entroncaba
un camino relativamente angosto, revestido de piedra triturada.
Se veía pobre y primitivo comparado con la supercarretera que
acababan de recorrer. Bajo una enramada estaban varios militares;
uno hacía centinela; el chofer presentó varios papeles y la camioneta
prosiguió levantando polvo mientras los prisioneros chocaban
unos contra otros y rebotaban a cada bache sobre las banquetas
de madera.

Don Rodrigo salió de su sopor; vio cómo llegaban a un inmenso
cuadrilátero; tenía un largo de cerca de dos kilómetros y un
ancho de uno; quizá más. Era una amplia meseta sobre una colina,
que unía dos cadenas de cerros; un sinfín de alambres de púas
formaban el perímetro haciendo una maraña imposible de cruzar;
solo un loco se habría atrevido a intentarlo. Una pesada verja
de hierro cerraba la entrada.

Al llegar a ella, y después de franquearla, los ocho cautivos
bajaron del vehículo. Don Rodrigo vio unas cuantas construcciones,
levantadas con la madera rolliza del monte. Se les hizo entrar
a un cuarto donde había un buen moblaje de oficina; al poco rato
llegó un oficial. El chofer le entregó un sobre; lo abrió, estuvo
leyendo con atención su contenido al mismo tiempo que lanzaba
de cuando en cuando miradas, ya a uno, ya a otro de aquellos
hombres.

Al acabar su inspección se volvió al chofer: ---_Gut, Sie können
zurücktreten_.

El otro alzó el brazo, dijo «Heil Hitler» y salió con actitud
mecánica.

Entonces el oficial fue llamando a cada uno por sus nombres:

---¿José Fuentes?

---Sí, señor.

---¿Cuántos años tiene?

---Treinta y uno.

---Muy bien, quítese la camisa.

El otro obedeció. Observólo y volvióse a su escribiente que tomaba
notas; díjole algunas palabras, consultó los papeles del sobre
y ordenó:

---Retírese. A ver, otro.

Así siguió ese examen rutinario. A Don Rodrigo le tocó ser el
quinto; declaró sus años y tuvo también que sufrir el examen
de su torso blanco y delicado de habitante de ciudad.

Con el último preso hubo un incidente:

---¿Miguel Martínez?

---Sí.

---Diga usted: sí, señor.

El hombre no contestó. El oficial levantóse de su asiento, acercóse
a él y con la mano izquierda lo tomó brutalmente por los cabellos,
lo sacudió con fuerza y repitió: ---Diga usted: sí, señor. Como
permaneciera mudo le incrustó el puño derecho en los labios y
volvió a ordenar: ---Diga usted: sí, señor.

El prisionero dijo entonces débilmente: ---Sí, señor ---al tiempo
que con la manga desgarrada se limpiaba la sangre que le brotaba
en abundancia.

El oficial se dirigió entonces a todos: ---Queda entendido que
aquí, a los alemanes en este campo, se les dice: señor---. Para
terminar habló a todo el grupo con un gesto torvo: ---Van ustedes
a trabajar en este lugar; la disciplina es muy estricta; la más
ligera rebeldía tiene pena de muerte. Quiero que esto lo entiendan
bien. Es todo.

Hablaba con voz imperiosa, en un español lleno de acentos germanos
y con una sintaxis plagada de barbarismos. Cuando terminó, volvióse
a sentar frente a su escritorio e hizo una seña a los soldados
que sacaron a los presos llevándolos a una especie de gran bodega.
Allí se les obligó a dejar sus ropas y a cada uno se le entregó
una unión de mezclilla azul, unos calzones y una camisa de manta;
unos zapatos y un sombrero de palma.

Después un mexicano, que llevaba el uniforme nazi, les habló:

---Hoy van ustedes a descansar; el trabajo comenzará mañana.

Los mismos esbirros los condujeron a un amplio techado; tenía
unos treinta metros de largo, unos cuatro de ancho y una cubierta
de lámina de zinc sobre la cual se extendía un sobretecho de
zacate. Esta primitiva construcción estaba protegida por uno
de sus lados con un endeble muro de ramas y lodo seco; cuando
llovía todos los presos se apiñaban contra él.

En el suelo había montones de paja; en un rincón le indicaron
a Rodrigo su sitio; a otro de los detenidos, el golpeado, le
mostraron otro lugar al extremo de esa galera; después salieron
con el resto y los repartieron en otras crujías semejantes.

Había quizás treinta en el amplio campo y entre unas y otras
se veían puestos de centinelas; en el centro se levantaban varias
torres construídas con macizos troncos, altas de unos siete u
ocho metros y coronadas por amplias plataformas de las cuales
asomaban los cañones de las ametralladoras y los faros buscadores.
En otro lado de aquella inmensa extensión se levantaban casernas
para la tropa y las casas de la oficialidad.

Don Rodrigo quedóse sumido en un pesado sopor; se sentía embrutecido
y no podía pensar. Al pardear la tarde un ruido de motores atronó
el espacio y se fueron encendiendo uno a uno las luces y los
faros; oyó rumor confuso de pisadas y lejanas voces de mando;
entonces incorporóse y vio largas filas de hombres; frente a
un cuartucho les entregaban una vasija, una cuchara de madera
y un jarro. Les sirvieron el rancho y todos se dispersaron con
él sentándose sobre las piedras y sobre rimeros de vigas. Rodrigo
calculó que pasaban de dos mil. Distinguía cómo mordían el pan
correoso. Un individuo, también de mezclilla azul, pero con galones
que denotaban alguna autoridad se acercó: ---Amigo, si quiere
comer apresúrese; vaya por su plato.

Pero Rodrigo no tenía fuerza para nada; quería seguir allí, acostado.

Después los presos llegaron a las galeras y se fueron tumbando
sobre la hojarasca; cerca de él se recostó uno barbudo y hosco.
A los pocos instantes y sin mirarlo, con los ojos hacia el vacío,
lo interrogó en voz baja.

---¿Lo acaban de traer?

---Si, ---contestó Rodrigo.

---¿De dónde viene?

---Del Estado de Guanajuato.

---¿Por qué lo trajeron?

---Porque protesté contra esta tiranía de bárbaros; dígame, ¿en
qué lugar estamos?

El hombre permaneció inmóvil; mascaba todavía los mendrugos de
pan; sus barbas temblaban con el abrir y cerrar de las mandíbulas.
No respondió. Rodrigo repitió la pregunta; pero ante el mutismo
del otro guardó también silencio.

Me cree un agente provocador ---pensó Rodrigo---; y no se equivocaba.
Los concentrados habían pasado ya por dolorosas experiencias;
individuos con aspecto de vagabundos y parias, como ellos, que
provocaban confesiones, y después los azotes, el calabozo y muchas
veces la ejecución. Por eso eran cautos. Don Rodrigo pareció
adivinarlo y dijo al fin: ---Ya irá usted viendo que soy también
una víctima de estos trogloditas. Y calló; asaltóle la idea de
que el agente provocador podía ser su vecino de camastro. ¿Qué
le importaba? Habría recibido la muerte como una liberación.

Al día siguiente lo despertaron voces descompasadas. Las estrellas
brillaban aún, pero una claridad parecía venir de detrás de los
cerros. Casi no había dormido por el frío; soñaba que andaba
desnudo y que se mofaban de él. Hiciéronlo formar, recoger su
escudilla y le dieron agua hervida con yerbas aromáticas y un
pedazo de pan. Al terminar todos los hombres devolvieron el cacharro
y de dos en fondo fueron saliendo del campo por la ancha verja
de fierro. A sus flancos, militares mexicanos daban órdenes;
eran cabos y sargentos. Soldados alemanes con pesados abrigos
hasta los pies y con ametralladoras portátiles caminaban también
vigilando la caravana.

Anduvieron como unos dos kilómetros; llegaron a una explanada
con un gran frente de cantera; en él se veían las bocas de estrechos
túneles. A cada hombre le daban su herramienta; algunos, en parejas
y armados de marros y barretas larguísimas, empezaron a perforar
la dura roca.

A Rodrigo le entregaron una carretilla y una pala mientras el
capataz le advertía: ---Va usted a trabajar allí, como aquellos.
Cuando haya sacado trescientos viajes, puede descansar---. Vio
a los prisioneros cómo iban cargando las carretillas con el material
desintegrado que formaba enormes montones y después las llevaban
a tirar al barranco, a unos treinta pasos. Al poco rato, sobre
estrechas vías, comenzaron a salir de los túneles vagonetas que
volteaban grandes bloques de piedra en los camiones alineados
abajo, a un nivel inferior; jadeaban estos con sus motores en
marcha y tomaban el camino revestido de guijos en dirección a
la gran carretera de concreto.

Comenzó el trabajo; Rodrigo metía con gran esfuerzo la pala en
los pedruzcos logrando apenas colmarla a medias; después vaciaba
su contenido en la concha metálica. Se propuso contar las paladas
para llenarla: una, dos, tres .., hasta sesenta. Mentalmente
calculó: dieciocho mil paladas ¡dieciocho mil veces, escarbando
en aquel material bronco! En tanto que sus compañeros ejecutaban
su tarea con prontitud, un gran desaliento lo invadía. Uno de
ellos volvióse a Rodrigo y le susurró en voz tan baja que parecía
un murmullo: ---Al principio es intolerable, después se acostumbra
uno… se endurecen los bíceps y las manos se llenan de callos.
No se desanime.

Como a las ocho estaba extenuado; su cuenta era apenas de quince
carretillas; casi no podía con sus brazos. Tenía las palmas de
las manos cubiertas de ampollas. Sacó su pañuelo, negro de mugre
y envolvióse los dedos de la mano izquierda; después lo desgarró
en dos para envolverse también los de la derecha. Al poco rato
las ampollas empezaron a reventar y el roce del cabo le producía
un dolor atroz; el contacto de los mangos metálicos hacíalo sufrir;
los músculos de las corvas le dolían. Llevaba acarreadas sesenta
carretillas cuando no pudo más y se sentó. El capataz advirtióle
en un tono impasible: ---Hoy tendrá usted que terminar cien,
cuando menos.

El sol le daba sobre el rostro; sudaba por todos los poros de
su cuerpo; eran como las doce. Se incorporó y trató de continuar
la tarea; pero inútilmente: las manos no le servían de nada.
Cada esfuerzo con la pala le causaba un ardor atroz; logró llenar
todavía quince conchas. Hubo un momento en que ya no pudo, sus
piernas se flexionaron y cayó al suelo. Dos compañeros se acercaron
y lo arrastraron hasta dejarlo sentado contra el cantil. Vino
el aguador que daba de beber a los presos y le echó sobre la
cabeza un jarro de agua; luego lo volvió a llenar y se lo acercó
a los labios. Rodrigo tomó unos sorbos y quedóse allí un gran
rato con los brazos colgados, la boca abierta, los ojos absortos.
Tenía las manos llagadas.

Así transcurrió el día; se levantaba y llenaba una carretilla
con enorme esfuerzo. Después abandonó la pala e intentó ejecutar
su trabajo con las manos desnudas. Tomaba los pedruzcos y los
iba echando a la concha cóncava. Al poco rato los dedos le sangraban.
Volvió a tomar la pala… Cuando los llamaron a formar para el
regreso había completado ochenta y cinco carretillas.

Llegaron al campo e hicieron la hilera para el rancho; pero él
se fue a tirar a su rincón; nadie pareció notarlo. Se adormeció;
al poco rato vio llegar a los otros presos.

Su vecino, el que le había hablado la noche anterior, acercóse,
le pasó la mano por la frente; después sacó de su bolsillo una
maraña de estopa grasienta, le enjugó el rostro sudoroso y Rodrigo
sintió que una gran dulzura lo invadía. Las lágrimas nublaron
sus ojos y comenzaron a correr por sus mejillas como una taza
que se desborda. A través de ellas veía el cielo claro, con la
última luz del crepúsculo; las estrellas escintilaban sobre la
silueta de los cerros; se oía el canto de las chicharras…

</section>
<section epub:type="chapter" role="doc-chapter">

# XI

++El++ aspecto grave de Remigio Guerrero me intimida y por esa
razón nunca me había resuelto a interrogarlo respecto a sus
actividades como guerrillero insurgente. Cuando creía tener una
oportunidad, la charla se desviaba hacia sus investigaciones
médicas.

Estaba decidido, pues, a prescindir de esos datos de primera
mano en la confección de mi historia. Después de todo ---me decía---
el mundo entero sabe quién es Remigio y lo que hizo durante la
invasión.

Iba por lo mismo a pasar por alto esas hazañas, o aventuras,
o sencillamente hechos rutinarios, como él después los calificó
al hablar conmigo. Los ataques de las guerrillas fueron, con
los meses ---esas son sus palabras--- una rutina, sujeta a ciertos
métodos y reglas técnicas, lo mismo que anestesiar un enfermo
o suturar una incisión. Tuvimos buen éxito porque empujados por
nuestro odio a los nazis llegamos a dominar esa técnica y nos
resolvimos a ponerla en práctica. Quienes conocen a Remigio dicen
que habla así porque es un introverso que odia la publicidad.
Existe en él un fondo de timidez huraña.

Pero en fin, si escribo estas líneas se debe a que una circunstancia
inesperada me puso solo frente a él, varias horas, en la soledad
de un pequeño lugar campestre, a donde fuimos a dar una noche.

Sucedió que en este invierno de 1960 Remigio había acudido a
San Miguel Allende a pasar varios días. Estaba reparando la vieja
finca de sus padres llena para él de tristes recuerdos de la
guerra; yo descansaba, recorría el pueblo y escribía; habíanme
concedido una prórroga a mis vacaciones y pasaba varias horas
diarias pergeñando notas o poniendo en limpio mis apuntes.

Tanto él como yo parábamos en la casa del ingeniero Uranga. Una
noche anuncié a mi huésped que al día siguiente tomaría el autobús
para México; me interesaba saber si existía aún la casa donde
estuvo alojada la fábrica de Alonso y también iba a visitar la
librería Pahlas, sucesora del «changarro» de Maclovio Otamendi.

Remigio me interrumpió: ---Espérate hasta después de comer; podemos
salir en mi máquina a las dos; para las cuatro estaremos en México.
Total, que llegarás un poco más tarde, pero en cambio iré acompañado.

En efecto, al otro día hicimos un almuerzo opíparo en el cual
el plato fuerte fueron trozos de cordero a la brocheta, que doña
Tula asa en una fogata de ramas de aguacate porque ---alega---
así toma la carne un gusto muy sabroso. Eso, con tortillas bien
calientes y una jarra de sidra fresca de la tierra, nos puso
de magnifico talante. Uranga se despidió de nosotros y poco antes
de las dos Remigio levantóse de la mesa dirigiéndose al garage
para sacar su autogiro.

Montamos a la cabina y salimos con la máquina hasta el vasto
prado que se extiende ante la casa. Allí Remigio desplegó las
alas y comenzó a probar el motor. Pedimos por radio informes
meteorológicos y nos dijeron que un gran manto de nubes bajas
se extendía sobre la zona de Querétaro y que nos recomendaban
evitar esa ruta; pero cuando les dijimos que íbamos en autogiro
nos dieron la autorización para el vuelo y nos indicaron cuál
debería ser nuestra altura mínima.

Hacía un tiempo húmedo. El mes de enero de 1961 ha sido excepcionalmente
frío y la mesa del centro se resentía esa tarde de una tormenta
en el Golfo. Caía una lluvia fina. Remigio metió los embragues,
aceleró y cuando las aspas giraban a gran velocidad comenzamos
a rodar sobre el pasto. Después de tres o cuatro metros de recorrido
nos elevamos lentamente, casi sobre la vertical, y a los cuantos
segundos el pueblo bajo nosotros parecía un juguete. Dimos una
amplia vuelta sobre las colinas y enfilamos rumbo a Querétaro
al mismo tiempo que ascendíamos para alcanzar la altura requerida.

Cuando el altímetro indicaba ocho mil quinientos pies sobre el
nivel del mar y apenas unos dos mil sobre el suelo, nos hundimos
en un gran banco de nubes grises y espesas; gotas menudas resbalaban
sobre la cavolita de la cabina y el paisaje desapareció ante
nuestros ojos. La niebla nos envolvía por todas partes y llegaba
a nuestro olfato su olor peculiar. Ascendimos aún más, a nueve
mil pies, al mismo tiempo que Remigio moderaba la marcha. Volar
así, a ciegas, aún en los autogiros o los helicópteros, aunque
se tenga un aparato de radar, me produce siempre una sensación
de vago peligro; tengo que decir que no iba disfrutando del viaje.

Remigio observó: ---Si nos vemos obligados a continuar así llegaremos
a México de noche, pero no me atrevo a ir más de prisa; la visibilidad
es casi nula. Y agregó:---Lo siento por ti que querías llegar
temprano.

---Por mí no se preocupe ---le respondí. Al poco rato pedimos
noticias del tiempo y nos contestaron que espesas masas de cumulonimbos
se cernían sobre gran parte de la mesa central y la vertiente
del Golfo. Seguimos volando, sin distinguir nada bajo nosotros
y no nos dimos cuenta cuando pasamos sobre Querétaro. Remigio
comenzó a dar muestras de inquietud. Al fin me dijo: ---Creo
que andamos perdidos---. Entonces inició un amplio viraje que
yo sentí únicamente en el vientre, porque la cerrazón era completa
y no relacionábamos nuestro movimiento con ningún objeto visible.
De pronto articuló con lentitud: ---Se ha descompuesto la brújula;
lo único sensato es aterrizar.

El velocímetro que marcaba veinticinco millas por hora bajó a
cinco o seis; el altímetro que da la cota sobre el suelo fue
indicando mil, ochocientos, quinientos pies. A medida que nos
acercábamos a la tierra la lluvia se hacía más tupida y cuando
estábamos como a cien metros volábamos en medio de un furioso
aguacero. Principiamos a distinguir siembras de trigo y finalmente
un caserío. Sobre la lámina de un tejabán que servía de hangar
en un campo de aterrizaje muy primitivo pudimos por fin leer
el nombre del sitio: Tezontepec. Entonces descendimos dulcemente
y nos posamos a un lado de esa construcción; Remigio llevó su
máquina bajo techo y nos decidimos a esperar.

A un kilómetro veíanse las primeras casas del poblado; pero todavía
llovía a cántaros y no se dejaba ver ni un alma. Por fin calmóse
el aguacero y aunque el cielo continuaba como panza de burro,
bajamos de nuestro aparato y nos decidimos a buscar un albergue.
Estábamos resueltos a pasar allí la noche. Una parvada de chiquillos
vino corriendo hacia nosotros; Remigio preguntó por el cura y
se ofrecieron a conducirnos.

---Estoy siguiendo un consejo de mi padre, quien decía que si
llega uno a un lugar desconocido buscando cama y comida, hay
que preguntar por el cura. Hace muchos años, antes de la guerra,
lo puse en práctica una noche que llegué a Tepatitlán. Venía
de Aguascalientes y tuve que dejar el coche como a cinco kilómetros,
hundido en el lodo hasta los guardafangos. Llovía bastante y
con mi maleta a cuestas me eché a buscar un hotel; los dos de
mala muerte en el pueblo estaban atestados. Entonces me metí
de rondón en la iglesia; el cura en el púlpito rezaba el rosario;
era un español cerrado y las eses, las elles y las ces y zetas
se remontaban hasta las bóvedas donde hacían una gran resonancia.
No se le veía la cara pues estaba oscuro y apenas si en el altar
las ceras ponían manchas de una luz amarilla. Pero cuando bajó
del púlpito y me acerqué a él vi que era un hombre como de sesenta
años, recio, con una barba blanca de dos días y una pelambrera
de pelos duros. Le expuse mi caso y esa noche dormí en una cama
con sábanas de lino que olían a membrillo en la casa de la viuda
de Luna, que era una señora santurrona vestida de muaré negro
bordado de lentejuela. Me prestaron una bata mientras se secaban
mis vestidos y me dieron de cenar opíparamente. El único pago
que tuve que efectuar fue rezar el rosario en la sala de la casa,
con el ama y los criados, ante una imagen de la Virgen del Carmen.

A todo esto íbamos llegando al caserío de Tezontepec, pero el
cura no estaba allí y la acogida que encontramos era bastante
fría, tan fría como el viento que nos azotaba el rostro. Entonces
me decidí a ensayar una pequeña traición a Remigio y a espaldas
suyas revelé su identidad. El ambiente cambió como por encanto;
todos se disputaron el privilegio de darle alojamiento y de ofrecerle
una cena. Por fin nos hicieron acomodo en la casa de don Refugio
Valencia, quien improvisó una verdadera comilitona e invitó a
medio pueblo. Remigio no comprendía cómo después de tantos años
en que su vida estaba lejos de la atención popular, lo habían
reconocido; que me perdone mi estratagema si lee estas líneas.

Como a las once nos dieron la venia para que nos fuésemos a dormir.
Nos destinaron una pieza amplia; unas sillas, dos camas tendidas
con sábanas de manta muy limpia y entre ellas una mesita con
un quinqué de petróleo formaban el moblaje. Me acosté y me puse
a leer; Remigio se acostó también y sacó un cigarrillo; estaba
boca arriba, con las dos manos en la nuca, abstraído en la contemplación
de las vigas del techo. La luz vacilaba y su parpadeo hacía mi
lectura muy difícil. Remigio fumaba sin quitarse el cigarrillo
de los labios; a cada chupada avivábase la lumbre como un topacio
incandescente al mismo tiempo que el humo lo envolvía. Por fin
comenzó a hablar con pausa:

---Nuestro pequeño tropiezo de hoy, que no tuvo consecuencias
porque ni un solo instante corrimos el más leve riesgo, me trajo
a la memoria, por su similitud, un suceso trivial de nuestra
lucha contra los nazis. A veces me preguntan si en esos años
tan azarosos puse en peligro mi vida y no sé qué responder. A
raíz del triunfo los reporteros me asediaban y las revistas americanas
me ofrecían muy buenos dólares por mis memorias; los rechacé,
no por virtud, puedes creérmelo, sino porque para hilvanar un
relato truculento habría tenido que inventar muchas cosas irreales.
En verdad nada de lo que me pasó fue interesante; es decir, el
interés consistía en otras cosas: en el despertar de las gentes,
en su espíritu de sacrificio, en su estoicismo, en su desdén
por la muerte. Lo que me apasionaba era ir descubriendo en las
almas tesoros de valor, de hombría, de entereza. Encontré en
el pueblo un acervo inagotable de virtudes; y eso, aunque no
era tema para un folletín, sí me interesaba hondamente.

Es cierto que me tocó presenciar muchas escenas de miseria, de
crueldad y de dolor; pero no eran, en último análisis, sino la
repetición de lo que el mundo entero presenció en el transcurso
de esos años. Además, lo que habría podido referir con emoción
eran mis propias desgracias y no tenía derecho para hacerlas
material de lectura cuando todos, quien más, quien menos, sufrieron
pérdidas similares. Los sucesos que desde 1940 nos daban escalofrío
a nosotros, los hombres de América ---quienes tan lejos nos sentíamos
de aquellas catástrofes ---dejaron de ser literatura periodística
para convertirse aquí en la dura realidad: chiquillos hambrientos,
caritas afiladas por las dolencias, cuerpecitos destrozados por
las bombas y lo más terrible: niños que morían fusilados gritando
«Viva México». Las ejecuciones en masa, las torturas de la Gestapo
o la G.P.U., la sangre, la emboscada, las noches a campo raso,
la lucha cuerpo a cuerpo; todo esto lo tuvimos también aquí.

Las pocas veces que corríamos una aventura, en el sentido novelesco
de la palabra, la recibíamos como un sedante; divertía nuestros
pensamientos de las rutinas de la guerra. Solo una ocasión estuvimos
enfrascados en una vasta trama con su cortejo de intrigas, espionajes,
esperas en la noche, plagios y disfraces; para estas maquinaciones
era muy hábil tu jefe el ingeniero Berrueta y nuestro amigo,
el inolvidable John McKim.

El hecho fue que nos proponíamos apresar a Von Virchow quien
volvía de Veracruz hacia México. Los patriotas de esa región
nos habían proporcionado datos que juzgamos exactos: el boche
traía un convoy entero, con su carro especial, ni más ni menos
que los ministros de Estado en los Gobiernos Revolucionarios.
Tendimos nuestras redes de manera que nos pareció maestra; una
mujer de Veracruz, que bailaba la rumba y movía el vientre como
una avispa cautiva, había seducido a uno de los oficiales nazis
del puerto y le arrancó secretos decisivos. Supimos del viaje
del jefe nazi a Veracruz y también la fecha de su regreso.

Estimé de importancia la captura de un pez tan gordo y decidí
darle una atención muy personal. Nuestros agentes recorrieron
los pequeños pueblos de la ruta preparando el golpe; la población
entera se sentía dispuesta a cooperar y los jarochos se relamían
pensando en una fritanga de higadillos boches.

Efectivamente la noche esperada dinamitamos y descarrilamos un
tren que pasó para México; pero después supimos que era de carros
y furgones vacíos. Donde esperábamos encontrar los restos del
jefe nazi y su estado mayor, solo hallamos pedazos de asientos,
vidrios rotos y fierros retorcidos. Lo grave fue que cuando nos
aplicábamos a esta pesquisa cayó sobre nosotros una fuerza enemiga.
Lo que nos salvó fue que no conocían esa sierra; pudimos, pues,
desparramarnos, aunque sufrimos bajas muy considerables. A mí
me buscaban, pero pude ocultarme en una choza de labriegos; estos
me sacaron esa misma noche y me llevaron a una cueva por las
fuentes del Atoyac, donde permanecí una semana hasta que me facilitaron
la huída hacia el Norte. Pero los boches hicieron una búsqueda
muy extensa y torturaron a muchas gentes para saber mi paradero.
Al pobre indio que me dio albergue lo mataron a palos, como si
fuese una alimaña; sus hijos, que me habían llevado al escondrijo,
no volvieron a su casa sino pasado más de un mes.

Yo creí por algún tiempo que Berrueta había muerto, hasta que
lo encontré en el Estado de Morelos, montado en un caballo rosillo
y con una sotana de cura. El disfraz le venía pintiparado.

Nunca había oído una relación tan larga de labios de Remigio.
El seguía tendido en la cama, como abstraído. Hablaba con voz
monótona, sin emocionarse; parecía que relataba sucesos de vidas
ajenas. Yo, naturalmente, había abandonado mi lectura y lo escuchaba
con atención concentrada; quería grabar en mi memoria cada una
de sus palabras.

Como el silencio se prolongó comprendí que la locuacidad de Remigio
había sido una racha fugaz y sin grandes esperanzas de renovarla
me decidí a decir algo:

---Pues lo que usted relata no es un esfuerzo rutinario, como
lo llama; al revés, es una acción siempre imprevista. Además,
el Ingeniero Berrueta me cuenta que sus actividades eran de toda
índole; me ha referido que en El Paso habló usted ampliamente
sobre México y que sus palabras hicieron mucho bien a este país.
Me dice que fue una lástima que nadie recogiese su discurso.

---Ese nombre de discurso ---me replicó Remigio, interrumpiéndome---
no es muy exacto. Estuvo muy lejos de la elocuencia.

Lo que pasó fue que, en cierta ocasión tuve que ir a Texas. Esos
viajes eran frecuentes, pues se hacía necesario arreglar los
envíos de armas para nuestros guerrilleros. Al cruzar la línea
me encontré con que habían organizado una comida de solidaridad
panamericana y me arrastraron a ella. Asistió el Vicepresidente
de los Estados Unidos y me sentaron junto a él; comenzamos a
charlar, pero era un poco sordo y además se empeñaba en hablar
español, de allí que yo articulase mis frases en voz alta con
gran lentitud y parsimonia.

El silencio se fue haciendo poco a poco a mi alrededor hasta
que de pronto dime cuenta de que todo el mundo me escuchaba.
Yo me había enfrascado en una explicación sobre lo que era México
y el pueblo de México y sobre las relaciones con nuestros vecinos;
seguí mi charla y me prestaron atención. Finalmente me pidieron
que me pusiese de pie y así continué exponiendo mis ideas con
una franqueza que quizás era demasiado ruda para una comida de
acercamiento diplomático. Con asombro mío me aplaudieron y el
Vicepresidente me abrazó al estilo mexicano y me dijo al oído:
sus palabras las conocerán en la Casa Blanca y además las trasmitiré
al pueblo de mi patria.

Lo cierto es ---dije a Remigio--- que su discurso alzó ámpula.

---Te repito que no fue discurso ---contestó Remigio---. Impresionó
porque la verdad siempre impresiona. Les dije muchas de las cosas
que escuché de mi padre; les hablé de que los gobiernos americanos
habían visto siempre su interés, pero nunca el bienestar de los
mexicanos. Les recordé que en tiempos del General Díaz fueron
los instrumentos de los aventureros yanquis que llegaron a México
a hacer fortuna; que más tarde sancionaron con su fuerza la desenfrenada
explotación de nuestro petróleo; que en épocas muy recientes,
antes de la guerra, habían dado su apoyo en nuestro país a regímenes
impopulares. Les dije que nunca habían hecho un esfuerzo inteligente
para acercarse al pueblo de México; que ese pueblo no estaba
formado como lo supusieron mucho tiempo por la gentuza burocrática
que malamente regía sus destinos, ni tampoco por la minoría egoísta
que defendía a toda costa su bienestar protegida por la intangibilidad
de los intereses creados. Les dije que la verdadera reacción
en México estaba formado por todos los parásitos enriquecidos,
desde los ministros hasta los revendedores de semillas, pasando
por una gama completa: parientes y paniaguados de los funcionarios,
banqueros, abogados picapleitos, alcahuetes de contratos de obras
públicas, corredores, diputados, rentistas, prostitutas y líderes.
Pero que el pueblo, integrado por la masa de hombres que laboran,
lo mismo dirigiendo industrias que ante los husos de un telar,
es progresista, generoso, trabajador, desinteresado y patriota
y que estaba dando fehacientes pruebas de ello. El error de ustedes
---le recalqué--- consiste en que no han sabido buscar a ese
pueblo; en vez de hacerlo han entrado en combinaciones con los
detentadores del poder. Y a ustedes les corresponde, como más
poderosos, buscar el camino limpio y honesto para llegar al corazón
de México.

Debo decirte ---continuó relatándome Remigio--- que el Vicepresidente,
a quien en particular me dirigí, me aplaudió con calor, y creo
que con franqueza; pero los que lo hicieron a rabiar fueron algunos
comensales que representaban al genuino pueblo yanqui. Ese pueblo
---te lo digo a ti ahora y no lo dije entonces por no aparecer
como un adulador vulgar--- es sencillo, inteligente, laborioso
y bueno. Por eso, después de la guerra nos hemos entendido tan
bien. Ahí tienes la historia de ese brindis. Creo que ahora nos
dormiremos; más vale que apagues el quinqué, pues si lees con
esa luz te vas a arruinar los ojos.

Antes de conciliar el sueño los conceptos de Remigio Guerrero
daban vueltas en mi cabeza; me quedaba una duda que me propuse
disipar al otro día.

Nos levantamos tarde y encontramos ya un avión militar que nos
llevó una brújula de repuesto, pues habíamos avisado por radio
nuestro percance. Después del desayuno emprendimos el vuelo a
la capital.

En el camino le dije: ---Ayer me decía usted que el aterrizaje
que hicimos le había recordado algo semejante, acaecido durante
la guerra. Pero no llegó a decirme lo que sucedió.

---Pues satisfaré tu curiosidad. Andaba por el Bajío, donde siempre
me rodearon partidas numerosas de guerrilleros, y tenía que ir
a Tehuacán. Para esas travesías utilizábamos pequeños aviones
que nos recogían en campos improvisados; volábamos de noche.
Despegamos cerca de las seis de la tarde, con un cielo claro,
y pasamos sobre Querétaro ya casi sin luz; a los pocos minutos
dejaba San Juan del Río cuyos faroles parpadeaban en la penumbra.
El aparato era un Bellanca prehistórico del año de la nanita,
que apenas llevaba en su tablero un altímetro, una brújula y
un contador de revoluciones. Yo iba sentado junto al piloto y
cuál no sería mi sorpresa al ver que movía el _switch_ de las luces
y estas permanecían apagadas. A los pocos minutos nos convencimos
de que el desperfecto no tenía arreglo inmediato y tuvimos que
enfrentarnos con el problema de volar a ciegas, sin indicaciones
de ninguna clase.

Por eso te decía yo que, en la acción, nunca me di cuenta del
peligro; siempre tuve algo que intentar, pero allí, en la panza
de aquel pajarraco, volando en la oscuridad y con la certeza
de caer de noche en territorio ocupado, mi vida no valía un comino.
Si salíamos con bien del aterrizaje, cosa bastante remota, era
para sucumbir en manos de los nazis.

Cerró la noche. Las estrellas brillaban y no había una nube.
Créeme que eso nos salvó, pues si nos hubiésemos perdido con
un cielo encapotado no te lo contaría ahora. Las siluetas de
las montañas se perfilaban ante nosotros. Yo le decía al piloto:
---Capitán, haga altura---. ---No puedo ---me respondía--- y
era que volábamos en un vendaval. Entonces comenzamos a rodear
los cerros procurando conservar nuestro rumbo, pero recuerdo
muy bien que con la última claridad del crepúsculo, una gran
estrella se veía por mi ventanilla, que era la del lado derecho.
Más tarde comencé a verla frente a nosotros y después a la izquierda;
comprendí claramente que derivábamos al Oeste. Capitán ---dije---
creo que andamos perdidos. ---Es cierto ---me contestó---. ---¿Usted
qué opina?--- ---Que debemos aterrizar en el primer pueblo
que encontremos---. En esos momentos pasábamos sobre un poblado
grande, eso se veía muy bien por sus largas hileras de luces
eléctricas; después, recapacitando, he creído que era Acámbaro.
Seguimos volando y yo pregunté: ---Capitán, ¿para cuántas horas
trae gasolina? ---Como para cinco ---fue la respuesta---. ---Pues
yo calculo que son más de las nueve ---observé--- hay que tomar
una decisión. Nos hablábamos a gritos para dominar el ruido del
motor.

Al cabo de cierto tiempo distinguimos otro caserío. Los minutos
se me hacían eternos y a cada instante temía escuchar las fallas
por falta de combustible. Aquellos aviones aterrizaban a velocidades
enormes, como todos los de alas extendidas y yo sabía que tomar
tierra en una noche sin luna y en un país montañoso era la muerte.
Entonces sí experimenté miedo, pues me sentía maniatado, sin
poder hacer nada; la impotencia descompensaba mis emociones y
sentía la lengua salobre y reseca dentro de mi boca.

Capitán ---le dije--- descienda lo más que pueda sobre los techos.

Iniciamos entonces un descenso en espiral; me daba cuenta de
que volábamos en un cajete. Al banquear parecía que las alas
rozaban los cerros cuyas masas negras se recortaban sobre el
cielo estrellado. Cuando estábamos como a ciento cincuenta metros,
vimos a las gentes echarse a la calle y alzar la cara; bajando
un poco más me convencí de que eran vecinos del pueblo. Mientras
tanto seguíamos trazando círculos; unos cuantos soldados estaban
en la pequeña plaza.

Entonces muchas gentes salieron dirigiéndose hasta sitios donde
no se distinguía ninguna luz, con hachones encendidos que como
fuegos fatuos parecían caminar por sí solos y se alineaban formando
los lados de un inmenso paralelogramo.

Capitán ---dije--- allí hay que aterrizar.

Comenzamos a tirarnos sobre esa extensión negra de tierra donde
no veíamos nada fuera de las teas flameantes; pero el piloto
la encontraba pequeña, pues volvíamos a remontarnos.

Capitán ---le dije--- nuestra única salvación está en aterrizar
cuanto antes. Si encuentra el campo corto baje en _sideslip_. Entonces
mi piloto comenzó a ensayar la maniobra; la efectuó una, dos,
tres, veces. Finalmente me sentí irritado. ---No es momento para
indecisiones ---le grité---. Déjeme ensayar una vez más ---me
dijo suplicante--- siento que se me acaba el campo.

Y cuando inclinaba el ala izquierda y el avión parecía patinar
en el aire, nos paramos en seco con un gran fracaso de cosas
rotas. Era que habíamos rozado una de las faldas de la colina.
De pronto me di cuenta de que la máquina estaba de nariz en el
suelo; yo me incorporé pues el choque me había echado de bruces
sobre la rueda de control. Sentí que no tenía un rasguño y procuré
salir cuanto antes. Mi piloto alzaba la cabeza con pesadez. ---¿Está
usted herido?--- No ---me dijo--- creo que no.

Abrí la portezuela y procuré descender; me hallaba como a dos
o tres metros de la tierra. A tientas y sujetándome como pude
logré descolgarme. Apretaba contra mí un portafolio donde traía
datos de importancia; el capitán saltó también, al mismo tiempo
que en la oscuridad escuchábamos una algazara. Corrimos hasta
detrás de un árbol mientras las gentes se aproximaban al aparato
destrozado. Las oímos hablar en español: ---«no, no son boches»---
«este aparato es yanqui». ---«Hay que encontrarlos antes de que
los cabrones nazis vayan a joderlos»---. Estas palabras sonaron
dulcísimas en mis oídos.

---Aquí estamos ---dije--- Viva México. Yo sabía que me jugaba
el todo por el todo.

En esos momentos oímos a lo lejos sonidos guturales de voces
alemanas. Un hombre se me acercó levantando hasta la altura de
mi rostro una linterna de petróleo. Comprendió desde luego quiénes
éramos pues jalóme violentamente. ---Vengan conmigo ---dijo---y
lo seguimos con apresuramiento. Caminamos tras él durante más
de media hora, hasta llegar a una casita.

Entonces hubo que arreglar la huída, pero para esta clase de
operaciones me sentía tranquilo y dueño de mí; el miedo ---para
darle su nombre--- se había esfumado. La acción, el movimiento,
me devolvieron mi aplomo y la salida de ese trance fue una rutina,
aunque este término te haga sonreír. Desde entonces no volví
a subir a un avión ---durante la guerra--- sin paracaídas. Creo
que este fue el peligro mayor que corrí en esos años de lucha;
y ya ves que el episodio estuvo muy lejos de las actividades
tradicionales del guerrillero que merodea, asalta y mata. Iba
a morir como un ciudadano pacífico que hace un paseo dominguero
en aeroplano; esas son las paradojas de la vida.

La travesía desde Tezontepec a la metrópoli la hicimos sin incidentes.
Aterrizamos en las azoteas del Nuevo Hospital Civil que se encuentra
por Coyoacán, en medio de una inmensa huerta de árboles añosos;
bajamos y nos despedimos. Remigio se quedó en el edificio y yo
tomé un autobús para ir al centro de la ciudad.

En resumen, muy contados detalles obtuve de la charla con Remigio.
Sé que cuando anunció a Don Rodrigo su resolución de unirse a
las guerrillas, se dirigió al Norte y de allí entró a la región
de los Altos en Jalisco. Los rancheros estaban ya en efervescencia;
los nazis habían establecido guarniciones militares hasta en
los pequeños caseríos, pero en los ranchos las gentes hacían
conciliábulos y hablaban entre sí sin tapujos. Finalmente varios
grupos iniciaron la lucha: se apostaban en las noches cerca de
los poblados y cazaban a los centinelas. Un tal Francisco Jiménez
que era famoso como tirador pues empuñaba dos pistolas con ambas
manos y a cuarenta metros ponía las dos balas en un blanco, comenzó
a rayar la culata de su carabina con marcas que indicaban los
boches abatidos.

Los campiranos ocultaban sus rifles en los maizales y si veían
a lo lejos una patrulla de invasores les tiraban apoyándose en
los troncos de los huizaches que sombreaban las sementeras. Con
ellos se juntó Remigio y, poco a poco, principió a constituirse
en el alma de aquellas actividades.

Cuando se hubieron reunido cuatro o quinientos hombres caían
sobre las patrullas nazis. Generalmente ejecutaban esas incursiones
en las altas horas de la noche matando a los centinelas con arma
blanca o estrangulándolos y degollándolos con una cuerda de piano.
Pasaban a los boches a cuchillo y huían silenciosamente.

Los invasores enviaron entonces a la región a un tal Buhle con
cuatro o cinco turiferarios del _quisling_ Cabañas intentando hacer
propaganda pacifista; era todavía la época en que se pretendía
que el pueblo mexicano comulgase con la rueda de molino de que
lo acaecido era un movimiento nacional y de que los alemanes
habían llegado con el único objeto de protegernos de los yanquis.
Pero una noche atraparon a Buhle y lo ahorcaron junto con dos
de los enviados de Cabañas, en un lugar llamado Yahualica.

En represalia llegaron tropas en gran número e iniciaron los
atropellos; pero ya la mecha estaba prendida y aquello se propagó
como un incendio en una troje llena de trigo.

Otros grupos surgieron por diversos rumbos y el país entero era
como un volcán que se forma en un terreno llano; la lava y los
gases brotan por diferentes partes. Miles de muchachos que por
patriotismo se habían inscrito en el servicio militar y que habían
sido mal alojados y alimentados por la incapacidad burocrática
del régimen legítimo, se unieron a las guerrillas y desperdigándose
por todo el país fueron predicando con la palabra y el ejemplo
una cruzada de exterminio contra el invasor. Surgieron grupos
en Veracruz, en la Sierra de Puebla, en la Huasteca, en las malezas
tropicales de Sinaloa, en las regiones pantanosas de Tabasco,
en las montañas de Guerrero, en los pueblos y aldeas del bajío,
en las inmensas sabanas de Chihuahua.

El pueblo, con el instinto del léxico castizo, los llamó insurgentes
y con ese nombre entraron a las noticias de los diarios, después
a las crónicas y seguramente, cuando se haga la historia completa
de esos tiempos el apelativo perdurará. Fueron un puñado, doscientos
mil, quizás trescientos mil; pero sus ataques despertaron la
zozobra entre los invasores los cuales juzgaron que sería una
locura seguir su conquista mientras no hubiesen pacificado por
completo el territorio que pisaban. Como los guerrilleros eran
intangibles y como entonces los mexicanos se dividieron en tres
grupos únicos: el de los traidores ---agrupados alrededor del
gobierno pelele--- los saboteadores y los insurgentes, los nazis
se sentían perpetuamente amenazados.

Esa clasificación tan simplista se la debo a Berrueta; él es
quien me relata a veces, con cierta prolijidad, cómo se efectuó
el milagro de nuestra resurrección.

---Mira ---es Berrueta quien habla--- antes de la guerra yo creía
muchas veces que éramos un país perdido. Las gentes de mi clase
vivíamos encastilladas en nuestro egoísmo; en una nación de escasos
hombres aptos ganábamos lo suficiente y aun lo superfluo sin
esfuerzos grandes. Los indios, desnutridos y miserables después
de treinta años de logorrea revolucionaria, trabajaban para todos
y nos mantenían a todos. Una minoría audaz mangoneaba el poder:
los funcionarios se enriquecían e iban formando una casta despreciable
de nuevos ricos. No teníamos ningún nexo de unión, es decir,
la unidad nacional no existía: carecíamos de unidad de raza,
de cultura, de idioma; de unidad de hábitos. En fin, te estoy
repitiendo lo que ya has oído hasta el cansancio.

Entonces vino la guerra, llegó la invasión y de pronto, como
el que ha vivido largos años alienado y recibe bruscamente un
choque que le devuelve el juicio, así el país entero se sintió
reintegrado a su ser. Se acabaron los revolucionarios, los blancos,
los rojillos, los patronos ávidos, los sindicatos deshonestos,
los burgueses y los proletarios. La tragedia nos enseñó de súbito
que necesitábamos reclasificarnos de acuerdo con nociones más
sensatas y humanas; pero que antes que eso era preciso una integración
unánime en un gran grupo que luchase para defender nuestro territorio,
la esencia de nuestra vida y la libertad del mundo.

Las frases anteriores se las he leído a Héctor Berrueta.

---¿Está usted conforme con ellas? ---le he dicho--- ¿reproducen
con fidelidad sus palabras?

Hombre, nunca he pronunciado una arenga semejante ---me respondió---
pero viéndolo bien están de acuerdo con lo que pienso yo, con
lo que piensa Remigio y con el pensamiento de la inmensa mayoría
de mexicanos amantes de su patria que pasaron por esa prueba
tan dura.

</section>
<section epub:type="chapter" role="doc-chapter">

# XII

++Habían++ pasado cuatro meses desde que Don Rodrigo fue llevado
al campo de concentración. La vida había tomado un compás de
dureza cruel. Poco a poco se fue descorriendo el denso velo de
ignorancia en que los nazis lo hacían vivir y supo que tanto
él como gran parte de los concentrados trabajaban en unas minas
de mica, mientras el resto se ocupaba en ampliar la carretera
a Panamá. Supo también que el campo, que se denominaba para fines
de identificación «Campo número siete», estaba encaramado sobre
la sierra Mixteca, a unos setenta kilómetros de la ciudad de
Oaxaca.

Después de la primera semana de encierro vivió unos días que
recordaba con espanto. Se trató de arrancarle confesiones que
atañían a los planes de Remigio. Este había partido del pueblo
después de anunciarle su resolución y no había vuelto a saber
de él; pero sus verdugos no podían creerlo así. Una noche lo
incomunicaron en una pocilga en tinieblas; más tarde entraron
con una lámpara y un oficial lo interrogó.

Contestó con claridad; relató que su hijo había estado en San
Miguel y había partido a Guanajuato a continuar su trabajo de
médico. ---¿Sabe usted que está en abierta rebelión contra el
orden nuevo?--- No lo sé ---fue la respuesta de Rodrigo.

Entonces lo dejaron allí solo y todo parecía haber terminado,
pero ya muy tarde entró el oficial que trabajaba como taquimecanógrafo
y le pidió que dictara su declaración. Después que la hubo tomado
aconsejó a Rodrigo que fuese más explícito. ---Su fábula no va
a satisfacer a los jefes ---le dijo--- ellos quieren que usted
confiese. ---Nada tengo que confesar. ---Bueno, yo le hago esa
advertencia; ya usted sabrá lo que hace. Por lo pronto hoy va
usted a dormir aquí; son las órdenes---. Al día siguiente llegó
un capitán ---nunca pudo retener su nombre--- y con el rostro
torvo procedió a interpelarlo. Ante sus afirmaciones reiteradas
agregando que nada podía añadir, el capitán tomólo de la camisa
con la mano izquierda y le descargó un puñetazo en la cara. Rodrigo
cayó contra los muros del tugurio; entonces con la punta de la
bota lo hizo incorporarse y volvió a golpearlo; lo obligó a levantarse
de nuevo y repitió por seis o siete veces esta maniobra bárbara.
Al terminar le dijo: ---esto es solo el principio. Tiene usted
hasta el medio día para reflexionar en su situación---. Salieron
dejándolo solo. Rodrigo quedó con la boca tumefacta, los ojos
congestionados, un labio partido, la nariz hinchada; la sangre
le brotaba copiosamente.

Y en efecto eso fue solo el principio. Rodrigo sabía que volverían,
reflexionó que en realidad no podía decir gran cosa más. Podría
decir sin dañar a Remigio que se había lanzado a la revuelta,
pues sus actividades eran ya conocidas; Remigio era un pregonado.
Pero su condición ---la de Rodrigo--- se haría peor. Resolvió
callar. Siguió entonces una etapa de torturas que es duro describir,
como se hace duro describir las lepras, los vicios de los invertidos
o las pasiones degradantes. Ese medio día llegó el mayor Böckh,
jefe del campo, quien después de nuevas amenazas lo sacó a un
gran corral donde se mantenían algunas vacas y bestias de trabajo;
a la vista de los presos que permanecían en el campo para hacer
la limpieza y servir a la oficialidad, lo mandó azotar hasta
que el pobre hombre quedó allí tendido, inconsciente, con la
cara sobre el estiércol. Permaneció así más de una hora, sin
poder incorporarse; por fin dos reclusos, dejando sus quehaceres,
se acercaron a ayudarlo; lo levantaron de las axilas y él caminó,
arrastrando los pies, hacia su sitio en la galera. Iba quejándose
dolorosamente, la frente perlada de sudor y los pelos, ya un
poco ralos, pegados en el cráneo. Con el rostro lleno de majada,
inconocible a causa de los golpes recibidos y la camisa adherida
a la espalda por la sangre ya seca de los verdugones, parecía
la viva imagen de Cristo. Sus cireneos, sin pronunciar una palabra,
lo llevaron hasta dejarlo tendido. Allí se quedó, jadeante, sintiendo
el palpitar desordenado de su corazón, preguntándose a sí mismo
si todo eso era real, si estaba vivo, si él era quien era. Se
quejaba, sin poderlo remediar, con la boca abierta y la baba
escurriéndole hasta sus ropas; en sus instantes de lucidez sentíase
morir y oraba, tartamudeando, en alta voz. Se le dejó allí abandonado.
Después se repitió la tortura varias ocasiones. Cuando salió
de ese sopor de sufrimiento lo tuvieron unas semanas ocupado
en labores de limpieza junto con otros presos a los que llamaban
faeneros; barría las enramadas, lavaba los cacharros del rancho
y recogía los excrementos, porque los reclusos no tenían lugares
de aseo.

Las tijeras y las navajas de afeitar estaban proscritas, pues
nadie podía tener en su poder ningún utensilio cortante, de manera
que los prisioneros llevaban largas barbas mugrientas. A los
pocos días Rodrigo comenzó a sentir comezones torturadoras y
al final del primer mes de su encarcelamiento los piojos y las
ladillas vivían en innumerable contubernio sobre todo su cuerpo.
El agua era escasa; las pipas, montadas en camiones, la llevaban
diariamente al campo, pero apenas bastaba para beber y para la
limpieza de la tropa. La oficialidad y los jefes eran los únicos
que disponían de ella a discreción.

Después lo sacaron nuevamente a trabajar a la sierra, pero entonces
comenzó a notar cosas extrañas e inesperadas. El capataz lo trataba
con benevolencia; le medía las tareas en tal forma que siempre
las completaba. Era cierto que alzaba la voz al estar presente
algún sargento nazi, pero en caso contrario permitía descansar
a sus presos y les menudeaba las raciones de agua. Guardaba sin
embargo las apariencias, porque sabía que los oficiales se apostaban
en los cerros vecinos y desde lugares bien escogidos observaban
con sus binoculares el trabajo de las cuadrillas; algunas faltas,
constatadas así, a la distancia, habían terminado en azotinas
disciplinarias destinadas a mantener el terror y la sumisión.

Rodrigo comenzó a sobrellevar un poco mejor su existencia de
forzado. La comida lo hacía sufrir, pues les daban dos raciones
al día, de pan y tortillas; en la de la tarde añadían arroz o
frijoles y una vez por semana un picadillo de carne. Esto era
poco, muy poco para sus hábitos de burgués bien alimentado que
siempre había comido sin medirse, que tomaba en las mañanas leche
cremosa, mantequilla y huevos frescos. Pero Rodrigo, que tenía
un espíritu equitativo, había juzgado siempre sus problemas de
bienestar, no como cosas absolutas, sino relativas, y allá en
San Miguel apaciguaba sus sueños de riqueza con el trato de su
clientela que venía descalza a consultarlo de los ranchos.

En aquellas montañas comparaba su suerte con la de los tres o
cuatro mil indígenas asalariados en la carretera quienes recibían
raquítica paga y parecían indiferentes a todo lo que acaecía
a su alrededor. Hablaban entre sí en su ininteligible idioma
musical y completaban, con indiferencia, tareas capaces de mediomatar
de fatiga a los reclusos. En el fondo resentían hondamente la
opresión teutona, pues eran maltratados sin tregua; su estoicismo
los hacía aparecer impasibles, pero odiaban al invasor.

Rodrigo trabajaba, en ocasiones, en camaradería con ellos y se
sentía conmovido cuando lo invitaban a compartir su rancho de
hombres libres, pero que era tan miserable como el de los prisioneros.
El, notario, ignorante de todo lo que no fuese la vida de San
Miguel, supo entonces que se organizaban en grupos, de acuerdo
con el caserío del cual procedían y que uno entre ellos, al que
llamaban tlacualero, iba a pie una o dos veces por semana, recorriendo
sesenta u ochenta kilómetros, a traer la pitanza; los hombres,
desde los sitios de trabajo, los oteaban a lo lejos, con vista
penetrante y encendían de antemano fogatas con las ramas y hojarasca
del monte. Los tlacualeros entregaban los itacates liados en
servilletas desteñidas que invariablemente contenían tortillas
de maíz, sal, chile, a veces jarros con frijoles hervidos y allá
de cuando en cuando un poco de carne secada al sol. Esta ración
de media semana la guardaban bajo la enramada donde dormían;
en ocasiones, si el sitio del trabajo estaba lejano, apartaban
desde la madrugada la comida del día y cargaban con ella poniéndola
sobre la cabellera hirsuta, bajo el sombrero de palma, o la acarreaban
en morrales de pita. A las ocho de la mañana encendían lumbradas
y sobre las brasas y cenizas calientes sancochaban las tortillas
del almuerzo sazonándolas con sal y chile y las comían enrollándolas
una tras otra, silenciosos, como si celebrasen un rito. Más tarde,
a las doce, volvían a comer sus tortillas de maíz; de vez en
cuando con frijoles. Rodrigo observaba cómo, en los días lluviosos,
los tlacualeros llegaban mojados a pesar de sus capas de palma
y entregaban los envoltorios húmedos; el aire cálido y encerrado
ponía sobre las tortillas una flora verde de hongos microscópicos.
Entonces aquellos indios las limpiaban tallándolas con piedras
rugosas y después las tostaban en el fuego, para secarlas. Se
mostraban impasibles; esa había sido siempre su comida: no de
ahora, con los alemanes dominando el país y obligándolos a trabajar
en esas obras de estrategia, sino de mucho antes, desde los tiempos
del general Díaz, desde los tiempos revolucionarios en los cuales
recibían muchas promesas cuando se hacían las campañas políticas
para venir después del triunfo a comer el mismo alimento miserable.

Pero en realidad esas gentes les debían tan poco a todos los
gobiernos de México que uno casi los habría absuelto por echarse
en manos de cualquier invasor humano e inteligente. Sin embargo,
supieron defender su tierra con una lealtad insólita. Guiados
por un fatalismo ancestral, realizaron las hazañas más increíbles,
las que requerían resoluciones más desesperadas y suicidas y
con un instinto fino de raza oprimida comprendieron que todavía
en México podían surgir gentes criollas o mestizas capaces de
pensar en ellos en términos de desinterés y justicia. Su mismo
instinto les dijo también que, en cambio, para esos hombres blancos
y rubios de lenguaje áspero, eran solo las bestias necesarias
transitoriamente para alimentar a un ejército de invasores. Por
eso no vacilaron en su elección y los soldados teutones se sintieron
a la postre, acosados en aquellas serranías.

Su contacto reconfortó mucho a Rodrigo. Una mañana el aguador,
que no era un recluso, sino un indígena a jornal, puso en sus
manos al llevarle el agua un papel doblado y mugroso. Fuese entonces
a apoyar con la cara hacia el frente duro de la cantera en actitud
de fatiga y lo extendió; era la letra de Remigio: «Padre, todos
estamos bien y confiamos en Dios. Escríbele a mi madre que está
desesperada sin saber de ti. Envía tu carta por el mismo conducto.
No te olvidamos».

Rodrigo sintió como un bálsamo sobre su corazón. Allí mismo destruyó
el pequeño pliego; pero antes lo leyó y releyó hasta grabarlo
en la memoria. Y después lo repetía: «todos estamos bien… escríbele
a mi madre… no te olvidamos». Prosiguió el trabajo con el espíritu
ligero, no sentía la fatiga. Pensaba que quizá volvería a verlos
reunidos a todos, en su casa de San Miguel; que esa familia apretada
por el amor sería nuevamente una realidad. Por mucho tiempo lo
envolvió una oleada de optimismo. Pensaba que Patricio abriría
los ojos y dejaría de formar parte de esa apariencia postiza
que no era México y hasta llegaba a soñar que iría a reunirse
con Remigio, a luchar por la libertad. ¡Libertad, libertad! ---pensaba---.
Qué fácil es decir la palabra, pero cuán difícil conocer que
se tiene; y él la tuvo su vida entera, a pesar del general Díaz,
a pesar de la Revolución, a pesar de los yanquis, de la prohibición
de los cultos y del artículo tercero. En los tiempos más duros
de la persecución religiosa, había tenido siempre en San Miguel,
como buen católico, un rincón oculto dónde oír su misa dominical
y también quien lo escuchara en confesión por Pascua Florida.
Y él ---notario en el pueblo--- se reunía con los vecinos en
la botica, en el frontón, en el jardín y en la misma Presidencia
Municipal, y hablaba mal de Calles, de Rodríguez o repetía el
chascarrillo diario acerca de los disparates de Ortiz Rubio.
Eso le hacía comprender que una corriente vital mucho más fuerte
que el poder público sofocaba los intentos de sujeción y que
hasta entonces había vivido como hombre libre en una tierra libre.
¡Libertad! Que no se siente cuando se tiene; que es como el aire.
Pero que cuando falta mata como la carencia de oxígeno; que cuando
nos la quitan recordamos que es algo que se palpa y se ve; se
masca y se toca como una cosa material.

Al día siguiente el aguador le proporcionó una espina larga tallada
con destreza y una tinta de carbón y zumo de una baya silvestre.
Así pudo escribir Rodrigo unas cuantas líneas que habrían de
dar a Verónica una poca de esperanza. En ellas le hablaba tranquilo,
le decía que estaba bien y que con la ayuda de Dios se verían
pronto. Desde entonces esos mensajes se cruzaban con frecuencia;
los indios, en el lugar del trabajo, se los entregaban envueltos
en las tortillas o con el cigarrillo de hoja de maíz. Don Rodrigo
a su vez sabía que cuando él escribía podía fiar sus respuestas
a cualquiera de ellos.

En la dureza de su existencia esas comunicaciones eran un motivo
para existir; una dulce cadena que lo ataba a la vida. La letra
clara y concienzuda de Verónica, la menudita de Ana, los rasgos
anchos y redondos de Remigio; todo eso le traía reminiscencias
de felicidad.

A pesar de este consuelo su salud se minaba a ojos vistas. Una
epidemia de disentería se declaró en el campo. Veíanse por doquier
rostros afilados; bajo los párpados de los enfermos había sombras
que bajaban hasta las barbas mugrientas y en sus sienes tintes
grises. La asquerosidad de esa peste deprimió el ánimo de los
reclusos y se redujo a lo ínfimo el rendimiento del trabajo.
La escasez de agua era una tortura; nadie cuidaba de su aseo
y soportaban la piojería con indiferencia. Los hombres no podían
permanecer en sus tareas a pesar de las amenazas y de los castigos;
algunos, en las mañanas, se rehusaron a salir del campo. Cuando
comenzó la mortandad se hizo venir violentamente un médico; pero
los pacientes perecían como ratas y el pánico comenzó a cundir
aun entre la misma soldadesca.

Don Rodrigo tuvo un ataque relativamente benigno, que relajó
sus energías y lo sumió en el pesimismo más negro. Sin embargo,
fue ese estado de espanto colectivo el que favoreció las ocultas
maniobras de liberación que se venían efectuando con paciencia
infinita desde hacía muchas semanas. La epidemia justificaba
el abandono repentino de las labores a cualquier hora del día
y la obediencia dejó de ser estricta. En las mañanas se llevaba
a enterrar a los muertos del día anterior; hubo vez que fueron
diez o doce. Los soldados invasores dejaban que los prisioneros
se encargaran de esas inhumaciones; como temían el contagio las
presenciaban de lejos. Mientras los reclusos cavaban las fosas
hablaban entre sí con entera libertad, seguros de no ser oídos;
se confiaban las noticias que uno u otro había recibido: los
fracasos de los invasores, la resistencia de algún villorrio,
los golpes de mano insurgentes. Era indudable que la disciplina
se relajaba y hasta el mismo mayor Böckh había pedido ya el traslado
del campo y la instalación de un hospital reglamentario de campaña.
Pero en México tenían problemas más serios que atender.

Los explosivos eran manejados al principio exclusivamente por
los alemanes; ellos los sacaban de los polvorines y cargaban
los barrenos destinados a volar los cortes de los cerros o a
ampliar los tiros de la mina. La labor era dura y fue pasando,
poco a poco, a los hombres asalariados en los trabajos.

Esta circunstancia permitió la sustracción sistemática de pequeñas
cantidades de dinamita, la que fue acumulándose hasta constituir
en manos de los reclusos un arma. Así principiaron los aprestos
de fuga cuyo plan estuvo concebido por Ricardo Almanza, un periodista
llevado al campo número siete debido a su resistencia para escribir
en los periódicos colaboracionistas de la Capital. En las largas
noches en vela analizó las fallas del sistema de vigilancia
de los alemanes, quienes contaban demasiado con el estado de
sumisión al que habían llevado a los presos. Almanza ideó el
mecanismo, sencillo en sí; pero era preciso encontrar los angostos
intersticios por dónde poderse introducir para resquebrajar esa
estructura de órdenes cuartelarias.

La primera medida consistió en el hurto de los explosivos que
los presos iban introduciendo al campo, gramo por gramo. En las
noches, y de un hombre a otro, pasaba la recolecta diaria que
concentraban en aquel que dormía en el centro de cada cobertizo;
allí, abriendo con las uñas un socavón en el suelo, se iba ocultando
el botín que envolvían con el papel parafinado de los cartuchos.
Pronto se supo entre los presos que era Ricardo Almanza el autor
de aquellas maniobras y sus órdenes, susurradas durante el trabajo,
se ejecutaban sin reparo.

Al aumentar la cantidad de explosivos se hizo urgente disponer
de ellos; había que minar la caserna, las casetas de los oficiales,
pero particularmente la habitación del mayor Böckh. Los edificios,
todos de madera, estaban montados en zancos que dejaban un estrecho
espacio vacío entre los entarimados de los pisos y el suelo húmedo.
Una tarde al hacer la limpieza del campo, uno de los faeneros
echó materias pestilentes bajo las casetas de Böckh y los oficiales.
Por la mañana estos protestaron indignados; golpearon como medida
disciplinaria a quienes juzgaron los presuntos reos de esa marranada
judía, y dieron órdenes para que se hiciese diariamente un aseo
cuidadoso de esos sitios; entonces con las toscas cucharas de
madera del rancho, los presos encargados de la limpieza comenzaron
a excavar bajo el centro de los edificios. Tomaban de los escondites
las bolas de dinamita envueltas en papel parafinado y las iban
colocando, una junto a otra, en esos agujeros que tapaban cotidianamente
con tierra.

A veces la lentitud de los preparativos desesperaba a los reclusos,
pues todos ansiaban la libertad y hubieran dado su vida por ver
emancipados a sus compañeros. Pero la consigna era obrar con
prudencia y una onda optimista recorría los dormitorios cuando
se sabía que el plan se desarrollaba sin tropiezos. Cuando Almanza
calculó que bajo los edificios había acumulado el equivalente
de algunas cajas de dinamita, se decidió a obrar.

Contaba con la fuerza eléctrica de la planta de luz; pero la
conexión de los alambrados era la maniobra más peligrosa y decidió
ejecutarla él mismo. Una noche hubo una coincidencia que a Almanza
parecióle casi milagrosa. Como a las once se descompuso un faro
buscador; precisamente el que protegía con su haz de luz las
casetas minadas y la galera donde él dormía; probablemente por
no bajar a esa hora hasta los almacenes, no fue reparado. Como
a las diez, en esa misma galera falleció un enfermo de disentería.
Entonces, contra todas las reglas, uno de los presos fue a dar
cuenta del suceso al centinela, quien le dio un culatazo, pero
en cambio alejóse de ese sitio hasta donde la disciplina se lo
permitía.

Almanza salió de la galera arrastrándose. Le era preciso hacer
primeramente las conexiones de la caserna de la tropa. Pasó al
pie de la torre apagada y se deslizó bajo el entarimado. Púsose
a trabajar con calma; era una noche oscura y sabía que tenía
varias horas por delante. Estaba decidido a hacer las cosas despacio
y a no comprometer el buen éxito con ninguna torpeza. Después
a gatas dirigióse al edificio de la oficialidad; cuando llegaba
a él escuchó ruido de pasos y el haz de luz de una antorcha eléctrica
barrió el suelo; pero el centinela dio media vuelta sin verlo
y entonces pudo arrastrarse bajo el entresolado. Su corazón latía
con más calma y finalmente cuando penetró bajo la habitación
del mayor Böckh recobró por completo su sangre fría. Habíase
hecho de un pequeño trozo de cañuela de pólvora y unos fósforos
y estaba dispuesto, si lo descubrían, a volar junto con la caseta
del jefe del campo. Este pensamiento lo hacía sentirse dueño
de sí. Pero el cielo estaba por él; pudo terminar sus conexiones
y llegar hasta los cables de la planta eléctrica. Almanza traía
jalando los dos alambres; llegó y amarró uno de ellos a uno de
los polos de la luz que iba a la habitación de Böckh, en seguida
conectó otro que llevaba suelto al segundo polo y se arrastró
trayendo tras de sí la vara mágica que iba a darles la libertad.
Los últimos cincuenta metros los recorrió alagartado sobre el
suelo, presa de una elación sin nombre. ¡A la menor alarma, podría
provocar la explosión! Allá, muy adentro, abrigaba el temor de
un fracaso, de una conexión mal hecha; pero no; había tomado
sus precauciones. Si una carga fallaba las otras darían resultado;
en cualquier forma el daño sería enorme. Llegó así hasta su camastro;
escuchó murmullos que tuvo que acallar con un ¡chis!… imperioso.
Sentóse y fue cuando sintió en sus entrañas la inquietud de la
incertidumbre. El campo entero estaba en vela aguardando la señal
convenida y la espera hizo suponer a muchos que todo había fracasado;
pero él se esforzaba en obrar con lentitud, en demostrarse a
sí mismo que era dueño de sus nervios. Notó que sudaba y se limpió
la frente con el revés del brazo. Entonces lanzó un silbido prolongado,
como el de un arriero; un silbido de esos que en las barrancas
recorren el aire de la noche como un cohete y despiertan los
ecos. Lo dejó extinguirse y en el momento en que escuchó el principio
de un grito gutural y cuando los dos fanales encendidos giraban
como ojos alocados, juntó una con otra las puntas de cobre. Por
una fracción de segundo pareció que nada ocurría; después escuchóse
un estallido inmenso. Tres lumbradas como tres volcanes brotaron
de la tierra y a su luz lívida vióse una masa informe de materias,
de objetos y de cuerpos humanos que se elevaba en el aire y se
perdía en lo negro del cielo. Al mismo tiempo las ametralladoras
perchadas en lo alto comenzaron su traque traque apresurado;
pero un inmenso clamor alzóse por todas partes. Cientos de reclusos
trepaban ya las escaleras de las torres; otros, del cobertizo
de Ricardo Almanza, haciendo a un lado los pedazos arrojados
por la explosión, se abalanzaron sobre los centinelas de la puerta,
los acribillaron a golpes, los destrozaron con las mismas bayonetas
que llevaban enfundadas en la cintura y abrieron la puerta de
hierro.

En realidad lo que dio completo éxito a los prisioneros fue la
sorpresa. De los doscientos y pico de hombres que guarnecían
el campo, apenas si cuarenta habían sido víctimas de de la hecatombe.
Los demás estaban ilesos. Pero nadie esperaba el atentado y,
sobre todo, Böckh y toda la oficialidad perecieron. Cuando los
reclusos viéronse dueños de la situación entregáronse a una desenfrenada
matanza; despojaban a los soldados de sus armas y a quemarropa
disparaban contra ellos las cargas enteras de sus pistolas automáticas.
Otros los echaban por tierra y con las culatas de sus propios
fusiles les machacaban los cráneos sobre las piedras del suelo.

La soldadesca nazi que escapó a esa San Bartolomé salió huyendo
hacia las tiendas de los ingenieros desparramadas a lo largo
de la carretera, con el propósito de llegar a la estación y ponerse
en contacto telegráfico con Oaxaca. Las radiotransmisoras portátiles
de las obras comenzaron a dar las noticias a diestra y siniestra
y ya para la madrugada, de Tehuacán, de Puebla y de Oaxaca salían
fuerzas para batir a los reclusos liberados.

Estos cometieron un error grave; cuando se quedaron solos, dueños
de todos los elementos del campo, su triunfo los cegó; su única
mira consistió en salir y tener la sensación del aire, de caminar
sin la vigilancia de los esbirros, de tirarse cuando el cansancio
los dominase. El resultado había ido más allá de sus más locas
esperanzas y sus espíritus, después de meses de opresión brutal
no estaban preparados para la acción coordinada que el caso requería.
En lugar de reunirse, de discutir un plan de huída, de armarse,
de enviar sus avanzadas para conocer con exactitud el peligro
que inevitablemente iba a amenazarlos, solo pensaron en dejar
aquel lugar donde habían sido vejados y torturados.

Todavía envueltos en la oscuridad comenzaron a salir en grupos
de cuatro o de cinco. Tomaban el camino de guijos hacia la carretera.
Otros se echaban por los cerros, sin rumbo. En esta confusión,
y después de la voladura, Rodrigo se vio perdido, obligado a
obrar en circunstancias anormales. Quedóse agazapado bastante
tiempo en su rincón escuchando los balazos, las imprecaciones
y los insultos. Cuando el ambiente se tranquilizó un poco, levantóse
y comenzó a orientarse en la oscuridad; vio grupos que pasaban
atropelladamente rumbo a la salida como manadas de sombras. En
aquel desconcierto comprendió que debería evadirse; pero lo dominaban
dos ideas, una de ellas tan inocente que más tarde lo haría sonreír.
Primero quería armarse, alistarse para vender cara su libertad
si pretendían apresarlo y la segunda era que necesitaba obtener
con qué afeitarse y cortarse el cabello. A los pocos pasos topó
con un cuerpo inerte, inclinóse y comenzó a palpar la tela gruesa
de un vestido, quizá un abrigo. Después un cinturón con un revólver
automático; al desprenderlo sus dedos tropezaron con otra funda
cilíndrica y dentro de ella un tubo metálico: era una luz eléctrica.
La encendió; dirigióla a todo el cuerpo sobre el cual estaba
inclinado y vio que tenía ante sí un oficial alemán cuyo rostro
deshecho no era sino una masa sanguinolenta. Rodrigo lo despojó
de su sobretodo, que se echó al brazo sin ponérselo, después
ciñóse el cinturón con la automática y dirigió su haz luminoso
hacia lo que había sido la casa de los oficiales; un ángulo aparecía
aún en pie.

Penetró a un cuarto a medio derruir, entre fierros retorcidos,
pedazos de muebles, ropa regada y manchas de sangre. Allí, como
una sonrisa del cielo, intacta encima de un lavabo hecho pedazos,
estaba una repisa y sobre ella una máquina de afeitar, unas tijeras,
un jabón. Sintió un optimismo incontenible; aquello le parecía
un regalo de la Providencia; se echó estas cosas al bolsillo
y dirigióse hacia la salida con los grupos que en la oscuridad
charlaban a gritos, reían o cantaban. Así se fue desalojando
el campo y para el alba estaba desierto; la aurora sorprendió
a los reclusos desparramados sobre las montañas, como rebaños
de cabras.

Don Rodrigo bajó solo por los riscos, caminando de prisa; quería
alejarse todo lo posible mientras comenzaba la persecución, que
veía inevitable. Guiándose por el sol se dirigió hacia el norte;
quería ir a un centro poblado para ponerse en contacto con los
insurgentes y reunirse a Remigio. Anduvo todo ese día, sin sentir
la fatiga, ni el hambre; entonces se dio cuenta que la labor
forzada del campo había endurecido sus músculos y lo había hecho
frugal. Bebió el agua de las charcas y en la tarde, cuando sintió
frío, se caló el sobretodo nazi y buscó un refugio para dormir.
Encontró una roca junto a unas malezas y allí echóse desde el
crepúsculo. Durmió una eternidad, con un sueño profundo que no
disfrutaba hacía meses; con las hojas secas habíase hecho una
almohada y un colchón con las agujas de las coníferas.

Despertó a la mañana siguiente con el sol bien alto y prosiguió
su caminata. Comenzaba a tener hambre cuando encontró un pastorcito;
un chiquitín que le dio unas tortillas y un jarro de leche de
cabra que Rodrigo saboreó con deleite y le dijo de un pequeño
venero, a tres o cuatro kilómetros. Efectivamente, el líquido
lloraba de las rocas y bajaba a una poza de unos tres metros
de diámetro. Desnudóse y se metió al cristal límpido. Entonces,
en su reflejo, contempló sus barbas largas y sucias; sacó sus
tijeras y se puso a cortarlas, mechón por mechón. Se enjabonó
y principió a afeitarse; sentía una delicia indescriptible al
ver cómo al paso de la hoja le iban quedando tersas las mejillas;
más tarde, guiándose apenas por su tacto, procedió a cortarse
los cabellos. Lo hacía despacio, no le importaba permanecer allí
toda la tarde o la vida entera. Sabía que iba a quedar trasquilado,
igual que una oveja, pero no le preocupaba; eso era la limpieza,
el agua, que nunca, como en aquella ocasión, era agua bendita.
Después de quitarse la asquerosa pelambre, empezó a despiojarse
con una escrupulosidad de benedictino, escogiendo pequeñas porciones
de su cabeza y de su cuerpo. Al contacto del agua la mugre se
reblandecía, y Rodrigo embargado por un goce sensual, se tallaba
con los guijarros que tapizaban el fondo de la poza; ponía todo
su cuidado en cada una de aquellas operaciones; cortóse las uñas,
se rebajó los duros callos que el trabajo le había formado en
las palmas de las manos y así pasó el resto de la tarde. Aunque
la charca era poco profunda se tendía de espaldas y nadaba suavemente,
contemplando el cielo. Cuando los rayos del sol comenzaron a
rozar los picos de los cerros y estos proyectaron sus sombras
sobre las cañadas, todavía estaba él en aquel sitio paradisíaco,
olvidado de todas las realidades y pensando solo en Verónica.
Durmió esa noche allí, bajo la sombra de las encinas y a la madrugada,
a la luz de las estrellas, echó a andar. Lo sorprendió el crepúsculo,
fatigado, sin haber visto a un solo ser humano; encontró al fin
una cueva fresca y bien cubierta, cuyo acceso se ocultaba en
los breñales; lugar ideal para descansar.

Al día siguiente, a media mañana emprendió la marcha, iba hambriento,
mas en la tarde vio un hombre con un burro y decidió abordarlo.
Era un tlacualero que llevaba alimentos a la carretera; venía
de Parián, un caserío sobre la vía del ferrocarril: ---Como a
cuatro leguas ---decía el indio. Este le dio unas tortillas,
le dijo que el trabajo en la carretera seguía su curso y que
había noticias de que los invasores estaban nuevamente en el
campo número siete. Cuando supo que Don Rodrigo era un evadido
se ofreció a devolverse y a llevarlo a Parián, donde podría ocultarse.
Caminaron despacio, con el propósito deliberado de entrar de
noche.

Parián es un caserío que sirve de estación ferroviaria; tiene
unas cuantas casas y un río de temporal que lo recorre desde
un extremo al otro. El guía se adelantó y volvió como una hora
después, con dos hombres; Don Rodrigo temió ser víctima de una
celada, pero no existía alternativa. Lo acompañaron a una choza
y allí le explicaron que iba a quedarse con ellos hasta que pudiese
proseguir su marcha. Hiciéronlo cambiar su traje de dril por
un calzón blanco y una camisa de manta; diéronle unos huaraches
en lugar de los zapatones de preso y le arreglaron para dormir
un rincón con un sarape raído. Eran gente muy pobre, pero buena;
los niños lo llamaban «siñor», las mujeres patroncito y los hombres
jefe. Lo halagaban diciéndole que allí no corría peligro. Bebió
un café caliente de olla y se echó a dormir una noche más. Y
durmió otra y otra. Parecía que estaba en relativa seguridad;
había enviado ya un mensaje a Remigio y comenzaba a hacer planes
para su fuga definitiva. Ahora es una tarea triste relatar cómo
se desbarataron sus sueños cuando creía haberlos convertido en
una cosa real que tenía cogida con la mano.

En la tarde de su último día allí, y a la hora de la siesta,
se suscitó un escándalo en aquel caserío. Un soldado alemán entró
a la tenducha donde se expendían víveres; varios hombres charlaban
bebiendo unas sodas. Se entabló una discusión por cuestión de
precio, pero como el soldado casi no hablaba español no pudo
ponerse de acuerdo con el dueño y cuando los ánimos se exaltaron
y las voces se alzaron, empuñó su rifle y con la culata le dio
un recio golpe en las costillas. Pretendió entonces alejarse,
pero los que allí estaban bebiendo se echaron sobre él, lo desarmaron
y lo golpearon. Entonces el nazi comenzó a gritar pidiendo ayuda
y se presentaron sus compañeros de guarnición ---apenas unos
seis--- a las órdenes de un sargento.

Más tarde los boches hicieron salir de sus casas a todos los
vecinos. Se iba a aplicar un castigo público al culpable para
inspirar con ese ejemplo respeto hacia los representantes de
la raza predestinada.

Don Rodrigo que estaba ajeno a ese tumulto fue obligado también
a salir y alinearse, junto con los otros. Desgraciadamente el
sargento se fijó en él; le llamaron la atención sus mejillas
rasuradas y blancas, que contrastaban con su frente requemada
por el sol y sus pómulos renegridos. Entonces lo hizo quitarse
la camisa y allí encontró, sobre las espaldas, las cicatrices
de los azotes, cicatrices que el tiempo no había podido borrar.
En seguida examinó sus manos; su anular conservaba aún la huella
de una sortija que usó muchos años, y que le habían robado durante
los días que pasó en la mazmorra de Churubusco. El sargento habló
en alemán con sus secuaces; ataron a Rodrigo por las muñecas
y lo condujeron a la estación; al verlo alejarse las mujeres
del jacal lloraban con grandes lamentos mientras los hombres
permanecían silenciosos. Esa misma tarde en el primer tren de
carga, fue conducido hacia el sur y por la noche lo entregaron
al nuevo jefe del campo número siete en donde lo identificaron
sin dificultad.

De momento parecía que la vida proseguía su viejo ritmo. En las
galeras se cuchicheaba; Almanza no había regresado y a la postre
se supo que pudo reunirse con Héctor Berrueta y sus saboteadores.
Por espacio de dos semanas continuaron llegando al campo nuevos
prófugos. Las milicias nazis los fueron acorralando y a la postre
más de mil quinientos habían regresado a ese lugar maldito. El
resto logró fugarse o murió en los primeros instantes de la evasión.
Después, durante varios días, cesaron las aprehensiones y parecía
que la situación habíase estabilizado. Pero una mañana se suspendió
el trabajo y en lugar de sacar a los reclusos a la tarea se les
alineó en una fila formando un inmenso cuadrilátero. El nuevo
jefe del campo, con actitudes teatrales, les hizo un discurso
que un oficial iba traduciendo. Les habló de todas las inepcias
que forman el credo nazi: de la raza, de la sangre, de Hitler
y del nuevo orden, para terminar diciendo que se había resuelto
quintarlos a todos y castigar ejemplarmente a los sorteados.
Los mandó numerarse y a los que obtuvieron esa lotería fatal,
se les ordenó diesen un paso al frente. Don Rodrigo fue el noventa
y cinco.

Como trescientos presos fueron conducidos al fondo del campo
y alineados contra las alambradas. Inmediatamente después dos
ametralladoras empezaron a vomitar proyectiles y los cuerpos
fueron doblándose, exánimes; muchos quedaron colgados de las
púas. Rodrigo marchó al suplicio tranquilo. Iba pensando en Verónica,
en Remigio, en Patricio, en Ana. Los estrechó a todos con su
pensamiento y los besó con efusión. Ellos habían sido su familia,
pedazos de su carne, la única realidad por la cual valía la pena
vivir. Al llegar al lugar en donde iba a caer para siempre pensaba
en los alemanes, sin odio, pero pidiéndole a Dios, a nombre de
todos los padres y todos los hijos y todas las esposas del mundo,
que el espíritu nazi se borrase de la tierra para toda la eternidad.

Fue de los primeros que cayó; una bala le atravesó el corazón
y murió sin sufrir.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIII

++Después++ de haber referido tantos sucesos dolorosos, tengo
que cambiar el diapasón atormentado de mi historia. La emoción
con la cual muchas gentes evocan tiempos idos y relatan los sufrimientos
de los días de la invasión, me ha contagiado muchas veces. En
la estancia de la casa familiar todavía cree uno oír el ronrón
del gato o la voz de Dorotea que llama para la cena. Va uno al
atrio de la Parroquia y sobre el costado de la torre del reloj
se contemplan los impactos de las balas que segaron la vida de
muchos vecinos y una de las hermanas de Don Estanislao Gutiérrez
que aún vive y está ciega, llora lágrimas que brotan en abundancia
de sus ojos apagados cada vez que recuerda la aprehensión del
viejo. Pero no quiero adelantar los sucesos.

Interrumpiendo el hilo lógico de la narración, voy a hablar ahora
de antecedentes previos a esos días angustiosos. Mencionaré a
personajes que ya han aparecido fugazmente en mi historia y otros
vendrán a danzar por primera vez en este tablado dramático. Me
acontece que los acontecimientos de entonces fueron tan complejos
y se presentan tan imbricados en mi mente, que no acierto a presentarlos
con su natural cronología.

Por eso voy a volver a tiempos anteriores a la invasión, cuando
los nazis, aprovechando las malas pasiones de muchos y utilizando
como instrumento la corrupción que era entonces el fondo de nuestra
vida política, pusieron en juego los mismos ardides que en Europa
les sirvieron para minar a los pueblos sojuzgados más tarde.

Ya los Estados Unidos tenían de esto una amarga experiencia,
pues habían dejado prosperar grupos que trabajaban al servicio
de los nazis. Un tal Kuhn, que era el pequeño führer del _German
American Bund_, había pronunciado una frase que los gobiernos
de América, en su ceguera, se empeñaron en olvidar: «Los papeles
de ciudadanía no nos han convertido en otro tipo de hombres.
Seguimos siendo lo que somos: alemanes en América».

El hecho es que el país se llenó de agentes nazis que, poco a
poco, desde el encumbramiento de Hitler, fueron instalándose
en él con distintos pretextos. Obraban sin darse a conocer de
sus representantes diplomáticos; en cambio traían cartas e instrucciones
para la colonia alemana y disponiendo de fondos en abundancia
les era fácil conectarse con especímenes criollos cuya cooperación
les ayudó para tejer una trama de intrigas.

Por una serie de circunstancias casuales el ingeniero Héctor
Berrueta, del cual ya he hablado en mi relato repetidas ocasiones,
se encontró cogido en sus redes. Pero los nazis se equivocaron
al aquilatarlo y nunca deploraron suficientemente su error. Quiero
referirme ahora a él con mayor detalle revelando algunos hechos,
aunque esto me acarreará sus censuras.

Debe tener a la fecha como unos cincuenta y ocho años; nació
con los albores del siglo. Es un tipo bajo de cuerpo, prieto
y regordete, pero en su cuello se enchufa una cabeza huesosa
y en esta una nariz larga sobre la que cabalgan los espejuelos.
Tiene unos ojillos vivarachos y unas mechas, que peina para atrás,
donde la calvicie ha comenzado a hacer sus estragos. Su aspecto
y reputación son de hombre inteligente, sagaz y agudo, y lo califico
con estos tres adjetivos porque cada uno corresponde a un matiz
diverso de su mente. Tiene un buen sentido práctico del que hace
alarde; llama a las cosas por sus nombres y se enfrenta a los
hechos sin vacilar.

Trabajo a sus órdenes desde que terminé mis estudios, hace unos
tres años, y puedo asegurar que esa reputación está bien ganada.
Mi juicio es insospechable porque los subordinados nos inclinamos
casi siempre a considerar a nuestros superiores como unos asnos.

Pero lo extraordinario es que este hombre que se antoja un buen
cura de pueblo dado a la molicie, sea capaz de una actividad
extraordinaria, de rápidas decisiones y aún ahora, que ya está
viejo y apoltronado, de esfuerzos físicos instantáneos.

Los acontecimientos de febrero de 1946 lo sorprendieron trabajando
como ingeniero de un banco; además en los ferrocarriles del gobierno
tenía un puesto que le quitaba apenas dos o tres horas diarias
y por fin resolvía consultas en las especialidades de su profesión.
Con esto vivía sin lujos, pero con facilidad, pues era viudo
y su único hijo murió apenas de cinco años. Cuando le preguntaban
por qué no reincidía en el matrimonio echaba la cosa a chacota
diciendo que se sentía como un pescado que logra zafarse del
anzuelo; también solía decir que no había quedado convidado a
reingresar a la hermandad de los maridos porque se había descubierto
una decidida vocación a la misoginia. Pero la verdad era otra:
vivió tres años profundamente enamorado de su mujer y quiso ser
fiel a su memoria; la muerte de su chiquillo, acaecida después,
no hizo sino fortificar su propósito. Era un motivo netamente
sentimental que él no confesaba por pudor.

En México trabó una amistad bastante estrecha con Alonso Quintanar,
lo ayudó a mejorar las primitivas máquinas de su pequeña industria
y a organizar esta como un vago remedo de una producción en masa.
Alonso llegó a tener un gran miramiento por su idoneidad y lo
incitaba a enfrascarse en actividades de más envergadura; pero
Berrueta le replicaba que no tenía ambiciones, que odiaba el
dinero y que prefería vivir como un escapista. Con este término,
que nunca llegó a incorporarse a los diccionarios, se adjetivaba
a aquellos que huían de la realidad por causas a veces encontradas;
esta evasión la realizaba Berrueta leyendo y estudiando; con
sus magros ahorros adquirió poco a poco una buena biblioteca
técnica, cuya lectura le permitía criticar despiadadamente el
organismo social anterior a la guerra e ir construyendo en su
mente una estructura que en aquella época no pasaba de ser una
utopía. Las gentes que lo trataban con confianza y con las cuales
hablaba de estas cosas le decían que su concepción era un mazacote
sin homogeneidad, pues en él mezclaba nociones capitalistas,
comunistas y hasta nazistas. El respondía que era anti-capitalista,
anti-comunista y anti-nazista, porque era un hombre libre y esos
tres sistemas atacaban la libertad; pero que nadie que fuese
inteligente podía negar que desde el punto de vista económico
estas tiranías habían aportado grandes enseñanzas. El tiempo
le ha dado en mucho la razón.

Nació en Pátzcuaro, el mismo pueblo de la familia de Serafín
Rafael Pérez, pero hubo entre ambos una gran diferencia de edad.
Cuando Héctor, en lugar de ir a la escuela hacía sus escapatorias
a la isla de Janitzio y pasaba las mañanas asoleadas con los
pescadores, Serafín Pérez vociferaba en México como revolucionario
de última hora.

Se encontraron por los años de treinta y cinco. Berrueta estaba
viudo desde hacía algún tiempo; Serafín Pérez se hacía llamar
ya el Doctor Siddartha y hablaba de Mussolini y de la siddarthita;
invitaba a Berrueta a su casa donde este lo encontraba encerrado
en un cuarto al cual no entraba ni el más tenue resplandor de
luz solar frente a una de sus figuras modeladas con la sustancia
plástica de su invención. La repasaba con su buril de emanaciones
catódicas bajo la lívida luminosidad de una gran lámpara de rayos
ultravioletas; Serafín trabajaba encapuchado hasta la coronilla,
las manos cubiertas con unos guantes espesos y con unos anteojos
opacos a esas ondas. El verlo así, entregado a su inspiración
creadora de escultor, era un espectáculo para los turistas, quienes
en manadas numerosas se presentaban con los guías y penetraban
curiosos y sobrecogidos hasta aquel misterioso laboratorio del
arte.

Berrueta cultivó relaciones con Serafín Pérez. Cuando quiso hacer
reminiscencias de Pátzcuaro, de su jardín romántico y de las
tonalidades verdes del lago, Serafín lo atajó:

---Héctor, quiero decirte una cosa: te estimo, no porque hayamos
nacido en el mismo lugar, sino por razones muy diferentes. No
me halaga que me hables de mi casa, ni de los pericos que sabían
decir «Ave María Purísima»; esas añoranzas estúpidas no me conmueven.
Además, me molesta que me recuerdes a mi madre, que quizá fuese
una excelente persona, como tú aseguras, pero a la cual nada
le debo. Y me molesta todavía más que me llames Serafín; llámame
Doctor Siddartha que es como me conoce todo el mundo.

Héctor Berrueta se le quedó viendo con sus ojillos penetrantes,
que en aquella época despedían un fulgor juvenil y le respondió:

---Veo que me pones un dilema. Está bien, te llamaré Doctor Siddartha,
aunque eso me parezca un poco ridículo, pues no puedo olvidar
que en la pila bautismal te llamaron Serafín Rafael; pero te
diré por última vez que tu madre era la mejor de las mujeres.
Ahora, doblemos la hoja.

Allá por los años de 1937 y 1938 el Doctor Siddartha lo puso en
contacto con el ingeniero Esteban Cabral; este no reparaba mucho
en su nuevo conocido, insignificante y oscuro, que permanecía
callado durante las discusiones sobre filosofía o estética prestando
solo un oído atento, y que si se pretendía que participase en
la conversación confesaba su ignorancia. Berrueta en cambio se
aplicó a conocer íntimamente al hombre: su inteligencia Caótica,
su espíritu desordenado, su vanidad y su ambición. Estas características
se oponían precisamente a las de Berrueta, quien se había sumido
voluntariamente en la oscuridad y cuya mente era esencialmente
constructiva.

Trabó conocimiento también con otros muchos individuos. El Doctor
Siddartha lo llevaba a tomar café con Atanasio Cifuentes y Guillermo
Abrego en un local de las calles del 5 de mayo donde formaban
una peña estrafalaria.

Cuando Berrueta se juntaba a comer con Alonso Quintanar e iban
a un restorán cerca del mercado de la Lagunilla a engullir un
cabrito en su sangre al estilo del Norte, el segundo le preguntaba
a aquel cómo podía perder su tiempo sosteniendo relaciones ficticias
con los ejemplares de esa fauna.

---Pues le diré ---respondió Héctor--- precisamente porque mato
así el tiempo; o lo pierdo, que es lo mismo. Pero me divierte
catalogar a esos ejemplares, como usted los llama; si en alguna
ocasión quiere usted que lo aburra le referiré muchas idiosincrasias
que quizá expliquen algunos fenómenos curiosos del México actual.

Esto lo decía Berrueta porque notaba anomalías extrañas en aquellas
gentes que no hacían nada y que trataban de arreglar el mundo,
para su provecho, en las mesas de los cafés. Le llamaba hondamente
la atención que el Doctor Siddartha tuviera un contacto estrecho
con Martín Cordero que era un comunista del tipo intelectual
y disfrutaba de un buen puesto en la Secretaría del Trabajo.
Llegó a la conclusión de que los fascistoides y los comunistoides
tenían más puntos de contacto que los que a primera vista podría
suponerse.

Algunos meses después de que estalló el conflicto en Europa hizo
Berrueta relaciones con Von Hütten, quien dio una cena a un
grupo de mexicanos. Fue invitado a ella por Serafín y encontró
en la casa del teutón a muchas gentes que había venido viendo
con cierta frecuencia durante los últimos meses. No faltaba allí
el ingeniero Esteban Cabral ni un abogado que ese día fue presentado
al anfitrión y que con el tiempo iba a representar un papel muy
notorio en los acontecimientos por venir: me refiero a Donaciano
Icaza a quien ya he mencionado. Por cierto que a la postre murió
peleando contra el invasor. Estaban otros alemanes de la colonia
en México, hombres que amasaron sus fortunas en el país, pero
Berrueta no los conocía sino de oídas: eran Gortz, Bechler, Schubert
y algunos otros de menos fuste. Von Hütten estuvo extraordinariamente
amable con Berrueta y lo presentó a sus compatriotas. El Doctor
Siddartha hizo de él un elogio campanudo, que ruborizó a Berrueta,
quien era y ha sido siempre un hombre modesto.

Héctor Berrueta replicó. ---No veo razón para que me abochornen
ustedes. En cuanto a ti, Doctor Siddartha ---y subrayó este título
con un tono procaz--- siento no poder decirte lo que te mereces.

---¡Hombre! ¡Hombre! ---repuso este último--- no seas insociable;
te hacemos estos cumplidos porque son justos.

Von Hütten tomó entonces a su cargo la charla con Berrueta y
se enfrascó con él en muchos tópicos que giraban en torno de
su profesión. Berrueta contestaba con palabras parcas.

Se sentía mareado en aquella atmósfera. A su alrededor todo el
mundo hablaba; se había bebido copiosamente y frente a los licores
y el coñac, que se tomaban con el café, las lenguas estaban
desatadas. Escuchaba fragmentos de frases entusiastas acerca
de la guerra; los alemanes habían invadido en esos días los Países
Bajos y se daba por descontada la caída de París. A sus oídos
llegaban los epítetos que Cabral prodigaba a los franceses, llamándolos
degenerados, corrompidos, faltos de virilidad y homosexuales.

En otro rincón de aquella sala Siddartha hacía uno de sus discursos
contra los judíos; lo rodeaba una media docena de hombres de
letras que vivían de puestos públicos y de colaboraciones en
periódicos de segunda fila. Todos libaban abundantemente y vociferaban
con los ojos brillantes y las copas en alto.

Sentado frente a una pequeña mesa, junto con Fischer ---un alemán
emparentado con Bechler--- estaba Guilebaldo Ordóñez quien había
estudiado en Alemania y trabajaba para la Administración de Petróleos.
Ambos bebían cerveza en tarros enormes; varios se les fueron
uniendo y comenzaron a cantar en alemán y en español. Cuando
el entusiasmo era general, Fischer se subió a la mesa y lanzó
un ¡Heil Hitler! que corearon las veinticinco o treinta personas
congregadas para ese convite.

Entonces el licenciado Donaciano Icaza que estaba un poco bebido
se levantó con la copa de coñac en la mano y propuso un brindis
a la memoria de Madero; todos lo secundaron. Pero el doctor Alcántara,
que fue cónsul en Múnich por los años de diecinueve, pidió a
su vez que se rindiera un homenaje a Venustiano Carranza ---ese
varón preclaro ---dijo--- enemigo de los yanquis y amigo del
pueblo alemán---. Todos contestaron con grandes aclamaciones.

Berrueta se sentía mal; las bebidas espumosas le producían dolor
de cabeza y aprovechando una coyuntura se escabulló sin despedirse.
Era como la una de la mañana, se dirigió a pie hasta su casa
y durmió con un sueño agitado.

Refirió más tarde a Alonso Quintanar todo lo acaecido.

---La verdad es, Alonso ---terminó diciendo--- que estas cosas
tienen un tinte misterioso que no me agrada. La gente que va,
ese contubernio de mexicanos que aparentemente profesan ideas
encontradas, pero que tienen un denominador común de ambiciones
vulgares; esos alemanes que rodeados de nuestros paisanos parecen
domadores de circo en medio de una _ménagerie_, todo, Alonso,
forma un conjunto repulsivo. Quiero que me dé su opinión franca.

---Mire usted, Berrueta ---contestó Quintanar--- se la voy a
dar sin reticencias. Juzgo que está usted en un foco de conspiradores.
En estos tiempos de guerra el sabotaje es una de las actividades
más importantes y para ella es preciso contar con buenos ingenieros.
Usted es un buen ingeniero y eso es todo; quieren utilizar su
mente ordenada para organizar una maquinaria que se preste a
sus fines.

---Está bien ---Alonso---. Le confieso que es precisamente lo
que he pensado, pero ¿en qué datos concretos funda su hipótesis?

---No se necesita ser adivino, amigo Berrueta. Los nazis vienen
realizando este trabajo en todas partes y me extraña que Hitler
haya retardado tanto su acción en México; en Nueva York fundó
desde que llegó al poder una sociedad que se llamaba Amigos de
la Nueva Alemania. Más recientemente las organizaciones de carácter
y denominación alemanes se han propagado como mala yerba.

---Es cierto ---replicó Berrueta--- pero yo le aseguro que no
hay ningún partido o liga aparente que una a todas esas gentes.

---Claro que no ---dijo Quintanar---. Han aprovechado las lecciones
recibidas en los Estados Unidos en donde se hicieron sospechosos
desde un principio por el matiz tan ostensiblemente alemán de
su propaganda. Aquí en México quieren prestar a todo un aspecto
netamente nacional. Hay que darle gracias a Dios por su mal olfato.
Se les ocurrió para sus planes elegir a Cedillo que era un apóstol
agrarista con haciendas, venal y lúbrico, además de ignorante.
Tal vez conozca usted la historia del Barón Von Merck; ya se
la referiré en alguna ocasión.

---Bueno, Alonso; pero vamos al grano ---repuso entonces Berrueta---.
Ya tengo su opinión. Ahora quiero un consejo ¿qué haría usted
en mi lugar?

---Voy a decírselo. Entre usted más a fondo en este asunto y
procure penetrar los secretos de los que podría yo llamar sus
cómplices; eso es todo.

Había transcurrido el año de 1940 y los triunfos nazis hacían
concebir a los alemanes de México las más locas esperanzas. ¿Qué
podría parecer fantástico después de haber subyugado a Europa
entera? Es cierto que al iniciarse 1941 la Gran Bretaña aún no
había caído de rodillas ante Hitler; los londinenses soportaron
bombardeos espantosos y su ciudad mostraba grandes heridas que
se hacían patentes por enormes áreas cubiertas de escombros.
En cambio habían demostrado al mundo que la destrucción de las
cosas materiales es preferible al derrumbamiento de los bienes
del espíritu.

Entonces fue cuando una gran pandilla de saboteadores profesionales
se extendió por el país al amparo de la molicie gubernamental.
Todos estos datos han llegado a mis manos por conductos diversos;
pero los que se refieren a la lucha, cuando ya los nazis estaban
en México, los he recabado en gran parte del mismo Berrueta.

Al entrar los nazis ---me ha dicho--- fui a ver a Alonso. Le
dije que Von Hütten y sus amigos me habían iniciado en algunos
misterios y que me proponía trabajar por mi cuenta y en su perjuicio.
Invité a Alonso a que colaborara usando la maquinaria de su fábrica
y él aceptó sin la menor vacilación. Me dijo únicamente: ---es
una fortuna que no tenga usted hijos; en cuanto a mí los únicos
que me ha mandado el cielo son los muchachos de mi fábrica y
ya están grandecitos. Pero tengo la corazonada de que no saldremos
vivos de la aventura.

---Sin embargo ---concluía Berrueta--- ya lo ves. Ninguno de
los dos sufrimos ni un solo rasguño y si no fuese por un piquete
de piojo aquí tendríamos quizás a Alonso.

Era difícil sacar más de Héctor Berrueta en una sola vez. Se
callaba, se ponía a trabajar y parecía que el pasado se esfumaba
al mismo tiempo que su interlocutor. Pero volví a la carga repetidas
ocasiones.

---Sabía muy bien de quiénes cuidarme; ---me dice--- había convivido
con ellos y los conocía de memoria; ellos, en cambio, me consideraban
como un semi-sabio chiflado que escribía sobre matemáticas y
experimentaba con las ondas ultracortas. Al triunfo nazi dejaron
de frecuentarme y pasé a la categoría de utensilio inservible.
Estaban engreídos. Cabral había vuelto a ser ministro y Donaciano
Icaza fue ministro al fin…

---¿Y Siddartha? ---le interrumpí.

---¿Siddartha? Se dedicó a lucrar y valido de su facilidad para
ver a Cabañas hacía el papel de alcahuete de grandes negocios.
Yo me consolaba pensando que como estaba viejo no podría hacer
daño mucho tiempo.

En otra ocasión en que habíamos trabajado varias horas volvió
Berrueta a recaer en el mismo tema y entonces fue un poco más
explícito:

---Alonso y yo nos preguntamos cómo iba a tomar el pueblo la
ocupación nazi ---me dijo---. En muy pocas semanas nos dimos
cuenta de que la tomaba muy mal. Comenzaron a hacerse chascarrillos,
y a Klee, el ayudante de Von Virchow, que era muy fatuo y usaba
monóculo, le hicieron muchas redondillas llamándolo «ojo de tecolote»;
las consonancias eran fáciles. Comprendimos que íbamos a tener
un campo propicio y nos decidimos a obrar.

Tenía bastantes datos en mi poder; no en vano conocí a Von Hütten
y a su comparsa. Cuando Cabañas fue nombrado Presidente y para
no despertar sospechas hice una visita a ese boche y le manifesté
que continuaba a sus órdenes. Pero estaba loco con el triunfo
y casi ni me habló. Los otros me consideraban un ser miserable
y no volví a verlos.

Así me dejaron con las manos libres. Me quitaron mi empleo en
los ferrocarriles para dárselo a algún paniaguado, pero no hice
ninguna protesta. Pensé que así dispondría de más tiempo para
mi labor y me resolví a vivir parcamente de mis ahorros.

Me propuse formar un vasto plan; ---continuó Berrueta--- tenía
la idea de que era necesario hacer una lista de las actividades
de sabotaje. Pero antes quise ir explorando las posibilidades
concretas. Uno de los obreros de Alonso vivía en una vecindad
donde habitaba también un muchacho trabajador del campo civil
de aviación, el cual estaba controlado por los alemanes. De allí
salían transportes cargados de tropas hacia el Norte. Ya no recuerdo
cómo se llamaba aquel chico; tenía una mirada muy inteligente
y apenas unos veinte años. Debería haber conservado su nombre
en la memoria pues murió más tarde, como un héroe anónimo, luchando
al lado de las guerrillas.

Una de sus obligaciones era, antes de la ocupación, llenar los
tanques de los aeroplanos que partían y los invasores lo obligaban
a desempeñar la misma tarea. Me convencí casi después de conocerlo
de que haría cualquier cosa contra ellos; se le saltaban las
lágrimas al referirme el trato que la soldadesca teutona daba
a los mexicanos que habían permanecido en sus empleos, que eran
casi todos, porque con halagos o amenazas se les había indicado
que no podían abandonarlos. Yo había inventado un pequeño juguete
que decidí ensayar. Era una especie de canica hueca… Bueno, Fernando,
no voy a aburrirte dándote detalles; ahora nos parece absurdo
pensar que alguien pueda ingeniarse para producir mecanismos
de destrucción. Este muchacho arrojó al tanque de uno de los
aviones aquella esferita cristalina cuyo diámetro no era mayor
que una moneda de cinco centavos. El aparato despegó llevando
cuarenta soldados que iban a Tampico y nunca se supo lo que pasó
con él. Mi amigo vino a verme al siguiente día solicitando más
bolitas; además trajo consigo a otros dos trabajadores resueltos
a jugarse la piel en la aventura.

Les hice entonces un pequeño discurso patriótico y le entregué
a cada uno su arma destructiva. A los dos días tres aparatos
que iban a Guatemala cayeron por el Istmo; los despojos de uno
de ellos fueron localizados, pero no se logró dar con la causa
del desastre. Entonces no tres, sino diez muchachos mexicanos,
acudieron a verme; comprendí que todo era cuestión de dar por
descontado que la vida iba a sacrificarse y con ese grupo formé
el pie veterano de mis saboteadores.

Me aconteció una cosa muy curiosa ---siguió diciéndome Héctor---.
Cuando me hice plenamente a la idea de que iba a morir en la
empresa y de que tarde o temprano me pescarían, extremé las precauciones.
Y es que llegamos a considerar un deber el sacrificar nuestra
existencia por el mayor número de vidas nazis. Observé cómo respondía
la gente y cómo para cada misión peligrosa había cientos de voluntarios.
Entonces entró firmemente en mi ánimo la convicción de que México
se salvaría.

Continuamos con el sabotaje de los aeroplanos hasta que un obrero
fue sorprendido en la maniobra; se le fusiló sin haber logrado
arrancarle ninguna confesión.

Berrueta me ha referido que hizo entonces un verdadero estudio
de los medios de sabotaje. Se aplicó a fabricar unos pequeños
mecanismos incendiarios que hacía llegar a todos los gremios
de estibadores y trenes enteros fueron consumidos por el fuego.

---Estos aparatitos eran una simpleza y conocidos desde la guerra
de 14 ---me dice Berrueta---. Fueron entonces puestos en uso
en los Estados Unidos por un químico alemán y produjeron grandes
estragos en los embarques que se hacían a Rusia. Los modifiqué
ligeramente y en la manufactura de Quintanar los producíamos
por centenares. En apariencia eran partes de los juguetes que
allí se fabricaban y en las visitas de inspección que recibimos
nunca encontraron nada anormal. Alonso obtuvo un gran pedido
de chucherías para las Juventudes Hitlerianas de México ---aquel
gigantesco _bluff_ de los boches que tenía por objeto atraerse
a los mexicanos con hijos y envenenar a la niñez--- y él mismo
invitaba a los nazis a que vinieran a cerciorarse del progreso
del trabajo. El hecho es que causamos daños enormes, pues cuando
la invasión cada mexicano en la zona ocupada era un saboteador
potencial. Podía uno dirigirse a cualquiera, lo mismo a una mujer
que pedía limosna en la calle, que a un tipo maleante o a un
señor respetable cargado de familia que vendía géneros detrás
de un mostrador; todos estaban dispuestos a un acto de sabotaje
contra los invasores.

Héctor Berrueta comenzó a convertirse en el centro de una colosal
estructura subterránea. Su vida rutinaria a la que estaba tan
apegado se desquició; dormía a ratos, aunque siempre procuraba
completar su ración diaria de ocho horas y comía a salto de
mata, en medio de sus papeles o en algún tabuco mugroso donde
conferenciaba con sus asociados. Seguía haciendo peritajes para
su Banco y aparentaba trabajar como agente de Alonso Quintanar.

Aquello fue extendiéndose como una mancha de aceite. El hacer
daño a los invasores se convirtió en un deber. Lo admirable de
esa época que reivindicó a México ante el mundo y lavó sus culpas,
fue que la vida humana se inmolaba casi con indiferencia en la
lucha contra Hitler. Si en una familia un hermano era sorprendido
saboteando un cargamento de semillas e inutilizándolas para la
germinación, su fusilamiento inmediato parecía enardecer los
ánimos de los hermanos menores y mayores, quienes, con los rostros
sombríos seguían en la tarea de arrasamiento.

Sería interminable dar cuenta de cómo los nazis encontraban en
todas partes la obra de los saboteadores. En un mismo día, en
la ciudad de México, descarrilaba un convoy con reses destinadas
a las tropas de ocupación; se incendiaban las existencias de
mantas para ropa del ejército; morían cien boches en un cuartel,
aparentemente de triquinosis; hacía explosión un depósito de
combustible de cincuenta mil galones y volaba hecho pedazos un
horno de fundición.

El mérito de Berrueta fue hacer un estudio del campo entero donde
el sabotaje podía ejercitarse e inculcar en la mente de todos
que un país saboteado no podía vivir. Fue entonces cuando adquirió
una pequeña imprenta y comenzó a distribuir sus volantes, pero
como era insuficiente pronto se ofrecieron espontáneamente docenas
de impresores. Algunos tenían sus prensas en el último patio
de una vieja casa, donde las mujeres lavan. De allí salían millares
de hojitas con instructivos precisos escritos por Berrueta.

Así fue como hicieron conocer los métodos para entorpecer el
trabajo en las fábricas. Los obreros retardaban sus movimientos;
paraban sus máquinas con los pretextos más fútiles, tomaban un
tiempo inmoderado para comer o se lesionaban ligeramente para
pasar a la enfermería. Estas maniobras eran el resultado de una
conspiración callada y todos los mexicanos ponían su mayor empeño
en realizarlas.

Los cagatintas del corrompido gobierno del _Quisling_ Cabañas llevaban
a cabo también una implacable labor de sabotaje. Se veían obligados
a trabajar para no morirse de hambre, pero odiaban a los invasores
y estaban en pugna con la administración a la cual servían. Las
instrucciones de Berrueta llegaron también a este campo y la
tramitación que era ya lenta antes de la guerra, se tornó aún
más torpe y flemática cuando se trataba de perjudicar la marcha
de los negocios públicos; en cambio los empleados de las tesorerías,
que eran unas fieras para el cobro de impuestos en épocas anteriores,
protegían entonces con una lenidad paternal a los morosos.

---Lo que nunca logramos ---me dice Berrueta--- fue fabricar
las sustancias para sabotear las siembras. Venían en ampolletas
selladas que era necesario romper en los canales de riego. Jamás
supe si obraban química o bacteriológicamente; pero las plantas
regadas con esas soluciones infinitesimales comenzaban a decaer
y eran una ruina en menos de una semana. Las ampolletas nos llegaban
de los Estados Unidos por aeroplanos que las soltaban en paracaídas
sobre regiones yermas; desde allí las traían los indios a los
centros poblados. Es cierto que el país se moría de hambre por
falta de cosechas, pero esto acontecía de todos modos, pues lo
poco que se recogía era destinado a los invasores y para remitirlo
a Europa donde la miseria se había hecho endémica.

Berrueta me ha referido también que en las extensiones interminables
recorridas por las cintas paralelas de los rieles era frecuente
que un indio, que llevaba a su mujer en un burro y caminaba a
pie tras de la bestia, se detuviera junto a la vía y después
de asegurarse de que en toda la soledad del gran círculo de su
horizonte no había un alma viviente, sacase una barreta con la
que botaba los clavos y aflojaba dos o tres tramos de riel. También
sucedía que en esos desiertos los chiquillos trepaban por los
postes y cortaban los alambres del telégrafo o el teléfono.

Se efectuaban, como era de esperarse, voladuras de puentes y
largos tramos de carriles. Pero eso requería una técnica más
complicada; había necesidad de colocar los cartuchos de dinamita
en lugares adecuados y hacerlos explotar por medio de conexiones
eléctricas desde distancias que no podían ser muy largas. De
todos los grupos de saboteadores los que dieron un más voluminoso
martirologio fueron precisamente los dinamitadores de caminos
de hierro.

Posteriormente la colaboración de un amigo perfeccionó el uso
de las bombas explosivas, principalmente para fines de sabotaje.
Esto me lo cuenta Héctor Berrueta.

---Nos habíamos conocido en la escuela ---me dice--- pero cuando
yo comenzaba mi carrera él estudiaba el último año. En el transcurso
del tiempo llegó a ser una autoridad en radio. Había hecho una
pequeña fortuna y vivía con ella confortablemente. Yo no lo veía
con frecuencia porque me molestaba el tono negativo de su mentalidad;
era demoledor en sus pensamientos, en sus juicios, en sus críticas.
Siempre le decía que inventara algo, que creara algo, pues de
no hacerlo lo iba a sorprender la muerte murmurando chascarrillos
contra el gobierno. Era un derechista por 1946; el tipo perfecto
del mexicano que odiaba a los yanquis y lo esperaba todo de los
nazis.

Por eso fue grande mi sorpresa cuando unos cuantos meses después
de la ocupación lo encontré y me dijo, en tono de confidencia,
que sus dos muchachos estaban con las guerrillas. Esto lo charlábamos
en la calle; noté la conmoción en su voz y lo arrastré suavemente
a mi casa. Allí el hombre se descargó del pesado fardo de su
desencanto y se desgañitó vomitando pestes contra los invasores;
contóme también que él había incitado a sus hijos a echarse a
la lucha.

No te haré largo este cuento ---me siguió diciendo Berrueta---
nos pusimos a hablar de radio y de allí saltamos a comentar el
sabotaje que cada día se intensificaba más. Le dije que si se
pudiera hacer estallar las bombas a distancia se habría dado
un gran paso para arrojar a los boches de México y le confesé
que yo había fracasado en mis ensayos. Tuvimos sobre el asunto
una larga discusión técnica.

Había yo olvidado esta charla cuando se me presentó como un mes
y medio después. Traía su invento en una maleta; era tan simple
que yo mismo me sorprendí de que no se me hubiese ocurrido algo
semejante. Es claro que hoy en que la transmisión inalámbrica
de energía ha alcanzado una gran perfección, aquello nos parece
un juego de niños; pero en esos momentos era revolucionario y
sobre todo de una gran utilidad. Consistía… bueno, eso no tiene
ahora interés.

Cuando hube comprobado su eficacia quise dar un gran golpe de
mano y logré colocar una bomba en uno de los cajones del escritorio
de Von Virchow. Pero el cómplice que debía hacerme las señales
desde el edificio de Relaciones perdió la cabeza, cometió un
pequeño error de tiempo y la explosión ocurrió cuando el jefe
nazi había salido de su despacho. Nunca más logramos agarrarlo.
Es cierto que pescamos a Klee y también, lo que fue para mí una
gran satisfacción, a Gortz; a este último tuve el placer de verlo
volar con todo y su automóvil cuando pasaba frente al Palacio
de Bellas Artes; pero el barón Von Hütten se me escapó también.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIV

++Guadalupe++ Otamendi se dirigía a la ciudad de México, en un
vagón de segunda. Salió esa mañana de Aguascalientes, pero en
la noche apenas habían llegado a Querétaro; y es que los trenes
caminaban tan mal que nunca se vio un desbarajuste semejante;
ni siquiera el de las épocas peores de la administración obrera.
Además, las escoltas se bajaban a menudo para revisar la vía
y prevenir los sabotajes. Como estos actos rara vez se cometían
contra trenes de pasajeros, los boches dieron en proteger sus
convoyes de bastimentos o pertrechos de guerra intercalando vagones;
todo esto contribuía al pésimo servicio y a la violación de los
itinerarios.

Guadalupe iba arrebujada en un chal negro; de su cuello pendía
una cadena de plata que traía ensartada una medalla de su Virgen
patrona. Bajo una falda parda asomaban unos zapatones toscos
y unas medias gruesas de algodón; con su rosario de cuentas negras
y unas antiparras de vidrios ligeramente ahumados, parecía una
señora beata de la clase media pobre; a su lado, junto a sus
pies, llevaba una canasta llena de trapos viejos y calcetines
a medio zurcir. Así pergeñada cualquiera le habría echado más
de cuarenta años o esa edad incierta de las mujeres maltratadas
por la vida que se amojaman en la soltería.

Iba meditando en su existencia de los últimos tiempos, desde
la invasión de los nazis. ¡Tantas esperanzas tronchadas! Su carrera,
que ella había tomado tan en serio, interrumpida por quién sabe
cuántos meses, o años; nadie podría decirlo. El hecho es que
ella había tenido que hacer a un lado sus libros de química,
sus probetas y sus experimentos. Los alemanes, es claro, trataron
de sobornar a la juventud que estudiaba. El oficial mayor de
ese doctor Belgrano, Ministro de Educación en el gobierno de
paja, llegó con un boche de gruesos anteojos, corbata de nudo
hecho y un alto cuello almidonado. Era el nuevo director; venía
de una universidad teutona y parecía un producto _ersatz_, con
sus pantalones angostos y su americana que dejaba ver los fondillos.

Es cierto que después comenzaron a llegar bultos con frascos
y sustancias para los laboratorios; pero los estudiantes fueron,
poco a poco, abandonando la facultad, Guadalupe entre los primeros.
Habló con Maclovio quien aplaudió con calor la decisión de su
hija y le dijo que aunque una carrera concluída es una gran cosa,
nada vale la pena de obtenerse a cambio de una indignidad. Ella
continuó leyendo sus libros en su casa, pero tenía un carácter
dinámico y no podía conformarse con un papel inactivo.

Además, su padre se afilió muy pronto a los grupos desafectos
al régimen y dedicó gran parte de su tiempo a la confección de
un pequeño periódico subterráneo que salía dos veces por semana,
el cual se llamaba «La Jeringa», en cuyo encabezado un tipo simbolizando
a México ponía una lavativa a un Hitler con las posaderas al
aire. Esta caricatura la hizo un dibujante a quien apodaban «el
chango»; había en ella una causticidad tan cruel y al mismo tiempo
tan realista, que su autor tuvo que abandonar la capital y fue
a reunirse con las guerrillas, pues los nazis no le perdonaban
el desacato. Cuando Remigio le preguntó por qué había salido
huyendo, contestóle que era cierto que le gustaba balancearse
en los árboles, pero colgado de la cola y no de una reata amarrada
al pescuezo.

Maclovio continuaba en su tenducho de libros usados. Ya no lo
visitaba ni el ingeniero Cabral, ni Abrego, ni Ordóñez, ni siquiera
el Doctor Siddartha. El primero era ministro y los demás medraban
a la sombra del nuevo gobierno. Maclovio despotricaba contra
los nazis delante de todo el que quería oírlo; Alonso Quintanar
le iba a la mano con frecuencia:

---¿Qué gana usted, Maclovio, con esos denuestos? Entréguese
a la publicación de su periódico; hágalo prosperar, consiga tinta
y papel. Desparrámelo por el territorio ocupado y recomiende
el sabotaje; eso dará qué pensar a nuestros enemigos. Pero hágalo
sin decir contra ellos una sola palabra; si lo agarran a usted
está bien, pero que no sea por imprudencias infantiles.

---Sí, ya sé, ya sé ---replicaba Maclovio--- usted quiere que
todos tengan su flema; al verlo cualquiera diría que es usted
un paniaguado de lo que ellos llaman el orden nuevo. Pero yo
no puedo disimular. Además que a lo mejor a usted lo pescan antes
que a mí. Así son las cosas.

---Es posible ---decía Alonso--- aunque esto no estaría dentro
de la lógica. Piense usted que mientras más se cuide mayor será
el daño que resentirán los boches; esa razón lo debería tornar
a usted más cauto.

Pero Maclovio no atendía argumentos. Hacía discursos subversivos
que no interrumpía ni siquiera ante clientes desconocidos. A
veces algún seudoaficionado a los libracos con aire de descuido
los sacaba de los anaqueles y los abría, mientras escuchaba con
el oído atento.

Guadalupe reconveníalo por sus imprudencias. Pero él era así
---se decía---. No sabía medir las consecuencias cuando algo
estaba contra la justicia. Entonces no lo detenía nada: ni el
sufrimiento, ni la miseria, ni el hambre. Porque pasaron hambre,
hambre de verdad cuando renunció a su puesto en el periódico
tan solo porque se empeñaron en suprimir de un artículo suyo
dos o tres líneas que parecían duras al gobierno. Entonces lo
veía llegar en las noches desalentado y sombrío. Ella, Guadalupe,
tenía diez años y Armando, cuando abrazaba a su padre, apenas
lograba apoyar su cabecita rizada a la altura de los muslos.
El los besaba y desvestía al chiquitín con el fin de acostarlo;
le daba un vaso de agua para engañar la pobre barriguita vacía
y le contaba un cuento. Ella se quedaba también dormida escuchando
las maravillosas aventuras de Ali-Babá; dormía como un tronco,
quizá por debilidad, y a la mañana siguiente se encontraba sola
con el hermanito que lloraba pidiéndole de comer. Maclovio llegaba
a veces con un buen pan o un trozo de pollo o un medio litro
de leche; pero eso no era ni bastante, ni regular. Así pasaron
como unas tres semanas hasta que un día se presentó Rodrigo Guerrero,
que venía de San Miguel y cayó a ver a Maclovio en su casa. Se
dio cuenta del conflicto y la sacó a ella y a Armando a un sitio
cercano donde se desayunaron como príncipes; de allí fuése a
buscar a Maclovio y armó con él una gresca imponente. El hecho
es que en aquella ocasión Rodrigo se llevó a los dos hermanos
a San Miguel y no regresaron a México hasta que Maclovio tenía
un modo normal de vivir. Pero este decía siempre que volvería
a mandar al diablo al director del diario y volvería a pasar
hambres, no con dos hijos sino con una docena, antes que tolerar
un atropello a la dignidad de su espíritu.

Guadalupe recordaba estos sucesos. El tren caminaba despacio
y hacía paradas muy frecuentes, muchas ocasiones en medio del
campo, en lugares donde no había ni un escape; el maquinista
se bajaba a engrasar un pistón o echaba agua a una chumacera.
Las ruedas de los vagones saltaban con irregularidad sobre los
carriles y el convoy entero producía una música discorde en la
que predominaban ruidos graves de metal. Cuando el tren se ponía
en marcha era con jalones bárbaros que producían un gran fracaso
de cadenas y fierros que entrechocaban; Guadalupe, entonces,
se iba hacia adelante o hacia atrás como si hubiese recibido
un empellón, pero después el jadeo de la máquina se hacía más
regular y ella proseguía ostensiblemente su rezo pasando entre
sus dedos las cuentas del rosario.

Sola, sin poder leer porque apenas un mechón de petróleo daba
una luz vacilante en un extremo del carruaje, sus pensamientos
seguían dando vueltas alrededor de los acontecimientos que habían
trastornado su vida.

Recordaba aquella casa de San Miguel que había sido para ella
y para Armando la representación del hogar perfecto. Verónica
quiso muchas veces incorporarlos a esa familia y en realidad
era para ellos una madre; una madre un poco lejana, porque para
llegar hasta donde estaba y sentir sus besos, era preciso caminar
un día enteró en un ferrocarril caprichoso que era como un ente
guiado por el azar, para quien el tiempo y las horas no tienen
sentido. Esa madre les escribía con frecuencia y hacía llegar
hasta ellos el calor de su corazón.

Pero Maclovio quiso retenerlos siempre a su lado, pues a pesar
de su rebeldía crónica que repercutía en la irregularidad de
sus hábitos, tenía un hondo instinto paternal.

En aquella casa había conocido a Remigio. ¿Desde cuándo? De siempre,
porque él le llevaba varios años y cuando ella lloriqueaba porque
alguien la despojaba de un juguete, allí estaba Remigio para
hacer justicia. Su recuerdo mezclábase a su conciencia más remota,
cuando recorría en silencio la sala en San Miguel Allende donde
Guadalupe sentíase pequeñita e indefensa. Fuése acercando a un
gran espejo y se paró ante él; allí estaba ella, con su carita
morena, dos moños de pelo detrás de las orejas y dos enormes
lazos que le salían por detrás, como los resplandores de un halo.
Se quedó absorta, ante aquella niña que le parecía una extraña.
De pronto vio en el espejo, como en otro mundo, la figura de
un muchacho fornido; junto a la morenita de ojos negros y vestido
rosa parecía un gigante. Era Remigio. Ella no había leído aún
---porque aún no leía nada--- «Alicia en el país de las maravillas»;
pero presentía que las cosas del otro lado de esa superficie
pulimentada y brillante formaban un mundo extraño donde se desarrollaba
una existencia prodigiosa.

Desde entonces Remigio creció junto a Guadalupe como algo íntimamente
unido a su vida. Lo veía en las vacaciones, bien en México o
cuando Maclovio iba a San Miguel. Ella fuése haciendo mujer y
él hombre; no podía precisar cuando se hicieron novios; lo que
sabía es que, desde que tuvo catorce años y los compañeros de
escuela la cortejaban, le era vedado coquetear, pues sentía ya
que su dueño era aquel muchacho que estudiaba medicina. Sabíalo
sin que nunca se hubiesen dicho una palabra, hasta que una tarde
en que estaban solos, Remigio quedósele viendo y exclamó: ---¡Caramba,
Guadalupe, me doy cuenta de que estás hecha una mujer!--- Y la
abrazó besándola furiosamente; ella lo golpeó y le dijo que no
era una chiquilla y que la respetase. Pero entonces él se sentó
a su lado y comenzó a decirle palabras dulces; le habló de sus
vidas, de que ambos perseguían las mismas cosas, de que sus destinos
eran los mismos. Le habló de su padre Maclovio y del suyo Rodrigo,
y de Verónica, que la llamaba siempre: hija mía.

En esa noche, arrastrada por el tren hacia un puerto preñado
de peligros, se veía a sí misma con el pecho sumido, las manos
ásperas, en su papel de solterona beata, y mientras maquinalmente
repetía los padrenuestros le llegaba el aroma de los árboles
y los recuerdos de aquella tarde revivían con tal vigor que los
valores se trastocaban. Sentía en las suyas las manos de Remigio
y el arrullo de su voz junto a la oreja y parecíale que lo irreal
era lo otro: el país desolado, las gentes que morían de hambre,
la humillación de la conquista y ese vagón de segunda clase lleno
de mugre, donde se aglomeraba una muchedumbre heterogénea y miserable.
¡Suerte que pudo conseguir un asiento junto a una ventanilla,
porque todos los pasillos estaban repletos y en la negrura de
la noche salían ruidos extraños de aquellas sombras que se apiñaban
en la oscuridad ¡como los mugidos y las rumias de un furgón
de ganado!

Su padre siguió su labor antinazi y la tienda de libros viejos
de la calle de Hidalgo se convirtió en un buzón de correspondencia
subrepticia. Generalmente alguien llegaba a vender un libro,
pronunciaba la contraseña y Maclovio pagaba por él cualquier
cantidad irrisoria. El libro se colocaba en un lugar determinado
y allí permanecía hasta que algún tipo de aspecto inofensivo
se lo llevaba previo un regateo que era ya maniobra convenida.
Quizás todo habría pasado sin tropiezos, pero Maclovio era de
una indiscreción desconcertante: injuriaba a Cabañas y a Von
Virchow y las noticias que publicaba en «La Jeringa» las gritaba
a voz en cuello desde su mostrador. Guadalupe le reconvenía también
con severidad y en la intimidad de la casa, cuando se quedaban
solos, le decía: ---¿Pero te empeñas en perderte? Lo que haces
es insensato. Yo odio tanto como tú a los boches; los odio porque
nací en México y soy cristiana y tengo en mucho la dignidad de
mi alma. Esto lo he aprendido de ti; pero no lo grito a los cuatro
vientos, pues no quiero sacrificarme vanamente.

Guadalupe recordaba aquel día terrible cuando un oficial nazi
y tres esbirros se presentaron en el tenducho de libros. Ella
no estaba, pero le contaron después que habían encontrado a su
padre leyendo las pruebas de su periódico, aunque no repararon
en ello. En realidad la Gestapo mexicana lo tenía fichado únicamente
como desafecto al régimen y católico recalcitrante enemigo de
Hitler. Ese fue el motivo de su aprehensión, pero no sospechaban
nada de sus actividades como periodista anti-nazi ni como buzón
de espías y saboteadores. Maclovio con tranquilidad dejó dentro
de un volumen las pruebas que corregía y se entregó sin resistencia;
salió a la calle echando la melena para atrás y vociferando en
voz alta. Lo mentaron en un camión cerrado y ya adentro el oficial
invasor acalló sus denuestos golpeándolo bárbaramente en la cabeza
con una cachiporra de hule macizo; llegaron a la vieja penitenciaría
convertida en prisión política y encerráronlo en una celda, clasificado
como rehén.

Armando andaba ya con Remigio bajo un nombre supuesto para no
comprometer a su padre y Guadalupe que estaba fuera regresó a
los tres días a México y se enteró del triste sucedido. Entonces
tomó a su cargo las riendas del pequeño negocio, dio instrucciones
al dependiente, que era para Maclovio de una ciega lealtad, de
que no se volviese a hablar de los nazis y continuó con el tenducho
abierto como buzón de saboteadores. Al mismo tiempo, ayudada
por Alonso Quintanar y Héctor Berrueta, encontró quien continuara
la publicación de «La Jeringa».

La disciplina de las prisiones de rehenes era muy severa; no
se les toleraba visitas y solo se comunicaban con el exterior
por cartas que llegaban a su destino por conductos subterráneos.
Así recibió Guadalupe varias. Recordaba que venían escritas en
un lenguaje virulento contra sus carceleros, pronosticando para
ellos la derrota y el exterminio del germanismo. Esas cartas
---¡cuán bien las rememoraba Guadalupe!--- llegaron sin tropiezo
a sus manos. Pero la situación de los boches era cada día peor
y comenzó el sacrificio de los rehenes como medio bárbaro para
reprimir los actos de sabotaje. La muerte de Gortz coincidió
con la destrucción de la fábrica de llantas «La Euzkadi», cuyos
dueños, unos españoles, habían sido llevados a un campo de concentración
por no plegarse a las exigencias de los nazis. El hecho es que
la tarde en que Gortz voló con su automóvil en forma misteriosa,
la fábrica de neumáticos quedó arrasada por las llamas, y al
día siguiente no era sino un inmenso solar de muros medio derruidos,
carbón y objetos achicharrados; el olor de la goma quemada se
percibió por muchos días en kilómetros a la redonda.

Entonces se publicó una orden para que los culpables fuesen entregados
y con ella una lista de veinte prisioneros que serían sacrificados
en caso de que no fuese acatada. En esa lista estaba Maclovio
Otamendi a quien fusilaron amordazado, pues con el rostro inconocible
por los golpes, con las ropas manchadas de sangre, los ojos casi
cerrados y la cabeza lisa como una bola de billar ---lo habían
rapado como castigo--- iba al paredón gritando mueras a Hitler
y vivas a México y a los países unidos.

Guadalupe habría querido olvidar aquellos sucesos; pero cada
vez que volvían a su memoria eran más vívidos y lancinantes.
Y ahora, que iba sola, sin más defensa que su valor, en aquel
vagón de tiempos de guerra ¿cómo podría despojarse de sus recuerdos?
No lloraba porque era de un temple indómito, lo había heredado
de su padre. Pero se sentía desamparada, sin más coraza que su
amor por Remigio, quien se oponía a esas actividades tan peligrosas
y hacía fuerza para que se afiliase a la enfermería de la Cruz
Roja. Pero ella tenía fe en su misión; creía que la muerte de
Maclovio, la muerte de Rodrigo, de tantos otros como ellos no
podían quedar impunes y tomó su puesto en la lucha, sin vacilar.

El sueño y el cansancio la vencieron al fin y dormitó varias
horas, arrebujada en su chal con la cabeza apoyada en el duro
respaldo del asiento; la despertaban a menudo las violentas sacudidas
del tren y el aire frío que entraba por la ventanilla, pues el
vidrio estaba roto. Cuando cogía el sueño ininterrumpido por
algunos minutos su mente se poblaba de imágenes inquietantes
con la sucesión desarticulada y absurda de los sueños. Despertaba
para volverse a sumir en su sopor.

De pronto escuchó gritos imperativos que la hicieron incorporarse
con los ojos bien abiertos. Sintió en su plexo solar la cuchillada
de la angustia; la misma de siempre que escuchaba frases en alemán:
---_absteigen_! _alles absteigen_!

Al mismo tiempo, tres o cuatro hombres de tropa hacían levantar
con la punta de la bota a los que se encontraban tendidos en
el pasillo central, mientras uno de ellos iba enfocando el haz
de una antorcha eléctrica en los rostros de los pasajeros. Como
nadie entendía la orden la hacían comprensible empujándolos con
las culatas de los rifles. Guadalupe se dio cuenta de que estaban
detenidos en una estación; lucían todavía las estrellas, pero
con esa claridad amortiguada que indica la proximidad de la aurora.
Ella comenzó a salir, formando fila con los demás, mientras en
las plataformas se escuchaban las órdenes altisonantes de la
soldadesca nazi: ¡_schnell_! _schnell_! _alles steigt ab_!

Bajaron en una vía central y treparon al alto andén; eran una
muchedumbre de varios cientos de pasajeros que se movían desconcertados
sin saber qué partido tomar. A la luz mortecina del día que despuntaba
podía verse el rótulo: Tula. Ochenta kilómetros a México. Guadalupe
se dirigió a la estación de boletos para inquirir a qué hora
proseguirían. El racionamiento estricto de gasolina había suspendido
todos los servicios de autobuses y la distancia era demasiado
grande para hacerla a pie. ¡Y sin embargo! ---se decía Guadalupe---
¿qué haré aquí si no pasan más trenes?

Se sentó en el suelo, con la espalda contra el muro, en el ancho
andén; el resto del pasaje, en actitud de espera, se apiñaba
en las salas, en el restorán, en los pasillos. No había noticias
de convoyes de pasajeros; se anunciaba uno para en la tarde,
pero no era seguro. La mañana comenzaba a entrar, el sol había
salido y sombras larguísimas proyectábanse sobre la plataforma
y los rieles. Guadalupe veía cómo el vaho se condensaba en el
ambiente glacial; hacía frío y sentíase aterida. Notó que un
hombre que se encontraba de pie junto a la bodega de carga la
veía con insistencia; era ya maduro, con cabello alborotado,
de aspecto recio, flaco de contextura y ojos penetrantes. Se
resolvió a hacer con las manos entrecruzadas sobre las rodillas
los signos por los cuales los saboteadores se reconocían; al
principio los inició con gran cautela, después más francamente.
Entonces el hombre aquel se acercó; llevaba huaraches y una unión
desteñida de mezclilla azul. Cuando estuvo casi junto a Guadalupe
comenzó la conversación en un tono casual; hubo una esgrima de
frases banales hasta que las respuestas de uno y otra los hicieron
comprenderse.

---¡Mala suerte, señora! ---Sí, dicen que no habrá tren sino
hasta en la tarde. ---¿Va a México? ---Sí, voy a ver a un amigo
de mi padre. ---¡Ah! ¿Dejó a su padre en el Norte? ---No, mi
padre también vivía en México. ---¿Vivía? ---sí, murió hace poco.
---¿Alguna epidemia?

El lenguaje popular llamaba muertes por epidemia a las originadas
por la invasión. Al oír la palabra, Guadalupe decidió aventurarse
un poco y contestó: ---Sí, murió por la epidemia---. Entonces
el tipo aquel se quedó viéndola fijamente y le dijo: ---Yo voy
también a México, si no le importa un viaje cansado creo que
la podré llevar; tengo un guayín de mulas y espero llegar en
la noche. Llevo a dos sobrinos míos; sus padres murieron también
en la epidemia.

A las ocho salieron rumbo a Tepeji del Río, por la carretera
de Ciudad Juárez. Ella iba al lado de su recién conocido, quien
era un tal José García y había tenido, por la región, un pequeño
rancho, de más o menos cincuenta hectáreas; hacía algunos años
le dieron un papel certificando que debería ser respetado por
el agrarismo, papel que estaba firmado por el presidente de entonces
quien parece que fue el apóstol máximo de ese desbarajuste. Pero
los líderes lo despojaron de cuanto tenía; una mañana un grupo
de desarrapados ebrios que venían desde San Juan del Río armados
de rifles se apoderaron de sus terrenos dorados de espigas; por
cierto que esos líderes, dice José García, son ahora autoridades
de varios pueblos con el gobierno de Cabañas. Entonces presentó
una queja a una oficina de la Presidencia. Mas cuando se convenció
de la inutilidad de sus gestiones y como el hambre comenzaba
a ser un serio problema, José Garcia, que vivió siempre de trabajar
su tierra, de desyerbarla y regarla, se convirtió en un parásito
y se dedicó a la compra y reventa de semillas. Pero estos atropellos
no lo hicieron un mexicano descastado, pues cuando vino la invasión
en lugar de pedir al gobierno del _quisling_ la justicia que le
había sido denegada, comenzó a sabotear al enemigo; su cuñado
y su hermana fueron llevados a un campo de concentración por
sospechas de haber inundado unos furgones de cemento que iban
con destino a obras militares y José García se hizo cargo de
los chicos.

Todo esto lo relataba mientras caminaban lentamente hacia el
Sur, bajo un cielo sin nubes y un sol que, a las dos de la tarde,
es siempre caliente en la Mesa Central. Guadalupe dormitaba a
veces, aporreada por la vigilia de la noche anterior; lo escuchó
en silencio y salvo confesarle su odio contra el invasor no dejó
traslucir el objeto de su viaje.

En la tarde, aletargada por el calor, el olor acre de las bestias
y el chirriar acompasado del vehículo, repasaba en su imaginación
los sucesos más recientes como si su vida entera tuviese que
desfilar ante su memoria en aquella travesía. De cuando en cuando
eran detenidos por patrullas de soldados nazis, cuyos uniformes
de un gris verdoso se identificaban a lo lejos; revisaban los
documentos de los viajeros, revolvían los canastos y las magras
alforjas de equipajes y los dejaban proseguir.

Remigio ---pensaba--- debe estar ahora por Durango, donde las
guerrillas dominan gran parte del estado. Su capital ha sido
la primera ciudad de importancia que pudo sacudirse el yugo nazi.
Allí el cura Valverde levantó al pueblo y aunque a la postre
cayó en la lucha, pasó antes a cuchillo al jefe nazi y a sus
oficiales. Aquellos sucesos, con toda su heroica barbarie, se
los refirió Remigio la última vez que se vieron.

Recordaba cuando comenzaron los primeros atropellos de los invasores
en San Miguel; Guadalupe y Remigio para que nadie los escuchase,
comentábanlos bajo los árboles del parque. Veían a don Rodrigo
taciturno y callado; él, de por sí tan dicharachero. Ya no ensalzaba
el orden y la disciplina de Alemania; lo que quería era libertad.
Verónica tejía en silencio. Patricio era como un hijo muerto;
nadie lo mencionaba; Remigio le aseguraba que en cualquier momento
abandonaría sus Juventudes Hitlerianas de México, pero esa certeza
---ella lo sentía así--- no era sino una esperanza. Cada nuevo
ultraje les encendía la sangre; ya no hablaban de casarse, ni
hacían proyectos para el futuro. Diríase que tenían miedo a hacer
un hogar y a que nacieran hijos destinados a la esclavitud del
régimen nazi; Guadalupe dábase cuenta ahora de que en sus últimas
entrevistas, cuando él estaba ya por unirse a las primeras guerrillas
---en esas noches del oscuro jardín de la casa paterna, donde
él, consumido por el deseo que nace de las separaciones y de
la muerte que acecha, la besaba con los besos del Cantar de los
Cantares--- no se había entregado por eso; por no traer a este
mundo una prole marcada con la suástica infamante.

Caminaron todo el día; los campos veíanse desolados. Las milpas
enanas levantaban una que otra espiga enfermiza; de cuando en
cuando, en la lejanía, una yunta de bueyes seguida de una figura
blanca parecía arrastrarse con lentitud; en extensiones inmensas
se enfilaban hasta perderse en el horizonte raquíticos montones
de rastrojo. Pequeños grupos de nazis los detenían con frecuencia,
los agobiaban a preguntas y hacían pesquizas en el carruco; después
los dejaban proseguir. Al principio Guadalupe se alarmaba; cuando
a lo lejos distinguía la patrulla o el camión militar que les
daba alcance, sentía cómo el miedo la estrujaba y el latir acelerado
del corazón. Pero ya para el crepúsculo aquello era solo una
rutina engorrosa. Entraron a México por Tlalnepantla y como a
las siete y media de la noche llegaron a Azcapotzalco.

José García se dirigió a Guadalupe diciéndole: ---Oiga usted,
yo no sé a dónde va; no quiero que me lo diga, pues en estos
tiempos todos tenemos nuestros secretos. Pero lo mejor es que
se quede esta noche en la casa donde paro; allí vive una prima
mía con su marido y habrá lugar para todos.

Ella aceptó, pues era tarde y en la metrópoli nadie debía salir
a la calle después de las siete. Llegaron a un patio destartalado
en cuyo fondo se alzaba la casa: unos cuartos muy primitivos,
sin vidrios en las ventanas y con pisos de madera que se quejaban
lastimeramente a cada pisada. No había luz eléctrica, sino viejos
quinqués de petróleo que esparcían una luz amarillenta y que
eran llevados de una pieza a otra.

La prima de José García, que se llamaba Carmen, y su marido,
un tal Jorge Molina, los recibieron con gran cordialidad. Fueron
a la cocina y de una alacena disimulada tras un rimero de cajones
Carmen sacó un gran trozo de pan; cortó algunas tajadas y se
puso a tostarlas sobre un brasero de carbón. Sacó también un
buen pedazo de cecina y en una olla de agua hirviendo echó dos
cucharadas de café; después extrajo con parsimonia de un paquete
unos trocitos de un azúcar moreno y revenido.

Entonces los invitó a esa magra cena y comenzó a quejarse del
hambre. ---Esto es el hambre ---les dijo---. Y aún así este café
es un lujo y son un lujo estas tiras de carne. Este es un banquete
en honor tuyo, José. Pero hay que ver a los chamacos del barrio.

No hizo más comentarios, ni interrogó a Guadalupe. Dióse cuenta
de que era más joven de lo que aparentaba; también de que podía
llamársela bonita. Pensó que hacía ese viaje exponiendo la libertad
y quizás la vida y ese pensamiento le bastaba. La llevó a un
catre angosto, pero con unas sábanas de manta blancas y suaves
a fuerza de lavarlas. Guadalupe se acostó y durmió con un sueño
reparador, cansada como cuando estudiaba las lecciones de la
mañana siguiente o volvía con sus compañeras de universidad de
una excursión dominguera. Se despertó temprano; el cuarto estaba
envuelto en una penumbra suave; las maderas de las ventanas,
de ocote delgado, tenían una tonalidad rojiza, de cosa viviente
como la de los dedos que se contemplan al trasluz de una lámpara.

José García la llevó a una antigua vecindad cerca del mercado
de la Lagunilla; atravesando un patio se llegaba a una escalera
angosta y oscura en cuyo desembarque se topaba con dos cancelas
verdes. La del lado derecho correspondía a la casa de Domitila
Saldaña quien tenía una pensión de huéspedes en donde comían
docena y media de abonados, jóvenes casi todos ellos. En realidad
aquella era una cofradía de saboteadores; hasta don Sotero Aldecoa,
que vestía siempre un chaqué porfiriano, recogía y llevaba correspondencia
para los buzones de las guerrillas. Todos sabían que en estas
maniobras exponían la existencia.

Allí fue recibida por Domitila; le tenía destinado un cuarto
con una cama de latón, una jofaina sobre un palanganero de metal,
un buró y un armario. Domitila le dijo: ---Hija, la esperaba
desde hace varios días. Le mandaré a avisar a Alonso Quintanar
que está usted aquí.

Guadalupe quedóse sola y se puso a pensar en lo que la había
obligado a emprender ese viaje tan azaroso. La lucha estaba en
su punto álgido; las guerrillas no concedían un minuto de reposo
a los ejércitos de ocupación dando así tiempo a que las Naciones
Unidas organizasen sus elementos. Los nazis nunca se sintieron
dueños del país, a pesar del gobierno pelele y de las melosidades
de los primeros tiempos. Esto es sabido y no voy a pretender
dar una impresión exacta de esas cosas. Pero esa lucha fue dura
y cruenta; cuando los invasores se convencieron de que convivían
con un enemigo dispuesto a todo, para quien los sacrificios no
contaban con tal de diezmarlos, emprendieron las represalias
en grande escala y lanzaron una propaganda apócrifa de pacifismo
católico. Entonces inundaron el país de hojitas y panfletos en
los que predominaba el lema de los mandamientos: «no matarás».
Esto era un sarcasmo, pero tendía a embotar el espíritu de sabotaje
y exterminio que embargaba a todos los mexicanos; con firmas
de arzobispos y obispos aparecían proclamas y cartas pastorales
que se comentaban con extrañeza en el seno de los hogares. Los
hombres las leían en las sobremesas con el entrecejo arrugado;
las mujeres las escuchaban sin atreverse a condenar las supuestas
palabras de los representantes de Dios sobre la tierra, pero
era claro que el odio al invasor luchaba con su lealtad a la
Iglesia.

Esta propaganda llegó a preocupar a los guerrilleros y a los
mismos estados mayores que dirigían la guerra. Era preciso que
la determinación con la cual se combatía a los invasores no decayese;
era preciso que el sabotaje continuase en todas sus formas y
también que la fórmula que ordenaba cambiar una vida mexicana
por dos vidas nazis persistiese sin tregua.

Entonces se adoptó la resolución de entrevistar al Arzobispo
que había permanecido en la Capital y que vivía, en realidad,
como secuestrado por el gobierno de paja y por el Jefe Supremo
de la ocupación, el Barón Von Virchow

Guadalupe recordaba los grandes esfuerzos que le costó convencer
a Remigio para que a ella se le enviase a desempeñar ese encargo;
conocía bien al dignatario eclesiástico y no era la primera ocasión
que arriesgaba su vida por la causa. Podía hacerlo una vez más.

El Arzobispo vivía en su casa de siempre, pero era objeto de
una estrecha vigilancia; era el rehén ideal. Se pensaba usarlo
en un caso extremo y por lo mismo se le prodigaban toda clase
de cuidados. Cuando se decretó la clausura de las iglesias en
todo el país para justificar su permanencia en México se dejó
abierta la Catedral Metropolitana. El arzobispo aceptaba estas
imposiciones con una gran paz de alma; alguna vez en que solicitó
permiso para salir del territorio ocupado por los invasores,
empeñando su palabra de que volvería, le fue negado con grandes
fórmulas de urbanidad. Aceptó el contratiempo como una de tantas
pruebas a las cuales todos estaban sujetos en estas épocas aciagas.

Era un hombre que andaba en los setenta, de aspecto juvenil,
recios cabellos blancos que ningún peine podía domar; sus ojos
sonreían detrás de las gafas. De inteligencia ágil, había logrado
llevar a su grey a través de las borrascas jacobinas de los gobiernos
revolucionarios. Durante los regímenes anteriores a la invasión
pudo obtener para su Iglesia un _modus vivendi_. Esto no lo satisfacía;
aspiraba al triunfo glorioso de la religión, al derrocamiento
de las leyes de Reforma, a la libertad completa de enseñanza
que permitiese inculcar el Ripalda al mismo tiempo que el silabario.
Pero se guardaba mucho de expresar estas secretas aspiraciones;
recomendaba a todos el respeto a la ley y la sumisión a la autoridad
civil. Profesaba la creencia de que la Iglesia es eterna y de
que su poder se va depurando con las vicisitudes.

Era un hombre de espíritu libre. Gustaba de hablar con toda clase
de individuos con tal de que fuesen inteligentes y se expresasen
con claridad. Entre sus relaciones se contaban algunas no muy
ortodoxas en materia de fe y otras francamente heterodoxas. El
las escuchaba y no rehuía la polémica. Había tratado con intimidad
a Maclovio Otamendi cuyo asesinato fue un golpe muy rudo para
su corazón. Por su intermedio conoció a Alonso Quintanar para
el cual anhelaba con palabras bondadosas su conversión a la fe;
pero mientras tanto discutían sobre muchos tópicos. Cuando Alonso
le decía que la Iglesia en general y muy particularmente la mexicana
no se habían compenetrado del alcance de la encíclica «Rerum
Novarum», agregando que era al clero a quien correspondía haber
aplacado el hambre y sed de justicia del pueblo mexicano, el
arzobispo le contestaba con suavidad diciendo que no le daba
la razón, pero que tampoco se la quitaba y que esas cosas se
juzgarían más tarde, cuando todos ellos no serían sino polvo
y el mundo fuese otro mundo. ---Esperemos que para entonces los
hombres vivirán cobijados todos por la Justicia y Misericordia
Divinas. Todo habrá cambiado; lo único que continuará inmutable
será la Iglesia.

---Está bien, señor ---replicaba Alonso--- pero eso no destruye
mi tesis.

---Hijo mío ---contestaba el prelado--- no pretendo destruirla.

Guadalupe lo conocía sobre todo por las charlas de su padre,
quien a menudo comía o cenaba con él. Maclovio alegaba que allí
se tomaba el mejor chocolate de México y también que la charla
era tan sabrosa como los molletes. Decía que el arzobispo juzgaba
a los hombres con una gran tolerancia y envolvía sus juicios
en una ironía indulgente. Maclovio solía contar sobre esto innumerables
anécdotas, una de las cuales venía a la mente de Guadalupe con
todos sus detalles.

Cuando se entabló la última lucha religiosa en la república,
allá por los años de veinte, o quizás de treinta, pero de todos
modos antes de la guerra, se había enseñoreado del Estado de
Veracruz un sátrapa que se llamaba Luis Pérez Chirino; decíase
que descendía de aquel Chirino de triste memoria en los anales
de los tiempos que siguieron a la conquista. Aunque de una familia
muy religiosa y con dos hermanas monjas, se distinguió por los
atropellos que cometió con los católicos de Veracruz. No pasaba
una semana sin que saliera un decreto atentatorio para la libertad
del espíritu; los vecinos del Estado, católicos y no católicos,
sentían náuseas cada vez que veían la firma fatídica: L. Pérez
Ch. y se vengaban de los ultrajes arrancando de los muros esas
ordenanzas para usarlas en los retretes. Finalmente un grupo
de ciudadanos atribulados fue a la Metrópoli y se presentó al
Arzobispo pidiéndole consejo; varios de ellos hablaron en términos
apasionados y se cuenta que alguno, procedente de Alvarado, salpicó
su protesta con términos que allí son de uso corriente en las
mejores casas y que también brotan de los labios de los carretoneros
cuando quieren arrear a sus bestias. El Arzobispo los escuchó
con placidez sin escandalizarse por los excesos jarochos de lenguaje;
parece que la indignación de sus hijos en Dios la encontró muy
puesta en razón. Pero les prohibió que pensaran en caminos torcidos
y condenó con energía los planes de revueltas. Cayó en su tema
favorito diciéndoles que la Iglesia Católica Apostólica Romana
es eterna y que Dios mismo la cimentó sobre piedras inconmovibles.
En fin, tranquilizó a aquellas almas exaltadas, diciéndoles que
a don Luis Pérez Ch. le podían acontecer muchas cosas: podía
abandonar el Estado de Veracruz; podía morir o perder su influencia
política o podía deponer su actitud combativa contra los fieles.
Esto último le parecía más que probable; bastaría con que gobernase
el país un presidente creyente.

Aquel grupo de gentes sintióse confortado y ya para levantarse
el Arzobispo les dice:

¿Y a este señor Pérez Ch. de dónde le viene la Ch.?

Todos los presentes se vieron unos a otros sorprendidos; nadie
acertó a comprender el alcance de la pregunta ni tampoco a contestarla.
Pero el anciano prelado formuló él mismo la respuesta con gran
suavidad: ---Creo que le viene de la madre---. Y como todos se
vieran entre sí y comenzaran a dibujarse sonrisas maliciosas,
él se levantó y al mismo tiempo que ofrecía su anillo pastoral
para despedir a sus visitantes, dijo con su voz clara y bondadosa.
---Sí, parece que la señora se apellidaba Chirino.

Esa misma mañana Alonso Quintanar apretaba efusivamente entre
sus brazos a Guadalupe Otamendi. Cuando estuvo al tanto de la
situación le ofreció acompañarla a ver al Arzobispo. En la tarde
Guadalupe llegó hasta la casa del prelado, un poco cansada por
la distancia, pues para llegar había tenido que recorrer a pie
la ciudad de un extremo a otro.

Se hincó con una rodilla ante el viejo sacerdote besando su sortija
y en seguida, sentada en un ancho sillón, comenzó a referirle
todas las peripecias de la lucha. Alonso Quintanar se hallaba
frente a ella y ambos la escuchaban con patente interés. Guadalupe,
al revivir con su relato los sufrimientos de las gentes vejadas
por los invasores, sentía que la emoción le subía por el cuello
desde las fuentes de sus entrañas, le paralizaba los músculos
del rostro y le apagaba la voz. Por momentos las lágrimas le
inundaban los ojos y esa envoltura cristalina les prestaba un
fuego apasionado, hasta que escurrían de las comisuras de los
párpados como el agua de los veneros que se pega a las rocas.
Pero ella continuaba hablando, sin sollozos, con una voz igual.
Pedía que la Iglesia declarase abiertamente la guerra a los nazis,
que los sacerdotes hablaran desde los púlpitos y en la sombra
de los confesonarios, que la causa de los guerrilleros fuese
bendecida en todo el territorio ocupado, que se diese un mentís
a la propaganda apócrifa vomitada sin cesar por las imprentas
boches. A medida que hablaba sus frases inundaban el ambiente
con inflexiones que para sus oyentes eran bien conocidas; recordaban
a Maclovio.

Al final el Arzobispo tomó la palabra y habló pausadamente. Veíase
que estaba emocionado y sus frases se oían solemnes en la pequeña
sala:

---Hija mía, me recuerdas a tu padre, a quien tanto quise, y
si no trajeras ante mí otros méritos ese sería bastante para
despertar mi simpatía. Tu padre fue un paladín de las causas
nobles y me conforta ver con mis ojos que persiste su buena simiente.
Ahora escúchame; quiero aliviarte y consolarte y que lleves mi
mensaje a nuestros amigos.

El viejo sacó entonces un enorme pañuelo y se sonó estrepitosamente.
En seguida prosiguió:

---En cuanto un sistema social entra en contacto con el orden
moral, la Iglesia reclama su competencia para juzgarlo y decidir
si sus bases están de acuerdo con el orden inmutable de Dios.
Por eso señaló por la voz de León XIII los errores del socialismo
materialista preconizando al mismo tiempo la justa distribución
de los bienes terrenos.

Por eso también el Santo Padre Pío XI desenmascaró ante el mundo
lo que es el nazismo: «una tiranía sin Dios» ---nos dijo--- «despiadadamente
destructora y brutalmente asesina». También dijo: «las maquinaciones
del nazismo no han tenido más fin que el exterminio de la Iglesia».

Nuestro Pontífice Pío XII a su vez, ante las doctrinas de los
totalitarios, ha pronunciado palabras de condenación para quienes
han modelado un nuevo ídolo que no salva, que no se opone a la
codicia ni a la soberbia; para quienes han modelado una nueva
religión sin alma, que carece del espíritu de Cristo.

Porque la Iglesia, hija mía, condena todos los regímenes que
degradan el alma. Proclama que el origen y el objeto primario
de la vida es la conservación, desarrollo y perfección de la
persona humana; no puede prescindirse de esta relación interna
y esencial con Dios. Pues el hombre individual participa de lo
divino, porque Dios está en él.

En defensa de estos principios estamos en pie de guerra y nuestra
actitud no es ambigua. Pero di a tus amigos, a nuestros amigos,
que la Iglesia lucha con sus métodos propios y con sus armas
peculiares. Los poderes temporales construyen aviones y barcos;
nosotros damos fortaleza a los espíritus y amurallamos las almas
con la fe. Por eso Cristo ordenó a sus discípulos: ---Id a todo
el mundo y enseñad a todas las gentes--- y también dijo: ---Yo
soy la luz del mundo.

Di a nuestros amados amigos que no olviden las palabras de nuestro
Santo Padre: «que este mundo viejo, que se apaga en el dolor,
genera un nuevo mundo». La hora de la paz vendrá y debemos hacer
de ella, no una paz frustrada y fracasada, sino una paz justa.
Haremos una paz que sea obra de la Justicia. Santo Tomás nos
lo dijo: «Opus justitiae Pax».

Tranquiliza a Remigio y a todos los que luchan con él. Infórmales
que por medios adecuados hemos comenzado a desenmascarar esa
propaganda apócrifa, renovada demostración de que los nazis hacen
de la mentira un arma suprema. Diles también que les mando mi
bendición. Y tú, hija, recibe la mía y por mi conducto recibe
también la de tu padre, que está en los cielos.

El prelado se levantó mientras Guadalupe se arrodillaba ante
él y besaba con efusión la mano izquierda que se le tendía, en
tanto que la diestra, haciendo la señal de la cruz, trazaba sobre
su cabeza signos que la cubrían, como un palio invisible. Los
labios del anciano musitaban las palabras del ritual.

Alonso Quintanar se había levantado también y desde la penumbra
de un rincón contemplaba la escena. Se acercó después y estrechó
la mano del Arzobispo, que se retiró dejándolos solos. Guadalupe
lloraba en silencio; Alonso le puso una mano sobre su hombro
y la atrajo contra su pecho. Después le dijo en voz baja: ---Debemos
irnos; pero sal tú sola y vete. Es una imprudencia que nos vean
juntos---. Y Guadalupe salió lentamente; había enjugado sus lágrimas
y en sus ojos se adivinaba una íntima alegría.

Alonso, en el curso de los años que siguieron, rememoró este
suceso. Quienes lo habían representado desaparecieron tragados
por la tragedia que los envolvía. El Arzobispo fue muerto por
los nazis en los últimos días de la invasión; Guadalupe había
desaparecido antes, en uno de sus viajes, misteriosamente, sin
dejar la menor huella. Toda esperanza de volverla a ver viva
se fue perdiendo con el tiempo, como la luz de las estrellas
en la alborada.

</section>
<section epub:type="chapter" role="doc-chapter">

# XV

++La++ muerte de don Rodrigo Guerrero no fue conocida en San
Miguel Allende sino varias semanas después. Los invasores ocultaban
invariablemente estos asesinatos, pues así como hacían cruel
ostentación de los fusilamientos de rehenes y de su despiadado
proceder para con los insurgentes, propagaban la especie falsa
de que en los campos de concentración se trataba con hidalga
magnanimidad a los recluídos.

Era una de tantas mentiras nazis. Se hacía aparecer este aislamiento
como una medida necesaria de profilaxis militar.

Al terminar la guerra con los Estados Unidos y al firmarse la
paz ---en la cual México con su _quisling_ y su gobierno pelele
quedaría como la atalaya nazi del Continente--- todos esos concentrados
volverían a la dulzura del hogar.

Pero la realidad era otra. Se conocía a través de las increíbles
comunicaciones insurgentes, pues de un lado a otro del país llegaban
las cartas de los confinados.

Si en el campo de concentración encaramado en La Mixteca una
hija escribía a su hermano que trabajaba en Tamaulipas, la misiva
hacía su camino en plazos increíblemente cortos. La sátira popular,
con su gracejo desencantado y pesimista, alegaba que el correo
secreto de los prisioneros, manejado sin estampillas ni burocracia,
era más rápido que el de los mejores tiempos de prosperidad revolucionaria.

Los buzones abundaban; el médico que hacía una visita casual,
o el desconocido que inquiría un informe sin trascendencia, o
el pordiosero que imploraba una caridad, dejaban al descuido
la carta escrita desde el campo remoto. Nadie sabía cómo esas
misivas se abrían paso insensiblemente de mano en mano.

El aceptarlas y hacerlas avanzar se constituyó en un credo de
honor; era un mandamiento acatado por todos y un detective curioso
habría encontrado en uno de aquellos sobres mugrientos que llegaban
a una mujer angustiada por la suerte del marido, huellas digitales
de magistrados, choferes, curas, obreros, estudiantes, mendigos
y hasta prostitutas.

Estas huellas redimían y honraban a quienes las imprimían y hacían
vislumbrar horizontes de esperanza. Ciertamente revelaban un
México no sospechado.

Por otro lado Remigio conocía detalladamente la inhumana vida
de los prisioneros. El trabajo de sol a sol, las comidas misérrimas,
los trastes sucios que nunca se lavan, los piojos, las disenterías,
las tifoideas, los excrementos en los dormitorios, los millones
de moscos posándose sobre la mugre, sobre la porquería y la podredumbre.
Esto lo sabían los insurgentes y lo hacían conocer al país entero.
Existían aún algunos engañados de buena fe; creían que esas noticias
eran infundios de la propaganda yanqui. Otros se dejaban engañar
con dolo; eran los políticos del gobierno de paja, que gozaban
de todos los privilegios y que prestaban su testimonio en favor
de los invasores.

Verónica no podía hacerse ilusiones. Es cierto que las cartas
de don Rodrigo procuraban tranquilizarla, pero su instinto le
abría los ojos. Las líneas eran cada vez menos firmes, los rasgos
más temblorosos, las letras menos claras. Y además, ella sabía
leer entre renglones con diafanidad absoluta: «mi tierra ---decía
él--- mi tierra de libertad; esa tierra de los tiranos como el
general Díaz; de los dictadores o dictadorzuelos revolucionarios,
pero donde pensábamos y hablábamos a nuestro antojo».

«Dile a Patricio que vuelva en sí. Que te escuche. Dile que nada
de lo que Hitler nos haya podido traer con su orden nuevo vale
lo que una sola alma esclavizada. Dile que nada significan la
disciplina, el trabajo y la constancia cuando no brotan de un
albedrío libre».

En otra carta le decía:

«Verónica, mi Verónica, yo no sé si volveremos a vernos; pero
escríbeme y dime que Patricio se ha unido a Remigio y lucha al
lado de él por el ideal cristiano en el que yo quise hacerlo
crecer».

Y Verónica, en su gran sencillez de mujer buena, comprendía que
su marido se sentía responsable por esa vida descarriada. Aquellas
alabanzas a Hitler, escuchadas por años en el hogar, se habían
prendido en el alma del muchacho; las tiradas contra los Estados
Unidos, cuando ya México estaba empeñado en la guerra, habían
dado sus frutos. Ahora Patricio era el jefe de las Juventudes
Hitlerianas de México y su obediencia al partido estaba por encima
de cualquier otro sentimiento.

Ella valorizaba su tragedia en función de los grandes afectos
de su vida: Rodrigo, Remigio, Patricio y Ana; y ne podía comprenderla.
¿Por qué se había trastornado tan profundamente su tranquila
existencia pueblerina, con cariños tan sólidos, como que eran
su misma carne?

Don Rodrigo, siempre apacible, afable, hablando a sus hijos
en ese tono de camarada, bromeando con ella, con Verónica, y
poniendo sus anchas manos paternales sobre las espaldas, los
hombros y las cabezas de todos. De cuando en cuando al salir
de casa, al dejarla sola en un cuarto, al desenlazar sus manos
de las suyas, la besaba en la frente o en la mejilla o en las
comisuras de la boca, con besos que sugerían la sensualidad de
antaño.

Remigio, alto y atlético, que la cargaba en vilo y entraba siempre
a verla en la noche, aunque llegase a deshora, y le escribía
dos o tres renglones amorosos cuando iba a la universidad, como
a una novia.

Patricio serio, reconcentrado, lleno de pasión y Ana, un poco
seca, pero leal, verdadera como una moneda de ley.

Y Verónica por encima de aquella síntesis de amor envolviéndolos
a todos como una manta cálida, con el calor de sus senos, de
sus brazos, de su regazo, de su corazón.

Ella había escuchado las discusiones familiares sobre las cosas
que pasaban en el mundo con una tolerancia incomprensiva. Para
ella eran temas abstractos, ajenos a sus únicas realidades. ¿Qué
importaba que hubiera fraude en las elecciones? ¿Qué, las consignas
a los gobernadores? ¿Qué más daba que el presidente fuese Flores
o Calles, Ortiz Rubio o Vasconcelos? ¿Y por qué habían de quitarle
el sueño las prácticas totalitarias de los italianos, los rusos
o los alemanes?

¿Por qué, mientras su marido fuese providente y bondadoso, sus
hijos buenos, la lana abrigadora, el pan aromático; y todo bien
habido?

Y no es que fuese necia ni egoísta. ¡Pero es que el mundo, para
Verónica, era tan transparente, tan fácil, tan sencillo!

Y pensaba, casi en su subconsciencia, después de oír hablar a
su marido con un tono un poco doctoral y a sus hijos argüir y
alzar la voz: ---¿Por qué tantas palabras para buscar la dicha
de los hombres, si el cielo nos cobija a todos y podemos alcanzarla
con solo acatar nuestros diez mandamientos?

¿Cuándo cambió todo aquello? Era difícil decirlo. Verónica se
sentía envuelta por el amor de todos. Sin embargo. el tono de
Patricio se fue haciendo autoritario. Cuando llegaron los alemanes
y las calles se llenaron de cascos grises y botas resonantes,
vivía en México con Eligia. Volvía a San Miguel con un séquito
cada vez mayor; más tarde paraba en alguno de los hoteles y solo
venía a verla para estar un rato con ella.

Aparecía severo, intolerante, hablaba de Remigio con ira mal
reprimida; Remigio, el baldón de esa familia, con su yancofilia.
Después, cuando ya Remigio era un líder insurgente, Patricio
evitaba mencionarlo ante su madre y había prohibido que su nombre
fuese pronunciado en esas entrevistas. Verónica, sin querer comprender
la ruina de su casa, lo escuchaba aún con ilusión. Quería ver
en sus ojos las mismas cosas de antaño, pero su fe vacilaba.

Con la aprehensión de Don Rodrigo, Verónica se dio cuenta de
lo insondable de su desgracia. Ella sabía que Patricio tenía
una postura de preponderancia decisiva ante el Gobierno y ante
el invasor. Sentía vagamente, casi con miedo, que su hijo no
estaba en un sitio limpio; pero en lo hondo de su corazón quería
explicárselo. Después de todo, aquella era una lucha civil. La
razón podía estar de cualquier parte y Patricio, cuando más,
sería un equivocado; tal vez a la postre el mundo sería mejor
y más feliz cuando de un hemisferio a otro dominasen los hombres
del partido.

Lo mandó llamar, con una breve carta, para que fuese en auxilio
del pobre viejo concentrado. Patricio ---pensaba--- podrá reparar
este atropello. Existe aquí un error monstruoso.

---¿Pero tú, Patricio, eres mi hijo? Y crees que los principios
de tu partido justifican mi desdicha y mi soledad? ¿Y tu partido
está por encima de los sufrimientos de tu padre? {.espacio-arriba1}

¿Qué no recuerdas nuestra casa, tu infancia llena de calor?

Patricio permanecía silencioso, sentado ante ella, atormentado
por esa entrevista y sufriendo también. Tenía la mirada clavada
en el suelo, entre sus botas abiertas. Con el fuete se golpeaba
las piernas, mientras su madre hablaba y hablaba haciendo desfilar
ante sus ojos su vida entera. La época de escasez, cuando era
muy chiquillo y a veces iba a la escuela con los zapatitos abiertos
de lado a lado. Y el trabajo duro de Don Rodrigo y el horizonte
que se iba ensanchando poco a poco.

---¿Qué no recuerdas, hijo, el año nuevo aquel cuando se hizo
la escritura de la mina?

Los bultos de México vinieron por docenas con vestidos, camisas,
abrigos y dulces. Entonces tuviste tu uniforme de _boy scout_ y
tu cuchillo de monte; el rifle y tu equipo de campamento. Para
Remigio y para ti llegaron las raquetas de tenis y pelotas y
zapatos y gruesos _sweaters_ de lana para que no se enfriasen.
Y todo, hijo mío, empapado en el cariño de tu padre.

Estábamos allí, solos, porque nada nos hacía falta, fuera de
nosotros mismos; Rodrigo y yo; Remigio y tú y Ana. Todos los
paquetes sobre la mesa y ustedes hundiendo sus manitas ávidas
en esas maravillas. Tu padre en un extremo los veía, dichoso
con tanta dicha. Y es que esos pequeños bienes materiales eran
un símbolo.

Verónica hablaba y hablaba. Ya no era la Verónica callada y llena
de reposo, sino que quería agobiar al hijo con un torrente de
recuerdos. Ella, de por sí tan parca en palabras pensaba que
su evocación iba a ablandar el corazón de Patricio.

---¿Recuerdas, hijo mío, cuando nació Ana? Te habíamos mandado
fuera de casa porque eras observador y queríamos preservar tu
inocencia. Cuando volviste, en la noche, entraste a mi recámara.
Yo estaba débil, pero feliz. Y tu padre te preguntó: ---¿Quisieras
tener una hermanita?--- Tú respondiste que sí y entonces la enfermera
puso en tus brazos ese pedacito de vida nueva.

Miraste a Ana con un azoro cómico y yo te pregunté: ---¿Te gusta?---
Y tú respondiste con una mueca: ---No, no me gusta---; y sin
embargo, Ana fue siempre la pasión de tu infancia y de tu adolescencia.
Fue tu novia antes de la que es ahora madre de tu hijo. Para
Ana eran tus halagos, tus regalos, tus cartas.

Y Ana ¡cómo te ha querido! ¡Con qué orgullo maternal ---mayor
que el mío, hijo---, te veía salir en las mañanas al colegio!
Ella, la mocosa de siete años, engreída con el chaval de trece.

Más tarde, cuando ibas ya a la universidad y te las echabas de
filósofo, la llamabas «mi saeta» porque decías que era larga,
delgada e inflexible, y como las saetas era impetuosa e iba recta
al blanco.

Ana no te comprendía, ¿recuerdas, Patricio? a los trece años
era flacucha, desgarbada y patuda ¡con sus trenzas enrolladas
en la cabeza y su gran moño ridículo! Y cuando tú le decías:
«ven, saeta», se abalanzaba sobre ti y te golpeaba con sus dos
puños sobre tu pecho con toda su fuerza y te gritaba: Patri,
Patri, no me digas saeta que me suena mal.

¿De dónde sacaba Verónica esa elocuencia sencilla? Seguramente
era su amor, su amor por todos. Por esas gentes suyas que ella
había unido y amasado en su corazón.

No defendía la libertad de Don Rodrigo; o mejor dicho, no solo
eso defendía; defendía la vida de todos, la alegría del pasado,
la plenitud de ese presente, que gozó hasta que algo se interpuso
y agrietó la ancha base donde se asentaba la existencia de la
casa.

No luchaba por la vida de ahora, sino por la que ella ansiaba
perpetuar para siempre en el cielo de su fe. Luchaba con el arma
de sus palabras porque sabía que esa vida tenía que ser la de
todos; los repasaba en su mente: Rodrigo, Remigio, Patricio,
Ana. Si uno solo llegase a faltar, su misión quedaría trunca
y mutilada. Ella, en el día del juicio, tenía que llevarlos a
todos, presentarlos a todos, y decir: aquí están, Señor, los
seres que entregaste a mis cuidados. Te los devuelvo limpios
como el día que vieron la luz.

Y uno se le escapaba. Quizá el más inteligente. Sin duda el más
reflexivo, el más hondo. Allí estaba, frente a ella, con la mirada
baja y casi vaga, los músculos del rostro tensos.

Cada palabra de Verónica la sentía Patricio como un tormento
físico; la sentía en el vientre y todos los humores de sus entrañas
lo corroían como un ácido al escucharla. Verónica, que era la
madre quintaesenciada, la madre por antonomasia; que los había
criado con tan fino instinto animal, como si fuesen cachorros.

Y al evocar ese pasado que ella revivía con sus palabras, Patricio
sentía cómo se agarrotaban los músculos del cuello y la saliva
que tragaba era amarga.

Pero era preciso reaccionar. Lo que escuchaba tenía fuerza, pero
él debía sobreponerse con una fuerza mayor. En el horno de la
guerra se estaba fundiendo la materia de un mundo nuevo. Por
primera vez en México ---tierra de españoles degenerados e indios
enfermizos--- se peleaba por un brote de héroes. Y de allí vendría
ese México que él soñaba, que los nazis le habían enseñado a
soñar; un país de superhombres batalladores, inflexibles, austeros,
entregados por entero a la gestación de una raza.

Había que reaccionar, defenderse del hechizo de la voz que le
hablaba; la misma que lo arrulló de niño. Había que rechazar
la suavidad de esas palabras y pensar en su grupo y en sus Juventudes
Hitlerianas y en el partido; había que acogerse a esos pensamientos
y ser brusco y arrancarse a la seducción, aunque allí dejase
pedazos de sí mismo. Es preciso hacerlo ahora ---pensó--- pues
de lo contrario iba a sucumbir y a arrodillarse, y a confesar
sus dudas, y a decir que ya no se sentía tan seguro de su credo
nazi, y a echar su cabeza en aquel regazo caliente y a llorar,
a llorar.

Tenía que acopiar energías sobrehumanas para romper el maleficio,
para ser quien debía ser. Voy a sufrir ---se decía a sí mismo---
estoy ya sufriendo más que los inquisicionados y voy a sufrir
más aún.

Necesito dejar de ser yo mismo para decir lo que debo decir,
porque hay que acabar con un pasado de prejuicios.

Eso es ---se dijo--- eso es lo que me agobia. El día de mañana,
cuando aquí los hombres sean distintos y tengamos una patria
unida al orden de Hitler, entonces el tormento de ahora me parecerá
sin importancia y mis lazos con mis gentes se habrán transformado
en savia para alimentar la raza del futuro.

Pero hay que decidirse; hay que decidirse. Yo también debo hablar…
Y su voz salió hueca, inhumana, como la de un ausente.

---Madre, tú no entiendes las cosas---. Y al mismo tiempo pensaba---:
hay que ser seco y conciso, para no desfallecer. Y él también
desconocía su voz.

---Madre, tú no entiendes las cosas. Las juzgas como si el problema
que se plantea dentro de los muros de esta casa fuera el problema
del mundo. Estamos luchando por cosas más trascendentales y debemos
seguir una línea recta, aunque lastimemos a otros o nos hiramos
a nosotros mismos…

Sus palabras vacilaban. Verónica lo interrumpió:

---Hijo mío, no te engañes. El problema de esta casa es el problema
del mundo: es un problema de amor.

Patricio prosiguió con sequedad:

---Madre, tenemos puntos de vista encontrados. Mi vida está al
servicio de una causa más alta que los pequeños afectos de una
familia. Tenemos disciplinas que seguir y jerarquías que respetar.
Remigio ha hecho caer la deshonra sobre nosotros, ha extraviado
a mi padre y lo ha empujado a una rebeldía culpable. El partido
tiene que defender el orden nuevo, tiene que implantarlo donde
no existe.

Cuando un elemento lo amenaza, debe aislarlo. Yo no quiero ser
el juez de mi padre; pero tampoco puedo ser su defensor. Hay
juramentos que me lo impiden. Mi padre está bien y cuando esta
agitación estúpida provocada por los insurgentes vendidos a los
yanquis haya terminado, mi padre volverá a tu lado y él mismo
hará justicia a lo que ahora te parece tan duro.

Patricio se levantó. Deseaba terminar esa entrevista que ponía
el vértigo en lo hondo de su vientre. Su figura enorme se destacaba
sobre el muro encalado de la pieza, con las piernas semiabiertas,
asentadas en el suelo del ladrillo. Y frente a él, en un sillón,
Verónica empequeñecida era la imagen del dolor humano. |

Verónica ya no lo veía. Su rostro parecía el de una moribunda.
Sus ojos estaban perdidos en la inmensidad del espacio que la
separaba de Patricio, su hijo. Su cálida verbosidad se había
apagado y era como si sus fuerzas hubiesen huído y hubiese perdido
toda la sangre. Era un pobre cordero degollado.

Y desde el fondo de su aniquilamiento, articuló trabajosamente:

---Vete, vete; tú no eres mi hijo. No quiero verte más. Vete,
te digo.

Y estas palabras caían secas como golpes de martillo sobre un
trépano.

Patricio juntó los talones con estrépito militar, se inclinó
llevándose la mano a la gorra y dando media vuelta salió del
cuarto con apresuramiento.

Verónica lo vio partir como en una niebla. La vida parecía haberla
abandonado; su cuerpo entero se aflojó y su carne y su mente
se hundieron en un piadoso desmayo.

Cuando despertó, despertó con la vida rota; su vida y la de todos
aquellos que eran pedazos de su ser. Eso era peor que si hubiesen
muerto. Se sentía ahora víctima de una espantosa mutilación;
sin prevenirla le habían arrancado uno de sus pedazos, un pedazo
que era ella misma, toda entera, con sesos y corazón y músculos
y entrañas. Y así, incompleta desintegrada tenía que reasumir
su existencia.

Y fue entonces cuando las cartas de Don Rodrigo comenzaron a
llegar. Lacónicas y casi incoherentes. Escritas con sabe Dios
cuántos secretos y cuántos peligros; a veces una palabra se interrumpía
en el trabajo forzado bajo el sol implacable, en el polvo de
la cantera, para terminarse casi a tientas a la luz de la luna
que entraba a la galera abierta al viento.

Llegaban papeles arrugados, pedazos de periódico, epidermis de
magueyes, telas desgarradas. Y decían siempre: «estoy bien».
Pero Verónica sabía que un régimen en que los prisioneros no
podían escribir cartas de amor a sus mujeres o a sus madres,
era un régimen de verdugos.

En ocasiones esas cartas las llevaba Remigio, que se aparecía
a deshora en los lugares más absurdos. Verónica lo encontraba
en la casa de su nana Chole, ya tan vieja, arrugada y enferma
que apenas era un trasunto de vida. A veces la seguían; resonaban
a su zaga las pisadas de botas, entraban con ella hasta la cama
de la nana Chole y buscaban y rebuscaban. El viejo Luz, hermano
de Chole, veía los uniformes nazis indiferente, con sus ojillos
de centenario amparado en su invalidez. Cuando los esbirros salían,
a los cuantos minutos aparecía en la puerta la figura de Remigio.

Nadie sabía de dónde venía, ni cómo llegaba. Sentábase al lado
de su madre, en la choza ahumada y la apretaba contra su pecho.
Allí, a la luz de un mechón, leía la carta del marido; Remigio
había acicalado y limpiado y planchado el papel lleno de arrugas,
pero ella adivinaba sin decir nada.

Leían juntos las frases de siempre: «estoy bien; nunca creí ser
tan fuerte para mis años. Lo único que me pesa es tu ausencia».

Remigio le decía:

---Lo ves, madre, todo terminará bien.

Ella se complacía en dejarse engañar, en aparentar credulidad.
Después se quemaba la carta para evitar una pesquisa.

Charlaban diez minutos. Remigio le contaba de Guadalupe.

Con el tiempo esas entrevistas se hicieron imposibles. Los esbirros
entraban con Verónica y uno de ellos se sentaba a la orilla del
catre de tablas donde Chole yacía. Verónica temblaba por Remigio
pensando que, de un momento a otro, iba a meter sus anchos hombros
por la exigua puerta. Pero no. En esos casos Remigio no se presentó
nunca; un ambiente de tranquila indiferencia reinaba en el pobre
jacal de piedra. Luz fumaba su cigarrillo de hoja; las hijas
y las nietas hilaban la lana bajo los árboles del estrecho huerto.

Una tarde, cuando ya no esperaba ver más al hijo, entró a confesarse.
Del fondo del confesonario le habló la voz querida. La cabeza
comenzó a darle vueltas y creyó que iba a desmayarse de confusión
y de alegría.

---Madre, aquí estoy; háblame muy bajo. Aquí traigo letras de
mi padre. Voy a leértelas.

Las mismas palabras tranquilas y esperanzadas.

---Hijo, dámelas; quiero ver esos rasgos que vienen de sus manos.

---No se puede, madrecita; la rejilla lo impide. Y además, si
te registran no podría volverte a hablar, ni aún aquí. Cerrarían
la iglesia y apresarían y matarían a medio mundo. Ya tú sabes
cuánto me buscan. Pero ten fe. Las cosas no son tan fáciles para
ellos y nunca podrán consumar la tarea de esclavizarnos.

Esas visitas de Remigio cesaron de pronto y con ellas las breves
cartas de Rodrigo. Verónica comenzó a sentirse presa de una inquietud
llena de angustia. Entretanto, la lucha envolvía al país; las
guerrillas surgían, daban un zarpazo en los campamentos y los
pueblos ocupados por los nazis y se esfumaban perdiéndose en
las sierras abruptas.

En San Miguel se hacía una vida relativamente tranquila; apenas
si unos mil hombres, casi todos alemanes, patrullaban las calles
continuamente. Las mujeres iban a misa y al rosario. La casa
de Verónica estaba siempre guardada. Dos esbirros la vigilaban
por fuera y cuatro o cinco estaban siempre apostados en las azoteas
circundantes. Nadie entraba si no era registrado y así se dejó
aislada y sola a la pobre mujer. Salía de cuando en cuando a
visitar a alguien del pueblo y los soldados de guardia la acompañaban
y no la perdían de vista un segundo; cuando iba a la parroquia
se situaban detrás de ella.

Verónica sobrellevaba con resignación esta vida de prisionera.
Sin noticias del mundo, sabía que Remigio era una grave amenaza
para el orden nazi del país.

De cuando en cuando, en alguna casa amiga donde había un radio
escondido en un desván lleno de polvo y telarañas, le daban una
que otra noticia aislada que transmitía la propaganda insurgente.
Por estos medios logró saber Verónica que cinco mil alemanes
habían sido envenenados en la capital y que en un solo día, en
diferentes sitios, treinta y siete jefes de tropa habían caído
acribillados a balazos desde lugares desconocidos. También supo
que las pupilas de un lupanar habían cosido a puñaladas a quince
oficiales durante una noche de orgía, cerrando después la casa
y huyendo furtivamente. Se colegía bien que México no era un
país hospitalario para los nazis.

Un mediodía un pordiosero se presentó en la casa de Verónica;
a través de sus andrajos pringosos se veían sus carnes tostadas.
Llevaba barbas de una semana en un rostro curtido por la mugre
y se cubría la cabeza con un destrozado sombrero de jipi; mechones
de una cabellera hirsuta le salían por los agujeros. Era la estampa
viva de la miseria y del vicio.

Uno de los esbirros le dio un empellón con la culata del fusil
y le habló en correcto español, aunque con marcado acento extranjero:
---¿Qué vienes a hacer aquí?--- El mendigo lo vio con la osadía
del que nada tiene que perder y está acostumbrado a los maltratos:
---Vengo a ver si me dan alguna cosa. Cuando no encuentro trabajo
doña Verónica me da de comer---. El esbirro miró a su compañero,
quien le dirigió unas palabras en alemán. Continuó interrogando:
---¿Qué traes contigo? El hombre parecía no comprender; miraba
con un gesto estúpido. ---¿Que si llevas algo para esta casa?---
---Nada, qué quieren que lleve si no tengo nada.

El alemán, alto, blanco y rosado, comprendía que su deber era
detener al visitante o registrarlo antes de permitirle la entrada.
Pero la mendicidad era fomentada en el paraíso nazi-mexicano;
cada mendigo al cuidado de la caridad pública era una boca menos
que alimentar y en donde las industrias de producción no se establecían
aún bajo la eficiente férula teutona, se toleraba a la gente
vagabunda. Por eso el guarda se inclinaba a dejarlo pasar, pero
el asco le impedía tocarlo siquiera.

---A ver, sacúdete esa ropa.

El hombre se sacudió los pantalones rotos y holgados que colgaban
de unos tirantes pegados al cuero.

---Quítate el saco. Enseña al revés.

El mendigo iba ejecutando las maniobras.

---Voltea los bolsillos.

---No los tengo ---contestó y mostró únicamente los agujeros
desgarrados.

---Quítate el sombrero. Quítate los zapatos.

Cada orden era obedecida; a prudente distancia el remedo de sombrero
y los huaraches fueron inspeccionados.

---Entra y despacha pronto.

---Está bien, jefe.--- Y se metió de rondón hasta la cocina.

Las criadas, pulcras, lo vieron con recelo.

---Digan a doña Verónica que quiero verla; que quiero que me
den de comer, pero que quiero verla a ella---. Se dejó caer sobre
un banco.

Verónica, al oír el recado, se levantó del ancho sillón donde
estaba hundida. Un desconocido que quería hablarle era siempre
una promesa de una nueva ---buena o mala--- ¡solo Dios sabe!
Y con una energía que parecía desmentir los estragos del sufrimiento
se levantó rápidamente.

Al llegar a la cocina adivinó en la mirada de aquel despojo humano
que tenía algo que decirle y salió con él a la parte trasera,
en el corral donde la ropa recién lavada se secaba al sol. Allí,
ocultos a todas las miradas tras una ringlera de huacales vacíos,
el hombre aquel se quitó el sombrero y de entre los pelos recios
y sucios, como quien hurga en un basurero insondable, sacó un
papel mugriento y se lo entregó.

---Niña ---dijo en seguida--- diga que me den de comer.

Verónica tomó temblorosa el papel que el desconocido le alargaba,
lo apretó en su mano como si temiese que alguien fuese a arrancárselo
y apenas pudo decir:

---Muchachas, denle a este hombre de comer, y si él quiere, llévenlo
a la pila de la huerta para que se bañe; en la alacena hay jabón
y toallas.

En seguida Verónica corrió temblorosa a su cuarto, sabiendo que
iba a llorar, dijese lo que dijese ese papel. Se encerró cuidadosamente,
espió por la ventana a través del visillo y allí, en un rincón,
desplegó la misiva. Venía escrita con tinta y traía una fecha
atrasada de varias semanas… Pero no era su letra… no. Verónica
temblaba de miedo. No era su letra, y las líneas comenzaron a
aparecerle borrosas. Hizo un esfuerzo supremo para reprimir el
temblor de sus manos y para fijar su vista… No podía seguir las
frases, unas cuantas, de la carta… «Don Rodrigo Guerrero»… «diezmaron
a los prisioneros»… «fusilados»…

La cabeza le daba vueltas y pudo todavía leer y comprender: «murió
como un justo y un hombre». En seguida se desplomó como un buey
al que abaten de un mazazo, dándose con la frente en la aguda
esquina de una mesilla. De la herida la sangre empezó a correr
sobre el pálido rostro. Junto a su mano flácoida había caído
la carta.

Así permaneció hasta que Juana, la criada, entró para llamarla
a comer. Al ver allí el cuerpo apenas pudo reprimir un grito
y mordiendo el delantal para no romper en sollozos, volvió corriendo
a la cocina; quería a todo trance que las cosas pasaran desapercibidas
para los guardas nazis.

Con voz entrecortada dijo:

---Dorotea, la niña Verónica está muerta.

Volaron presurosas al cuarto de Verónica y trabajosamente la
colocaron en la cama; pronto se dieron cuenta de que respiraba;
limpiáronle la sangre que comenzaba a secarse y trataron de reanimarla.
Verónica suspiró hondamente y sin abrir los ojos comenzó a llorar
en silencio; las lágrimas le corrían desde los lagrimales por
sobre las mejillas como pequeños ríos de dolor. Ni Juana ni Dorotea
habían osado interrogarla, pero esta última vio sobre el suelo
el papel ajado y acercándose a la ventana lo leyó en silencio.
Comenzó a sollozar.

---Juana, Juana, han matado a don Rodrigo.

Entonces las dos mujeres se arrodillaron a los lados del lecho
donde Verónica estaba recostada y sobre su falda extendida lloraron
copiosamente.

Verónica se incorporó por fin y fue a sentarse con las manos
crispadas sobre los brazos del sillón. La fuente de sus lágrimas
parecía haberse secado. La tarde transcurría en silencio. La
noticia se esparció por San Miguel y comenzaron a llegar a la
casa los viejos amigos y conocidos de aquella familia. Se oían
en la puerta las discusiones con los esbirros que registraban
a los que iban llegando; por fin decidieron prohibir la entrada.
Entonces esas gentes se estacionaron en las aceras; cincuenta,
cien, y ciento cincuenta, y más. Hasta que se presentaron dos
patrullas de soldados nazis en las dos bocacalles que cerraban
la manzana. A culatazos hicieron un grupo de los que allí aguardaban
y los llevaron hasta la plaza donde los obligaron a dispersarse.

Los que pudieron llegar se acercaron hasta donde seguía Verónica,
callada y hosca. No se atrevían a preguntarle nada, ni a dirigirle
ninguna palabra de consuelo, que aún llena de caridad habría
parecido banal. Sus ojos despedían fuego, su rostro se tornaba
cada vez más duro.

Pasaron así las horas sin que nadie osara ofrecerle ni alimento,
ni un calmante, ni agua siquiera para refrescar su lengua reseca
y salada de lágrimas.

No había cuchicheos, ni murmullos. Solo silencio. La tarde principió
a pardear y la luz a huir de la pieza; los rostros se esfumaron
primero y después se hundieron en las sombras. Cada cuarto de
hora el reloj de la torre hacía oír sus campanas. Así dieron
las seis, las siete, las ocho y cuarto. La oscuridad era completa
y nadie se veía.

De pronto, del sillón donde estaba Verónica, partió un grito
agudo, mientras se oía claramente que ella se levantaba. Aquel
grito puso el espanto en el ánimo de todos y al mismo tiempo,
como si fuese una señal, estalló un coro de sollozos y lamentos.

Entonces, sin que nadie pudiese evitarlo por lo inesperado, Verónica
se abalanzó fuera de la casa, atropelló a los guardas apostados
en el zaguán y echó a correr por en medio de la calle.

Todos quisieron detenerla, pero fue inútil; antes de llegar a
la esquina su voz aguda comenzó a desgarrar la negrura:

---¡Asesinos, asesinos, han matado a Rodrigo! ¡Hitler, asesino,
asesino, asesino! ¡Hitler, asesino! Te mataron los nazis, Rodrigo;
¡asesinos, asesinos!

Los esbirros de guardia no habían sabido qué hacer. Solo sintieron
una sombra que salía tropezando con ellos y cuando oyeron los
gritos, a lo lejos, no podían de momento comprender su significado.

Las gentes abrieron instantáneamente sus casas y muchos se echaron
al arroyo; cuando pasaron frente a San Francisco, tres o cuatrocientas
hacían ya un grupo compacto. Nadie abría los labios y solo se
oía como un eco que se repite en la soledad, la voz aguda de
Verónica: ---¡Asesinos, asesinos!…

Llegaban ya a la plaza, cuando del convento de las monjas, donde
estaba el cuartel, se aproximaron varias veintenas de boches.
La muchedumbre, que marchaba hacia la parroquia, casi chocó con
el grupo de hombres armados; estos dispararon al aire, pero la
masa de gentes del pueblo se hacía más y más apretada.

Los soldados, entre las sombras, dando golpes salvajes con sus
rifles, se dirigieron a tientas hacia la voz que seguía gritando:
---¡Asesinos, asesinos!

Unos cuantos, entre los que rodeaban a Verónica, la tomaron entonces
por los brazos, la metieron al atrio y antes de que sus perseguidores
pudieran darse cuenta de lo acaecido la empujaron dentro del
curato.

El grupo de quienes la seguían comenzó a disolverse como por
magia y los nazis creyendo que la fugitiva había buscado el refugio
de la iglesia, entraron a esta con ruido de armas y estrépito
de botas.

El cura Videgaray bendecía en ese momento, revestido con la capa
pluvial y la custodia en la mano; el tintineo de las campanillas
llenaba los ámbitos y el olor del incienso llegaba hasta los
corazones. Ante la solemnidad del rito la soldadesca se detuvo.
Ellos esperaban ver a su perseguida huyendo por la nave y en
lugar de eso encontraban una unción sobrenatural. El cura, sin
embargo, presentía que algo había ocurrido. Terminó la bendición,
colocó la custodia en el tabernáculo y se dirigió a la Sacristía.
Allí lo esperaba ya el doctor Bernáldez que en las tinieblas
había seguido aquella protesta popular.

---Padre, Padre, doña Verónica Guerrero está aquí. Si la cogen
la matan.

El cura pensó y obró con la rapidez de un lobo.

Salió de la Sacristía rumbo al curato; allí, en la puerta, forcejeando
en silencio con cinco o seis gentes jadeantes, estaba Verónica.
La habían amordazado para que no gritase más, mientras ella se
debatía con furia.

Váyanse todos, dijo el cura y cogió a Verónica por un brazo arrastrándola
hacia los claustros. Esta se quitó el pañuelo de seda que le
tapaba la boca y reanudó su grito: ---¡Asesinos, asesinos! ---pero
ya el cura había atravesado varios pasillos oscuros, había llegado
a un recinto abovedado y a tientas, abierto una puerta baja que
daba a una escalera. Obligó a Verónica a entrar por ella y la
hizo descender; al final de esta escalera larga y estrecha, por
donde apenas podía pasar un hombre, abrió otra puerta y empujó
a Verónica hacia un túnel negro y húmedo. Echó un postigo y volvió
rumbo a la Sacristía.

Cuando entró a esta un oficial nazi y como una docena de soldados
lo esperaban.

---¿De dónde viene usted?

---De mi casa, contestó el cura.

---¿Dónde está la mujer que se refugió en esta iglesia?

El oficial se había acercado a medio metro del cura, amenazante.

El cura no respondió.

---Conteste usted. ¿Dónde está esa mujer?

---Como el cura continuase mudo, el oficial le cruzó el rostro
con un fuete y agregó:

---Queda usted detenido hasta que la entregue.

Y el cura salió por el centro de la nave, entre dos filas de
soldados.

En la oscuridad los cirios encendidos ponían círculos de luz
sobre las imágenes; las mujeres que habían asistido al rosario,
lo veían pasar, arrodilladas, y sollozaban sin contenerse.

A la mañana siguiente era un sábado. Desde las ocho algunos presos
de la cárcel ---pues no habían encontrado para la tarea hombres
libres--- iban por las calles de dos en dos vigilados por esbirros
nazis. Llevaban un gran bote de engrudo e iban tapizando los
seculares y bien cuidados muros de piedra del pueblo con un edicto
que salía del cuartel del lugar. Para las diez las hojas impresas
en mimeógrafo se habían fijado por centenares.

«Se da a los habitantes de San Miguel un plazo que fenecerá mañana
a las cinco horas para que sea presentada en el Cuartel de esta
ciudad, situado en el ex convento de las monjas una mujer llamada
Verónica Guerrero, esposa del licenciado Rodrigo Guerrero, preso
en un campo de concentración por su actitud subversiva, y madre
del bandolero Remigio Guerrero, cuya cabeza está pregonada por
su actividad militante en contra del nuevo orden».

«Si para esa hora la mujer susodicha no se encuentra en manos
de las autoridades militares, el cura Joaquín Videgaray será
pasado por las armas, por considerársele encubridor de su fuga».

Aquel pregón consternó al pueblo. Quizá alguno o algunos sospechaban
dónde se encontraba Verónica, pero nadie era capaz de decidir
entre su vida y la del cura. En todos los rostros se leía que
nada podía salvar a este último, pues solo un golpe de mano lo
habría arrancado de las manos de sus verdugos. Quizá Remigio;
pero este se encontraba lejos.

Los vecinos de San Miguel sabían que la suerte del sacerdote
estaba echada. Algunos, quizá media docena, se atrevieron a visitarlo
durante ese sábado y sus carceleros, incapaces por todos los
medios violentos de arrancarle su secreto, permitieron esas entrevistas.
Suponían que sus amigos podrían convencer a Videgaray de que
entregase a Verónica. Pero ninguno osó mencionarla.

Videgaray estaba extenuado con los golpes; tenía una ancha herida
sobre un pómulo, mal cubierta con gasa. Algunos, al encontrarlo
de rodillas, orando, ni siquiera lo interrumpieron y así fue
como pasaron las horas; llegó la tarde y después la noche, hasta
que el cansancio lo venció y en un camastro de tablas durmió
su último sueño hasta la madrugada.

Cuando lo sacaron por el zaguán del ex convento algunos espiaban.
Se había prohibido al vecindario formar grupos y manifestar en
alguna forma el pesar o la indignación. El cura iba entre una
patrulla de diez soldados, al mando de un teniente; para aterrorizar
más a los sencillos habitantes del pueblo se le condujo al atrio,
contra las paredes de la torre, y allí se le fusiló. La mañana
era fresca y luminosa; al sonar la descarga el reloj de la iglesia
daba las cinco y media. El cuerpo ensotanado y sangriento de
Joaquín Videgaray, cura de San Miguel, quedó allí tirado hasta
que, cuando los nazis se retiraron, su prima y otras mujeres
lo recogieron para sepultarlo cristianamente.

Pero cuando el teniente y sus diez hombres regresaban al ex convento
de las monjas, al cruzar la calle frente a la casa de los Condes
de la Canal, sucedió algo insólito.

Sonaron unos disparos y el teniente y tres de los suyos cayeron;
los cuatro heridos mortalmente. El oficial con el cuello roto,
dos soldados con balazos en la cabeza y el tercero en el abdomen.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVI

++El++ día en que fusilaron al cura Videgaray y fueron muertos
cuatro de sus ejecutores, las gentes se recluyeron en sus casas;
presentían las represalias.

Esa misma mañana, para las nueve, ya habían sido aprehendidos
unos veinte vecinos, entre ellos don Estanislao Gutiérrez, el
agricultor; el boticario don Antonio; el abarrotero Paco Améndola
y don Nicanor Aceves, que escribía la historia de San Miguel.

El doctor Bernáldez logró ocultarse a tiempo, no se sabía dónde,
y también don Aurelio Manterola; pero otras quince gentes de
menos relieve completaron el grupo.

Cinco soldados al mando de un capitán hicieron los apresamientos.
Llamaron primeramente con las culatas de sus fusilentes a la
casa de don Estanislao en las calles de San Francisco; al abrir
la criada el capitán le puso su enorme mano sobre el rostro y
la echó a rodar por las losas del zaguán. El viejo don Estanislao
quien desde los corredores de la parte alta acechaba tras un
pilar, lanzó un: «¡hijos de puta!» que retumbó de un muro a otro
del patio y salió resueltamente al encuentro de los esbirros
tropezando con ellos en el desembarque de la escalera, sobre
las amplias galerías. El oficial teutón, un hijo de alemanes
nacido y criado en México, lo increpó:

---¿Usted es Estanislao Gutiérrez?

---Sí, yo soy.

---Pues el hijo de puta es usted ---vociferó al tiempo que le
cruzaba el pecho de un fuetazo.

Don Estanislao todavía alcanzó a responderle con un sonoro cachete,
mas los hombres lo sujetaron mientras él se debatía gritando:

---Cabrones, hijos de la… ---no pudo proseguir porque lo amordazaron.
Entonces el capitán comenzó a azotarlo sin tregua.

Las hermanas de don Estanislao salieron. Una, la más joven, se
abrazó a él pretendiendo que no se lo llevaran; la otra, una
solterona enjuta, golpeaba a los aprehensores con el revés de
los puños cerrados, hasta que recibió un culatazo en la frente;
cayó hacia atrás mientras de una ancha herida sobre la ceja izquierda
le manaba sangre.

Don Estanislao, inconocible por los verdugones que lo desfiguraban,
salió de su casa, entre los soldados.

Se dirigieron en busca de don Nicanor Aceves, a la calle del
Recreo. Este abrió personalmente el portón, percatándose al instante
de lo que acaecía.

---¿Vienen ustedes por mí? Salgo desde luego.

---No, señor, ---dijo el capitán--- usted ya no entra; venga
en seguida.

Don Nicanor contempló entonces a don Estanislao y se dirigió
al oficial:

---Son ustedes unos mentecatos tratando así a un hombre; cuando
salgan de este país les van a ver las espaldas hasta los niños.

El aludido lo miró iracundo.

---Continúe usted y va a pasarle lo mismo. ¡Afuera!

Salió el grupo y llevaron a cabo todos los otros prendimientos.
Don Antonio sufrió un desmayo al contemplar a don Estanislao
marcado como un Cristo. Cerca de las nueve los detenidos pasaban
por en medio de la calle, frente a la Presidencia Municipal;
a los remisos los empujaban con las culatas.

Por el rumbo del parque otra escena se había desarrollado. Un
grupo de presos municipales, vigilados por dos esbirros, iba
pegando en los muros un nuevo edicto. Allí se amenazaba torvamente
a la población de San Miguel; se les daban cuarenta y ocho horas
para entregar a los matadores de los verdugos nazis. «En caso
contrario todos los rehenes que hoy fueron aprehendidos serán
pasados por las armas».

El edicto contenía frases que presagiaban una era pavorosa;
se obligaba a los habitantes de la ciudad a caminar por en medio
del arroyo y a levantar los brazos cuando encontrasen a un soldado
nazi.

«…estoy resuelto ---decía al final el jefe invasor--- a exigir
de todos, aún con medidas de la mayor dureza, la colaboración
al nuevo orden que nuestro Führer ha decidido implantar en América
y el respeto al Gobierno legítimo de esta república».

Dos individuos del pueblo se acercaron a leer la ordenanza en
el momento en que los presos fijaban unos ocho o diez ejemplares
en ringlera. De pronto uno de ellos lanzó un silbido y doce hombres
como brotados del polvo rodearon intempestivamente a los nazis.
Trataron estos de abrirse paso, pero uno fue derribado por los
asaltantes. El otro logró desasirse, cortó cartucho y a una distancia
de seis o siete metros disparó todo el cargador.

La banda dispersóse, pero dos de los que la formaban quedaron
allí, muertos. Junto con ellos, dos de los presos; otro perdióse
a la vuelta de la esquina, por el callejón del puente de las
Ánimas; el soldado lo siguió y encontró huellas de sangre fresca,
pero estas se detenían intempestivamente. Resolvióse entonces
a volver a donde había quedado su compañero; esperaba encontrarlo
muy mal herido, pero solo estaba golpeado y maltrecho. Se había
sentado apoyándose contra la pared de una casa con las piernas
extendidas sobre la acera; el otro lo ayudó a levantarse y a
caminar cojeando hacia el cuartel en tanto que como despojos
de la lucha quedaron regados los edictos y junto a ellos cuatro
cuerpos sin vida.

Al cabo de algunos minutos varias mujeres asomaron los rostros
por un postigo entreabierto y al ver solitaria la calle salieron
y fueron arrastrando pesadamente los cadáveres, uno a uno, hasta
el interior de una casa. De allí los condujeron al huerto del
fondo y entre sus sollozos reprimidos algunos hombres abrieron
apresuradamente una gran fosa y les dieron sepultura.

A la sazón por todo el barrio unas patrullas abrían los portones
con gran escándalo de pisadas y palabras altisonantes; pero la
pesquisa que hicieron fue muy desordenada y superficial. Después
se reconcentraron en el ex convento de las monjas.

Mientras tanto Körner había hecho llamar a su despacho al presidente
de la junta directora, Melquiades de la Peña; politicastro pueblerino
de antecedentes oscuros que fue en alguna de las administraciones
revolucionarias diputado local. Había sido compinche de asesinos
e inveterado ratero de dineros públicos. Capaz de todas las abyecciones
y perseguido por sus latrocinios se afilió al gobierno que apoyaban
los invasores. Su insignificancia lo llevó a la Junta Directora
de San Miguel.

El coronel Körner lo esperaba con su intérprete. Melquiades,
que llevaba uniforme, juntó los pies frente al escritorio de
Körner y saludó levantando la mano: ¡Heil Hitler!

---Va usted a visitar a los vecinos más representativos y les
va a advertir que la amenaza que hoy he hecho pública se cumplirá.
No van a amedrentarme porque un grupo de cobardes golpeen a un
soldado. Si es preciso convoque usted al vecindario y dígales
que ahora se fusilará a los hombres, pero que haré lo mismo con
las mujeres y aún con los niños. Todos servirán de rehenes para
garantizar el orden. Estoy resuelto a que San Miguel no constituya
un problema, ni siquiera pequeño, para nuestras tropas que combaten
con los yanquis.

Melquiades de la Peña contestó:

---Así se hará, mi coronel, pero yo me atrevo a pedir a usted
que dicte una medida necesaria. Haga cerrar la parroquia y todas
las iglesias.

Körner vio a Melquiades con curiosidad, casi sonriente y terminó:

---Las iglesias seguirán abiertas. Es el deseo de nuestro Führer
que el pueblo de México siga concurriendo a ellas mientras podemos
establecer la Iglesia Católica Mexicana; pero mandaré guardias
para que estén siempre en el interior. Ahora, cumpla usted mis
órdenes y retírese.

De la Peña dirigióse primero a la casa del doctor Bernáldez;
pero no lo encontró, se había esfumado y nadie lo había visto
en el pueblo; sus enfermos lo esperaban desde en la mañana pues
era tempranero para sus visitas.

En la casa de Facundo del Hoyo hubo únicamente un breve diálogo
sostenido desde los balcones del primer piso hasta el patio:

---Señor de la Peña ---contestó Facundo--- tengo cuarenta y cinco
años, una madre anciana, mujer y siete hijos. Dígale usted a
su coronel que no me siento con fuerzas para rebelarme; que haré
lo que se me ordene.

Pero en otras casas nadie le abrió y en las de gente de menos
fuste lo trataron francamente mal.

El herrero Miguel le dijo que él no sabía ni leer, ni escribir;
que lo dejaran tranquilo, y que si lo querían fusilar que para
cuándo lo dejaban.

El carnicero Praxedes, que estaba borracho, le gritó que su jefe
el boche le gustaba para colgarlo de una percha, y que se largara,
que no lo siguiera jodiendo.

De la Peña se decidió a convocar a una gran asamblea en el teatro.
Quiso juntar una fanfarria y hacer un pregón; pero no pudo reunir
a los músicos; estaban en las siembras o fuera de sus casas.
El pregonero salió con solo un tambor y se detuvo en todas las
esquinas de la ciudad; en la tarde apenas si siete vecinos concurrieron
al mitin.

Un ambiente de opresión pesaba en el pueblo. El barbero cerró
su local, pero lo obligaron a abrirlo. Uno que otro parroquiano
presentóse y el barbero lo sirvió silencioso, sin musitar palabra.
La tristeza oprimía los corazones; la muerte del cura era un
gran duelo para todos y el encapillamiento de don Estanislao
Gutiérrez y los demás, resecaba las lenguas. El pueblo había
perdido su claridad alegre; parecía que hacía meses desde que
esa misma madrugada, con el canto de los gallos, el cuerpo de
Joaquín Videgaray había caído al pie de la torre.

La prima Remedios y otras mujeres lo habían limpiado con amor;
le quitaron del rostro las costras de sangre, le cerraron los
ojos y lo revistieron con su ropa talar. Pretendieron llevarlo
a la iglesia donde el padre Benítez de la sagrada casa iba a
decir una misa de cuerpo presente; pero los oficiales invasores
lo impidieron y también que se formara un cortejo para llevarlo
al camposanto. Ordenaron que se procediese al entierro allí sin
más dilación; entonces varios feligreses abrieron una fosa en
el patio del viejo curato y en ese mismo sitio, amortajado en
una sábana, el cura de San Miguel descendió al lugar de su último
reposo. Era ya media mañana; por los claustros pululaban figuras
enlutadas y gente del pueblo: labriegos descalzos y enhuarachados,
viejos con rostros de santones, mujeres envueltas en sus rebozos,
niños de miradas inocentes. Se dispersaron cuando los cavadores
aplanaron el suelo con el revés de las palas.

Todos sabían que el drama apenas comenzaba; la flor y nata de
la gente buena de San Miguel iba a morir. ¡Don Estanislao, tan
hombre, tan entero, tan caritativo! Era anticomunista y nazista,
cuando para los mexicanos el nazismo no parecía, juzgado de lejos,
sino la antítesis de gobiernos desbarajustados y prevaricadores.

Ahora a ellos, allí prisioneros en el convento de las monjas,
esos conceptos y opiniones les parecían un sueño ante la implacable
realidad. ¡Y los demás tan confiados siempre en su suerte y en
la bondad de Dios, solo porque en el transcurso de generaciones
ni ellos ni ninguno de los suyos había muerto de hambre ni de
puñalada trapera!

¡Don Rodrigo y Don Estanislao uniendo sus dialécticas contra
las de Don Nicanor, de Alonso Quintanar, de Maclovio Otamendi!
Don Rodrigo y Don Estanislao, ante las palabras calientes de
la controversia, entablaban una polémica desinteresada, como
si discutiesen problemas de la vida lunar. ¡Alemania está tan
lejana! ¡El océano es tan ancho! Y de eso hacía apenas unos meses.
No; no había transcurrido una eternidad, ni se habían precipitado
las edades geológicas; ni siquiera eran otros los dioses de los
hombres. Sencillamente el mundo era el mismo y los alemanes estaban
en México: y Don Rodrigo Guerrero había sido asesinado y millares
caían todos los días.

Las iglesias comenzaron a llenarse de gentes. Las familias de
los detenidos se hincaban de rodillas frente a las imágenes,
les encendían ceras y oraban en voz alta, con un sonido rumoroso,
como el borbotear de un manantial. En la parroquia estos grupos
eran incontables y sus oraciones subían hasta las bóvedas; los
hombres permanecían de hinojos con los brazos en cruz, las mujeres
se daban golpes de pecho con las manos que empuñaban los rosarios
y el tintinear de las cuentas se entremezclaban a sus rezos.
Los niños de pecho rompían a llorar, de pronto, en el regazo
oscuro de las madres.

Así transcurrió el día. Como a las cinco de la tarde un muchacho
entró en la iglesia; era Francisco Toledo, jefe mecánico de la
fábrica de hilados, que los invasores tenían trabajando para
su provecho. Los esbirros alemanes que hablaban y reían bajo
el coro, saludáronlo al pasar, en un español chapurreado; lo
conocían pues Francisco había ido al cuartel habilitado de artesano
para arreglar las máquinas de escribir. Lo vieron pasar sin desconfianza.

Francisco se arrodilló ante un confesonario. El padre Vadillo
que había venido de la iglesia de San Antonio a sustituir al
cura Videgaray, se acercó a los pocos momentos, bendijo al penitente
y pareció oírle en confesión.

---Hijo ---díjole--- ya sabes todo lo que pasa; es preciso que
Remigio lo sepa. Doña Verónica no puede quedarse indefinidamente
donde está y es también menester que los insurgentes conozcan
nuestra angustia; si ellos vienen podremos siquiera morir peleando
y no acribillados como perros. Por eso te mandé llamar. Comunícate
con Remigio, como lo has hecho otras veces.

---Está bien, Padre. Esta noche saldrán a buscarlo.

Francisco abandonó la Parroquia y tomó rumbo a la casa del Conde
de Casa de Loja, a menos de una cuadra. Después de trasponer
el ancho zaguán, tocó en la reja. Asomóse de lejos Ernestina
Pereda quien todavía ahora habita allí con su marido, Enrique
Pereda.

Gritó desde el fondo:

---Francisco, pase; le envié un recado porque mi baño anda mal.

Francisco penetró al corredor mientras la señora de la casa se
perdía por la cocina; pero en lugar de ir al baño entró a una
pieza contigua llena de penumbra y que se veía a las claras abandonada;
quedóse allí unos minutos y finalmente se dirigió a un rincón,
hizo a un lado un baúl lleno de viejos trastos, descubrió una
escotilla y penetró por ella cerrándola tras sí. Daba acceso
a una escalera de piedra; descendió y por un túnel bastante espacioso,
de unos dos metros de ancho, llegó a una rotonda abovedada. Exploró
Francisco el recinto con su lámpara de mano; cinco túneles partían
en diferentes direcciones. Tomó resueltamente uno y caminó por
él bastante tiempo; quizás diez o quince minutos; en el trayecto
dos túneles más estrechos entroncaban con el primero, pero Francisco
siguió hasta encontrar un pequeño arco al ras del suelo por donde
apenas cabía un hombre; apagó su luz y arrastrándose lo traspuso.
Encontróse a la sazón en el fondo de un pozo, de unos doce o
quince metros de profundidad. En lo alto había un malacate y
de él, enrollada, pendía una cuerda. Todavía el cielo aparecía
claro y Francisco sentóse a esperar hasta que todo se fundió
en una oscuridad profunda y las estrellas escintilaron en el
cenit. Entonces restiró bien la cuerda y apoyando los pies en
las oquedades del muro, se fue izando hasta llegar al brocal.
Ya que estuvo fuera respiró el aire fresco de la noche, detúvose
unos instantes para recobrar el aliento y en seguida, sin encender
su linterna, comenzó a orientarse a tientas en las sombras.

De pronto unas manos garrudas lo sujetaron fuertemente por detrás;
eran cuando menos dos hombres, pues se sintió paralizado, sin
poder moverse.

---Diga quién es---. Reconoció la voz de Isidoro y respiró aliviado.

---Soy yo, Francisco.

Uno de los que lo sujetaban encendió un fósforo y lo acercó a
la cara.

---Es cierto, Isidoro, es don Francisco.

---Isidoro, no vayas a hacer la burrada de matar aquí a un boche;
acabarían con ustedes y estos lugares se harían inseguros.

---No se apure, don Francisco; pero ningún boche vendría aquí
a estas horas. Y si viene el tal Melquiades o alguno de los suyos,
no la cuenta.

---Bueno, Isidoro, hay algo urgente. Se trata de que lleves un
recado a Remigio. ¿Sabes dónde está ahora?

---No lo sé, don Francisco. Yo no voy sino hasta León. Y de allí
tienen que ir a Zacatecas. Dígame lo que tengo que decir.

Francisco le dio instrucciones precisas. Que le digan que doña
Verónica está escondida, pero que es preciso sacarla de aquí,
porque se encuentra muy enferma. Que fusilaron al cura; que están
presos veinte vecinos principales y van a fusilarlos también.
Tienes que salir inmediatamente. ¿Necesitas dinero?

---Deme quince pesos para dejarle aquí a mi vieja. Yo me llevaré
unos doce reales. Si me encuentran con dinero me truenan; pero
cuando no lleva uno nada, casi ni preguntan.

---¿Cuándo vas a salir?

---Pos nada más que esté más alta la luna.

---Bueno, Isidoro; que tengas buen viaje---. Y le alargó quince
pesos en billetes y monedas de plata que contó a la luz afocada
de su lámpara.

---Oiga, don Francisco, ¿y qué estos cabrones irán a matar a
don Estanislao? Ya mataron al tata cura y eso lo pagarán. Llegará
el día en que daré tantas cuchilladas en los cuerpos de los boches
que se me cansarán los brazos. Y al de la Peña lo he de capar,
por traidor.

---Matarán a don Estanislao, Isidoro. Y esa es una de las cosas
que tiene que saber Remigio. Pero no es hora de pláticas; tengo
que regresar,

---Bueno, Don Francisco, adiós.

---Adiós, Isidoro.

Bajó Francisco al fondo del pozo, tomó el túnel de regreso; pero
al llegar a la rotonda echó a caminar por uno distinto al que
lo había traído. Por fin llegó a una gran cámara abovedada, atestada
de cajas de municiones y de dinamita. En un rincón se veían tres
ametralladoras y unos doscientos o trescientos rifles. Francisco
exploró con su lámpara y enfocó la boca de un colector de avenamiento
abandonado. Por allí escurrióse a gatas hasta topar con un gran
cajón vacío, apartólo y el haz de luz de la linterna recorrió
los muros de un cuartucho pequeño; en un rincón arrancaba una
escalerilla de caracol, ancha apenas de dos pies, por la cual
subió. Al final de ella levantó una trampa que daba a un cuarto
lleno de trebejos, pedazos de muebles, paja, fierros inservibles,
puertas despedazadas, vidrios rotos. Francisco buscó su camino
a través de esos desechos y salió a un pasillo que lo condujo
al patio. '

Estaba en la casa de los Condes de la Canal. No había nadie.
Abrió la gran puerta tallada y echó a andar a la fábrica.

Las crónicas de San Miguel consignaron las brutales ejecuciones
de esos días. Primeramente la de Don Estanislao Gutiérrez, con
tres compañeros de infortunio. Más tarde tocóle su turno a Nicanor
Aceves que murió profetizando la ruina de Alemania y el fin violento
de Hitler. Y así fue realizándose el macabro programa anunciado
por Körner.

El pueblo estaba con los nervios destrozados casi en colapso
por el choque traumático. Los hombres acudían a sus ocupaciones
habituales con la mirada baja, los obreros laboraban ante sus
máquinas con un turbo fulgor en los ojos. Cuando alguien veía
aproximarse por la calle a un boche entraba a cualquiera de los
zaguanes para no tener que levantar los brazos. A su vez la soldadesca
nazi evitaba salir; creían siempre que iban a ser cazados desde
una torre.

Tres días después de los últimos fusilamientos un ronroneo de
motor dejóse oír a lo lejos y fue amplificándose poco a poco
hasta llenar el espacio; cuando el aparato estuvo sobre el pueblo
vióse claramente que era aliado. Los alemanes que disponían de
dos cañones antiaéreos, uno en el cerro de la Bolita y otro por
el rumbo de Mexiquito, comenzaron a disparar sin tregua; escuchábanse
las explosiones al tiempo que los proyectiles se convertían en
motitas de humo sobre el cielo azul. Mientras tanto los curiosos
subieron a las azoteas y salieron a los balcones y a las calles.

Después de dos vueltas amplias rozando casi las colinas que circundan
San Miguel, el avión voló sobre la plaza, a poca altura. Las
gentes anhelantes vieron cómo de la cabina se desprendían nubes
policromas que lentamente se transformaban en millares y millares
de papeles multicolores, pequeños como confetis. Después esos
confetis se extendieron sobre el firmamento y cuando el avión
desapareció en el horizonte se fueron agrandando hasta transformarse
en volantes cuadrados que se mecían caprichosamente y llegaban
con suavidad a las azoteas o a los balcones, o se posaban en
las copas de los árboles y en los hilos de la luz como parvadas
de pájaros. Otros, en número incontable, cayeron hasta el arroyo
o se colaban por los escasos zaguanes entreabiertos arrastrados
por los chiflones.

La expectación pudo más que el temor y manos furtivas recogían
esos papeles y los introducían a las casas. Otros, para obtenerlos,
subían a las azoteas o abrían las ventanas. En esos volantes,
rojos, rosados, amarillos, verdes, violetas, se leía, sobre una
pálida V que abarcaba toda la hoja, lo siguiente:

«Mexicanos: Las fuerzas de este país, junto con las fuerzas americanas,
han recuperado Monterrey. Podemos decir que esta recuperación
es definitiva. Las guerrillas insurgentes y los francotiradores
prepararon esta operación venturosa y cooperaron brillantemente
a su feliz ejecución. Radiaremos detalles; que aquellos que nos
escuchen transmitan a los demás las buenas nuevas».

Al duelo de las últimas semanas se sustituyó una elación inmoderada.
Los vecinos querían salir llenos de regocijo, pero los soldados
alemanes en grupos de tres o cuatro con los rifles por delante
y con orden de disparar, las recorrían de un extremo a otro.

Sin embargo, un río oculto de optimismo se deslizaba de casa
en casa y se transmitía en los saludos rutinarios. Las iglesias
se llenaron de fieles y las oraciones tenían un tono gozoso de
aleluya.

La soldadesca continuó vigilando el pueblo durante ese día y
la noche, pero a la mañana siguiente, sobre los blancos arcos
invertidos que bardean el convento de las monjas, podía leerse,
en grandes letras, rodeada de un marco de esos volantes venidos
del cielo, la siguiente quintilla.

¿Has visto tantos papeles? \
Guárdalos en el retrete; \
los vas a necesitar \
con lo que vas a zurrar \
al llegar los insurgentes. {.poesia}

Körner hizo catear los lugares en donde, con mayores probabilidades,
podían existir receptores de radio. Dio con dos, pero era difícil
hacer mil quinientas requisas en otras tantas casas; así pues
las noticias del campo aliado llegaron a los atribulados sanmigueleños.

Al correr de los días, y pasando de boca en boca, las versiones
perdieron su austera exactitud hasta hacerse fantásticas. El
hecho incontrovertible fue que Monterrey cayó definitivamente
en manos de las naciones unidas; en ese ataque los aliados emplearon
por primera vez un aparato extraño que bajaba verticalmente del
cielo como un gran molino que agita las aspas y que vomita al
aterrizar decenas de soldados con toda clase de equipo ligero.

Habían pasado unas cuatro semanas de la entrevista entre Francisco
Toledo e Isidoro Reyna, cuando una madrugada, como a las tres
y media, Remigio llegó a la casa del último. Isidoro tenía el
sueño del coyote, de manera que al oír pasos despertóse y cuando
se escuchó la voz queda de Remigio ya estaba de pie abriendo
la puerta.

El jacal de los hermanos Reyna se levanta sobre la falda de los
cerros que bajan desde la carretera de Querétaro hasta el Callejón
del Arrastre, entre riscos abruptos y peñascales llenos de huizaches.

Los nazis sabían que había allí unas cuantas chozas de campesinos
misérrimos, mas no se aventuraban por esos suburbios temerosos
de una emboscada, aunque militarmente no les concedían la menor
importancia.

---¿De dónde viene, Jefe? ---preguntó Isidoro. Y en seguida,
sin esperar la respuesta: --- Todos lo aguardan con ansia.

---Vengo del Norte; dejé el caballo frente a Atotonilco, a la
orilla de la presa; allí tenemos buenos amigos. Pasé el día de
ayer con Chon Reyes. ¿Te acuerdas? Hasta que se hizo de noche
y la emprendí a pie.

---¿Y por qué llega tan solo?

---Porque ahora no puedo arriesgar nada. Varios muchachos van
a reconcentrarse por Atotonilco y me van a esperar.

---¿Le doy café, Jefe?

---Sí, dámelo. Tengo tiempo antes de que empiece la alborada.
Y cuéntame las cosas de acá. Todas tristes ¿no es cierto? Primero
dime de mi madre. ¿Dónde está?

---Don Remigio, solo sé que la tienen en un lugar seguro; creo
que se aposenta en la cueva grande de la parroquia. Don Francisco,
el de la fábrica, me ha dicho que está muy enferma y se lo oí
también a Vicenta, la mujer de Praxedes. Pero ya usted la va
a ver.

Las noticias son dealtiro malas. Afigúrese, Jefe, que estos tales
mataron a don Estanislao, a don Nicanor, y a Pancho. ¿Para qué
le digo más? Ora, que la gente está contenta desde que vino el
arioplano. Echó un titipuchal de papelitos y todos creemos qu'ora
sí ya nos toca la de ganar…

Y así siguió su charla, contando, a su modo, las tristezas de
las viudas y los huérfanos, el horror de los fusilamientos, los
rumores de la victoria…

---Dicen que el boche que está allí en el convento se trae una
cagalera que no le para; y dicen que el traidor de Melquiades
no puede tragar porque los huevos ya le llegan al pescuezo.

---Gracias, Isidoro; nos veremos en la noche.

Era ya de día; Remigio bajó por el pozo y se internó por la red
de túneles, hasta llegar a una puerta ancha que daba a la gran
bóveda, bajo la parroquia. Con el corazón encogido la fue abriendo
lentamente.

El recinto estaba mal iluminado con quinqués de petróleo y allí,
en el fondo, acurrucadita en un sillón, estaba Verónica. Remigio
se abalanzó a abrazarla, se arrodilló ante ella y comenzó a besarle
los mechones casi blancos, las mejillas chupadas, los ojos inexpresivos
y hundidos, la tez arrugada, la frente donde el dolor había labrado
surcos hondos. Remigio no la veía; a la luz vacilante la acariciaba
toda con los labios y una tristeza sin nombre le desgarraba las
entrañas. No la veía; pero se preguntaba: ---¿Es esta mi madre?
¿Es mi madre esta ancianita decrépita, que parece tener cien
años? ¿Es Verónica, mi madre, este cuerpecito exhausto?

Y al recordar a la mujer de apenas hacía unos meses que los protegía
a todos con su ternura y su fortaleza, estalló en sollozos, hundiendo
la cabeza en el regazo; unos sollozos hondos, que lo sacudían
convulsivamente.

Verónica, mientras tanto, seguía con la mirada vaga e inexpresiva;
sus manos sin fuerza se habían posado maquinalmente sobre la
cabeza del hijo; sus dedos tenues le acariciaban los cabellos,
igual que en otro tiempo, con un ademán que brotaba de la subconsciencia
como el último resto de lo que ella fue.

El doctor Bernáldez, que contemplaba la escena desde un rincón,
aproximóse al fin y fue a colocar una mano dulcemente sobre el
hombro de Remigio. Este se incorporó y abrazó con efusión al
viejo amigo; el amigo que le recordaba tantas cosas de su padre
y del hogar destruido.

Una sombra erguida y con una barba que a la luz tenue parecía
hecha de largas hebras de seda blanca se acercó a su vez; era
don Aurelio Manterola. Remigio lo saludó también y después permaneció
silencioso.

---Hijo mío ---comenzó el doctor--- no te tengo buenas noticias;
encuentro a Verónica grave. Lo peor es que su estado escapa a
mi penetración de médico provinciano; además, ya te imaginas
que aquí carecemos de toda clase de recursos. Si estuviésemos
en un lugar civilizado lo más propio sería convocar a una junta.
Indudablemente hay un estado de caquexia, pero con complicaciones
serias; no conoce, ni recuerda nada. No hay signos de locura,
pero sus centros nerviosos están gravemente alterados. Si puedes,
Remigio, llévatela de aquí; quizá un cambio sea decisivo; ya
tú comprendes que este ambiente no es de lo más adecuado para
una enferma que ha sufrido lo que tu pobre madre.

---Doctor, a eso he venido; a ver si puedo sacarla. El intento
es peligroso, pero al verla como está comprendo que no queda
otro remedio. Yo abrigaba la esperanza de poderla dejar aquí
un poco más, pues esta situación no durará mucho; los nazis tienen
sus días contados. San Miguel volverá a ser un pueblo libre,
aunque nunca será el mismo pueblo, sobre todo para mí y muchos
otros. Pero veo que es preciso hacer algo y tengo que partir
con ella; habrá que enfrentarse con lo peor, ¿no es así, doctor?

---Es cierto, hijo.

---¿Puede andar?

---Poco, aquí lo hace ayudada por nosotros y por Remedios; la
atiende también una de las sobrinas de la nana Chole, que está
con ella día y noche. Pero no pienses en ningún esfuerzo largo.

---Bueno, doctor; fortalézcala un poco; yo pensaba hacerla caminar
esta noche, pero no es posible.

Durante el día acudió Francisco Toledo a ver a Remigio y también
dos muchachos de la fábrica. Estuvo Miguel, el herrero y Atanasio,
un hombre ya maduro que era como el cacique de los tejedores
del lugar y con gran prestigio entre la gente campesina. Iban
a recoger instrucciones, ansiosos por lanzarse a la lucha. Pero
Remigio los calmó esta ocasión, como otras.

Hicieron un recuento de armas: había en los túneles novecientos
rifles y como un millón de cartuchos. Seis ametralladoras, ciento
sesenta automáticas 45, esas sí con sobra de municiones. Los
insurgentes habían acumulado como doscientas cajas de dinamita
y cañuela en abundancia, pero Remigio anuncióles que mandaría
un explosor de bolsillo y espoletas eléctricas. Le informaron
que el lugar donde despachaba Körner estaba en la planta baja,
en un salón grande con ventanas a la calle. Finalmente recorrió
todas las vías subterráneas y volvió a la hora de almorzar a
donde estaba Verónica.

La tarde se pasó tristemente, de charla con el doctor Bernáldez
y don Aurelio. Se habló de don Rodrigo; ya Remigio conocía su
fin: había sido asesinado en un campo de concentración en la
Mixteca, en unas minas de mica. Les anunció grandes cambios;
el primer golpe serio al invasor había sido la recaptura de Monterrey.

La fuga de Remigio con Verónica, aunque llena de peligros, se
realizó sin más incidentes que la mortal fatiga de llevar casi
a cuestas esa humanidad miserable. En la madrugada salieron ambos
por el pozo de la casa de Isidro, vestidos de campesinos; encaramaron
a Verónica en un borrico sobre el cual se veía tan pequeñita
y enjuta que no inspiraba desconfianza. Remigio iba a su lado,
cuidando de que no cayera; Isidoro llevaba a la bestia del ronzal.
Al llegar a la orilla de la presa tomaron una lancha, arribaron
a Atotonilco al mediodía y allí esperaron hasta la mañana siguiente.
La única zozobra consistió en las preguntas que les hicieron
los delegados del lugar, dependientes de Melquiades de la Peña;
pero Remigio e Isidoro llevaban los pasaportes obligatorios a
todos los caminantes y los sellos, legítimos unos, falsos otros,
inspiraban un profundo respeto y les dieron la salida franca.
Esa noche durmieron cerca de Dolores Hidalgo en los llanos contiguos
por donde va el camino a Guanajuato y antes de amanecer un avión
aterrizaba en el lugar convenido. Allá subieron Verónica y Remigio
y a las dos horas de vuelo llegaban a Monterrey. Todo se había
realizado al minuto, con esa precisión que caracterizó siempre
los golpes de mano de las guerrillas y que Remigio parecía haber
aprendido de los comandos ingleses.

Verónica iba herida de muerte. Los médicos regiomontanos decidieron
enviarla al Hospital Hopkins. Los yanquis la recibieron como
a una reina enferma. Pero todo fue inútil; a los tres meses su
vida se extinguió como la de un cirio agonizante. Para no defraudar
su fe Dios creó un cielo para recibirla, con nubes color de rosa,
arcángeles alados, querubines tan lindos como Remigio, Patricio
y Ana cuando eran pequeños y un solio de nácar donde don Rodrigo
la esperaba sonriente, a la diestra del Padre.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVII

++Berrueta++ me ha explicado que las cosas comenzaron a perder
su placidez. Los nazis se habían convencido ya de que todas sus
zalamerías no bastaban para engañar a los mexicanos. Es cierto
que tenían un gobierno y que se respetaba la ficción política;
pero Cabañas y sus secuaces eran sinceramente odiados por el
pueblo y algo peor aún, despreciados. Los actos de sabotaje se
multiplicaban atacando todos los sectores de la vida. Las instrucciones
de Berrueta eran minuciosas: se dirigía en ellas a los choferes,
a los farmacéuticos, a los zapateros, a las verduleras, a los
expendedores de carne, a los constructores, a los artesanos.
En fin, no había actividad que hubiese quedado fuera de sus previsiones;
cada quien sabía la manera de entorpecer en alguna forma la vida
colectiva. Los huevos llegaban corrompidos a las mesas boches;
la carne hedía en el rancho de la tropa; las llantas caíanse
a pedazos apenas estrenadas. En todo el país el sabotaje crecía
como un monstruo implacable y los nazis se sentían asfixiados.

Entonces comenzaron las represalias. La luna de miel de los invasores
había terminado hacía mucho tiempo; arrojaron a un lado su disfraz
y enseñaron las garras. Una pana de un camión quería decir una
azotina para el chofer; una descompostura en la máquina de una
fábrica era el campo de concentración o el fusilamiento. El país
comenzó a vivir en el terror; pero eso no detenía a los saboteadores.

Berrueta seguía haciendo su vida aparente de ingeniero modesto;
iba a su banco y despachaba sus labores rutinarias en la fábrica
de Alonso Quintanar. Un día recibió la visita de Abrego, quien
desempeñaba un puesto en la Secretaría de Propaganda, la que
no era otra cosa que una Gestapo a la mexicana. Inquirió lo que
hacía y cuánto ganaba; cuáles eran sus simpatías hacia el Gobierno
de Cabañas y hacia los invasores. Berrueta contestó con indiferencia,
diciendo que para él un Gobierno ideal era el que lo dejase tranquilo;
que no tenía ambiciones y que con unos cuantos libros, vino para
sus comidas y un baño de cuando en cuando, estaba satisfecho.
Sobre esos temas bordó incesantemente, pues Abrego insistía con
maña queriendo conocer toda clase de pormenores sobre su vida.

A los pocos días se sintió vigilado. Dos hombres se apostaron
en la acera frontera a su casa, uno frente al Banco y otros dos
en la fábrica de Alonso Quintanar. Entonces tuvo que hacerse
cauto y descuidar por completo sus labores de sabotaje. Discutió
con Alonso la situación y decidieron que Berrueta saliese a Zamora,
aparentemente a ver a la clientela. Pero las cosas iban a precipitarse.

---Apenas tuve tiempo para obrar ---me cuenta ahora Héctor---.
Hacía ya varios días que vivía en la esclavitud, pues encontraba
a los esbirros en la puerta de mi casa cada vez que salía y me
acompañaban como guardias de corps.

Las cosas estaban muy tirantes; los boches se habían convencido
de que la ocupación de México no era coser y cantar; te imaginas
el revuelo que se armó con la muerte de Klee. El día que la bomba
lo convirtió en fragmentos fue de regocijo popular. Pero las
represalias se habían ya iniciado, y cada acto de sabotaje costaba
muchas vidas. Mi aparato transmisor para hacer explotar las bombas,
diminuto como una cámara fotográfica, yacía ocioso en el Banco
en un mueble de archivos inútiles.

Pero voy a seguir con mi historia. Una mañana que llegué a la
fábrica Alonso se encerró conmigo en su despacho y me dijo:

---No sabemos quién trajo esta carta anónima y la dejó sobre
mi escritorio. Léala usted.

Se anunciaba que la fábrica estaba en entredicho y sería cateada;
para aprehenderme esperaban solo recabar ciertos datos concluyentes
en mi contra.

---¿Y qué opina usted? ---le dije a Alonso.

---Pues que esta advertencia viene de alguno de nuestros amigos.
Y si no, al tiempo. Por mi pueden catearnos. No encontrarán nada.
Naturalmente que si solo buscan un pretexto nadie nos salvará.
Pero con ese riesgo se ha alquilado la casa; ya nuestras vidas
están devengadas y me llevaré la satisfacción de saber que la
invasión nazi fracasará.

Las cosas se agravaron, pues los esbirros del servicio secreto
prendieron a Timoteo Reyes, uno de los mejores obreros de Alonso;
lo llevaron a su casa, por el barrio de Santa Julia, donde hicieron
una requisa, encontrando un frasco con ácido pícrico. Inmediatamente
después, con lujo de fuerza, llegaron a la fábrica de Quintanar
y la inspeccionaron minuciosamente. Se escapó de la destrucción
de sus máquinas y existencias porque pudo demostrar que trabajaba
en un gran encargo para las Juventudes Hitlerianas de México,
manufacturando juguetes bélicos destinados a los párvulos y porque
también pudo probar que había solicitado él mismo visitas periódicas
a sus locales. Pero el pobre de Timoteo Reyes sufrió durante
varios días todas las torturas imaginables y al fin fue fusilado.

Cuando se conoció esta tragedia Alonso reunió a sus obreros y
con lágrimas en los ojos les dio cuenta de lo acaecido. Al mismo
tiempo les dijo que el sabotaje era cada vez más peligroso y
que deseaba relevarlos de su compromiso, si así lo deseaban.
Pero todos, a una voz, reiteraron su propósito de seguir en la
brecha y se discutieron medidas para evitar otra desgracia. Debo
decirte que eso no se consiguió pues otros dos muchachos corrieron
la suerte de Reyes y los que quedaron estaban a punto de desbandarse
cuando los nazis fueron expulsados. El último mes y medio del
régimen del gobierno pelele Quintanar lo pasó escondido con gente
maleante del barrio del Rastro.

Voy a acabar refiriéndote cómo pude escapar. Desde que aprehendieron
a Timoteo comencé a tomar mis precauciones, pues no dormía en
mi pieza, sino que unos vecinos de la misma vecindad me ocultaban
en su tugurio. Tú no puedes tener idea de esos caserones; yo
habitaba en uno por la parte que se llamaba el México viejo,
en la calle de Manzanares, que era famosa porque conservaba incólumne
una gloriosa tradición de mugre y de piojos. Ahora hay allí unos
parques y unas piscinas de natación, pero en esos tiempos aquellos
eran los rumbos más sórdidos de la ciudad. Me había mudado para
poder operar más libremente a una casa que tenía varios patios
y se comunicaba con otras. Cierto día toda la manzana de edificios
amaneció rodeada de sabuesos. Me advirtieron de ello para que
no saliese y fuíme a ocultar a un desván. El resultado fue que
penetraron como treinta hombres registrando vivienda por vivienda;
pero su labor fue muy imperfecta, porque se perdieron en aquel
laberinto y más de diez habitaciones ---por llamarlas de algún
modo--- escaparon al cateo. A la mía llegaron llevándose un sinfín
de papeles sin importancia; destrozaron mis muebles y se robaron
toda mi ropa.

Entonces pude comprobar la cohesión de espíritu que la guerra
había despertado por doquier. En los dieciocho días que estuve
en aquel agujero se formó una conjura para ayudarme. Yo comprendía
que mi única expectativa era reunirme con los insurgentes, pues
mi labor de sabotaje se había nulificado al obligarme a vivir
prófugo. Sin embargo, en mi escondrijo tuve la satisfacción de
comprobar que mis esfuerzos por crear un gran organismo saboteador
no habían sido vanos, pues en ese lapso, antes de mi fuga y sin
mi más ligera intervención, la refinería de petróleo que estaba
por el rumbo de Azcapotzalco se convirtió en pavesas y el túnel
de Barrientos quedó obstruído por un derrumbe. Esto amén de pequeños
destrozos que se contaban por docenas.

Por fin una noche se presentó uno de los obreros de la fábrica
de Quintanar llamado Arnulfo Villegas, con un sujeto amigo de
él, malencarado y sucio. Me envolvieron en unos sacos de yute
y me echaron encima de un camión Ford desvencijado que arrojaba
chorros de agua hirviendo por el radiador y hacía un ruido infernal;
los dejé hacer pues no me quedaba sino confiar en mi buena estrella.
Supongo que nos detuvimos en una garita porque oí voces a mi
alrededor, unas en alemán, otras en castellano y finalmente alguien
que decía: ---¡Adelante! ---Como al cabo de una hora en que
el motor jadeaba como un caballo viejo sentí que me levantaban
en vilo al mismo tiempo que escuché el susurro de la voz de Villegas:

---Buena suerte, señor Berrueta. Lo llevan a Veracruz. Todo saldrá
bien.

Después supe que me habían pasado a un camión que acarreaba efectos
para los nazis y que, por lo mismo, gozaba de salvoconducto;
el chofer se había prestado gustoso a la maniobra.

Muy entrada la noche y tras unas horas que me parecieron eternas
el camión volvió a detenerse. El chofer bajó y desembarazóme
de mi envoltura.

---Me he parado para que se estire usted un poco.

La carretera estaba desierta y hacía frío.

---¿Dónde estamos? ---le pregunté.

---Cerca de Perote ---me respondió---. Vamos a Veracruz; la ruta
por Tehuacán es mejor, pero esta es más solitaria.

Discutimos la situación decidiendo que yo podía ir a su lado,
pues era ya bien conocido por su tráfico y el pasaporte nos protegía
con amplitud. El resto del viaje fue para mi menos penoso; charlé
con el chofer; contóme que en Veracruz el régimen era excesivamente
opresivo y que varios oficiales nazis habían sido asesinados
por mulatos víctimas de un trato inhumano. Me contó también que
un tren con cuarenta vagones de café que iba de Córdoba hacia
Veracruz para embarcarse con destino a Alemania se incendió en
el camino; también que un día la playa del hotel de Mocambo,
convertido en estación de convalecientes teutones, había amanecido
sembrada con una tal cantidad de pedazos de vidrio que nadie
pudo bañarse en ese lugar por mucho tiempo; y finalmente que
habían volado los depósitos de la cervecería de Orizaba.

Yo llevaba barbas de más de una semana y por dieciocho días no
me había bañado ni lavado siquiera; pude pasar muy bien como
ayudante del conductor. Nos detuvieron en Perote y después en
Jalapa. Ya te imaginarás que pasé un miedo horrible, pero a través
de tantos años esas emociones se adormecen. Sería ridículo que
ahora tratara de impresionarte reviviendo los peligros corridos;
el menor tropiezo significaba para mí la vida, y con eso te lo
digo todo. Cuando la guardia de Jalapa revisó nuestro salvoconducto
el corazón me latía desordenadamente y cuando un sargento nazi,
cerca de Perote, enfocó sobre mi rostro su linterna eléctrica
debe haber visto el terror en mis ojos porque me examinó de arriba
abajo. Al convencerse de que llevábamos víveres destinados a
un jefe teutón del puerto se desvanecieron sus sospechas y nos
dejó pasar. Pero te aseguro que a pesar de los años transcurridos
todavía recuerdo esos sucesos como algo de lo más angustioso
que experimenté en mi existencia de entonces. El relato podría
ser emocionante si me hubiesen apresado, sometido a las torturas
de la Gestapo y fusilado al fin; pero aquí me tienes vivito y
coleando en este naciente mundo de nuevo. El hecho es que poco
antes de llegar a Veracruz dos hombres a caballo nos detuvieron
y hablaron algunas palabras con el chofer; este entonces hízome
bajar diciéndome que me fuera con esa gente, que eso estaba dentro
del plan y que podía seguirlos tranquilo. Me hicieron ir tras
ellos y como a unos cien metros de la carretera, detrás de unas
malezas, tenían otro caballo en el cual monté. Después de caminar
al paso unas dos horas llegamos a una choza, la meta de nuestro
pequeño recorrido, donde comimos espléndidamente mariscos y frijoles
negros. Se dominaba desde allí un paisaje tropical de cocoteros,
una playa llena de guijarros plateados y una rada azul rayada
con la espuma blanca de las olas. Recostados sobre la arena,
como grandes cetáceos que duermen la siesta, vi dos barcas de
pescadores.

Permanecí con esos jarochos una semana sintiéndome por primera
vez tranquilo, después de tantos meses de lucha. Entonces díme
cuenta de que mis nervios estaban deshechos y de que ese reposo
me era indispensable; me bañaba en el mar y dormía casi el día
entero. Pero todavía era necesario correr otra aventura para
llevarme a suelo ocupado por las guerrillas.

No deseo hacerte más larga esta relación; me llevaron costeando
no sé cuántos días en una de aquellas barcas hasta dejarme cerca
de Matamoros, en el estado de Tamaulipas, y aunque siempre he
tenido un apego muy humano a la existencia le pedí muchas veces
a Dios, durante esa travesía, que nos torpedeara un submarino
del Eje, pues me mareé desde que nos internamos un poco en alta
mar y me convertí en un fardo inanimado hasta el momento en que
me levantaron en peso y me depositaron sobre la orilla caldeada,
a la altura de Soto la Marina.

Berrueta fue conducido a Brownsville y allí conferenció con jefes
americanos y mexicanos que pretendían incorporarlo a sus fuerzas;
pero él se opuso. Lo que deseaba era hablar con Remigio Guerrero
quien andaba entonces organizando guerrillas por el estado de
Sonora; por fin se vieron, charlaron y Berrueta se internó junto
con el líder insurgente en territorio enemigo.

Contó desde entonces con elementos de sabotaje que le llegaban
de los Estados Unidos y que él enviaba por toda clase de comunicaciones
subterráneas hasta el centro del país. La distribución de panfletos
se multiplicó fantásticamente, pues las imprentas del lado americano
hacían ediciones de cientos de miles que los aviones de guerra
dejaban caer en los centros más poblados. En ellos se predicaba
una guerra de exterminio contra los nazis indicando con precisión
cómo había que llevarla a cabo.

De entonces data el enlace intimo de Berrueta con John McKim.
Sobre este personaje, que todos los mexicanos tenemos ahora en
el corazón, se han hecho muchas leyendas. Descendía de escoceses
y había nacido en Boston. Cuando vino el enlistamiento a pesar
de su terquedad fue rechazado por su miopía; usaba unas gafas
de vidrios muy gruesos que le deformaban los ojos. Lo único que
consiguió fue un puesto en una fábrica de aviones, en calidad
de calculista, pues un tío abuelo suyo había sido arquitecto
y casi por herencia lo era él también.

El hecho es que obtuvo permiso para acompañar a Berrueta al territorio
invadido. Su labor se hizo peligrosa en extremo, pues desconocía
el idioma del país y su cabello rubio lo delataba. Organizó por
su cuenta una cofradía de francotiradores que operaron dentro
de las ciudades cazando soldados boches. Esto lo hacía en conexión
con Remigio, pero llegó a convertirse en un ente fantástico;
la imaginación popular lo vistió con los ropajes más fabulosos
y se referían de él historias increíbles.

Lo que es cierto también es que creó una estructura para sacar
de los lugares invadidos a los niños que habían quedado sin amparo
y que vagaban muriéndose de hambre. Pudo llevar muchos miles
de ellos a los Estados Unidos donde se les cuidó hasta que pudieron
reintegrarse a su patria. Este servicio le conquistó la gratitud
de las gentes de México, y cuando los nazis lo fusilaron, casi
al final de la contienda, vimos en él a un héroe nacional.

Dice Berrueta que fue uno de los hombres más extraordinarios
que le tocó en suerte conocer y que si el destino no hubiese
tronchado prematuramente esa existencia habría sido indispensable
en la organización de nuestro mundo nuevo.

Lo que sí tengo presente porque ya era yo un muchacho grande,
es que hace unos once o doce años, allá por el cincuenta, la
madre de John McKim, pidió que los restos de su hijo fuesen trasladados
a su tierra natal. Los quería reunir a los de su padre, que reposaban
en un cementerio de Boston. Entonces se le hizo a esta viejecita
un ruego para que permitiera que John se quedara para siempre
en México; ese documento no lo redactaron las embajadas, sino
que salió de un grupo de pacíficos ciudadanos que se juntaron
cuando ya se daban los primeros pasos para una exhumación solemne.
Abrieron una oficina con el fin de recoger firmas; la afluencia
de gentes fue tal que se hizo necesario establecer varias en
la metrópoli y otras en distintas ciudades y a la postre se suscribieron
aquiescencias hasta en los poblados.

Ese pliego suplicatorio le llegó a la madre de John McKim con
millones de nombres; dicen que lloró al recibirlo como si su
hijo acabara de morir y contestó:

---«Well, I guess poor Johnny´s bones don´t belong to me anymore;
they will always belong to Mexico».

De allí nació la idea de levantar un monumento a la memoria de
este muchacho y abrióse para ello una colecta pública. Es el
que ahora vemos en el bosque de Chapultepec, bajo unos ahuehuetes
centenarios. Su inauguración ha sido uno de los espectáculos
más genuinos de fervor popular al que me haya tocado en suerte
asistir.

Estos renglones que son un homenaje a John McKim los he intercalado
deliberadamente en mi relato en espera de que un biógrafo escriba
su vida.

Pero tengo que volver a Berrueta. Comenzó para él una existencia
nueva. Se hizo maestro en el arte del disfraz y se introducía
a las ciudades ocupadas. Fue sucesivamente dentista, agente viajero,
notario, vendedor de falluca, agricultor, cura. Estableció en
Ciudad Juárez un taller del cual salían por centenas pasaportes
nazis y papeles falsos para viajar por el territorio invadido.

En todo el país se difundió con la rapidez de una manga de langosta
un lema que decía con crudeza: «Mexicano, antes de morir mata
dos nazis». Entonces acontecieron cosas épicas: del sanatorio
de tuberculosos situado en Huipulco se escaparon diez enfermos
desahuciados. Uno de ellos se hizo volar con una bomba en medio
de un grupo de boches; los otros cumplieron meticulosamente su
tarea y antes de matarse despacharon a mejor vida a dos invasores.
Los que con estos actos se convertían en héroes preferían casi
siempre el suicidio a sufrir las torturas de la prisión.

Las represalias eran tremendas; pero los actos de heroísmo se
repetían sin cesar y los asesinatos que los alemanes cometían
en grupos de inocentes no hacían sino enardecer a los que, poseídos
de una fe mística, ceñíanse voluntariamente la corona del martirio.
En Guadalajara, cuando llevaban a fusilar a veinte rehenes, uno
de estos, muchacho fornido de treinta años, arrebató el rifle
a uno de los que formaban la patrulla de ejecución y mató a tres
soldados antes de caer acribillado. Los otros reos aprovechando
el desorden del momento tomaron parte en la refriega y aunque
a la postre los mataron a todos, el saldo para los nazis fue
muy duro, pues perdieron nueve de sus hombres.

Fue entonces también cuando un grupo de francotiradores, encabezados
por John McKim en persona, asaltaron el campo de concentración
número dos, escondido en la sierra de Puebla; acabaron con los
boches que lo custodiaban, y pusieron libres a más de quinientas
mujeres que allí sufrían un cautiverio durísimo. Esta aventura
anda por allí en un romance popular que he oído a un bardo callejero
en la feria de Lagos, acompañándose de una guitarra. Pero es
indudable que todas estas hazañas son dignas de una canción de
gesta.

Berrueta me cuenta todavía con elación el hecho que coronó sus
hazañas de sabotaje.

---Todo fue obra de la casualidad, o si quieres fue un rayo de
intuición; el hecho es que puso a los invasores en nuestras manos.
Estos radiaban sus instrucciones en mensajes cifrados cuya clave
no habíamos logrado desentrañar; teníamos un cuerpo de criptógrafos
estudiando las órdenes alemanas que por radio cruzaban diariamente
el aire de un ámbito a otro del país, pero nuestros esfuerzos
habían sido inútiles. Presentíamos que las actividades contra
las guerrillas se regían por esa clave misteriosa que estaba
fuera de nuestro alcance.

Yo había sido un poco aficionado a la criptografía y desde que
siendo un mocoso leía a Julio Verne conocía el sistema de rejillas.
Después, en mis ratos de ocio, penetré un poco más en esta ciencia,
si le quieres llamar así.

Estaba yo en León ---continúa diciendo Berrueta---; hacía en
esos momentos el papel de experto en arboricultura. En realidad
llevé un cargamento de ampolletas para sabotear las siembras.
Trabajaba en una huerta de los alrededores cuando se presentó
un individuo pidiendo verme; tenía un aspecto de homosexual con
su cabeza embadurnada de grasa y un olor de perfume barato. Me
le quedé viendo con un aire severo y disgustado en el que se
adivinaba a las claras mi repugnancia, pero en cuanto estuvimos
solos cambió su actitud y me encontré ante mí con un hombre que
trascendía virilidad en sus gestos y ademanes. Entonces me dijo
la contraseña que Remigio y yo usábamos en nuestras comunicaciones:
---«No se le puede pedir más al cristal».

---Hable usted ---le ordené.

---Pues dice Remigio que tiene algo importante y necesita verlo.

---¿Dónde está?

---Se encuentra en Ocotlán, en Jalisco. El preferiría que se
regresase usted conmigo.

---Está bien ---le dije---, voy a arreglar mis cosas y nos iremos
en la tarde.

Sabía que Remigio solo me llamaba por causas urgentes.

A la otra mañana, a las doce, arribamos a Ocotlán. La guarnición
nazi era escasa; pero estaba siendo reforzada porque conocían
la presencia de guerrilleros en los alrededores. Sin embargo,
me fue fácil llegar hasta Remigio que se encontraba como a ocho
kilómetros de la población con varios cientos de hombres cuyo
jefe era un mestizo, de ojos verdes, oriundo de los Altos.

Nos abrazamos al vernos y cuando estuvimos solos me dijo:

---Hubo un encuentro antier; matamos a varios boches y he aquí
lo que le quité a uno de ellos.

Se acercó a su maleta, extrajo de ella una aparato y me lo alargó.
Era una pequeña máquina con teclas.

---¿Qué es esto? ---me digo Remigio, y en seguida, sonriendo:
---¿no iremos a volar con ella? Berrueta, no le vayan a dar a
usted un machetazo a caballo de espadas.

Pero yo ya sabía a qué atenerme ---sigue refiriéndome Héctor
Berrueta---. Era una máquina de tipo Hagelin para descifrar mensajes
de las que se manufacturaban en Suecia en aquellos tiempos. La
examiné con cuidado, me cercioré de que funcionaba bien y le
dije a Remigio:

----Esto es únicamente una de las patas de la mesa. Sin la frase
clave nada podemos hacer y a veces esa clave es una larga sucesión
de letras que no tienen ningún sentido. Pueden ser cincuenta,
o cien o más. Me temo que no hayamos adelantado mucho. ¿Registraron
bien al boche?

---No le encontramos nada ---dijo.

---Pues no queda otro remedio sino seguir mandando los mensajes
a los criptógrafos de Washington ---repliqué---. Allá ellos podrán
estudiar la frecuencia de las letras y hacer miles de ensayos
que para nosotros resultan imposibles.

Me quedé pensando en la inutilidad de mi viaje, pero me propuse
estudiar el caso. Comimos y mientras tomábamos el café Remigio
me anunció que él salía inmediatamente rumbo al Norte.

---Siento haberlo molestado ---agregó---. Juzgué importante
que usted viese ese mecanismo.

En la tarde, mientras limpiaba el aparatito, vi en letras rayadas
con una navaja sobre la pintura de esmalte negro esas dos palabras
que pasarán a la historia como una fórmula de maldición: «Heil
Hitler». De pronto un idea me atravesó el cerebro. ---¿Serán
estos tan idiotas y tan vanidosos ---me pregunté--- que esos
dos vocablos constituyan la clave o cuando menos parte de la
clave?--- Me levanté agitado del asiento donde me encontraba
y fui a despertar a uno de los capitanes guerrilleros que dormía
a esa hora.

---Capitán ---le dije--- ¿tiene usted alguno de los últimos mensajes
radiados por los boches?

---Aquí está uno de antier ---me contestó y entregóme un papel
con una secuencia de signos disparatados e incoherentes. Me puse
a trabajar con la máquina usando, una a una, las letras de «Heil
Hitler». Al mismo tiempo pensaba que, de tener la loca suerte
de que mi suposición fuera exacta, probablemente aparecerían
términos en alemán; yo no conocía el idioma y no disponía ni
de un mal diccionario. Cuál no sería mi sorpresa al ver que,
letra por letra, se comenzaba a leer: e-s-p-e-r-e-n-e-n-e. ¡Esperen
en e! Eso era todo; había acertado en el principio, mas de nada
serviría si no conocía el resto de la clave. Entonces apliqué
nuevamente las mismas dos palabras, lo cual era pueril, pues
resultaba claro que la cifra no podía limitarse a ellas y las
conclusiones fueron negativas. Sin embargo, con cierta dosis
de paciencia y ensayando de un signo a otro la fracción de mi
clave, pude averiguar el ciclo de su recurrencia. Esto me desanimó
no poco, pues completa constaba de más de ciento cincuenta cifras.

Puse la maquinilla en mi valija y emprendí el camino de regreso.
Fui meditando y al llegar tenía mi teoría hecha. El resto de
esta contracifra está en _Mein Kampf_ ---me dije---. Entonces
me eché a buscar por todo León hasta que di con una edición alemana
de este papasal y comencé mis ensayos. Trabajé ocho días con
una perseverancia de benedictino ---me dice Héctor Berrueta---.
Ensayé todo lo imaginable siguiendo el método que me pareció
más lógico y era tomar del libro las frases y párrafos que el
autor había escrito en bastardilla y que, por lo mismo, consideraba
medulares. Caí por fin en el muy conocido lema boche: «¡Deutschland
über alles in der Welt!» Me dije que cómo no lo había pensado
antes. Ya te imaginarás ---continuó Berrueta--- cuál sería mi
excitación al ver que las trasposiciones con la máquina me daban:
s-e-l-u-g-a-r-h-a-s-t-a-q-u-e-l-l-e-g-u-e-c-a-r-r-o-f-n-c. Es
decir, podía ya leer de corrido: esperen en ese lugar hasta que
llegue carro F. N. C…

Pero esa no era toda la clave. Bueno, Fernando, esto sería el
cuento de nunca acabar. El resto de esa contracifra, que es una
invocación, la encontré por fin en _Mein Kampf_ y se ha grabado
con caracteres de fuego en mi memoria: «Dios Todopoderoso, bendice
nuestras armas; sé justo como siempre lo fuiste; juzga si merecemos
la libertad; Señor, bendice nuestra lucha». Entonces pude descifrar
el mensaje completo; anunciaba un envío de municiones y armas
a Celaya.

Fuíme inmediatamente en busca de Remigio y le participé mi descubrimiento.

Desde entonces caíamos sobre los convoyes de pertrechos, las
escoltas y los trenes cargados de víveres. Dábamos noticias a
los yanquis de los planes del enemigo; los Estados Mayores se
preguntaban cuáles eran nuestros medios de información. El daño
que causamos a los alemanes fue incalculable.

Cuando Berrueta hubo terminado su relato lo interpelé a mi vez:

---Bueno, ingeniero, las cosas parecen muy sencillas tal como
usted las refiere. ¿Pero cómo dio con la idea que lo llevó a
ese resultado? No encuentro la explicación.

Héctor Berrueta se estiró, sentado frente a su escritorio, con
las manos cruzadas en la nuca. Era un día frío y charlábamos
frente a una chimenea encendida. Me dijo con lentitud:

---Mira, Fernando, fue un pequeño problema de psicología práctica.
Todo mi análisis se basó en cómo debería obrar un hombre fanatizado
por el servilismo; sus móviles de acción tenían que girar forzosamente
alrededor del amo todopoderoso que ordenaba desde Alemania. La
luz me la dio el oficial que en su maquinilla descifradora rayó
las dos primeras palabras de la clave: «Heil Hitler».

Durante ese período la vida de Berrueta se confundió con la lucha
de los guerrilleros. El sabotaje se acrecentó con tal intensidad
que el territorio invadido vivía en el caos.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVIII

++Una++ noche, cuando Patricio llegó a su casa, cerca de las
doce, encontró a Eligia llorando.

La escena se iba haciendo tan habitual que no había nada que
decirse. Las dificultades económicas no existían, y, por el contrario,
Patricio había impuesto su deseo cambiándose de casa y vivían
ahora en un apartamiento casi lujoso del Paseo de la Reforma;
tenían criados y un auto.

Sin embargo, no eran felices. Patricio se había tornado irritable;
alzaba la voz, decía palabras altisonantes y hablaba siempre
de que él se sacrificaba sin encontrar la simpatía de su mujer.
Vivían aislados, pues frecuentaban solo las tertulias de las
señoras alemanas (quienes tenían la consigna de congraciarse
con las familias del país) o las fiestas que organizaban los
funcionarios del gobierno pelele. Eligia encontraba allí a la
esposa del licenciado Murillo, una rubia oxigenada y gorda que
se sentía a sus anchas en ese ambiente, o a la señora Vázquez
que leía novelas y soñaba con tener un salón. Esas relaciones
no podían satisfacer a Eligia, tan seria e introversa, quien
solo tenía pensamientos para Patricio y para su hijo. En cambio
habían perdido a los viejos amigos de la casa; a Maclovio Otamendi
y Alonso Quintanar. Este último dejó de visitarlos a raíz de
la ocupación alemana, o mejor dicho, desde que Patricio fue un
personaje ligado al régimen nuevo. Maclovio continuó frecuentándolos
por algún tiempo; pero un día surgió una discusión agria. Es
cierto que las conversaciones de Maclovio eran siempre exaltadas:
Eligia lo tenía muy presente cuando casi reñía con Don Rodrigo.
Pero en México ---quizá con menos aspavientos--- la cosa fue
muy otra. Maclovio había cenado con ellos y hablado de temas
sin trascendencia; a la hora del café, en una estancia espaciosa
vecina al comedor, Maclovio sacó un pequeño libro del bolsillo
y comenzó a explicar que allí tenía una prueba de la estulticia
nazi. Patricio frunció el ceño sin responder.

---Esto que ves aquí es una colección de cantos infantiles. ¡Lástima
de tan buen papel, de tan excelente impresión! Están destinados
a los _kindergarten_ del nuevo gobierno; debes intervenir porque
tú vas a formar a la futura juventud de tu patria y cuando los
párvulos lleguen a tus manos, después de haber asimilado estas
indecencias, los vas a recibir con los espíritus torcidos.

Al mismo tiempo le alargaba el panfleto a Patricio, quien lo
abrió, estuvo leyendo algunas páginas y se lo devolvió diciendo:

---No veo aquí nada que deba alarmarlo.

---¡Cómo! ---saltó Maclovio--- ¿no ves el alcance de esta inmundicia?
Ya los niños no juegan a Doña Blanca, ni se duermen escuchando
la carita de luna de Señora Santa Ana. Ahora tienes aquí, editadas
a todo lujo, con su música al lado, canciones que hablan solo
de cañones relucientes, de ametralladoras para matar a nuestros
enemigos y de submarinos emperadores del océano. Y fíjate en
esto que es la confesión del crimen: «Baladas ---les llaman baladas---
destinadas a los niños de tres a siete años». ¡Por Dios, Patricio!
Esto sería grotesco si no formase parte de un plan criminal.

Maclovio, con sus ojos fulgurantes tras las gafas, con los mechones
sobre el rostro, agitaba el libraco con toda la amplitud de sus
brazos.

Patricio contestó en un tono iracundo, hablando de la yancofilia
de unos cuantos y Maclovio dijo que a Patricio lo enfurecía su
censura porque su conciencia no estaba en paz. Patricio se encendió
entonces en cólera y Maclovio abandonó la casa dando un portazo.

Esto fue antes de la prisión y muerte del licenciado Guerrero
y desde esa noche no volvieron a ver al viejo amigo, quien murió
tragado por el torbellino de la guerra. Eligia se sintió aún
más sola; a veces recibía cartas de su madre, pero empezaron
a llegarle terriblemente mutiladas por la censura, y es que doña
Concha era enemiga de los nazis e imprudente para escribir. En
cierta ocasión una de esas cartas contenía una advertencia amenazante
de los censores y desde entonces no volvió a tener noticias de
San Miguel.

El carácter de Patricio se volvió taciturno; nada lo alegraba.
Al principio el orgullo del trabajo lo hizo locuaz y hasta jactancioso;
le llevaba fotografías y recortes de periódicos. Pero a medida
que el tiempo transcurría y la lucha entre los invasores y sus
enemigos se iba enconando, hablaba menos. Las visitas a San Miguel
lo ponían de un humor detestable.

Patricio, tan honesto consigo mismo, comenzó a sentir que las
«Juventudes Hitlerianas de México» tenían una marca militarista
que chocaba con el espíritu libre de sus _boy scouts_. Allá, en
sus tiempos de líder sin recursos, cuando no disponía de otro
tesoro que su generosidad, nunca pensó hacer de sus scouts una
escuela de entrenamiento militar y admitía apenas que fuesen
soldados por accidente, para reprimir una injusticia o defender
un derecho; pero el militarismo como esencia le era repugnante.

El había deseado realizar en sus _boy scouts_ la vida íntegra;
que cada quien hiciese un universo de su yo. Recordaba al barbudo
Walt Whitman: «One´s self I sing --- a simple, separate Person»
«I know I am deathless; I know this orbit of mine cannot be swept
by the carpenter´s compass». ¿Pero podría el hombre llegar al
día de su muerte con una vida realizada cuando se quiere reducirlo
al evangelio de _Mein Kampf_?

La tutela de Müller comenzó a irritarlo. Esa vigilancia impersonal
y solícita alteraba sus nervios. Hubiera querido verlo con las
flaquezas y exaltaciones de un hombre: colérico, desfallecido,
generoso, optimista o desesperado. Pero su actitud de placidez
inalterable no era humana. Aunque sin reproches que hacerle no
sentía en él un amigo. Visitaba su casa y les hablaba de Hitler,
a él y a Eligia, con un arrobamiento de sectario. Al mismo tiempo
les hurgaba el alma, como queriendo descubrir sus secretos; era
afable, dicharachero, contaba anécdotas, preparaba cocteles y
bebía, pero sin excederse. Llegaba temprano a la oficina, sin
hacer sentir nunca su autoridad. Jamás supo Patricio cuál era
su jerarquía, ni cuáles sus consignas; no daba órdenes, a menos
que Patricio se lo indicase, pero nada se le escapaba; conocía
todos los detalles del trabajo.

Después de algunos _high-balls_ contaba chascarrillos acerca de
Roosevelt, Churchill o Stalin y siempre hablaba del presidente
Onésimo Cabañas con el desprecio más grande, ridiculizándolo
sin piedad. Pero jamás se permitía la menor broma respecto a
sus superiores. Patricio y Eligia no lo mencionaban cuando estaban
solos; era entre ellos un acuerdo tácito no hablar nunca de aquel
hombre.

En su primera visita a San Miguel, Patricio llevaba ya el espíritu
ensombrecido. Había recibido dos o tres órdenes que le era duro
cumplir; mas al mismo tiempo su lealtad para aquellos que le
habían permitido ver realizadas ante sus ojos tantas cosas hermosas,
le hacía ser obediente. ¿Para qué obligar a todos los jefes de
patrullas a leer en las excursiones esa historia de Alemania,
desde Bismark a Hitler? ¿Qué tenían que ver sus muchachos con
esas cosas que estaban tan fuera de su mundo?

Cuando podía disponer de unas horas y salía él mismo al campo
con algún grupo, les hablaba de la patria futura. Les mencionaba
el pasado como una cosa triste, como una experiencia desdichada,
enfocando su optimismo hacia el porvenir; quería un México sano
y laborioso. Creía llegada definitivamente la hora del triunfo
y el fomento de odios parecíale estéril.

En esos paseos se relajaba
la disciplina militar y los muchachos correteaban a su alrededor,
o caminaban por los senderos en grupos irregulares o se encaramaban
a los cerros; en lugar de hacerles conferencias sobre el arte
moderno de la guerra, les relataba episodios de la conquista
de México, o si sus grupos eran de pequeños sacaba de la faltriquera
«La Isla del Tesoro» y la leía, a pesar de ser una maldita obra
inglesa.

Pero esto era ya muy de tarde en tarde; sus obligaciones burocráticas
lo absorbían: estar a las ocho de la mañana en la oficina, dictar
circulares, llamar a los proveedores, conferenciar con los jefes
de zona, dar instrucciones precisas sobre los cursos de Geografía.

Esto último tenía cosas que aceptaba solo por espíritu de disciplina.
El habría preferido las nociones escuetas de su niñez: «una isla
es una extensión de tierra rodeada de agua; el cerro es una elevación
aislada del terreno y de menor altura que una montaña; un río
es una corriente de agua que va a desembocar a otra, a un lago
o al mar». Todo esto era ciertamente seco, pero exacto y no aportaba
malsanas asociaciones de ideas. Cuando él fue el supremo pedagogo
de sus patrullas les había hecho comprender lo que era un cerro
trepándolo. Pero ahora había que sugerir la posibilidad de colocar
en él una pieza para arrasar al pueblo tendido en sus faldas.
¡Y recordaba a San Miguel!

Lo había lastimado también la difusión, en cientos de miles de
ejemplares, de un libro de un tal doctor Stellrech. Los nazis
lo tradujeron en un español reseco y preciso. Se titulaba «Educación
militar de las Juventudes Hitlerianas de México». En la soledad
de su estudio, en las noches, leía las frases candentes: «nacemos
y nos educamos para ser soldados» y esta otra que le carcomía
el cerebro: «¿Saber es poder? ¡Qué va! El poder está en el cañón
de un arma».

Esto le parecía escrito por un gangster o por un diputado de
los gobiernos anteriores.

Sin embargo, seguía trabajando sin descanso. Para no reflexionar
trabajaba; para no recordar, trabajaba. El trabajo era su droga,
su morfina. En la calle corrían los rumores, circulaban periódicos
clandestinos. Una mañana, al llegar a su despacho, encontró uno
sobre su escritorio; estaba tirado en mimeógrafo. El encabezado
decía en letras mayúsculas: «Los insurgentes hicieron un raid
sobre Durango». Relataba cómo se habían hecho setecientos prisioneros
nazis y destruido la explotación del Cerro del Mercado. Patricio
tomó la hoja y la leyó toda, sin omitir una sola noticia, ni
una crítica, ni una sátira contra el invasor. El nombre de Remigio
Guerrero venía citado varias veces.

Pero él procuraba no pensar en nada: salía a conferenciar con
el doctor Belgrano o era recibido en audiencia por el presidente
Cabañas. Una que otra vez fue llamado por el Barón von Virchow;
entonces lo informaba ampliamente.

Elsa seguía dándole sus clases de alemán. Se reunían en un pequeño
salón privado, donde había sillones cómodos y una mesa. No podía
hablar gran cosa, pero ya traducía de algunos libros pasablemente;
comenzaba a leer _Mein Kampf_. Elsa era una muchacha eficiente
y cumplida; rubia como una Walkiria; Patricio contemplaba sus
piernas desnudas ---pues ninguna mujer llevaba medias: las fibras
iban a las fábricas de paracaídas---. Eran blancas, lampiñas;
vellos casi invisibles ponían sobre la piel diminutos puntos
dorados. Llegaba siempre vestida de traje sastre, con una camisa
hombruna cuyo escote recogía con una suástica de esmeraldas.
Patricio no se preguntaba si esas gemas eran legítimas, y en
caso de serlo cómo estaban en poder de una muchacha profesora.
Llevaba también un medallón con un retrato de Hitler. A medida
que descubría las perfecciones de Elsa ---los ojos enormes y
claros; los labios carnosos, las pequeñas orejas rosadas--- encontraba
más y más absurda esa imagen del Führer sobre la carne lechosa.

La adoración nazi por el Führer lo agobiaba. Las imágenes de
Hitler iban a las manos de sus patrullas por millares. «Nuestro
Führer pasando revista; nuestro Führer comiendo con sus soldados;
bajo el Arco del Triunfo en París; acariciando un niño». Se repartían
en las escuelas como si fuesen íconos. Rememoraba con qué desprecio
le hablaba su padre de la costumbre porfiriana ---que heredaron
y acogieron con calor los gobiernos de la Revolución--- de exhibir
retratos del presidente en todas las oficinas públicas, en los
juzgados, en los hospitales y en las prisiones. Cuando estaba
en el Colegio del Estado la fotografía del presidente presidía
las cátedras, las juntas de maestros, los exámenes y Patricio
recordaba que los estudiantes revoltosos la colgaban en los retretes.
Paladeaba retrospectivamente con repugnancia ese sabor de adulación
indigna y estúpida.

Un día le dijo:

---Elsa, me va usted a permitir que le diga una cosa y no la
tome a mal.

---Por supuesto que no, Patricio; entre camaradas hay que decirse
todo lo que se piensa. Lo escucho.

---Mire, Elsa; aquí tenemos un refrán: «A la tierra que fueres
haz lo que vieres». En México las muchachas cuelgan a su cuello
imágenes de santos, o llevan retratos de sus padres, de sus maridos
o novios. Ese es un lugar a donde no llega la política. Quítese
esa fotografía.

Elsa lo vio un poco sorprendida; se puso seria. Pero a la postre
su boca se abrió en una gran sonrisa amable.

---Bueno, Patricio, cuando me lo pida usted en buen alemán, me
la quitaré como premio a un discípulo aprovechado.

Después hizo un gesto lleno de gracia y agregó:

---Y quizá si me lo pide en español; pero hay que saberlo pedir.

Su premio, a la postre, no fue precisamente ver el pecho de Elsa
libre de ese medallón, sino sentir los besos de su boca, apretar
sus senos y finalmente tenerla desnuda en sus brazos. Ella había
fustigado poco a poco su deseo hasta que esas lecciones se convirtieron
para Patricio en una tortura; cualquier contacto lo encendía.
Pero era tímido e ignoraba lo que significaban los gestos de
las mujeres, pues no había conocido sino a Eligia. Además estaba
enamorado; no quería, ni necesitaba a ninguna otra; pasaba por
alto, sin comprenderlas, las insinuaciones ocasionales de aquellas
señoras que encontraba en las tertulias de los burócratas nazis.
Su profesora de alemán, en cambio, era la tentación cotidianamente
renovada.

Cuando Elsa deshizo aquel primer abrazo, su rostro resplandecía
con una expresión de sed aplacada. Después se entregaba a menudo
a Patricio con sensualidad; iba por él en las noches y lo llevaba
a su apartamiento de soltera, en donde hacía vestir a Patricio
pijamas de seda y abría champaña helado. Esas muestras de lujo
chabacano eran para este algo nuevo; le sorprendía esa mujer
que se entregaba sin decirle «te quiero», que le hacía besar
todo su cuerpo al mismo tiempo que bromeaba diciéndole que esa
era carne blanca sin contaminaciones; que aludía veladamente
a la sangre india de Eligia y que, risueña, lo invitaba a que
hiciesen un hijo para el Führer.

Patricio llegaba a su casa profundamente turbado. Para él aquello
no era una aventura ligera; claro que sabía cómo sus amigos y
conocidos mantenían relaciones fugaces sin que eso les quitara
un minuto de sueño; pero él había crecido de otro modo. Se había
hecho hombre al lado de Eligia; le debía una fidelidad total.
Cuando abrazaba a su mujer, al llegar de ver a su amante aria
---como ella se llamaba a sí misma, riendo--- lo hacía con una
pasión desesperada, como quien quiere a toda costa rescatar una
falta. Pero en la vida que hacía, de trabajo sin tregua, ligado
con esa situación extraña, Elsa formaba parte de su opio; se
embriagaba con ella y pretendía, en sus brazos, encontrar el olvido.

El drama los iba envolviendo lentamente; el sufrimiento de la
población se palpaba. México sentía una dolencia hasta entonces
desconocida, el hambre. Las señoras de la clase media, acostumbradas
a mandar a sus criadas a la plaza, hacían ahora filas interminables
en los mercados. Se racionaba la carne, el azúcar, el café, el
pan. Todos los productos se destinaban a las tropas o salían
a Europa, en convoyes, burlando la vigilancia aliada. Las fábricas
de telas, de mantas y de zapatos trabajaban a todo tren para
los invasores. Pero centenares de pequeños talleres clandestinos
se sustraían al control nazi y sus productos aliviaban un poco
las necesidades de las gentes. La metrópoli iba adquiriendo un
aspecto de abandono, como el de esos pueblos perchados en las
montañas cuando se agotan las minas que les dan la vida; o como
esas ciudades de los cuentos que se llamaban «de irás y no volverás».
La gasolina quedó exclusivamente destinada a usos oficiales y
las gentes que tenían que salir de casa montaban en vehículos
estrafalarios, tirados por mulas o por caballos esqueléticos.
Esto era un motivo de regocijo para los empleados públicos, porque
llegaban tarde a sus labores y apenas si trabajaban. La población
fue adquiriendo hábitos de ocio y se respiraba un aire de rebeldía.
En realidad la táctica de laborar mal, poco y con lentitud formaba
parte de un gran plan de sabotaje al cual todos los hombres del
territorio ocupado se apegaban estrictamente. Ocurrieron entonces
los dos primeros bombardeos aéreos que tomaron como metas los
centros ferroviarios. Las gentes los recibieron con júbilo; en
lugar de apagar las luces lanzaron al cielo, por millares, los
haces luminosos de antorchas eléctricas; alrededor de la estación
central de Buenavista se encendieron grandes fogatas. En las
calles oscuras corrían grupos confusos gritando «Muera Hitler».
Los destrozos fueron enormes.

Patricio continuaba su trabajo en ese ambiente anormal; sin embargo,
él no quería pensar sino en sus «Juventudes Hitlerianas de México».
Después de todo, esa era su vida; pero no podía cerrar los ojos
a las cosas que lo herían. No; no quería para sus muchachos esos
moldes torturadores. En los escritos que tenía obligación de
leer, porque debían ser las normas de su acción, encontraba frases
que su espíritu rechazaba: «el desarrollo de la capacidad mental
es de importancia secundaria». Según eso ---se decía--- una página
de Leibniz o de Bergson, o la vida de un Pasteur no tienen ya
valor. O esta otra, que encontraba monstruosa: «los niños deben
aprender a tirar. En la guerra es de mayor importancia disparar
que escribir».

Le pedían una exégesis en español, al alcance de los pequeños,
de «El Mito del siglo XX», esa piedra angular de la filosofía
nazi. Leía el libro y sin poderlo remediar parecíale una propaganda
de charlatán de plazuela. Iba posponiendo siempre el cumplimiento
de esa orden. ¿Qué explicación daría de que «el pecado contra
la sangre y la raza es el pecado original del mundo»? ¿Cómo y
por qué decirles que ellos, los muchachos mexicanos, son de una
casta degenerada? ¿Cómo hacerles comprender a esos niños de ascendencia
vasca: los Jáureguis, los Urrazas, los Ugartecheas, los Zárragas,
los Urdanetas, los Amuchásteguis que son de una raza inferior,
porque no son arios?

La prisión de su padre ensombreció su espíritu. Aquel día, después
de la entrevista con su madre, regresó violentamente a México.
Quería pensar que su sacrificio y el que había impuesto a los
suyos los engrandecía a todos; se esforzaba en creer que estaba
obrando con desinterés supremo por una felicidad que solo disfrutarían
las generaciones del porvenir. Pero la dura realidad lo atenaceaba:
había dejado hecha pedazos a su madre. A Verónica, que tenía
derecho a ordenar, porque le dio el ser y lo cuidó de pequeño
y lo rodeó tantos años con una envoltura de amor; que tenía derecho
a su misma vida, porque todo lo que era, a ella se lo debía:
su salud, su alma, sus sesos, sus músculos; todo lo había modelado
con la suavidad de sus manos. Pero ella no había usado de esos
derechos; suplicó, lloró, habló con palabras persuasivas. Y,
Patricio, su hijo, la había rechazado. A ella, que lo veló durante
noches innumerables cuando iba a morir de la escarlatina; que
estuvo junto a su camita por días enteros, que le limpiaba los
labios con un algodón tan suave como una caricia, que le dejaba
por horas y horas su mano entre las suyas pequeñitas, hasta que
la inmovilidad hacía que la sangre le punzara las puntas de los
dedos. Patricio sabía que ese cariño tan lleno de ternura lo
había devuelto a la vida; y él la dejó en San Miguel destrozada,
como si la hubiera despedazado a hachazos.

Esa noche pretendió trabajar sin conseguirlo. Entonces se fue
a refugiar con Elsa, a ahogar en aquella carne limpia y olorosa
a jabones caros, sus tormentosos pensamientos. Por nada del mundo
habría acudido a Eligia, que le conocía el alma, que ponía, con
su mismo amor un valladar a su deseo; a Eligia que conocía su
casa de San Miguel, las piedras del zaguán, los muebles de su
sala; que había visto a su madre recoserle la ropa y hornearles
las tortas de vigilia. Eligia habría encontrado en su rostro,
en la abstracción de sus ojos, en la falta de atención, en sus
palabras mal hiladas, los síntomas inequívocos de su tormento.
No; no quería verla. Y creyó que con Elsa, en la satisfacción
renovada del deseo hasta el límite de sus fuerzas, encontraría
el olvido.

Al día siguiente volvió al trabajo, mas esa ficción de sacrificio
a lo nazi, por el eterno porvenir de la raza, no lo sostenía
ya. Eso estaba en su cerebro; pero la ansiedad le tasajeaba las
entrañas. Así continuó la tarea, fue a su casa en la noche; evitó
la mirada de Eligia, y un día y otro día se sucedieron y el tiempo
fue mitigando un poco el dolor.

Procuraba hacer a un lado todos los acontecimientos que inquietaban
a los invasores. Al sabotaje que iba aniquilando la vida del
país conquistado, como una gangrena que ataca los miembros y
va invadiendo todo el cuerpo, oponía su tremenda voluntad y su
actividad infatigable. Lo desagradaban cotidianamente nuevos
detalles en aquella pedagogía, impuesta por los nazis, pero los
aceptaba con tal de seguir hacia adelante.

Aquellos conocimientos de aritmética bélica, impartidos en plena
campiña, bajo la sombra de los árboles, seguían su rutina implacable.
Si la distancia de México a San Antonio, Texas, es de mil quinientos
kilómetros, ¿cuánto tiempo tardará un bombardero que vuela a
razón de cuatrocientos kilómetros por hora, en recorrer esa distancia?

Qué distinto cuando les hablaba del color púrpura de los crepúsculos
o de las crecientes de la luna. Les enseñaba a reconocer la estrella
polar y a guiarse en la noche por las constelaciones. Lavaban
su ropa en los arroyos y ensartaban los conejos en varas para
asarlos en el fuego perfumado.

Cuando llegaba la vena poética les hablaba de Homero y les leía
la Cólera de Aquiles o la despedida de Héctor y Andrómaca. ¡Cuán
lejanas le parecían aquellas cosas!

Entonces sucedió algo que lo humilló profundamente y que vino
a apagar el último destello de alegría en su tarea. Fue la expulsión
de dos chicos judíos que formaban parte de sus Juventudes. Era
cierto que en las escuelas no se permitía a los niños judíos
sentarse con los otros en los mismos bancos. Eso lo resentía
Patricio porque iba contra la tradición de su casa, en donde
las criadas, que eran indias, formaban parte de la familias donde
Chole, la nana de Verónica, en un parto grave, había parido en
la gran cama matrimonial de la abuela.

¡Y hubiera podido prevenir el atentado! Pero _Herr_ Waldersee llegó
con su uniforme nazi y su cabeza rapada. Contra todas las reglas
enlodó con sus botas las orillas de la alberca. En seguida hizo
formar a los muchachos, y a dos de ellos ---tipos espléndidos---
los expulsó del local, por judíos. Patricio no estaba allí; él
sabía que a pesar de su disciplina y de sus ligas con el partido
habría echado al agua a _Herr_ Waldersee, ante el regocijo juvenil.
Pero no supo nada sino hasta que Mauricio y Samuel se presentaron
en su casa y con una gravedad de hombres maduros le relataron
el atropello. Recordaba cómo el mayor, un mozalbete de catorce
años, haciendo esfuerzos para no estallar en sollozos exclamaba:
---¡Maestro, quiero irme con los insurgentes!

Si su padre, ahora prisionero, supiera estas cosas, no dejaría
de rebelarse, a pesar de su pasión por el orden y de su odio
por los yanquis. Venía a su memoria un hecho que se relataba
en la casa; esa casa de San Miguel, ahora en ruinas. Cuando eran
jóvenes Rodrigo y Verónica hicieron un viaje a San Antonio, Texas.
Al subir a un tranvía Rodrigo tuvo una sensación de azoro; había
dos compartimentos y en uno de ellos un letrero: «Colored». Rodrigo
vaciló un momento, pero al fin tomó a Verónica de la mano y la
arrastró hasta un sitio libre entre una negra que vestía una
falda rosa, de percal floreado y un negro de cachucha, de barba
rala casi blanca, que leía el periódico a través de sus antiparras.
---¡Nunca me habría perdonado obrar en otra forma! ---concluía
Don Rodrigo.

Patricio formuló un escrito de protesta y lo dirigió directamente
al Presidente Cabañas; invocaba la libertad y hacía hincapié
en el hecho de que la conducta de los dos muchachos expulsados
era intachable. Envió una copia a Von Virchow. El original se
perdió en los montones de papeles de las oficinas de gobierno;
pero a los pocos días, mientras trabajaba en su despacho, Wilfrido
Müller entró y puso ante sus ojos la copia remitida al jefe nazi:
---Has dado un paso en falso ---le dijo--- no sé qué aconsejarte---.
Patricio no le contestó. Tenía ya la convicción de que las Juventudes
Hitlerianas de México eran solo la escuela preparatoria de un
futuro mecanismo militar. Para él, que tenía el culto del deporte
desinteresado y que creía en el sentido esencial de un cuerpo
armonioso, esa convicción era un rudo desengaño.

Pero esa noche había en las lágrimas de Eligia un mayor desconsuelo;
por momentos la sacudían los sollozos convulsivos. ---Vamos.
¿Qué te pasa? Supongo que no será lo de siempre. Es necesario
que te expliques---. Ante el silencio de ella comenzó a levantar
el tono de la voz. ---Quiero saber por qué lloras; esto no puede
seguir así. Es preciso acabar---. Sus frases tenían un alcance
injusto. Al fin Eligia pareció serenarse y díjole calladamente:
---Tenemos muy malas noticias---. Le alargó una carta; venía
en un sobre sin sello postal; un hombre desconocido la había
entregado en la puerta de la casa. Era de Concha, su madre, y
le relataba lo que había pasado desde que Verónica supo la muerte
de don Rodrigo. Contaba el tumulto callejero, el fusilamiento
del cura, el prendimiento de los vecinos, las muertes ulteriores.
---Yo no quería mostrártela ---dijo Eligia--- pero he pensado
que debes saber todo esto. {.espacio-arriba1}

Patricio palideció; no se atrevió a calificar la carta ni de
apócrifa ni de falsa. Era sin duda la letra de Concha y sabía
que esta mujer no era capaz de asentar mentiras tan crueles que
destrozarían el corazón de su hija. Se quedó mirando al suelo
largamente, sin proferir palabra. Eligia se le acercó y él tomó
su cabeza en las manos y la apretó, mientras ella la apoyaba
en sus rodillas y reanudaba el llanto en silencio. ¡Lo que temía
había llegado!

Patricio pasó la noche allí. Todo el día siguiente no salió de
casa. Eligia contestaba las llamadas telefónicas diciendo que
estaba enfermo; al otro día hizo la mismo, pero en la noche se
presentó Wilfrido. Sin ponerse de acuerdo, tácitamente, Eligia
y Patricio se abstuvieron de contarle la verdad; Wilfrido encontró
un ambiente de tristeza tan manifiesta que se marchó en seguida.
Por la mañana Patricio volvió al trabajo y se sumergió en una
actividad febril; comía y dormía en su oficina, pasaba gran parte
de las noches en vela y a las siete llegaban los taquígrafos
a tomarle dictados. No había visto a Elsa, pero por fin cayó
nuevamente en sus brazos y entonces se encerró con ella varios
días en una furia de posesión que no tenía fin. Elsa lo seguía
en su demencia. Patricio estaba torvo, silencioso; ella bromeaba
tumbada en el lecho, mientras él bebía café o champaña. A ratos
dormía con un sueño inquieto y en las noches recorría la recámara,
como un atormentado.

Era indudable que su mente se descarriaba. En el último día,
como a las tres de la tarde se dio una ducha, se afeitó y comenzó
a vestirse. Cuando iba a salir, Elsa se le abrazó, colgándose
de su cuello, cubierta solo con la camisa de seda que le llegaba
hasta sus pies; apretó su cuerpo contra él y le dijo con su voz
más cálida: ---Patricio, Patricio, cásate conmigo---. El la apartó
con un gesto de cólera, con la mano izquierda tomó la camisa
por el ángulo del escote y con un movimiento brutal la rasgó
dejando a Elsa desnuda. Entonces con tono áspero le contestó:
---¡Casarme contigo! Pero si tú eres una puta---. Y salió apresuradamente
a la calle.

No volvió a ver a Elsa y reanudó su existencia de arduo trabajo.
Eligia había recibido cartas anónimas con nuevas versiones de
la tragedia. Los días que llegaban, su marido la encontraba llorosa
y la consolaba con ternura. Sus últimos excesos parecían haberle
devuelto su tranquilidad; se le veía triste y no sonreía; pero
besaba al niño y lo enviaba a dormir con su mismo gesto de antaño.
Eligia, al verlo nuevamente tan tranquilo y lleno de bondad,
intentó convencerlo de que había que dejar esa vida; renunciar
a ese puesto. Pero Patricio se sentía cogido. ---Dejar eso para
ir ¿a dónde? ¿Al campo insurgente? Eso nunca; él no era un traidor---.
Y escuchaba a Eligia sin responderle, mientras ella se estrujaba
las manos acopiando razones.

Una mañana, mientras trabajaba, sonó el teléfono de parte del
Barón Von Virchow. Se le pedía que fuese inmediatamente a verlo.
Montó en su coche y llegó en unos minutos al antiguo edificio
de Relaciones. Descendió en la puerta misma del ascensor; los
ujieres lo introdujeron sin tardanza al despacho mismo del jefe
nazi. Estaba con Wilfrido Müller a su lado. Recordó su primera
entrevista; las mismas sombras bruscas modelaban el rostro de
rasgos teutones; era la misma su mirada, pero quizá sin el escondido
optimismo de antes. Müller en actitud idéntica a la de aquella
otra ocasión, actuaba como intérprete; diríase que nada había
cambiado.

Patricio levantó el brazo: ---Heil Hitler.

Von Virchow lo miró afablemente y se dirigió a Müller. Este
lo escuchó con atención y fue traduciendo con meticulosidad,
escogiendo las palabras: ---Dice el Barón que conoce de oídas
tus progresos en alemán; que lo único que deplora es que hayas
roto con tu maestra en forma tan brusca. Pero que prefiere que
yo interprete para que no haya ningún mal entendimiento.

---Estoy a sus órdenes ---respondió Patricio.

El Barón Von Virchow continuó:

---Estamos extraordinariamente satisfechos de su labor. Debemos
elogiar su inteligencia, su energía, su capacidad. Pero en los
últimos tiempos la calidad del esfuerzo no es la misma, ni tampoco
los resultados.

---Los tiempos no son los mismos ---replicó vivamente Patricio---.
El ambiente ha cambiado y no se respira el mismo aire. Yo trabajo
como siempre, pero debo ahora luchar con circunstancias adversas
que antes no existían.

Von Virchow mostró un aire de disgusto:

---Un buen nazi no debe ejercitar ese espíritu de análisis. Yo
sostengo que es su trabajo el que ha perdido calidad.

---Señor Barón ---dijo entonces Patricio--- no se ha tenido en
cuenta la idiosincrasia de los jóvenes mexicanos. No se puede
hacer una traducción literal de la pedagogía nazi.

---Está bien; está bien ---interrumpió Von Virchow con aspereza---.
Esto no es una polémica. Lo he llamado para darle una oportunidad
de que continúe su labor; no deseamos perderlo.

---_Tanke Schón_ ---contestó Patricio.

---¡_Gut_! ¡_Gut_! ---dijo Von Virchow---. En seguida comenzó a hablar
despacio, escogiendo sus términos:

---Nosotros no atribuímos al ambiente general, como usted dice,
la decadencia en su trabajo. Hemos estudiado su caso con excepcional
interés y puedo decirle que se debe al ambiente… ¿cómo diría
yo? de su vida privada. Procuré rodearlo de camaradas de confianza,
jóvenes y agradables; el teniente Müller, Fraulein Elsa, por
ejemplo. Pero el mal está en su casa, y, va usted a perdonarme
por mi franqueza, está en su mujer.

Von Virchow continuó tranquilamente:

---He resuelto no abandonarlo. Hemos hecho demasiadas ligas para
dejarlo seguir otro camino; ha sido usted uno de nuestros preferidos.
Por su interés y por el interés del partido debe usted continuar
a nuestro lado; tenemos respecto a usted grandes proyectos. Quizá
uno de ellos sea enviarlo a Alemania con una misión especial;
pero para esto debe usted estar libre y dispuesto a tomar una
mujer aria. En una palabra, _Herr_ Guerrero; debe usted divorciarse;
cuanto más pronto mejor. Nosotros le facilitaremos el procedimiento.
Creo que no tengo nada que agregar.

Patricio se sintió tan desconcertado que solo pudo decirle:

---¿Puedo retirarme?

---Sí ---le contestó.

Cuando ya Patricio daba media vuelta, Von Virchow le dijo todavía:

---Y no piense en renunciar. Usted no puede abandonarnos.

Patricio llegó a su coche casi ciego. Ordenó que lo llevaran
al bosque de Chapultepec y allí descendió; sentóse en una banca
y hundió su cabeza entre las manos. El paseo estaba solitario.
Patricio se vio en el fondo de un abismo, de donde no podía salir.
Su vida entera pasaba ante sus ojos; su niñez, en aquel clima
de amor, donde todo era alegría tranquila y trabajo sin amargura.
Recordaba a su padre desde que él era un pequeño: joven, lleno
de brío, enamorado de Verónica; él, Patricio, fue creciendo;
ya iba a la escuela, aprendía aritmética y jugaba a las canicas.
Su padre se tornaba ligeramente obeso. Nació Ana y la vida de
Patricio se llenó con una luz nueva. La recordaba pequeña, pataleando
en la cuna, con la pancita al aire. El la miraba con un cariño
inmenso, porque Ana se prendió en su corazón desde que era un
niño; y la miraba con curiosidad, porque nunca había visto el
sexo de una mujer y ese misterio estaba lleno para él de interrogaciones.

Recordaba que su padre y su madre adivinaban su azoro y sonreían.
Creció, se hizo fuerte. Aprendió a pelear. Don Rodrigo lo cuidaba
siempre, con su mirada vigilante. Verónica lo llenaba de mimos,
por algo era el menor de los hombres. Le envolvía las tortas
que comía en la escuela, durante el recreo; le miraba las manos
para ver si estaban limpias y en seguida lo besaba apretándolo
junto a su pecho. Tenía el recuerdo de su padre trabajando, trabajando
siempre; siempre lleno de fe.

Y después la universidad y Eligia que era una parte de sí mismo;
que había entrado tanto en su vida. Eligia que había sufrido
tanto como él en los últimos meses, que le había dado un ejemplo
de templanza, que nunca había dudado de su amor; Eligia a la
que sentía tan por encima de la atracción primitiva de Elsa,
su amante aria. Y el niño, moreno como su madre, de ojos tan
negros.

Ahora todo estaba deshecho. Su padre asesinado; su madre loca,
escondida Dios sabe dónde. Y tenía que abandonar a su mujer o
condenarla a vivir con él una vida de perseguidos; el jefe nazi
había hablado bien claro.

Patricio se dirigió a su casa. Eligia se sorprendió al verlo;
nunca llegaba para la comida de mediodía. Se sentaron a la mesa
callados, pero ella se sentía contenta; la presencia inesperada
del marido la alegraba. Patricio comió parcamente y al terminar
dijo: ---Hijita mía, voy a dormir un rato en el estudio---.
Allí lo acompañó Eligia, lo acostó, le arropó los pies en una
manta y bajó los visillos; la pieza sumióse en una dulce penumbra.
Entonces lo besó en la frente, pero él atrajo su boca a la suya.
Ella se desprendió y salió de puntillas como si lo dejase dormido.

El escuchó cómo se alejaba. Estuvo así mucho tiempo, boca arriba,
con la mirada vaga. Recordaba sus _boy scouts_, el colegio, su
noviazgo, Eligia… Por fin levantóse sin hacer ruido y fue a su
escritorio. Sacó un revólver del cajón; sentóse en el diván,
se cercioró de que había un tiro en la recámara, apoyó el arma
en su cabeza, apretó el gatillo.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIX

++Una++ noche se quemaron los motores de la fábrica de telas
y se cayó en la cuenta de que habían sido mojados intencionalmente.
Hubo que parar el trabajo y enviarlos a México para embobinarlos.
Como el sabotaje era patente efectuóse una investigación y apresaron
a cinco trabajadores. El asunto no paró allí; se convocó a todo
el personal y fueron detenidos doscientos cincuenta operarios.
Durante dos días permanecieron frente al edificio, vigilados
por esbirros y amenazados por los cañones de las ametralladoras.
Después se les llevó a la estación y se les embarcó en cuatro
furgones con destino desconocido. Esta deportación produjo un
verdadero tumulto: las mujeres, hijas o hermanas de los exiliados
se abalanzaron sobre la soldadesca tratando de golpearlos con
los puños. La conducción hasta el tren constituyó una escena
tremebunda pues los cien boches que la efectuaron recorrieron
el trayecto entre denuestos y gritos de cólera. Los hombres iban
silenciosos, de cuatro en fondo; algunos trataban de calmar el
motín por medio de palabras tranquilas; pero una mujer ya vieja,
de cabellos grises, cogió una piedra y le dio en la cabeza a
uno de los esbirros. Estos dispararon al aire, aprehendieron
a ocho o diez mujeres y las ataron por las muñecas. A la sazón
llegó una camioneta con dos ametralladoras con las que amenazaron
a las revoltosas conminándolas a que dejaran a los deportados
continuar su marcha. Del grupo de estos últimos se alzó entonces
la voz de Antonio Cota, un tipo cincuentón que operaba en las
máquinas que miden la manta.

---Mujeres, mujeres, no hagan locuras; las van a matar a todas.
Déjennos ir tranquilos. Ya vendrá la nuestra.

Entonces levantóse un coro de lamentos y de sollozos y se formó
un grupo compacto a través de la ancha vía, ante las ametralladoras,
mientras los presos continuaban su camino entre las dos filas
de custodios.

La situación de alimentos se había vuelto angustiosa. Como todos
los productos eran confiscados por los nazis, las huertas estaban
casi abandonadas, pues quienes las cultivaban hacían un sabotaje
sistemático ante los ojos mismos de sus opresores. Envenenaban
a las gallinas para que no hubiese huevos, regaban las hortalizas
con soluciones de creolina o hacían sajaduras en las cortezas
de los árboles frutales. Se perdió toda la cosecha de aguacates
y las chirimoyas, tan famosas en San Miguel, se dieron raquíticas,
sin pulpa, convertidas en cáscara y semillas. Comenzaron a sentirse
los efectos del hambre; los niños salían de sus casas, con los
ojitos cavernosos, y se arrojaban sobre cualquier desperdicio
que encontraban en el arroyo. Varios bebés de pecho, cuyas madres
no podían darles de comer, murieron por falta de leche.

Los insurgentes enviaron misteriosamente cajas de alimentos en
polvo y eso alivió un poco la situación, porque las madres de
criaturas en lactancia recogían en la parroquia las raciones
indispensables. Pero la escasez era cada vez mayor.

Körner utilizaba, como medio represivo, la distribución de las
tarjetas de aprovisionamiento. Las hermanas del finado don Estanislao
Gutiérrez tenían que aguardar días enteros en las oficinas de
la Presidencia Municipal y después se las obligaba a formar al
último en las filas de las que esperaban con la canasta al brazo.
Lo mismo se hacía con todos aquellos que mostraban ostensiblemente
su desafección por el régimen. Esas pequeñas represalias fueron
a la postre ineficaces, porque el vecindario protegía a las víctimas
escogidas de la persecución boche y compartía con ellas sus raciones.

Eso, sin embargo, no mejoraba en nada la miseria ambiente. Todo
el bastimento del Bajío servía para las tropas de ocupación;
en San Miguel iban los sargentos y cabos a presenciar por sí
mismos la ordeña de las vacas y los presos de la cárcel cargaban
los botes para llevarlos al cuartel. Entonces los ordeñadores
sustraían el líquido a deshoras para entregarlo a las madres
más necesitadas y aún para tirarlo; con el fin de evitar este
sabotaje Körner ordenó que todas las vacas se concentrasen en
un solo lugar; las llevaron a la Huerta Grande, pero allí comenzaron
a morir de dolencias que el veterinario del regimiento se declaraba
incapaz de diagnosticar.

Ese año las cosechas fueron pésimas. Era cierto que había llovido
poco, pero contribuyó más a ello el sabotaje sistemático. Los
ejidos, que ya de por sí daban casi nada, se volvieron totalmente
improductivos; los agricultores que conservaban aún sus explotaciones
condenaban a sus familias a la miseria con tal de que los invasores
no tuvieran con qué alimentarse. En los casos en que la gente
iba a trabajar bajo la presión inmediata de las bayonetas nazis,
las milpas o los trigales se perdían también. Los insurgentes
repartían, por centenas de millares, unas ampolletas las cuales
disueltas en el agua de los riegos, arruinaban las plantas; sus
aviones volaban sobre los campos, a ras de tierra, en vastos
distritos agrícolas indefensos y esparcían sustancias que enfermaban
las siembras de temporal.

Las masas campesinas, que constituían las tres cuartas partes
de la población, se enfrentaban a estos acontecimientos con estoicismo.
No existía para ellos una gran diferencia entre estos tiempos
de miseria absoluta y aquellos en que sufrían la tutela del Banco
del Estado. Con la práctica adquirida en largos años de demagogia
pizcaban durante la noche lo mejor de las mazorcas y lo acumulaban
en escondrijos que nadie podía localizar. Así lo hicieron cuando
dependían del Banco y lo hacían ahora azuzados por un sentimiento
de patriotismo con objeto de hacer sentir al invasor que habían
hecho la conquista de un páramo. Los jefes nazis vociferaban,
pero se sentían impotentes ante esa acción callada. En el Estado
de México, por la región de Tenancingo, el _gauleiter_ local ejecutó
a varias docenas de campesinos, pero las víctimas llegaron ante
el pelotón que había de fusilarlos con una calma desconcertante.
Los deudos recogían los cadáveres y por la noche celebraban un
velorio en el que los hombres bebían aguardiente y café, mientras
las mujeres rezaban el rosario. En la mañana se efectuaba el
entierro. Seis mocetones llevaban la caja; el cortejo lo presidía
el cura vestido de sotana, amito y estola; detrás, cuatro o cinco
músicos tocaban el violín o la bandurria; las muchachas llevaban
flores, los hombres enarbolaban palmas. Ellos marchaban con las
caras duras y torvas; las mujeres llevaban los ojos hinchados
por el Hanto, pero resecos. Cada uno de estos funerales originaba
el paro total del trabajo por todo el día; después volvían a
sus chozas y el sabotaje recomenzaba con mayor intensidad.

La fábrica de hilados reanudó sus labores bajo la vigilancia
más estricta de la soldadesca nazi. Había lo menos cien hombres
armados distribuidos en los salones; los obreros eran registrados
al salir y al dejar su trabajo y parecía que la producción había
recuperado su ritmo habitual. Pero al cabo de un mes llegaron
noticias de que las telas procedentes de San Miguel se caían
a pedazos cuando eran cortadas y entraban a las máquinas de coser.
Arribó entonces de México un capitán boche, con gruesos anteojos
de miope y una pesada maleta de madera que contenía un laboratorio
portátil. Muestreó los productos en sus diferentes fases de manufactura
y declaró que el responsable era el cuidador de la bodega de
algodón, pues la materia prima llegaba a las máquinas ya dañada.
Körner hizo entonces una pantomima de consejo de guerra que él
mismo presidió teniendo a su lado a Melquiades de la Peña, y
el bodeguero, Dolores Sorondo, un anciano octogenario que había
sido guardia rural en tiempos de Don Porfirio, fue condenado
a muerte. Los verdaderos culpables que eran Jesús Beltrán y Benigno
Ordóñez, hermano del sastre, al saber la sentencia que pesaba
sobre Don Lolo ---como todos llamaban al viejo Sorondo--- decidieron
entregarse; fueron a verlo, pero él los disuadió. Les hizo ver
que estaba enfermo, que no le tenía apego ninguno a los contados
días que le quedaban de vida, que en realidad los médicos decían
que no pasaría el año ---lo cual era exacto--- pues padecía un
cáncer y les suplicó que guardaran silencio, contra el juramento
de que continuarían el sabotaje. El coronel nazi hizo publicar
profusamente las conclusiones de ese juicio sumario y ordenó
la ejecución en la misma fábrica, ante todos los obreros formados.

Körner quiso presenciar el acto para darle un aspecto de mayor
solemnidad. Don Lolo llegó tranquilo, después de haberse confesado
y avanzó a colocarse frente al pie del muro donde debía caer
para siempre. El Padre Benítez lo confortaba. Cuando un oficial
iba a dar las últimas órdenes, el reo se dirigió hacia Körner
preguntándole si podía dirigirle unas palabras, a lo que este
accedió después de que el intérprete lo informó de la petición.
Entonces Don Lolo le dijo con voz clara, que todos los trabajadores
pudieron escuchar:

---Oiga, Coronel, dígale a Hitler que no podrá acabar con nosotros;
que se lo ha de llevar la…

Sonó la descarga antes de que Sorondo terminase y de que el jefe
teutón conociera en alemán el contenido de ese mensaje profético
para su Führer.

El invasor había sufrido ya golpes rudos, pues si bien es cierto
que conservaba aún casi todo el territorio ocupado desde el principio,
también lo era que cada nazi apenas era dueño del terreno que
pisaba. Había habido tantos millares de casos en que los invasores
encontraban la muerte en forma inesperada, que se sentían sobrecogidos
de pánico con el menor pretexto, y nunca salían solos a la calle;
caminaban siempre por parejas y si lograban juntarse cuatro o
cinco se sentían más seguros.

Las gentes, sobre todo las mujeres, inventaban los medios más
estrafalarios de matar boches. Dos días antes de la liberación
definitiva de San Miguel, una viejecita llamada Máxima Ramos,
muy conocida por su oficio de comadrona y que además era dueña
de tres telares que le venían de herencia, en donde en los buenos
tiempos trabajaban sus sobrinos, mató a un soldado en la puerta
de una carbonería donde esperaba una fila interminable. A pesar
de sus ochenta y siete años planeó ella sola la maniobra consiguiendo
en la farmacia que estaba en los bajos de la Casa de Allende
suficiente estricnina para matar una compañía. Cargó una jeringa
hipodérmica con una solución concentrada y mientras el alemán
vigilaba el proceso de las ventas se le acercó con disimulo y
se la hundió en el muslo. El hombre cayó como herido por un rayo
con las quijadas trabadas y presa de horribles convulsiones.
Cuando un compañero que estaba cerca acudió a enterarse de lo
acaecido lo encontró en agonía; no pudo referir lo que le había
pasado, pues murió a los veinte minutos sin poder hablar. El
hecho hubiera tenido consecuencias trágicas para el vecindario,
pues el envenenamiento era clarísimo, a no ser porque esto aconteció
la antevíspera de la expulsión definitiva de los invasores. Se
hicieron aprehensiones y se anunciaron represalias que no pudieron
ya consumarse. Las mujeres se dispersaron y en la confusión del
momento la culpable arrojó la jeringa al carbón del expendio;
por varios meses nadie supo la verdad; pero doña Máxima se puso
en trance de muerte, debido a sus años, y antes de entregar su
alma al Creador hizo una confesión pública relatando el homicidio
con todos sus detalles; contó que ese esbirro maltrataba a las
mujeres que iban a la compra y que un día lo vio golpear tan
brutalmente a un chiquillo que se hizo el propósito de matarlo.

Durante el período agudo de sabotaje en la fábrica, Körner hizo
venir un día a su oficina a Melquiades de la Peña para darle
instrucciones:

---He decidido cerrar definitivamente todas las iglesias. Voy
a dar la orden militar. Pero es necesario que usted redacte un
manifiesto en que diga que esta medida se hace necesaria por
culpa del clero. Haga usted un documento bien fundado.

Está bien, mi coronel ---contestó el pequeño _quisling_---. Hoy
mismo haré lo que me ordena. Nada más me permito recordarle que
esto se lo pedí a usted hace tiempo.

A Körner le disgustó visiblemente la observación. ---Usted obedezca---;
contestó.

En las puertas de los templos se instalaron guardias permanentes
y amenazóse con cárcel a quienes intentasen entrar. A los sacerdotes
se les prohibió bajo pena de muerte cualquier acto de culto,
inclusive los auxilios a los moribundos; en igual forma se conminó
también a los jefes de familia en cuyas casas se oficiara.

Esta clausura era un golpe muy rudo para el vecindario, de por
sí tan devoto; pero casi todos lo esperaban. El Padre Benítez
lo tenía previsto y había hablado del asunto con muchos de sus
feligreses. Entonces las gentes rezaban el rosario en sus casas;
los domingos se juntaban y exactamente a las doce hacían intención
de oír la misa; quienes tenían radio la escuchaban de boca de
un sacerdote, pues como la suspensión de cultos era ya general
en el país, los insurgentes hacían decir una en Monterrey y la
radio la difundía por el territorio invadido. Pero muy pocos
conservaban sus receptores, de manera que casi todos se acogían
a la pastoral del Arzobispo que hacía válidas las intenciones
de cumplir este precepto a mediodía los domingos y fiestas de
guardar. Como los nazis conocieron esta pastoral procuraron invalidarla
por todos los medios que estaban a su alcance. En San Miguel,
Körner ordenó primeramente que los vecinos salieran de sus casas
a esa hora, pero ante la imposibilidad de hacer cumplir esta
consigna mandó la banda militar al jardín, obligó al comercio
que no cerrara un solo instante, en tanto que la soldadesca irrumpía
en todos los sitios procurando alterar cualquier signo de recogimiento.

En el mercado estuvo a punto de producirse un motín porque un
domingo un grupo de nazis entró escandalizando y pretendiendo
obligar a Eduwigis, la verdulera, a que bailase con ellos. Esta
mujer, ya madura, recorría a la sazón quedamente las cuentas
de su rosario: todo el mundo callaba, pues por acuerdo tácito
nadie decía una palabra durante esa media hora y las transacciones
se interrumpían. Los soldados, al verla rezar, la levantaron
de su sitio y comenzaron a cantar a grandes voces, haciendo un
círculo a su alrededor, cogidos de las manos. Entonces, uno a
uno, todos los que se encontraban próximos fueron acercándose
en silencio; llegaron los que venden alfarería en la calle que
va al oratorio y los que tienen los expendios de huaraches frente
al atrio de la Santa Casa y los fruteros de mesones, que acudían
allí por costumbre, pues carecían de fruta que vender. Los escandalosos
volvieron la vista en torno de ellos; habían supuesto al principio
que los rodeaba un público que iba a divertirse; pero la fila
se hizo más tupida y llegó un momento en que se encontraron dentro
de un círculo de espectadores mudos, cuyos rostros estaban cargados
de amenazas. Entonces primero uno de los nazis y en seguida el
resto sacaron sus revólveres y apuntando a la multitud se abrieron
paso e hicieron una retirada hacia la calle. Ya libres en ella
volvieron las espaldas dirigiéndose rápidamente al cuartel.

Mientras tanto en la villa, agobiada por la miseria y el hambre,
se hacían preparativos contra el invasor. Todos los hombres sabían
ya que en el momento en que viesen unas luces rojas salir de
la torre del reloj debería principiar el ataque. Los lugares
de reunión serían las bocacalles en donde cada grupo de ochenta
o cien actuaría a las órdenes de un cabecilla. Era necesario
cortar al invasor las salidas lógicas, que eran la calle real
a la carretera y el camino al ferrocarril. En los callejones
estrechos y escarpados que trepan hacia la parte alta, como el
del Hospicio, o el de las Piedras Chinas o el del Chorro, se
tenderían alambradas que venían preparándose de antemano. Cada
ranchero que tenía diez metros de cerca de púas había ido entregando
el alambre a los vendedores de fierros viejos y de allí los recogían,
después de un regateo ostensible, los vecinos de antemano designados.
En el camino a la estación, con el pretexto de hacer unas excavaciones
para fabricar ladrillo se preparaba un nido de ametralladoras.
Para cortar la retirada por el rumbo de Mexiquito los obreros
contaban con apoderarse de la fábrica haciendo una carnicería
con el centenar de boches que hacían su custodia.

Por el camino de Querétaro o por el de Dolores Hidalgo iban llegando
arrieros con nuevos elementos, que disimulaban bajo brazadas
de leña. Se pretextaron entonces unas obras en la casa del Conde
de Casa de Loja. Enrique Pereda y Ernestina, su mujer, fueron
los más animosos para arriesgar sus vidas en esta maniobra; había
que cambiar unos techos en la planta alta, los que una noche
se derrumbaron estrepitosamente y se hacía necesaria una recua
de burros para llevar materiales. Melquiades de la Peña hizo
entonces una inspección ocular, certificando que la reparación
era necesaria. Los burros entraban a la casa con los costales
de arena e iban haciendo una gran pila junto a la fuente del
patio, entre los macetones de rosales, geranios y palmas.

Durante la noche se retiraban del montón pertrechos y cartuchos
de dinamita. De los depósitos en los túneles comenzaron a salir
los rifles y las automáticas 45 y llegó un momento en que prácticamente
los dos mil adultos que habitaban en San Miguel tenían una arma
y su dotación de municiones. Una mañana estuvieron a punto de
descubrirse los manejos en casa de los Pereda, pues el más pequeñito
de sus hijos, que apenas contaba quince meses, se puso a hurgar
en la arena y tropezó con un proyectil. Nadie se percató del
hecho; pero uno de los guardas alemanes encontró a la criatura
jugando y despertáronse sus sospechas; llamó a sus compañeros
y al fin un sargento se presentó en la casa de los Pereda para
iniciar una pesquisa. Afortunadamente aterrorizaron al chiquitín
quien se echó a llorar y fue incapaz de proporcionarles el menor
indicio.

Sujetaron a Enrique y a Ernestina a un severo interrogatorio
y registraron la casa. Movieron los muebles, levantaron los tapetes,
abrieron los armarios, vaciaron los cajones y baúles. Por una
verdadera fortuna no dieron con el escotillón que conduce a los
túneles.

Quien durante esa temporada hubiera podido leer en los rostros
de los sanmigueleños habría encontrado en ellos una secreta esperanza.
Miguel seguía aparentemente herrando bestias, pero en realidad
forjaba barretas y picos; José el carpintero, fabricaba cabos
de palas. Los tejedores manufacturaban cuerdas, y correas los
curtidores y los talabarteros. Estos artículos eran recogidos
por gentes del pueblo que los llevaba a la casa de los Pereda
o a la casa de los Condes de la Canal, o bien eran entregados
a Isidoro Reyna o a Luz, el hermano de Chole; también los introducían
a la parroquia. El pueblo estaba casi entregado en cuerpo y alma
a una conspiración abierta, cuyo objetivo era la expulsión de
los invasores. Aunque los golpes de mano de los insurgentes,
los triunfos de las naciones unidas y especialmente la toma de
Monterrey habían recrudecido las vejaciones tratando de mantener
aterrorizada a la población civil, esos mismos hechos hacían
renacer el optimismo. Llegaban continuamente órdenes de que nadie
debía precipitarse; de que era menester conservar la serenidad
y no efectuar ningún movimiento prematuro. Generalmente el Padre
Benítez pasaba las consignas; a veces mostraba un mensaje escrito
de puño y letra de Remigio y la gente se sentía agitada por la
impaciencia y la confianza. Era la prueba de que el hijo predilecto
del pueblo no los olvidaba.

La radio americana dio la noticia de la muerte de Verónica; los
semanarios más conocidos: Life, Collier's, Time, publicaron largas
reseñas de su vida y retratos de Remigio. San Miguel entero se
sintió conmovido hasta su médula. Las mujeres, contra todas las
prohibiciones, pretendieron hacer una procesión hasta la iglesia
de San Antonio, de la cual Verónica era muy devota, pero los
esbirros nazis lo impidieron; las muchachas Huacuja que los injuriaron
en la calle, fueron a dar a los calabozos del cuartel. Sin embargo,
los obreros de la fábrica como protesta por la pérdida de don
Rodrigo y de Verónica resolvieron hacer un paro de diez minutos,
el que llevaron a efecto a pesar de la actitud amenazante de
los guardas que los vigilaban.

Francisco Toledo seguía frecuentando el ex convento de las monjas.
Era el _jack-of-all-trades_ para los nazis y lo mismo arreglaba
la estufa que una chapa descompuesta. Con el pretexto de tender
nuevas líneas de luz había levantado un plano del edificio y
lo acotaba con paciencia, tomando hoy una medida, mañana otra.
Körner y la oficialidad lo trataban con confianza y no faltaban
algunos que lo acusaran de estar vendido a los alemanes. Aunque
él resentía estos reproches injustos tenía que continuar su tarea
que era importantísima para los planes insurgentes.

El despacho del jefe estaba en la planta baja, en la crujía que
ve al oriente. Se entraba a él por el corredor, pasando por una
sala de espera; pero tenía una salida directa a la calle. En
la parte alta se había hecho arreglar unas piezas y tanto Körner
como sus oficiales tenían allí sus habitaciones. En la crujía
sur, colindando con la iglesia, se acondicionaron depósitos para
pertrechos y armas y el resto de la construcción quedó para la
tropa. En el curso de unos cuantos meses el edificio se veía
limpio y habitable, con pisos nuevos y agua en todas partes;
causaba tristeza pensar que eran los invasores quienes habían
devuelto su decoro a ese lugar que en los tiempos de la revolución
fue cuartel de un regimiento cuyos hombres encendían fogatas
en el interior de las piezas, mientras los caballos comían la
pastura en los locales destartalados y ensuciaban los claustros
con estiércol. Los boches cuidaban aquello con esmero, como si
fuese bien propio; seguramente pensaban ocuparlo para siempre.

El jefe nazi teníalo todo previsto para un ataque de afuera.
Había artillado las alturas que rodean el pueblo por el oriente;
estableció defensas sobre los cerros que van de sur a norte a
lo largo de los terrenos de Mexiquito. Tenía un nido de ametralladora
en la Torre de las Monjas, otro en la de San Francisco y también
en San Miguel; estableció puestos avanzados sobre la carretera
a Querétaro y en realidad creíase seguro. Venía pidiendo con
insistencia más cañones antiaéreos, pues desde la incursión del
avión que desparramó la propaganda aliada sentíase a merced de
un bombardeo; pero la artillería se enviaba de preferencia a
otros sitios. Sus ametralladoras no cubrían dentro del pueblo,
sino tramos insignificantes. La del ex convento abarcaba casi
todo el camino a la estación; pero hacia el oriente apenas si
dominaba un ángulo de la plaza del jardín. Desde el campanario
de San Francisco se columbraban espléndidamente los alrededores,
mas no se divisaba una sola calle; las ametralladoras de Santo
Domingo defendían únicamente la del Correo. Esta situación era
perfectamente conocida por todos y los hombres que, llegado el
caso, habrían de actuar como cabezas de los grupos sabían con
exactitud cuáles calles y en estas cuáles aceras eran las que
les ofrecían protección más segura.

Una mañana se presentó Remigio. Entró por el túnel que sale a
la casa de Luz, sobre el callejón del Arrastre. Inmediatamente
se dirigió a la estancia abovedada bajo la casa de los Condes
de la Canal y habló con don Aurelio Manterola; después llegó
Remedios y más tarde el Padre Benítez. Al poco tiempo se presentaron
Francisco Toledo, Atanasio Barajas, Isidoro Reyna y Miguel, el
herrero. Remigio venía optimista: los aviones americanos habían
hundido casi todos los barcos japoneses que estaban surtos en
Acapulco; en el Istmo las guerrillas tenían amenazado el ferrocarril
y algunos patriotas habían saboteado el oleoducto interoceánico.
Chihuahua había caído en manos de las fuerzas aliadas y una gran
línea de ataque se extendía desde allí hasta Monterrey.

---Aquí es necesario acabar con esta situación ---dijo Don Aurelio---.
Yo ya no puedo con esta vida de topo; debí haberme salido con
el doctor Bernáldez. En el pueblo las cosas van de mal en peor.

---Es cierto ---agregó el Padre Benítez---. No tienes idea de
la miseria de la gente. Como las iglesias están cerradas visitamos
a la feligresía yendo de casa en casa. No hay que comer, el aspecto
de los niños es espantoso. Por otra parte la hostilidad de los
boches se hace cada día más patente; los telares de mano ya no
trabajan y esa gente vive de la caridad; a los obreros de la
fábrica les pagan, es cierto, pero salarios de hambre y con una
moneda depreciada; además no hay nada que comprar. Es preciso
hacer algo, Remigio.

Francisco Toledo le informó de todo.

---En la fábrica estamos listos; el patrón Don Martín sabe que
vamos a volar los telares y se muestra conforme. Pero es necesario
apresurarse; ya no es posible contener a la gente. Todos están
armados y un día algún atolondrado o bebido va a comenzar la
balacera fuera de tiempo. Necesitas decirnos algo para poderlos
tranquilizar; y mejor que palabras queremos órdenes.

---Daremos el golpe de un momento a otro. Dos o tres días antes
mandaré a un muchacho para avisarles y yo estaré con ustedes
cuando sea preciso. Ahora vamos a hablar y a inspeccionarlo todo.

Fueron entonces a la bóveda, bajo la parroquia, y allí, a la
luz de los quinqués de petróleo, dieron una ojeada a los preparativos.
Se revisaron las listas de los jefes y sobre un plano de la ciudad
se discutieron los movimientos de cada grupo. Remigio dio un
horario aproximado; las cosas comenzarían a las siete de la tarde;
era preciso sorprender a Körner en su despacho; había que cortar
la luz eléctrica; se calculaba que para las nueve la población
entera debería estar dominada por la gente de San Miguel.

Recorrieron la galería que conduce al ex convento de las monjas;
marchaban sin hacer ruido y hablaban en secreto; querían evitar
la menor imprudencia. No se aventuraron a subir por la escalera
que conduce al doble muro de la sacristía, pues cuando la clausura
de los templos todo el mundo fue expulsado de ese lugar y Remigio
y Toledo temían que estuviese ocupado con hombres nazis armados.
En el fondo de la estancia que se abre al extremo del túnel cuatro
hombres trabajaban perforando un tiro horizontal; dos de ellos
golpeaban suavemente con unas barretas, los otros dos iban sacando
la piedra desintegrada en unos cubos de madera. Andaban descalzos
deslizándose como sombras. Por el tiro apenas cabían dos hombres
tendidos; la tarea era tan fatigosa que se renovaban cada dos
horas y habían pasado semanas para desalojar unos cuantos metros
cúbicos de una caliza suave. Toledo golpeó dulcemente en la boca
del tiro y los hombres salieron arrastrándose. Entonces Remigio
se deslizó a su vez; al cabo de unos minutos regresó. ---¿Estás
seguro de la dirección? ---preguntó a Toledo---. Absolutamente,
Remigio. Además vamos a poner una carga enorme, no podemos dejar
nada al azar; habrás visto que hay una perforación lateral; va
al subsuelo del depósito de armas y municiones. Allí hemos arreglado
una oquedad donde caben muchas cajas de dinamita; bajo la oficina
de Körner nos falta cualquier cosa; acabaremos hoy.

---¿Cuándo cargas? ---preguntó Remigio---. Mañana--- respondió
Francisco Toledo--- e inmediatamente comenzaré a alambrar.

Regresaron hasta la bóveda de la parroquia. Toledo siguió dando
cuenta de sus dispositivos: ---Como la explosión será a la luz
del día he escogido a los mejores tiradores para ver si acallamos
las ametralladoras. Ya te acuerdas del sobrino de don Aurelio
que partía un peso al aire; estará con un rifle en la parte posterior
de la Casa de los Condes de la Canal; desde los arcos se domina
la Torre de las Monjas. El asegura que de los dos primeros tiros
inutiliza las ametralladoras y que en seguida se dedica a cazar
boches. Rafael Sotelo cree que él podrá deshacerse de los hombres
de la iglesia de San Miguel.

---Ojalá y no sean demasiado optimistas, ---comentó Remigio.

---No lo son ---respondió Toledo---. El pueblo será nuestro;
yo no aseguro que lo conservemos en nuestro poder, pero daremos
el golpe.

---Si lo tomamos no nos lo arrebatarán; yo te lo garantizo ---le
dijo Remigio.

Este partió y se vivieron entonces días de una impaciencia angustiosa.
Los nazis olfateaban algo en el ambiente y extremaban el rigor
en sus medidas. A las seis de la tarde las calles quedaban desiertas;
se efectuaban súbitos cateos en las casas; se hacían las aprehensiones
más inmotivadas. Pero nada habían podido confirmar. Dos incidentes
iban a dar al traste con todos los planes.

Pablo Colunga, un primo de Praxedes, el carnicero, fue detenido
en la calle y se le encontró en su poder una automática Colt
45 con municiones americanas; el arma estaba nueva y era fácil
ver que nunca se había disparado con ella. Se le quitó también
un cinturón con varios cargadores. Amarrado lo llevaron inmediatamente
a su casa y destrozaron hasta el más pequeño objeto, sin encontrar
ningún otro indicio. Levantaron los entarimados, desgarraron
la ropa, hicieron orificios en las paredes de adobe y con barretas
agujerearon la azotea. Fue una fortuna que Cecilia, la hermana,
estuviese fuera, pues habría sido encarcelada y torturada también;
pero había salido y fue advertida a tiempo. Pablo fue conducido
ante Körner quien puso en práctica con él, sin que faltase uno,
los procedimientos que lo hicieron famoso en Polonia. Todos ellos
están minuciosamente descritos en documentos irrefutables y quiero
evitarme el sufrimiento de repetirlos. Ya tuve que describirlos
cuando don Rodrigo pasó por el infierno del campo de concentración.

Cinco horas después de su apresamiento Pablo agonizaba. Se le
había azotado y golpeado; se le dislocaron los codos; lo colgaron
de los dedos pulgares; simularon un fusilamiento y finalmente
se le emasculó. Körner estaba fuera de sí, con el rostro inyectado,
apoplético por la ira. Quizá si hubiese procedido a la tortura
con mayor sangre fría, prolongándola dos o tres días, habría
vencido la resistencia sobrehumana de su víctima. Pero perdió
todo control sobre sí; empuñó él mismo el látigo y lo descargó
sobre las desnudas espaldas de Pablo Colunga hasta que el cansancio
en el hombro derecho le produjo calambres de dolor. Después,
vomitando denuestos, se entregó a las mayores violencias.

Cuando el médico del cuartel se acercó y declaró sin ambages
que el hombre estaba en coma, mandó atar una cuerda del cuello
de la estatua de Fray Juan de San Miguel, en el atrio de la parroquia,
y lo colgó desnudo.

Este último acto cavernario era el único que provenía de una
mente calculadora e impasible. Pretendía Körner provocar el
motín. Si existían más automáticas en el pueblo quería verlas
y acallarlas. Sabía que ese cuerpo mutilado que pendía de la
imagen venerada iba a encender en cólera a las gentes. Pero estas
sofocaron su indignación; varias mujeres del pueblo pidieron
el cadáver y sin una protesta lo llevaron a sepultar. Eso fue
todo. Esta actitud preocupó al jefe nazi hondamente.

El otro acontecimiento estuvo a punto de tener consecuencias
más graves. Dos reclutas nazis que pasaban los días cateando
domicilios en busca de oportunidades de sustraer objetos valiosos,
recibieron instrucciones de efectuar una pesquisa en la casa
de Chole, quien estaba calificada de sospechosa por sus ligas
con la familia de Remigio. Al llegar a la puerta, en el Callejón
del Arrastre, vieron a dos hombretones encaramados en una construcción
en ruinas, que se alzaba en el huerto; los dos sobrinos de Chole,
Julio y Anacleto que eran los que andaban arriba de los viejos
muros de piedra, descendieron precipitadamente. Los soldados
efectuaron su búsqueda en los cuartos de adobe, hurgaron debajo
del camastro donde Chole continuaba enferma, bañaron con los
haces de luz de las antorchas eléctricas todos los rincones y
salieron nuevamente al huerto. En seguida, después de cambiar
algunas frases en alemán, treparon por la escalera de mano que
los muchachos no tuvieron tiempo de retirar. Arriba, entre las
anchas paredes se veía un pozo circular, seco; abajo se divisaba
un arco. Era la salida de un túnel. Uno de los soldados se descolgó
por una escala marina, exploró el fondo y con grandes gritos
comunicó su hallazgo al compañero. El otro descendió a su vez;
pero tras ellos, con agilidad de monos, bajaron los dos sobrinos
de Chole. Llevaban las pistolas en la mano y se situaron a los
lados de la galería de entrada. Los dos nazis caminaron como
unos veinte metros por el interior del túnel; entonces, comprendiendo
que habían cometido una imprudencia decidieron regresar; se daban
cuenta de la importancia del hallazgo y había que comunicarlo
a sus oficiales. Pero el primero que se arrastró para salir al
pozo recibió un pistoletazo en la cabeza y en seguida Anacleto
le hundió varias veces en la espalda una charrasca encorvada.
El herido lanzó un ronquido agónico al mismo tiempo que echaba
una bocanada de sangre.

---¿Pa qué lo mataste? ---dijo Julio---. Quién sabe si nos hubiera
servido.

---Vamos por el que anda adentro ---fue la respuesta---. Mientras
tanto el otro soldado se había dado cuenta de la suerte de su
compañero y fuése retirando por el túnel oscuro, caminando de
espaldas, con la cara hacia la entrada del pozo. Cuando uno de
los sobrinos se asomó por ella ---lo cual podía observarse desde
adentro porque quedó tapada la ráfaga luminosa que penetraba
al ras del suelo---el boche apuntó a la sombra que la obstruía;
pero sea el miedo o la oscuridad, el hecho es que no hizo blanco;
el proyectil se arrastró por el pavimento levantando una nubecilla
de polvo y haciendo saltar pedazos de piedra. Entonces entró
el otro sobrino y comenzaron a cambiarse disparos; en los túneles
oscuros el sonido se propagaba y ahuecaba con una gran sonoridad.
El boche tiraba sin tregua, al azar, hasta que uno de los proyectiles
alcanzó en un hombro a Anacleto. Este sintió la sangre que le
bajaba a la palma de la mano y al mismo tiempo notó la imposibilidad
de levantar el brazo. ---Oye, Julio, ya me jodió ese cabrón ---le
dijo a su hermano. ---Espérame aquí ahora vuelvo por ti ---le
respondió el otro. ---No, todavía puedo seguir.

Pero no tuvieron que continuar mucho tiempo esta cacería; el
eco de los balazos llegó hasta los túneles del centro. Los hombres
que trabajaban en ellos, creyéndose descubiertos decidieron vender
caras sus vidas y salieron con los rifles y las automáticas en
la mano, mientras otros arrastraban una ametralladora para cubrir
la entrada. Su sorpresa fue grande cuando vieron únicamente a
un nazi que caminaba para atrás de espaldas hacia ellos. Pronto
lo tuvieron desarmado y sujeto. Los datos que les proporcionó
iban a serles muy útiles. Hubo que ocultar el cadáver del nazi
muerto y en seguida llenar el pozo, hasta el tercio de su profundidad,
con tierra, ramas secas y basura. Efectivamente, al día siguiente
un pelotón de hombres al mando de un capitán, registró minuciosamente
la casa de Chole. Julio y Anacleto se habían marchado y encontraron
solo a Luz, ciego, fumando bajo los árboles y a Chole tendida,
como siempre, en el catre.

La desaparición misteriosa de sus dos soldados inquietó más aún
a Körner. Su olfato de carnicero le avisaba de un peligro que
no podía localizar.

Una madrugada Pedro Reyna, primo de Isidoro, llegó al jacal de
este último. Traía el encargo de decirle a Francisco Toledo,
de parte de Remigio, que al día siguiente en la noche se daría
el golpe. Se comunicó la noticia a todos; Toledo tenía ya listos
el alambrado de las cargas, las conexiones de sus fulminantes
eléctricos y había probado múltiples veces sus circuitos con
el galvanómetro; él quería que su labor fuese perfecta. Además,
en el tiempo sobrante ordenó otros barrenos suplementarios; uno
de ellos iba al zaguán donde los centinelas del cuartel hacían
guardia día y noche y donde estaban siempre listos los dos tanques
de que disponía Körner, Al otro día llegó Remigio con Augusto
Jomini, un muchacho de origen suizo, nacido en México, que le
era devoto y que hablaba alemán a la perfección; habían caminado
únicamente de noche, pues Körner ejercía una vigilancia muy estricta
en los caminos; detenían a los arrieros y a los carreteros que
eran los únicos que se aventuraban por la vía de Querétaro.
Prohibió también la pesca en la presa de Begoña y ordenó que
los pasaportes de caminantes fuesen todos resellados en el cuartel.

Desde la llegada de Pedro Reyna los túneles habían ido llenándose
de gente. Un grupo debería salir por la Casa de los Condes de
la Canal, otro por la Casa del Inquisidor a donde se derivaba
un pasaje estrecho. Se planeó sacar una ametralladora a la Calle
de la Canal, otra a la de Hernández Macías; un grupo, partiendo
de la Casa del Inquisidor y aprovechando la confusión inicial,
debería llevar otra al refugio preparado en la avenida a la estación.
Se decidió también colocar otra ametralladora en los arcos posteriores
de la Casa de los Condes.

A su vez Körner tenía su gente acuartelada. Después de las seis
de la tarde la ciudad veíase solitaria; las patrullas recorrían
las calles con los rifles amartillados. El jefe nazi permanecía
en su oficina hasta las siete y media y en seguida se levantaba
para ir a cenar con sus oficiales; comer con ellos era para él
una disciplina de campaña.

Allí permanecía hasta las nueve o nueve y media; a esa hora,
algunas ocasiones, recorría la villa con cautela; temía un atentado
personal, pues no ignoraba los odios que se había concitado.
Se acostaba a las diez, se levantaba a las cinco y desde esa
hora hasta bien entrada la mañana despachaba el papeleo burocrático.
Era un hombre metódico.

Las escenas de la última noche nazi en San Miguel deben haber
sido de una terrible confusión. Antonio Ramírez, que era entonces
un chico de catorce años y trabajaba como mocito con las solteronas
hermanas de don Estanislao, cuenta ahora que él estaba deseoso
de pelear; pero que nadie le daba beligerancia. Dice que a las
siete, pardeando la tarde, se escucharon detonaciones en la parroquia
y que él se dio cuenta desde luego de que eran cohetes. Salió
como un gamo a la calle y vio, contra el cielo crepuscular, proyectarse
unas luces de bengala rojas. Al mismo tiempo oyó el traquetear
de ametralladoras en lo alto; para entonces ya había llegado
hasta el jardín. Mas tal vez antes ---se rectifica--- un enorme
estruendo se elevó en los aires y una nube enorme invadía la
calle de la Canal. Los edificios se cimbraron y escuchó un gran
fracaso de cristales rotos; al mismo tiempo un sinfín de piedras
de todos tamaños aparecieron por los aires, entre ráfagas de
tierra suelta, y comenzaron a caer en el arroyo; él se refugió
en los portales del lado oriente, pero vio a un hombre que había
salido de una casa, desplomarse como abatido por un rayo cuando
un pedrusco enorme le dio en la cabeza. Aunque el dicho de Antonio
Ramírez tiene la fuerza de que proviene de un testigo presencial,
la realidad es que todo fue simultáneo. Al mismo tiempo que las
bengalas iluminaron el cielo vespertino, Francisco Toledo hacía
funcionar el detonador eléctrico y un ángulo íntegro del ex convento
de las monjas se convertía instantáneamente en escombros. Unas
piezas del hotel que se encuentra enfrente y que eran ocupadas
por los nazis vinieron por tierra. Körner y una gran parte de
sus oficiales perecieron en esta explosión.

Entonces la casa de los Condes de la Canal comenzó a vomitar
hombres que se dirigieron a lo que quedaba del cuartel, pegándose
a los paramentos de las casas. Miguel el herrero apareció con
otros varios llevando una ametralladora y pudo acercarse con
ella hasta el hotel central. Remigio salió con otra de la Casa
del Inquisidor cubriendo las calles de Hernández Macías; unos
cien patriotas que estaban en el mismo sitio tomaron por la calle
de la Pila Seca y dieron vuelta por la Quebrada hasta salir a
la de la Canal. Su objeto era impedir la retirada a la estación.

Los nazis perchados en la Torre de las Monjas, una vez repuestos
de la primera sorpresa, comenzaron a hacer destrozos con su ametralladora.
El sobrino de don Aurelio recibió un balazo en la cabeza al principio
de la refriega; se quedó colgado sobre las balaustradas con los
brazos fláccidos. El sargento boche que tenía a su cargo la máquina
no podía ver casi nada de la calle que estaba a sus pies, pues
la ametralladora de la Casa de los Condes había abierto el fuego
y los proyectiles rebotaban contra las columnas de cantera desprendiendo
de ellas fragmentos que volaban por los aires.

Antonio Ramírez cuenta que los nazis trataron precipitadamente
de salir del cuartel. Aunque presentían el peligro que afuera
les esperaba, tenían miedo de que el resto del edificio estuviese
también minado y se resistían a concentrarse en su interior.
Pero sus intentonas fracasaron, pues era difícil trepar por sobre
los escombros y quince o veinte hombres cayeron en cada una de
ellas. Entonces buscaron refugio en la crujía poniente contigua
a la vieja casa de las monjitas de la Concepción, que habían
sido expulsadas por Körner. Mientras tanto un grupo de insurgentes
tomó por el túnel del convento, forzó la comunicación hacia la
iglesia y emplazó una ametralladora en los mismos corredores.

Las patrullas boches que recorrían las calles, viéndose perdidas,
buscaban para parapetarse el refugio de las casas. En el callejón
de las Piedras Chinas cinco hombres huyeron como locos en la
penumbra y fueron a caer en una maraña de alambre de púas que
de pronto se interpuso en su camino. Antes de que pudieran levantarse
una muchedumbre frenética en la que había muchas mujeres cayó
sobre ellos y los pasó a cuchillo.

En la fábrica la lucha había sido dura. Más de cien operarios
perdieron la vida. La acción se inició volando el salón grande
de telares, casi en el mismo momento en que volaba el cuartel;
pero las ametralladoras de los nazis emplazadas en el exterior
ocasionaron grandes pérdidas. Finalmente una saltó hecha pedazos
con una bomba de mano y entonces los servidores de la otra pieza
la retiraron a sitio más seguro. Esa tregua sirvió a los insurgentes
para rehacerse; lograron por fin acallarla y fueron dueños del
campo; comenzaron a desarmar a los nazis dejándolos bien custodiados.

A la sazón las piezas de artillería que dominaban la ciudad desde
el cerro de la Bola y Mexiquito, empezaron a bombardearla. Aunque
los nazis se veían ya vencidos las granadas que estallaban en
las calles o que hacían orificios en las fachadas, producían
un gran desconcierto; Remigio se dirigió entonces al cerro de
la Bola mientras que de la fábrica dominada ya por los obreros
salía un grupo rumbo a Mexiquito al lugar donde estaba emplazado
otro de los cañones.

Antonio Ramírez dice que entre las sombras vio a Francisco Toledo
entrar al ex convento, acompañado de Augusto Jomini, por en medio
de pilas de cascajo y cuerpos inertes. Dice que uno de los tanques
se veía volteado, inservible, pues una de las orugas estaba despedazada;
el otro se encontraba totalmente cubierto por los escombros y
no se veía de él sino parte de la torrecilla. Francisco llevaba
ante sí, picándole los riñones con un revólver, a un oficial
nazi. Entró con él violentamente a una pequeña pieza junto a
la escalera ---que había escapado a la destrucción--- donde estaba
un transmisor de radio. Jomini y Toledo obligaron al boche a
contestar las llamadas y a anunciar que no ocurrían novedades.
Lo hicieron decir también que iba a reparar su aparato y que
juzgaba que por cuatro días no haría comunicados, pero que la
situación era normal.

Mientras tanto unos tres o cuatrocientos nazis continuaban encerrados
en el ángulo del cuartel; un puñado de voluntarios treparon a
la torre de las monjas y a la cúpula de la iglesia y comenzaron
a arrojar granadas de mano. Estaba ya completamente oscuro y
cada explosión en las tinieblas era seguida de quejas o imprecaciones.
Por fin se escucharon voces bien articuladas.

---Dicen que quieren parlamentar. ---Contéstales que se dejen
de bromas; que no queremos parlamentos. Que se rindan---. Jomini
les tradujo las palabras de Remigio. Entonces abrieron lentamente
una puerta, precedidos de una vara con un pañuelo blanco y fueron
saliendo uno por uno.

Isidoro Reyna, con una media docena de muchachos, una vez pasada
la confusión de la primera media hora, no tuvo otra mira que
dar con Melquiades de la Peña. Por fin lo encontró en un cuarto
interior de la Casa Municipal; el pequeño _quisling_ salió demudado,
sin decir una palabra. Isidoro lo llevó por en medio de la calle,
dándole de patadas en el trasero y seguido de tres o cuatro que
lo reconocieron. ---Anda Isidoro ---decía uno--- mátalo de
una vez---. Otro agregaba: ---No lo entregues, Isidoro; no seas
pendejo. Si lo entregas se escapa; di que se te fue un tiro---.
Pero Isidoro había ofrecido entregarlo vivo y llegó con él hasta
donde estaba Remigio.

Los pocos grupos de nazis atrincherados y los que servían la
ametralladora de la iglesia de San Miguel, se entregaron unos
después de otros. Para las diez todo el pueblo se encontraba
en poder de los insurgentes; Remigio había cumplido su propósito
de tomarlo con su misma gente.

Se comenzó a hacer un recuento; a identificar a los muertos,
a curar a los heridos. Las mujeres se fueron presentando para
ofrecerse como enfermeras; los sacerdotes buscaban a los moribundos.
Entonces la ciudad revivió; se restableció el servicio de luz
y las gentes se echaron a la calle: eran libres. Los hombres
que acababan de jugarse la existencia prorrumpían en sollozos
y se abrazaban. Las iglesias se abrieron y las campanas se echaron
a vuelo; el aire se llenó de melodías. Un puñado de hombres improvisaron
hachones y llegaron a la iglesia de San Francisco; sacaron un
Cristo y comenzaron a pasearlo por las calles; otros se les fueron
uniendo. Después grupos de mujeres formaron en esta procesión
y más tarde se juntaron a ellas parvadas de chiquillos. Recorrían
el pueblo en silencio; el arroyo se llenó y era difícil transitar.
Se extendían por cuatro o cinco calles formando una muchedumbre
compacta. De improviso en el aire quieto de la noche una voz
principió a cantar:

«Mexicanos al grito de guerra…» {.poesia}

Dice ahora Antonio Ramírez que él, con otros mocosos, se había
trepado hasta la Torre de las Monjas, donde encontraron varios
nazis muertos. Dice que el canto del himno, que salía de millares
de gargantas, llegaba hasta ellos y se elevaba al cielo como
la voz de un órgano extrahumano.

</section>
<section epub:type="chapter" role="doc-chapter">

# XX

++La++ recuperación de San Miguel Allende por las fuerzas insurgentes
no significó en manera alguna la derrota de los invasores. Este
golpe de mano fue, más que nada, un alarde de osadía en el que
tuvo mucho que ver el rencor de Remigio.

Las gentes de San Miguel pensaron que con la expulsión de los
boches la existencia en el pueblo recobraría su plácida normalidad;
pero se engañaban. Se hizo preciso organizar la defensa y esos
deberes militares embargaron al vecindario; era evidente que
sacrificarían hasta la vida antes que volver a caer en manos
de la soldadesca nazi. Los fusilamientos que se iniciaron con
la muerte del cura Videgaray estaban frescos en la memoria de
todos.

Sin embargo, nadie ponía en duda que comenzaba el ocaso nazi.
Durango primero; más tarde Monterrey. La caída de San Miguel
Allende apresuró la de San Luis Potosí. Los ejércitos aliados
procuraban apoderarse de las vías de transporte y llegar hasta
el riñón del país.

Las catástrofes que estuvieron a punto de dar al traste con el
destino del hombre sobre el planeta despertaron la conciencia
universal. El patriotismo americano se despojó de sus fantasmagorías
del comienzo de la guerra y paladeó la realidad amarga: hubo
que aprestarse a defender el suelo donde florecen y fructifican
las cosas por las cuales vale la pena vivir.

Al mismo tiempo un sentimiento de comprensión hacia el pueblo
mexicano nos cobijó a todos y por primera vez supimos que nuestra
cercanía en la materialidad de la tierra, nos mantenía ligados.

En el esfuerzo titánico que entonces se desplegó, México puso
un gran espíritu de sacrificio, entereza para la lucha y la sangre
de sus hijos. Los Estados Unidos, a su vez, destilaron su mejor
esencia, su jugo de gran pueblo. Los ingleses, que en 1940 dieron
al mundo la lección más espléndida de valor y de apego a los
bienes del espíritu, desarrollaron una labor titánica en Canadá,
mientras unos cuantos defendían, como un símbolo, un Londres
destrozado, ensangrentado, ennegrecido, hecho añicos. Un Londres
al que sus reyes, sin palacios y casi sin albergue, compartiendo
con todos la miseria y el dolor, nunca quisieron abandonar. Un
Londres que fue la atalaya de la humanidad.

Y los pueblos todos de la América: los del Centro que son como
un cordón umbilical que une al norte con el sur y al sur con
el norte. El Brasil, hollado el primero por la planta invasora,
y Venezuela que nos dio a Bolívar, y Perú y Chile y la Argentina,
cantada por el poeta que la llamó «región de la Aurora». Todos
sintieron que formaban parte del continente; por eso, aun en
los días más negros, los salvó la fe en un porvenir común.

París pasó también por la prueba terrible. Después de la muerte
del mariscal octogenario y del asesinato de Laval, los franceses
hicieron una carnicería de nazis en la gran urbe. Se entablaron
luchas en las calles, de casa a casa; volvieron a levantarse
las clásicas barricadas y los parisienses, que tanto amaron siempre
su metrópoli, la ofrecieron en holocausto. Ellos que la quisieron
ciudad abierta para no hacerla sufrir con los bombardeos, iniciaron
los incendios de los edificios y de los monumentos convirtiéndolos
en ratoneras de sus enemigos. Reinó la anarquía y los boches
vivían aterrorizados por turbas vengativas; enjambres de aviones
alemanes trataron de sofocar ese avispero dejando caer toneladas
de explosivos sin otro objeto que convertir en una ruina lo que
fue en 1940 el símbolo más preciado de su conquista. Durante
los meses que duró esta devastación fueron cayendo destrozadas
las mejores reliquias del espíritu humano: Nuestra Señora, el
Louvre, el Arco de Triunfo, la plaza Vendóme, el Luxemburgo.

Los muros de ese París donde tantos hombres amaron y trabajaron
trocáronse en rimeros de piedras, sangre, cuerpos destrozados
y vísceras humanas. Mas sobre los restos trágicos de las construcciones,
convertidas en escombros, había siempre un grupo de gentes que
cantaban _La Marsellesa_. Una bomba enorme cayó al pie de la torre
Eiffel; aquella estructura simbólica que apuntó tanto tiempo
al zenit se retorció grotescamente sin caer, y en las noches
se destacaba como una mueca sobre el cielo iluminado por los
incendios.

El espíritu francés revivió en todo el suelo galo. Entonces fue
cuando el general en jefe de los franceses, desde su cuartel
en el norte de África, lanzó al comenzar el definitivo exterminio
de los nazis la proclama que comenzaba así: Franceses, París
ya no existe, pero la Francia es inmortal.

Divisiones magníficamente organizadas de los ejércitos unidos
comenzaron a atacar y a presionar sin tregua a los alemanes en
los límites septentrionales del territorio ocupado. Brasileños,
guatemaltecos, americanos, ingleses, argentinos y mexicanos pelearon
codo con codo. En México nadie escatimaba la sangre propia, ni
la de los hijos. Aquella propaganda de 1942 para que ningún mexicano
combatiese fuera del territorio nacional, aparecía sin sentido
cuando ese mismo territorio había sido hollado por un invasor
bárbaro.

Los saboteadores continuaban sin tregua su labor. Se incendiaron
simultáneamente los depósitos de petróleo en Tampico, en Tuxpan
y en Minatitlán. Al mismo tiempo voló en Veracruz un enorme depósito
de municiones y pertrechos; en México en la fábrica de pólvora,
que trabajaba para los invasores, estalló una bomba que causó
graves daños. Ese mismo día un convoy repleto de nazis que venía
de Orizaba cayó en uno de los puentes dando un saldo de centenares
de muertos.

Dichas catástrofes eran el resultado de una vasta trama.

El gobierno pelele trataba vanamente de ocultarlas; muchas transmisoras
clandestinas las difundieron por el territorio invadido y por
todo el mundo. Alonso se sintió vigilado y llegó a considerarse,
con razón, en grave peligro; desde entonces pensó que lo mejor
sería huir.

El régimen de Cabañas batía el récord de la impopularidad; todos
sus miembros, desde el llamado Presidente hasta el último de
los secretarios eran objeto del ludibrio y del sarcasmo populares.
Los dísticos o cuartetas de mofa aparecían pintarrajeados en
los zaguanes de los funcionarios y en las portezuelas de sus
coches; cuando Cabañas se dirigía a su casa los pilluelos de
la calle le silbaban y echaban a correr. Un grupo de estudiantes
arrojó sobre el ingeniero Cabral, en el instante en que entraba
al ministerio, bolsas de papel llenas de excremento. Las hojas
clandestinas corrían de mano en mano dando cuenta de los triunfos
de los Países Unidos con caricaturas que satirizaban al _Quisling_
mexicano, a sus secuaces y a sus protectores alemanes.

Imperó entonces un período de terror. Dos internos del Hospital
Civil fueron ahorcados en la Plaza del Carmen. Diariamente con
lujo de publicidad se fusilaban docenas de vecinos en la Ciudadela.
Pero lo más grave, que ocurrió frente al edificio de Relaciones
donde despachaba el Barón Von Virchow, fue un motín de mujeres
ocasionado por el fusilamiento de dos niños, uno de doce y otro
de catorce años, convictos de haber gritado: «Muera Hitler».
El tumulto fue disuelto a balazos por las ametralladoras nazis
que dispararon desde los balcones.

Mientras tanto las tropas aliadas tomaron Torreón y por la costa
del Pacífico habían limpiado de invasores desde la línea divisoria
hasta muy al sur del Estado de Sinaloa; en aquellos momentos
atacaban Mazatlán, que estaba en poder de los japoneses. Tampico
era una pila de ruinas. La aviación americana arrasó toda la
región petrolera y las divisiones nazis que estaban en posesión
de ese puerto encontrábanse prácticamente copadas, pues apenas
si se podían mover entre la costa y la altísima muralla de la
Sierra Madre.

Los guatemaltecos lograron apoderarse de Champerico y por este
lugar comenzaron a recibir elementos que distribuían a sus guerrillas;
con ellas hostilizaban al enemigo tanto en su territorio como
en el Estado de Chiapas.

Remigio se presentó una noche pasadas las doce en la casa de
Toribio Castro a quien conocía de largo tiempo atrás. Era uno
de los obreros de más confianza en la fábrica de Alonso Quintanar;
le dijo que quería ver a su patrón a quien deseaba le avisase
su llegada y que por lo pronto lo único que pretendía era descansar,
pues había llegado rendido.

Al día siguiente Remigio Guerrero y Alonso Quintanar se vieron
por las calles de la Concepción Tequipehuca en la sacristía de
una iglesia vieja, que ahora ya no existe, y que tenía una fachada
con un carácter muy popular encalada de rosa. Charlaron largamente,
pues Remigio llevaba instrucciones escritas para una buena docena
de saboteadores y cabecillas que mantenían, dentro de la ciudad,
el espíritu antinazi. Además le precisaba hablar con algunos
de ellos. Estas peligrosas incursiones éranle a menudo reprochadas
en el campo aliado, pues allí les parecía que corría riesgos
inútiles; pero él sostenía que ese contacto personal con quienes
arrostraban el peligro en las poblaciones invadidas era lo que
más ayudaba a mantener en efervescencia el entusiasmo.

Remigio permaneció en la capital cuatro días y en ese tiempo
conversó con Alonso repetidas ocasiones y le anunció la próxima
ofensiva aliada. En seguida regresó a incorporarse a los insurgentes.

Poco después los ejércitos de las naciones unidas atacaron simultáneamente
varias plazas del norte. Cayeron Aguascalientes y León; en Mazatlán
los nipones resistieron un largo sitio, aunque se rindieron al
fin. En Aguascalientes antes de salir incendiaron una por una
todas las manzanas de casas; los talleres ferrocarrileros quedaron
convertidos en ruinas y la población entera tuvo que emigrar
hacia el norte, en busca de refugio.

Fue en Irapuato donde mostraron claramente que combatían presas
de la desesperación, pues un barrio enorme recibió de los invasores
que huían un terrible bombardeo de gases asfixiantes; millares
de gentes murieron como ratas. Este hecho produjo consternación
en el mundo.

Todo el Bajío había sido recobrado por las naciones aliadas y
los observadores consideraban inminente el ataque a la metrópoli.
En ella el trabajo estaba interrumpido en muchas partes y con
mayor razón el de mi padre, que necesitaba un ambiente de paz.
En los últimos tiempos de la invasión mi madre cosía para un
consorcio que explotaba clandestinamente la miseria y el trabajo
de las mujeres; sospecho ahora que se trataba de alguna combinación
sucia de los paniaguados del gobierno pelele, pues un individuo
descendía de un camión con el escudo nacional ante nuestra casa
y recogía las prendas de ropa que mi madre le entregaba. Recuerdo
que hacían sus cuentas y dejaba sobre la mesa algunos billetes
y cortes de tela para la entrega próxima.

Mi padre, que era hábil de manos, trabajaba en un garaje del
gobierno donde se reparaban camiones y otras máquinas del enemigo.
Comprendo que lo hacía con repugnancia, pero era la manera de
completar el gasto diario y de no morir de hambre; mi madre tenía,
de los días de bonanza, algunos ahorros, pero los veíamos agotarse
con rapidez.

Los muchachos de mi barrio habíamos organizado una verdadera
pandilla y nos dedicábamos a robar comestibles. A medida que
la situación de los alemanes y de la administración de Cabañas
se tornaba precaria, nuestra tarea era más fácil, a pesar de
la creciente escasez. La vigilancia se había relajado y los saqueos
que a veces efectuábamos en las casas de los boches conspicuos
y de los funcionarios del gobierno tenían la simpatía callada
de todos. En realidad nunca nos persiguieron y nuestros riesgos
eran solo efectivos en el momento mismo de nuestras depredaciones.

Pero estas correrías nos permitían de cuando en cuando llegar
a nuestras casas con víveres. Un pedazo de jamón, un bulto de
café o azúcar. Ni mi madre ni mi padre me preguntaban cómo los
adquiría. Solo una noche que llegué a la hora de la cena y extraje
de mis bolsillos un embutido y una lonja de bacalao seco que
vinieron a alegrar un poco una mesa donde todo era penuria, mi
padre me dijo: ---Espero que no hayas despojado a gentes más
necesitadas que nosotros. ---Pierde cuidado ---le contesté---
son de la despensa del Inspector de Policía---. Eso no era cierto;
pero mi padre pareció tranquilizarse y comenzó a rebanar el embutido
con visible avidez.

En las últimas semanas de la ocupación nazi no querían dejarme
salir a la calle; temían que un trastorno grave pudiese sorprenderme
lejos de casa, como en efecto sucedió. Una tarde, cerca de las
siete, venía yo por la Avenida Insurgentes, a la altura más o
menos de las calles de Niza, cuando oímos sobre nosotros zumbido
de aviones. Las sirenas de alarma no nos advirtieron de nada,
pues venían funcionando muy irregularmente de tiempo atrás; en
ocasiones no se escuchaba una sola ni siquiera en ataques aéreos
prolongados.

Caminaba junto a mí un compañero de escuela a quien llamábamos
«Chicuás» porque tenía seis dedos en la mano izquierda. Me tomó
violentamente por un brazo y me jaló hacia una verja entreabierta
que daba a un jardín desierto. Allí nos agazapamos, mientras
afuera oíanse gritos y silbidos. Al mismo tiempo escuchábamos
las pisadas de una multitud apresurada. No nos atrevimos a asomarnos
sino hasta que el rumor de las máquinas pareció perderse en el
horizonte; pero apenas sacamos las cabezas cuando vimos venir
por la avenida, rumbo al sur, es decir, hacia la dirección de
San Angel, unos quince o veinte camiones nazis. Estaban pletóricos
de tropa y disparaban con sus ametralladoras a diestra y siniestra,
tal parecía que para causar el terror. Volvimos a ocultarnos
tras el rodapié de la verja, mientras las balas silbaban; muchas
de ellas producían, al pegar en las piedras de las fachadas,
pequeños ruidos secos. Por fin nos arriesgamos nuevamente a asomarnos
y entonces dímosnos cuenta de que una gran cantidad de camiones
salía por la avenida Chapultepec; racimos de hombres bajaron
de ellos y atacaron a los boches. A los pocos minutos aquello
fue un combate en toda regla. Nosotros, con la inconsciencia
de nuestros pocos años, no nos atrevimos a acudir, es cierto;
pero estábamos embobados con el espectáculo. Llegaron refuerzos
nazis y probablemente también los recibieron los patriotas que
los atacaban, pues la aglomeración de combatientes se hizo mayor.
Los aviones volvieron a volar pasando a poca altura y la casa
frente a nosotros saltó ante mi vista convertida en escombros.
Alzamos los ojos y supusimos que los aviones aliados y los alemanes
estaban empeñados en una lucha mortal; deben haber sido treinta
o cuarenta por cada lado.

Por fin el tumulto pareció alejarse y nos resolvimos a salir
al arroyo. Seguía el tiroteo, los soldados nazis iban perseguidos
a lo largo de la avenida. Nosotros sin saber lo que hacíamos
corrimos en esa dirección, confundidos con el populacho. La lucha
volvió a recrudecerse en otro de los cruceros; a nuestro paso
encontramos tirados un gran número de cuerpos de boches, que
reconocíamos por los cascos y los chaquetones. Había también
civiles caídos, pero a estos las mujeres que salían de las casas
los iban recogiendo y con gran esmero los llevaban a puestos
de socorros improvisados. Vimos también un oficial mexicano muerto;
su uniforme era inconfundible, con su estrella de mayor y una
imagen de la Guadalupana que los guerrilleros y las tropas leales
usaron como insignia durante la guerra; Chicuás y yo nos quedamos
contemplándolo unos momentos llenos de emoción y de miedo. Al
mismo tiempo vimos que de muchas casas salían los vecinos armados
de revólveres y tapaban la retirada a los grupos de boches, disparándoles
a quemarropa. A veces se entablaban combates individuales y uno
u otro adversario caía a los pocos instantes. En otras solían
caer ambos y después de revolcarse en el suelo, uno solo se levantaba
mientras su contrincante quedaba tendido. Cuando llegamos a la
avenida Obregón, untándonos a los paramentos de las paredes,
las fuerzas nazis parecían aniquiladas.

Después supimos que las tropas de las Naciones Unidas hicieron
diferentes incursiones dentro de la ciudad; algo así como un
ataque simultáneo de varios comandos. En casi todos lados obtuvieron
victorias que sembraron la desmoralización entre los boches y
en cambio alentaron grandemente a los habitantes sojuzgados.
Como a las ocho, creo yo, vimos a los últimos alemanes correr
disparando sus armas, perseguidos por la chusma. Pero precisamente
en esos momentos sentí un golpe en la rodilla izquierda, con
una sensación de calor. Al mismo tiempo quise moverla y no pude:
parecía inerte, ajena a mí. Entonces caí en la acera mientras
le decía a Chicuás: ---Ya me pegaron---. Me di todavía cuenta
de que mi camarada me desgarraba el pantalón y me amarraba su
pañuelo en la pierna. Una mujer joven se acercó a nosotros y
se inclinó sobre mí mientras yo, como en medio de un sueño invencible
y sintiéndome desfallecer, oía taconear junto a mis oídos y el
ruido de pisadas de muchas gentes que corrían. Después perdí
el sentido.

La toma de la Metrópoli por ejércitos aliados no tuvo los rasgos
de épica tragedia que en años anteriores habían caracterizado
el toma y daca de las poblaciones rusas que quedaron convertidas
en pavesas. Léase «La Recuperación de la Capital Mexicana», por
el general Agustín Musques, quien mandaba varias divisiones de
leales y hace un concienzudo estudio de esos sucesos. Lo que
yo apunto aquí son apenas deshilvanados recuerdos de un chico
de catorce años.

México es un lugar poco estratégico y las industrias de guerra
creadas por los nazis durante la invasión fueron establecidas
en lugares más defendibles. Los tremendos destrozos que mostró
la urbe cuando la rescataron los aliados los produjeron los mismos
invasores durante los últimos días de ocupación; así lo venían
haciendo en todo el territorio que abandonaban y si en la capital
no fue mayor el aniquilamiento se debió a que sus habitantes
la defendieron con denuedo y vengaron implacablemente, cada vez
que les fue posible, esos actos bárbaros de destrucción estéril.
Sin embargo, se produjo un enorme incendio en el Palacio Nacional
y una bomba de un avión nazi destruyó la torre oriente de la
catedral; también muchas manzanas de la avenida Madero recibieron
el castigo de los ataques aéreos. En los últimos días de su nefasta
conquista los boches disfrazaron algunos aeroplanos con las insignias
aliadas y se dedicaron al arrasamiento de monumentos y grandes
construcciones. Todos han sido reedificados excepto lo que en
la época porfiriana se llamó Palacio Legislativo y en la revolucionaria
se convirtió en una especie de enorme arco triunfal. Esta gigantesca
cúpula muestra aún los boquetes de los bombardeos nazis y se
encuentra cubierta con plantas trepadoras que le dan el grato
aspecto de una ruina en medio de un florido jardín el cual es
cuidado con esmero. Se dice que se ha querido conservar así como
un símbolo de la vanidad podrida de dos eras o dos regímenes
que aunque aparentemente antagónicos y enemigos irreconciliables
tuvieron sin embargo, en sus vicios, grandes semejanzas.

Afortunadamente les faltó tiempo a los boches para consumar sus
proyectos de siniestra ruina y aunque la urbe tuvo durante años
el aspecto de haber sido sacudida por un terremoto catastrófico,
barrios enteros, cuando menos físicamente, quedaron intactos
después de la invasión.

Yo pasé esos días en la cama, sin poderme mover, de resultas
de mi balazo en la rodilla; pero Chicuás venía a verme con frecuencia
y me relataba las nuevas más sensacionales. Naturalmente me daba
detalles aislados, pues no estábamos en edad de formarnos juicios
de conjunto sobre las cosas de entonces; Chicuás era dos años
mayor que yo, aunque mucho menos crecido. Los aliados fueron
formando un cerco cada vez más apretado alrededor de la ciudad;
establecieron posiciones fuertes en los puntos de posible salida
del Valle, pero nunca lograron impedir la comunicación de los
boches con el Istmo, a través de la famosa carretera Panamericana
que ellos habían reconstruído en su totalidad convirtiéndola
en la espléndida vía que conocemos. Por ella pudieron salir;
de no ser así medio millón de nazis habrían quedado cogidos en
una trampa.

La llegada de los convoyes aéreos de tropas aliadas que iban
ocupando las faldas de las montañas circunvecinas y que poco
a poco aterrizaban más y más cerca de los suburbios, tenía para
mí todo el atractivo del misterio; me parecían episodios de una
novela de Wells relatados solícitamente por Chicuás. Pero mientras
él pasaba horas enteras en los edificios más altos explorando
el horizonte con unos prismáticos que mi padre le había prestado,
yo tenía que estar quieto frente a una ventana desde la cual
apenas descubría los árboles del parque.

---Mira ---me decía Chicuás--- por delante viene un aeroplano
inmenso. Creo que tiene diez motores; cuando vuelan sobre la
ciudad los veo tan grandes que parecen cubrir con su sombra una
manzana; detrás de ellos, como arrastrados por medio de cables
que no alcanzo a ver con mis gemelos, flotan en el aire unas
como embarcaciones, con una forma alargada de submarinos y provistas
de dos mástiles, en cuya punta giran muy despacio unas aspas
enormes; son cuatro sobre cada mástil y dan vueltas en sentidos
encontrados. Cada bombardero viene arrastrando diez o doce de
esas lanchas aéreas; en la distancia parece como si las abandonasen
una a una y entonces van descendiendo casi verticalmente con
la lentitud de una pompa de jabón.

Cuando ya los nazis habían sido expulsados, los muchachos organizábamos
jiras para ir a ver esos aparatos fantásticos que se habían posado
en los riscos y estribaciones de las montañas. Eran como Chicuás
me los había descrito; estaban provistos de un pequeño motor
que les servía exclusivamente para poder dirigirse en su descenso
y elegir el sitio del aterrizaje. Después supe que se basaban
en el mismo sistema que los autogiros, hoy tan en boga. Aquellas
canastillas, de un metal muy ligero, llevaban a guisa de toldos
unas cubiertas de una sustancia plástica transparente y de sus
panzas salían docenas de soldados con un equipo completo y muchas
veces vehículos listos para el combate. Chicuás me contaba que
un mismo bombardero, al que podía identificar con sus binoculares,
iba y regresaba varias veces durante un mismo día con su cauda
de navecillas.

Formóse así un formidable ejército de paracaidistas, para usar
este término que ya entonces no resultaba muy exacto. Comenzaron
a tomar, paulatinamente, las pequeñas poblaciones del valle;
en su huida, los nazis dejaban extensos campos minados para retardar
el avance de sus enemigos. La lucha por el dominio aéreo fue
muy enconada. Los alemanes acumularon todas sus reservas; sabían
que al perder la ciudad de México perdían también los últimos
girones de prestigio militar que aún conservaban ante un mundo
expectante. Hubo veces en que, sobre las faldas de los dos volcanes
se entablaron duelos formidables entre cientos de aviones de
ambos bandos. Los americanos desarrollaron un esfuerzo jamás
imaginado; sus fábricas vomitaban aeroplanos como antes automóviles
y tractores. Comenzaron a usar, para atacar los campos de aterrizaje,
helicópteros que hacían tiros de una precisión absoluta y que
los cazas protegían volando alrededor de ellos como un enjambre
de halcones.

Después presenciamos desfiles de fuerzas nazis que se trasladaban
de un lado a otro a través de las avenidas desiertas. Bajo mi
ventana pasaron repetidas veces hileras interminables de tanques
que aparentemente se dirigían al sur; iban a velocidades fantásticas
o cuando menos me lo parecían a mi, quizás por comparación con
la serena inmovilidad de los árboles del parque. Los seguían,
a veces, interminables flotillas de camiones. También pasaban
por la calle cuerpos motorizados y hombres de infantería que
llenaban el arroyo hasta donde la vista lograba alcanzar.

Cuando los nazis se vieron perdidos incendiaron extensas zonas
residenciales, muchas iglesias y la mayor parte de los edificios
universitarios. La tarde que los estudiantes de la preparatoria
quisieron apagar la conflagración que se propagaba de un confín
a otro de su vasta escuela, fueron ametrallados sin piedad y
las calles de San Ildefonso y de Justo Sierra quedaron sembradas
de cuerpos sin vida.

Sé que el relato de estos hechos aislados no da idea de la gran
maniobra de expulsión de los enemigos, ni tampoco de los peligros
de esos días; pero yo cuento aquí lo que entonces impresionaba
mi imaginación juvenil, por lo que se refería a sottovoce en
mi casa y lo que me platicaba Chicuás.

Por fin aconteció lo que no olvidaré nunca; lo que en mi recuerdo
permanece intacto con la frescura de una cosa que acaba de ocurrir
en el día y que llena nuestra memoria de imágenes cuando queremos
conciliar el sueño. Fue la entrada a México de las fuerzas aliadas.
Mis padres me llevaron convaleciente; andaba yo con una muleta
que manejaba con gran soltura. No sé cómo, pero mi padre logró
que nos dejaran subir a la Columna de la Independencia, que había
quedado erecta por milagro, pues todas las esculturas a su alrededor
fueron convertidas en añicos y hasta la figura dorada de la cúspide
había perdido las alas, la cabeza y los brazos que primitivamente
sostenían una corona de laurel. Pero ese ángel áptero quedó como
el símbolo de la victoria y los mexicanos le profesamos un afecto
que lo embellece. No lo cambiaríamos por la mejor obra maestra.

Desde aquella altitud el desfile era como una miniatura; mas
lo que perdía en tamaño lo ganaba en extensión. El Paseo de la
Reforma mostraba sus dos cintas de asfalto aceitoso que se abrían
en cada glorieta. En muchos lugares podían verse los boquetes
producidos por las bombas; los de mayor diámetro habían sido
rellenados provisionalmente con los escombros de las casas derruídas.
Muchos árboles veíanse caídos a lo largo de los amplios andadores;
algunos habían sido arrancados de cuajo y mostraban al aire sus
raíces desgajadas. Desde la cúspide de la columna podía yo contemplar
grandes manchones de follaje que las llamas habían achicharrado,
y a un lado y otro casas y más casas que no eran sino un hacinamiento
de escombros.

Pero en medio de ese paisaje de desolación se movía una muchedumbre
compacta, que se ondulaba con movimientos lentos, como los de
la mar tendida.

Por fin vimos asomarse, viniendo del rumbo de las Lomas de Chapultepec,
la descubierta de la columna, formada por los guerrilleros insurgentes.
Eran la expresión misma del pueblo, con sus trajes disímbolos;
unos venían a pie, bien armados, muchos con sus sombreros de
paja tejida; otros con chambergos de fieltro. Algunos traían
altas botas de monte o huaraches; no pocos caminaban descalzos.

Los de a caballo montaban bestias todas de diferentes pelos y
alzadas, ora con aparejos de sillas rancheras, ora con monturas
tejanas o albardones ingleses. Entre todos eran varios miles;
el resto de los guerrilleros continuaban diseminados aún por
el país, muchos haciendo la última limpia en regiones donde grupos
fugitivos de nazis y japoneses se habían ocultado. No pocos se
confundían con la multitud de espectadores.

Detrás seguía el desfile de las banderas, formado por una larga
hilera de abanderados, cada uno enarbolando una insignia. A su
paso empezaron a saltar de entre aquella multitud compacta, racimos
de flores. Todas las flores que produce esta tierra volaban por
el aire: claveles, rosas, gladiolas, gardenias, geranios, lirios,
violetas. Los jardines de la ciudad y los campos circunvecinos,
y las huertas de Morelos, de Michoacán y de Veracruz se habían
despojado de sus galas y las habían enviado para arrojarlas al
paso de aquellos hombres.

Después venían grupos de soldados americanos y canadienses que
se alternaban con los nuestros. Innumerables bandas daban al
aire sus notas metálicas. Los muchachos conscriptos que tantas
pruebas dieron de patriotismo y de valor, regresaban convertidos
en veteranos, curtidos por el peligro.

Los hombres gritaban sin cesar; las mujeres lloraban. Las risas
se unían a las lágrimas; las niñas alargaban por encima de las
cabezas de sus padres, que las mantenían en alto, sus manecitas
llenas de flores y los chicos encaramados en los árboles de la
gran avenida lanzaban «vivas» con los rostros encendidos.

Este desfile duró no sé cuántas horas; caballos, tanques, camiones,
artillería, motocicletas, jeeps por millares. La multitud se
había echado a la mitad de la calle y rodeaba a los vehículos
y a los soldados. Mientras tanto los aviones volaban de un extremo
a otro de la extensa urbe y pasaban sobre nuestras cabezas tan
bajos que parecía que podíamos tocarlos con las manos. En la
gran plaza, frente a la catedral mutilada, el palacio nacional
ahumado por las llamas y la Casa Municipal convertida en escombros
y viguetas retorcidas, se había levantado un templete inmenso
donde quinientos o seiscientos músicos esperaban la llegada de
las columnas militares; cuando estas y una apretada muchedumbre
llenaban aquellos ámbitos esa orquesta sobrehumana comenzó a
emitir acordes que parecían los truenos de una remota tempestad.
Las voces de los orfeones se elevaban como una plegaria, mezcladas
con la gravedad cálida de las maderas y el lamento de los violines.
En aquel caldero prodigioso, agitado por la batuta de un mago,
se entrelazaban los acordes épicos de _La Marsellesa_ con nuestro
«grito de guerra» y el «grito sagrado» de la Argentina. «The
land of the free and the home of the brave» se fundía en las
notas del «God save the king» y en los compases de _La Bravanzona_.
Cada frase brotaba por fin libre como un pájaro que emprende
el vuelo.

Nosotros descendimos de muestra columna como nuevos estilitas
que después de conversar con los dioses en la altura de su retiro
bajan a rozarse con una humanidad que goza y que sufre. Nos mezclamos
a una turba abigarrada que cantaba y reía; sentados en los troncos
de los árboles caídos veíamos hombres que tocaban la guitarra
y eran acompañados por muchachos y muchachas que les hacían coro.
Las gentes se tornaron dicharacheras y bromeaban al tropezar
con los demás transeúntes; los mozos llevaban enlazadas por la
cintura a sus novias y las besaban sin recato.

Toda la tarde anduvimos así, sin fatigarnos. Yo me paraba de
cuando en cuando a descansar mientras mi madre sostenía la muleta;
a veces caminábamos tramos largos y me apoyaba en el brazo de
mi padre. Me sentía casi bueno y comenzaba a asentar en el suelo
mi pierna convaleciente.

Rendimos nuestra jornada después de las diez; mi padre, que era
muy descreído, se arrodilló en el corredor de nuestra vivienda
desde el cual se contemplaba un cielo casi negro, salpicado de
estrellas; mi madre se arrodilló también en tanto que yo, que
no podía hincarme, me quedé entre los dos, de pie, apretando
con mis manos, contra mis muslos, sus queridas cabezas. Mi padre
dijo: ---Hay que dar gracias a Dios por habernos concedido volver
a ser hombres libres---. Permanecimos así algún tiempo; en seguida
nos fuimos a dormir.

Después todos sabemos lo que pasó. La última resistencia invasora
quedó reducida a la nada. Los ejércitos aliados avanzaban de
norte a sur y de sur a norte; los países invadidos comenzaron
a respirar nuevamente, uno a uno, el aire de la libertad. México,
Guatemala, Honduras, Nicaragua, y en el sur Brasil, Venezuela
y Colombia. El exterminio de boches y nipones fue espantoso;
cosecharon lo que habían sembrado: el odio y la venganza. Dicen
algunos que dos millones perdieron la vida en esta loca tentativa
de sojuzgar a un continente; la expulsión de los invasores de
América fue el golpe de gracia para el germanismo conquistador.
{.espacio-arriba1}

Aquella flotilla gigantesca de submarinos, ya muy diezmada, hizo
un intento para regresar a Europa; muchos atravesaron el bloqueo
aliado, pero muchos se hundieron para siempre en los mares de
América. Todo esto es ya la historia. Lo que tiene un gran valor
anecdótico y humano son los relatos de los que presenciaron la
llegada de los restos del gobierno pelele. Fueron apresados cerca
de Jamaica, en una pequeña embarcación que pretendía pasar desapercibida.

El _Quisling_ Cabañas y Von Virchow huían con algunas docenas de
sus secuaces. Fueron traídos a México, juzgados por un tribunal
civil y condenados a muerte por delitos del orden común. Cuentan
los que lo vieron que Von Virchow forcejeó con sus guardas cuando
lo conducían al patíbulo; por fin, con la guerrera hecha pedazos
por la lucha, los cabellos en desorden y sudoroso, hubo que atarlo
y así recibió la descarga. Cabañas murió sentado en una silla.

Alonso Quintanar y Remigio Guerrero se juntaron nuevamente en
México unas dos semanas después de estos acontecimientos. Estaban
en la fábrica de juguetes y el primero relataba al cabecilla
insurgente la última visita de Rodrigo, cuando vino con Maclovio
y los tres habían saboreado un menú de guerra. Alonso hablaba
de sus amigos con afecto, pero sin lamentaciones; consideraba
que sus muertes estuvieron en el orden natural de las cosas.
Fueron una parte del precio pagado. {.espacio-arriba1}

Remigio llevó la charla a los graves problemas de la hora.

Recuerdo, Alonso ---dijo--- unas palabras de usted en una fecha
que nunca olvidaré; fue a mediados de 1942 y mi padre dio una
comida a raíz de mi graduación como médico. Entonces Maclovio
hizo un brindis y todos formábamos un haz tan apretado que parecía
que nada podía destruirlo. Usted, Maclovio y mi padre discutieron
acaloradamente sobre la guerra, sobre esta guerra que ahora ha
terminado; hablaron sobre comunismo y nazismo, sobre la revolución
española y usted nos dijo que España se había curado de la paresis
del comunismo y le había quedado un grave paludismo nazi. ¿Lo
recuerda usted, Alonso?

---Sí; pero no comprendo qué es lo que ahora te hace rememorar
ese símil que ya entonces estaba un poco manido.

---Se lo voy a decir, Alonso. Ya usted comprende que el resultado
de esta lucha me afecta hondamente; en ella perdí a mi madre,
a mi padre, a mi hermano y a mi novia. Perdí además a Maclovio,
para no hablar de otras gentes. Usted los conoció a todos y sabe
lo que eran en mi existencia. Muchos, después de esas desgracias
y quizás con razón, podrían pensar que la vida carecería para
ellos de sentido; pero yo me digo que esos sacrificios nada significan
si el mundo abre los ojos a una aurora nueva. No estoy poetizando,
Alonso; le hablo en nombre de mis muertos. En cambio si volvemos
a caer en una tiranía, cualquiera que sea su etiqueta o su clasificación,
pensaré que los hemos defraudado y que la sangre de millones
de mártires fue derramada en vano. No quiero hacer frases, pero
siento que la amargura me envenenaría para siempre.

---Tienes razón, hijo mío ---contestó Alonso---. Te llamo ahora
hijo mío con un sentido nuevo. Pero no adivino tu pensamiento.

---Voy a mostrárselo al desnudo, como lo hice siempre ---replicó
Remigio---. Me digo que el mundo se curó del cáncer nazi que
a todos nos pareció mortal. Me pregunto a mí mismo, con miedo,
si no habrá quedado gravemente enfermo de comunismo.

Alonso se le quedó viendo, con una simpatía que rayaba en la
ternura, mientras Remigio tenía los ojos en el vacío del cuarto.
Los dos callaron hasta que el hombre viejo comenzó a hablar pausadamente:

---Mira, Remigio, esta pregunta se la hacen muchos hombres en
todos los ámbitos del mundo: lo mismo en China, que en Inglaterra,
que en Polonia, que en Francia, que en los Estados Unidos. Se
la hacen los padres que perdieron a sus hijos, las esposas que
perdieron a los maridos o los hijos a los padres; se la hacen
como tú. No me tomes por un pedante, pero te la voy a contestar.

Remigio lo interrumpió bruscamente: ---Perdóneme, Alonso, pero
antes de saber lo que piensa yo quiero poner ante usted, con
toda su crudeza, las dudas que asaltan mi espíritu; quiero que
las conozca antes de que usted me diga algo sobre estos tiempos
nuevos. Lo hago porque esa es la actitud sincera de mi parte.

Alonso le dijo con dulzura: ---Habla, hijo mío; te escucho. Habla
con franqueza.

Remigio comenzó entonces pausadamente: ---Es cierto que el pueblo
ruso puso un valladar a las conquistas nazis; a la máquina guerrera
alemana se enfrentó la máquina guerrera soviética. Podría argüir
que esos mecanismos militares solo los fabrican las tiranías;
pero prefiero pensar con generosidad y concluir que fue únicamente
el patriotismo ruso lo que produjo el milagro.

Esta gran hazaña nada tiene que ver, sin embargo, con lo que
estamos discutiendo. No borra la lucha de clases, que es la ponzoña
más activa con que se ha envenenado el espíritu de los hombres;
no borra la destrucción de la familia, sobre la cual el bolchevismo
ha lanzado ataques tanto o más enconados que los de los mismos
nazis.

Y hay otras cosas, Alonso, que nunca podremos olvidar: la miseria
del proletariado ruso, mientras sus dirigentes formaban una burocracia
burguesa bien cebada; la G.P.U. con sus campos de concentración
y sus trabajos forzados; las persecuciones políticas, el hambre
de los obreros no afiliados al partido, la pena de muerte para
los niños.

Debemos recordar a la niñez vagabunda y desamparada, a las muchachas
prostituidas por los funcionarios de estado que gastaban en perfumes
y en joyas y las nubes de mendigos que pululaban en las estaciones
de ferrocarril o en los restoranes para echarse sobre los restos
de las comidas, mientras la propaganda ensalzaba el paraíso comunista.
Con razón se le llamó a Rusia el país de la gran mentiras

Bueno, Alonso; tengo miedo a que el mundo crea que el comunismo
lo ha salvado y se eche en brazos de un bolchevismo que está
podrido, porque no le queda ya ni siquiera la fe romántica de
sus pioneros. La imagen de Polonia, crucificada por Rusia y Alemania
en septiembre de 1939, no puede borrarse de mi mente.

Y respecto a nuestro México, donde siempre hemos llegado atrasados
a todas las grandes experiencias de la humanidad, temo que ahora
vayamos a continuar con la experiencia del comunismo. En fin,
me debato en un mar de dudas y no sé qué pensar. Alonso, he dicho
apenas una mínima parte de lo que me acongoja, pero creo que
no puedo decirle más.

Cuando Remigio terminó, Quintanar acercóse a él y le dijo:

---Tengo confianza en el porvenir de nuestra patria y tus temores
no se realizarán. Además, ese porvenir está en las manos de los
jóvenes; de ti y de todos los que la salvaron. De ustedes depende
que México no caiga en las manos de los ineptos, de los ladrones
y de los farsantes. La voluntad de los que se sacrificaron por
este suelo será respetada y lo que ellos decidan tendrá el apoyo
de todos.

Pero voy a tranquilizarte. Lo terrible de la situación del mundo
antes de esta guerra era que se debatía voluntariamente entre
los dos cuernos de un dilema: comunismo o nazismo. Si escapaba
de uno era para ser ensartado por el otro. Las gentes no veían
otra alternativa. Esa situación mental, sazonada con la salsa
de nuestro antiyanquismo, fue lo que ocasionó la entrada de los
invasores. Pero el milagro de esta guerra ha consistido en que
nos libertó de esta mazmorra de dos salidas y nos abrió otros
horizontes. El mundo nuevo no será ni nazista ni comunista. Los
rencores de razas y de clases han terminado para siempre y de
hoy en adelante ningún ser humano se condenará a sí mismo a vivir
amargado por el odio. Todos los credos negativos desaparecerán
de esta tierra y eso es lo que eran el comunismo y el nazismo:
místicas destructoras de las fuentes mismas de la vida. Los
pueblos de Rusia y Alemania volverán los ojos hacia la antorcha
que enarbolarán los pueblos libres; y nosotros nos contamos entre
estos. La terrible prueba sufrida nos integró al consorcio de
una espléndida armonía.

---Que Dios lo oiga ---terminó Remigio.

Era cierto que nuestros comunistoides habían pensado que la victoria
de las Naciones Unidas era el triunfo del comunismo. Creían en
la vuelta del frente popular en Francia, en la hegemonía del
Partido Comunista en Inglaterra, en la implantación de una república
Soviética en España. Y naturalmente creían que ellos serían los
amos de México, que toda la burocracia quedaría en sus manos
y con ella el presupuesto, la embriaguez de poder y las infinitas
posibilidades de corrupción. Desde sus refugios en Estados Unidos,
a donde habían huido cuando entraron los nazis, hacían planes
y escribían en los periódicos de izquierdas, mientras aquí, del
Bravo hacia el sur, el pueblo, el ejército mexicano y sus aliados,
luchaban desesperadamente.

Pero esos comunistoides cometían errores fundamentales de juicio.
No se habían dado cuenta de que con la guerra el comunismo había
desaparecido y de que en Rusia misma al iniciar los alemanes
su invasión en 1941, era ya una cosa del pasado. Cuando Hitler
fue vencido era preciso crear una vida nueva; el capitalismo
estaba agonizante y el comunismo había demostrado su incapacidad
como fórmula de vida colectiva. Los comunistoides de México,
parásitos de una época de desconcierto, se vieron ante el dilema
de morirse de hambre o trabajar.

Ellos alegaban que tanto la heroica resistencia de Rusia, como
la de México, eran el resultado de la devoción de las masas por
el comunismo. La verdad era que en México esas masas misérrimas
lo único que querían (y nunca consiguieron bajo los gobiernos
de la Revolución) era un poco de comida y agua clara, ya que
un Dios providente los vestía de brisas tibias y de rayos de
sol. No tenían fe en sus líderes porque estos, por maldad o por
imbecilidad ---casi siempre por ambas causas--- solo les habían
aportado nuevas inquietudes y gabelas. Por eso al terminar la
guerra la lucha social según la entendían los comunistoides se
esfumó como una bola de humo y las gentes sufridas fueron al
encuentro de un poco de bienestar y felicidad por senderos muy
diferentes.

México pudo emprender entonces su camino, como el enfermo que
sale de un padecimiento grave y doloroso para entrar en una convalecencia
reparadora. Del mismo modo que se eliminan los humores tóxicos
y las celdillas se renuevan, así este país se renovó. La guerra
que había peleado con tan gran espíritu de sacrificio le atrajo
el respeto del mundo.

Los Estados Unidos se dieron cuenta de que la verdadera política
del buen vecino consistía en acatar la voluntad de las gentes
de México, de los hombres que trabajan, de los que viven forjando
la patria, y no en sancionar los atropellos de una minoría audaz
divorciada del interés de las masas. Los americanos saben ahora
que aunque diferentes a ellos porque nuestros atavismos abrevan
en fuentes distintas, somos también sus iguales cuando se trata
de defender el espíritu humano, que es lo único sagrado en este
planeta. También se han convencido de que las realidades mexicanas
son diferentes a las suyas y así hemos podido seleccionar a nuestros
hombres y labrar nuestro destino. Después de la victoria todos
aquellos que la forjaron volvieron a sus actividades fecundas
sin buscar el premio en el poder político. Trabajamos en una
gran obra de cooperación y de armonía y espiritualmente somos
una avanzada en esta civilización de postguerra.

El mundo merece en verdad la denominación de mundo nuevo. ¿Para
qué hablar de las últimas etapas de aquella lucha gigantesca
que culminó con el aniquilamiento de la mística nazi? Tendría
que mencionar el asalto final a Europa, el asesinato de Hitler,
la muerte de Churchill en un avión que lo conducía a China, la
entrada a Berlín de las tropas aliadas, el esfuerzo titánico
de millones de hombres que querían reintegrarse a la vida, que
deseaban sembrar el trigo, hacer el pan, hilar la lana, besar
a sus mujeres y acariciar a sus hijos; que tenían ansias de dedicarse
a las humildes tareas de la paz.

Después el mundo se ha ido transformando poco a poco en un mundo
de concordia. No más lucha de clases, ni lucha de razas, ni de
religiones. Lo que los hombres buscan ahora es libertad y seguridad:
que haya buen pan y abrigo en las casas, que los niños jueguen
y eduquen sus almas; que las mujeres lleven a las mesas los asados
humeantes y se pongan flores en los cabellos, que los viejos
descansen y relaten cuentos a los nietos. Los hombres quieren
trabajar: construir escuelas, piscinas, canchas, jardines; hacer
bellas estatuas y pintar frescos armoniosos; quieren investigar
sobre los microscopios, curar las dolencias, sanar las llagas,
devolver la vista a los ciegos; quieren tejer telas calientes,
fabricar sandalias y también cuidar los naranjos y los rosales,
cultivar el maíz y las viñas, ordeñar las vacas y las cabras.

Es cierto que la fuerza no ha desaparecido de la tierra; pero
la hemos alejado de nosotros lo más posible. La tenemos confinada
en una lejana isla del Pacífico. Solo allí hay submarinos y bombarderos
prestos siempre para saltar sobre los que pretendan romper el
nuevo pacto. Y hemos arrancado para siempre los colmillos y las
garras al pangermanismo milenario.

Todo ha sido tan fácil, tan sencillo, tan claro. Solo fue menester
una poca de buena voluntad, de buen sentido, de amor.

Cierro esta historia sin párrafos altisonantes, sin arengas ni
lirismos. Ha sido una historia triste; una historia de «sangre,
de sudor y de lágrimas».

La cierro con desconsuelo porque sin conseguirlo quise impregnar
estas páginas con el dolor de mis protagonistas.

Mi fracaso me ha tornado humilde; pero esta sensación está mezclada
con una gran felicidad. La de que yo, y millones y millones,
formamos una humanidad mejor; avistamos en el horizonte la luz
de la aurora.

Y a través de veinte siglos encontramos el sentido de las palabras
que El pronunció: «Bienaventurados los mansos porque ellos heredarán
la tierra».

</section>
<section epub:type="epilogue" role="doc-epilogue">

# Epílogo

++Aunque++ no creo que estas páginas lleguen a ver la luz pública,
he inquirido tanto para escribirlas y he hablado con tantas personas
testigos de los hechos, que mi espíritu pugnó con la idea de
dejarlas como un amasijo informe. Además avancé demasiado en
esta tarea para abandonarla. Les he dado una unidad, colocando
cada cosa en su sitio, pues como hoy he escrito un sucedido y
mañana otro ---según mis oportunidades de conversar con aquellos
que los presenciaron--- todavía ignoro cómo voy a proporcionarles
su estructura de incontestable veracidad. Al escribir esta especie
de exégesis, robándole tiempo a labores que en mi vida son fundamentales
porque son las que la justifican, me pregunto si la colocaré
al principio o al fin. No lo sé todavía.

Me incitaron a la tarea de emborronar estas cuartillas algunos
amigos, entre ellos Berrueta, mi jefe; Armando Otamendi y el
ingeniero Uranga. Por supuesto no tengo nada de escritor; eso
no quiere decir que desdeñe a los escritores, por el contrario
los admiro cuando son buenos. En la vida que ahora vivimos dentro
de lo que se ha dado en llamar el «Mundo Nuevo» los escritores
ocupan un lugar predilecto; alimentamos la certeza de que si
bien es verdad que nada se consuma sin la acción, también lo
es que esta solo es fecunda cuando la precede una idea. Como
es natural, únicamente se consideran escritores a quienes tienen
ideas y las saben expresar, lo que constituye una coincidencia
poco frecuente.

Yo me ocupo de actividades ajenas a la literatura. Le ayudo a
Héctor Berrueta en el manejo de la Cooperativa del Bajío, que
regula el consumo en el centro del país; visito las agencias
en las ciudades de mi zona y voy a México a menudo. Este viaje
no es muy de mi agrado; la capital en 1961 me da la idea de una
casa inmensa llena de estancias superfluas y de muebles inútiles,
cuyos antiguos moradores eran gentes que tenían un sentido equivocado
de la vida y se habían rodeado de un sinfín de objetos estorbosos,
caros y feos. Después habita ese caserón absurdo una familia
sensata que lo que quiere es limpieza, agua clara en abundancia,
una cocina reluciente, algunos libros y lechos confortables;
entonces deciden reducirse a media docena de piezas donde entran
el sol y el aire y cierran los grandes salones innecesarios.
México me produce esa impresión; se ven ringleras de edificios
de oficinas que ahora están clausurados; multitud de lo que antes
eran pequeños comercios exhiben telarañas en las cortinas metálicas
de sus escaparates; en los sitios que debieron ser los más ostentosos
vense locales a donde hace mucho no penetra la luz, con grandes
letreros que dicen: Banco Fulano, Banco Mengano; son los restos
de un sistema económico disparatado. Un viejo que ahora cuenta
unos setenta años y a quien suelo ver en esos viajes, me dice
que él trabajaba en un reducido tienducho de la avenida Madero
---vía que aparte de unos cuantos edificios coloniales tiene
un aspecto innoble--- donde su patrón vendía relojes, bastones,
alhajas falsas, plumas fuentes y un sinfín de chucherías feas.
El único que allí ejecutaba una labor útil era un relojero que
tenía su mesa de trabajo en el rincón más oscuro donde iba perdiendo
la vista y la vida; el dueño y dos dependientes esperaban a los
incautos y les metían por los ojos a precios fantásticos aquella
falluca inservible.

El resultado es que la ciudad de México hace mucho que ha dejado
de crecer y mientras el centro tiene un aspecto de soledad los
suburbios se ven llenos de gentes que trabajan en sus Casas o
en talleres cercanos. Además, muchas se han ido al campo y cuando
son prósperas vienen en sus autogiros o en sus helicópteros, permanecen
unas horas en la capital y regresan a sus granjas. Me dicen que
este mismo fenómeno, aunque mucho más agudo, se observa en las
metrópolis gigantescas como Londres o Nueva York.

Pero en fin, me estoy extraviando en un sinnúmero de digresiones;
me sentiré feliz si es que he logrado a la postre hacer un relato
sencillo y exacto, que se deslice con agilidad, como el agua
de un arroyo, y que pueda leerse sin cansancio excesivo.

La cosa se suscitó en San Miguel Allende. Me gusta este lugar
porque me da idea de lo que era una villa del siglo ++xviii++, con
una arquitectura muy digna y austera y al mismo tiempo con un
sabor barroco lleno de fantasía. Me cuentan que ahora está mucho
mejor que antes, porque con la transmisión inalámbrica de la
fuerza eléctrica todos los postes, hilos y transformadores han
desaparecido del paisaje urbano. La planta transmisora está por
la estación, donde nadie la ve, y los receptores se encuentran
dentro de cada casa. Por lo mismo, cuando recorre uno sus calles
empedradas se respira un ambiente de quietud y de legítima antigüedad.
No hay signos neón ni anuncios para atraer a los turistas y la
vida se alimenta con una serie de actividades ocultas que no
se hacen ostensibles. El gremio de tejedores manda hermosos tapetes
y mantas a todo el país y aun al extranjero; la cerámica es famosa;
su fábrica de vidrio donde se conserva todavía la tradición del
trabajo a mano, envía sus productos a los Estados del centro.
Los plateros manufacturan muy bellos objetos y el gremio de costureras
hace ronronear sus máquinas en docenas de casas. La que en otro
tiempo fue la fábrica de Don Martín Pérez ha sido modernizada
después de la invasión y ahora la maneja por cuenta de la Cooperativa
del Bajío su hijo Martín, a quien todos llaman Martín el Chico.
Si a esto se agrega que el agua abunda y riega las antiguas huertas
donde se dan mejor que nunca los duraznos, los higos, los aguacates,
las chirimoyas y las nueces, se comprende que San Miguel Allende
tenga innumerables devotos. Los que pueden reciben en sus aparatos
de televisión los mejores programas de México; algunos que hablan
y comprenden el inglés ven y escuchan las películas de Nueva
York; el receptor del ingeniero Uranga es de gran alcance y allí
vamos a oír los conciertos transmitidos de París. Esto nos halaga
a los que fuimos criados por nuestros padres en el amor a una
Francia inmortal.

Hace unos meses ---porque estoy escribiendo estas páginas bien
entrado 1961--- nos reunimos varios amigos en la casa del ingeniero
Uranga a quien ya mencioné. El maneja algunos sistemas de riego
de la región y representa en Guanajuato a la Sociedad Irrigadora
de México la cual está controlada por un grupo de expertos, asistidos
a su vez en su labor por una pléyade de ingenieros agrónomos
quienes ---al revés de los de antaño--- no se ocupan de política
y conocen su oficio. Esa Sociedad Irrigadora que ha logrado dar
bienestar a cientos de miles de ejidatarios, en otro tiempo miserables,
se formó con los despojos de una Comisión de Irrigación y un
Banco Ejidal los cuales, según dicen las gentes de entonces,
eran focos de desbarajuste.

La casa de Uranga es una de las pocas habitaciones nuevas de
San Miguel. Está construída a un lado de la carretera que va
a Querétaro y data de unos cuantos años antes de la guerra. Tiene
un aspecto popular, con sus fachadas encaladas de blanco y sus
cornisas corridas a tarraja, pintadas de azul. Se levanta sobre
una meseta desde la que se domina el pueblo; de día la vista
es espléndida, con las colinas que la rodean. Se ve la parroquia,
la plaza de toros y el panorama entero de cúpulas y campanarios;
de allí baja, hasta los manantiales del Chorro, la falda abrupta
del cerro con peñascos enormes que parecen enjalbegados con lechadas
chillonas: anaranjadas, amarillas, violetas, ocres, rojas. La
finca de Uranga es conocida como la Casa del Cantil.

Berrueta y yo habíamos comido con él. Nos instó tanto para que
fuésemos sus huéspedes durante nuestros días de permanencia en
la ciudad, que a la hora de la sobremesa fuimos a la posada de
San Francisco donde estábamos alojados para recoger nuestros
bagajes. A mí me dio Uranga un cuarto lleno de luz, con vista
hacia una cañada plantada de árboles. Aunque no soy dado a la
molicie los días que permanecí allí me levantaba un poco más
tarde que de costumbre y disfrutaba desde el lecho de este paisaje
a través de la ventana abierta.

Por lo demás, si bien es cierto que en esa casa se vive con comodidad
y contábamos con una ducha tibia a cualquier hora, no existen
los refinamientos de las viviendas recién erigidas en la capital.
Todavía cuelga del centro de las piezas el anticuado farol con
su bulbo eléctrico y junto al buró la lámpara para leer en las
noches. Los muros recorridos por esas corrientes de ínfima tensión
que con su fosforescencia suave dan una luz tan apacible, no
se conocen aún en San Miguel en donde la mayor parte de las cocinas
ostentan su prehistórico brasero de carbón o leña. La residencia
de Uranga tiene una estufa de petróleo de hace como veinte años
lo cual constituye allí un adelanto extraordinario. Cuando le
pregunto a Uranga por qué no compra una estufa de onda corta,
él replica que se puede vivir como hombre civilizado en un ambiente
de atraso mecánico.

Sobre eso se charlaba una de las tardes que pasamos con él cuando
después del trabajo, como a las cinco y media de la tarde, tomábamos
café en la terraza que ve al pueblo. Sobre las colinas se encendía
el crepúsculo y los rayos del sol, casi horizontales, nos daban
en la cara.

---No me critique usted ---le decía a Héctor Berrueta--- por
vivir en este caserón destartalado; es cierto que caliento el
agua con olotes, pero corre en abundancia y Doña Tula, que es
mi ama de llaves, lo tiene todo reluciente de puro limpio. Además
me atrevería a afirmar que una tortilla frita en una cacerola
diatérmica nunca tiene el sabor que le da una sartén de esas
que jamás se lavan y solo se frotan con papel. Puedo decir también
que los huevos los recojo yo y son estrictamente frescos; todavía
no hay huevos _ersatz_ que puedan compararse con los que ponen
las gallinas.

Tiene usted razón; pero voy a serle franco ---replicó Berrueta---.
En su casa me siento como creo yo que se sentían nuestros abuelos
cuando iban a los ranchos en donde no había periódicos, ni automóviles,
ni agua corriente en los baños. Eso les parecía bien porque jugaban
como niños a la vida primitiva; pero al cabo de una semana la
civilización los atraía. Con esto, amigo Uranga, no pretendo
desdorar su hospitalidad.

---Berrueta, ha pronunciado usted una palabra que me escuece.
¡Civilización! ¿Qué es, qué ha sido la civilización? Cuando éramos
muchachos, antes de la guerra, pensábamos que la civilización
eran los automóviles, los aviones, los ferrocarriles que se llamaban
aerodinámicos, los refrigeradores, la luz neón. Todavía mucha
gente piensa ahora que consiste en los autogiros, las casas construídas
en serie, los nuevos acumuladores de fuerza o los concentradores
de energía solar. No, Berrueta; la civilización es algo más hondo;
está en el corazón y en el cerebro de las gentes; es una cosa
de cultura. El cristianismo echó las bases de una civilización
sin aviones estratosféricos ni televisores. La civilización consiste
en exaltar nuestro instinto de sociabilidad acallando el insistente
llamado de la selva primitiva; consiste en creer que la existencia
no es lucha de unos contra otros, sino cooperación; consiste
en la tolerancia, en la generosidad, en el desinterés; en la
glorificación de las cosas del espíritu por encima de aquellas
que son objeto de tráfico y que se compran con dinero.

Yo sé que hay muchos que no piensan así. Encuentro sin embargo
mucho más pedestre tratar de resucitar a Nietzsche o a Marx o
a Spengler o _Mein Kampf_, que entre paréntesis, es uno de los
libros más bajos que se han escrito. Y permítame acabar mi tirada:
si viviésemos en un mundo poblado únicamente por hombres superiores,
como tantos que vivieron antes de esta era mecánica: superiores
por la bondad, por la filosofía, por las letras o por las artes;
superiores por el bien, en una palabra, podríamos asegurarnos
una dosis razonable de felicidad muy por encima de la que hasta
ahora hemos alcanzado sin necesidad de aparatos que devoren el
viento ni transmisores inalámbricos de bolsillo. Cuando más yo
pediría algunas docenas de laboratorios donde los investigadores,
que son los monjes de ahora, seguirían luchando contra el dolor
y las enfermedades.

---Todo eso es cierto ---contestó Héctor Berrueta--- pero es
que forma parte de la felicidad relevar al hombre de los esfuerzos
que lo hacen sufrir y eso solo lo da la tecnología. El trabajo
en las minas es todavía en muchos casos una especie de esclavitud.
Los mineros se envenenan con el plomo, con el mercurio o se vuelven
tuberculosos; solo los adelantos técnicos ofrecen una esperanza
de redención. Considere usted el caso clásico entre nosotros:
antes de la conquista los indios eran las bestias de carga de
sus caciques; ahora los camiones eléctricos se deslizan silenciosos
por las carreteras tersas como espejos; cada uno lleva lo que
difícilmente cargarían cientos de «tlamemes». Existen ya muchas
industrias en que basta trabajar cuatro o cinco horas para satisfacer
el consumo. La humanidad comienza a disfrutar del ocio que es
un producto de civilización genuina; eso únicamente es posible
por la tecnología, de la que usted habla tan mal.

---Mire, Berrueta ---dijo Uranga--- ese último argumento suyo
me impresiona. Pero tiene usted que convenir en que en los míos
hay también mucho de verdad. Le voy a proponer un acuerdo: convengamos
en que la tecnología es, o puede ser o quizá deba ser un elemento
de civilización, pero que esta no se realiza sin un fondo de
honda cultura colectiva. Le voy a tapar todas las salidas: cuando
esa cultura no existe la tecnología nos lleva a la barbarie.

---Casi, casi estoy conforme con usted ---contestó Berrueta.

Nos quedamos en silencio; soplaba un vientecillo fresco, pero
la última luz del día era tan suave que nos tenía allí arrobados.
San Miguel conserva sus farolas que se iban encendiendo en las
calles; las cúpulas de las iglesias perdían su volumen y se
recortaban en el cielo de tonos verdes. Nos envolvía una gran
paz.

La voz de Berrueta que brotó de la oscuridad volvió a oírse otra
vez.

---Óigame, Uranga; su café está excelente. Yo uso una probeta
de onda ultracorta, especial para el caso; pero dista mucho de
resultarme lo mismo.

---Habrá que preguntarle a Doña Tula. Se que tuesta el café en
una cacerola tapada para que no se vaya el perfume, que lo muele
en el momento de hacerlo y finalmente que usa una cafetera del
año de la nanita. La receta habrá que pedírsela e ella.

Al escuchar esa conversación pueril, nadie habría imaginado lo
que quienes la sostenían sufrieron durante la invasión. Berrueta
desplegó su energía en un gran organismo de sabotaje; su influencia
se hizo sentir en las fábricas, en las oficinas del gobierno
pelele, en los campos, en las aldeas, en las minas. Los instructivos
que repartió por millones y millones en hojitas que caían de
los aeroplanos o que aparecían misteriosamente en los más inesperados
lugares se consideraron entonces modelos en su género. Los nazis
ofrecieron sumas fabulosas por su cabeza y habla muy alto de
la gente de entonces que en ese ambiente trastocado nadie se
dejó tentar.

En cuanto a Uranga, batióse sin descanso como uno de los jefes
de las guerrillas. El dio el golpe de mano en Tehuacán y agitó
contra los invasores a toda la región del sur.

En estos momentos la figura de Tula sé acercó a nosotros, en
la oscuridad del corredor. Detúvose a cierta distancia, como
queriéndonos identificar. ---¿Está usted aquí, señor? ---Aquí
estoy, Tula. ¿Qué se ofrece? ---Llama el aparato de su biblioteca,
conecté y parece ser Don Remigio que viene en un avión.

Uranga se levantó precipitadamente y volvió después de algunos
minutos. ---Efectivamente es Remigio Guerrero ---dijo---. Viene
en el clipper directo de Londres; llegará a México mañana en
la mañana y me anuncia que estará aquí para comer conmigo. Hace
más de un año que no nos vemos y esa visita es para mí muy agradable;
pienso que nos reunamos algunos amigos. Desde luego aquí nos
encontramos tres; pero veremos si está en el pueblo Néstor Zayas;
llamaremos también a Mauricio Ross, con la esperanza de que sus
flores no lo tengan demasiado ocupado. Televisaremos a Armando
Otamendi a ver si su trabajo en los estudios de cine le puede
dejar uno o dos días libres y finalmente a Víctor Ramos, que
con seguridad está en Yuriria; ojalá que no tenga algún enfermo
grave o un parto inopinado.

Uranga íbase animando al hacer estos planes. La llegada de Remigio
era para él algo más que una visita agradable; era algo que removía
sus más caros recuerdos, que revivía las emociones de su juventud,
cuando ambos recorrían de un extremo a otro el territorio de
la república combatiendo a los nazis. Todavía ahora son jóvenes,
andan entre los cuarenta y los cincuenta años, pero entonces
ellos y otros muchos contaban alrededor de treinta; eran parte
de la generación que salvó a México. Uranga tenía afecto fraternal
por Remigio; la guerra los había dejado a ambos solos.

Todo lo que escribo en seguida no tiene importancia para quienes
se hayan interesado únicamente por la acción de mi relato; son
solo reflexiones de aquellos hombres representativos de este
mundo nuevo.

El día siguiente fue domingo y además 20 de noviembre; en ese
día se conmemoró el cincuentenario de la Revolución. Desde la
ciudad de México difundióse un discurso; el orador dijo que la
Revolución comenzó en aquella fecha, hace cincuenta años, pero
que el compromiso contraído con los que vertieron su sangre para
iniciarla había sido roto miserablemente por quienes usufructuaron
el título de revolucionarios. Hizo una reseña amarga de hechos
bochornosos; de gobernantes ladrones, de cámaras de legisladores
compuestas por analfabetos empistolados, de estulticias sin cuento;
habló de lo que era aquella burocracia corrompida e inepta y
de la miseria de las clases laborantes, que seguían explotadas
sin piedad. Mencionó nombres de apóstoles de pega que tenían
edificios de apartamientos, de líderes que eran dueños de fábricas
donde se burlaba la ley, de campeones del agrarismo que pagaban
en sus haciendas jornales de esclavitud. Habló de la farsa de
la lucha de clases, fomentada por quienes trataban a toda costa
de incorporarse a una plutocracia mezquina y torpe; y analizó
cómo vino formándose, incrustada en todos los estratos sociales,
una clase voraz de parásitos, que roban a la colectividad el
pan que se comen y el dinero que gastan en satisfacer su hambre
atrasado de apetitos vulgares. Nos dijo de los banqueros parásitos,
los rentistas parásitos, los ministros parásitos, los líderes
parásitos, los empleados parásitos; toda la gama del parasitismo.
«Esa clase parásita impidió siempre que pudiera formularse siquiera
un programa para alcanzar el bienestar colectivo, que fue el
ansia de la Revolución. Ha tenido que llegar el mundo nuevo,
después de la invasión, para que asestemos golpes de muerte a
ese cáncer social y para que al salir a los campos encontremos
espigas en las parcelas y sonrisas en los labios de los hombres.
¡Estamos por fin haciendo la Revolución; pero tenemos el pudor
de no llamarnos revolucionarios!» Estas últimas frases las he
transcrito textualmente, porque contienen un tópico bastante
reafirmado ahora y el que creo ya mencioné: los hombres se ufanan
de que son ellos los que están cumpliendo con las promesas de
aquella Revolución. El orador de este aniversario fue Roque Buendía,
que fabrica instrumentos ópticos.

Como esa fecha se conmemora también con eventos atléticos, que
han sustituido a los desfiles de empleados amenazados por el
cese, jugué con el equipo de fútbol de San Miguel en calidad
de interior izquierdo ---que es mi puesto en la oncena de Irapuato---
contra los de San Luis Potosí. Obtuvimos un empate honorable.

A la una y media de la tarde estábamos con Uranga en su casa,
además de quienes nos contábamos como sus huéspedes, Néstor Zayas,
Mauricio Ross y Víctor Ramos. Este último es un médico del Bajío; no
tiene el tamaño de una figura científica, pero ejerce su profesión
con amor en una colectividad que comienza apenas a salir de una
era de barbarie en materia de higiene; Mauricio Ross se dedica
a la floricultura, envía por avión rosas y gladiolas a Nueva
York y al Canadá. En cuanto a Néstor Zayas ha vuelto próspero
el gremio de los tejedores, antes tan miserables, y aunque continúan
tejiendo en sus telares primitivos, gozan de independencia y
de bienestar. Ninguno de estos hombres se enriquece, pero esto
no depende de la índole de sus ocupaciones, sino de que en el
mundo nuevo el dinero acumulado ha dejado de ser un elemento
de poder. Ahora nadie atesora porque fuera de ciertos límites
el dinero pierde su valor y al cabo del tiempo se nulifica; las
instituciones de Seguros Sociales que han comenzado a funcionar
después de la guerra proveen para los que van haciéndose viejos
---con el tiempo proveerán también para la educación de las nuevas
generaciones---; las gentes que trabajan empiezan a disfrutar
ahora de una sensación de seguridad que les era desconocida:
saben que su trabajo hasta los cincuenta y cinco años, realizado
honestamente, las pone a cubierto de las enfermedades y de la
vejez. Lo único que se ha necesitado para realizar esta labor
es que los hombres usen con honestidad su inteligencia y su buen
sentido en favor de la felicidad común. Si comparamos nuestro
mundo nuevo con lo que era hace veinticinco años ---en 1935,
cuando yo nací y todos los pueblos vivían en la zozobra esperando
la guerra--- nos convencemos de que en cinco lustros hemos avanzado
más que en cinco siglos anteriores. Los pensadores se sentían
entonces ufanos de lo que la humanidad había realizado desde
el Renacimiento; pero se luchaba sin tregua por adquirir el poder.
Ese poder estaba primeramente en manos de una minoría capitalista;
después, en los últimos años antes de la invasión, lo iba acaparando
el Estado y las gentes vivían oprimidas por una burocracia gigantesca.
La guerra acabó con esta tendencia suicida y podemos afirmar
ahora que el mundo nuevo que se viene forjando, desde luego no
es nazista, por gracia de Dios, pero tampoco es comunista ni
demócrata en el sentido estrecho de aquellas democracias de hace
tres décadas. Estamos llegando, cada vez con mayor perfección,
al gobierno del pueblo por el pueblo, pero no creemos en la mentira
del voto y hemos aplastado para siempre las corrompidas máquinas
políticas. En México dimos un paso decisivo: desembarazarnos
definitivamente de todas las cámaras legisladoras sustituyéndolas
por cuerpos de hombres que saben algo a fondo y que ponen su
saber al servicio de la comunidad, y al mismo tiempo investidos
de un poder tan absoluto que las tiranías individuales se hacen imposibles;
los encabezados de los diarios anunciando un asesinato perpetrado
por un gobernador o un senador de la república nos parecen ahora
acontecimientos de la edad de las cavernas.

En realidad no sé por qué me empeño en reseñar estas cosas que
son ahora nuestro pan cotidiano, pero es que esta historia que
he pergeñado me ha puesto en contacto con esa vida extraña, que
está próxima a nosotros por los escasos años transcurridos, pero
que parece casi la de otro planeta, según es el cambio que han
sufrido las conciencias.

Me es forzoso abandonar estas divagaciones, para volver a la
trama de los pequeños hechos que vengo refiriendo. Esperábamos
a Remigio Guerrero; a él se había unido en México Armando Otamendi,
hijo de Maclovio, muerto durante la guerra. Armando, que ahora
debe tener cerca de cuarenta años, es actor de cine.

Anunciaron su salida de México como a las doce y media; esperábamos
que llegasen poco después de las dos, pues Remigio venía piloteando
su autogiro, que es una máquina lenta. Uranga hacía versar la
conversación alrededor de su viejo amigo:

---A este Remigio Guerrero le debemos mucho los que vivimos ahora
y más aún le deberán nuestros hijos y nuestros nietos. Si estuviésemos
en el tiempo de las pompas oficiales, cuando los burócratas hacían
fiestas para inaugurar hasta una fuente pública y todo eran acuerdos,
audiencias y condecoraciones, Remigio tendría una cartera o sería
Embajador. En lugar de eso trabaja en su sala de hospital; en
Londres ha estado estudiando a fondo la cirugía de los nervios.
Parece ser que un tal doctor Cooper está realizando cosas extraordinarias.

Después se dirigió a mí y me dijo con acento un poco enfático:
---Fernando, vas a estrechar la mano de un gran hombre; el adjetivo
no es una palabra hueca. Está hecho de la misma carne que nosotros,
pero es grande por el temple de su alma.

Tú, Fernando, estás joven; hace catorce años jugabas al balero
o estudiabas la regla de tres; pero alguna huella conservas sin
duda del drama que nos tocó vivir. Hubo entonces chico de doce
años que se dejara azotar antes que decir «Heil Hitler» y en
una escuela donde unos esbirros nazis buscaban a un profesor
insurgente los chiquillos se pusieron a cantar el himno. Pues
mientras esas cosas sucedían, Remigio iba de una ciudad a otra
organizando la resistencia contra el invasor predicando a todos
la nueva cruzada; los remisos comprendieron entonces el peligro
que se cernía sobre ellos y empuñaron las armas. Remigio iba
a los sitios de mayor peligro; iba a donde se acababa de sufrir
un descalabro, para confortarlos y darles ánimos. Entraba y salía
de las ciudades, invisible como un ser incorpóreo. El llamarse
insurgente era un timbre de orgullo y los nazis temblaban al
sentir la proximidad de las guerrillas. El se multiplicaba, volaba
sobre el territorio ocupado para llevar instrucciones a los lugares
más expuestos, para organizar el suministro de pertrechos o dar
inesperados golpes de mano. Es cierto que sin el poder de las
Naciones Unidas y su ayuda decidida nunca habríamos arrojado
al invasor de nuestro suelo; pero fuimos nosotros quienes lo
desmoralizamos, quienes hicimos tan precaria su existencia que
cada soldado alemán no se sentía dueño ni del terreno donde se
proyectaba su sombra en un día de sol. Fue Remigio uno de los
que despertó ese espíritu en el pueblo: lo comunicó a los hombres,
a las mujeres y aun a los niños. No sé si por su buena o su
mala estrella salió ileso de los años de lucha; quizás si hubiese
muerto tendría altares en todas las casas.

Uranga se iba enardeciendo. Lo escuchábamos con recogimiento
y yo, por mi parte, con una profunda simpatía.

Néstor Zayas dejó oír su voz: ---Tienes razón, Uranga. Si yo
tuviese un hijo lo desearía como Remigio. Pero no salió ileso;
perdió todo lo que constituía su vida. ¡Y en qué forma! A mí
me lo contó todo prolijamente Alonso Quintanar. ¿Lo recuerdas?
El fue gran amigo del viejo notario Don Rodrigo y de Verónica.
Alonso no era sensiblero; pero cuando hablaba de estas gentes
se atragantaba y perdía la voz.

---Pues yo sí los vi, una sola vez ---dijo a la sazón Víctor
Ramos, que era hombre que hablaba muy de cuando en cuando---
Remigio y yo nos conocimos estudiando medicina; él iba unos
tres años adelante, pero éramos amigos y al recibirse, su padre
hizo una fiesta aquí en San Miguel. Yo vine desde Guanajuato.
¿Esto fue hace…? Bueno, no lo recuerdo con exactitud; más o menos
veinte años. Aquel convite no lo olvidaré nunca. Todo exhalaba
un vaho cálido de amor familiar; los tres muchachos eran guapos
y Verónica se veía en ellos. Se quedaba contemplándolos fijamente
y parecía como si el espacio no los separara. Ella andaba entonces
por la cincuentena; muy blanca y de pelo muy negro con sus hilos
plateados; no era lo que podría decirse hermosa ---cuando menos
a esa edad--- pero tenía un aire muy distinguido de señora provinciana.
Aquella casa dejaba una honda sensación de dicha. Regresé al
día siguiente a Guanajuato y no volví a verlos más; vino la invasión
y con ella la lucha; fui médico de las guerrillas y a Remigio
lo seguí viendo con frecuencia. Después supe todo lo que pasó.

Yo había quedado sin decir nada. Pero entonces me dirigí a Uranga:
---Oiga, ingeniero, quiero decirle una cosa. No es la primera
vez que escucho conceptos semejantes cuando se habla de Remigio
Guerrero y de los suyos. Permítame hacer una sugestión: alguien
debería escribir algo; esas vidas piden cuando menos quien recoja
los datos dispersos y los consigne con cariño.

Entonces Héctor Berrueta me dijo: ---No está mal lo que dices
y ya hablaremos de ello. ¿No le parece, Uranga?

---Ciertamente ---contestó este--- ya hablaremos de ello.

Esta charla la teníamos en la biblioteca; de ella se salía a
una terraza desde la cual se veía el panorama. Era un día fresco
y claro; el cielo estaba azul, sin una nube. Uranga salía de
vez en cuando a inspeccionar el horizonte; de pronto me llamó:
---Ven, Fernando; tú eres el de mejores ojos.

Salí y efectivamente, sobre los cerros volaba un insecto extraño.
Todos acudieron a ver la llegada; el pequeño monstruo fue creciendo
y a los pocos minutos empezaron a distinguirse las alas rotatorias
de un autogiro. No tenía ese vuelo raudo de los aviones de alas
fijas, sino que parecía un enorme molino arrancado del suelo
por un vendaval. Después la cabina se aclaró a nuestra vista
y el estruendo de la hélice llenó el espacio sobre nuestras cabezas.
El aparato inició una vuelta amplia alrededor del pueblo; entonces
Uranga dirigióse al frente de la casa y con dos banderolas blancas
en la mano comenzó a hacer señales. Por dos ocasiones el autogiro
pasó arriba de nosotros rozando los árboles; salieron de su panza
las dos ruedas del aterrizaje y de una altura como de cien metros
se desprendió con lentitud, casi verticalmente. La velocidad
disminuía a medida que se acercaba al suelo; los últimos cinco
metros los bajó tan despacio que se hubiera dicho se iba a quedar
ahí para siempre. Al fin se posó con suavidad en el verde pasto
que se extendía ante el caserón.

La hélice con un esfuerzo lleno de fatiga dio las últimas revoluciones
y el aparato entero se estremeció con una ligera sacudida. Abrióse
la cabina apareciendo primero la figura maciza de Armando; lo
conocía por verlo con frecuencia en las pantallas de los televisores.
Tras él asomóse Remigio. Los abrazó a todos; a mí me estrechó
la mano al mismo tiempo que sonreía.

Me habían dicho que tenía menos de cincuenta años, pero a primera
vista representa sesenta. Vinieron a mi mente los versos del
poeta: «qué cascos de nieve que pone la suerte ---qué arrugas
precoces cincela en la cara». Sin embargo, fijándose mejor en
él se descubre en sus ojos, a pesar de sus cabellos grises y
de los hondos surcos que le dan a su rostro la dureza de una
escultura, un brillo juvenil. Después, al contemplarlo allí sentado,
bajo la sombra de los árboles, esa impresión de vejez y de severidad
se fue borrando. Parecía un atleta en reposo; hablaba pausadamente
y una gran sonrisa lo iluminaba.

Comenzó a charlar; nos contó cosas de Londres y de París. En
Francia la guerra ha transformado a las gentes; las ha tornado
austeras. Solo piensan en reconstruir material y espiritualmente
su suelo devastado. Inglaterra afirma las bases de un imperio
nuevo. La ideología sobre la que ahora se edifica la existencia
ha simplificado muchos problemas y un londinense se siente más
cerca que nunca de las muchedumbres de la India. Remigio hablaba
de estas cosas con entusiasmo; se veía claramente que el optimismo
lo embargaba; era el optimismo de aquel, que al final de la vida,
encuentra que todos sus sacrificios lo han acercado a la meta.

Uranga le contó cosas de México, del entusiasmo con que todos
trabajan en la construcción del Mundo Nuevo.

Continuábamos en el jardín y la conversación se generalizó. Hacía
calor a pesar de la estación. Refugio nos trajo grandes jarras
de sidra fría. ---A ver qué les parece dijo Uranga--- es de las
huertas de la presa.

---Está magnífica ---comentó Armando--- pero tenemos hambre.
Uranga, llévanos a comer.

En la noche pasábamos la velada repartidos en la biblioteca y
la gran estancia. En este último sitio Remigio, Armando y el
doctor Ramos buscaban en el televisor las últimas noticias. Uranga,
Berrueta y yo fumábamos en un rincón rodeados de libros. Uranga
me dijo:

---Oye, Fernando, realmente valdría la pena escribir algunas
páginas sobre las gentes de Remigio ¿por qué no lo haces tú?
Todo lo ocurrido tiene un valor ejemplar; la vida debe cimentarse
sobre principios inmutables. Van y vienen los años y los siglos
y los hombres siguen siendo buenos y malos; veraces y mendaces,
leales y pérfidos; laboriosos y parásitos; caritativos y crueles;
generosos y egoístas. Estas clasificaciones tan simples son viejas
como el mundo, pero todavía ahora tienen el mismo significado
eterno. Al fin de los tiempos unos quedarán ---como dice el mito---
a la diestra del Padre y otros a la siniestra. Con unos se construyen
los mundos nuevos como el que ahora nace; con otros se forjan
las opresiones, se acaba con la libertad y se aherroja a los
débiles.

Berrueta interrumpió: ---Déjame hacer mi papel de Sancho Panza
con sentido práctico. Mira, Fernando, vas a quedarte aquí en
diciembre; son tus vacaciones anuales. Documéntate, busca en
los archivos, visita los lugares. Haremos una pequeña monografía;
queremos así preservar del olvido a esas gentes y publicaremos
un opúsculo de treinta o cuarenta páginas en magnífico papel
y con buenas ilustraciones.

Yo fui débil y acepté el encargo.

Pero me he extraviado. En lugar de hurgar los archivos del curato
para ver las fes de bautismo y el libro de actas del Registro
Civil con objeto de precisar las fechas en que se casaron Rodrigo
y Verónica y nacieron Remigio, Patricio, y Ana, me dediqué a
averiguar qué se decían en la intimidad de la casa, lo que ella
guisaba para tenerlo satisfecho, los cuidados que prodigaban
a los hijos y los sacrificios que realizaban para verlos crecidos
y bien educados; sostuve largas pláticas con gentes que los visitaban.
En cambio no conseguí un solo retrato, ni una carta, ni un documento
auténtico; esta historia, como monografía, es un fracaso. No
sé cómo he podido terminarla. {.espacio-arriba1}

He releído la historia de Rodrigo Guerrero y Verónica Arriaga;
la labor fue superior a mis fuerzas. Pretendí revivirlos sin
lograrlo; repaso estas páginas y aunque a veces parecen tomar
cuerpo en ellas y amar y sufrir, están lejos de lo que fueron,
cuando pasaron por la existencia con su envoltura mortal. Tampoco
he conseguido hacer sentir cómo un mecanismo diabólico, ideado
por un monstruo, trituró con una mística estrafalaria ---poblada
de deidades sin sentido como la raza, la sangre y la fuerza---
las cosas mejores de esta tierra: el amor y la libertad. {.espacio-arriba1}

No me he resuelto a destruir estas páginas. Las he dejado sin
corregir, con los capítulos en desorden, con sus numerosas lagunas
sin colmar y la cronología plagada de errores; pero las he escrito
fervorosamente. Ahora las envuelvo con una meticulosidad de amante
que empaqueta las cartas de una novia desaparecida. Y las voy
a colocar aquí, en la añosa chimenea, en uno de los escondrijos
donde quiero creer que depositaba sus secretos algún hidalgo
del siglo ++xviii++. Allí tendrán una tumba recatada; cuando alguien
las descubra dentro de cien años quizás encuentre en ellas el
calor que ahora les falta; tal vez el tiempo las anime con un
soplo de vida.

+++FIN+++ {.espacio-arriba2 .centrado}

Julio de 1942 \
Abril de 1942 {.espacio-arriba2 .izquierda .sin-sangria}

</section>
