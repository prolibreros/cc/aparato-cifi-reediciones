<section epub:type="chapter" role="doc-chapter">

# TROKA, el poderoso (fragmento)

## TROKA, el poderoso, habla

Los niños saben ya quién es +++TROKA+++, \
su amigo. ¿Quién los lleva por la ciudad? \
Un gran camión: \
es +++TROKA+++. \
¿Y de México a San Luis Potosí? \
La locomotora: \
es +++TROKA+++. \
Los niños saben que se puede cruzar el océano en un hermoso barco de vapor: \
es +++TROKA+++. \
+++TROKA+++ puede volar convertido en aeroplano, \
en dirigible; \
puede bajar al fondo de los mares y ser el \
submarino; \
atravesar las montañas abriendo túneles, \
trepar por los elevadores en los edificios gigantes. \
En los puertos \
sus grandes brazos mecánicos \
son las grúas. \
En las ciudades \
se levanta dominador, \
con las chimeneas. \
+++TROKA+++ puede traer la luna hasta nuestra tierra \
y enseñar sus canales, sus volcanes muertos: \
es el telescopio. \
Su voz, dominando las distancias, \
salvando todos los obstáculos, \
nos llega desde muy lejos: \
es el radio. \
En las noches, \
sobre las pantallas, \
nos da el gesto, el ademán y la voz de artistas lejanos: \
+++TROKA+++: \
es el cinematógrafo. \
es el espíritu \
de las cosas mecánicas, \
de lo que el hombre ha inventado, \
ha creado, \
con su inteligencia, \
con su esfuerzo. \
El hombre, piensa \
crea, \
elabora, \
construye, \
hace máquinas. \
+++TROKA+++, el poderoso. \

### Primera aparición de TROKA, el poderoso

+++TROKA+++, el poderoso, habla:

---Oíd mi sonoro canto que asciende por mi garganta de cristal
y se amplía en el magnavoz de mi boca. Soy +++TROKA+++, el poderoso.
El espíritu del metal y de la electricidad. ¡Qué grande, qué
fuerte, qué resistente soy! Formado estoy de duros planos pulidos
y brillantes. De aluminio son mis piezas internas que me dan
agilidad, ligereza; sobre balas de acero giran mis articulaciones.
Mi estructura es de hierro y en su interior zumba como un corazón,
el motor eléctrico que me alimenta. Tengo como los hombres una
cabeza; únicamente que la mía es de bronce, y en ella encierro
un cerebro hecho todo de aparatos electromagnéticos; de este
cerebro salen y se distribuyen mis nervios, hilos metálicos que
corren a través del mundo y transmiten las órdenes para que actúen
los hombres. En las noches, como pequeños soles, protegidos por
convexos cristales, torrentes de luz alumbran los caminos, incendian
las ciudades: son los ojos ultrahumanos de +++TROKA+++, el poderoso.
Y tengo un canto, el canto industrial del mundo, que el fonoradio,
voz gigante, transforma en un himno. Escuchadle: desde él se
elevan los gritos de las sirenas que llaman a los hombres a su
trabajo en las fábricas y dicen adiós a la tierra en la partida
de los barcos que unen los continentes. Lleva el alarido de las
locomotoras que cortan los panoramas campesinos. Escuchad el
rumor solemne de los motores y el jadear impaciente de las máquinas
que llenan los talleres. Esos clamores, son los de las vastas
multitudes de las grandes ciudades; sobre ellos arrojan sus aullidos
feroces los claxon agresivos, las campanas de los trenes eléctricos.
Yo soy el genio mecánico que nació de todos esos ruidos, de todos
esos impulsos, de todos los afanes de este tiempo.{.espacio-arriba1}

Soy, cuando me detengo, la rígida y levantada majestad de las
chimeneas; cuando camino, el veloz automóvil, la forzuda locomotora,
el barco que domina los mares. Las torres inalámbricas elevadas
al cielo, son como brazos míos que recogen el rayo de la tormenta
y la onda lejana. Las grúas son mis músculos de acero. La rígida
estructura de las calderas, semeja el torso de un gigante y puede
ser mi cuerpo y las cajas de los automóviles con sus aristas
bruñidas, al frente los redondos faros de mis ojos que tras­pasan
la sombra y los salientes labios del magnavoz, hacen pensar en
las cajas que encierran los magnetos. Mi estructura es mecánica
para que resista la vida actual violenta, ruda, cambiante y siempre
en movimiento. Estoy construido por todos los hombres utilizando
todos los elementos; soy la síntesis del genio universal. Los
herreros con sus martillos que despiertan la aurora; con sus
fraguas que chisporrotean en las noches y arrojan las estrellas;
los mecánicos que vigilan los organismos de acero; los paileros
que remachan la altura de los edificios en las armazones metálicas;
los albañiles que les dan cuerpo de cemento; los electricistas
que despiertan las noches; los mineros que arrancan a la tierra
su fuerza para entregarla a la industria; los fundidores que
en los altos hornos cuajan el rojo fulgor de los bloques de hierro;
los laminadores que estiran y extienden esos bloques de hierro;
los ingenieros que transforman cables y láminas en puentes audaces;
los sabios que en sus laboratorios arrebatan cada día una nueva
fuerza a la Naturaleza; los hombres negros que recogen la sangre
de los árboles del caucho en las selvas del Amazonas; los hombres
blancos que cortan la madera en el Canadá; los hombres amarillos
que siembran el arroz en las llanuras de China; los ingleses,
los argentinos, los siberianos, los turcos; todos los hombres
de todas las razas, de todos los mundos, de todos los climas,
están en mí. Soy el radio que cruza los mares y suena en todas
las latitudes; el mensaje eléctrico que nos cuenta lo que hacen
los hombres del mundo; la voz del tiempo; el clamor universal;
el grito humano. Yo levanté los edificios, que horadan las nubes;
los túneles que atraviesan los mares. Soy el que sale de las
minas, de las fábricas, de los talleres, de los laboratorios.
Todo está en mí, y ya sabréis al conocer mi historia, cómo me
levanté por el es­fuerzo de los hombres, mis hermanos. Soy +++TROKA+++,
el poderoso.

### Segunda aparición de TROKA, el poderoso

Esta mañana han venido hasta la Estación de Radio que me sirve
de domicilio, deseosos de conocerme, Anselmo y Raymundo, dos
escolares que escucharon mi primer discurso. Siguiendo la voz
del radio, los dos pequeños investigadores han hallado mi casa
y su visita me ha sorprendido gratamente.

---Venimos a verte, poderoso +++TROKA+++, ha dicho Raymundo;
queremos cerciorarnos de la verdad de tu existencia, queremos
ver cómo funcionas.

Y han trepado por las escaleras metálicas que suben por mis piernas,
hasta el balcón que rodea mi cuerpo alto como una torre.

Después han llegado a mi cabeza por donde asoma la bocina de
mi boca.

Han examinado los faros que me sirven de ojos; y audazmente,
han penetrado por mis oídos hasta mi cerebro.

A la salida, Anselmo me ha dicho:

---Queremos ver tus ojos en la sombra, cuando los enciendes;
serán seguramente como el faro que vi la noche que llegué a Veracruz,
y cuya luz, muy fuerte, penetra profundamente en la oscuridad
del mar. Sus convexos cristales hacen pensar en la fábrica gigantesca
que los construyó. ¡Qué poderosos hornos habrán fundido el vidrio
que luego sale en rojos chorros hacia los moldes o es tomado
en pequeñas partículas por los vidrieros con sus largos tubos
metálicos, que soplan con fuerza para darle forma! ¡Qué hermosa
es la industria del vidrio! Pero los enormes cristales de tus
ojos habrán sido hechos de otra manera, fundidos en moldes muy
grandes, muy gran­des…! ¡Quién hubiera estado en el momento en
que corrían los rojos chorros hacia esos moldes!

Después ha hablado Raymundo:

---Sabes, +++TROKA+++, tu cerebro es parecido a una central de
teléfonos que visité hace algunos meses con los niños de mi escuela;
allí se reúnen los hilos que vienen de todas partes; únicamente
que, frente a los tableros de los teléfonos, unas señoritas trabajan
colocando transmisiones para unir los números que solicitaban
los clientes, y en tu cerebro todo se hace automática­mente.
Dicen que así son ya algunas estaciones telefónicas de los Estados
Unidos y de Europa, que funcionan sin que ninguno las vigile.
¡Qué admirable es lo que hemos visto! Tu cerebro es una máquina
maravillosa: quieres andar, y el motor que regula tus movimientos
de las ruedas, parece que recibe una orden y la obedece desde
luego; entonces eres un automóvil. Quieres hablar y te transformas
en un radio que dice tus palabras. ¡Qué bello! En tu cerebro
se está como en un gran salón que estuviera adornado con maquinaria.

Después, los dos chicos han querido visitar mi cuerpo; he abierto
para ellos la pesada puerta de acero que cierra mi estómago y
han penetrado a mi central eléctrica.

Aquí es donde llegan los cables que desde Necaxa, Tepuxtepec,
Tepeji y Lerma, por medio de grandes torres de acero distribuidas
en el campo a iguales distancias, traen la fuerza eléctrica que
las caídas de agua, las cataratas, las poderosas corrientes crean
por medio de colosales turbinas y que luego en las ciudades,
poderosos dínamos multiplican para distribuirla al uso de los
habitantes.

En estas centrales puede decirse que se almacena también en gigantescos
acumuladores, igual que la almaceno en mi estómago.

Qué extraordinario es todo lo que se relaciona con la electricidad;
en las noches se da vuelta a un botón y la luz se enciende brillante,
deslumbradora.

Esto ya no llama la atención de los niños; pero si ustedes hubieran
vivido en los días en que las gentes se alumbraban con velas
de sebo, con pobres velas que había que estar despabilando en
cada momento; si vivieran en esos pueblos alejados de todo progreso
donde se siguen alumbrando en esa forma, entonces comprenderían
lo que vale la lámpara eléctrica que da más luz que cien velas
y no requiere ser cuidada.

Pues bien, mi estómago es una central eléctrica: allí llegan
los alambres de diversos productores de fuerza eléctrica y yo
almaceno en ella, en mis acumuladores, la que necesito para mi
existencia.

Anselmo y Raymundo lo han visto y contarán a ustedes, próximamente,
cómo se usa esta energía; dirán a ustedes lo que vean en su viaje
por el interior de mi cuerpo, que van a seguir.

---¿Quién de ustedes quiere acompañarlos? +++TROKA+++, el poderoso,
los invita cordialmente.

### Tercera aparición de TROKA, el poderoso

Anselmo y Raymundo, mis pequeños amigos, siguen en el interior
de mi cuerpo dedicados a observar el funcionamiento de mi extraordinario
mecanismo.

Después de visitar mi central eléctrica, suben hasta el segundo
piso por escaleras de hierro, que dice Anselmo le recuerdan las
que vio en la sala de máquinas de un gran vapor que visitó en
Veracruz.

Han conocido mis pulmones, gigantescos compresores de aire que
me sirven para mover las turbinas neumáticas que, a su vez, hacen
girar los dínamos que multiplican la fuerza eléctrica.

Mis curiosos camaradas suben ávidos de conocer nuevas máquinas
y se detienen a contemplar la enorme sirena con que anuncio mi
salida de la Estación de Radio, con que llamo a mis amigos distribuidos
en todos los horizontes.

---Es, dice Raymundo asombrado, como las sirenas de las fábricas,
pero mucho más grande; su grito, cuando lo oí por primera vez
me recordó la marcha de los trabajadores respondiendo al llamado
de los silbatos y pensé que al poderoso rugido respondía una
marcha universal de millones de hombres y mujeres, de obreros,
camino de una jornada hacia una fábrica tan grande que llenaba
la tierra. Siempre había oído con asombro ese rumor poderoso
que principiando como un leve zumbido se va adueñando del aire;
pero desde que lo escucho lanzado por tu garganta, me entusiasma
como si escuchara un himno al trabajo y al esfuerzo de los hombres.

En honor de mis dos inteligentes visitantes he hecho sonar la
sirena y después de un momento de silencio, al concluir el grito,
el gigante clamor vuelve a repetirse devuelto por el eco, cuando
ha llegado hasta los cerros próximos.

Después han querido visitar mi garganta, poderoso aparato de
radio; han visto los enormes bulbos que condensan la voz recogida
de todos los ámbitos del mundo y las sensibles placas donde el
lejano ruido se hace audible.

---Me siento, dice Anselmo, como un insecto metido en la caja
de radio de mi casa; si las hormigas piensan, seguramente que
han de hacerse las mismas reflexiones que yo me hago ahora, cuando
se introducen en el para ellas extraño mundo que canta y hace
música.

Yo les he dicho:

---Callad un momento, vais a escuchar ahora mismo la Música Mecánica
de motores y hélices y la he hecho llegar desde la remota distancia
para ellos.

---¡+++TROKA+++, poderoso +++TROKA+++ ---grita Anselmo entusiasmado---,
haz que se oiga nuevamente esa música extraordinaria; quisiera
que la escucharan mis compañeros de escuela; es tan bella! Estoy
seguro de que les habrá de gustar.

---Lo haré con gusto ---le contestó---; espera un momento, voy
a orientar mis antenas hacia el norte: confío en que esté en
el aire esa música… Sí; sí; aquí está; escuchémosla.

---¡Qué maravilloso es todo esto!--- vuelve a exclamar Anselmo
lleno de júbilo. Ahora podemos escuchar cuando nos plazca y solo
por tu poderosa voluntad, la música mejor del mundo, no importa
en dónde se toque. Es suficiente con que tus antenas la recojan
del aire y luego la condensen los aparatos de radio en tu garganta.
Y pensar que hace algunos años, si se quería oír algún músico
famoso, o algún cantante de voz privilegiada, era necesario viajar
hasta donde él estaba, empleando para lograr esto, mucho tiempo
y mucho dinero. Únicamente los ricos, los ociosos, podían gozar
de esa satisfacción, y ahora hasta el más pobre puede, cuando
quiere, sin gasto mayor, gozar con la música de los grandes maestros,
con los mejores cantantes. Te lo debemos agradecer a ti, grande
y poderoso +++TROKA+++.

---Mis buenos amiguitos ---les respondo agradecido--- habéis
visto ya mi cuerpo: habéis observado que no hay nada misterioso
ni mucho menos milagroso en él, que está formado con los aparatos
mecánicos y eléctricos que ya se conocen, únicamente que unidos
para lograr un fin.

Habéis sido testigos de cómo funciona este cuerpo de metal hasta
parecer como si lo moviera una voluntad humana; es el prodigio
del genio de los hombres.

Pronto viajaremos por todo el mundo para saber cómo han hecho
lo que al fin se condensa en mi cuerpo.

Iremos a visitar las minas, las fábricas, los laboratorios y
las escuelas: todo lugar donde el genio del hombre, su trabajo
y su acción, tienen un asiento.

Viajaremos por el aire, bajaremos al fondo del mar, y acaso,
acaso, lleguemos en un viaje feliz hasta el otro mundo, hasta
una de esas estrellas que en las noches nos miran con sus luces
temblorosas.

Ahora, mis pequeños amigos, subid por mis brazos, contemplemos
el panorama desde mi altura, acerquémonos al cielo. ¡Arriba…
arriba… compañeros!

</section>
