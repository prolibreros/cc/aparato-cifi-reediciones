<section epub:type="foreword" role="doc-foreword">

# La experiencia poética del resplandor del ser

En 1947, Rafael Bernal publica _Su nombre era muerte_. El
autor de _El complot mongol_ tenía entonces treinta y dos
años y cargaba experiencias que dejarían en él una huella
honda, definitiva… En efecto: católico, Bernal se había
comprometido con el sinarquismo, conoce el ejercicio de
la guerra en los campos de México y el clandestinaje en
las ciudades donde se manifiesta como agitador social.

Encarcelado en varias ocasiones ---la última, a raíz de
encapuchar la estatua de Benito Juárez, en la alameda
central de la ciudad de México---, el presidente Miguel
Alemán, quien por esa acción decreta el 21 de marzo como
fiesta nacional en desagravio al oaxaqueño, termina
por indultar al escritor, quien se resiste a aceptar el perdón
por considerar que no había habido motivo qué perdonar.
Sin embargo, la orden presidencial se cumple
contra la voluntad del novelista.

Espíritu crítico, observador y lúcido, Rafael Bernal
se desengañaría años más tarde del movimiento sinarquista
por considerar que cedía a intereses de banqueros
y terratenientes, perdiendo así sus esencias campesinas
que pugnaban por el respeto y la conquista de la pequeña
propiedad campesina.

En todo caso, en el año de 1947, el autor había dado
testimonio ya de un profundo desencanto en la ruta social
y política que seguía su país al publicar, en 1945, una hermosa
y nostálgica novela _Memorias de Santiago Oxtotilpan_,
donde el pueblito de ese nombre narra su historia que es
un constante volver a empezar hasta quedar invadido, según
los versos de López Velarde, de una vaga tristeza
reaccionaria. Si en este libro, Bernal se muestra como un
maestro de prosa narrativa, a la que imprime un tono lírico
que va impregnando al lector sin mengua de esa eficacia
que tan a menudo olvidan los narradores contemporáneos
y que consiste en no olvidar la primacía de la
acción, en _Tres novelas policiacas_, publicado el año siguiente,
no solo conserva esas virtudes sino que se vuelve
el iniciador del género en la novela latinoamericana.
Obras de aprendizaje, aunque logradas, Rafael Bernal
queda en situación de escribir la que es, a mi juicio, una
de sus dos obras maestras: _Su nombre era Muerte_.

Y es que estamos ante un relato de ciencia ficción y
de misterio pero, además ante una novela teológica y política
que en la mejor tradición chestertoniana anticipa
el mundo de Orwell y conlleva la densidad humana de
una novela de Greene o de Bernanos. Una novela contemporánea
desde la raíz porque a lo mencionado anteriormente
se añade que parte de un pesimismo radical:
un mundo que se desmorona sin espacio ya para la esperanza
y que plantea, en su sorprendente final, la necesidad
íntima que anida en el corazón del protagonista ---y
que el autor nos hace sentir como el de cualquier hombre
de aquí y ahora--- de reconstrucción del mundo desde
el deseo que anida en el núcleo del ser humano y le
hace presente la generosidad básica de la existencia. Y
es que las fuerzas del mal que se desencadenan en el libro
tienen por causa el desengaño sin límites de aquel
que se supo partícipe del bien, la bondad y la esperanza
supremas y buscando hacerlos en sí chocó con su entorno
hasta llegar al desengaño y a la lucidez destructiva.
Mas no escapa el hombre de sí; el yo-mismo remite al
yo-otro en un ansia de comunicación que solo el amor
puede restituir.

Rafael Bernal nos hace vivir la experiencia poética
del resplandor del ser que brota de donde menos se podía
sospechar, esto es, de un ahondamiento en el mal, de
manera que una alianza con las fuerzas oscuras devuelve
a la criatura al seno de Dios. La criatura que no se ha
perdido en los laberintos de la razón y a quien la imaginación
restituye las luces posibles del mundo.

_Su nombre era Muerte_ plantea, ni más ni menos, la existencia
de un estado totalitario ---semejante al de Orwell
en _1984_--- pero que aquí no es obra de los hombres, sino
de los moscos, y que pretende la destrucción del género
humano. Y la quinta columna es un hombre, el protagonista,
que vive aislado en la selva chiapaneca porque odia
a la humanidad y ha reorganizado su vida entretenido haciendo
el bien, contra su voluntad, a los indios lacandones
quienes le han tomado como una divinidad. Les hace
el bien para entretenerse, para que las horas, los días, los
meses no se le hagan en exceso largos, porque algo dentro
de él le ha impedido darse la muerte. Pero ese hombre,
como ya he señalado, no es una víctima de la razón
y helo aquí que observa a los moscos, días, meses, años; y
como ese hombre es un aficionado a la música, logra recrear,
a través de su flauta, el lenguaje de los moscos hasta
comunicarse con ellos. Se volverá su agente en la tierra,
la quinta columna, el médium necesario para llevar a
efecto su plan de dominación de la humanidad.

Y Rafael Bernal logra que creamos todo esto hasta entregarnos
la organización del reino de los moscos que es,
de hecho, análogo a un estado totalitario proyectado a
una sociedad de futuro que Orwell y Huxley también vaticinarían,
que hoy, en fin, no sentimos tan lejano por el
desarrollo tecnológico alcanzado.

Llega, entonces, una expedición de antropólogos a la
selva. Se ponen en contacto con el protagonista de la novela.
Este siente el renacimiento de eros ante la visión
de la señorita Johnes, una bella alemana, auxiliar del
científico jefe, el doctor Wassell, quien ha sido su tutor,
la ha sacado del desamparo y la pretende. Mas hete aquí
que la señorita Johnes se ha enamorado del músico
Godínez, a quien el alemán ha sacado también de la miseria
dándole un trabajo en la expedición. El protagonista
experimenta la traición por venir. El protagonista no
puede razonar, está impedido de ver que Johnes y
Godínez son jóvenes, incluso se ha vuelto ciego para experimentar
que su visión de la deslealtad, que preside el
corazón del hombre, está motivada por su apetencia carnal
de la alemana.

¡Qué mejor momento podía darse para desencadenar
la fuerza brutal y sin asideras del imperio de los
moscos!

Rafael Bernal imprime un _tour de force_ sorprendente
a su historia que se ha vuelto ahora una de amor y de deseo.
El odio sin asideras del protagonista le lleva al enfrentamiento
con Dios, cual debió haber sido aquel de
Luzbel. Y es que él es, a su pesar, un creyente, pues como
bien vislumbró Pascal, «te busqué porque ya te había
encontrado».

La lucha entre el Bien y el Mal se desencadena. ¿Qué
sería Dios en el reino de los moscos? Pregunta que da
lugar a un planteamiento teológico con consecuencias
políticas insospechadas para el lector y que da un máximo
de intensidad al relato.

_Su nombre era Muerte_ es un libro narrado en un estilo
puro y clásico donde los tres elementos que Graham
Greene destaca para la construcción de novelas alcanzan
la máxima eficacia: movimiento, acción significativa,
personajes vivos. El lenguaje, en cuanto a las palabras,
debe estar al servicio de los tres elementos que constituyen
una gran novela. Y resulta que un escritor antes de
los cuarenta años se encuentra con frecuencia demasiado
apegado a sus propias palabras, a su propio punto de
vista. Bernal, en _Su nombre era Muerte_, ha logrado el distanciamiento
necesario, verdaderamente sorprendente a
la edad en que escribió la novela, que le ha permitido
confeccionar una en que su visión del mundo se vuelve
la de todo lector sensible y atento.

Novela de múltiples lecturas, el autor ha logrado el
prodigio de asegurar una primera en la que el involucramiento
con la historia es tal que luchamos por apagar las
diversas reflexiones que nos asaltan, queriendo saltar sobre
las páginas para devorar el relato; lo que no hacemos
movidos por el placer del texto que nos exige la permanencia,
la lectura despaciosa y el temor, que de repente
se hace presente, de que hemos andado ya demasiadas
páginas y el libro debe más pronto que tarde concluir.

_Su nombre era Muerte_ es, sin lugar a dudas, una de las
mayores novelas en la historia de la literatura mexicana.

Francisco Prieto {.espacio-arriba1 .derecha}

</section>
