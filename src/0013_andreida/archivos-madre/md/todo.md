<section epub:type="chapter" role="doc-chapter">

# Capítulo I

+++Había+++ inusitada actividad en el colegio de St.
Mary, ese día en que da comienzo nuestra historia.

Bueno, después de todo, bien pudiera no ser esta una verdadera historia, ya que la calidad íntima
de su naturaleza la acerca, con más justeza, a la leyenda; una leyenda maravillosa de inteligencia y
amor, en el más sublime de los connubios. Pero, a
fuer de honrados con nuestros lectores, insistimos
en que se trata de una historia, si bien nos inclinamos nosotros mismos, a pensar que es todo lo contrario.

Una historia sucedida en un espacio de tiempo
reciente, actual; casi pudiéramos decir que aún se
aprecia en ella la brillantez de lo nuevo, la lisura de su mocedad… o, acaso, acaso no ha sucedido aún… Pero, no oscurezcamos artificialmente lo que vosotras Melita, Consuelo, Victoria, Teresa, Tekla, Ingeborg, sabéis que ha sucedido… ¡Tonta de mí…!
Es verdad, no lo sabéis… Recapacito y pienso que
a vuestro lado tuvisteis aquel monstruo de formas
bellas, exquisitamente refinadas, de presencia cautivante y no visteis nada sobrenatural en él, a despecho de vuestra innegable extrañeza que, de fijo
no os detuvisteis nunca a analizar debidamente.
¡Aquella criatura maravillosa que, encerrando un
peligro mortal para la especie, se hizo llamar por
vosotras Andréïda! Recordadla en mi honor… se
envolvía en la tela preciosa del artificio… tenía
pretensiones de perfección…

Mas es natural que todo esto así sea… ¿Quién
de vosotras podrá decir, a ciencia cierta, en mi relato: esta soy yo, aquella eres tú…? Y, sin embargo, y, a pesar de vuestra íntima creencia, sois vosotras y nada más que vosotras…

Y, ahora, continuemos:

Decíamos que aquel día el St. Mary hervía de
actividad. Los amplios corredores del soleado edificio contenían difícilmente el ir y venir de jovencitas de oscuros uniformes y altos cuellos. ¡Uniformidad en el vestir que pasaba la plancha inconsciente, artificiosa y siempre superficial de lo común en el aspecto físico como en el espiritual!

En contraste con la aglomeración juvenil de los
pasillos, se dolían de su desierto los campos de
tennis y el imponente tapete verde aterciopelado
que constituía la pista de recreo y que también extrañaba, en aquel día, la inquietud alegre de los piececitos femeninos que solían hollarlo a diario.

Y era que las colegialas se aprestaban a regresar a sus hogares. Las que habían cursado el último año, lo hacían para siempre; después de haber
recibido el consabido pergamino, enrollado pulcramente y sujeto por bello listón morado y naranja que atestiguaba la inutilidad de unos estudios, puramente de ornato, y con el imperdonable pecado de enviar a aquellas chiquillas a luchar frente a la vida, todavía más inermes por infatuadas, con el solo fárrago de un conjunto heterogéneo de prejuicios y conceptos falsos; los que, sin duda, influirían más tarde, fatalmente, en el desarrollo de sus vidas.

Relajada la disciplina en aquel día, y que nunca fuera lo suficientemente enérgica para ser beneficiosa, el castellano triunfaba, con algazara, en las bocas frescas de las colegialas, y nadie podría decir que se trataba de un colegio en los Estados Unidos del Norte. Aparte de que, la abundancia de cabecitas morenas proclamaba, bellamente, el triunfo de la latinidad.

Por un instante, hubo un revuelo en el corredor
principal. Se acercaba el grupo de las graduadas a
quienes las demás colegialas se apresuraban a abrir
paso con una especie de respeto; un poco por ese
concepto hueco de las distancias que tan bien saben inculcar en esta clase de colegios y que de tan
firme manera arraiga en la juventud burguesa; y,
otro poco, por ese complejo de inferioridad de nuevos ricos que en épocas pasadas les hiciera sentir
una cascada de aristocracia y del cual, el burgués, no
ha podido desprenderse al paso de cien años, como
si una maldición ancestral todavía pesase sobre él.

Entre las graduadas destacaba, por lo desgarbada y poco femenino de su indumentaria, la profesora a quien todas llamaban simplemente Nelly, quizá porque apenas ha dos años era todavía una colegiala como ellas. Punto menos que imposible era el adivinar la juventud extrema de tan extraña personita; puesto que unas monumentales gafas azules, harto antiestéticas, cargaban de años la feucha
carilla de expresión bondadosa. Expresión que inútilmente trataba de desvirtuar el ademán brusco
de los brazos, demasiado largos, que se empeñaban
en mentir actitudes rígidas y un tanto viriles.

De pronto, y todas a una, pidieron a Nelly el
que las invitase, para la tarde, en reunión de despedida, insinuando, al propio tiempo, que esta se llevase a cabo, como en habituales circunstancias, en
el minúsculo departamento individual de la primera. A lo que poco después accedió la joven profesora,
no sin haber presentado resistencia, aludiendo como
principal razón de su actitud reacia, el que apenas
si contarían con suficiente tiempo, todas ellas, para empaquetar sus cosas. Un torbellino de aplausos,
besuqueos y abrazos premiaron, al punto, su docilidad bondadosa, torpemente encubierta con una
acritud fingida. Y no tardó mucho en desbandarse
el grupo, en aras de la inquietud febril, propia del
último día de estancia en el colegio.

Los ojos maternales de Nelly siguieron, por
un momento, la parvada femenina que se alejó bulliciosa. De manera confusa, se enseñoreó del espíritu de la joven profesora una especie de miedo indefinido por todas aquellas vidas juveniles, en las
que se adivinaba y palpaba un anhelo incontenible
de lanzarse al mundo, unido a una gran avidez, a
una desmesurada avidez de todo.

A su lado, una voz de inflexiones graves, de sugerencias hondas, aún tratase de modular la más
banal de las frases, la obligó a estremecerse:

—¡Vamos, Nelly, que el tiempo vuela y no es
cosa de dejar sin empaquetar los libros y la ropa!

La interpelada, aún no dueña de sí misma, exclamó con visible aturdimiento:

—¡Andréïda…!

—La misma, Nelly, ¿te he sobresaltado?

Turbada la joven profesora por haber sido sorprendida en su meditación, se limitó a refunfuñar:

—Tú siempre tienes la virtud de sobresaltar a
todo el mundo. ¡Te acercas cuando menos se te espera…!

Andréïda ríe, y responde:

—¡La admirable virtud de Satán!, ¿es verdad?

—Verdad es. —Asiente Nelly con un leve matiz
de molestia, y tomándola del brazo, añade: No disparates tan de mañana. Ven.

Y, a su vez, se alejan las dos jóvenes mujeres.

/* * * {.espacio-arriba1 .centrado}

Ahora se hace necesario el presentar a Andréïda. {.espacio-arriba1}

A través de nuestra historia tendreis oportunidad sobrada de irla conociendo íntimamente; mas,
por ahora, nos limitaremos a deciros que, bajo todo
punto de vista, nuestra protagonista —¡es natural
que esta historia, como todas las historias, posea
una protagonista!— es un ser sorprendente, desconcertante. Y aclaremos de una vez por todas, adelantándonos a posible interpelación que, Andréïda
no es un ser de transición; nada más lejos de ella.
Por el contrario, es un ser de porvenir que se adelantó, por designio inexplicable, a ese extraño elemento problemático que llamamos tiempo y que
medimos audazmente por rotación solar.

Andréïda, como todo ser que ha roto un equilibrio, una continuidad, es el ser rebelde por excelencia. Desordenador de toda norma humana.

La armonía de su espíritu, como la de su cuerpo,
es una nueva armonía y corresponde a un nuevo
equilibrio que aún no lo es completamente, porque
no ha acabado de hallarse a sí mismo. Alma cansada de cauces rutinarios, intenta forjarse uno nuevo, independiente, y para ello, recurre a formas tan
cambiantes e inapresables como las nubes y el
agua corriente que refleja a estas de infiel manera.
Pero, sobre todo esto, se alza su maravillosa voluntad que, haciendo radicalmente, a un lado, debilidades, concesiones e incoherencias humanas, rige férreamente su propio destino y el de los seres que la rozan, en la única forma que puede hacerlo el más individualista de los espíritus, es decir, de manera anárquica… mientras no es detenido, en su trayectoria fatal, por un derrumbamiento absoluto.

Bajo la presión de una formidable sensualidad
contenida, cuya propia contención monstruosa miente una frialdad antibiológica, Andréïda marcha erguida entre dos paredes lisas que limitan el pasado
y el momento actual, llevando al frente su glorioso deseo y su tormento de ilusoria liberación íntegra, preñada de engañadora infinidad.

Andréïda se cree fruto de un progreso que lo
es solo de nombre, y a quien vemos fanfarronear
audazmente por el tiempo.

Surge deslumbradora y por primera vez del carro poderoso de la civilización, como una bella excepción a la que ella misma espera seguirá, en breve, la regla.

Precisamente de ese mismo carro grotesco. pero
imponente, del que tira pomposamente el progreso,
cuyos remos potentes se encargan de magullar, con
impertalista indiferencia, la carne del eterno esclavo aún cuando, en compensación, podemos contemplar sus fauces ensangrentadas por los tironas de la mano retrógrada, y sus flancos desgarrados a
golpes de vergajos, asestados por el genio creador
del hombre.

He aquí explicado el por qué las ruedas de oro
del carro de la civilización van tintas en sangre,
y, adheridos a sus rayos, llevan pingajos de carne
humana, por lo que la estela luminosa que trazan,
no es más que una amalgama satánica en la que
se funde el metal áureo con la sangre humana.

Pues bien; en el mágico vientre de este carro
soberbio que alberga todas las maravillas de nuestra era con todas sus monstruosidades y sus incongruencias, podemos decir que, son tres los personajes que ocupan los asientos preferentes en
nuestros concepto. Uno de ellos es la ciencia que
con una mano agarrota a la muerte y llega casi a
vencerla, y con la otra la acaricia y la encierra en
los monstruosos cultivos de microbios y en los asfixiantes gases destinados a exterminar la raza humana. El segundo, es un ser de formas redondas y
angulares, cuyas articulaciones silenciosas destilan
aceite y se ven cubiertas con un polvillo negro y sucio: es la mecánica. Lleva encadenados a sus espaldas millones de esclavos y, sin embargo… su
obra estaba destinada a ser de redención. Y en
cuanto al tercero, el tercero es el nuestro, y ¡oh,
asombro los habitantes del carro no se han dado
cuenta de su presencia ni del peligro mortal que
encierra… Acaso uno que otro lo haya percibido
por intuición, de manera subconsciente… ¡Quién
lo sabe a ciencia cierta! Sus formas son bellas, exquisitamente refinadas, su presencia cautiva… ¡Tiene pretensiones de perfección! ¿Necesitamos decirlo…? Es Andréïda, el monstruoso ejemplar primero del tercer sexo… En su aliento lleva el aroma inefable del fruto prohibido y, en las entrañas, la frialdad estéril de la materia muerta artificiosamente.

/* * * {.espacio-arriba1 .centrado}

Encontramos, de nuevo, a Nelly y Andréïda en
el minúsculo _cottage_ de la primera, separado del
gran edificio del colegio por la gran pista de recreo. {.espacio-arriba1}

La habitación que es a un tiempo dormitorio,
cuarto de estudio y sala de recepción, atrae la vista
con sus paredes claras. Una ventana baja y ancha,
por la que entra luz en abundancia, ostenta muy alegres y vaporosos cortinajes. En toda la habitación
impera el admirable sello sobrio y práctico de los
americanos.

Cierto desorden propio de la partida, altera su
aspecto incoloro de costumbre. En estos momentos,
Nelly, ha dado fin a la tediosa tarea de empaquetar,
y Andréïda ha ido a sentarse sobre el alféizar de
la ventana. Sus preciosos ojos, de expresión enigmática, descansan suavemente inmóviles sobre el
oscuro verde de la inmensa pista que a lo lejos antójase y se une con las nubes, en una mentida extensión marina.

El pensamiento de la joven ha retrocedido ocho
años y recuerda un día como este, en el que ella resistióse desesperadamente a regresar a su casa.
Mentalmente vuelve a leer la carta de la hermana
mayor que le anunciaba, de manera terminante, el
que, habiéndose hecho cargo de la casa de la madre
de ambas, por la simple y aplastante razón de subvencionar ella todos los gastos con su trabajo, desde aquel preciso día, en adelante, no permitiría más gastos superfluos, fuera del alcance de su actual humilde situación; por lo que le manifestaba que, considerando, dentro de esta clase de gastos, su estancia en aquel colegio de lujo, Andréïda debía terminar toda relación con él y regresarse, en seguida, a México.

La razón toda, todo el derecho, estaba de parte
de aquella hermana, muchísimo mayor en edad que
ella, que con un soplo de realidad destruía, de un
golpe, su ficticia situación. Así lo comprendió entonces Andréïda; pero sin dejar por ello de sublevarse contra aquella hermana que había absorbido
la voluntad materna hasta el punto de que su silencio aprobase, sin explicación, el que se dispusiera arbitrariamente de su vida de jovencita. Y aquel día ya lejano, Andréïda había comentado con amargura: ¡Triste voluntad materna pagada con el vil
precio de una humildísima comida y una cama de
hierro!

¡Ocho años hacía de ésto…! Apenas quince
contaba ella entonces…! Qué insufribles le habían
parecido los términos dictatoriales de la desafortunada misiva… Recordaba cómo, en aquellos momentos, le había parecido imposible la idea de renunciar al ambiente confortable del colegio. Ambiente que le prestaba la ilusión estúpida de creerse hija de familia rica.

Ella, la pobretona, había edificado a su rededor toda una leyenda de riquezas fabulosas… Como todas sus compañeras había hablado de automóviles, criados, residencia en las Lomas de Chapultepec… Pero en ella, aquello no había sido vanidad
torpe, no; fue un deseo incontenible, extraño, indefinido de perfección, de elevación sobre la miseria, sobre lo sórdido, sobre lo feo…

De ahí que, a toda costa, se propusiese el no retornar a México, porque ello significaba regresar
a la sordidez lóbrega de su casa, al medio mezquino
y estrecho en el que se complacía y regodeaba el
prosaismo de su hermana mayor…

Agravó aquel su firme propósito, el ambiente
brujo americano en el que las libertades se veían
débilmente comprimidas por las bardas del colegio.
La sed de lujo de sus compañeras, que era incuestionablemente también suya, y que dentro de una
irresponsabilidad para con los derechos humanos,
empujaba sus vidas hacia un epicureismo sui géneris que, a su vez, las impulsaba a gozar el momento presente de la manera más intensa, como si estuviesen a un paso de la nada. Uníase a todo esto
una falta absoluta de la más leve noción del deber
y que, en consecuencia, les filtraba en la sangre un
exacerbado egoísmo que no les permitía pensar más
que en el yo y siempre en el yo.

Y, al final de todo, estaba esa maldita fiebre
de juventud que también había hecho presa en Andréïda, y que parece ser el mal de América. Maelstrom gigantesco y fatal que revela, hasta a una chiquilla de dieciséis años, el que se le escapa el preciado don; y sabe ya ella de la rapidez fantástica
de su paso, obligándola a lamentar el minuto que
vuela y hasta a sentirse ya vieja… Horriblemente
vieja… Todo lo cual se ve avivado satánicamente,
en los espíritus juveniles, por los numerosos _cocktails_ que acostumbran a ingerir a diario y a escondidas. Mezclas diabólicas de las que en cada ocasión se ufanan las colegialas de ser autoras y en adjudicarles nombres incorrectos, impúdicos… Pero
aún había más: estaban aquellas escapatorias nocturnas, aquellos _partys_ con algo de sabatino y
de morboso. Y alzado sobre ellos, la loca alegría de
vivir, la fiebre insensata de apurar la copa cuanto
antes y hasta las heces… aunque llegara a producirles náuseas…

Todo este conjunto perverso era lo que a Andréïda le había parecido imposible de dejar. ¡Y pensar que por todo esto, tan inconsistente y banal, ella había hecho lo que había hecho…!

El penoso recuerdo tenía todavía la virtud de
enrojecer sus suaves mejillas de magnolia. Con una
mirada sabiamente disimulada de sus bellos ojos,
examinó, furtiva a Nelly, como si aún dudara, a
despecho de los años transcurridos, de la cándida
credulidad de la amiga.

Con toda meticulosidad recordaba ahora de qué
manera mañosa y hábil había sabido inspirar lástima a Nelly, fingiéndose huérfana y describiendo una soledad, desamparada que la aguardaba en México, y que no era sino puramente imaginativa.

Todavía hoy, Andréïda, no podía asegurar que
Nelly hubiese creído del todo aquella hábil patraña,
aunque, bien es cierto, que los resultados desprendidos de ella habían sido todo lo satisfactorios que esperara su urdidora. Y es así como, desde aquel día, Nelly la tomó bajo su protección, a la vez que de la muy convencional de sus lejanos parientes, los que hechos ya a la vida americana no se pararon en averiguar, con minuciosidad, en qué gastaba exactamente su sobrina la ridícula póliza, con la cual había sido favorecida a la muerte de su padre.
Despreocupación muy natural en aquella tierra donde el individuo parece tener una cómoda norma: “vivir y dejar vivir”.

Y así transcurrieron ocho años en los que los
gastos de Andréïda fueron sufragados por Nelly,
y en los cuales, también, las vacaciones de la segunda fueron, asimismo, las de la primera.

Por lo que respecta a su casa, Andréïda se limitó a escribir que una alma bondadosa del colegio se había hecho cargo de ella proporcionándole la educación a que creía tener derecho, educación que ni su madre ni su hermana habían sido capaces de sostenerle, y en ese estado de cosas prefería que no se ocupasen más de ella.

No sin dolor comprendía ahora Andréïda lo mucho que su inconcebible actitud debió haber hecho
sufrir a su madrecita. Las cartas de ella fueron
escaseando, toda vez que Andréïda no contestó ninguna, y su madre, creyendo seguramente que una
investigación perjudicaría la posición de su hija,
optó por un silencio resignado y censurable.

Tres años más tarde, cuando el espíritu de Andréïda había sufrido una transformación desconcertante a influjo del ambiente, y de lecturas, recibió una carta orlada de luto que firmaba la hermana. En ella le comunicaba, con un laconismo acusador, la muerte efectiva del ser que debía haber sido para Andréïda sobre todas las cosas amado, y que en su feroz egoísmo se atreviera a negarle la vida, asesinándola anticipadamente de pensamiento. Ante la aterradora noticia no tuvo ella más que una frase monstruosa, que salió silbante de entre unas quijadas que se endurecían implacablemente para no temblar en un sollozo desgarrador; y que reveló, con meridiana intensidad, el extraño fenómeno que iba operándose con lentitud en aquel cerebro privilegiado: ¡se ha roto mi último lazo con la especie!, fue el inusitado comentario.

Todo esto recordaba Andréïda en este día, lleno
de sol, y último de su estancia en aquel colegio que
de tal manera había obrado sobre su espíritu, y en
aquel país extranjero que había influído con nuevas normas de vida en sus conceptos sobre esta.

Sentía espantada el alcance incalculable de su
propio egoísmo, y de la mala levadura calculadora
con que, admitía, había nutrido siempre su corazón.

¡Su madrecita! ¡Linda madrecita la suya!

En la niebla del recuerdo se le aparecía, como
una dulce figulina, en la que el más ligero trazo
acusaba una fragilidad conmovedora. Vagamente
se la imaginaba Andréïda por los suntuosos salones
de la casa lujosa que habían ocupado sus padres, y
en la que trascurriesen plácidamente los años breves de su infancia, rodeada de exquisitos refinamientos. En su memoria resaltaban, como algo tangible al tacto, las bellas manecitas de su madre que
solían aletear sobre el teclado blanco y negro de un
monumental piano de cola, el que se antojaba más
grande por contraste con la pequeñez increíble de
la figurita que, indecisamente, lanzaba al aire las
notas tristes y un tanto morbosas de dos o tres
valses de Chopin. Y como en un “leitmotiv” torturante, veía, más tarde, Andréïda, las mismas manecitas una inmóvil, sujetando artística paleta, demasiado artística para ser de un profesional; y la
otra inquieta, con idéntica indecisión de un ser incompleto, incapaz de ordenar sus sensaciones y orquestar el tumulto de su desorden interior, sujetando un fino pincel que emborronaba colores sobre una tela inmaculada… Y eso era todo… todo el
recuerdo de la madre se reducía a aquellas manecitas que no sabían hacer más… quizá, acaso, y también en ocasiones, bordar un pañuelillo delicadamente, en el que delineaba curvas melosas y deprimentes…

Es fácil de imaginar el terrible drama de aquella casa, cuando la poca escrupulosidad del padre
la sumió en la miseria, a causa de una sucesión de
noches desafortunadas en el juego.

Y el drama cotidiano que, a fuerza de serlo, el
hombre acaba por encontrarlo biológico y, por lo
tanto, natural; siguió el fatal curso habitual de
trágico desbarrancamiento.

Primero, fue la venta de las alhajas de la madre, seguida de los muebles, de la ropa y de todo
aquello que el Monte de Piedad no rechazó…

Más tarde, vejamientos innúmeros para la infeliz esposa que velaba en la oscuridad, con un
celo espantoso y reprimido, el regreso del hombre
vicioso, alcohólico y despreocupado que, no contento con haber dilapidado su propia fortuna, arrastró la de su mujer y la de sus hijas…

Y así fue cómo la madre de Andréïda pasó a
engrosar la fila interminable de las esposas desgraciadas y en la miseria, como una callada víctima más de las innumerables que desparrama sobre la tierra la criminal despreocupación de nuestros hombres latinos.

Cuando la muerte libertó a la madre de Andréïda del lazo odioso, otra tiranía parecida hizo presa
en ella. Y fue la de su hija mayor que, con el espíritu práctico de la época y la escuela del dolor
en la que había nutrido su adolescencia, supo bien
pronto ganarse el sustento. Y, al hacerse de la llave
de la despensa materna, se hizo también dueña de la
voluntad de aquella pobre mujer, sierva irredimible, destinada a vegetar en dulce o amarga molicie
irresponsable.

Y como a la hermana mayor siempre le había
molestado la menor por su patente superioridad en
belleza y en inteligencia, cualidades ambas que aún
en embrión ya resaltaban; admitió con gusto la
solución que la pequeña Andréïda dió a su propia
vida. Por lo que, sin más preocupación, la abandonó a su suerte y a la madrina desconocida y providencial que con tal oportunidad venía a librarla
de una pesada carga; confirmándose así que, la hija mayor del infortunado matrimonio, había heredado la proverbial desaprensión paterna.

La voz de Nelly cortó de un golpe los dolorosos
recuerdos de Andréïda.

—Ven —dijo— ayúdame a cerrar las maletas.

Y el llamamiento fue atendido dócilmente por
Andréïda.

Al observar el ensimismamiento de la amiga,
la comprensiva Nelly, se apresuró a preguntar:

—¿En qué piensas, chiquilla?

—¡En tántas cosas! —fue la respuesta ambigüa
de la bellísima joven.

—Verás, yo también he cavilado mucho este
día…

—Y, ¿a propósito de qué, querida? —a su vez,
interrogó Andréïda.

—A propósito de ti y… de ellas…

—¿De mí? ¡Qué ocurrencia…!

—Ya lo sabes —afirmó Nelly— nunca como hoy
he sentido la responsabilidad de tu preciosa personita sobre mis pobres hombros…

Ligeramente emocionada Andréïda, se apresuró
a decir:

—Gracias, Nelly; ¡has sido siempre tan buena…!

—Pche… No hablemos de eso… Lo único que
quiero saber es si te sientes realmente contenta de
venir conmigo… Estos últimos días me ha preocupado, también, la duda de que si poseerías el suficiente coraje para enfrentarte a la vida y salir victoriosa…

La respuesta de Andréïda no se hizo esperar:

—Y en qué forma, Nelly, y en qué forma…!

Nelly, esforzándose por amenguar aquel optimismo que la amedrentaba, procedió a exponer con minuciosidad, a Andréïda, el que su fortuna personal se la habían comido las colegiaturas y que su sueldo como profesora en los dos últimos años, apenas si alcanzaría para pagar los pasajes de las dos
jóvenes hasta la ciudad de México, restando una
pequeñísima suma para sostenerse con ella unos
seis meses, bien humildemente. Para ello contaba,
afirmó, con el cambio favorable del dólar que contribuiría más que a triplicar aquella suma mezquina.

Andréïda, con el objeto de desviar aquella conversación que la avergonzaba por muchos motivos y
se le hacía profundamente penosa, se apresuró
a trazar todo un plan optimista por desarrollar
en la ciudad milagrosamente bella de México, objeto en aquel día de todos los dulces pensamientos
de las colegialas que añoraban íntimamente la patria… Y, como si recordara algo de pronto, procedió a cambiar el cauce de la conversación con
una pregunta.

—Comprendo que te preocupes por mí, pero
¿por qué por el resto de las colegialas?

Reaccionando Nelly y realmente interesada en
el nuevo tópico, repuso:

—Y ¡cómo no han de preocuparme…! ¿Crees
tú que se pasan aquí, en vano, ocho años que se dicen pronto, pero que hay que vivirlos para saber
de su monótona eternidad, sin que la indignación se
apodere de nosotras, sublevándonos al contemplar
ese puñado de vidas agostadas, irremediablemente
torcidas, no ciertamente por la educación que aquí
se imparte, no; sino por la atmósfera que se respira? Confiesa conmigo, que a nosotras, las latinas, nos sienta mal este aire del norte…

Ante la inacostumbrada parrafada en labios de
la flemática Nelly, Andréïda no pudo menos que
reír alegremente y lanzar al aire un rotundo ¡_My
goodness_!, cuyo acento hubiese envidiado el más
auténtico de los yankees.

—No te rías, Andréïda —suplicó un tanto amoscada la joven profesora.

—Pero si no me río —negó Andréïda con desfachatez risueña—. Es decir, sí me río —tuvo que admitir seguidamente al observar la escandalizada extrañeza que en su amiga provocara su intrascendente cinismo—. Estoy contigo, Nelly. Tienes razón, todo es cuestión de meridianos, de clima, de aire, como tú dices. Nosotras, muchachas latinas, trasplantadas aquí, por vanidad de los padres, por qué sé yo… Mira, si tú quieres por darle en la
cabeza al Gobierno Mexicano…! Y vamos, digo
yo, ¡si le habrá dolido el golpe a ese abstracto señor!
No, no te asombres, Nelly —se apresuró a
añadir—, recuerda que desde el llamado conflicto
religioso, en el 28 precisamente, se redobló nuestra
llegada a estos planteles. Los inundamos con nuestras personillas; porque, claro es, nuestros padres, no podían consentir la pravedad herética de enviarnos a colegios donde se impartiera una educación socialista.

Nelly, asombrada de la clarividencia de su amiguita sobre un hecho no muy abstruso por cierto,
exclamó:

—Has dado en el clavo, Andréïda. Eso es, esta
fué la principal razón de ellos…

—Una razón de una sinrazón, mi estimada Nelly, que llamaría el buen Galdós.

Suspirando la profesora Nelly, comentó:

—¡No saben lo que han hecho esos padres!

—Ya se darán cuenta, bien es que un poco tarde, —corroboró Andréïda—. Cuando lleguen a sus
hogares esas hijas, carne de su carne, que criticarán sus costumbres y sus rancios prejuicios con la desfachatez propia de aquí, con la admirable camaradería americana.

Una carcajada irónica subrayó cumplidamente
su cáustica observación.

—Y añade a esto, Andréïda, la insufrible actitud que asumirán en nuestro México estas mismas chiquillas que en su estancia aquí han tenido a gala,
contrariando el reglamento, hablarlo todo en español por puntillo patriótico; pero que, al regresar
allá, aparentarán haber olvidado su idioma y atormentarán los oídos de sus familiares con una jerga indecente de _slang_. Es decir, exagerarán la nota: se sentirán más americanas allá que mexicanas
aquí. Harán alarde, como cosa suya, de las libertades impropias que conocen de oídas y han observado a distancia; imponiendo, finalmente, su voluntad desarrollada, de manera tan terrible, por la
_self-discipline_ de aquí, a despecho de conveniencias sociales y de costumbres. Se sentirán muy _up-to-date_…

—Nelly, Nelly —interrumpió jocosamente Andréïda— que te sienta muy mal el pesimismo… Y he de decirte, además, que si bien estoy de acuerdo contigo en lamentar las actuales consecuencias que tenemos a la vista, sí, también te digo… —la voz de Andréïda tomó una inflexión casi esotérica— que podían ser muy otras esas consecuencias, si estas chiquillas, como tú las llamas, fuesen más allá… más allá… como he ido yo…

Los ojos de Nelly reflejaron fielmente una curiosidad expectante, y que, en realidad, había estado reprimida desde hacía bastante tiempo.

—A propósito, Andréïda, dime…

—Chist, ahora no… Ya escucho la campana que nos llama a comer. Corramos…

Y levantándose, la bellísima joven, desapareció
por la puerta con la ligereza de una mariposa, a
tiempo que lanzaba al aire el prodigio de su límpida
risa.

Así fue como terminó aquella conversación, llena de acertadas observaciones, cuya índole era habitual entre las dos excelentes amigas.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo II

+++COMO+++ previamente fue convenido, en la tarde
de aquel mismo día se reunió un grupo numeroso
de colegialas graduadas en el _cottage_ de Nelly.

La reunión presentaba el escandaloso y detonante bullicio de costumbre, aunque para los ojos profundamente observadores de Nelly se hacía patente
algo como una inquietud desorientada y febril que
pesaba hoscamente sobre aquellas cabecitas juveniles. Y es que, a pesar de su despreocupación, las colegialas no ignoraban que, al trasponer el umbral del colegio, se convertirían, de pronto, en mujeres responsables de sus actos.

Al igual que en cualquiera otra reunión social,
cuatro o cinco, a lo sumo, de las jóvenes, destacaban sus voces e imponían rumbos a la conversación. Entre éstas se contaba, muy principalmente, María Luisa, bella jovencita de mórbidas formas, lentos ademanes perezosos y sensuales que denotaban, hasta en el más ligero de sus movimientos, una gran bulimia carnal.

—Nuestro último té! —exclamó con un dejo de pena.

Un breve silencio siguió a sus palabras en las
que las colegialas percibieron, por primera vez, la
dolorosa revelación de la adolescencia que ha dejado de serlo bruscamente.

Isabel, otra chiquilla de trazos puros y claros,
de boca suavemente femenina y ojos ingenuos que
miraban la vida como un hermoso cuento de hadas,
suspiró gozosamente y dijo:

—Otra vez a nuestro México lindo, y ahora para
siempre… ¡Qué felicidad! ¿No sienten que les estalla el corazón de alegría?

María Luisa que, como todas las sensuales, no
podía concebir otra alegría que no estuviese ligada
a sus sentidos y que tenía para aquéllas siempre lista una crítica mordaz, se apresuró a verter un
jarro de agua fría sobre aquel calor inocente y sin
trascendencia.

—Ya salió la romanticona, la pajarita sentimental… —y añadió: pues mira tú, yo malicio que no va a ser muy grata mi estancia allá y que va a haber muchos disgustos en mi casa por causa mía.

Esta vez fué Pita, muchacha de miembros enjutos, de ojos humosos, por un extraño fuego pecaminoso, y cutis amarillento y opaco, la que se encargó de replicar a María Luisa:

—¡Quién duda que los habrá…! ¿Y en qué casa
podrá haber paz en pisándola tú?

Al punto, el ambiente de la reunión se caldes, y
todas a una, divertidas y morbosamente interesadas
en aquel duelo verbal, se apresuraron a azuzar los
ánimos de las jóvenes contrarias.

María Luisa, con un mohín de sus labios jugosos que tenía algo de bajo, de un matiz canallesco que
no se atrevía todavía a revelarse drescaradamente
se contentó con decir:

——Calla, so envidiosa, que te pasas la vida escupiendo bilis por esa boca torcida que Dios te ha
ha dado.

Por su parte, Andréïda, que había permanecido
callada y secretamente divertida al observar aquel
mosaico de individualidades, proyectando, en pequeño, los eternos conflictos de la vida, se apresuró a intervenir conciliadora:

—A callar, chiquillas. Tengamos la fiesta en
paz. Olviden todas sus rencillas en gracia a que
nos reunimos por última vez. ¡Cuántas de nosotras
no nos volveremos a ver nunca!

Las palabras pacifistas de Andréïda fueron corroboradas ampliamente por Nelly que, a su vez,
añadió:

—Tiene razón Andréïda. Hasta hoy no han sido ustedes más que mujercitas en preparación, en potencia; esta noche, en cruzando el dintel del colegio, comenzarán realmente a vivir… Allí se bifurcan nuestros caminos que nos llevarán Dios sabe a dónde…

Las últimas palabras de Nelly estaban impregnadas de una tristeza previsora. Pero la seriedad no podía enseñorearse largo rato en aquellos atolondrados espíritus juveniles; por lo que se esfumó completamente, dejando su lugar a la curiosidad
irreprimible que poseía a las colegialas por conocer
los nombres de las profesoras encargadas de llevarlas a la frontera, donde ya las esperaban sus
respectivos familiares.

Sabían que Pita, la muchacha angulosa, conocía de cierto los nombres aquéllos que de tal manera
las interesaban; pero que rehuía solapadamente el
decirlos, por un prurito vanidoso de hacerse la interesante.

Andréïda, que poseía sobre todas ellas cierta
autoridad, se dirigió a Pita con malicia:

—Anda, díselos que las tienes impacientes, no
te hagas la señorita Misterios, que tú todo lo sabes…
—Después, lanzando un mirada circular y burlona, añadió—¡ Como que se te pasa el tiempo barbeando a las misses! ¡Y que no es poco fina tu navaja de barba… ¿Lo saben, muchachas? Usa _blue-blade_, la última hoja lanzada por la Gillete…

Un estentórea carcajada acogió las palabras mal
intencionadas de Andréïda; pero Nelly, a quien disgustaba sobremanera esta actitud de mortificar a
un semejante, se apresuró a reprochar duramente a Andréïda, diciéndola:

—¡Vamos, que ahora eres tú la que mete cizaña! —y, sin hacer caso del mohín falsamente compungido de Andréïda, acto seguido le rogó a Pita manifestase los nombres de las profesoras para satisfacer la curiosidad de las colegialas.

Al escuchar las jovencitas el primero de ellos, o
sea el de Miss Mc Intyre, lo acogieron con alegres
muestras de aprobación. No sucedió así, en cambio,
con el de Miss Pitkin que hizo el efecto desastroso
de una bomba.

—¡Bah! La “gallina verde” —fué el comentario
rápido de María Luisa, y añadió sirviéndose de una
terminología muy mexicana—. ¡Ya nos “fregó” la
Directora!

Pita, que ignoraba el origen del motecito aquél,
endilgado irrespetuosamente a Miss Pitkin, y que
había querido conocerlo desde hacía bastante tiempo, cosa que le había sido negada sistemáticamente
por sus compañeras, quienes abrigaban el justo temor de que fuese con el cuento a la interesada, inclinación ésta muy propia de su carácter y de sobra conocida por ellas aprovechó esta oportuna ocasión para que se lo contasen, y así lo suplicó. Esta vez fué complacido su deseo, ya que todas estuvieron de acuerdo en que no tenía caso el seguírselo ocultando.

María Luisa, la mocita de chispeante y pintoresca charla, fué la encargada, por sus amigas, de hacer el jocoso relato.

—¿Han oído ustedes —comenzó diciendo— el
cuento de aquel americano a quien se le perdió una
cotorra en México?

¡Sí! ¡No!, fueron las opuestas exclamaciones de
las colegialas que, a pesar de ello y deseosas de apreciar el gracejo indiscutible de María Luisa, estuvieron de acuerdo en que se los narrase.

Verán —prosiguió gentilmente— no tiene gran
gracia… El tal gringo ignoraba el nombre del
animalito; pero eso no obstó para que saliese de su
domicilio en busca de la malhadada cotorra y, al
primer transeúnte con el cual tropezó, le hizo la
siguiente pregunta: —La graciosa mímica de María
Luisa se coordinó felizmente al imitar, de manera
magistral, el cómico acento de un americano hablando español— señorr, señorr, ¿no ha visto ostea
una gallina verrde que habla como yo?

Las colegialas que necesitaban bien poco para
soltar al aire el trapo de la risa, celebraron jocosamente el cuento. Pero María Luisa se apresuró a
reanudar el hilo de su relato.

—No es eso todo, esperen —y el silencio se hizo,
aunque no perfecto, pues de vez en cuando y a pesar
de los esfuerzos que cada una de ellas hacía para
dominar sus carcajadas, éstas brotaban espontáneamente de sus bocas frescas.

—Pues bien —añadió la narradora— así iba diciendo Miss Pitkin el día en el que también ella perdió el desgraciado lorito que Pita le había obsequiado, trayéndoselo desde la lejana Huasteca —y volviendo a imitar el acento americano, concluyó: —Oyes, muchachos, muchachos, ¿has vísto ostedes
el animalito verrde de México?

El alboroto estalló en una risa general incontenible hasta que Nelly se vió precisada a imponer orden con la amenaza, todavía de fuerza, de que si alguna de las otras profesoras las oía, con toda seguridad que se apresuraría a propinarles el último “speech”, lo que sería, hizo notar, bien poco agradable como fin de curso.

Pero una vez la conversación encauzada por el
sabroso tema de las travesuras escolares, fué ya
materialmente imposible el meter al orden a aquel
grupo de muchachas, en las que la alegría de vivir
se desbordaba.

Saltaron una y otra anécdota a cual más graciosa. Entre ellas recordaron con gusto una, por la cual habían sufrido las consecuencias de un original castigo, la propia María Luisa y buena parte
de la clase. Castigo que les había sido impuesto
por aquel azote de las colegialas que respondía al
nombre de Miss Pitkin.

El nuevo sistema de reprensión había consistido en amordazar con una servilleta a la primera parlanchina de la clase y, como era perfectamente
lógico y natural, fué María Luisa la primera en
recibir el castigo. Bien es cierto que, a la neurasténica Miss Pitkin le había costado un poco cara su genial idea, porque muchacha que amordazaba,
muchacha que al minuto levantaba el dedo pidiendo
el cambio de servilleta a causa de que estaba mojada. ¡Y vaya mojaduras! comentaron las colegialas. Con seguridad que aquel día las muchachas, objeto del castigo, agotaron sus reservas de saliva, con directo detrimento del bolsillo de la profesora a quien aquella semana le subió la cuenta del lavado en varios dólares, por lo que tuvo que desistir de servirse de su original medio de reprensión.

La tetera corría de mano en mano y las bandejas de pastas se agotaban a ojos vistas. En vano María Luisa, siempre golosilla, había suplicado le
sirvieran un poco más de la conserva de manzana
que tanto le gustaba.

A las anécdotas había seguido la enumeración
descabellada de planes para nuevas diversiones que
todas creían les esperaban en México. Unas a otras se habían preguntado el lugar respectivo de la República en el cual iban a habitar, y una gran mayoría se había pronunciado por México. lsabel aseguraba jubilosamente que su tutor la llamaría también allí.

Poco a poco, fué cayendo sobre las colegialas el
hastío de las conversaciones agotadas; conversaciones cuyos tópicos, bien reducidos en ellas, giraban siempre alrededor de las mismas materias. Sin embargo, nadie intentaba iniciar la despedida, con excepción de las colegialas más jóvenes que habían sido enviadas, por las mayores, a lavar la vajilla.

Había anochecido casi del todo y una de las muchachas intentó dar la luz; pero, al punto, María
Luisa suplicó que no lo hiciese porque así se estaba
mejor.

—Platiquemos —dijo— de algo que nos encoja el
corazón.

Un rayito morboso se encendió en todos los
ojos; pero nadie supo eatisfacer la inusitada invitación.

El silencio se hacía cada vez más molesto, hasta que Antréida optó por romperio.

—A ver, voy a leerles la buena ventura… —su
voz tuvo inflexiones extrañamente opacas.

Todas, a una, saltaron interesadas y fueron a
sentarse en círculo, alrededor de Andréïda que
apoyaba levemente su esbelto cuerpo sobre la mesita de centro. Algunas de las muchachas llevaban todavía en sus manos las tazas de té de que habían hecho uso.

María Luisa, con su habitual sorna, preguntó a
Andréïda:

—¿Eres gitana?

—Sí,—contestó, al punto, la interpelada —una
gitana americana que, para leer el futuro, no necesita ni las palmas de vuestras manos ni echar las
cartas…

Nelly, visiblemente divertida ante la salida original de su protegida, interrogó, a su vez:

Y, ¿entonces?

—Nada, que solamente necesito vuestras tazas
de té. ¿Hay quien me preste alguna?

La curiosidad había prendido su embrujo en
aquellas almas juveniles que alardeaban de una incredulidad moderna que estaban muy lejos de sentir, y cuyo escepticismo desentonaba, en absoluto,
con la ingenuidad desarmada de los ojos.

Pita, que al principio hiciese un mohín de desaprobación, más por costumbre de censurar todo lo
que partiese de Andréïda, por quien sentía un odio
y una envidia que no se molestaba en disimular,
interrogó, a su turno, dejando entrever un innegable interés.

Pero, ¿cómo, cómo lo haces?

Sin hacer caso, Andréïda se limitó a pedir de
nuevo una taza, y observando, divertida, que nadie
osaba alargarle la suya, exclamó un poco despectivamente:

—¡Vaya, tienen miedo de lanzar un atisbo al
futuro!

Corrida Pita en su vanidad de ser superior, en
cuyo plano deseaba siempre se la viese, observó
acremente:

—¡Miedo! ¿Es que das por descontado el que
creamos las mentiras que tengas a bien decirnos…?

Esta vez la observación molestó a Andréïda,
puesto que ello implicaba el que se la juzgase como
autora de un acto de flagrante falsedad. Y no había cosa más aborrecida por su naturaleza, ingénitamente recta y enamorada de lo vertical. Razón por la que se apresuró a retirar su ofrecimienao, no
sin antes rozar con su desprecio a aquellas mujercitas insubstanciales en quienes, a lo sumo, les reconocía el privilegio de divertirla un rato, en calidad de seres en los que se observa, con extrema facilidad, sus particulares deficiencias.

—Nada, no he dicho nada, —asentó Andréïda— si mis palabras las van a considerar como mentiras, por mí, déjenlo. Yo lo hacía porque no estábamos precisamente lo que se dice muy divertidas,
hace un momento… Ya se sabe que a ustedes solo
se las divierte con cocktails, cuentecitos un sí no
escabrosos, o muchachos para ejercitarse en el “necking.”

Las palabras de Andréïda desconcertaron visiblemente a su auditorio, y, de manera confusa, las colegialas se dieron cuenta de que haban incurrido en el desagrado de aquélla a quien en secreto unas
temían, otras odiaban y admiraban las más.

María Luisa, de resoluciones rápidas en el fondo de las cuales se podía adivinar siempre una extraña generosidad, ofreció su taza a Andréïda con
espontánea franqueza, a tiempo que le dijo:

—¡Bah! Yo no le tengo miedo a nadie ni a nada. Después de todo, Andréïda, he de confesarte
que si me dices mentiras o no, yo de todas maneras
las creeré a pies juntitos. ¿Sabes…? Tienes algo
extraño. A veces me das la impresión de que no
eres una muchacha como nosotras…

Las palabras extraordinariamente sinceras de
María Luisa reflejaban, con fidelidad, el sentir general de las colegialas.

Sí; todas convenían en que Andréïda no era
una muchacha como las demás. Por ejemplo y ahora mismo, cuando tomaba esa actitud que la despesaba del resto del grupo, y en sus extrañas pupilas parecía aquella mirada helada que tenía algo de
sobrenatural, de poco humano, de endurecimiento
peligroso, que la alejaba irremisiblemente de ser
una entre las colegialas.

Y es que la superioridad de Andréïda, a pesar
de informarla una compleja cultura enciclopedista
de lecturas, en las que no había sido guiada más
que por su propio capricho, que tenía el raro dón
de gustar de lo selecto, tenía también por razón
fundamental y al mismo tiempo por base, una como nueva naturaleza sobrepuesta a la común.

Lentamente accedió Andréïda a tomar la taza
que le ofrecía María Luisa, y observando, pensativa, las ramitas de té que restaban en su fondo, levantó la mirada inquisitiva de sus ojos enigmáticos, y con el aire vago y la voz incolora que debieron poseer las antiguas pitonisas, procedió a hacer la exposición tajante y el análisis despiadado de la
individualidad de su amiga. Para ello se sirvió de
un lenguaje crudo y realista, el cual no se detuvo
ante ninguna consideración convencional.

—Eres, por excelencia, la genuina mujer sensual
—empezó diciendo, y su afirmación fué casi una acusación enérgica.

María Luisa, que íntimamente se sentía defraudada por el extraño giro que tomara la anunciada
profecía, preguntó con impaciencia y con un desnudo y sincero interés en la voz:

—¿Me casaré pronto…?

—La voz de Andréïda se endureció para proferir:

—No me interrumpas…

De pronto, se dejó escuchar el tono enérgico
de una de las profesoras americanas que, desde el
jardín, llamaba a gritos: —Miss Elizabeth, Miss
Elizabeth —y que tuvo la virtud de sobresaltar y
estremecer a todo el grupo. Por lo que Pita observó
con acritud:

—¡Ya has logrado tu objeto, Andréïda! Ya nos
has puesto nerviosas… ¡Criatura más extraña!

Apresuradamente Nelly urgió a Isabel para que
cbedeciera el llamado, quien así lo hizo, no antes
sin refunfuñar sobre la inoportunidad de la profesora que la impedía quedarse a escuchar. En su
precipitada salida suplicó a Andréïda que la esperara para que, a su vez, le dijese a ella todas aquellas cosas tan bonitas que esperaba de su destino
Cosas que concretaba en un maridito amable a quien
ella quisiese mucho, unos hijitos “güeritos” como
americanos, puesto que a ella así la placían. En su
rápida enumeración de dichas futuras fué nuevamente interrumpida por la voz impaciente de quien
la llamaba, y optó por obedecer al punto.

María Luisa acompañó su precipitada salida con
las siguientes palabras de una ironía burlona, muy
de acuerdo con su carácter:

—Corre, corre… que ya nos contarás tus boberías. Y miren que no es poco romántica la chiquila —e imitando la voz y ademanes de Isabel, añadió: —Un maridito bueno… algo así como un azucarillo que se le deshace a una en cuanto se lo echa a la boca…

Andréïda quedó pensativa a la salida de la pequeña Isabel. No era necesario poseer pretendidas dotes adivinatorias para augurar a aquella preciosa
chiquilla el que sufriría muchas desilusiones y muchas lágrimas en su vida. Y tal parecía que el resto de las muchachas hacía reflexiones similares a
las de Andréïda, pues se había hecho un silencio
significativo.

Los ojos de Andréïda tropezaron con la desabrida figurilla de Pita, y sufrieron, al instante, una
transformación desconcertante. Toda la dulzura de
que fueran dueños al observar la salida precipitada
de Isabel, desapareció de las pupilas para dejar en
su lugar una expresión desconcertante de desprecio
y asco, muy parecido a los que experimenta quien
contempla, a su pesar, algo viscoso, torcido y repugnante.

Y, en verdad, que el odio que alimentaba Pita
contra Andréïda tenía en esta última la correspondencia debida en la forma de un desprecio anonadante.

Andréïda, durante el último curso, había espiado la conducta de Pita con la curiosidad atenta de un viejo psicólogo.

Presentía que el acto más sencillo e inocente
se veía terriblemente deformado, por el solo hecho
de que Pita lo efectuase. Había siempre algo deforme y morboso en todo aquello que la agria colegiala realizaba, proyectando, sobre ello, el envilecimiento.

Andréïda había sido muchas veces testigo impotente y amordazado, a causa de la miopía de sus mentoras, de la impenitente afición de Pita a proteger colegialas de menor edad y de los cursos inferiores, con veladas intenciones malévolas que a la singular Andréïda no escapaban. Reciente estaba, todavía, la escandalosa amistad última de Pita con la pequeña Betty, con la chiquilla rubia y sonrosada de la cual no se separaba en las últimas semanas, y a la cual había enredado en la malla traicionera y falaz de sus pequeños regalos suntuosos de heredera privilegiada.

Lejos de Andréïda estaba el poseer una mentalidad mezquina y malintencionada, puesto que sus múltiples lecturas y su propia especial naturaleza la colocaban en un plano de la más alta comprensión humana. Es más, en ocasiones, ella había criticado duramente la incomprensión de muchos pedagogos que se mostraban en este punto más que maliciosos, y reconocía que su criterio acusador sobre la atracción que en la pubertad inclina a la muchacha hacia otra muchacha, no tenía razón de ser. Andréïda proclamaba rotundamente que este amor era natural, sano, puro, biológico…

Ella misma reconocía que a los catorce años había pasado por esa etapa. Había querido a Nelly
con ese afecto un poco exaltado, que el transcurso
de los años se había encargado de convertir en el
puro y fraternal que hoy las unía. Aquello, en su
concepto, había sido la natural expansión del corazón que buscaba un objeto de amor… La crisis
terrible de la niña que se ve empujada, por fuerzas
ocultas, a amar… a amar las flores, la tierra que
pisa, el aire que respira… Era, en fin, la naturaleza que en un organismo sano no tenía otras consecuencias que ejercer la sensibilidad… Sensibilidad que en esta clase de colegios suele exacerbarse lo indecible, por falta de afectos familiares… por la carencia de hogar… Pero que en Pita, en una alma deforme como la de ella, en unos nervios defectuosos como los de ella, significaba el desequilibrio, la aberración.

Por un momento, Andréïda, se reprochó duramente su desprecio por Pita, tornándolo en un sentimiento de compasión. Y se dijo, que no había razón para culpar a la desgraciada muchacha. Era sencillamente un caso patológico, un cuerpo que del colegio debía marchar a un sanatorio, si las llamadas profesoras encargadas de cultivar su juventud, hubiesen conocido a conciencia su profesión… Una profesión que bien podía variar los cauces de la humanidad…

Hubo un instante en que los ojos de Andréïda
sorprendieron el movimiento habitual de la mano
de Pita hacia el bolsillo interno del uniforme. Sabía de sobra Andréïda lo que aquella mano buscaba, y que no era otra cosa que una minúscula cajita conteniendo mentol pulverizado. Para los ojos
de cualquier extraño, aquel hábito no hubiese significado lo que para la observación analista de la
singular joven tenía de importancia deductiva, por
las fatales consecuencias que de aquella afición, a
simple vista inocente, pudieran desprenderse en ei
futuro.

No cabía duda de que Pita era una criatura
propicia a todos los vicios, a todos los descarríos.
Y Andréïda no podía menos que indignarse pensando, no precisamente por lo que fuera o dejara de ser su compañera, sino por todos aquellos seres sanos que arrastraría en su caída, con el imán peor… con el imán irresistible de lo prohibido…

La voz de María Luisa interrumpió, por fin, las
reflexiones de Andréïda.

—Anda, Andréïda —le dijo— prosigue, que ya
falta bien poco para la llegada del autobús.

Andréïda hizo un esfuerzo sobre sí misma y trató de hablar con naturalidad y ligereza.

—Oh, María Luisa, perdóname, no tiene importancia. ¿Querías saber si te casarías, no es eso?
Pues sí, en llegando a México.

María Luisa palmoteó con exagerada alegría
se apresuró a preguntar:

—¿Rico el galán…?

—Rico y viejo.

—Naturalmente —aceptó la muchacha con desvergüenza— no se puede encontrar la riqueza, la juventud y la buena presencia unidas. ¡Qué se va a hacer —suspiró con cinismo—. Por lo demás… ya sabré yo cómo compensar ese defectillo. Y la maliciosa intención de sus últimas palabras hizo reír de buena gana a sus compañeras.

Nelly, siempre atenta a todo aquello contrario
al buen juicio que debía animar la conciencia de
las jóvenes, se apresuró a reprender a María Luisa, a lo que ésta replicó con desenfado: —Pues ni
te pase por la imaginación, mi querida cuáquera,
que renuncie a gozar el cachito de dicha a que tengo derecho… —después, dirigiéndose a Anádréida,
añadió: —Oye, pues me has dicho bien poco… y
¿qué pasó con aquello de la tragedia y del drama
y de no sé qué?

Sonriendo Andréïda, replicó al punto:

¿Tendré que decírtelo? ¿No crees tú que el cachito de dicha a que has aludido, no la llevará lo
que se dice muy bien con el viejo de los miles de
pesos?

Una carcajada maliciosa acogió las palabras de
Andréïda, y haciendo coro también María Luisa,
comentó cínicamente:

—No, chica, en esto fallas… Esas ofensas ya
no se lavan con sangre en este siglo… se hace la
vista gorda y nada más.

Mientras todas reían el franco cinismo de María Luisa, Andréïda no pudo menos que decirse
para su interior: He aquí la eterna Madame Bovary, metamorfoseada y muy a la derniére cri. Y tornando a mirar el fondo de la taza, en la que la colocación de las hojitas de té le indicaban claramente que había de correr sangre en la vida de la abúlica y sensual María Luisa, comprendió, de pronto, que las causas estarían muy distantes de ser las mismas de la heroína de Flaubert; por lo que no sería precisamente su vida la sacrificada, aunque, por otra parte, el maldito dinero sí seria el indicado para meter su perra manaza en la tragedia venidera.

Isabel entró bulliciosamente en aquellos momentos, de vuelta ya de haber cumplido con los fastidiosos y finales requisitos de la Dirección.

Con visible interés ardiente se dirigió a Andréïda suplicándole que, a su vez, le dijese a ella
la que la esperaba en el porvenir.

Al contemplar Andréïda aquellos dulces ojos,
bellamente crédulos, tuvo un movimiento de retroceso, de confuso escrúpulo. Sabía de sobra el amor y la admiración que María Isabel sentía por ella, y sabía también que para la dulce chiquilla era ella un ser superior, perfecto, bueno, bello sobre todas las cosas. Por lo tanto, optó por rehuir lo que constituía en el caso preciso de Isabel, un verdadero compromiso a su conciencia.

Al contemplar Nelly la indecisión de su amiga
cuyas razones no se le escapaban, dijo:

—Basta ya de tonterías. No es nada divertido…

Isabel un poco desconcertada y sin saber exactamente a qué atribuir aquellas reservas y reticencias para con ella, exclamó timorata:

—Muchachas, y además, ¿no será también pecado?

La maligna Pita soltó al aire su ríspida risita
de costumbre, y percibiendo, a su vez, la inesperada actitud reacia de Andréïda, deseó forzar la voluntad de ésta, por el solo prurito de oponerse a su
encubierto y oculto deseo.

—A qué poca cosa llamas tu pecado —comentó
la agria criatura—. Anda, atrévete, y que Andréïda te diga tu suerte. A ver si contigo se muestra
menos pesimista… ¡siquiera por el cariño que la
tienes !'—agregó intencionadamente.

Isabel, sin comprender la baja intención de las
palabras de Pita, quien aprovechaba cualquier ocasión para manchar con su malicia la conducta clara de sus compañeras, se apresuró a asegurar inocentemente :

—Como quererte, tú sabes que te quiero mucho,
Andréïda. —Y después, dirigiéndose al resto de las
muchachas, añadió— Ustedes no saben lo que yo le
debo… las clases que me ha hecho… los problemas de aritmética que me ha resuelto…

Tan ingenua confesión provocó escandalosa risa
en las colegialas, las que, en el acto, adoptaron una
actitud comprensiva y de protección para con la
dulce Marisabel.

No dándose por vencida Pita, comentó con sorna: —Ah, vaya… entonces tu cariño entusiasta es
interesado…

Un rubor más encendido que el que le produjese
la risa de sus compañeras, encendió de indignación
la carita ingenua.

—No, no; no es interesado… Claro —añadió un
poco avergonzada, pero aceptando con gusto su inferioridad— que también sin ella no hubiese terminado ningún curso. A mí me gusta coser, bordar y
¿saben ustedes? la cocina, ¡Uy, lo que me gusta a
mí la cocina!

Al oír las colegialas las últimas palabras de Isabel, volvieron a reír con mayor gusto, y María Luisa con cómico énfasis, apuntó:

—Nada, nada, muy femenina. Será indudablemente víctima del lobo-hombre.

El regocijo subía de punto, y bien pocas fueron las que escucharon la observación pensativa de Nelly:

—¡Víctima ancestral, eterna sierva del hombre…!

Andréïda, profundamente divertida, replicó con
dulzura a Nelly:

—Mi buena Nelly, que ahora eres tú la profetisa —y riendo suavemente, terminó— Ante la competencia me retiro…

Al oír Isabel las últimas palabras de Andréïda,
saltó con apresuramiento.

—Oh, no, Andréïda…

Compadecida Andréïda de aquel interés infantil, trató de convencer con dulzura a Isabel de la
causa aparente de su negativa.

—Sí, chiquilla… He de confesarte gustosa que
yo también te quiero mucho, y por tal razón no podría ver con claridad tu futuro… No te lo diría todo y sería una especie de traición… Por lo tanto,
solo me es concedido el desearte que seas muy feliz y el aconsejarte que no abrigues tantas ilusiones, que no seas tan espontánea en amor… ¡Amor! 
—y el sonido acariciante de su voz se hizo abstracto para añadir—¡En amor el que ama es siempre el esclavo, el amado es el dueño!… Resérvate…
el estar a la espectativa es ya una ventaja…

La velada seriedad de las palabras de Andréïda
tuvieron la virtud de volver al mutismo a aquel conjunto de muchachas, a quienes se hacía imposible
tomar la vida en serio.

A su vez, Nelly terció con acento maternal:

—Sí, pequeña, tiene razón Andréïda. No mires
todo con ese cristal color de rosa que llevas en tus
manecitas como un glorioso trofeo… Recuerda
que los hay azules, verdes, rojos y… también negros…

Un estremecimiento irreprimible agitó el grácil cuerpecito de Marisabel que exclamó amedrentada:

—¡Dios mío, me dan miedo!

Pita sugirió sarcástica:

—Es por tu bien, querida. Desconfía, desconfía… desconfía…

—¿De ellas? —interrogó Isabel sin comprender.

—De todo y de todos —contestó Pita con acento huecamente doctoral—. Hasta de ellas también…
De la vida sobre todo… La vida no es bordar, soñar y cocinar, es… que te la explique Andréïda que parece estar tan bien preparada para afrontarla.

La sugerencia de Pita provocó expectación entre las colegialas, quienes se agruparon con un nuevo interés en la mirada.

María Luisa reforzó con sus palabras la petición
de Pita. —Si, Andréïda —suplicó— Dínos qué va a
ser de ti —y con una extraña indecisión en el acento, añadió—. A veces me entran unas ganas locas de preguntarte: ¿quién eres tú…?

—¿Yo…?

Alguien dió la luz bruscamente provocando un
sobresalto general e infundado.

—¿Yo…? Andréïda —apuntó la interrogada,
para añadir riendo segura de un triunfo oculto—.
Alguien igual a ustedes en forma…

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo III

Por unos instantes reinó el desconcierto entre
las colegialas y un extraño miedo se apoderó de ellas
en lo supconsciente.

Isabel, más serena en su dulce simplicidad imperturbable y difícil de ser afectada por lo inasible, afirmó con un convencimiento conmovedor:

—No, Andréïda, eres mucho más bella…

Poco susceptible Andréïda a doblegarse ante falsos convencionalismos y con esa rectitud suya, que
era uno de sus mayores atractivos, admitió sencillamente lo dicho por Isabel.

—¿Más bella…? Quizá sí…

Hubo un revuelo de extrañeza por la inusitada
y franca admisión, y Pita se apresuró a comentar
con mordacidad:

—¡Me asombra tanta modestia!

Los ojos extraordinariamente sinceros de Andréïda se volvieron hacia Pita con una orgullosa expresión de reto en las pupilas.

—No la tengo, porque no se puede tener modestia de lo que es obra exclusiva de nuestro esfuerzo…

María Luisa, sin comprender, se precipitó a interrogar:

—¿De tu esfuerzo?

—Sí, de mi esfuerzo —afirmó orgulilosamente
Andréïda— …Esta belleza que sin tapujos admiran ustedes, y de la cual estoy, sin alarde, orgullosa, es obra mía…

El desconcierto entre las colegialas subió de
punto. Siempre habían percibido algo de extraño en
su original compañera; pero nunca, como en aquellos momentos, se había revelado ante sus ojos Como un personaje extraordinario y completamente nuevo que venía a sorprenderlas con su presencia,
por primera vez.

Solamente Pita, con el sentimiento envidioso
que siempre le había inspirado la belleza indiscutible y asombrosa de Andréïda y que, además, para ella había tenido el imperdonable pecado de mostrársela altiva e inaccesible, observó con sarcasmo:

—Lo creo… por lo menos son muchos los cosméticos y de magnífica calidad que abundan en tu tocador…

Un recuerdo reciente se hizo presente a la inocencia peculiar de Isabel, obligándola a exclamar con apresuramiento:

—Ahora me explico el día que te encontré en
este cuarto, registrándolo todo.

Una carcajada irreprimible tornó a borrar la
seriedad momentánea de las colegialas.

María Luisa, con su gracejo inimitable, comentó:

—Espionaje alemán y del más fino. ¿Mata Hari?, una alumna del _kindergarten_ al lado tuyo…

Una vez agotadas las risotadas de las colegialas que celebraron ruidosamente la salida jocosa de
María Luisa, la expectación hizo, de nuevo, su asiento en los espíritus juveniles tan susceptibles de ser afectados por lo maravilloso.

Por su parte, María Luisa, sinceramente interesada, observó:

—Oye, Andréïda, es cierto todo eso que estás diciendo, o es “puritita vacilada”.

La risa impenitente tornó a reinar a raíz de la
expresión muy mexicana de María Luisa, y ésta se
apresuró graciosamente a afirmar:

—¡No se me olvida mi México!

A lo que Nelly comentó con pesimismo:

—Ya se te olvidará, desgraciadamente, en cuanto llegues.

—_Sure_ Mike —afirmó imperturbable la muchacha ante la oculta reprensión que encerraban las palabras de Nelly, y con simpático descaro, añadió— ¡Y lo que voy a presumirles a las cursis de México _city_—. Después, volviendo repentinamente a lo que en verdad la interesaba, mucho más, ciertamente, que la anterior faramalla de pretendidas profecías, suplicó:

A ver, Andréïda, vas a ser buena conmigo y me
vas a decir cómo consigues ese color precioso de
pelo que luces…

Por sobre su actitud informal, la vanidad femenina de las colegialas se hizo, de pronto, presente y
diversas voces alabaron la cabellera extraordinariamente bella de Andréïda. Sin embargo, fué Isabel quien demostró la más entusiasta admiración. Sus palabras tomaron giros poéticos y precisos para calificar la hermosa tonalidad de pelo de la amiga y su voz tuvo vibraciones emocionales de éxtasis cuando dijo:

—Muchachas, si es bronce caliente, a ratos rojizo, a ratos dorado…

Profundamente divertida Andréïda con el sincero asombro admirativo de sus compañeras se limitó a decir:

—Es un poco complicado y de mi exclusiva invención; pero si lo desean les daré la fórmula escrita…

—¡Qué descaro! —comentó fingidamente indignada Pita—; ¡Esto es inconcebible! ¡Vanagloriarse
de que todo en ella es artificio…!

Pero María Luisa, que siempre estaba a la zaga
de Pita para desvirtuar sus malévolas intenciones,
exclamó burlona:

—No, que en ti ese colorcito amarillo que a leguas pregona el agua oxigenada, es natural ¿verdad…?

Por enésima vez el espíritu retozón de la crítica tuvo, por efecto, el hacer reír con gusto a las
muchachas.

Sin embargo, a Nelly, a quien invariablemente
disgustaba aquella actitud de las colegialas, se apresuró a reprobar, con energía, sus dimes y diretes. De paso, lamentó los resultados negativos de su reunión, calificándola de verdadero fracaso, puesto que solo había servido para poner de realce el ningún cariño que se guardaban unas a otras, a pesar de los numerosos años de haber convivido estrechamente. Todo lo cual parecía no haber servido sino para que conocieran al dedillo los defectos ajenos, sin reconocer, en cambio, ni una sola virtud ni una sola bondad en las compañeras.

La dura y razonable filípica de la joven profesora fué rechazada por María Luisa con desenfado:

—Perdón, _my dear teacher_, usted hubiera querido lagrimitas y besuqueos… Y ha habido algo
mejor… Nos hemos comprendido y conocido más,
gracias a Andréïda y… ¡lo despampanante, chicas! —añadió la frívola muchacha que en esta vez
mostraba mucha perspicacia—, nos hemos dicho, con
franqueza, todo aquello que reservábamos y que envenenaba nuestras relaciones. ¿Se puede pedir algo mejor?

Ganada, por completo, la causa, las colegialas la
abrazaron, apoyando con calor lo dicho por ella;
por lo que Nelly se vió forzada a admitir cordialmente que ellas tenían razón.

Mientras tanto, Pita, que maquinaba en silencio
el apoderarse de los secretos de tocador, que, al decir de la propia Andréïda, constituían la base de su belleza, aprovechó la ocasión que le daba el repentino silencio de las colegialas para reconciliarse aparentemente con la primera e interrogarla, a conciencia sobre el apetecido objeto.

—Ya que has nombrado de nuevo a Andréïda,
María Luisa, —comenzó diciendo para desvirtuar,
en parte, su gran curiosidad— que nos diga ahora
con qué menjurges ha dado esa blancura, ese color
y ese satinado maravillosos a su cutis…

Todas apoyaron con calor la petición oportuna
de Pita; y, Andréïda, examinando fijamente a ésta,
y dirigiéndose al grupo en general, dijo:

—Atrtificio también, todo artificio. Como artificio son las formas esbeltas de mi cuerpo. —Y con
un asomo de superioridad triunfante, añadió—. Ustedes luchan, sin éxito, por comprimir sus caderas
de latinas que les dan aspecto de ánforas pasadas
de moda… Yo, veánme —y dió una graciosa vuelta
en redondo que puso de manifiesto lo que trataba de
probar verbalmente.

—_Slender figure_ —fué el comentario aprobativo
de Nelly.

—Ciencia y nada más —afirmó Andréïda—. Ciencia de los ejercicios, ciencia de masaje, ciencia bruja moderna…

\* \* \* {.centrado .espacio-arriba1}

Por la maravillosa cara pasó la ráfaga enigmática, razón fundamental de su ser extraordinario. Y
hubo algo como un aleteo genial en la voz que, a
continuación, evocó, no tanto para las colegialas, como para ella misma, el proceso de su transformación. Transformación que había obrado el milagro de crear un nuevo ser dentro del suyo propio.{.espacio-arriba1}

Con pasmosa realidad, su verbo suave, de cadencias magníficas y sugerencias insospechadas,
hizo surgir, en eclosión bellísima y ante los maravillados ojos de las colegialas, el mito mecánico de Villiers, pretexto y causante directo de aquella su transformación desconcertante.

En efecto, el fruto maléfico y bellísimo de aquella imaginación diabólica y señoril que llevó por
nombre, en la tierra, el de muy noble Conde Matías
Villiers de L'Ile Adam, había sido la chispa reveladora, el polo magnético a cuyo contacto el cerebro de Andréïda se iluminara dando cauce concreto
a su, hasta aquel momento, indefinible sed de superación.

La admirable ficción escrita de Villiers había
caído en manos de la extraordinaria joven hacía
unos años, y precisamente en ese momento psicológico y difícil en el que la mujercita deja bruscamente de ser niña, y un día despierta en su camita blanca con la imaginación inflamada, hasta lo absurdo,
y con el corazoncito, que le late de manera desbocada, deseoso de emprender hazañas guerreras a lo Juana de Arco, o anhelar martirios como los sufridos por los próceres del Cristianismo. Todo lo cual no es, en el fondo, más que la naturaleza que busca, por todos los medios, su desbordamiento violento. Esta es la época de la vida en la que, hace más de un cuarto de siglo, las mocitas sentian una repentina vocación religiosa, y sus familiares se apresuraban a deshacerse de ellas cómodamente, internándolas en los conventos.

Sin embargo, para Andréïda —frondosa morera
cargada de crisálidas que solo esperaban oculto calor, de cualidad divina, para metamorfosearse totalmente— el albor maravilloso de su existencia tuvo muy otros anhelos y nutrió otra clase de ilusiones, dando origen a la gestación de un sueño audazmente formidable.

La lectura de la Eva Futura de Villiers, en aquella época trascendental de su adolescencia expectante, marcó en su cerebro el principio del fiero
escape a los dominios trillados e inexplicables de la
vida, el mismo que debía señalar su existencia, de
manera inconfundible, en el futuro.

Sus manecitas de niña, ya con impaciencias de
mujer, al pasar una a una aquellas hojas del desconcertante libro, y leer en ellas el milagro escrito de una pluma maestra, a cuyo conjuro surgía Miss Hadaly. sublime criatura, síntesis de lo prodigioso en idealización e ingenio inauditos, cun todo su maleficio deslumbrante de Eva paradoxal que obligó a pensar a la Andréïda carnal en una posibilidad mayor, es decir, en la Eva perfecta.

Sin el más leve desfallecimiento intelectual, la
adoslescente que era entonces Andréïda, afrontó la
exhumación de Miss Hadaly; llena de inquietante belleza, en las profundidades misteriosas del mágico
Menlo Park. Por ella vino a descubrir los arcanos
de aquella naturaleza mecánica, destinada a confundir la insuficiencia intolerable de la vida, desde el preciso instante en que no era otra cosa que
el esqueleto de una sombra, hasta la creación triunfal de esa misma sombra consumada.

Seguramente que el Conde Villiers no sospechó
nunca que, al conjuro de su evocación, henchida de
lo maravilloso y lo absurdo en cuya conjunción fortuita plasmara la vida imaginaria de tan extraordinario engendro, una Andréïda de carne celebraría con él, lustros más tarde, un pacto tenebroso de emulación superior.

Y, sin embargo, y no de otra manera, así fué.

A medida que el ambiente en que se mueve la
Andréïda mecánica, asumió perfiles cardinales en
el mito de Villiers, la pequeña Andrea supo que, desde aquel preciso minuto, ella nacía a la verdadera
vida convertida, de pronto, en Andréïda carnal. Por
derecho incuestionable de asimilación cerebral, desde aquel instante el lugar del parto doloroso de Miss Hadaly, fué el suyo propio. ¿Y cuál mejor y más
hechicero podía haber escogido la ambiciosa y extraordinaria colegiala que aquel con el cual Villiers rodea a su sorprendente engendro?

Bien podía, pues, placer a la adolescente el dima magnífico con que su creador dotó a la inimitable Miss Hadaly y que, en hermosa floración de
imágenes reflejaba vívidamente una mezcla calenturienta de Alkazar de Bagdad y de museo sutilísimo del prolífico inventor americano que, al genio
francés, se le antojase un mago de ensueño.

Juzgadlo, por un momento, vosotros mismos.
Imaginad una amplísima estancia en las profundidades de la tierra y en la que, en promiscuidad lujosa, se ven esparcidos fastuosos y floridos arriates de artificiales flores prodigiosas, agitadas por
un céfiro purísimo imaginario que unge de rocío los
pétalos impregnados de exóticos perfumes. Por entre el suave follaje discurren aves del paraíso, y
mil otros pájaros cuyas gargantas, a cambio de sus
trinos usuales, emiten las romanzas más bellas de
los cantantes famosos de nuestro siglo. La sombra
glauca de una fontana de alabastro eleva el plateado líquido de su esbelto surtidor hacia la celeste
bóveda artificial y hace caer, en torno, la música
monocorde y obsesionante de la hermana agua. Y,
por último, una lámpara de plata que proyecta un
círculo brillante de luz azul pálida revela al espectador, frescamente iluminada, la maravillosa presencia de Miss Hadaly. Tal el sorprendente aposento en que se mueve ella. No es de extrañar, entonces, que, a una adolescente, enloqueciera el deslumbrador espectáculo haciéndola soñar un mundo nuevo.

\* \* \* {.centrado .espacio-arriba1}


La actual Andréïda de carne había obligado a
las colegialas a asistir a la génesis de su vida superior, creando con la música de sus palabras el fantasma casi visible de una vida más bella y más rica, superpuesta a la usual; supremamente rebelde a la larva mediocre, despreciable e infinita de todos los largos días transcurridos desde la Creación. El cerebro sencillo de las colegialas había sido excitado tentadoramente y sus rostros se replegaron en un recogimiento estremecido por indefinibles sensaciones nuevas, reveladoras de un futuro imrerio de amor y terror sin límites.

No parecía si no que el pensamiento poderoso de
Andréïda se hubiese inflamado al contacto de
auditorio, dúctil a las más variadas sensaciones y
que estaba lejos de adivinar en ella la inminencia
del delirio de sentirse ÚNICA.

De ahí que el impetuoso espíritu de Andréïda
siguiera generando, en súbita fecundación asombrosa, imágenes de un fuerza y una luz propias,
que propagaban algo como una suprema felicidad
intelectual, en cuyas maravillosas ondulaciones se
fundiese el doble prodigio de su bello cuerpo y de
su alma original.

La vida perfecta, por ella anhelada tan apasionadamente, se hundía en la profundidad de sus pupilas y hablaba por ellas la sinuosidad elocuente de los labios hermosos, en exaltación terrible.

Las hermosas y perfectas palabras fluían impetuosamente, revestidas de no se sabía qué esplendor divino y alucinante, haciendo perder el sentimiento de la propia vida y elevando al espíritu al sortilegio desconcertante de lo ficticio.

La respiración de las jóvenes se hacía difícil
por la proximidad ardiente de la milagrosa transfiguración que se operaba en Andréïda, y que respondía a una necesidad de perfección y belleza, obligándola a vivir en superación portentosa.

Todo ésto tan desconcertante lo palpaban las
colegialas; pero asimismo percibían también como
una exasperada voluntad de establecer entre ellas y
Andréïda una diferencia profunda e inaccesible a
su vida interior.

Hízose una pausa en cuyo silencio la deslumbrante evocación dejó el rastro luminoso de lo fugitivo y la voz de Andréïda descendió de haber rozado
lo inaudito, para seguir diciendo en tono fríamente
analítico:

—Narraba a ustedes que, el Conde Villiers comprendió, como nadie, las razones que al hombre lo
obligan a no ser fiel a ninguna mujer, y encontró la
causa de esa sed insaciable que lo empuja a libar en
el cáliz de todas las flores, buscando una perfección
que no encuentra. Anhelando la mujer que satisfaga su carne y le muestre, a la vez, el abismo de
una espiritualidad que reúna en sí toda la gama variada e infinita de los espíritus femeninos. Por último, una mujer que responda a sus pensamientos
con la vaguedad inteligente de un pensador metafísico. Por ello, el conde Villiers, fabricó una mujer perfecta encarnada en este autómata a quien llamó Miss Hadaly. Una obra maravillosa de mecánica y
electricidad… —La voz de Andréïda se opacó sensiblemente para, en seguida, dejar sentado rotundamente— Pero ese Robot de su imaginación no podía hacer daño a la Mujer…

De ahí que Huysmans —añadió— ese fauno cínico que únicamente supo beber el amor en las copas que se venden, y cuya originalidad descansó siempre en lo monstruoso, como si un anhelo insensato de libertarse de los tentáculos burócratas que lo aprisionaron tediosamente toda su vida, le impulsara a recompensarse a sí mismo, marchando imaginariamente él también a contra pelo, concibió
con visión más clara de la realidad, un final más lógico para esa Andréïda que el que cobardemente
le diera su creador Villiers. Un final vergonzoso para el hombre… Verán ustedes.

El poseedor de la Mujer Artificial sería atraído
un día por las piernas desnudas de una moza de alquería, y, al volver… destruiría la maravilla mecánica que le fué donada por el Mago de Menlo
Park. ¡Siempre el triunfo de la carne bruta sobre
la materia inteligente!

Pues bien;—prosiguió con firmeza Andréïda—
yo he ido más allá de Villiers… yo he fabricado
con mi propio protoplasma esa perfección por él
anhelada y que responde al deseo oculto de todos los
hombres… Para mis ocultos fines el hombre no
será más que el material necesario con el cual fundaré mis dominios… Sin embargo, yo seguiré siendo para él la atracción ancestral del fruto prohibido y, al propio tiempo, le ofreceré a la vista una
forma más bella, más estilizada por la civilización
de mi cuerpo y mi alma…

La voz suavísima de Andréïda mecía inquietamente a las colegialas, sembrando en ellas la confusión espiritual y llevándolas hacia un mundo desconocido en tercer plano.

—Decía que —continuó la joven después de brevísima pausa— puesto que Villiers, por privilegio
incontestable de genio, creó su Andréïda, su autómata, su imitación humana a imagen y semejanza nuestra, aherrojando de esta manera la Ilusión
en una forma artificial, haciendo prisionero al ideal
dentro de una maravilla de electricidad y de ciencia, plasmando una alma sugeridora de esas impresiones bellísimas y elevadas que hacen vibrar nuestros nervios con estremecimientos de eternidad. Yo también, “fantasma por fantasma, quimera por quimera”, me he creado a mí misma; he metamorfoseado mi realidad humana en artificialidad sublime… Para ello no he tenido que luchar como Villiers, con fenómenos químicos y físicos, ni he hurgado con tesón el Infinito para robarle sus fórmulas de vida y engañar el corazón y los sentidos con
un maniquí electro-magnético, revestido de materia
radiante… No, mi tarea ha sido más difícil, aunque menos complicada… He desafiado a Dios más
gravemente… Y yo, al igual que la muñeca artificial creada por el genio de Villiers, tengo en mis
manos los anillos que en ella regían sus movimientos, sus pensamientos y sus acciones. solo que mi poderosa voluntad ha variado la naturaleza de esos anillos, sín aminorar, por ello, su efectividad. Y son, los anillos que forman mi razón; una razón que regirá mi cuerpo de manera inflexible; que anulará los instintos bajos de la carne… No seré yo quien haga entrega de la simbólica flor de oro al Amado… En cambio, seré una autómata de la vida que abandonará sus caminos trillados… He logrado libertarme de la carne maldita que desde las primeras
noches del Mundo nos encadenó, en calidad de siervas, de seres inferiores, al homo sapiens… Por
primera vez en la historia de los siglos, la mujer dominará al Mundo, y el hombre se arrastrará a sus
pies irremediablemente vencido… Ese hombre que
se hace cada vez más débil, abandonándose al demonio de sus deseos, y en quien la Era nuestra diluye, día a día, los restos de su voluntad. La enorme voluntad que en el pasado, y con justicia, lo hiciera Rey de la Creación…

Las últimas palabras de Andréïda fueron acogidas con el más significativo de los silencios.

—Preguntaban ustedes, hace un rato, qué iba
a ser de mi —imperturbable prosiguió Andréïda, y
con un leve movimiento de hombros y fácil ironía
en las palabras, añadió— No lo sé ni yo misma, En
cuanto interrogo a las hojitas de té algo sobre mi
persona, se interpone una niebla que me impide
ver con claridad. Sin embargo, ya he saciado vuestra natural curiosidad que obligó a que saltara de
los labios de María Luisa una pregunta, a pesar de
que su sentido común le apuntaba que era ilógico el
hacerla.

—Es verdad—asintió Nelly, rompiendo el mutismo general—pero con tus palabras no has hecho
otra cosa que oscurecer, aún más, el enigma. ¿Quién
eres tú, sí, quién eres tú?… Fácilmente nos responderán que alguien a quien hemos conocido desde
niña y que encarna a la mujer por excelencia, la
misma en la que adivinamos todas un algo superior; pero que insospechadamente abriga en su cerebro un abismo de locura que solo de imaginarlo
estremece nuestro espíritu.

—Eres la mujer —terció María Luisa— que aborrecemos las mujeres y que atrae a los hombres extraordinariamente… embrujándolos con oculto hechizo…

—Lo crees tú así? —interrogó Pita con incredulidad ofensiva y ya repuesta del asombro que le
causara la revelación de la compañera extraordinaria— ¿Y yo que te creía a tí, María Luisa, la acaparadora del _it_?

Y lo soy —afirmó riendo la juvenil muchacha—
Pero de un “_it_ clarabaulesco”, el _it_ un poco zafio de la carne.—Y luego, volviéndose orgullosamente hacia Andréïda, dijo— ¿Me creías más ignorante…? Ya lo ves que no; estoy contenta con el “rol” que tú me has dado y que, por lo demás, la vida se hubiese, más tarde, encargado de adjudicármelo… Y no solo estoy contenta, sino que ahora que lo conozco al dedillo, haré de él _mon affaire_”, como diría nuestra insigne profesora de francés, Mlle. Vatard.

Profundamente disgustada Nelly por el giro escabroso e impropio que había tomado la trascendental conversación de aquel grupo juvenil, exclamó
airada:

—¡Es cínica vuestra manera de hablar!

Andréïda contempló de pies a cabeza a su dilecta amiga y con acalorado esceptismo, afirmó:

—No; la cínica es la vida. Es la vida insensata
que se complace en hacer ejemplares como María
Luisa, muñecas de placer y de lujo que hacen inútiles los esfuerzos de los padres, de las religiones y de la educación, porque lo llevan dentro, en la carne ardiente y fogosa de que están plasmadas… Y es cínica también la vida cuando crea mu1erc¡tas dulces como Marisabel; mujercitas de hogar, santas y abnegadas, de una sensibilidad exquisita que después se ve cruelmente defraudada y azotada en los embates con que ella castiga la insensatez de ser buena, de ser dulce, de ser abnegada. Y es más cínica aún, cuando se deprava y arroja seres como Pita, material y espiritualmente deformes. Y qué decir del cinismo de la vida cuando permite que encerrando tú, Nelly, ese tesoro de bondad, ese caudal de sabiduría, esas reservas inmensas de amor, las envuelve en una forma poco amable, a la que tú, en tu impotencia, te empeñas en empeorar, masculinizando tu indumentaria, ocultando con gafas la dulzura de tus ojos que es la dulzura de tu alma misma; enronqueciendo tu voz con los acentos de una severidad brusca y forzada; lanzándote a combatir en el terreno de hombre y abandonando la misión femenina de la maternidad, para la cual estás mejor dotada que ninguna de nosotras, y que te es vedada por un terrible y cruel absurdo de la vida maldita. ¡Pobre futura solterona que malgastará el caudal de su amor en minucias!

La voz cálida y apasionada de Nelly se precipitó para negar con violencia el audaz aserto de Andréïda.

—En minucias, no; —dijo—que mi vida ya la
le consagrado a rehabilitar a la mujer, a redimirla…

Las palabras se ahogaron en la garganta de la
joven profesora, los azules ojos se volvieron suplicantes a todos los rincones del aposento como en
trágica espera de razones de mayor peso.

—¿Ves…? —apuntó compasivamente Andréïda
y su lógica, despiadada por justa, la obligó a añadir—No encuentras palabras para determinarme
esa misión, de la cual siempre te has mostrado orgullosa… Te rodea el vacío… Tu leal feminismo
se estrella ante la Vida—e imperceptiblemente, pero con una fuerte intención sugirió—ante una María Luisa y una Pita sin redención; contaminadas
antes de que lo sean en realidad…

Las aludidas al escuchar la comprometedora opinión de su compañera se apresuraron a interrogar:

—¿Y tú, Andréïda? —Inquirió María Luisa—
Ya nos has encasillado a todas; a todas nos has clasificado… Tú… ¿qué casillero te reservas…?

Andréïda se limitó a sonreir levemente sin contestar.

—¿Cuál es tu feminismo? —a su vez, preguntó
Pita— ¡En esta época, todas tenemos el nuestro!

Los luminosos ojos de Andréïda se ensombrecieron por influjo de una pasión reconcentrada.

—Yo no tengo ninguno —afirmó despectivamente—. Yo no soy feminista… —añadió con los labios apretados y acento amenazador— …Yo no quiero redimir a nadie… Yo soy el efecto de todas esas causas que he señalado y que hacen de la tierra el infierno de las Mujeres… No esperen nada de mí, al contrario. Yo las esclavizaré aún más, hasta hacer de ustedes simples máquinas de reproducción o de placer… Pero, también les digo que yo las vengaré con refinamiento jamás imaginado; a ti, María Luisa, eterna explotada que se finje a sí misma explotadora y vencedora del hombre; a ti, dulce Marisabel, a vosotras las dos, seguras víctimas del macho. Y a ti, Nelly, y a ti, Pita, con quienes la vida se ha ensañado despiadadamente.

Las colegialas, en su estupefacción, no acertaban a dar con las palabras acertadas que reflejaran su desconcierto, Unicamente Nelly alcanzó a exclamar, realmente sobresaltada ante el abismo que se abría a sus pies y que le revelaba la personalidad enigmática de aquella niña, a quien había visto crecer a su lado, sin sospechar la naturaleza tan extraordinaria de que hoy se revestía su ser privilegiado y pródigamente dotado con todos los dones que hacen sobresalir al ser humano, o sean, una desconcertante inteligencia y una belleza física milagrosa.

—¡Tu exaltación me asusta, Andréïda —dijo la
joven profesora—. Vuelve a la razón, temo en tí un
desequilibrio mental.

— Desequilibrio mental!—comentó de manera
despectiva la joven, para añadir con redoblada pasión—Jamás, jamás ninguna de vosotras llegará a
razonar con la firme serenidad que lo hago yo, por
la sencilla razón de que he abolido en mí todo aquello que hace de la mujer una presa fácil… La carne que hace de María Luisa una mujer sensual; el
corazón que hace de Marisabel una supersensible;
la imaginación que hace soñar a los azules ojos de
Nelly, tras las gafas ahumadas… Y como ya les he dicho que en mí no hay carne, mis entrañas heladas no pueden jamás caer en aberración. Soy recta en espíritu y en cuerpo; he matado el corazón y
he muerto a la materia… ¡Que me tema el hombre… que me tema la Vida! Quisieron saber de mi
y lo han logrado plenamente… Hoy me afirmo, nc
como mujer que es bien poco. .. sino como un cerebro dispuesto a dominar en favor de mi yo perfecto… Lo ha dicho Nelly, en el umbral del colegio
bifurcan nuestros caminos… Oirán hablar de mí

El timbre estridente de la Administración,
anunciando a las colegialas la llegada del autobús
y, al mismo tiempo, el que debían estar en sus respectivas habitaciones para hacer entrega de sus
baules, apenas si dejó oír defectuosamente el comentario trascendental de Nelly, a las últimas frases de Andréïda.

—¡Tienes el mismo orgullo rebelde que hizo
caer al arcángel de la leyenda mística! —El acento
con que acompañó a sus palabras tenía algo de terrible admonición bíblica.

Las colegialas fueron saliendo de la habitación
de Nelly con una inacostumbrada seriedad en los
ojos y una pensativa discreción en los ademanes.

Al salir, María Luisa, todavía pronunció una
última frase dirigida a Andréïda:

—Nos asombras; eres absurda y, sin embargo,
algo me impide el reírme de tí y de tus palabras.

Andréïda asintió con dulzura y, cambiando el
tono grave de hacía un momento, con sonrisa enigmática la invitó a que riese de todo lo dicho por ella, diciendo:

—¡Si todo lo que he dicho ha sido con ese objeto, para que rían y para hacerlas pasar el rato!

Otra vez se dejó oír el llamado perentorio del
timbre y de una voz femenina y gangosa que anunció a lo lejos: autobús, _five minutes_.

A raíz de la advertencia, todas las colegialas corrieron en desorden, dejando a Nelly y Andréïda
solas.

Por un momento las dos grandes amigas se miraron a los ojos como dos desconocidas, hasta que Nelly hizo un reproche con acento sinceramente dolorido:

—No debiste; de ninguna manera debiste hablar en esa forma tan descarnada…

Con una sonrisa levemente irónica en los labios de bellísimo trazo, Andréïda replicó:

—¿A mi querida feminista le asustan las palabras?

Había en el tono de la frase algo tan acariciante que desarmó los reproches violentos que Nelly tenía ya en la punta de la lengua.

—Bien sabes que no —hizo constar la profesora— pero es que me duele que tú hayas sido, precisamente, quien cometiese el sacrilegio de descorrer
el púdico velo con el que la vida sabiamente se cubre y que debe siempre nublar la vista de las jóvenes.

—Y eres tú, mujer moderna, quien me lo dice? 

Esta vez el acento de Andréïda tomó la dureza
metálica de los grandes bronces.

Nelly, con un desasosiego que en vano intentó
traducir en palabras, repuso:

—Andréïda… tú no has mentido… Tú no
has representado una comedia atrevida para hacernos pasar el tiempo; tú no has dado vida a una broma. Por primera vez he ido al fondo de tu pensamiento y me horrorizas…

—¿Estás segura de haber ido al fondo de mi
pensamiento? No lo afirmaría yo tan rotundamente… 
—Advirtió Andréïda con el brillo divertido
en los ojos de aquel que observa la impotencia de
alguien que intentase descubrir lo perfectamente
oculto.

—Sí, tienes razón… siempre tienes razón…
——Reconoció Nelly revelando honradamente su.impotencia —El enigma se hace más profundo—. Y esta vez la expresión inteligente de la carita feucha, expresó un horror mal contenido para decir
—Andréïda, eres un monstruo, has ido más allá… has
ido demasiado más allá…

Al escuchar las anteriores palabras, las facciones de Andréïda se endurecieron, y, observándolo
Nelly, se apresuró a añadir con doliente arrepentimiento:

—Oh, perdóname, no sé lo que me digo… ¡Te
quiero tanto…! No sé cómo he podido decirte eso;
ha brotado de lo más hondo de mí misma y, sin
embargo, antes de pronunciar esas palabras, hubiera dado la sangre de mis venas por evitar decírtelas y, más que nada, por haber eliminado el motivo que me ha obligado, de manera irresistible, a
pronunciarias…

Se hizo una pausa en la que Nelly se acercó y
trató de coger a Andréïda; pero no pudo menos que
retirar la mano haciendo un involuntario, pero irreprimible, gesto de repulsión.

El abismo se ahondó entre las dos jóvenes mujeres.

—¡Qué es ésto, Andréïda? —inquirió espantada
Nelly— Vuelve en ti… —y, pasándose la alargada
mano por la frente que percibe bañada en sudor
frío, añade— ¡Oh, no; soy yo la que debe volver en
mi… perdóname… mil veces perdóname…

Nelly se ha quedado mirando intensamente a
Andréïda, y, con horror creciente, da un paso hacia atrás, diciendo:

—Esos ojos… esa frialdad extraña de tu carne… ¿Qué has hecho de tí misma…?

La pregunta ha sido honda, como una pregunta hecha al infinito, a lo incognoscible.

—Qué aberración de aberraciones has cometido…?
—prosigue Nelly— si fuese creyente o ignorante pensaría que estás poseída del maligno, como en época medioeval… que has enajenado tu alma al diablo…

Las lágrimas asoman a los nobles ojos azules y
la voz se torna suplicante para añadir:

—Andréïda… y si así fuese…

El silencio de Andréïda deja el abismo abierto
mientras su grácil mano se tiende sobre él en un
pacto amistoso que rehuye Nelly.

—¡ Andréïda, Andréïda! ¡Qué misterio encierras, Andréïda! De qué pecado tan grande debes
dar cuenta a Dios…

Las últimas palabras han sido pronunciadas en
el tono incoloro en que podría haberlas emitido una
sonámbula.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo IV

La llegada de Nelly y de Andréïda a México, la
Ciudad Portentosa, significó el principio de sus vidas independientes y responsables.

Nelly había deseado, a su arribo, poner en práctica su deseo previsor y modesto de habitar en una
casa de asistencia de las muchas que se anuncian en
los periódicos; pero, Andréïda, —que por cierto
abundaba en los conceptos del Crispín benaventino,
moderno Sancho Panza inteligente, respecto a considerar la presentación como lo más esencial en la
vida —alegó firmemente que sería de todo punto imposible el triunfar desde una bohardilla o desde un
miserable sótano maloliente y obscuro. Y no hubo
más remedio que arrendar un departamentito en
un lujoso hotel de la colonia Hipódromo, pues vanos fueron todos los juiciosos argumentos que Nelly
exhibió, poniendo de manifiesto el que, con aquella
renta exagerada, sus modestos ahorros se verían
consumidos totalmente en menos de tres meses.

Los ojos luminosos de Andréïda, encendidos por
el deseo indomable y portentoso de triunfar a toda
costa, opusieron una resistencia sonriente a la que
no hubo otro camino que doblegarse.

Y empezó para las dos jóvenes una vida de apariencias; de aquéllas que, si las corona el triunfo, son obra de inteligencia y, si las conduce al fracaso, obra de pillos.

Nelly bien pronto encontró acomodo dando clases de inglés a particulares, y colaborando, en sus ratos de ocio, en la sección de sociales de uno de los
principales periódicóos de la capital.

Desde luego, para Andréïda fué muy distinto.
Se resistía a admitir un trabajo rutinario y poco interesante y, por otra parte, desconocía, ella misma, aquellas habilidades suyas de las cuales podría extraer provecho y convertirlas en medio de sustento y hasta de lujo.

Uno de tantos días, al atardecer, discurría Andréïda, con ese paso rápido que le era habitual y al que difícilmente podía ajustarse compañera alguna,
por la Avenida Juárez, que se había convertido, desde su llegada, en su paseo favorito.

Algo como la sombra del fracaso nublaba la expresión radiante del bello rostro y su grácil andar se resentía por la huella artera de una fatiga interna.

Casi no percibía la finísima lluvia que empapaba su cuerpo, con la fatigosa terquedad que asume
en las tardes nubladas de México, remozando diligentemente las fachadas de los edificios y lustrando la lisura obscura de los pavimentos.

Comenzábanse a encender profusamente los
anuncios luminosos en rojo, azul, verde, amarillo,
fingiendo una inundación de luz, con toda la claridad eléctrica, exasperante e insolente del mal de la época: publicidad hueca, desmesurado engaño global al Universo.

En el fondo de la amplia Avenida, con un señorío helénico que se traga la tierra movediza, harta de sostener el peso muerto de civilizaciones caducas e importadas y que mal arraigan en el virgen
suelo de propias e inmensas fuentes potenciales, se erguía el gran Palacio Blanco, el de anacrónica caperuza amarilla, y que tiene a su derecha una serie numerosa de árboles domesticados, en juego servil y apropiado a la naturaleza de su grandeza.

El gran Palacio Blanco, en aquella tarde como
en todas, contemplaba con tristeza de gran señor que añora tiempos pretéritos, a pesar de estar ya hecho a las humillaciones de la época, el insolente compañero a su frente que haciendo ridículos pininos, con sus escasos pisos, en un perenne deseo de rascar el cielo, ponía tristemente, de manifiesto,
su intento frustrado y caricaturesco, convertido en
mala y desencajada copia de lo americano.

Por un momento, Andréïda, interrumpió su andar rápido frente a los escaparates deslumbrantes
con su profusión de cristales y chucherías costosas.

Sus ojos seguían, sin ver, la fuente charolada
y polícroma de los automóviles, cuyas luces tamizadas, avanzando frente a ellos como dos chorros
de luz paralelos, se multiplicaban al reflejarse en la lisura negra de la avenida. Los pequeños y abombados círculos rojos y verdes de los semáforos regían, automáticamente, su marcha, deteniéndolos u
obligándolos a avanzar con un reprimido rumor de
velocidades en primera.

Había algo fresco, argentino, brillante, en el espectáculo crepuscular de la gran Ciudad y que reavivaba en Andréïda su deseo de triunfo, sumiéndola
en el iluminado éxtasis que le era habitual en los momentos de sus extrañas exaltaciones, las que, alternativamente, se veían desfloradas por la sombra del fracaso y del no saber, con exactitud, cómo
encauzar los rumbos de su inteligencia.

El deseo supremo de Andréïda ascendía por entre la premura agitada, ondulante y oscura de la multitud que discurría a su lado sin reparar en ella.
¡En ella que aspiraba un día en llegar a forzar
aquella indiferencia, al parecer imperturbable!

Sobre la agitación imprecisa de la muchedumbre se elevaba el deseo de Andréïda, mientras sus bellos ojos se posaban con admiración, que la costumbre no llegaría nunca a hastiar, en la majestuosa sinfonía marmórea a su frente y que ella presentía aún tibia por la impresión genial de los pulgares plásticos que, a leves golpes de frágiles cinceles, habían sabido modelar su portentosa grandeza. Aquéllos mármoles de clásicas morbideces alimentaban, calentando, su desmesurado anhelo triunfal.

Exhausta, sofocada ante el peso del doble prodigio de su anhelo y el mármol, cuyo ritmo e imagen se acoplaban penetrándose en la excitación de su pensamiento, plasmóse, de pronto, para ella, con
todo el esplendor de una revelación, el sueño cerebral de su orgulloso espíritu, tal y como debía realizarse.

La Ciudad espléndida tendíase a los pies de Andréïda, abierta a su deseo, desplegando la riqueza
toda de sus cuatro arterias que van a desembocar
en la monumental Plaza de la Constitución. Había
como una promesa fácil en la luminosidad alegre
de sus arbotantes y de sus escaparates. Y la Ciudad prometía… prometía… no importaba que más
tarde cobrase en la forma y moneda que ella sabe
hacerlo, y que bien conocen los incautos que por primera vez la contemplan y en ella creen.

Pero nada de ésto interesaba ahora a Andréïda
La grandeza de la ciudad palaciega había logrado
el milagro de reavivar, en la joven, la condición misteriosa de la cual necesitaba para hacer nacer la
obra de nueva belleza, de nueva armonía, destinada
a alterar los cauces conocidos de la tierra. A su
conjuro, Andréïda tornaba a conquistar la seguridad maravillosa de sentirse única y predestinada a
realizar el deseo imperioso que iba tomando modalidades y perfiles de un extraño y desconcertante
apostolado. Se renovaba en ella la génesis de su vida superior, amenazada de frustramiento, por no
encontrar empleo adecuado a sus facultades extraordinarias.

Una vez reafirmada en sí misma, examinó con
nuevos ojos el resplandor amarillento, azul, púrpura y oro de la gran Ciudad y volvió sobre sus pasos. Comprendió que necesitaba vivamente el que
su reciente resolución tuviese una expresión inmediata. Dejó atrás el monumental Palacio Blanco, sin ningún otro pensamiento que el de reflejarse por
entero en la acción.

La cabeza, hasta hacía poco inclinada, fué echada ligeramente hacia atrás, con aquel su gesto acostumbrado de desafío juvenil. solo la contracción
convulsiva de los labios denotaba la tensión extrema del espíritu. ¡Parecía que, en los imaginarios y
fugaces intervalos luminosos de un relampagueo,
gsu cerebro tratara de descubrir los trazos máximos
que, más tarde, desenvolverían su nueva vida!

Lejanísima, extraña, indiferente al ronroneo admirativo que su paso levantaba, como si aquel murmullo paradógico de caracol marino, cuya monótona repetición poco ingeniosa de sobados piropos
deshojados en honor de sus ojos, de su boca hechicera, de aquel cuerpo juncal y juvenil, maravillosamente formado, no mereciesen de Andréïda la mirada furtiva y ligeramente agradecida con la que
las mujeres suelen premiar o finjir enojo ante estas puerilidades de los hombres latinos.

Absorta en su torbellino interior, forzaba las
raíces de su propio ser para encontrar la respuesta
justa a la suprema pregunta que había sido renovada al soplo y reflejo de las luces, como nada en el
mundo superlativamente distintivas de la grandeza
de las cosas.

Su deseo crecía en un abrazo desmesurado que
intentaba abarcar al Universo.

El grato tintineo argentino de las campanillas
del reloj de El Universal, labrando en coro aéreo el
Himno Nacional, pareció comunicar con sus vibraciones el sentido de la realidad en Andréïda.

Con un leve estremecimiento que la hizo recordar algo, torció por la calle estrecha de la izquierda
y se detuvo ante un oscuro portón entreabierto.

El ruido de las máquinas impresoras la advirtió
que había llegado y, por un segundo rapidísimo, su
paso vaciló un instante, temeroso de ser apresado,
ligado para siempre a aquéllo. Aquéllo que en aquel
momento significaba para Andréïda entrar a la vida común de todos.

Su desfallecimiento fué momentáneo. Había que
hacer concesiones, se dijo; pero sus piernas temblaron, un poco, al subir lentamente los escalones de carcomida madera. Y con el temblor impreciso del que se ve obligado a pedir algo que puede serle, con facilidad, negado, penetró al estrecho saloncito en el que se oprimían dos escritorios atestados de papeles y en los que parecía imposible que el dueño pudiese encontrar el pliego preciso que necesitara.

Un jovencito que, inexplicablemente rendía todavía culto al traje poco pulcro de que gustara Mimí Pinsón, tecleaba, de manera febril, en una vieja
máquina Oliver, colocada en una mesita cercana a
vno de los escritorios. Sus ojos cansados, que de
sobra se sabían pendientes exclusivamente del día
en que vivía aquel desmedrado cuerpecillo de epidermis amarillenta en la que la única coloración rosácea descansaba en los repugnantes barros del cuello y de la faz, se elevaron, un instante, en muda interrogación a la recién llegada.

—¿El señor Núñez…?

—Un momento. —Fué la lacónica respuesta, y
Andréïda quedó sola en aquel reducido aposento del
que no podía salir nada que perdurase arriba de la
breve vida fugaz de una revista, condenado todo fatalmente a marchitarse como flor de un día. El ruido insoportable de las máquinas, adormecido vanamente por la puerta de madera; el fortísimo olor
de las tintas que debían estar usando, en aquel momento, para la portada a colores, ascendían como
fuerzas negativas a toda inspiración. Todo aquello
deprimía extrañamente la ambición desmesurada,
sin límites ni frenos de Andréïda. Nunca como en
aquel momento trascendental, se le presentó a la
joven la imposibilidad absoluta, para ella, de aceptar la vida mediocre de los que trabajan para ganar escasamente el pan nuestro de cada día.

Se alzaron tumultuosamente en el fondo de su
pensamiento todos sus sueños soberbios, todos sus
insaciables impulsos de predominio; pero, sin embargo, se dijo que aquéllo no significaba una claudicación, acasa. acaso… se tratara de una concesión.

Sus ojos que vagaran indiferentes por las paredes del aposento, tapizadas de retratos de artistas
de revista, en poses atrevidas e intrascencentes y
que hablaban muy claro de su poca importancia como sombras humanas, parpadearon al oír a su espalda un ¡Señorita…!

La voz varonil, de acento profundamente español, tuvo la virtud de turbar a Andréïda. Otra vez
cayó sobre ella la sombra del fracaso, sintiéndola
en la raíz de los cabellos como un escalofrío carnal
de insuficiencia. El ruido de las máquinas ocultó
piadosamente, el desconcertado tartamudeo cobarde
de sus primeras palabras. Con un esfuerzo supremo
trató de dominar sus nervios, y al comprender que
su torpeza no había sido percibida por su interlocutor, procedió a exponer, escuetamente, el motivo de su entrevista.

En respuesta escuchó una serie inconsistente de
excusas. Le fué enumerada la copiosa colaboración
que recibía la revista, se alegó en su contra lo desconocido de su nombre y, por fin, se le comunicó que se haría todo lo posible porque uno de sus originales apareciera en el número que en aquellos momentos se estaba imprimiendo. Eso sí, le aceptarían toda la colaboración que desease a guisa de ensayo, sin remuneración alguna, porque reconocía él que había bastante originalidad y fuerza ea sus escritos.

Andréina se hizo cargo inmediatamente. Tendió
su fina mano enguantada y se despidió sin prometer nada.

El adolescente, empeñado en vestir las ropas de
otro siglo y en pergeñar de carrera las exigencias
del actual y, por. lo mismo, completamente impotente para lanzar hacia adelante el atisbo espiritual de
su mirada, ni siquiera levantó la cabeza de su maquinal tarea.

Andréïda bajó con paso vacilante los escalones
carcomidos, mientras una sonrisa amarga y sarcástica desfiguró el trazo noble de sus bellos labios. ¡Y
pensar que ella había creído hacer una concesión…
alargar la mano para recibir una remuneración!

Cada crujido amenazador de los carcomidos escalones marcaba una fatiga, un desengaño, una ilusión frustrada.

El sueño de su vida, magnificado por su cerebro hacía unos instantes, sufría ahora el embate y
la opresión de una ola violenta de desesperanza.

Nuevamente encaminó sus pasos hacia la Avenida Juárez.

La ciudad tentadora y profunda ofreció a su
vista la misma iluminación de hacía unos minutos.
Y, después de todo, ¿por qué había de nublarse su
iluminación ante la minúscula tragedia de un fracaso individual?

Con los dientes apretados, Andréïda profirió quedamente una respuesta:

—Pero es que no se trataba de un fracaso individual… se trataba…

Un coche que ostentaba en el parabrisa el letrero de “libre”, y cuya brillante carrocería se veía circundada por antiestética franja de color, se acercó a la acera.

La invitación del “ruletero” hizo que Andréïda
sintiera, de pronto, la humedad desagradable de su
ropa y la necesidad espiritual de regresar junto a
Nelly. ¡Los seres grandes necesitan más que los pequeños del calor comprensivo de los humanos!

\* \* \* {.espacio-arriba1 .centrado}

—¡Qué pálida vienes, Andréïda! —Fué la exclamación de Nelly al ver entrar a Andréïda en el monísimo departamentito en el que la interpelada había impreso ya su huella personal con dos o tres chucherías de exquisito gusto, y cuya feminidad no
amalgaba, lo que se dice muy bien, con sus desconcertantes ideas. {.espacio-arriba1}

Sin añadir más palabras ociosas, Nelly penetró
en la alcoba que les era común, y violentamente procedió a preparar ropa para Andréïda. Mientras
tanto, esta última se había dejado caer desfallecida en la blandura de un monumental sillón, cuya
comodidad agradecía, de manera inconsciente, en
aquel momento.

Unos minutos más, y Nelly anunció que el baño
estaba listo.

Lentamente Andréïda se dirigió a él. El vapor
que despedía la preciosa bañera de un verde opalino
suavísimo, hizo entrar en calor el cuerpo aterido
de la joven que con apresuramiento se dispuso a penetrar en el agua caliente y perfumada, abandonándose a la dulzura sedante de su calor.

No transcurrió mucho tiempo sin que Nelly penetrara llevando en los brazos una suave bata afelpada y urgiera a su amiga para que no permaneciera mucho tiempo en el agua.

La voluntad de Andréïda sufrió un sacudimiento que la obligó a erguirse. Suaves gotas aperladas,
por reflejo del blanquísimo cuerpo, resbalaron
lentamente, poniendo un brillo maravilloso de flor
ungida con rocío en el prodigio femenino de belleza sin par.

Dos movimientos dulcemente ondulantes y la
bata fué sujeta debidamente a la grácil cintura.

La voz de Nelly llamó con acento maternal:

—Ven, Andréïda, junto al radiador —y, ofreciéndole una copita de esbelto y frágil tallo, la obligó a que bebiese su contenido fortificante.

Una vez reanimada, tuvo la joven una sonrisa
de agradecimiento para la admirable amiga.

—Eres la camarera perfecta —dijo, y, acercando su cara a la feucha de la amiga, dejó en ella el
roce alado de una suave caricia.

Después se acurrucó frioleramente en el sillón
monumental y sus ojos sinceros encontraron los de
Nelly en los que se leía una inquietud que no tenía
fuerzas para expresarse en palabras.

Andréïda, en respuesta a aquella mirada, se limitó a decir:

—Nada, otra vez nada. Tendré que resignarme
a ser siempre tu parásito.

Una suave carcajada argentina fué la inesperada contestación de Nelly, que exclamó, con regocijo, al punto:

—¿Eso era todo…?

—¿Te parece poco? —repuso Andréïda, y para
sí misma se dijo que no era todo, puesto que había
tanto y tanto impreciso detrás de ello.

—Alégrate, Andréïda, no desmayes ante el primer fracaso.

—Pero si no desmayo —fué la respuesta poco
convincente de ella.

—En cambio, yo te reservo una buena nueva —agregó jubilosamente Nelly— ¡Yo te he conseguido
trabajo fácil y bien remunerado!

—¿Tú…? —Inquirió con incredulidad Andréïda.

—Es decir, yo no; pero hoy ha venido a buscarte el ingeniero Alonso. ¿Lo recuerdas? Aquel muchacho joven que nos presentaron en casa de mi madrina el otro día, y que pareció quedarse tan impresionado con tu belleza…

Una muy femenina sonrisa de Andréina demostró a Nelly que había recordado.

¡Claro que recordaba al ingeniero Alonso! El
único ser interesante, a sus ojos, en aquella insubstancial reunión a que había hecho referencia Nelly. Con agrado rememoró ahora la recia figura que
había hecho gemir, con sus pasos, el entarimado viejo de la pretensiosa residencia de la madrina de
Nelly, y que tuvo la virtud de fijar su escurridiza
atención. Los rasgos blandos de aquella fisonomía
clara, de expresión infantil y golosa, se le representaron vívidamente hasta en el detalle de la pesada cabellera negra, charolada, con algo nuevo, de recién lustrado que la había hecho sonreír
aquella noche. Recordó también que a su analista
observación no había escapado tampoco el trazo perfecto, escultural de la frente; pero en la que tras de ella se adivinaba un espíritu limitado. Y el breve bigotillo pueril que sombreaba virilmente la ancha herida de la hermosa boca sensual… ¡Vaya si recordaba Andréïda! Después… la mirada codiciosa de los ojos obscuros que toda la noche habían intentado atarla a su repentina adoración. ¡Oh, la luz
simple de aquellos ojos honrados!… Y el abrazo
violento del fuerte brazo que la sujetó la cintura,
y que se adivinaba ejercido por alguien a quien le
es imposible guardar la actitud mesurada y convencional del que baila en una simple reunión social… Todo lo recordaba Andréïda y con ello el
juicio que, a primera vista, le había merecido el joven ingeniero, por lo que lo catalogó decididamente
entre los hombres que besan la carne de los hombros. Hombres que van derechos a la caricia honda, soberbia, brutal. Incapaces de la dulzura tierna,
profunda, de los refinados, de la suavidad quintaesenciada y trivial, tan dulce a la femeneidad.

La voz de Nelly vino a rescatar de su ensimismamiento a Andréïda.

—Te decía que el ingeniero Alonso —apuntó
Nelly— informado por la charla poco discreta de mi
madrina sobre: la difícil situación económica que
guardamos, se apresuró a hacer gestiones en una
Secretaría de Estado, con el resultado halagador de
haberte conseguido un puesto de importancia, cuyo
sueldo se eleva a cuatrocientos y pico de pesos mensuales, y, al que si quieres, podrás atender o simplemente limitarte a cobrar cada quince días.

Eso no, —se apresuró a rechazar Andréïda, con
un ligero matiz de indignación en la voz.

—Parece ser que eso es usual aquí —respondió
Nelly.

—Pues yo no lo acepto. Bien sabes que repudio
los favoritismos, Nelly, quiero estar segura de que
se me paga mi trabajo y nada más que mi trabajo. No quiero verme obligada a pagar demasiado caro, en el futuro, un favor de esa especie.

—Eso mismo le advertí yo al señor ingeniero,
que estuvo inmediatamente de acuerdo conmigo —observó Nelly.

—¿Y cuándo tomaré posesión de mi empleo? —preguntó a su vez, Andréïda, deseosa de dar cauce, lo antes posible, a su mal comprimido dinamismo.

—De aceptar tú, mañana mismo, puesto que me
comunicó el mencionado señor que únicamente esperaba llevar a firmar tu nombramiento. Es más, hasta me preguntó si tendrías, por casualidad, dos fotografías tuyas, tamaño credencial.

—¡Qué alegría, Nelly, qué alegría! —fué el comentario jubiloso de Andréïda, y con su generosidad innata de noble espíritu, añadió: —La primera quincena que gane voy a gastarla íntegra en tí…

La comprensiva Nelly sonrió bonachonamente.
En sus azules ojos había como un temeroso pensamiento maternal, al contemplar la súbita alegría
de la amiga, hacía solo unos minutos lastimosamente vencida. Sin embargo, replicó con firmeza:

—El primer dinero que ganes deberás entregármelo íntegro para que ingrese al fondo común, y yo
me encargaré de distribuirlo juiciosamente.

Una límpida carcajada acogió burlonamente las
palabras de Nelly.

—Me olvidaba de tu proverbial cuanto fastidiosa previsión —dijo Andréïda— pero tienes razón;
cuatrocientos pesos no son precisamente una fortuna que se preste para hacer grandes locuras.

\* \* \* {.espacio-arriba1 .centrado}

Aquella noche Andréïda apenas si durmió. Su
cerebro calenturiento elaboró mil y mil planes…
Y es que, después de la lucha de tantos días, providencialmente había encontrado esa difícil solución que viene a proveer nuestras nimias cuanto importantes necesidades primarias. Libre, por fin, de
aquel peso agobiador, su horizonte se despejaba con
lo que acrecía su actividad cerebral. Vino a significar, en verdad, el nacimiento de su personalidad, hasta aquel momento cohibida y como en embrión bellísimo y prometedor.

De aquel instante, en adelante, su inteligencia,
al servicio del más descabellado y potente de los deseos, se elevaría triunfalmente, en espiral gigante
y luminosa hacia el azul de todas las ensoñaciones y
superaciones idealistas.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo V

Desde el momento en que Andréïda tomó posesión de su empleo, la vida se regularizó para ella con toda la monotonía deprimente y anodina, habitual al esclavo burócrata.

Se convirtió en una nueva muchacha bella entre
todas aquellas que hacían de la salida del Ministerio un desfile de modas y procuraban trabajar lo menos posible frente a sus escritorios. La hora de
abandonar el pseudo trabajo siempre las encontraba polveando la naricilla, o retocando, con el encendido lápiz, las boquitas tendidas hacia algo ambiguo, en un gesto trágicamente esperanzado, que el
transcurrir implacable de los años se encargaría de
ir amargando.

Y Andréïda pasó también por las alternativas
angustiosas de la cesantía que levanta su espectro cada día primero del año, y también a raíz de marejadas políticas, y la cual se aplica, por lo
regular, sin tomar en cuenta, en favor de la víctima,
su antigiiedad en el empleo ni su gran o poca eficiencia; aunque sí responde siempre a la nefasta
plaga de las recomendaciones de influyentes y de los
compadrazgos políticos.

A Andréïda, sus relaciones con aquel medio singular sirviéronla a manera de vasto campo de experimentación psicológica y social, en el que le fué dado el aprender a despreciar, con asco, a la mujer que trabaja por saciar un indigno prurito de lujo, y que se sirve del empleo como funesto trampolín para hundirse en las esferas munificentes de la prostitución. En cambio, también se enseñó Andréïda a compadecer y amar al otro extremo, o sea,
el miserable sector de las que, obligadas a llenar una enorme necesidad biológica de conservación y que,
por cierto, son siempre las menos favorecidas por
la naturaleza, se encargan de suplir con un trabajo
agobiador y agotante las deficiencias y holgazanerías de las primeras, recibiendo, siempre, más ruines remuneraciones.

Las compañeras de oficina de Andréïda brindaron a su observación especulativa diversos puntos dignos de estudio y sumamente útiles para un espectador aficionado a la escabrosa ciencia social. Por lo que muchas veces fué objeto de conversación, entre Nelly y ella, el particular problema de cada una de aquellas mujercitas.

Estaba, por ejemplo, aquella pobre Tere, mujercita exigua que había ya pasado de la treintena y
gastaba gran parte de su raquítico sueldo en una
búsqueda, demasiado cruel para ser ridícula, de nuevos y engañosos procedimientos de embellecimiento,
con un desesperado anhelo de alargar una pretendida juventud que nunca fuera en exceso amable,
pero que para ella representaba techo, vestido y comida; ya que para ninguna mujer, como para la
empleada, la juventud tiene un precio tan inestimable. ¡Cuántas torturas y cuántas hambres costaba a aquella mujer la aparición de una nueva arruga!

Ahí estaba también Cholita, mujercita agitadora, parlanchina, insubstancial y, en el fondo, un poco demagoga. Preconizaba la entrada de la mujer mexicana a las actividades políticas, persiguiendo con ello un fin “pancista” y nunca un ideal social. Prueba innegable y clara de esto último podía encontrarse, fácilmente, en los particulares oficios a que se dedicaba ella y las que con ella pensaban igual y que eran siempre de intriga, agitación y jamás de reconstrucción social. A menudo sacaba a relucir tres o cuatro frases de oropelescos terminologismos socializantes, cuyo desconocimiento absoluto de su verdadero sentido hacía reír a Andréïda; pero que, sin embargo, a la tal Cholita le redituaban una bonita suma de cierto Partido político, la que venía a doblar, amablemente, el ya crecido sueldo con que se premiaba su irrisorio trabajo oficinesco.

Proverbial era entre todas las empleadas el que
los poquísimos oficios escritos por la tal Cholita,
tenía que rehacerlos la desmedrada Carmela, de manitas nudosas y fantásticamnte ágiles sobre la máquina de escribir o sobre el block de taquigrafía,
quien por lo demás también rehacía el trabajo de
muchas otras y… muchos otros. Feminista y revolucionaria sincera, sustentadora comprensiva de
las verdades esenciales desprendidas de la gran exreriencia revolucionaria mexicana, cuando, por excepción, rompía su habitual mutismo y exponía el
tesoro de bellos conceptos que su despejada y original inteligencia innata elaboraba en el silencio de su actividad maquinal, solía encontrar siempre la impermeabilidad descorazonadora en la conciencia envilecida de aquellas irredimibles mujercitas de clase media pretensiosa y que oponían, de manera sistemática, al Gobierno, la molicie abúlica y recalcitrante que hace imposible la más epidérmica. identificación en ideal social, y cuyas bocas bisbiseaban en los rincones del propio edificio del que extraían, casi estafando, su manutención, la más injustificada, sistematizada y mordaz de las críticas hacia el visible acto gubernamental del día. Críticas que ya
quisiera para sí imaginar y lanzar el más fosilizado de los reaccionarios.

La excepcional Carmela, dotada de una sensibilidad y vitalidad excesivas, las que iba desgranando
apresuradamente en la máquina pulverizadora y estéril de su empleo burocrático, comprendía, de sobra, que sus esfuerzos eran inútiles y que se los
tragaba la gran coladera nauseabunda del egoísmo,
el prejuicio y la abulia, y ésto era, en realidad, la
causa única de su creciente neurosis.

Andréïda, en su inevitable roce con ella, había
aprendido a estimarla en todo lo que valía, a despecho de sus frecuentes accesos de malhumor y de sus salidas intemperantes. Constataba también, en Carmela, muchos puntos de contacto con la personalidad de Nelly; aunque reconocía que, en cierto aspecto, eran esencialmente diferentes, o sea, en elmilusorio cuanto lamentable ideal de la primera empeñada en soñar para la mujer una imposible regresión a los cuatro muros cerrados del hogar; y que era en Carmela un ideal disfrazado, en lo subconsciente, de suprema condenación al obstáculo hostil en que se estrelló, sin hallar cauce, su profunda veta maternal de gran hambrienta de amor.

Desde el primer día fué Carmela la única amiga
que hizo Andréïda en aquella oficina, y a la cual,
con justicia, podía otorgársele tal calidad. Sin embargo, tal amistad no tardó en ser objeto de críticas insensatas y de cuchufletas de mal gusto por parte del oficinesco rebaño femenil, aunque bien es cierto que siempre fueron reprimidas en presencia de la inigualable Andréïda.

Y volviendo, de nuevo, a la revista superficial
de caracteres que informaban el conjunto de compañeras de oficina de Andréïda, tendremos que ha:
cer constar que ninguna de aquellas mujercitas insubstanciales tuvo la ingrata virtud de exasperar el espíritu recto de Andréïda, en la medida que lo hacía una tal de nombre María Eulalia, tipo vistoso,
brillante, extremadamente juvenil y de bellos ojos
falaces que sabían cubrir su depravación canallesca con una mentida inocencia virginal, la cual hacía
años que había dejado de serlo en condiciones bien
ruines.

En los corrillos de la oficina solía susurrarse,
entre risas equívocas, la última y todavía reciente
e importante conquista de María Eulalia. Y cada
vez que ésto acontecía, renovábase en las fisonomías
neutras de los empleados un interés, cuya pravedad
parecía reptar y circular por las arterias frías de
aquellos eternos insatisfechos, esclavos de la sordidez económica del medio que los fuerza a una abstención perjudicial; pero incapaces, por otra parte, de forjar para sí una vida abierta de propia iniciativa.

Al saltar la imagen equívoca de la joven empleada, evocada por los labios chismosos de cualquie
ra de aquellos seres, cuya irredimible medianía gustaba de salpimentar su propia desabridez, hurtando a la vida ajena atisbos de su intimidad, todos, a una; apretujábanse en torno del que hablaba con un leve rumor de faldas agitadas y de pies tardos que se arrastran.

La imagen de María Eulalia surgía, entonces,
desdibujada por la precaria elocuencia del que hablaba, pero intensamente sugerente a medida que
iba coprando perfiles íntimos y diferentes en los
cerebros de aquellos entes sociales que se atrevían,
en aquel instante y haciendo a un lado su desproporción ridícula, a ser hombres con la imaginación.

El rumor opaco de la voz evocadora parecía propagar, en las conciencias, un formidable incendio,
convulsionando los hondos y tímidos instintos, por
lo general, estrangulados a causa del prejuicio y del
medio. Y era entonces cuando, en el paroxismo de
las pasiones descritas, se desnudaban los rostros.
En la faz de la más jovencita de las empleadas afloraba, en tales momentos, un deseo insensato que,
al deslumbrarla, la obligaba a bajar los ojos, mientras le subía a la frente, en confesión implícita, una ola roja de placer que la hacía pensar, por primera vez, con descaro: ¡También yo, por qué no!

Luego Tere, la mujercita exigua, solía dejar oir
una risita desentonada y ríspida, cuya informe lujuria la envolvía toda entera en una niebla turbia.

También entonces se entremezclavan los breves
chillidos encandalizados de las maduras mujeres
honradas que no habían podido dejar de serlo, y cuya exageración y aspavientos eran demasiado gruesos para ser verdaderos. Sus poco agraciados rostros se inclinaban hacia delante con una gran vergüenza; pero, al mismo tiempo, con una ansiedad enorme de captar detalles. ¡Bajo las escuálidas caritas embadurnadas de afeites, se trasparecía un loco
y trágico deseo!

Era llegado el momento en el que las bocas de
los hombres jadeaban un poco, los dientes se clavaban en los labios convulsos en un inútil y postrer recurso de propio dominio, los puños se crispaban febrilmente, mientras las miradas se abrían paso, sin decoro, a través de sus compañeras de oficina. Y es que la carne hablaba ahora con voz más fuerte que todas las normas sociales, trabajosamente elaboradas en siglos de vivir en común.

Después de ésto, el grupo se dispersaba, pues ya
no importaba mucho saber la táctica que la hábil
María Eulalia había seguido para conquistar al improvisado y joven político, venido de la provincia
con una enorme inexperiencia, pero también con un
inmenso y sincero ímpetu generoso de favorecer a
las masas y redimirlas de su paupérrima condición.
Impulso que, en la capital, rápidamente se había visto desvirtuado al calor de la vida fácil y del mucho dinero que empezó a llenar sus faltriqueras al ocupar altos puestos gubernativos; por lo que no tardó mucho en convertirse en un nuevo contaminado de
la fatal fiebre del oro y los negocios turbios, engrosando las apretadas filas de los traidores a la causa social; todo lo cual implicó una indiscutible y
vergonzosa claudicación. Quizá el mal se hallara radicado en su propia habilidad para encumbrarse por
obra y gracia de circunstancias, y no de verdadera
inteligencia; probablemente también, porque su
sentido social fuese ligero deslumbramiento, obra
más de las hambres sufridas y de haber sentido en
las espaldas el látigo de la injusticia social; y sobre todo ésto, asimismo, la inconsistencia del espíritu tornadizo de los mediocres que estorba más al avance de la humanidad en sus conquistas sociales que todo el peso aplastante del egoísmo humano. Pero
todas estas consideraciones importaban poco a los
empleados, puesto que ellos sabían que si el alto
funcionario, amante de María Eulalia, había sufrido, en el pasado, la herida del dolor social, hoy se
veía cicatrizada, eficientemente, con los crecidos
emolumentos que percibía en su doble calidad de
representante popular y de importante funcionario
de la administración.

Por otra parte, la elegante María Eulalia, cuyo
paso ondulante y actitud insolente solía hacer brillar de envidia la insubstancialidad de sus compañeras, por privilegio de amante cobraba tres magníficos sueldos: en diversas Secretarías; razón por la que solo podía conceder unas horas de su preciosa asistencia a aquella oficina, de la cual era precisamente Jefe el mencionado funcionario. Horas en las que se ocupaba exclusivamente de la dificilísima y constructiva tarea de contestar las llamadas telefónicas. Sin embargo, la indignidad de aquella situación que hubiese sublevado a cualquiera que no fuese empleado burocrático, se encargaba de cubrirla la cobardía de todos los que, conocedores de la influencia definitiva de la muchacha, se apresuraban, en su presencia, a rendirle honores de reina burócrata.

El espectáculo deprimemte de aquella oficina
obraba en el espíritu de Andréïda de manera desoladora y, por ello, cuando por las tardes iba a refugiarse en el regazo acogedor y sano de Nelly, sentía una especie de desquite purificador en acompañar a la dulce amiga en sus actividades socializantes y feministas. Esto no obstaba, para que en su presencia, se burlara alegremente del afán apostólico de Nelly.

¡Qué diferencia tan grande entre aquellas mujeres de oficina, apresadas en la red turbia de todos los prejuicios, influídas de no se sabía qué absurdo complejo de superioridad, de inteligencia y clase, tan infundado el uno como ridículo el otro! Mujeres amorales las unas, insubstanciales las más y que, desde la altura ficticia de una situación de apariencias, solían lanzar una olímpica mirada despectiva a la diligente obrera, a la mujer que verdaderamente conoce el significado sublime y constructor del trabajo…

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo VI

La providencial intrusión del ingeniero Alonso en las vidas de Andréïda y Nelly, no solamente había favorecido a la primera con un empleo, sino que también, para la segunda, había gestionado un puesto aceptable como profesora de un centro cultural nocturno, sostenido por el partido político del que
era afiliado y en el cual la inteligente Nelly había
hallado el campo propicio que necesitaba para desarrollar su ideal.

Era a este centro al que gustaba Andréïda de acompañar a Nelly y según expresión muy suya, solía decir, que tales visitas le servían a manera de un baño reconfortable y purificador; por lo que su frecuente asistencia a tales reuniones, en las que se
daban cita un buen número de humildes obreras, hacía pensar a Nelly en la probabilidad de que un día, su amada amiga, se convirtiese a su fé social.

No escapaba a Andréïda la ilusa pretensión de Nelly y reía, íntimamente segura, de sentirse inconquistable, Mientras tanto, aquéllo le servía de nuevo espectáculo, cuya belleza percibía, con avidez, su exquisita sensibilidad. Contemplaba a sus pies, con los brazos abiertos, la parte sana de la humanidad. Y era en verdad algo vivo y palpitante, que incitaba a acariciar, aquellas dulces caritas fatigadas, aquellas manecitas temblorosas, maltratadas y enrojecidas que se ocultaban, con vergüenza, bajo el rebozo o el chalecito negro, con un soberbio orgullo que no pedía nada; pero que un día estaba destinado a levantarse serenamente y a exigir lo que, por siglos, se le ha negado. ¡Y qué decir de los ojos de pupilas negrísimas que absorbían, como agua de vida, Jas enseñanzas de Nelly…! Insensiblemente Andréïda comparaba estas mujercitas-verdad, sanas de cuerpo, y de cerebros vírgenes y un gran espiritu generoso de sacrificarse en aras del conglomerado social, con las otras muñecas ambiguas de oficina, exaltadas y frías, despilfarradoras y calculadoras a un tiempo, con una sed y un hastío de vivir impregnados de una amargura mortal; egoístas, pero también incapaces de afirmarse en la vida y reclamar sus derechos con valentía; eternas cobardes aferradas al pasado y, en pocas palabras, seres insuficientes, mutilados por una falsa cuanto superficial y deleznable cultura.

Andréïda, por el solo placer de encenderse en las pupilas negrísimas la gran rebeldía, pidió una noche a Nelly le permitiera dirigir la palabra a las obreras.

Nelly accedió al momento y sin reservas, aunque íntimamente se sintió estremecer con un vago
temor. Nunca sabía ella, a ciencia cierta, hacia dónde llevaban a Andréïda, sus extrañas reacciones psíquicas.

Esa misma noche tomaron las dos amigas el pesado y sucio camión en el que se apretujaban, como
de costumbre, tres veces más pasajeros del número
que, en realidad, podía albergar el reducido espacio maloliente del vehículo.

Bien pronto, la hidalguía mexicana que el hombre del norte no ha podido todavía sepultar bajo su nefasta influencia utilitarista, brindó a las dos jóvenes la dureza, apetecible en aquella repugnante promiscuidad, de una banca estrecha de maderamen
grasiento. Y el pesado carromato arrancó con un violento bamboleo. El exceso de peso obligaba al conductor a llevar constantemente la máquina en ruidosa velocidad segunda y cada parada obligatoria,
empujaba a los viajeros unos sobre otros, con un
agitar de brazos y manos que se aferraban, con desesperación, lo mismo a la esquina de una banca que
al hombro de una dama. Sin embargo, todo ésto ocurría en silencio, sin chillidos femeninos ni protestas viriles. El metropolitano es un ser resignado que la soporta todo con una gran paciencia ovejuna.

La insolencia luminosa de la ciudad advierte a
Andréïda y a Nelly que han llegado al centro y que
deben cambiar de vehículo.

Después de breve espera en una esquina, en la
que se ven obligadas a soportar el empuje de unu
avalancha humana que fluye y se detiene a capricho, para charlar y que no guarda la más ligera y
lógica regla de avanzar, Nelly y Andréïda tienen
que sortear varias filas de coches, antes de poder
tomar el nuevo vehículo. Como en el primero, en este nuevo camión, se hacina un número aplastante de
pasajeros y, por razones de situación social, se hace en él más acre aún, el peculiar olor a humanidad. Por las mismas razones, la indumentaria de los viajeros ha variado en pobreza; pero idéntica resignación estoica se observa en los semblantes. Las cabezas destocadas y libres de las mujeres proclaman ya una esclavitud abandonada, la del rebozo, y otra por tomar, la del sombrero.

A medida que se deslíe la impresión brillante de
la gran Ciudad, el camión penetra por calles aún
más ruidosas e impregnadas de un inconfundible y
fuerte olor a sebo derretido y maloliente.

En una esquina poco iluminada descienden las
dos jóvenes. Nelly se aferra al brazo de Andréïda y
las dos atraviesan varias calles empedradas y oscuras. Se encuentran en los barrios tortuosos, lóbregos y miserables de la colonia de la Bolsa.

Los ojos de Andréïda captan una visión turbia y
lívida de faldas alborozadas, chillidos agudos, atisbos de vidas que arden y sangran en la densidad repugnante y estúpida del bajo vicio. Los sucios zaguanes, de las grandes viviendas, despiden bocanadas heladas y pestilentes y el increíble hacinamiento humano salpica de andrajos los patios negros, cruzados de cordeles, en cuyo suelo se encharca el agua jabonosa. Andréïda siente la miseria, ligeramente entrevista, como un dolor físico que le oprimiese el pecho y, la asfixia del medio, se le sube a la garganta, como una protesta silenciosa y reconcentrada que la obliga a apretar los dientes con rabia.

Al choque brusco con la realidad miserable, los
conceptos que a saltos y a brincos deambularan por
su cerebro cuando su gran curiosidad la llevara a
absorver las doctrinas de moda, cobran para ella
perfiles nuevos, de una brillantez que la enceguece.
En aquel instante deja de ser, para Andréïda, mera
literatura, la protesta airada y, Casi general, de los contemporáneos.

Sus ojos tienen todavía en la retina el brillo centelleante de las colonias residenciales de la ciudad y de su gran centro comercial ostentoso. Aún su olfato recuerda el perfume de los jardines de los potentados con sus suavísimas alfombras de pasto esmeralda. Y no puede menos que reflexionar en que parece que el brillo de los de arriba necesitase siempre de la oscuridad de los de abajo para acrecentar su luminosidad, y que la magnificencia de los palacios suele alimentarse de la miseria de las zahurdas para afirmar su suntuosidad. ¡Siempre la gran desigualdad humana, como base corroída y corrupta de la grandeza de las ciudades, y piensa Andréïda que México, el de los palacios, no ha escapado a la regla.

De pronto, ofende el olfato de las dos jóvenes
mujeres, el insoportable hedor a pulque que escapa
de una inmunda piquera, por lo que apresuran violentamente el paso.

Dos cuadras más allá divisan, por fin, el macizo
y gris edificio iluminado, el que por unas horas
arranca de su miseria y abre nuevos horizontes a
los eternos parias.

Un grave murmullo de colmenar humano recibe
a Nelly y a Andréïda, Y extraña cosa, aquel edificio redentor, enclavado precisamente en la miseria,
la hediondez y el vicio, parecía, con la sola influencia de su amplitud y de su luz, borrar de las caras la expresión torva y, de las pupilas, la desesperación que la extrema pobreza clava, de ordinario, en ellas.

Por otra parte, la gente que acudía al llamado
cultural nocturno, notábase, a simple vista, que se
esmeraba en vestir su ropita más limpia. Aunque,
eso sí, no podía esperarse una gran disciplina de
aquellos grupos; pero su alegría ruidosa, desbordante, franca, hablaba al espíritu de una incipiente liberación.

Nelly y Andréïda penetraron a un amplio salón
en el que ya esperaban a la primera, un centenar
de muchachas.

La salutación cordial con que fueron recibidas
reanimó, con una suavísima onda de calor, los ateridos corazones de las dos amigas, enfriados a su
roce con la miseria en las callejas oscuras.

Con frase sencilla Nelly comunicó al crecido grupo que, esa noche, en lugar de la acostumbrada clase, su amiga Andréïda a quien ellas conocían ya de
vista, iba a dirigirles la palabra; pero no se atrevió a añadir sobre qué tópico hablaria la amiga, ya que ella misma lo desconocía.

En la gran sala, de paredes lisas, se levantó, al
instante, un murmullo hosco de populacho inconforme. Andréïda percibió claramente tres o cuatro pares de ojos fanáticos que la examinaban de arriba abajo con manifiesta hostilidad, pronta a revelarse en un acto brutal. El rumor crecía por momentos, ascendiendo a golpes y precipitándose tumultuosamente, con un estruendo pavoroso de río en creciente que desborda sus aguas turbias. Y, por un segundo, pudo apreciar Andréïda lo que es el furor de la turba.

Andréïda retrocedió dos pasos con una fría cólera en los bellísimos ojos. Nelly, con los puños convulsos, dió dos golpes violentos en la mesa escritorio, Y se hizo un silencio preñado de amenazas.

Andréïda, con su habitual movimiento orgulloso,
echó la cabeza hacia atrás, haciendo resplandecer
su fúlgida cabellera leonina, La amplia frente se serenó de pronto y la repentina cólera de sus ojos se apagó gradualmente.

Fríamente comprendía la hostilidad de aquellas
mujeres rudas, las que comulgando, de manera perfecta, con la suave humildad de Nelly, rechazaban
en ella, desde su exquisita indumentaria hasta su
calidad retinada de supercivilizada que no podían
menos que percibir en abstracto.

Con una impasibilidad absoluta que maravilló
a las mujeres, haciendo retroceder en sus muestras
de inconformidad a las más hostiles, Andréïda lanzó una dominante mirada circular, al parecer indiferente. El silencioso efecto logrado en torno suyo por su fría actitud, enardeció su espíritu selecto, y la conciencia absoluta de su dominio sobre su auditorio, un segundo antes amotinado, la hizo sentirse única.

Percibía la vibración, a su aldededor, de un hálito eléctrico de espectación. La sangre le latía en las
sienes dolorosamente, y sus pálidas manos tenían la
frialdad del hielo. Pero, al mismo tiempo, su lucidez era completa e irradiaba de ella la magnificencia y aplomo de quien se siente, al contacto de la muchedumbre, conductor de masas.

Por un segundo, se embriagó de potencia y por
primera vez, también, paladeó con fuerza el dominio ejercido sobre los semejantes y del cual había
tenido ya vaga noción al contacto con sus compañeras de colegio.

Erguida, firme, muy pálida, a pesar de sentir
sobre su cuerpo los tentáculos de la hostilidad, ordenó con voz firme:

¡Sentarse!

En las mujeres se produjo un movimiento oscilatorio. Sin saber por qué, obedecieron dócilmente, mientras las más cercanas al escritorio retrocedían hasta tomar asiento en las primeras bancas.

Al observar Andréïda el repentino y dócil movimiento de retroceso casi sintió odio de constatar la inconsciencia servil del grupo, el que, por otra
parte, solo obedecía a esa ley natural que obliga a
las masas a replegarse o a seguir ciegamente a una sola y grande voluntad. Los ojos de la extraña joven brillaron con un gran deseo de fustigar al rebaño, de sacudirlo e inocularle, de una vez por todas, la gran rebeldía, desvastadora de hierros esclavizantes, Deseó golpear a aquel minúsculo pedazo
de humanidad con fiereza tanta, que llegara a brotar la sangre de su carne humillada.

Con una sonrisa tranquila que no revalaba la
tempestuosa ola que la había agitado unos segundos
antes, avanzó Andréïda, con firmeza, dentro del radio de luz que iluminaba el templete magisterial.
Un rápido además circular de su pálida mano afirmó el silencio en su derredor. Por cierto, an silencio tan completo que jamás la dulce Nelly había logrado, con su amistosa actitud, para hacerse escuchar de aquel indisciplinado grupo.

Inmóviles, adosadas unas contra otras en las
bancas duras, todas miraban febrilmente a la recién
venida. A Andréïda le parecía un monstruo oscuro,
de cien cabezas brillantes, replegado en su hostilidad. Al frente destacábase ella, con su esbelta estatura recortada en el círculo de luz, circundada por una belleza etérea y la superioridad suprema que le confería su propia gran fuerza de voluntad. Su hipersensibilidad exquisita le reveló, de súbito, que había ganado a las jóvenes mujeres, tornando gradualmente su primera hostilidad en creciente admiración colectiva que solo a ella correspondía afirmar, y en ésto también aquel grupo obedecía a esa necesidad de adoración ineludible que sienten las masas deseosas de fundir becerros idolátricos con el oro de la verdad. Y Andréïda olvidó la tremenda lucha silenciosa y su mirada se retrotrajo hacia dentro en búsqueda convulsa de conceptos e imágenes. Se vió a su cuerpo contraerse en un terrible esfuerzo de concentración y, por fin, la admirable musicalidad de su voz se elevó llenando el gran salón de paredes lisas.

La tenue frialdad que impregnara las primeras
palabras de Andréïda fué fundiéndose a medida que
avanzaba el rítmico período verbal y las manos que
en un principio se unieran, una contra otra, como
en un intento de desmenuzamiento doloroso, se libertaron ampliando el ademán para lanzar hacia el
auditorio el ímpetu desbordante del pensamiento a
tiempo que sugerían, con perfección silenciosa, la
vasta amplificación de la imagen.
 
El sentimiento singular que, en aquellos momentos, dominaba a Andréïda fué precisándose con más exacta expresión al aumentar, paulatinamente, la firmeza del verbo acuñando la idea. Y fué para ella, su propia fluidez verbal, la revelación súbita y portentosa del ideal abstracto que, por tanto tiempo, atenaceara su espíritu en un tormento renovado a diario.

Como en una iluminación imprevista vislumbró
un ángulo desconocido, velado a su conciencia hasta aquel instante, de su propia obra grandiosa por hacer. Y la hermosa visión se prolongó más allá del
velo impreciso, salpicado de ojos que ardían, como
la obscuridad vaga de un cielo de invierno, ayuno
de luna, pero constelado de estrellas…

En la exaltación magnífica de su propio sueño
de dominación, entrevió aquella vida completa y extrapotente que había deseado siempre para sí y, fascinada por la vehemencia y ardor de su propio deseo, quiso hacer partícipe y proyectar sobre su auditorio la visión de un vivir vasto y libre, dichoso,
sobre aquella estrecha vida animal, ciega de pensamiento, de que hoy se daba cuenta al contacto con
las humildes, y que le dolía en la propia carne, encendiendo la hoguera de su rebeldía que su verbo estaba destinado a propagar en la conciencia virgen
de las miserables.

Dió principio a su obra de agitación distendiendo, ante los ojos ingénuos de sus oyentes como una
hábil Scherezada milianuchesca, los muelles tapices sobre los que se regodean los poderosos, para después describir, con violentos y brillantes colores, toda la magnificencia y belleza de la vida, y de la cual ellas, las proletarias, son inconscientes partículas edificadoras y molestas huéspedes, rechazadas despectivamente a un lado, a la hora preciosa del
festín.
 
La armoniosa fluidez verbal de Andréïda acariciaba aquellos oídos desconocedores de lo bello, descubriéndoles mundos desconocidos de dicha, bordando con luz nueva todas las exquiciteces no imaginadas y llevando a aquellos cerebros, de lentus reacciones, violentamente hacia su gran objeto, o sea, encender la llama inextinguible de la ambición en
aquellas almas opacas, para, poco después, revelarles su uso como acicate magnítico de superación humana.

Y, llegado aquí la brillante disertación de la
joven, fué arribado el momento en el que la frescura gallarda de las palabras de Andréïda perdió toda su exquisita fluidez. La melodía armoniosa se tornó, de pronto, bronco torrente entrecortado. La suntuosidad del verbo adquirió la brevedad cortante del ataque. Y atrayendo hacia sí aquel pedazo informe de humanidad miserable, su palabra rompió la angustia servil de su condición de paria, dándole a beber, a grandes sorbos, la suprema embriaguez de la gran rebeldía. Y fué una sola llama rebelde el auditorio humilde. Andréïda percibía, con una extraña lucidez de observación exterior, las bocas semiabiertas, los rostros febriles en los que se retrataba un estupor pueril; las negras cabelleras crespas, como agitadas por un viento de tempestad, y sonrió levemente, segura de su triunfo.

De manera material sintió entre sus manos las
voluntades amorfas de todas las mujeres aquellas,
v la asaltó el deseo insensato de hacer con ellas un
haz triunfal y rojo para agitarlo amenazadoramente sobre la gran ciudad despreocupada, voluble y lujosa.

La tensión sobrehumana de su espíritu fué cediendo, su voz volvió a adquirir la suavidad primera. Las aguas tormentosas fueron lentamente volviendo al cause de un remanso brillante y transparente sobre el que rielaba un supremo júbilo de luz… Pero el fuego del verbo quedó sobre él, con un calor fosforescente y concentrado, listo a lamer el cielo con llamas intermitentes.

Se hizo el silencio…

Bajo los párpados azulados y suaves de Andréïda se abrió paso un brillo cruel de bestialidad satisfecha de dominio, que contrastó profundamente con la mirada amplia, de supremo conoc1m1ento humano, de hacía unos instantes.

Fué un silencio breve. El silencio de una porción
de la humanidad que no la han dejado saber cómo
externar su emoción. Y una amplia vibración informe y palpitante se elevá, poco después, de aquellas
bocas doloridas, repercutiendo libremente en las
paredes lisas, No fué el aplauso convencional de las
aglomeraciones humanas privilegiadas, fué la explosión libre de los primitivos, fué la expresión de
seres que se encuentran, por primera vez, a sí mismos y que contemplan, también por primera vez,
en las palmas callosas de sus manos, las piedras
preciosas y brillantes de un tesoro inadvertido; la
repentina revelación tangible de estados humanos
superiores, puestos a su alcance por un solo movimiento rebelde.

Andréïda sonreía levemente, diabólicamente,
mientras acariciaba sus oídos la manifestación ruidosa del milagro recién obrado por ella. Elevada,
de nuevo, por la aspiral ascendente y sonora del
triunfo alcanzado, analizaba, con severidad, el fenómeno allí realizado, y comprendía que respondió,
en esencia, al ritmo impalpable, a la traba perfecta
y armónica de su verbo, más que al significado hondo de las palabras.

La comprensión momentánea y accidental entre
ella y sus oyentes se había establecido y realizado
por la virtud sugestiva y sugeridora del sonido, razón por la que había legado tan perfectamente a lo
más hondo de las almas. No era posible, en otra forma, que la verdadera potencialidad del núcleo central de sus ideas y de su elocuencia, hubiesen sido
absorbidos, de manera tan completa, por las mentes novicias. Por lo que, no cabía duda, que la breve comunión absoluta de ella con las jóvenes humildes había tenido su extraordinario fundamento portentoso en la comunicación material insólita que, como una corriente eléctrica, había partido de Andréïda hacia aquellos cerebros vírgenes y desconocedores de la esencia substancial de las frases. El sonido de su voz había vibrado carnalmente, bien con una incitación acariciadora o también, fustigadora de impresiones que, repercutiendo en la misma Andréïda, había generado resonancias infinitas y trascendentales.

Por última vez, Andréïda, paladeó con fruición
el espectáculo de aquellos seres sobre los que ella
habíase proyectado toda entera, durante un espacio
de tiempo. El ardor turbulento de aquel mar de ojos
temblando en la sombra, como un cielo de invierno
salpicado de estrellas, la llenó de un íntimo gozo. El
agitar insano de los brazos oscuros le dolía a ella
en la propia coyuntura ardorosa de sus miembros
delgados. El tabletear de los blanquísimos dientes
primitivos en las mandíbulas rígidas, la colmaba de
un vago y alegre terror inconsciente. El alarido entrecortado de las bocas entreabiertas destilando la
baba del estupor y del asombro la estremeció con
el ruror fanático de los reveladores y de los profetas. La sangre hirviente que hinchaba las venas de
las sienes mojándolas con un sudor febricitante que
pegaba a las frentes abombadas los mechones oscuros y crespos, le latía a ella en las propias arterias.
Y sobre su exaltación, y sobre su carne y su hielo y
su ardor la palabra desvastadora, la palabra sacra,
la palabra creadora: ¡REBELIÓN! Y la rebelión
era la que agitaba el bronce profundo de aquella carne de esclavas, en una suprema vibración reivincadora. Y la punzó el pensar no poder arrancar el
mismo aullido a toda la patria sobajada. Le doliá
el no poseer, en aquellos instantes, la ubicuidad divina para provocar en la pasiva espina dorsal, de
los etermamente inclinados, el mismo estremecimiento rebelde y redentor.

Una sola onda enorme envolvió su extraño espíritu, resumiendo, en uno, el vasto pensamiento y todas las angustias de su delirio superior. Y sobre la vorágine informe de los deseos despertados con violencia, de pronto ella restó en el vórtice, con una suprema serenidad del que ha hallado la forma y la expresa a los hombres con la plasticidad refulgente de las cosas forjadas bajo el justiciero sol y afirmadas, perennemente, en la superación del estado humano.

Andréïda recibió en sus ojos puros, devuelta y
magnificada por la turba su propia exaltación. Y
sintió que el triunfo le era necesario como la respiración. Cerró los párpados y la embriaguez de lo
consumado con sublimidad, fué lentamente dejando
caer en sus venas la laxitud dulcísima de la emoción
agotada.

La obra tantas veces imaginada en el pensamiento con perfiles gigantescos había sido allí realizada en pequeño, sobre un centenar de cabezas femeninas, sobre unas bocas entreabiertas que ahora sabían ya del murmullo rebelde, de resonancias trascendentales, destinado a ir acrecentándose en incontenible avalancha demoledora que, muy pronto, estremecería a la tierra con un tremendo descoyuntar de sistemas y un derrumbamiento estrepitoso de cimientos corroídos y falsamente afirmados sobre la desigualdad humana. La obra se había precisad: en Andréïda al contacto de la miseria, por primera vez palpada en toda su horrenda realidad. Su espíritu transportado, con violencia, a la nueva órbita del mundo por hacer, obligó al juvenil rostro a tornarse hermético y a encerrar dentro de sí, en inviolable refugio, el secreto de la nueva transformación y todas las transformaciones pasadas que se encierran en la sublime palabra: rebeldía, rebeldía, rebeldía, y cuyos círculos concéntricos, una vez lanzada al agua, van dispersándose, ampliándose infinitamente, adquiriendo las proporciones magníficas de la universalidad. Todo, por la rara virtud del verbo, capaz de abrigar el germen de un mundo nuevo.

Andréïda abrió sus ojos mortales. Una depresión inmensa se infiltró en su sér, mientras desfilaban ruidosamente las mujeres humildes que no tardaron en ser tragadas por la bocaza oscura de la calle tortuosa y reintegradas, por ello, al giro común de la vida. Las palabras de Andréïda eran ya vibraciones fugitivas y sin retorno, escapadas del espacio reducido, encerrado entre las cuatro paredes lisas. Su exaltación febril restaba a sus pies, a manera de un blanco gusano, aún fosforescente, pero cuya proximidad le helaba el pensamiento. Y sobre el agua febril de la vida, tan hondamente agitada por ella, cayó la niebla glauca y fría de la noche.

Una ola repentina de tristeza enfrió el corazón
de Andréïda y con un movimiento instintivo de sus
ojos se contempló las manos pálidas, imaginando
que estaban maculadas con las cenizas grises de la
hoguera fugaz, encendida por ella. Sus ojos buscaron los de Nelly y tuvo la sensación física de estar
pensando exactamente lo mismo que su amiga.
¿Contra qué barreras infranqueables y ásperas iría
a estrellarse aquel terrible deseo de vivir, tan hábilmente inculcado por ella en aquellas almas opacas?
¿Cómo reaccionaría la amoría voluntad de las humildes para crear la nueva vida, en caso de que la influencia de su verbo se alargara suficientemente? ¿Qué nuevas energías había ella despertado?
¿Qué fenómenos sociales, qué delirios nuevos empeñarían a las miserables en la nueva y compleja tarea de amarrarse a los hombros las alas de la victoria? ¿Había hecho bien? ¿Había hecho mal? ¿Había solo atendido al oculto deseo de plasmar, por una vez y sobre una arcilla maleable y dócil, su sueño de dominación? ¿O quizá también había animado su embriaguez momentánea el gran delirio de su exaltación? No, no, mil veces no. Si se había inclinado, un momento, sobre aquellas ávidas bocas
oscuras, fué por un límpido movimeinto lumínico
de compartir su ideal, de dar a beber la copa de su
sangre y el albo pan de sus íntimas convicciones.
Generosidad, vanidad, actitud dominadora, prurito de afirmación de cualidades y despliegue de potencias frente a sí misma, unido todo… quizás sí. Desvanecida ahora la nube de su exaltación, veía ante sí, condensarse una revelación de perfiles nuevos y desconocidos.

Substrayéndose Andréïda a la curiosidad imortuna de que comenzaba a ser objeto por parte del resto de los alumnos adultos de aquel gran plantel, se refugió en la sombra del patio en espera de Nelly, Allí, en la oscuridad ocultadora espió la emoción de los rostros que, con rara lucidez remembrativa, fué reconociendo en los diversos y prietos grupos que iniciaban la desbandada hacia sus miserables lares. Y la asaitó una duda dolorosa. ¿Perduraría la vibración de sus palabras? ¿No habría sido todo un intento descabellado e inútil?

Tras las humildes iba el dolor eterno y el eterno
amor. Ella les había enseñado a tender los brazos
hacia los bienes fugitivos, a implorar la alegría de
la vida, a exigir, en un llamamiento mútiple y formidable, todo aquello de que habían, hasta hoy, sido privadas tan injustamente. ¡Ah, el deseo implacable de todo lo que no se posee y por primera vez
se contempla! ¡Ah, la brillantez distendida de lo
exquisito frente a las pardas vidas de las humildes! ¡Y el dolor trasmudado en eficaz actividad estimulante, y la vileza de las lágrimas derramadas
en pasivos rostros resignados, tornadas felizmente
en alarido bronco, y la inclinación servil de las espaldas humilladas, transformada en arrogante verticalidad rebelde! ¡Ah, las sacras palabras, las devastadoras palabras, las divinas palabras creadoras: ambición, rebeldía, rebeldía, ambición…

La cabeza juvenil de Andréïda se doblaba bajo el peso agobiador del pensamiento y quedaba exánime, como si el palor de la luna se hubiese trasladado a su hermosa faz. Y huyendo del rumor de las voces que la hieren no sabe dónde ni por qué, echa andar deseosa de una soledad imposible. En su cerebro flota un mar de confusión y de gérmenes.

A poco, es alcanzada por Nelly y la mano larga y nerviosa de la amiga sujeta con fuerza la suavidad de su brazo.

La angustiosa interrogación que ha sofocado
Nelly todo el tiempo en el que su amiga hablara, la
domina, de pronto, y se le vuelve ardiente pregunta:

—¿Por qué, por qué aquéllo…?

Andréïda se estremece bajo el látigo de la concisa interrogación inevadible. Y la pregunta adquiere la difícil concavidad que exige siempre un eco: ¿Por qué, sí, por qué aquéllo…?

Pero hecha ya palabras la comprometedora inquisición, sucede a la propia duda una gran serenidad, y Nelly, pudo percibir, antes de que hablasen los labios queridos y sabios que ellos eran dueños de la pureza y verdad.

Andréïda comienza a hablar con manifiesta dificultad. No parece si no que hiciese mucho tiempo que no usara el precioso don o también que, la fluidez anterior hubiese agotado, por completo, la fuente viva de su privilegiado verbo. Las palabras saltan ahora cortadas, sin querer encajar en la frase y remisas, del todo, a la expresión justa del pensamiento que las anima; pero, poco a poco, el poder sublime de la ilación va haciendo orden en aquel caos.

Y dijo Andréïda:

—¡Nelly! Y si no hubiese sido más que por arrojar de aquellos espíritus, dulcemente tristes, la angustia callada de la humillación que no sabe del amplio y fértil cauce de la rebeldía? ¿Y si hubiera sido por infiltrar en las almas pasivas el desprecio por el lamento inútil y estéril? Y si hubiera sido por sembrar el deseo… por sacudir la inercia… por erguir la voluntad atrofiada? ¿Por darle voz a una desesperación que se devora en silencio, por sacudir un insomnio que muere en la espera, por enseñar a exigir a un humano lo que le debe otro humano, por dotar de luz unos ojos que ruedan angustiosamente en las órbitas apagadas? Dime, Nely, ¿qué?

El silencio de Nelly la llenó de alegría, de una
loca e impensada alegría que la justificaba toda entera. Había hecho algo, sí, lo comprendía ahora luminosamente. Era, en realidad, un don lo que ella
había dado al enseñar a aquellas bocas entreabiertas y oscuras a aullar, en lugar de proferir la vileza
del contenido sollozo callado y las lágrimas vanas
que no llevan a ningún fin palpable. Era un don el
que ella había hecho al enseñar a los puños humildes a alzarse en una suprema amenaza a lo alto…

Una suave risa agitó su garganta y pensó que
era todo un principio de doctrina lo que ella había
enunciado, obedeciendo a un extraño deseo, brotado naturalmente de su alma selecta y gloriosa, en
medio de una extraordinaria sublimación desconocida.

Después de ésto, el regreso de las dos jóvenes se
hizo en silencio.

\* \* \* {.espacio-arriba1 .centrado}

En la suave penumbra de la alcoba y entre la
dulce caricia de las sábanas blancas, Nelly, la mesurada Nelly pensó que hacían falta seres como Andréïda. Es decir, agitadores sublimes, destinados a
marcar con violencias de tempestad, de ciclón, el
avance y la superación humana, No de otra manera la madre naturaleza se sirve para purificar su
atmósfera y derramar los dones de sus aguas fructificantes. {.espacio-arrriba1}

¿Qué valía su humilde obra, la mesurada obra de Nelly, la constructiva obra lenta de su inteligencia, sin un sacudimiento formidable como el de esta noche, generado por Andréïda? ¿Quiénes si no los como ella, podían sacudir eficazmente la aplastante inercia de ese gran _champ de muettes_, de resignados mudos, de sufridos pasivos, incapaces de reclamar lo suyo con energía?

Sin embargo, el espíritu reflexivo de Nelly abominaba la inquietud nociva recién establecida por Andréïda, aunque su innato raciocinio lógico le apuntase que, aún fracasando aquel formidable impulso dado a la energía enmohecida de aquellas mujeres, sería de todas formas digno de profundo respeto. Y porque era preciso obtener la íntegra emancipación de los humildes, eran lícitos todos los medios, aún los de la agitación destructiva, a pesar de que ello costase a Nelly un oscuro dolor, puesto que comprendía que ya no contemplaría más la expresión de dolor abotagado y vago en los ablícuos ojos ingénuos, y que el descontento minaría, bien pronto, la artificial resignación de domesticadas pasivas.

No se le escapaba tampoco que el mérito y la
justificación de la mujer estaba precisamente entre
aquellos puños humillados a quienes Andréïda había enseñado a levantarse orgullosos. Y comprendía también que la joven no solo se había limitado a
obligarlas a exigir una modificación radical de sus
Actuales y miserables condiciones económicas, sino
que las había incitado a rescatar su albedrío, a afirmar su clase, a descifrar el trágico enigma que estremece a nuestro siglo en un mar de vacilaciones,
dudas y zozobras.

Recordaba cómo la voz casi profética de Andréïda se había elevado para afirmar que la más grandiosa lucha, de todos los tiempos, y que excedería en violencia y fanatismo a todas las conflagraciones de los siglos pasados, sería la crisis de la emancipación femenina. Y con incontrolable emoción volvía a la memoria de Nelly otro período de la trascendental charla de Andréïda con las obreras, de la mujer amiga que se había propuesto esquivar el amor y que con tan gran dulzura había expresado la doble misión femenina de la maternidad y de la acción social. Y se dijo, con una sonrisa incrédula en los labios, que quizá hablase en aquellos términos dulcísimos Andréïda, porque desease hacer de aquella parte de la humanidad femenina las famosas máquinas de reproducción de que, en ocasión del último día de estancia en el colegio, había hablado a las colegialas. Su clarividencia, sin tristeza ni añoranza, sobre el divino dilema había hallado en su verbo las palabras justas para sobreponer la primera a la segunda y, a pesar de aguijonear a las mujeres sobre sus inalienables derechos hacia la nivelación de la remuneración de sus esfuerzos en la lucha económica, tuvo frases elocuentes y comprensivas para fijar, de manera prominente e inalienable a otros intereses, la misión sobre todas las cosas primera, o sea, la maternidad. Y fué así como el privilegio radiante de la mujer lo impuso Andréïda, a despecho de su íntima ideología elaborada exclusivamente en favor de su ultra-potente y extraña personalidad.

Para Nelly fué tarea superior a sus fuerzas el
deslindar lo útil y lo peligroso de la expansión verbal de Andréïda ante las obreras.

Pensó Nelly que la necesidad de formas nuevas
y el libre desenvolvimiento del grupo, de la comunidad y de la clase le era necesario la bisexualidad para la perfección y justo equilibrio de la organización social. Y era indudable que seres como Andréïda, poseídos de un delirio de grandeza y con
fuerza suficiente para cambiar el curso de los siglos, eran los únicos capacitados para intentar la
magna hazaña, la cual no podría realizarse más que
por el enardecimiento de las masas bajo el verbo
creador de los guías agitadores, Y, todos a una,
nbreros, campesinos y mujeres, impelidos en el mismo sentido, fincarían los verdaderos cimientos de
la anhelada transformación social.

Como todos los reflexivos, Nelly, por largo tiempo había aspirado a un régimen más perfecto para la humanidad; pero se sentía incapaz de mover, con
eficacia, un dedo en favor de esas condiciones radicalmente nuevas, que exigen, a su vez, nuevos espíritus, hombres nuevos, mujeres nuevas, capaces
de sentir nuevas concepciones morales. El solo pensamiento de una actitud violenta la hacía sonrojar
de vergiuenza. Pero reconocía que Andréïda, en esta
noche, había obrado, a manera de ardiente sol, que
entreabre las yemas hinchadas, cubiertas de pelusilla grisácea del futuro fruto, provocando, a influjo de su ardor, la maravillosa floración que no tardaría en contagiar al universo entero con su actividad creadora.

Y ya en la niebla del sueño y de la inconsciencia,
Nelly recordó vagamente una frase de su favorita
Ellen Key: “Los sentimientos son la savia ascendente que cambia el espectáculo de la vida humana”.

Andréïda llevaba, en el alma inflamada, el sagrado fuego de los innovadores, de los descubridores de mundos nuevos, de los que sabían la difícil
ciencia de acicatear la indiferencia humana. Se lanzaba a la lucha por un inveterado anhelo de libertad, y su impresionabilidad sufría con todas las
miserias que contemplaba en torno suyo y que no hacían más que afirmarla en su orgulloso puesto de
rebelde eterna. La certidumbre y conciencia de su
altura le prestaban arrestos suficientes, en sus momentos de exaltación, para hacer temblar un mundo.

Nelly, por el contrario, solía acudir al llamamiento de la justicia, con la miel de la piedad en los labios. Su obra constructiva se perdía en la
mediocridad del esfuerzo aislado y en vano se esforzaba por intentar la perfección intelectual de sus
discípulas, poco despiertas. Su bella alma desinteresada y su corazón leal estaban destinados a consumirse, y su pobre obra a ser desestimada hasta
por los más cercanos a ella. Sus intentos, truncos
de organización, eran siempre paralizados por la
idiosincracia racial. Esto no obstaba para que trabajase fieramente en la realización de su sueño de
grandeza colectiva y de justicia social.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo VII

— ¿Por qué no me saluda, Andréïda?

—Perdóneme, no lo había visto…

—No me había visto después de haber elevado los ojos por encima de mi cabeza?…

—Es que…

—Sea usted franca, comienza a molestarla mi presencia…

Era tal la congoja infantil de aquellos ojos viriles que Andréïda no pudo menos que reír alegremente.

—Es usted un incorregible niño grande, ingeniero Alonso —afirmó con soltura.

—Un niño molesto a quien se aparta fácilmente
con el caramelo agridulce de una risa.

—Nada de eso; voy a serle franca, como usted
me pide… Sepa, mi señor ingeniero, que, el no
haberlo visto antes, porque no lo vi ¡hay que dejar
firmemente sentado este hecho!, se debió a un acto
puro de discreción… —al terminar de hablar, la
risa retozaba en los suaves labios de Andréïda…

—¿De discreción…?

—De discreción, si ¿no repara, mi excelente
amigo, en que son ya un tanto impertinentes las
historietas fantásticas que a nuestra costa urden
mis simpáticas compañeritas de oficina?

—¿Es un reproche…? No volveré ya más…

—¿A encontrarme a la salida de la oficina? No,
se lo ruego…

—Obedeceré…

La lisura planchada de la cabellera intensamente negra descubierta bajo el sol de la tarde, desarmó la firmeza de Andréïda que se apresuró a añadir, consciente de que se mostraba injusta con la
honda admiración que a ella se rendía de manera
tan incondicional:

—Bien, mi dilecto amigo, esa admirable docilidad merece un premio… Le consagro la tarde…

—Es usted sencillamente encantadora…

Una amplia sonrisa distendió los labios carnales del ingeniero Alonso y Andréïda lo envolvió en una mirada maliciosa, al margen completamente de aquella alegría gozosa, de aquella emoción viril.

—¿Iremos…? —interrogó ella con un dejo displicente.

—¡Adonde usted ordene…!

—¡Al bosque…!

La proposición había brotado rápidamente, impensadamente, de los labios de Andréïda. Con seguridad que había pesado en su elección el bello
sol de aquella tarde de junio, unido también al
deseo subconsciente de respirar el aire fresco y libre de Chapultepec, el acre olor acariciante de sus
pinos, la sombra profunda y misteriosa de los ahuehuetes milenarios, el perfume vivificante de la tierra mojada y negra, potencial y magnífica, extremadamente rica de vida y de gérmenes.

El brillante y ligero coupé del joven ingeniero
se encargó de lievar raudamente a los dos jóvenes,
internándose, a poco, por las umbrías avenidas,
en círculo, del bosque imponderable, de belleza única. Al dar la vuelta, a espaldas del castillo y frente al recinto que encierra el baño del rey poeta, Andréïda propuso detener el coche y bajar.

La frescura del bosque, en la tarde calurosa,
excitaba en aquellos momentos a la joven, empujándola a obrar, por primera vez, sin analizar sus sensaciones, como una verdadera chiquilla a quien
las letras jamás hubiesen inoculado la pravedad de
sus virus dulcísimos, de reacciones incognoscibles.

Por su parte, el joven ingeniero guardaba un
silencio significativo que más parecía un terrible
esfuerzo de dominio sobre sus emociones que la actitud de un agradable compañero en una tarde de
asueto.

Sin una palabra que turbara el silencio de la
tarde calurosa, los dos jóvenes se internaron por
esa parte endiablada de la derecha, tan rebelde y
feraz que hace pensar en el hecho increíble de que
desconozca el tufo repugnante de la gasolina civilizadora y domesticada.

Los ahuehuetes de inverosímiles troncos gruesos y retorcidos ondulaban la sombra espesa de sus ramas sobre el prodigio azul del traje transparente
de Andréïda que trasparecía la carne mórbida de
la espalda y los brazos. Los helechos, al borde de
un arroyuelo aferraban la madeja enmarañada de
sus raíces a las piedras redondas y ásperas y erguían sus tallos rizados y coquetos que hablaban muy suave e incitadoramente, de la vasta lujuria de los trópicos y de las sombras profundas de sus
bosques de origen. No se oía más ruido que el temblor de las hojas y el crujido sordo de las ramas
secas al ser tronchadas bajo los pies inquietos, ligeros y ágiles de Andréïda y su compañero.

En una vereda estrecha y retorcida el hombro
de Andréïda, más provocativo bajo el misterio sutil
y terriblemente tentador de la tela transparente que
si hubiese ido desnudo, rozó el áspero casimir. Bajo
las pestañas de oro hubo un deslizamiento de luz
turbulenta que estremeció la médula del hombre.

Adelantóse ella y las comisuras de su boca se
ensombrecieron con dos pliegues malignos, a tiempo que pensaba el que no estaría mal, en aquella
tarde deliciosa, una experiencia emotiva. De sobra
sabía ella que para su fuerte voluntad, el joven ingeniero era un instrumento de lo más sensible y
dócil; pues estaba ya acostumbrada a pulsar sus
sensaciones en el ya largo tiempo transcurrido de su conocimiento con él, y durante el cual ella había probado su fuerza de seducción, y también e invariablemente había aprendido a rechazarlo con la más
fría de las sonrisas, helando, en aquella forma, toda
incipiencia de hacer íntimas unas relaciones que ella no deseaba pasasen del límite de una amistad útil y agradable.

Sin embargo, aquella tarde una especie de locura la empujaba a mostrarse provocativa y a desplegar el sutilísimo encanto de su natural seducción, por lo que, en su fuero íntimo, aceptó cuanto había de verdad en aquella certera frase de Gourmont, al afirmar que el desconocimiento del hombre no implica necesariamente el desconocimiento del placer. En aquel instante y para Andréïda, la palabra abstracta tomaba un sentido preciso que la hacía mirar al ingeniero con curiosidad. El brillo de los ojos oscuros la asustó un poco, afectaban la fiebre de un insomnio turbulento; la mirada del hombre iba más allá de la mujer, penetrándola y hablándola de un porvenir de caricias y de ternuras.

Para aliviar la tensión que se había impuesto
entre los dos, ella lo invitó a que mirase el portento
de dos ahuehuetes entrelazados en una cópula infinita y cuyos troncos en sus bases eran ya, de tan
unidos, un solo árbol. El retorcimiento angustioso
de uno de ellos, grabado indeleblemente y en profundos surcos que hendían su corteza venerable, hablaba de una absorción cruel y poderosa del segundo, indudablemente más fuerte, lo que plasmaba,
en el primero, un desmayo carnal de vencimiento
interno que sobrecogía el alma. A las ramas altas
enredaba el “paxtle” el fingimiento de una floración de nieve augusta, cuyas hebras plateadas eran
otras tantas estalactitas brillantes que completaban, a maravilla, la ficción; mientras que, por la
parte baja, la hiedra abrazaba al tronco su suavidad hipócrita de cortesana y parásito.

Andréïda apretó el paso. Había en el ambiente
un olor a miel y a tierra mojada que quebraba su
mirada, desconcertando su admirable ritmo interior. Era demasiado fino el aire que acariciaba su
cuerpo, eran demasiado azules las campánulas entre la hierba verde del suelo, era demasiado profundo el esmeralda de los árboles y también la turbaban, en exceso, los tenues rayos de sol que se filtraban aquí y allá, como pensamientos cuya luminosidad excesiva encegueciese y dañase. La emborrachaba el silencio y los olores primitivos. La tierra negra revelaba a su cuerpo juvenil, por primera vez y un poco tardíamente, el gran secreto eterno de la vida.

Y echó a correr con una risa loca que se abría
paso por entre la gloriosa bóveda de ramas entrelazadas en un abrazo infinito. Fué una risa ligera, sonora, infantil, con un temblor de gracia eterna que fué a llenar los huecos oscuros de los troncos milenarios, reverdeciéndolos con un verdor de adolescencia. El lindo traje, a la vez casto y atrevido
en su transparencia, volaba en torno suyo y se le
aplastaba al cuerpo revelándola entera, a tiempo que
la suave tela se cubría con un polvillo de sol tamizado que temblaba y desaparecía con un huir de follajes, en interminable juego de luz y de sombra.

Desconcertado de pronto, el joven ingeniero, no
supo qué actitud tomar. Se encontraba a sí mismo ridículo y torpe. Contemplóse los brazos musculosos y las potentes y ágiles piernas, atados irremediablemente por el fardo poderoso de los lazos absurdos de una civilización que impone su esclavitud
sobre el instinto y domestica la inmoralidad “natural” de la naturaleza, deformando su crueldad y
su belleza. Sin embargo, su alma medrosa y cobarde se encargó de calmarlo, convenciéndolo de que se trataba de una chiquillada imposible de seguir; por lo que dejó correr sola a Andréïda.

Instantes después, la encontró sofocada y ardorosa junto al lago que en la tarde se rizaba con
un escalofrío de oro. A lo lejos, los remos indolentes de una esbelta y pequeña barquichuela ahondaban un surco vacuo e infecundo que se iba cerrando, simbólicamente vacío, sobre las aguas quietas. En la orilla, un grupo albo de patos se disputaban, con una algazara hueca y desagradable de _meeting_ político, una presa cualquiera.

Andréïda observó al joven ingeniero que se llegaba a ella con una dulzura turbada en el rostro viril y una gran indecisión que desmadejaba, un poco, su andar cuidadoso de hombre que gusta de vestir bien.

A despecho de ella misma, la mirada se le vuelve provocativa y los labios le tiemblan, y se ofrecen
a la caricia no conocida y que sospecha extrañamente dulce. ¡En verdad que, en la tarde aquella, no parecía sino que Andréïda probara de dejar ser
Andréïda!

Ahora él se encuentra en pie, junto a ella. La
maravillosa cabellera de Andréïda se torna rubia
al sol, con el rubio de oro y ceniza de los trigales
ardientes. La frente muy erguida conserva toda la
pureza infantil y casta de la virgen; pero ¡cuánta
curiosidad maligna hay en los ojos profundos de
pensadora! La mujer no es una promesa en la
tarde tibia, es una curiosidad que acecha…

Molesta por haber entregado un poco de sí misma en aquella carrera alocada, se excusa con frase incoherente, aludiendo a la frescura del bosque que
invita a penetrarla,' al aire vivificante que empuja
la sangre de las piernas, al olor de la tierra y los
árboles que place respirar a pulmón abierto.

El joven ingeniero se cohibe de nuevo, y de nuevo también se encuentra ridículo y necio frente a
ella. La superioridad indiscutible de Andréïda le
ata la lengua con un miedo invencible de ser rechazado y le cierra, como siempre, la boca, aunque la
terrible pregunta le quema los labios.

Meses largos de esperanzas renovadas cada día,
de aplastamientos mortales e indecisiones angustiosas, no han curado su amor nutrido con un obsesionante deseo carnal insatisfecho que desazona sus
horas de insomnio y que, por el contrario, han ido
acrecentándolo en tal forma, que las copas merce.
rias le dan náuseas y se siente incapaz de ir a beber
en ellas. En los meses largos ha pasado por las
alternativas más terribles de ilusión y desesperanza. Ha sentido las emociones de él en las manos de
ella, como un frágil y brillante insecto que distraía
los ojos de la bella. Y si permitió él mismo alargar
su tormento, había sido únicamente por la convicción firme que le daba la extraña conducta de Andréïda, cuya fría indiferencia imperturbable era
idéntica para todos los hombres, limitándose a
aceptar sus emociones en calidad de homenaje debido a su belleza, sin dispensar, en modo alguno, predilecciones de ninguna especie y sí ejerciendo su
encanto de la manera más natural, como si en el
ejercicio de él radicara el porqué de su hermosa
existencia.

Mientras esto discurría el joven ingeniero, Andréïda, íntimamente satisfecha, vigilaba la profunda turbación del hombre, encontrándolo hermoso en
Su seria palidez y en la expresión reconcentrada del
rostro que le prestaba un aire espiritual que pocas
veces había podido ella sorprender en aquel ser regido, preeminentemente, por sus sentidos. Y en
aquella plácida tarde de junio, ella acabó por encontrarlo hermoso y de su gusto. Bien es verdad que,
no dejaba de reconocer que su hermosura era la
hermosura desaliñada del atleta, sin ese encanto,
sin ese no sé qué que hemos dado en llamar unas
veces distinción y otras raza…

Andréïda observa que en la precipitación de la
caminata el joven ingeniero se ha arrancado del
cuello la corbata, desabrochando el primer botón
de la camisa de seda suave y que deja adivinar la
amplitud y musculatura del pecho, descubriendo la
garganta atezada y poderosa. El deseo entreabre la
boca grande de dientes sanos, la barba oscura sombrea el cutis recién rasurado y las grandes aletas
de la nariz se agitan revelando una clase de potencia animal y de alegre sensualidad indomable. El
cuerpo esbelto, vigoroso, flexible se acerca peligrosamente a Andréïda. Ella experimenta, por primera vez, un desfallecimiento que le dobla las piernas y para salvarse de ese algo que siente se aproxima, se deja caer al pie del árbol gigante, bajo el cual se encuentran, y cuyo hueco pedestal de raíces enormes y toscas, retorcidas y potentes semejan víboras escuras que hincaran las cabezas en la tierra negra para nutrir el ramaje poderoso, sustentándolo con un verdor sólido, con un verdor de siglos que aquieta el alma.

El fuerte olor húmedo que sube del fondo del
lago y del opaco musgo esponjoso que se ve, en trechos, esmaltado de minúsculas florecillas azules y
púrpuras, tiene la virtud extraña de marear a Andréïda que acaba por cubrirse el rostro con un ademán desarmado e infantil. Al lado de ella toma
asiento, dulcemente, el joven ingeniero e intenta
un movimiento que su fatal indecisión se encarga
de hacer frustrar antes de nacer. solo sus ojos se
sacian de belleza, recorriendo el gracioso cuerpo recogido en una actitud aniñada, cuya honda coquetería ni la propia Andréïda, en aquellos momentos,
valoriza. El hombre desea apasionadamente descubrir el suave rostro tras los dedos de encendidas uñas; enmarañar aquellos cabellos maravillosos y
violentos; conocer el sabor de aquellos labios jugosos; palpar la dulzura de su carne trasparecida tentadoramente tras la gasa azul del delicado vestido
que, de manera tan admirable, sabe moldear aquel
cuerpo de diosa joven. El ardor del abrazo soñado,
en tan largos meses de espera, se le sube a la cabeza… Va a pronunciar la palabra reveladora, esa
palabra reservada tanto tiempo en su garganta y
que sus labios se resisten siempre a pronunciar,
amordazados por una absurda timidez y por las actitudes desconcertantes de la bien amada.

Aquella nueva actitud desconocida de ella, tan
juvenil, tan femenina y provocativa, lo enloquece.
Jamás, en tanto tiempo de conocerla y soñar con
ella le ha parecido tan dulce e infantil, tan exquisitamente mujer… Va a hablarla…

Bruscamente ella se ha descubierto el rostro.
Torna a mirarlo con esa mirada suya tan desconcertante y maliciosa que parece acechar algo, aguardar no se sabe qué. Sin embargo, esta vez su aplomo se ve turbado por la mirada fija, insistente,
atrevida como una caricia y elocuente como la más
elocuente de las frases. Una de esas miradas de
hombre que van más allá de la mujer…

Con un movimiento rápido, que no necesita ayuda, se yergue ella. Ha desaparecido de la bella garganta la agitación de hace poco. Unos minutos la han bastado para recobrar toda su frescura, toda su vibrante fortaleza.

Una sonrisa invita al joven:

—¿Corremos…?—el picaresco mohín de los labios tiene la virtud de incitar al ingeniero e irritarlo.

Ella ha unido la acción a la palabra y torna a
huir sin esfuerzo y casi sin ruido.

De nuevo él la deja alejarse, seguro de alcanzarla y de su fuerza. Ahora ella se desliza por la suave pendiente de una estrecha vereda. Rauda cruza la alameda de los poetas, la de los filósofos, la de los reyes. Da un rodeo a la maravillosa fuentecilla del Quijote y de Sancho, en eterno diálogo chispeante y filosófico, hecho prodigiosa talavera de Puebla.

Otra vez la pareja se ha hundido en el bosque.
Los dos parecen haber olvidado ahora, del todo,
los necios prejuicios, los necios respetos, los vacuos
pudores. Son brava y alegremente dos animales jóvenes, bellos y robustos que no se asustan de su
deseo y cuya sangre les empurpura los labios y les
agita el pulso de las sienes y de las manos. El hombre desea, como nunca, se le entregue, con alegría y
sin remilgos, el maravilloso y refinado sér, un poco
misterioso, desconcertantemente inteligente; pero
siempre sincero, que corre frente a él y que le ocultan, de vez en cuando, los troncos verticales de los pinos.

A distancia se ha detenido ella, parece desafiarlo con la mirada y poco después torna él a oír
la misma risa nerviosa que tan bien sabe fustigar
sus nervios de hombre. Y de nuevo, echa ella a correr, desapareciendo al instante.

El hombre, seguro de que al final es suya la
victoria, acorta el paso, prolongando sabiamente el
placer para gozar unos instantes la plenitud de su fuerza masculina.

A lo lejos de la nave verdeante, vuelve a aparecer la encantadora. El sol criba el traje azul con grandes medallones dorados en un juego diabólico de luz y de sombra… Y la mujer huye, huye… Ligera, imponderable, única. Embriagándose de aire, de sol, de frescura que le resbala, la acaricia y la envuelve el prodigioso cuerpo. Su rostro obstinado de virgen loca flota en un baño místico de luz. La firme garganta tiene un acelerado temblor rítmico que la obliga a entreabrir la boca con delicia. Por los maravillosos ojos pasa la sombra del espanto atávico al hombre, mientras el obscuro instinto la empuja a erguir aún más la cabecita, con el orgullo insensato de sentirse perseguida. Los dos milagros agudos de su pecho hienden el aire, victoriosos. El calor de la sangre en las suaves mejillas, las tiñe de gloria. El juego preciso de los elásticos músculos de sus piernas delgadas, como las de un adolescente, la produce un placer físico incomparable… Y huye… huye… embriagada por esta caza de amor.

Un movimiento rápido de la bella cabecita hacia su espalda, la indica que ha dejado al hombre atrás, muy atrás… ¡Lo ha vencido! Una amplia carcajada estridente, inverecunda, llega a los oídos del hombre quien, sorprendido primero y encantado después, abandona el acelerado paso complaciente para ensañar sus piernas en una rápida carrera. Y corre tras la hermosa presa a través del milenario bosque magnífico. Al darse cuenta ella y sentirlo
venir, redobla el esfuerzo de sus músculos.

El imperio primitivo del bosque ha embriagado
sus venas. Los ojos del hombre se velan tras un
velo rojo. Del fondo de su alma le sube a la garganta un gemido ronco y sordo que los dientes se encargan de trozar entre los labios apretados. ¡Debió ser el aullido gozoso de la edad de piedra! Las venas del cuello son ahora dos cuerdas tensas que se hinchan en un esfuerzo final y, bajo la apretada enramada, el hombre se lanza vertiginosamente.

La mujer pierde terreno, la distancia se acorta
cada vez más entre los dos y vencida, casi extenuada está a punto de caer en el preciso momento en que una garra rapaz cae sobre su hombro suave desgarrando el lindo traje.

Hay algo salvaje y mortal en el brazo que la
sujeta fuertemente por la cintura y que la obliga a
caer entre la maleza con las piernas tronchadas.
Las columnas corintias y regulares de los pinos gigantes bañan al sol sus agudas copas orgullosas de
centinelas impávidos o de cirios simbólicos que velaran al tiempo, traspasando el espacio. A su frente, sobre el tapete verde ultramar del césped, juegan unos chicuelos ensayando las puntas de sus pies
en el viejo cuero de un balón desinflado. La blancura inmaculada de unas nubecillas se entretiene, en el cielo, forjando los más absurdos y fantásticos relieves.

Andréïda se abandona al brazo vigoroso del
hombre que le sujeta la breve cintura. Agotada, los
ojos abiertos en un asombro sin fin, los labios trémulos, goza unos segundos la extenuación y el extravío dulcísimo de la derrota ancestral, y tiembla
al lado del hombre, laxos los nervios, ardientes las
mejillas por el calor sagrado.

Bien pronto, el abrazo se estrecha brutalmente y
envuelve la ligereza preciosa del vestido. La hoca
de él resbala por el pelo maravilloso y va a aplastarse en la suavidad mórbida del hombro, mientras una mano brutal desgarra la blusa.

—La amo a usted —ha musitado la voz bronca del ingeniero.

Andréïda se desprende con violencia. La voz de
ella vibra de ira reconcentrada, acerada, monstruosamente fría y despectiva.

—Me ofende usted, déjeme…

El hombre retrocede, sus ojos se posan asustados sobre la gasa desgarrada del traje, y un obscuro
espanto le enfría las sienes al observar las señales
rojas que su brutalidad ha estampado en la pureza
de la carne blanca. Se juzga estúpido y bestial;
pero le faltan palabras para reconvenirse a sí mismo y pedir excusas. Sus labios no se mueven siquiera para pedir perdón.

Ella de pie, junto a él, tranquila, convertida de
mievo en la mujer metálica, en la Andréïda cuyos
nervios no saben de estremecimientos de eternidad,
lo condena fríamente, con una sorda cólera en los
ojos. Se da cuenta ella, de pronto, que sus emociones la han traicionado, que ha sido víctima de un
desfallecimiento momentáneo de sus sentidos y de
su curiosidad de virgen que desafía el deseo del
hombre; pero, también, sabe de cierto que su alma
no tomó parte en el peligroso juego.

El ingeniero, frente a ella, ha palidecido con
la palidez de un espectro, con el espanto de un hombre que ha contemplado en el semblante de la amada la más terrible de las reacciones y ha medido la
repugnancia invencible que una caricia suya ha producido.

Ella no siente la más mínima piedad ante la
espantosa palidez del hombre. En el fondo de su
alma de mujer se alza un rencor enorme contra
aquel goce brutal y egoísta que ha sorprendido en
sus ojos al aplastarla a ella; contra aquella premura inconsiderada y despojada de la actitud delicada
del amante inteligente que sabe halagar primero la
imaginación femenina. Jamás perdonaría ella al
ingeniero aquel alarde primitivo de su fuerza, jamás excusaría la dicha demasiado gozosa de aquellos ojos que se habían impuesto sobre ella con toda la fuerza trastornadora y salvaje de la naturaleza.

Y ahora, la nueva actitud contrita de él lo condenaba aún más a los ojos de Andréïda, a tiempo que lo encontraba también absurdo y necio en su repentino y tardío respeto de civilizado. Dueña de sí misma hace en voz alta su reflexión.

—Está usted terriblemente ridículo, amigo mío.

Un estremecimiento doloroso crispó la cara viril, ingenua y pálida, increíblemente pálida, con una
palidez verdosa que acentúa el reflejo verde de los
pinos. Sus ojos vuelven a clavarse, como hipnotizados en el traje desgarrado que deja asomar el rosa
suavísimo de la ropa interior. Y Andréïda, juzgando impertinente la mirada, hace un movimiento
nervioso que descubre el nacimiento blanquísimo
del pecho, suave como el capullo lumínico de una
flor misteriosa. El corazón del hombre palpita sordamente. La mujer lo mira de arriba a abajo, con
una mirada cruel que le cierra la puerta de toda
esperanza, y fríamente, sin la menor piedad para
aquella cara desvastada de hombre, dice:

— Nunca, lo oye usted, ¡nunca!

Apenas si la vibración de la palabra final se
ha extinguido en el aire cuando de nuevo la mirada
mecánica y segura reveló que Andréïda volvía a
ser tal, la mujer desconocedora de toda inquietud
pasional. Su boca, entonces, exteriorizó una satisfacción burlona y maligna de sér superior que contempla, divertido y sin asomo de piedad, la debilidad inverosímil de los mortales.

Y esta vez se aleja ella definitivamente, cubriendo con una mano la desgarradura del traje…

El la dejó ir, ¡qué podía hacer! Sus ventiséis
años se rebelaban ante lo inexplicable; su emoción
de hombre se había aplastado cruelmente contra un
hielo misterioso, contra el hielo monstruoso de un
extraño fenómeno, el fenómeno extraño de una mujer que ha dejado de ser mujer.

Experimenta la extraña sensación de haber
oprimido entre sus brazos el esqueleto frío de una
sombra hermosa, maravillosamente hermosa.

Su fisonomía se entenebrece visiblemente. Siente que la vida ha acabado para él. Falto de la presencia de ella la concibe únicamente como un páramo desierto, sin atractivo alguno, que no vale la pena de recorrer. Hasta antes de haber encontrado a la maravillosa mujer, le habían satisfecho todas las
mujeres más o menos bonitas, más o menos jóvenes, más o menos pulcras. Después de conocida Andréïda, después de haber encontrado a la mujer, consideraba inútil vivir para encontrar mujeres. En aquel minuto de abismo él supo que ningún hombre que conociera de la mágica dulzura de besar a Andréïda podría seguir viviendo privado de ella. Y se dijo, ¿cómo olvidar la cálida palidez de magnolia de su carne? ¿Cómo no volver a pensar en el misterio profundo de los bellísimos ojos? ¿De qué forma libertarse del imperio hechicero de aquella boca cruel, tan pronto enunciadora de los placeres más caros y enloquecedores, como tan pronto burlona y despiadada? ¿Y qué decir de las manos paganas que acariciaban el aire con ademanes gráciles y se antojaban propias para hundir la cara ardiente entre las palmas suaves y frescas? Y, finalmente, ¿cómo eludir la atracción y la fragancia inaudita de la carne marmórea de aquella milagrosa flor humana?

La terrible humillación de no haberla sabido
enamorar un poco agudizó su desesperación.

En un fulgor extraño de revelación interior valorizó, de pronto, el hondo significado que tenían las palabras anónimas y terribles escuchadas aqui
y allá y que describían a la joven como un extraordinario ser, cuyo terrible orgullo lo había orillado a alterar las leyes naturales para transformar
su propia persona en una ficción monstruosa que
escalofriaba el alma y cuyo capricho parecía radicar en afirmarse como espantosa excepción.

Oscurecía. La soledad, ahora hostil, del bosque
lo envolvía en una vaharada áspera de resina que
le raspaba la garganta. Un vaho caliente de locura
se iba apoderando de su cerebro. En lo más hondo
se le hacía insoportable recordar la mirada humillante de ella al desnudar su integridad de hombre,
exhibiéndolo en la picota del ridículo ante sus propios ojos. El corazón le palpitó con violencia. Después de todo, ¿qué había hecho de malo? Estrecnar
su cuerpo maravilloso, besar su hombro blanquísimo, de una pureza astral? Y, ¿por qué aquella fuga
loca y provocativa? ¿Qué razón había tenido para
irritar sus nervios de hombre y después arrojarle
á la cara su desprecio sublevante? ¿ Qué derecho se
había arrogado al escupirle al rostro la ridiículez
de su tardío, pero al fin respeto de civilizado?

Una tristeza infinita le anegó la garganta y deseó llorar como un chiquillo desamparado a quien se ha azotado injustamente.

Algo irremediable había cruzado su vida, nublándola para siempre. Algo que presentía jamás podría reaccionar sobre ello y que lo acecharía siempre en el deslizamiento de los días sin amor. Sus ojos recordaron, una vez más, el surco de carne palpitante que los había atado con una dulzura y un deseo de posesión infinitas. ¡Nunca, nunca más! A sus espaldas vibraron las palabras de negación eterna, de crueldad impiadosa.

Sus manos se elevaron para oprimir las sienes,
su cuerpo se estremeció con un temblor nervioso,
rebelde al azote de su virilidad humillada. Inclinó
la cabeza y sus labios resecos musitaron con una
desesperanza infinita: ¡Y esta fiebre de la sangre,
y este vértigo terrible del alma! ¡Mi pobre sueño,
ei pobre sueño de mi juventud! ¡Y mi deseo, el eterno deseo de mis nervios…!

Después… una gran depresión distendió sus
músculos, mientras un soplo diáfano y fresco, como
de brisa marina, desordenó sus cabellos negros y
lisos. Agazapado en la sombra escuchó, con indiferencia, los silbatos estridentes de los guardianes del bosque que invitaban perentoriamente a salir de él. Cerró los ojos reteniendo la poca claridad celeste que iba estrechando lentamente la infinita copa invertida del firmamento. Las nubes habían perdido su cándida blancura y como sueños mancillados, se hinchaban con hinchazones negras de tempestad nocturna. La algarabía de los pájaros amenguaba hasta oírse, cada vez más espaciados, los chillidos de protesta y los píos dulcísimos del anochecer.

El hombre pensaba, pensaba. Su exaltación
amorosa de la tarde era ya un puñado de cenizas entre sus manos fuertes. El deseo inmenso de su vida, el deseo insensato de renacimiento, de reproducción
habíase tornado ahora en un deseo de aniquilamiento, en un deseo de muerte, de paz y descanso, de infinito descanso… Todavía el perfume de la noche próxima y la tenue claridad celeste le trajo palpitante, vívido, el recuerdo de ella. Y otra vez también el divino estremecimiento lo invadió de nuevo y de cara a la tierra negra adoró a la mujer y la hizo suya, en el misterio magnífico del bosque en penumbra. Olvidó locamente los labios duros que supieron de la palabra nunca, de la palabra negadora y hostil y se limitó a beber en la frescura de la tierra pletórica, la frescura de la boquita roja y reidora de las primeras horas de la tarde. A sus ojos acudió la visión azul del traje transparente que el viento aplastaba un poco, desnudando el misterio de la carne mórbida de aquella mujer de veinticuatro años y cuyos sentidos ignoraban toda vibración pasional.

A ciegas, con la boca ardorosa pegada a la tierra, el hombre tembló de deseo, mientras sus dedos se quemaban en el fuego inconsciente que atenaceaba su cuerpo. Se adosó más estrechamente a la tierra y enloquecido por la oscuridad, su pensamiento dislocado efectuó una regresión enfermiza de aniquilamiento interior, revelándole la futura vida sin
cauce ni esperanzas de encontrarlo. Uno a uno desfilaron los días transcurridos desde el primer encuentro con la amada maravillosa. En el estado
anormal de sus nervios y de su imaginación exacerbada, adquirieron realce monstruoso los más ligeros detalles, Recordó conversaciones, comentarios sobre la frialdad inexplicable de Andréïda. Tuvo un significado nuevo la leyenda extraña que la ciudad iba
forjando alrededor de la joven. Contó una a una sus invitaciones rechazadas siempre, sus insinuaciones
delicadas aceptadas con complacencias, que de pronto,
lo llenaban de una loca esperanza, para después sumirlo en la desesperación de una mirada que se alzaba con una inocente incomprensión, atándole los
ímpetus. Recordó la indiferencia de ella para todos
los hombres y la pasión incontenible de éstos que
la seguía por doquier, como una cauda roja de conquista cruel. Las apariciones teatrales de Andréïda
en todos los sitios donde una gran multitud podía
tener asiento y la fiebre extraña que empurpuraba
sus mejillas, cuando desde un palco alto lanzaba
su mirada de dominadora. A decir verdad, esa era
la única pasión que tenía la virtud de teñir de placer su semblante ardiente. Pero sobre los hombres
y todas las cosas, se alzaba siempre su frialdad
sistematizada, el dominio absoluto de los seres y los
objetos, ejerciendo una absorción nefasta, una ambición de predominio sin límites, hasta anular la
personalidad de los humanos que a su alrededor tenían el privilegio o la desgracia de acercársele. El
mismo, reconocía ahora, no había sido más que un
instrumento a quien en la tarde cercana había pretendido pagar sus servicios con una hora de locura
que él llevó demasiado lejos.

De nuevo la sintió imposible, inaccesible, como
en el momento de decirle ¡nunca!, como en el momento de la humillación suprema de su virilidad, haciéndole beber, implacable, el minuto absurdo de su ridiculez.

Una risa amarga hizo temblar su garganta de
hombre y pensó con rabia en su calidad de ser irredimiblemente indeciso, absorbido, del todo, por la
poderosa personalidad de ella…

Como todos los débiles pasionales, arribó para
él el minuto de su rebeldía, la que falta de expansión por su ingénita impotencia, no podía menos
que dirigirse contra él mismo. La oscuridad daba
a su espanto de ser que se siente atado, para siempre, a un destino funesto, una desesperación febril. Con una mirada de locura, mirada de aquél que no quiere pensar mucho una súbita y drástica resolución, extrae el sobre de una vieja carta y sobre él traza nerviosamente la frase consabida, la sobada
frase reglamentaria del suicida que ha tiempo terminara con su dramatismo para tornarse cómica,
comicidad que obliga a sonreír al ingeniero y que
se vuelve mueca de honda amargura y piedad para
sí mismo. ¡La peor y la más dramática de las piedades! Por un momento ha sentido la tentación de
evadirla; pero un escrúpulo final detiene sus deseos, pensando que quizá se llegaría a saber que Andréïda había pasado las últimas horas de la tarde con él y su inmenso cariño lo impele a defenderla de una posible acusación futura. Lleva la mano a la cintura y con una resolución nerviosa de quien no quiere pensarlo mucho por temor a desistir, saca la Colt de cachas ostentosas sobre las cuales el artífice autóctono burilase en plata el escudo de su estado natal y apoya el frío cañón en la sien. Una sonrisa cruel para sí mismo, deforma su bella boca sensual de apasionado y como en un relámpago gozoso, imagina el temblor de los suaves párpados de ella sobre los ojos fríos, heladamente fríos, monstrusamente fríos que mañana seguramente contemplaría.

El bosque reprodujo a lo lejos la seca detonación. Los pájaros, sorprendidos en su reposo, agitaron :as alas en la oscuridad acogedora de las ramas y alzaron el vuelo. Una lechuza de suave pechuga blanca cambió de sitio y, no sabemos por qué, sus redondos ojos inmóviles reflejaron el fatal y escalofriante ¡Nunca más! que sabía tan bien graznar el cuervo negro de Poe…

</section>
<section epub:type="chapter" role="doc-chapter">

# Capíitulo VIII

A la mañana siguiente todos los diarios capitadinos daban cuenta, a grandes titulares, del suicidio
del joven ingeniero Alonso, haciendo, al mismo
tiempo, las más variadas suposiciones sin faltar
quien insinuase, como causa directa de la fatal determinación del joven, su amor por cierta damita
misteriosa y bellísima, cuyas excentricidades habían
sido ya objeto de la atención metropolitana. Y no
decían más.

Nelly fué la primera en enterarse del infausto
acontecimiento y, sin perder tiempo, pertetró en la
alcoba para comunicárselo a Andréïda, quien, por
ser domingo, alargaba perezosamente su permanencia en la cama leyendo la última novedad literaria.

La cara desencajada de Nelly y sus manos temblorosas que estrujaban nerviosamente el periódico sobresaltaron a Andréïda, la que, incorporándose,
se apresuró a preguntarle por la causa de su turbación.

—Lee —fué la respuesta lacónica, mientras los
ojos habitualmente amables de Nelly se habían tornado, como ningunos, acusadores.

A Andréïda le bastó leer el encabezado para que
su rostro se demudara con un palor de agonizante. Sin embargo, con un esfuerzo sobrehumano entreabrió los suaves párpados que la emoción había entrecerrado y apresuradamente leyó la extensa información. Un velo de lágrimas cubrió copiosamente el rostro, fluyendo de los ojos inmóviles. Sintió un dolor físico en lo alto de la frente y en las propias raíces de los cabellos. Las sienes le latían con un fragor vertiginoso de vida impetuosa. Un temblor nervioso la sacudía de los talones a la nuca, y el nudo de la garganta amenazaba con una suspensión momentánea de su respiración. Después, no pudiendo soportar el brillo ominoso de los ojos fraternos, se cubrió la pálida cara con las manos, en un ademán muy parecido al de la tarde anterior; pero que ahora obedecía a muy distinto motivo.

En los primeros instantes su cerebro se resistió
a creer en la realidad espantosa del trágico fin de
su amigo. No podía aceptar que aquel cuerpo joven
y flexible permaneciese inmóvil para siempre, y su
pensamiento daba vueltas y más vueltas sobre la
misma reflexión, mientras sus labios musitaban
mecánicamente: ¡ No puede ser, no puede ser! ¡Dime que no es verdad, Nelly! Y sentía en los nervios
las sensaciones más disímbolas, en una creciente lucha por anular a toda costa la verdad que ponía
en su cuerpo un escalofrío de terror. Ella había
aprendido a vencerlo todo en su, relativamente, breve existencia; había sabido reducir todo a su verdadero sentido efímero y temporal, conocía el valor transitorio de todo lo humano. Sin embargo, frente
a este hecho brutal e irremediable se sentía, por
primera vez, desarmada.

De pronto recordó con extraña claridad, la indignación que le había causado, todavía la noche anterior al acostarse, el contemplar en su blanco hombro la huella brutal de los cinco dedos viriles y el escalofrío de repugnancia invencible que volviera a
invadirla bajo el recuerdo quemante de los labios
sensuales y voraces. Se le representaron, de nuevo,
a la memoria las extensas reflexiones, el análisis
despiadado y frío, la disección de su propia alma,
hecha por ella, valientemente, en la noche pasada.
Dos nuevas y grandes lágrimas rodaron con dificultad por sus suaves mejillas empalidecidas y poco
acostumbradas a aquel riego ardiente. Y en aquel
momento terrible de desfallecimiento interior, el alma orgullosa de Andréïda se llegó a preguntar, espantada, si no habría amado su corazón, sin saberlo,
al joven ingeniero.

Con un deseo subconsciente de espiar su falta
irreparable, se complació en prolongar su agonía,
atormentándose a sí misma al recorrer, uno a uno,
todos los detalles de la tarde anterior, de final tan
trágico. Reprochóse duramente su conducta ligera
que nunca imaginara ella tuviere consecuencias tan
inmediatas y funestas, segando en flor la vida de
aquel que Andréïda había juzgado, de manera equívoca, como un niño grande, engolosinado de deseo e incapaz de adoptar en la vida una actitud seria, fuera de la compra de un automóvil o la elección de una corbata.

La culpa mayor de Andréïda, lo reconocía ella
ahora, había consistido en no haberlo tomado en
serio, en haber jugado con él un juego peligroso que
a un niño hubiese hecho llorar, pero que a un hombre le obligó a tomar la más drástica de las resoluciones. Ella, precisamente ella que se apreciaba de psicóloga, había errado fatalmente su juicio sobre el complejo anímico de aquel ser amable que había sabido solo pedir a la vida la felicidad de sus sentidos.

Extenuada, con una angustia mortal en el alma
que no podía deshacerse en lágrimas, se dirigió a
Nelly:

—Hermana, ¿no me creerás tú también culpable de esa horrible resolución? —Después, con los nervios descentrados y el hablar apremiado de quien, aun acusándose con sinceridad, intenta hallar frente a sí mismo la disculpa de su pecado, prosiguió: —Es preciso que me creas, Nelly. Ayer en la tarde fué la primera vez en la que se me puede tachar de haber jugado un poco con un cariño que juzgué, bien equivocadamente, como el pasajero y fugaz de un niño demasiado mimado por la vida. Fué un juego sin importancia, un juego pueril en en el que entró la influencia morbosa del ambiente y del paisaje. Si tú quieres, fué parte también una curiosidad malsana, un deseo insano de observar sus reacciones emocionales y al propio tiempo las mías. Pero nunca, óyelo bien, nunca creí que él llegase a tomarlo en esa… forma.

Un sollozo desgarrador interrumpió bruscamente sus frases entrecortadas.

—Nelly, Nelly, dime que me crees, dime que no soy culpable de ese horrible crimen.

La congoja de la amiga amada, enterneció, por
fin, a la joven profesora que no podía ver sufrir a
aquella niña sobre todas las cosas querida para ella,
aunque, en su fuero interno, la calificase muy severamente.

—Vamos, Andréïda, cálmate. Vas a acabar por
hacerme creer que sigues siendo una niña irresponsable, a despecho de tu suficiencia y de ese culto ególatra que te va acercando al narcisismo. Cálmate. Ahora, más que nunca, tenemos necesidad de toda nuestra serenidad. Habrás reparado que, encubiertamente, te aluden los diarios, por lo que no tardarán los periodistas en estar aquí y rodearte de una publicidad molesta que no quiero hacerte la ofensa
de creer que desees.

No habló más, fijó en Andréïda sus ojos inquietos y maternales y con gran ternura procedió a vestirla como si en realidad se hubiese tratado de una niña pequeña.

Una hora después Nelly había arreglado todo lo
necesario para emprender un corto viaje sin que
Andréïda, que seguía sentada al borde de la cama,
se hubiese dado cuenta de nada. Terminados todos
los arreglos se acercó a la cama.

—Andréïda, marchémonos.

Los grandes ojos de la joven se abrieron con estupefacción.

—¿A dónde…? No pretenderás que huya cobardemente, como una culpable.

—No pretendo nada —contestó con serenidad Nelly. —Lo que deseo es alejarte de muchas curiosidades malsanas que intentarán, a toda costa, abrir tu vida al público. He escrito dos líneas al director del diario en que trabajo, quien, por otra parte, ha aprendido a estimarme sinceramente, y creo que eso bastará para que, en unas cuantas semanas, le echen tierra al asunto.

Andréïda se resistió todavía. Le parecía que el
hecho de huír agrandaba su culpa moral, viniendo
a ser algo como una traición al hombre que había
sacrificado su vida tan locamente por un jugueteo
coqueto de ella. La gran angustia volvió a oprimirle
el pecho. Se dejó caer sobre la cama, sacudido con
violencia su bello cuerpo por los sollozos y presa para siempre del dolor de un remordimiento tenaz e
ineludible. Y débil, desfallecida, como si sus lágrimas las llorase sobre el cuerpo del ser amado, se
dejó guiar por Nelly.

Ya en el coche destinado a conducirlas, interrogó maquinalmente a la amiga:

—¿A dónde me llevas, Nelly??

—A Taxco, ¿no querías, hace poco, pasar un
mes allí en silencio y reposo absolutos?

Andréïda cerró los ojos, el muelleo del coche
amortiguó su desesperación.

Seguíale pareciendo de una injusticia atroz el
que aquel cuerpo joven que ella había sentido estremecerse de placer en la tarde anterior, hoy se
encontrase rígido, Lo imaginaba dolorosamente en
la noche oscura del Bosque,—pensó que las noches
del Bosque debían ser extrañamente oscuras, henchidas de rumores temerosos, y lo vió, clarísimo, con los ojos abiertos, muy abiertos, de cara a la luna. Su rostro expresivo debía tener un palor más intenso que aquel que le descendiese cuando ella le espetó la frase hiriente, mortal, insana, al calificar su emoción viril de ridícula. Su pensamiento atormentado se detuvo por el deseo morboso de conocer la última palabra que debieron pronunciar los labios yertos. Espantada del punto al que la condujesen sus trastornadas ideas, se tornó a preguntar si no lo habría amado acaso sin saberlo. Analizó el dolor de hoy comparándolo con aquel otro que la atosigara hacía tantos años al conocer, por carta de la
hermana, la muerte de su madre. Y se dijo que era
más acerbo el de hoy. En aquel entonces, la distancia, la imagen desvanecida de la madre, en tantos
años de ausencia, no había podido dar a su espíritu
la sensación de realidad que ahora, una mala y trágica fotografía del ingeniero Alonso le producía; es
más, recordaba ella que, allá en el colegio, habían
pasado muchos días antes de que se hundiera en su
alma la certidumbre de lo irremediable. Todavía,
mucho tiempo después, le pareció una noticia irreal,
una mala pesadilla de la cual despertaría un día con
un gran grito de gozo. El pasar del tiempo, poco a
poco, adentró la verdad; pero nunca llegó a tener,
para ella, la realidad brutal, restregada cruelmente
sobre sus ojos doloridos, de aquel retrato borroso en
la plana periodística y que se adivinaba ensangrentado.

Una hora después de rapidísimo viaje, Cuernavaca se mostró a la vista de las jóvenes.

Andréïda, aniquilada por un cúmulo de pensamientos dolorosos, pidió a Nelly el que el coche se detuviera, un poco, junto a un estanquillo para telefonear.

Desde el automóvil, Nelly oyó que Andréïda pedía comunicación de larga distancia con una florista de México y que le encargaba una fantástica corona mortuoria. Cuando regresó al coche, notó una
tranquilidad y una serenidad tan grandes en los ojos
de Andréïda que Nelly no pudo menos que estremecerse y pensar si la extraña joven creería poder pagar su responsabilidad, tan grave, en aquella forma
perfumada y banal. Pero bien pronto se convenció
de que no era así. Andréïda recuperaba, poco a poco, su fortaleza retrotrayéndose hacia dentro. Comprendía, de pronto, que el camino que se había impuesto a sí misma no podía verse afectado por una
pasión de hombre, por un acto irreflexivo, casi de
locura. Pasión que ella, desde un principio, había
rechazado en mil formas. No concebía cómo, el ingeniero Alonso, había podido interpretar la conducta de ella, en la tarde anterior, fuera de un coqueteo intrascendente. Reconocía, avergonzada, que su
coquetería había tenido todo el picaresco primitivismo que podría haber desplegado la más inocente mecanógrafa de un despacho o la costurerita humilde de cualquier taller, y se sentía humillada;
aunque sin dejar de reconocer, también, que ella no
había dejado de imprimirle ese inconsciente valor
que su maravilloso sér ponía en todo lo que de él
provenía. Reprochábase, eso sí, muy duramente la
brusquedad que manifestó en sus frases despiadadas, con el agravante de que ella había provocado, abiertamente, la conducta de él. Y la invadió otra
elase de remordimiento, todavía más punzante, el
de recordar que se había separado del amigo con
una frase hiriente, insufriblemente mordaz y despiadada, y también, el que no había mediado entre
ellos la fórmula habitual de despedida, cuya cordialidad hubiese endulzado, quizá, la sombría separación suprema, ¡Oh la fuerza inconmensurable, infinita de las palabras! Del corazón de Andréïda subía un deseo agudo de llorar por él, de testimoniarle, en alguna forma, su cariño fraternal, ese cariño con el que él no había querido contentarse. ¡Pobre, pobre amigo mío! musitó. Jamás me perdonaré, sé que jamás me perdonaré mi ligereza criminal. Rememoró la dulzura temerosa y turbada de la voz de
él cuando se dirigía a ella, el desesperado amor violento de las pupilas negras e ingénuas, y la gran felicidad que transfiguraba su viril rostro a la menor condescendencia de ella. Y se dijo que había sido bueno y afectuoso y que no había merecido que
la vida lo enfrentase cruelmente a ella, a ella que
no había podido hacer nada por él, porque sentía,
sin remedio, en la espalda, un inquietante rumor de
alas, y en las venas una especie de potencia maravillosa que la empujaba más allá de la posibilidad de un deseo de hombre.

Y ya en el resbaladizo plano de encontrar excusas a su proceder ante su propia conciencia, se dejó deslizar fácilmente por la pendiente lógica de la
deducción. No concebía, a pesar de todo, que su
coqueteo hubiese sido suficiente razón para tan gran
locura. Es más, se dijo a sí misma que, con seguridad, el ingeniero era un enfermo pasional, un demente que no necesitaba sino del minuto propicio
para revelarse.

Y su pena se disolvió en una especie de anonadamiento que la dejó insensible por varias horas.

\* \* \* {.espacio-arriba1 .centrado}

Por un acuerdo tácito, las dos amigas no pronunciaron una sola palabra en todo el largo camino. {.espacio-arrriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# Capitulo IX

Plácidamente transcurrían los días en el maravilloso Taxco. Mágico pueblecillo colonial que no parece si no que haya olvidado el paso del tiempo.

Andréïda deslizaba su vida entre las empinadas
callejuelas saturadas de belleza, quietud y romance
provincial. La hermosa joven había aprendido a
aborrecer los sábados y los domingos, días en los
que redoblábase el ir y venir irreverente de los “turistas” que empringaban la risueña claridad de Taxco con el tufo insoportable de la gasolina, y cuyos
carros producían un gran estruendo de rechinar de
muelles que se dislocan en las calles empinadas y se
lamentan, con ruidosas protestas, de la falta de lisura en las piedras bola del arcaico pavimento. En
esos días Andréïda se acostumbró a huír a la montaña, trepando como una cabra silvestre, sin dejar, por ello, de abominar, también, la carretera que mancillaba con su gran cinta cigzagueante y negra
el maravilloso paisaje; no perdonándole el grave
pecado de abrir, con moderna facilidad, la entrada
ensoñadora del pueblecillo único. Joya para arribar
a la cual ella, se decía siempre, impondría una preparación adecuada, un ayuno místico de cuarenta
días.

En una de sus frecuentes y solitarias correrías
le tocó tropezar con un espectáculo de un fuerte colorido dramático, que absorbió irresistiblemente su
mirada de observadora, operando en su pensamiento la más extraña de las reacciones.

Aconteció, que una tarde, en la que estaba ella
empeñada trabajosamente en escalar la más alta de
las montañas que circundan el pintoresco pueblecillo y en cuya cumbre se le antojaba a Andréïda podría abarcar, con la debida amplitud, el ensoñador
paisaje, dominando una extensión infinita de picos
montañosos; vió venir, hacia ella, un grupo reducido
de indígenas, el primero de los cuales lanzaba al
aire estruendosos cohetes, mientras el segundo, que
lo seguía un poco atrás, llevaba en la cabeza una delgada y pulida tablita sobre la cual descansaba un
pequeño y rígido bulto, envuelto en papel de china
blanco y circundado por un gran lazo azul, también del transparente papel.

En el primer momento pensó Andréïda que se
trataba de algún producto para vender, de algún obsequio para el señor cura del lugar, o bien alguna
ofrenda, en cirios, para el hermoso templo de Taxco; pero, bien pronto, se dió cuenta de su terrible
equivocación. Tras del hombre avanzaban, con lentitud, una veintena de indígenas ataviados con sus
mejores galas domingueras, las mujeres luciendo
hinchadas faldas almidonadas, de brillantes colores,
azules, rosa fuerte, solferino, y los hombres inmaculado calzón blanco y patío. Más atrás, un terceto de músicos atronaban el aire con las notas desafinadas de un violín de hechura casera, una guitarra destemplada y un harpa inconcebiblemente vieja y roída en su primitivo maderamen.

La última de las humildes mujeres que formaban el fúnebre cortejo casi atropelló a Andréïda,
quien había quedado abstraída, a un lado de la estrecha vereda, contemplando el inusitado espectáculo para su citadina conciencia. Los ojos de las dos mujeres se penetraron mutuamente. Los de la indígena lo hicieron, de manera maquinal, y tenían tal
expresión de sufrimiento interno y desesperado, así
como también una especie de estúpido anonadamiento, que hizo vacilar el alma de la joven, revelándole
un dolor nuevo, un dolor no sospechado por ella, el
dolor de los dolores, el dolor de la madre a quien le
ha sido arrebatado, impíamente, el hijo de su corazón.

En las ojeras marchitas y profundas de la mujer que la había rozado, sin reparar en ella, conoció
Andréïda, de pronto, el significado cruel de las noches eternas pasadas al lado del enfermito, espiando el efecto de una cucharada o de una infusión de mágicas yerbas que por pravo y oculto hechizo han
perdido, inexplicablemente, todo el milagro curativo, que se les atribuye, de su benéfica acción… En
aquel rostro de mujer, prematuramente envejecido
en unas horas de angustia infinita, para describir
la cual faltan adecuadas palabras al lenguaje humano, le fué dado apreciar a Andréïda toda la inutilidad de la sabiduría que es experiencia, toda la ineficacia de las plegarias que se elevan en desesperada y humilde imploración, renovada segundo a segundo y en la que va involucrado el ofrecimiento absurdo de inmolación de la propia vida a cambio de la pequeña e indefensa que debiera andar entero el camino de ser; toda la inconsistencia de la oración que emplaza, suplica, amenaza y ruega a esa abstracta entidad, sorda y muda que no entiende de concesiones piadosas, de justicia y de lógica humana, ni acepta esa clase de cambios y de sublime sacrificio. Sacrificio que encierra en sí, en esa espontánea voluntad suprema de dejar de ser, más divinidad que toda divinidad concebible.

En contraste lancinante con aquella desesperación materna, marchaban los acompañantes del duelo, indiferentes a aquel dolor y con un júbilo irrespetuoso de burda incomprensión, celebrando lo que
ellos acostumbran llamar la entrada de un nuevo
angelito al cielo, Hay que reconocer que, en el
fondo de ésto, cxiste siempre una especie de filosofía primitiva que defiende arrolladoramente y a toda costa la vida, y que no es otra cosa que un movimiento instintivo de retroceso y alejamiento, muy
semejante a aquel que sufren y ejercitan los animales al husmear tierra empapada en sangre.

En la actitud de los indígenas se afirmaba, una
vez más, ese egoista complejo humano de ignorar
voluntariamente la muerte, con unánime hostilidad testaruda e ineficaz que se resiste siempre a
aceptar este hecho como el más natural de los acontecimientos a que está sometida la vida en general.

La misma Andréïda sintió en aquellos momentos, al contemplar los hombros de la mujer que se inclinaban hacia la tierra, al observar aquellos pies tardos y humildes que se arrastraban en el más duro de los calvarios, el de ir a entregar a la tierra el
hijo de la entraña nuestra, una violenta rebeldía
contra la naturaleza que de tal manera implacable
helaba los más tiernos brotes, absorbía el renuevo
inocente, con un deleite culpable y una alegría inconmovible de monstruo apocalíptico, y que, en cambio, permitía el que discurrieran por su faz arrugada, la larga cadena risible de los caducos, de los inútiles, de los deformes, de los tarados irremediablemente.

Andréïda volvió mecánicamente sobre sus pasos
y, frente a la débil curiosidad epidérmica de los indígenas, fué a situarse tras del hombre de la ligera
carga fúnebre sobre la testa. Una estrecha abertura del papel transparente en el que estaba envuelto,
le permitió ver el dulce y rígido cuerpecito que apareció plano, demasiado plano, sobre la tabla rasa y,
en quien la muerte afirmaba rudamente el contorno,
destinado tan prematuramente a desvanecerse en el
tiempo, a borrarse definitivamente del mundo. La
pielecita restirada del rostro traslucía las líneas de
la calavera, encrespando los cabellitos deslucidos y
ásperos que horas antes debieron poseer toda la suavidad dulcísima del plumón. Uno de los delgados
párpados, por efecto de la miosina, se había resistido a cubrir por completo la pupila que brillaba en el fondo con un brillo mentido y estático de vida inmóvil, de contemplación inerte que escalofriaba el cuerpo de quien lo veía y que, en esta forma, destruía la imagen delicada del sueño que, en vano, intentaba ensayar su actitud; por lo que le fué dado a Andréïda apreciar que, entre los dos sueños tan semejantes entre sí, existía un abismo hostil de graves diferencias. La linda boquita, antes con seguridad deliciosamente jugosa, empezaba a delinear una sonrisa innoble que se adivinaba ya con una gran prisa en degenerar, y pensó la joven que no
tardaría mucho en descubrir las encías; aquellas pobrecitas encías que traslucían ahora los conmovedores guiones blancos minúsculos de los perlados dientecitos que no tuvieron tiempo suficiente para crecer y perforarlas y que hablaban de primavera próxima, de frustrado milagro no florecido.

El cuerpecito, bajo la ropita humilde, se adivinaba agotado, exhausto, consumido por la fiebre y los sudores, con el vientrecito aplastado bajo las costillas y la carnecita flácida que tuviera un tiempo
redondeces de manzana jugosa. Pero lo más conmovedor era, sin duda, el pobre pechito inerte, los
débiles pulmoncitos que debieron estremecerse, antes del último desgarramiento cruel en el momento
de escaparse bruscamente de los brazos maternos,
con esa súbita rebeldía de la vida que precede al
aliento final, y que termina con un sordo crujido
siniestro de todo el organismo. De aquella terrible
lucha hablaban los puñitos fuertemente apretados,
lucha en la cual ni siquiera la madre, ni siquiera
aquel vientre sagrado que lo había sustentado un
lapso mayor del vivido sobre la tierra, había podido hacer nada por ayudarle. Aquel pequeñísimo sér
había tenido que pasar por el terrible trance completamente solo, en la soledad infinita que nos es
dado contemplar con una actitud casi estúpida, en
medio de un anonadamiento atroz y cuyo miedo supremo tiene siempre, en nuestro cuerpo físico, reacciones innobles. El, el pobrecito sér que de todos
necesitaba ayuda permaneció solo, terriblemente solo, como si aquéllo hubiera significado el primer avance de la absoluta soledad a que estaba ineludiblemente condenado.

Con el ánimo encogido, Andréïda quedó al lado
del camino, y su alma que no era cristiana, ni estoica, ni resignada, experimentó todo el terror pagano de la impía fuerza que arrastra a los humanos
al abismo incognoscible, y que nos subleva y nos
aterra, pero siempre nos deja las manos cruzadas
e inermes.

Asaltó a Andréïda, de pronto, el deseo confuso
Y quizá morboso de saber qué sería de aquellos suaves párpados de niño que tan perfectamente debieron abrirse, días antes, con un gran asombro risueño de contemplar el sol. Quiso saber qué sería de
qquellos labios que seguramente supieron gorjear
su alegría como el más hermoso de los jilguerillos, y
también deseó saber qué sería de aquellas manecitas
suavísimas que tan sabiamente supieron, con certeza, acariciar el rostro rudo del padre y el pecho
fecundo de la madre en el que, día a día, debieron
acostumbrar el clavar las uñitas en una afirmación
dichosa de posesión, de la más sagrada y legítima
de las posesiones. Y se preguntó, angustiada, qué
quedaría en unos meses de aquella imagen bellísima de niño que ahora se delineaba, tan firmemente, en una rigidez de muñeco de cera que a las claras excluía todo recuerdo de vida anterior.

Nada, nada seguramente, acaso una sombra confusa que se esfumaría en la memoria de la madre, a
despecho de su deseo sincero en recordarla, efectuando, en mente, retornos lancinantes sobre la imagen bienamada… Y, después… el aniquilamiento total ¡Y es que los cuerpos inanimados se vuelven tan extraños a los mismos que los amaran, de la manera más perfecta, que no parece sino que ellos
mismos quisiesen desencarnarse aún más cempletamente! De ahí que nuestros sentidos se encarguen, en el subconsciente de arrojar los seres queridos del
corazón en tal forma que nos obligan a ver en la
sombra rígida una sustitución artera e innoble y
tan ajena al sér que amáramos profundamente que,
más tarde, llega a excluir, hasta de nuestro cerebro,
la más ligera posibilidad de situarlos en un mundo
innominado prevaleciendo, sobre todo ésto, en nuestro espíritu y en aquel momento una como aprensión
física y de terror inconsciente que nos prohibe acercarnos ya más a él haciendo total entrega. ¡Tan fuerte es así esta extraña sensación! Opuesta a ella y con una aplastante falta de lógica reservamos, en nuestro deseo de sobrevivirnos y para nuestra exclusiva persona, una vaga idea de sobrepasar, en alguna forma, la línea misteriosa y oscura que para los otros seres, hasta los más hondamente amados, imaginamos definitivamente final.

El tropiezo inesperado y casual de Andréïda con
aquel cuerpecito infantil reveló a la joven la gran
verdad eterna de la muerte, la inicua verdad única
sobre la inmensidad hosca de la Tierra. Estereotipado en su cerebro restaría, para siempre, aquella infantil boquita violácea, aquellos ojos y aquellas
sienes irremediablemente hundidas. En fin, todo el
horror de la noche eterna, con su rigidez hostil y
desconocida que de forma tan completa, la aleja de
los vivos. Nunca podría olvidar ya más Andréïda
aquella frentecita abombada que estuviese iluminada, tan brevemente, por el pensamiento y que en el
momento de haberla visto ella brillaba aún por el
reciente sudor helado de la agonía. ¡Ah, el dolor
de contemplar las pobres boquitas que exhalan un
estertor demasiado grande para su pequeñez! ¡Ah,
el dolor de ver las diminutas aletas de la naricita
cómo se dilataban en un deseo inconmensurable de
asir el aire que ya no llega nunca a alimentar los
'pequeños pulmones exhaustos! ¡Ah, el dolor de oír
el indescriptible sonido que produce la espantosa
aspiración espasmódica y que retumba fúnebremente en el corazón angustiado de la madre, caliente aún por una loca esperanza que no cesa hasta el último momento y se rehusa a creer en el final! ¡Ah,
la desesperación infinita de no saber cómo ayudar,
cómo participar con alma y vida en el trance angustioso que sufre el dulce cuerpecito, bella prolongación nuestra y tan inerme y pequeño y, al mismo
tiempo, tan gigante en su esfuerzo y lucha desigual
por la vida! ¡Y ese enloquecedor hipo que desgarra
la diminuta entraña y que no lo detiene nada, sino
la muerte! ¡Y hay quien concibe un Dios en el cielo! ¡Y hay quien se atreve a hablar de un Dios todo
misericordia y bondad! ¡Un Dios que mata a nuestros hijos, seres dulcísimos que no han hecho mal a
nadie ni a nada, y que mueren porque sí, por una
enfermedad alevosa a la que el doctor se encarga de
dar un nombre hueco! No, no, no hay nada, Andréïda se dijo que no podía haber nada que no cediese a una plegaria de una madre que se ofrenda entera a cambio de la florecita frágil que tan inenarrable dolor costó a sus entrañas fecundas.

¡Qué absurda, pero qué risible le parecía a Andréïda la idea de un sér oculto y omnipotente, discerniendo los medios menos previstos para producir, en el momento señalado, la crisis definitiva y fatal de la materia! En aquellos instantes, para Andréïda se deshizo, con un levísimo rumor de pavesa encendida entre sus manos blancas, la idea de Dios… Esa gran patraña elaborada trabajosamente, en siglos de imaginación, por los humanos que se resisten, a toda costa, a sucumbir de manera absoluta y definitiva.

En virtud de una comunión absoluta con aquella
madre que la había rozado en su calvario y cuyo
corazón primitivo estallaba calladamente, sin explosión de sollozos, sin hacer gala de su desesperación, Andréïda, la sensitiva Andréïda supo, en breves minutos, del dolor maternal. Y es que la sensibilidad nos acerca a lo divino y por la sensibilidad somos salvos, en la medida en que somos sensibles.

Mientras tanto, el macabro cortejo se había alejado, perdiéndose a la vista de Andréïda. Los ojos
de la joven que, por un momento, los había oscurecido la sombra de la muerte, se abrían ahora enormes, con una expresión interrogativa de suprema angustia. Hasta aquellos momentos su conocimiento de la muerte había sido simplemente retórico, absolutamente lejane a sus sentidos y, como todos los jóvenes, su actitud frente a ella había tenido esa ignorancia egoísta, esa indiferencia irresponsable de quien se cree al margen de una posibilidad todavía remota. La juventud suele ignorar o hacer por ignorar la idea de la muerte. Sabe únicamente que ella emerge de las tinieblas para sumergirse más tarde, otra vez en ellas, y que en el espacio de tiempo entre los dos fenómenos goza y sufre, es decir, vive y eso le basta; porque para la juventud tiene solo importancia la inmensidad del propio sér, mientras que la importancia del resto de los humanos es siempre, para ella, relativa.

Andréïda estaba lejos de ser una cxcepción también en este sentido. de ahí que, con el alma vacilante y aturdida todavía por la luz intempestiva que se
había hecho, de pronto, en su cerebro, emprendiera
el ascenso a la montaña con el espíritu y el corazón
empeñados en una vaga tempestad de la que bien
podía brotar el rayo de gracia.

A medida que sus ágiles piernas conquistaban
la altura, se sentía perdida en un mundo que le
parecía nuevo y, por incongruencia, al mismo tiempo, terriblemente viejo; pero que hasta hcy se le revelaba en su verdadero y cruel aspecto de sufrimiento renovado y de eterna putrefacción.

Por un instante llegó a sus oídos, sensiblemente
amortiguado, el llamado lúgubre y largo del Angelus que las esbeltas torres maravillosas de Taxco
hacían a los fieles del lugar. Y era aquel agitar monótono y ritual de campanas, como un gran manto
inmóvil y tibio de sonido que entorpeciera el pensamiento y arrullase el alma.

Cansada, con los nervios en tensión y ya cerca
de la cumbre, Andréïda se dejó caer sobre una roca
desnuda y de forma extrañamente fantasmal.

En la altura de la montaña se había retardado
la primavera. Sobre las ramas negras de la vegetación enana apenas si unas cuantas yemas brotaban
duras y viscosas, con una pelusilla grisácea en el
fondo de la cual se sorprendía un avivamiento púrpura, algo como una invitación oscura a inclinarse
religiosamente sobre ellas. Las hojas podridas, al
pie de las matas, exhalaban un hedor de cementerin.
Cercop de Andréïda una planta de malvaloca hacía
brillar al sol el verde lujurioso y aterciopelado de
sus amplias hojas, cuya amplitud era de suyo una
amplitud de cadera matrona.

En la augusta soledad de la montaña, Andréïda
sufría la más tremenda tempestad del espíritu. Cogida en un haz tumultuoso de pensamientos opuestos y de toda clase de impresiones sombrías y hostiles, se imponía a su alma, sobre todo este torbellino caótico, la imagen del ingeniero Alonso, a manera de un cilicio cruel que le flagelase el corazón.
En este estado de sobreexcitación, propicia a todos
los descarríos, le pareció a Andréïda el que hasta
aquel minuto había andado todo el tiempo y sin saberlo, sobre un gran cementerio de vidas innúmeras
que tomaban en la obscuridad las más extrañas
transformaciones, para llegar a ser parte del reino
mineral sobre el que ella se encontraba ahora sentada. Y se dijo que el polvo del Globo debía estar
integrado con la carroña de la humanidad entera,
todo lo cual si bien era una verdad muy vieja, para
la joven le parecía un descubrimiento nuevo. Y perdió confianza en sí misma. Prestó interpretaciones
absurdas a todos los sucesos singulares de su vida, a
todas las circunstancias más o menos misteriosas
que la habían rodeado. Creyó sentir sobre ella la
reprobación aplastante del Universo entero. Juzgó
estériles las maceraciones voluntarias y los deseos
vencidos a que había sometido su espíritu selecto.
¿Dominar, someter, comprender? Para qué, se dijo.
¡Para qué agitarse y consumirse inútilmente sobre
el gran camino de muertos que es el Universo!

La niebla, una neblina lechosa, hosca y mortecina, ascendía envolviendo a la montaña con las primeras sombras glaucas de la noche y se espesaba en la lejanía azul, sobre los picachos montañosos
que sobresalían arriba de las nubes bajas.

Los ojos de Andréïda intentaban medir lo que
restaba del día, siguiendo con atención el retroceso
del sol que inflamaba, en tremenda hoguera, al horizonte con una explosión final de encendidos rayos
oblicuos.

El aire saturado de emanaciones nocturnas se
enfriaba bruscamente, y las estrellas empezaban a
abrir, en la bóveda celeste, perspectivas fosforescentes.

Es la noche de la muerte que sube, pensó Andréïda con yn gran espanto en el corazón, y la antorcha de sy pensamiento pareció vacilar, temblar, apagarse por momentos. Y en el más secreto fondo de su sér le pareció oir algo como una protesta airada… alguien que le decía que había equivocado la vida. Con toda la energía vacilante de su alma se resistía a admitir ésto, repitiendo con frase entrecortada el que aquéllo no era posible, el que aquéllo no podía ser de ninguna manera, precisamente “ahora” que él había muerto.

Debo y quiero expiar mi imprudencia, porque
no fué más que una desdichada imprudencia, se dijo,
pero la vida de ella no la había equivocado, no podía
haberla equivocado.

Intempestivamente y frente a ella vió erguirse
la sombra ensangrentada del amigo, de una manera
tan vívida y real que la vida pareció suspender su
corriente en las arterias de la joven. Sin embargo,
la imagen del ingeniero aparecía a los ojos de Andréïda purificada en sus perfiles por las horas de
remordimiento pasadas, como si su alma se cirniese
muy ligera, en resplandores vagos, y se afirmara
únicamente para atormentarla. Era una clase de
fantasma abierto en enorme interrogación sobre la
eternidad. Un sutil fantasma cuya réplica mortal
debía estar a estas fechas en plena delicuescencia,
habiendo dado ya el paso trascendental de lo orgánico a lo inorgánico, en el camino de la disolución
cterna; pero que para Andréïda tomaba, en postrera ficción, su maravilloso cuerpo juvenil de atleta,
aquella su forma superior de vida orgánica que en la
tarde pasada en Chapultepec a ella la fascinase por
brevísimos instantes.

Reconocía Andréïda, por enésima vez, que ella
no había tenido el más ligero derecho sobre la vida
de él, y, en cambio, él se la había ofrendado entera,
de la manera más natural y más inútil, seguramente, por eso, su fantasma se vengaba ahora encendiendo en ella, de nuevo, la mala fiebre de los sentidos y la imaginación.

A medida que la espectral imagen se acercaba a
ella, parecía como si su sombra se espesase en el aire y los ojos de Andréïda adquirían, por momentos,
un pravo dón maldito de extraña clarividencia, Las
ropas del ingeniero las adivinaba Andréïda tiesas
por los líquidos gelatinosos en que se licuaba su unidad superior. De la piel del rostro del muerto desaparecía el amarillento cerúleo que debió invadir
su faz en el instante de escapársele la vida por la
abierta herida de la frente, y daba lugar ahora a
un rosa, al último rosa, a un rosa abyecto, presa ya
del pulular tembloroso de las larvas, de la repugnante gusanera que se cebaría en él devorándole los tejidos, sorbiéndole los sesos en una invasión miserable de formas inferiores. Andréïda supuso también que debía tener ya el vientre de un verde claro
y estar terriblemente hinchado, y la espalda oscura.
Y entonces sintió que le penetraba por la dilatada
nariz un imaginario olor nuaseabundo de cadáver
putrefacto. Con el alma henchida de horror se asombró la mujer de percibir tal cantidad de macabros
detalles reveladores. Antojósele que la tierra entera
trasudaba un humor reluciente y que de manera paulatina íbase transformando en un vapor ondulante
y gélido, de un fúnebre color amarillento. Le pareció notar, entonces, que el firmamento se oscurecía
al ser invadido por una monstruosa bandada de voraces cuervos. Y, en el ambiguo silencio de la hora,
los fulgores nocturnos adquirieron, a sus ojos de
alucinada, la irradiación temida de una universal
destrucción. En una última oscilación lúcida de su
vacilante cerebro, Andréïda se enfrentó tensamente
a la visión macabra… Y en la cumbre mística, cerrando los ojos, Andréïda se dejó abrazar por el helado abrazo del fantasma.

Fué una especie de extraña y terrible expiación
macabra que perturbó sus sentidos con algo como
una embriagadora impureza, y que, al flotar sobre
ella y su espíritu, intentó mecer su pena en las alturas. En el supremo instante su mano había oprimido desesperadamente una rama de tomillo y, al retirarla perfumada, se la llevó a los labios. Deseó
con deseseperación reaccionar, desasirse de aquel
abrazo macabro y tuvo, hasta la intención de mofarse de su absumdo terror. Se palpó el cuerpo, se
llevó las manos a la frente empapada en un sudor
helado: todo su ser le dolía con un dolor agudo de
arterias enfriadas con el hielo de la materia extinta. Advirtió que jadeaba. Con una atención claramente despierta percibía los menores detalles y deseó anular el propio ser sensitivo. No pensar ya
más, acallar, de una vez por todas, el remordimiento, el asco, el oscuro terror; destruir en la propia
memoria hasta el nombre del muerto; pero en vano
su voluntad trataba de clavar todo ésto a martillazos en su cerebro vacilante, consiguiendo únicamente que el tremendo esfuerzo estremeciera, a intervalos, su martirizado cuerpo.

Por fin, su alma logró arrancarse bruscamente
a la fúnebre intimidad, y liberada del fantasma, se
preguntó si habría sido todo ello una mala pesadilla.
Sin embargo, reflexionó, al punto, el que no había
podido quedarse dormida en tan breves cuanto eternos instantes. ¡No existe el tiempo!

Apenas si el sol se había ocultado y el horizonte
se iluminaba ampliamente con el último esplendor
del ocaso. El púrpura y el violeta danzaban resbalando maravillosamente sobre el paisaje y todo parecía desmenuzarse en la luz, con un mágico resplandor. La lejanía saturada de un azul profundo,
brillante, de lisura de plata, invadía profusamente
las alturas, fingiendo una gran bahía esplendente
de la que emergían los picachos de aquella gran cadena de montañas, como enormes y esbeltas torres
bizantinas de maravilla.

El cielo retenía todavía un matiz tierno, hecho
de todos los colores, que se fundía en una gran beatitud de luz de profunda pureza. La falda de la
montaña, en cuya cumbre se encontraba Andréïda,
empezaba a desaparecer al ser invadidas, sus resquebrajaduras, por la niebla y las sombras que se espesaban en la maleza y tras los grandes pedruscos.

¡No podía haberse quedado dormida! se repitió,
una vez más, Andréïda, y un espanto pálido le subió
al cerebro y una idea se le hizo carne en la significación oscura del fenómeno. Y volvió a sentir sobre su aterido corazón el rígido peso que precipitaba en sus arterias un frío sutil. Aquella sensación inmaterial, más allá del pensamiento, era de una incalculable fuerza absurda e inverosímil.

¡Lo sobrenatural no existe!, musitó Andréïda
para su cerebro, como si se tratase, para ella, de un
conocimiento recién adquirido y del cual no se está
muy seguro; pero a pesar de ésto, le pareció que el
muerto había dejado en sus labios algo de sí mismo,
encadenándola para siempre a él.

La noche se echaba encima y Andréïda no osaba
moverse. En la hora nocturna la naturaleza tenía
algo de siniestro, de vergonzoso; parecía que intentara desmentir su fin innegable de triunfar en constantes y renovadas explosiones de vida y que se solazase, en cambio, en su desintegración, creando sobre el estiércol de su fracaso y su destrucción, diminutos seres primarios, impuros y repugnantes. ¡ Y
qué es sino la noche una anticipación fragmentaria
de la gran noche eterna…!

El alma de Andréïda, prisionera de un muerto,
oía resonar en sí la lamentación desengañada del
Eclesiastés, confundida en el gran clamor que arranca de lo remoto de los siglos con un enorme ruido
átono, lleno de todos los sonidos jamás percibidos
por humano órgano físico, extrañamente sordo y
como subterráneo de canto milenario que en la muerte adormece las generaciones de los hombres en una
penumbra de ocaso.

La naturaleza, en aquella hora nocturna, tenía
algo de quimérico y de salvaje, de inerte y de voraz.
No era una noche como las demás que Andréïda había conocido sin decirle nada a su alma y que habían
resbalado sin importancia, extinguiéndose tras de su
breve vida. No. Todo aquello inaccesible a los sentidos y a la observación de los humanos, hoy, reconocía Andréïda, le era revelado a ella por una extraña disposición de ánimo y de ambiente. En su
gran círculo, vertiginoso y maldito, la noche gestaba, 
—con una especie de morboso deleite que hacía al espíritu un terrible daño, —un exceso de formas que se
desmenuzaban, que se prostituían sustentando el
universo de lo infinitesimal, y que se abría, ante la
joven, con la desproporción inversa del macrocosmos, escapando ágilmente a su percepción con esa
sorprendente relación de lo infinito exterior con lo
infinito interior, y que obliga a nuestro cerebro a
andar a marcha dislocada, solo de imaginar ese infinito cósmico de partículas ínfimas en oposición a
las desmesuradas unidades astrales.

De la tierra, sede de vida que es organización,
fluía, por doquier, una callada licuefacción negra
de carne putrefacta que lo empapaba todo. La noche
hervía, en plena efervescencia de rápida descomposición y ruina, en el clímax de su liberación perversa.

Frente al monstruo, blanco y lúgubre a la luz de
la luna, que era el Universo, la idea de la muerte y
su podredumbre en el desmenuzamiento de las formas tcrresres, representó, ante la fragilidad humana de Andréïda, su papel importante y real sometiendo a la extraordinaria joven a una tortura vergonzosa y odiosa, al mismo tiempo. Comprendió que la vida era un accidente entre dos fines y que la humanidad entera pertenecía a la tierra, moviéndose siempre en la incertidumbre del lapso que se le prestaba entre el nacimiento y la muerte, cuya relación natural entre los dos fenómenos ella se resistía a aceptar. Dentro de su cerebro le fué dado medir a Andréïda, por un instante, el espanto estupendo y homicida de negar toda prolongación del sér. Y sintió dentro de ella el espacio vacío al que muy pocos hombres han sabido acercarse sin miedo, y experimentó plenamente el gran cansancio del mundo que gira sobre su propio eje, en un procrear sin fin y en una perpetua destrucción de lo que crea.

Volvió sobre su memoria el recuerdo lancinante
del joven amigo; pero, en esta vez, despojado de todo
privilegio fantasmal, y con los ojos de la carne Andréïda se adelantó al tiempo y contempló las diversas etapas que, con toda seguridad, estaba destinada a sufrir aquella unidad humana en el tambaleo tétrico de su disolución. El espectáculo que ahora imaginaba Andréïda era de tal naturaleza abyecto
y miserable que hacía vacilar su valeroso espíritu,
al mismo tiempo que ejercía sobre él una como fatal
fascinación, particularmente profunda. Espió con
horror hipnótico todas las fases de fermentación
pútrida que asolan y se ceban en el miserable tejido
humano, y cuyas sorprendentes combinaciones orgánicas son de una repugnancia y viscosidad que el solo pensar en ellas humilla el alma de los seres, con
la peor de las humillaciones, la de saberse alimento
de viles gusaneras. ¡Cuántas y cuántas invasiones
de insectos necesita el cuerpo humano para verse
libre del hedor final, antes de la llegada del último
e ínfimo coleóptero negro que, al cabo de tres años,
termina por purificar los huesos de la carne maldita y aun así, siempre queda, en los recovecos de la
osamenta, vestigios impuros, una especie de grasa
infamante, de sebo amarillento de cadáver, a la vista del cual el más soberbio de los sueños humanos
no puede menos que destornillarse como un fantoche
ridículo a quien, de pronto, se le reventaran los hilos de su artificiosa moción!

El sueño de grandeza de Andréïda se desplomó
sin ruido. ¡No podía ser en esto también excepción!
Ante la verdad de verdades, drama de dramas de la
humanidad que hasta aquel momento le había sido
dado comprender en toda su intensa profundidad,
ella no podía seguir creyendo en la utilidad y perdurabliuad de su frágil obra, tan frágil, pasajera y
mortal como la unidad carnaf que la había imaginado y sustentado.

¿Dónde está la verdad? ¿Qué es la verdad? Ahí
estaba solo ella, en la cumbre mística de la meditación y el ayuno, con su esplendorosa juventud
abierta en no sospechada ofrenda; en toda su belleza, en toda su fuerza; la cabellera luminosa a los
cuatro vientos, las manos repletas de frutos como
encarnación vibrante de la vida, como eterna visión
de la Eva eterna. Detrás de la mujer toda su breve
vida, elaborada por el capricho de sus manos, tanto
como por las circunstancias y el ambiente. Pudo
haber aceptado la felicidad usual, la prosaica felicidad usual que como raudal precario de agua tibia
mana siempre ajeno a imprevistos remolinos y a
excesivos fervores, es decir, la felicidad de una simple mujer, plasmada humanamente; pero, a semejanza de aquel ejemplar genial y hermoso del divino judío que no quiso ser simplemente el hijo del
hombre, ella se resistía tambiérn a ser sencillamente
mujer y había arrojado, con firmeza, de su vera y
como cosa despreciable, la convencional felicidad de
los humanos. Mas ahora cabía el preguntar si con
ella no habría arrojado su propia vida; si acaso no
estaría la realidad y la certidumbre fuera de su ser
y su deseo, para siempre; o dentro de él, como ella
había creído hasta hoy. Sus razonamientos perdían
pie, se hundían y se agarraban a la razón como a un
tablón podrido que zozobrase bajo aquel enorme peso. Sin embargo, muy en lo hondo, restaba, sobre
el pesado fardo del remordimiento, el deseo primitivo que aún seguía deslumbrando victoriosamente
a la mujer llena de confusas sombras y de quimeras,
revestida con el esplendor reverberante de la ambición suprema.

La claridad lunar incensaba el aire con manantiales irreales de paz argentada y azul. Los grandes
mantos negros de las tinieblas nocturnas envolvían
los contornos inmóviles con suavidades insospechadas, transformando visiblemente el color, la forma,
el peso y la atmósfera que solía rodearlos a la luz
cruda del sol. Brillaban las estrellas en las alturas,
entre limbos azules y serenos y sus mundos luminosos espolvoreaban de incandescencia cósmica la inmensidad fantasmagórica de la gran vía blanca, del
gran chorro de cristal lechoso. El cielo entero desbordaba, en torrentes de brillantez fantástica, el milagro estupendo de su baño lustral. La noche ardía
en una indescriptible magnificencia de luz, en una
apoteosis triunfal de fuego lívido.

¿Ante la eterna belleza de los mundos qué significa la disolución de un sér, qué significa la descomposición de un billón de seres? ¿En el silencio
tenaz del Universo, qué cuenta una voz de mujer
interrogando al Infinito? ¿En aquella transformación perpetua de los astros, en aquel cuajar eterno
de átomos en formas errantes sobre espacios imposibles que en el cielo enhebran mundos dispersos,
qué importa un pensamiento femenino atrapado en
la falaz vorágine de lo ignoto? ¿Qué representa el
dolor lancinante de un remordimiento ante la destrucción universal y premeditada que se devora a sí
misma, de manera implacable y voraz? Fulgores que
se apagan, gestación de auroras y de estrellas que no
fueron, átomos que se dispersan, vidas ignotas, gigantes y minúsculas, que se extienden o mueren en
una lucha feroz sin tregua y sin fin. ¿Qué importa
al todo el destino de un sér o de un mundo? ¿Y qué
objeto tiene la enorme fatiga de intentar comprender? ¿Y qué importancia adquiere, por último, el sentir claramente, el diluir del propio cuerpo en el
tiempo, ya que no somos otra cosa que una viviente
corrupción?

Hay que olvidarse del despojo cercano y del propio fin. Hay que alejarse de prisa, muy de prisa; hay que cerrar los ojos al resplandor de la luz invisible, de la infranqueable luz que nos enceguece y nos encierra dentro del piadoso cautiverio de la vasta incomprensión humana.

Y es así como ella, Andréïda, una brizna minúscula de Tempestad sobre el gran camino de muertos que es el Universo, sintió que, agotado el cerebro, llegaba para ella el momento preciso de su liberación. Desde aquel instante conoció que aquel muerto no yacería ya más sobre su corazón. Una temible alegría la invadió entera. Por fin, el muerto había pasado de largo, tornándose, poco a poco, en una sensación remota como si un velo de alas negras, con un rumor de olvido, flameara en la sombra de su memoria.

La mujer desfallecía de fatiga, sus hombos se
estremecían a intervalos casi imperceptiblemente y,
de pronto, se irguió. El yo físico imponía su derecho a la voluntad remisa. Los labios resecos exigieron agua, alimento el estómago y las piernas, entumecidas por el largo reposo, un gran deseo de moverse. Con las mejillas ardientes y las extremidades heladas Andréïda emprendió el regreso; mientras su pensamiento fugitivo efectuaba, también, un
retorno vertiginoso sobre sí mismo.

Primero lentamente, después con una prisa enorme, descendió la montaña. Las palmas de las bellíeimas manos, al aferrarse a las rocas o a las malezas, sufrieron desgarramientos atroces y pronto corrió, por entre los dedos, el encendido líquido tibio y pegajoso. La tibieza de la sangre reveló con claridad a Andréïda que ese algo caliente de su hermosa juventud se interponía fuertemente frente a la
muerte infinita. A medida que la joven descendía
por los prados constelados de hongos rosáceos, percibía en el aire algo amistoso y acogedor. Un pequeño sapito saltó antes de que ella posara el ligero
pie sobre la yerba y aquel salto imprevisto provocó
en su garganta una risa nerviosa con un temblor de
amistad para el minúsculo sér que la llenó de alegría, como una expresión tácita de su existencia
real. Entonces y solo entonces, se dió cuenta exacta
de la enorme fatiga física que la agobiaba y, con la
respiración difícil por la reciente carrera, tuvo un
solo pensamiento, el de llegar cuanto antes al hotel.

Metió los pies en una charca sonora de ranas y
poco después llegó a poblado. Pero todavía tuvo que
hacer diversas y penosas ascensiones y descensos
por las callejuelas oscuras que esa noche se mostraban singularmente acogedoras. Parecíale a la joven que las propias piedras, que las luces encendidas en los portales de las casas, la envolvían en una tibieza grata. La tenue luz de los faroles obligó a huir, del cerebro de Andréïda, aquella cosa quimérica que se le había aparecido en la oscuridad de las horas pasadas y que indudablemente se había quedado en la montaña, para fortuna suya.

Por fin, vió ante ella la casa colonial, acondicionada de hotel, cuyo cancel moderadamente aceitado, se abrió con demasiada violencia por el empuje nervioso de su mano crispada. La luz poderosa de
un foco de nitro hizo cerrar los hermosos ojos a la
joven.

El bellísimo rostro de Andréïda había adelgazado en las horas de angustia sufridas y tenía un aire macilento y empalidecido que asustaba. ¡Venía
del país de las sombras, de haber palpado la muerte con sus propias manos! Y, de pronto, al abrir los
ojos, contempló frente a ella la más sorprendente
visión de vida, la más vibrante plasmación de ser,
el deseo de no morir encarnado en la forma de un
hombre. De un hombre que la miraba fascinado,
visiblemente turbado por su belleza, y Andréïda se
sorprendió dé encontrar, en los ojos punteados de
oro que de tan codiciosa manera la contemplaban
el camino del regreso. ¡Era como un poderoso cable
de Tentación colocado sobre su terrible abismo de
desesperación y de dudas!

Al fulgor lívido que había sentido zumbar en su
interior allá en la cumbre mística de la negación,
sucedía ahora un fulgor rojo de sensual tibieza al
que se entregó con inusitada docilidad y sin oponer
su acostumbrada hostilidad.

Por segunda vez, ella lo miró observando detenidamente en él una parecida turbación a la de ella
y ambos contuvieron la respiración. Finalmente, los
suaves párpados de la joven se abatieron sobre los
maravillosos ojos, con ese recelo que trasluce siempre la primera honda impresión de un hombre en la
mujer virgen.

Después, no sabiendo qué hacer, Andréïda se
deslizó, con un movimiento felino y de elegancia innata, hacia su habitación, mientras, inexplicablemente, de lo más hondo de ella, su ambicioso sueño volvía a tomar cuerpo y se hacía sólido y poderoso. Y la extraordinaria joven acabó por pensar que no podría ya nunca más abandonar su deseo de predominio y superación después de que éstos habían subsistido a la terrible prueba de la Montaña.

\* \* \* {.espacio-arriba1 .centrado}

Por su parte, el hombre había visto desaparecer
silenciosamente a la extraña y bella visión ensoñadora que llevaba en los cabellos luminosos residuos de hojas secas, en los ojos un abismo de tortura y en la frente purísima el esplendor de extraños pensamientos; mientras las manos de trazo puro y sensual, empurpuradas por la sangre de las palmas desgarradas en no sabía qué suprema lucha desesperada, las llevaba cruzadas sobre el pecho, en ademán candoroso y soberbiamente encantador. {.espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# Capitulo X

La primera sensación de Andréïda, a la mañana
siguiente, fué la de su soledad. Su cuerpo ondulante, perfumado y flexible se revolvió en la suavidad
de las sábanas con una ansia instintiva de vida.

Como borrada por una agua clara, purísima y
fecunda, había desaparecido de su alma la mala pesadilla fúnebre que la asaltara en la tarde anterior.
Sin embargo, bajo la sombra deliciosa de las largas
pestañas se alargaba otra sombra, la sombra de una
restalgia vaga y sin nombre, de un deseo incierto Y remoto, algo así como la niebla de un anhelo confuso que se le clavaba en la carne con un extraño y dulce titilar de entrañas.

La carnosidad suave de los labios bellísimos experimentó un leve temblor de fruto divino, próximo
a desprenderse del árbol de la vida. Y entonces, de
seando libertarse del traidor embrujo, de aquel desfallecimiento de sus nervios, hasta aquel momento
jamás experimentado por ella, saltó bruscamente
del lecho y fué a descorrer los cortinajes de cretona floreada, cuyos colores brillantes y chillones contrastaban con la severidad oscura del inmueble colonial. Recordaba que, a propósito de tales colores de un primitivismo atroz, había dicho ella a Nelly que
le parecían una irreverente carcajada infantil para
la golilla almidonada del estirado gran señor español.

Un torrente de viva luz se entró por la ventana, deslumbrando sus pupilas y fué a quebrar sus
hilos de cristal sobre la despeinada cabellera luminosa de Andréïda, quien se dedicó a aspirar, con
deleite, el aire fresco de la mañana.

La vista que se ofrecía a su contemplación era
sencillamente arrobadora. Desde la ventana podía
dominar una serie de tejados rojos y risueños y de
fachadas inmaculadamente blancas que tenían por
fondo las montañas azules y a su izquierda un girón
del campanario mágico de la catedral, laborado fatigosamente por manos indígenas y en el que se
sentía, más que se veía, el que, aun habiendo seguido con fidelidad aquellas manos oscuras las indicaciones estéticas del conquistador, habían también trasladado a la piedra la exquisitez anímica de sus almas libres y soberanamente artistas. Así se explica cómo, a través del inmeso y riquísimo acervo arquitectural único de todo México, puede sorprenderse aquí y allá, dónde el español exigiera el clásico acanto, la concepción artística del indio, casi insensiblemente, estilizó agudeces de cactus o cinceló palmas.

Andréïda se complació en extraer todo el goce
visual de aquel paisaje, con aquella disposición sensible para lo bello que la empujaba a exprimir la
esencia de las cosas con un fervor atormentado de
artista que se complace en estrujar el motivo de su
emoción estética hasta hacerle destilar cuanto de
bello contiene. Sabía que, si bien su ventana daba,
por un extremo, a la callejuela pintoresca de su derecha, para llegar a la entrada de la casona se veía
obligada a descender un piso por una escalera estrecha de encendidos y pulidos ladrillos. Exigencia caprichosa del terreno escarpado y montañoso sobre
el cual está levantado Taxco entero y que a ella la
llenaba de un goce estético profundo.

El airecillo fresco vivificó sus nervios y la alegría mañanera, hecha con todos los sonidos de un
pueblo feliz que ignora las premuras odiosas de la
edad moderna, se le antojó a Andréïda un hermoso
himno al sol. El andar cansino de un aguador indígena que iba dejando a su espalda un rastro luminoso de agua clara al zarandear las ollas rojas que se equilibraban suspendidas de un palo pulido por
el uso y colocado sobre su hombro, apresó la atención de la joven que después fué atraída por el llamamiento monótono e ininteligible de un vendedor de
sandías que ofrecía su rozagante mercancía abierta
al sol, mostrando las entrañas encendidas y jugosas
del fruto, como una prava y descarada invitación a
los dientes voraces. Todo, todo le parecía a Andréïda
que en aquella mañana vivía con vida más intensa,
con una especie de furor sagrado, empeñado en afirmar su triunfo sobre las sombras nocturnas que,
hacía unas horas, habían efectuado su retirada.

Arrancándose pesarosa al espectáculo cautivante de la calle, la blanca mano de Andréïda dejó caer
los cortinajes que hicieron, de nuevo, la suave penumbra en la habitación. Con ella le pareció a la
joven que le entraba, de pronto, frío y fué a refugiar su cuerpo en las sábanas aún tibias. Una tristeza ardorosa la asaltó otra vez en unión de un deseo innominado y febril de no sabía a ciencia cierta qué con un ¡Bah! despreciativo para su especial estado de espíritu se dijo a sí misma que quizá fuese ella de otra insatisfecha de belleza como su favorite Oscar Wilde.

Una parvada de chiquillos que pasó por la calle
armando una algarabía de pájaros gozosos, la ofendió con su explosión de vida, y deseando analizar su
disgusto infundado, contradictorio e injusto, acabó
por atribuirlo a la soledad en la que la había dejado
Nelly al marchar, hacía de ésto ya una semana, a
la ciudad de México, urgida por las exigencias de su
trabajo y también para atender de cerca el desarrollo de los acontecimientos, temerosa siempre que el
nombre de la bienamada sufriera menoscabo.

Por cartas frecuentes de Nelly sabía Andréïda
que a la curiosidad, un poco impertinente de los
periodistas con la que fuera asaltada los primeros
días, la admirable amiga había opuesto una sistemática y firme discreción, lo que había evitado hasta el que lograsen el publicar el más humilde de los
retratos de Andréïda,

Todo esto pensaba Ándréïda mientras oprimía
el botón del timbre con frecuentes llamados, sabiendo, de sobra, lo lento del servicio, por lo que necesitaba ser acicateado en aquella forma. Veinte minutos después y cuando ya comenzaba la joven a
perder la paciencia, entró la muchachita de suaves
ojos oscuros y mejillas fuertemente rosadas que
acostumbraba servirla y que traía, como todas las
mañanas, con gran torpeza la bandeja de su desayuno, consistente en un café malísimo que repugnaba
a Andréïda, pero que, en cambio e invariablemente,
venía colocado el humilde servicio sobre la más primorosa de las tohallitas deshiladas. Con una sonrisa
cortada y un poco boba, preguntó la muchacha si no
se le ofrecía algo más a la “niña” e hizo entrega de
un fajo de periódicos que llevaba bajo el brazo y de
una carta. Rehusó Andréïda sus bien intencionados
servicios y la sirviente acabó por retirarse, no de
muy buena gana.

Andréïda se dispuso a leer la carta de Nelly, reconocida inmediatamente por ella a causa de los altos rasgos metódicos trazados en el sobre. Y poco
después se informaba de su contenido. En forma ordenada y fría 1e hablaba Nelly de que la reacción de
los diarios capitalinos había sido beneficiosa para
Andréïda; pues que debido, con seguridad, a la encubierta notoriedad que había tenido en aquellos
días su nombre, recibió nuevas y halagüeñas proposiciones para colaborar en diferentes e importantes publicaciones. Añadía Nelly que, haciendo ella caso omiso de la terminante negativa de Andréïda para aceptar tales proporciones, ella había aceptado, en su nombre, las más ventajosas, y no se había limitado solamente a ésto, sino que, sustituyéndola, había enviado ya una serie de artículos que, si bien en ellos, el público, no había encontrado la más ligera relación con la oculta tragedia que su prava curiosidad le inducía a husmear, sí había bastado, el solo nombre de Andréïda, para que fuesen vorazmente leídos. Por el mismo correo, decía, enviaba a la amiga los artículos ya publicados. En rengiones más abajo manifestaba la práctica muchacha que ella, Andréïda, no tenía necesidad de calificar aquéllo como lo había hecho en su carta anterior, con aquel feo nombre de _tarea de buitres y de hienas_ y que en adelante se abstuviese de hacer esa clase de referencias zoológicas. Comprendía que su estado de espíritu era el menos propicio para estimar debidamente el magnífico escalón que le brindaban, para su ascenso, aquellas no buscadas circunstancias; pero que sí debían aprovecharse. Todavía más abajo le hablaba Nelly del esfuerzo que debía hacer Andréïda por levantar su deprimida voluntad y emprender ya por ella misma aquellos trabajos que a Nelly, tanto costaban; pues era cosa terrible eso de querer imitar el inimitable estilo caprichoso de la amiga y abordar los inabordables temas que ésta gustaba de desarrollar.
En medio de su cólera y su disgusto Andréïda no
pudo menos que sonreír, para su interior, solo de
pensar que Nelly había intentado y además creía
de buena fe haberla sustituído, aun fuese en aquellos artículos, seguramente banales que aparecían bajo su nombre y que ella no se molestaría siquiera en
leer. En una post data final, Nelly le anunciaba,
al descuido y como si se tratase de algo sin importancia, el cese de la joven en el ministerio. Andréïda se mordió los labios con rabia, hondamente
mortificada por lo que se le antojaba una insufrible
humillación, y, arrojando a un lado y con disgusto
los diarios, ingirió de prisa el pésimo café de su desayuno, doblemente pésimo en aquella mañana por
estar ya frío.

Poco después procedió a bañarse en la incómoda
bañera puesta allí por una indicación suya. Indicación calificada de exigencia por el hostelero. Minutos más tarde y todavía húmeda bajo la bata afelpada, examinó displicentemente los diversos contratos ya firmados y que le había incluído Nelly.

Obligada a admitir, por las sensatas reflexiones
de la amiga, la necesidad de reanudar su vida activa, se dispuso Andréïda a doblar la hoja de la dolorosa rectificación espiritual a que había sometido
su alma en todos agquellos días, tan intensamente vividos, en el silencio de los paseos solitarios por los alrededores de Taxco.

Consecuente con su nuevo propósito se dispuso
a improvisar de escritorio la minúscula e incómoda
mesita de su alcoba, colocando sobre ella buen número de cuartillas en blanco. Transcurridos unos
momentos de concentración estéril e incompleta,
pues que su mirada y su atención se distraían, a pesar suyo, bien haciendo inventario de los mercenarios y poco artísticos objetos de la habitación o bien contemplando su propia imagen reflejada en el espejo del tocador, situado precisamente frente a ella, acabó por retirar con rabia los plieguecillos, insolentes en su blancura inmaculada. Su repentina impotencia la desesperó unos instantes como a un gran cantante que constatara, de pronto, la pérdida de su carísima voz.

Dió unos pasos por el reducido recinto de la cámara y encendió un cigarrillo, consciente de que la
excitación nerviosa que solía producirle, la ayudaría,
un poco, en su propósito de trabajo.

Andréïda no era una habitual del intrascendente y perfumado vicio, adoptado con alarmante generalidad por la mujer moderna. Es más, abominaba la joven el ver que la mujer hiciese exhibición de él en sitios públicos y concurridos, y únicamente transigía con él como motivo estético en la intimidad espiritual de un salón reducido. Tampoco había permitido que se impusiese a sus costumbres como necesidad habitual, puesto que su espíritu selecto y la disciplina moral a que invariablemente sometía su cuerpo, repugnaban de todo aquello que significara una supeditación esclavizante, por ligera que ésta fuese. Andréïda no era mujer que se dejara dominar por ningún vicio.

Esta vez la excitación nerviosa del humo azul no
surtió el efecto de costumbre y que ahora era esperado tan ardientemente por la joven. Andréïda acabó por achacarlo a la incomodidad de la mesa, a lo
extraño y mercenario del ambiente, ayuno de objetos
familiares que en México solían rodearla con la cordialidad de las cosas que amamos y que contribuyen, de manera tan perfecta, a dar la necesaria confianza en sí misma. En vano fumó un cigarrilo tras otro a grandes bocanadas, con un deseo de embriagarse y sacudir sus nervios sumidos en una abulia inacostumbrada. Con mano nerviosa trazó una serie de conceptos falsos, de contrasentidos y errores,
vertidos en frases vulgares que, al releerlas, la exasperaron. Terminó por admitir que en aquel momento le era materialmente imposible exteriorizar sus
ideas, y acabó por achacar su impotencia a que en
un artículo periodístico no podía decirse nada, ni
valía la pena de decirse nada ya que su efímera vida, destinada a marchitarse en un día, no respondía,
al esfuerzo ejercitado, a satisfacción.

Por un instante la cautivó la originalidad repentina de una idea que andaba a saltos y brincos en
su cerebro, y se afanó en una caza infructuosa de la
palabra justa que tradujese debidamente el concepto, encajando en la frase con la perfección requerida. Ansió, de pronto, un mágico pincel que trazase
en el aire perfiles violentos y concretos en impresiones luminosas y rápidas que llevaran al cerebro contenidos de luz. Y encontró lastimosos e ineficaces los minúsculos signos que deforman sardónicamente el pensaniiento vaciado en la absurda mecánica de las frases, de las que huye la idea, la hermosa idea, como un pájaro multicolor e inapresable que se ríe del falaz intento de sujetarlo.

En cambio, se dijo Andréïda, cuán distinto era
el imperio de la palabra oral, la suave, la violenta
palabra que se esparce en ondas musicales, con el límite único del espacio, alargada por el ademán sugerente de las manos, suscitadoras de emociones
encontradas y bellísimas que se esculpen en la tenue ligereza del aire. ¡ Ah, el imperio de la bella palabra hablada que se escapa triunfal, ajena a las limitaciones de lo elaborado meticulosamente, con la
rigidez estulta de las imposiciones metodizadas!

Andréïda deseó lo imposible para sus cuartillas
en blanco, al envidiar la breve vida de la alocución
de un orador que plasma en las almas de sus oyentes
precisamente aquéllo suyo que desea esculpir y, por
ello, abominó de la palabra escrita que se ve sujeta
a ser deformada por cada lector, quien se limita a
traducir su sentido siempre conforme a sus íntimas
convicciones, del todo alejadas a la naturaleza exacta de la idea que inspiró al autor.

Con un ademán violento rompió las hojas escritas, y sobre una nueva cuartilla en blanco, Andréïda
trazó cifras, pasando insensiblemente del estéril estado creativo a la vida práctica. Las sumas que obtuvo de las diferentes entradas económicas que, en
adelante, disfrutaría por concepto de los contratos
celebrados, demostraron a la joven que suplirían, con
holgura, sus necesidades de exquisita. Convencida de
ésto, con satisfacción, procedió a vestirse con aquel
cuidado meticuloso y artista que ponía siempre en
el arreglo de su preciosa persona. Espió con ojo crítico su delicada silueta reflejada en la borrosa luna, poco gentil, del tocador, y prendió en su hombro un pequeño ramito de miosotis artificiales que armonizaron, de manera delicada, con su figura y con su
gracia. Andréïda adoraba la franqueza de las líneas
sencillas que dibujan los movimientos, y gustaba
también, de la ligereza espiritual y libre de los pequeños adornos vívidos que animan, con un toque
de color, el conjunto, Hacía del atavío de su cuerpo
un culto, el culto del artista que se complace en su
ebra sabiendo de antemano que su afán se perderá en una horas y, sin embargo, pone en el empeño
toda la sublimidad de su temperamento estético. Así
debió sentir y no de otra manera, Miguel Angel, cuando modeló su escultura, en nieve, en el jardín de los Médicis.

Un último clip para sujetar una onda rebelde y
Andréïda se dispuso a salir. El ademán de su brazo
quedó incompleto en el aire. Tornó a invadirla el
anhelo incierto y como de expectación que la afectara en las primeras horas de la mañana. Aquel algo
indefinible que parecía colarse por la ventana, filtrarse por las paredes lisas, poniéndose de manifiesto cuando menos se le esperaba para situarse allá en el fondo de su pensamiento, como en sutil acecho. Una vez dentro solía tomar la forma de un vago
des_a.hento apacible y lánguido para asumir, al rato,
la inquietud febril de un anhelo inaudito y sin nombre que terminaba por sumergir su espíritu en una
tristeza infundada e inquieta. Sensaciones estas, todas fugaces y confusas, que escapaban al análisis de
Andréïda, esta vez no muy certero ni quizá deseoso
de escarbar muy hondo en el subconsciente.

Conoció la joven que aquello era más que un superficial malestar moral y le pareció que asumía las
características de un estado especialísimo de su carne y de su pensamiento, produciendo en su alma un
vacío ansioso y, a la vez y por incongruencia, sumamente grato.

Por lo regular Andréïda se complacía en estar
sola; sin embargo, en aquella mañana deseó la compañía de un ser humano inteligente y comprensivo
y sin explicarse el por qué y a raíz de este repentino e inusitado anhelo le vino a la memoria la mirada expresiva de los ojos punteados de oro que en la
noche pasada le habían manifestado su admiración
de manera bien poco discreta.

El deseo inaplazable de volver a encontrar aquella mirada la impelió a salir, con precipitación, de su
alcoba, sin detenerse a reflexionar inquisitivamente
sobre el origen de aquella súbita necesidad de compañía.

Descendió con ligereza infantil la pulcra escalera reluciente y se detuvo en el primer descansillo para poder recorrer con la vista, desde aquel punto estratégico, todas las personas que en aquel momento
se encontraban dispersadas por el reducido patio que
el dueño se empeñaba en denominar _lobby_, contagiado de un grave “pochismo”, por el frecuente tráfico de “turistas” que había favorecido, en los últimos años. su antes poco próspero negocio.
Hondamente defraudada al no tropezar su mirada más que con las figuras poco gratas de una media docena de americanas, feas de manera increíble y que reunían sobre sus cuerpos caducos, con gran aplomo y un mal gusto anonadante, las más extraordinarias combinaciones de colores, y junto a ellas, varios pares de largas piernas enfundadas en blancos pantalones que columpiaban sus magras figuras,
de edad indefinible, en sendos y cómodos equipales.
Las blancas cabezas se tocaban con grises cachuchas
y las ciranescas narices aspiraban, con deleite, el
apestoso tabaco de sus pipas. A cada aspiración, las
hondas arrugas de los cuellos se encogían como encendidos mocos de pavos.

El segundo tramo de la escalera lo bajó Andréïda con lentitud y en la misma forma salió a la calle. Su indecisión sobre qué camino enderezar sus pasos la obligó a detenerse en la puerta, sintiéndose,
de pronto, sola en el mundo; sola, sin alegría y sin
dolor.

Finalmente optó por dirigirse al hermoso templo que no distaba de allí ni dos cuadras completas.

Decíase a menudo Andréïda que con gusto le encantaría pasar horas y más horas en la penumbra digna y un tanto misteriosa de las iglesias, si éstas
alguna vez se encontrasen completamente desiertas. Le contrariaba, sobremanera, tropezar a su entrada con la mirada inquisitiva de la vendedora de
imágenes y medallas que practicaba, a ciencia y paciencia de los párrocos, un simonismo burdo. Había
cbservado la joven, en diversas ocasiones, la sonrisa complaciente y rastrera de estas mercaderes para la invasión impertinente y poco respetuosa de
los “turistas” americanos y, sin embargo y de manera invariable, Andréïda había podido constatar el
mohín escandalizado que solían adoptar al entrar
ella, con su tipo indiscutible de latina, y al no dibujar sobre su frente el ritual y pueril ademán acostumbrado.

Disgustaba también a Andréïda encontrar, a su
paso, toda una serie de narices puntiagudas elevadas con una curiosidad impertinente y del todo incapaces para sentir una genuina emoción estética,
por haber perdido la sensibilidad al roce canallesco del _dollar_.

En aquella luminosa mañana, el sol se entraba
a raudales por la claraboya del sin par templo de
Taxco, e inundaba el centro de la espaciosa nave
con un haz luminoso de maravilla en el que danzaban millones de minúsculas moléculas de un oro pálido, hermano de aquél con que gustara espolvorear
sus imágenes la fe ferviente y tranquila de Fray
Angélico. Aquel resplandor tenía algo de inaudito
milagro de procedencia divina al teñir, con un fulgor alegre, la iglesia entera.

Los ojos de Andréïda vagaron con deleite por
los bien conocidos detalles ornamentales, cuya suntuosidad complacía al refinamiento sofisticado de
la joven. Poco después pasó a la sacristía, en cuya
humedad maloliente se desteñían los maravillosos
lienzos de Cabrera, entre ellos, el del señor De Borda, gambusino genial, filibustero y poeta.

Queriendo Andréïda hacer tiempo hasta la hora de la comida, intentó tomar unos apuntes ligeros de aquella cabeza inteligente, y tres horas más tarde, tomó el camino de regreso al hotel.

No era una dibujante experta; pero sabía captar el trazo esencial y revelador que conquistaba, al
punto, la mirada del experto por la franqueza realista de la intención.

Apretó el paso la joven con la idea de encontrar
posiblemente, a la hora de la comida, al desconocido
que de tal manera había perturbado su habitual indiferencia, y, poco después subía a su habitación
con el objeto de asearse y retocar su tocado.

Minutos más tarde, penetraba en el salón lleno de humo y de ruidos característicos en los que
se mezclaban breves órdenes dadas en voz alta y
en inglés, hasta el punto de que se creyera, con facilidad, encontrarse uno en algún lugar típico y
ficticio de la California, si no se observara la lentitud desmañada de los camareros y camareras, improvisados de tales.

Andréïda tomó asiento en una pequeña mesa,
un tanto alejada de aquel bullicio ensordecedor hecho de tintineo de platos y de áspera lengua extranjera y se dispuso, con ojos inquisitivos, a recorrer
mesa por mesa. Su disimulada inspección no le dió
los resultados que esperaba; pero, al final de la comida y cuando ya había desistido, al parecer, de alimentar aquel inusitado interés, se sorprendió a sí
misma al constatar su pulso acelerado por efecto
de ver entrar al desconocido en compañía de dos
jóvenes.

La mirada de él resbaló un momento sobre ella,
mientras tomaban los tres asiento en una mesa cercana a la ocupada por Andréïda, La joven bajó los ojos y se reprochó aquella agitación, diciéndose que
había hecho mal en dar importancia al encuentro de
la noche anterior; puesto que, ahora sabía que la
insistencia de aquel pensamiento había sido la razón de su malestar moral.

A pesar suyo apreció la apostura varonil del
hombre a la luz del día y su indiscutible y atrayente
personalidad; asimismo sorprendió, con disgusto,
en él, un gesto de indiferencia displicente como de
un ser que vive alejado de todo y ha sido muy mimado por la fortuna, además de codiciado por las
mujeres.

Al atento oído de Andréïda llegaban, de la mesa
vecina, frases aisladas, y por ellas se enteró que el
nombre de su desconocido era el de doctor Raúl. En
contraste con la indiferencia nada fingida del hombre, sus dos jóvenes acompañantes no despegaban
la vista de Andréïda, testimoniándole la más rendida de las admiraciones, lo que acabó por molestar
a la joven y precipitar su salida.

Pesarosa, desapacible y descontentadiza sin saber exactamente de qué ni por qué, Andréïda se retiró a su habitación.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo XI

El interés de Andréïda por el desconocido a
quien podía ahora ya distinguir con un nombre, el
del doctor Raúl, cesó, o por lo menos creyó ella que
había cesado.

Con afán se dispuso a escribir su primer artículo para el periódico, que de manera más apremiante
solicitaba su colaboración, y lo hizo con aquella facilidad suya que en la mañana había notado faltarle. Al cabo de tres horas escasas salió de su péñola,
que de tal tenía solo la aguda punta, una acabada
pieza literaria en la que lo eran todo las palabras
y bien poco el espíritu; pero que, por lo mismo, placía en extremo a su temperamento de cerebral.

Satisfecha de sí misma procedió a pulir las cuartillas, con esmero de artífice florentino y a pasarlas
en limpio en su diminuta máquina, anunciada pomposamente como silenciosa por una fábrica americana, pero que, en honor a la verdad, solo hacía un
poco menos ruido que el acostumbrado. Una vez colocado el clip para sujetar las cuatro hojas reglamentarias, las encerró en un sobre y decidió ponerlas personalmente en el correo. Era un buen pretexto para estirar las piernas y vagabundear un poco por el pueblo, hasta la hora de cenar.

Comenzaba a anochecer. Y, llegada que fué
al correo, enderezó sus pasos hacia los tenderetes, de
los que estaba plagada la colonial placita tranquila
y que en aquella hora era la parte más iluminada
del pueblo, pues que contribuían al deficiente alumbrado sendos mecheros de petróleo despidiendo negras columnas de humo e impregnando el ambiente
de un olorcillo acre y caracteristico, todo lo cual
contribuía a que, en la claridad tenue y profunda,
la empedrada calle se hundiera dos siglos atrás.
Andréïda no hubiera extrañado mucho el oír a sus
espaldas el tintineo sonoro de sables toledanos chocando en los pedruscos, o también el percibir, en
cualquiera encrucijada oscura, media docena de sombras embozadas en amplias y nobles capas españolas, charlando animadamente.

En aquella hora propicia a las bellas imaginaciones en la que el cielo se hace ligero y sutil antes de encapotarse con las densas sombras nocturnas, Andréïda experimentó un extraño goce en deambular por entre los “puestos” que desplegaban
con abigarramiento la vistosidad de su heterogénea
mercancía. Había allí de todo, cinturones de colorines, amplias bolsas tejidas, olorosas cajitas de madera de limonero incrustadas, y tantas cosas más
que la vista revolvía con agrado y que en el día eran
la gran atracción de “turistas”; pero que en la noche desplegaban su verdadero embrujo de cosas
elaboradas pacientemente con las manos de las que
tomaban un poco de vida, de la cual carecen, sin duda, los productos en serie con los que nos inunda el
extranjero.

Despertado en Andréïda su deseo de curiosear,
optó por dar vuelta a la placita y aventurarse por
una callejuela oscura que la llevaría a un cobertizo
en el cual le habían dicho encontraría, entre harapos oscuros y malolientes una magra viejecita, poseedora de las prendas más hermosas que en Taxco
podían adquirirse. Nadie sabía cómo ni dónde conseguía aquellos objetos la singular doña Cholita, ni
a quién pertenecían las manos de hada que elaboreban aquellas preciosidades, obra de una gran paciencia y una habilidad consumada; pero lo único
cierto era que nadie en el pueblo podía competir
con aquella mercancía milagrosa. Andréïda se dijo
que quizá, si la viejecita hubiese oído alguna vez, en
su remota infancia, hablar de la existencia misteriosa de los gnomos, con seguridad que hubiese satisfecho la curiosidad del pueblo atribuyendo la procedencia de aquellas laboriosas prendas a la colaboración fantástica de los diminutos seres que la gente razonable se empeña tontamente en negar su
existencia.

A punto ya de llegar en “ca doña Cholita” Andréïda vió a sus pies una estrecha zanja recién
abierta y al otro extremo un hombre que intentaba
también pasarla. Elevó los ojos y, a pesar de la penumbra, reconoció, al instante, al doctor Raúl.

La mano de él se extendió sobre el obstáculo de
la manera más natural a tiempo que su voz, de una
sonoridad profunda insospechada, emitía un ¿Me
permite? cuya sencillez obligó a Andréïda a extender casi inconscientemente la suya, rozando apenas
la del hombre. Como cumplía, al pasar por su lado,
Andréïda agradeció el galante gesto con una suave
sonrisa.

Y, como si hubiese mediado entre ellos una cita previa, el doctor echó a andar al lado de la joven,
admitiendo Andréïda, a pesar suyo, que le complacía aquel inesperado encuentro. Secretamente divertida sintió el encanto de aquel paseo casi íntimo
con el desconocido que había ocupado su pensamiento, todo aquel día, de manera tan arbitraria.

Bien pronto la charla se deslizó entre ellos con
una facilidad alegre, como si se hubiese tratado de
la de antiguos conocidos que se encuentran después
de prolongada ausencia.

Naturalmente fué él quien inició la conversación, diciendo:

—Desde lejos reconocí, al instante, el ritmo inconfundible de su andar—y añadió: —Aunque ahora tiene toda la serenidad que le faltaba anoche.

Antes de que Andréïda tuviera tiempo de replicar, prosiguió el hombre:

—He de decirle, sin embargo, que me gustó mucho más la turbación desvalida que tenían ayer sus
pasos… 

Y Andréïda se maravilló a sí misma de emitir
una explicación de su conducta.

— ¡Venía tan cansada! —se excusó ella.

—¿Y sus manos cómo siguen? —preguntó súbitamente él.

Sorprendida ella de la naturalidad con que él se
arrogaba el derecho de interrogarla, afirmó:

—Oh, ya están curadas, no fué nada grave, unos
simples rasguños —Y visiblemente desconcertada y
sin gran ilación terminó diciendo: —¡Me gusta mucho andar y anoche se me hizo tarde en la montaña!

Y como se encontraran ambos, precisamente, bajo el farol de la esquina, Andréïda las extendió con
sencillez, para corroborar su aserto. El las tomó con suavidad a tiempo que se encendía en sus ojos una
sincera admiración.

—¡Ah, qué bellas!

Avergonzada Andréïda de tan espontáneo elogio, las retiró con violencia, y comprendiendo él
su desacierto, exclamó:

—Dios mío, no quisiera que lo tomara a impertinencia; pero soy extremadamente sensible a la belleza de las manos y las suyas…

Con un gesto interrumpió Andréïda el nuevo
elogio que se venía encima y arrepentida de su movimiento y temiendo, al propio tiempo, que él la
juzgara quizá gazmoña optó por disculparse a su
vez.

—Soy un poquitín vanidosa —dijo —y me disgusta mostrarlas ahora que están tan terriblemente morenas…

Esta vez la suave disculpa de ella tuvo la extraña virtud de disipar la momentánea tirantez establecida por la irreprimible exclamación admirativa
del joven doctor, quien con una inflexión juguetona en la voz grave y alentado por la nueva actitud
de Andréïda, rechazó la afirmación de ella.

—¿Morenas?, ¡qué va!, acaso doradas, y es que,
con toda seguridad, las ha mirado el sol como debió
mirar a la Sulamita del cantar de los cantares; pero, no por eso, son menos hermosas.

Halagada Andréïda por la delicadeza atrevida
de la galantería no reparó en que aceptaba, sin reparos, aquel tributo elogioso, sobradamente íntimo
y un tanto indiscreto.

Internáronse por callejuelas y más callejuelas,
haciéndosele difícil a Andréïda el ajustar su paso
regular y metódico al de él, cuyo compás variaba a
capricho, obedeciendo al cambio de sus ideas; por
lo que tan pronto se hacía precipitado y nervioso como se convertía, al rato, en lento y perezoso.

En el transcurso de su conversación se sorprendió Andréïda de encontrar tantos puntos de concordancia en pensamiento, así como en sus mutuos gustos particulares.

Por ejemplo, él había afirmado, de manera rotunda, el que le gustaba enormemente Xochimilco,
a pesar de su olor a fritangas, su mala música y la
asiduidad terca y molesta con la que los indígenas
asaltan al paseante, contagiados de un comercialismo que les es ajeno. De inmediato, ella se había
apresurado a añadir, haciendo caso omiso de la reserva habitual que a sí misma acostumbraba imponerse al vedar su pensamiento y con mayor razón si
había que externarlo a un desconocido, diciendo:

—Adoro Xochimilco, porque el cielo está allí en
todas partes, en las copas de los huejotes ancestrales de estructura exquisita, en las aguas tranquilas
de los canales, en el colorido detonante de los claveles que esmaltan de sangre española las chinampas floridas…

Y una vez rota la reserva de Andréïda fué aún
más lejos e interrogó directamente al amigo reciente.

—¿Ha ido usted en domingo? —y, al observar la
sonrisa expectante, se apresuró a añadir: —Si, en
domingo; pero no a los canales, sino al mercado, a
la iglesita…

Secretamente divertido por el calor que ella había puesto, por primera vez, en interrogarle, afirmó
un poco maligno:

—¡Cómo no! Me encanta mezclarme en aquellos
grupos domingueros, risueños y voraces que escupen caña de azúcar y hacen derroche de colores, de
caderas y de flores.

Calló Andréïda y se cuidó mucho de hacer saber el que a ella le gustaba ver únicamente en las
muchedumbres su calidad de masa, de elemento dúctil para servir a fines precisos.

Arribaron a la placita, punto imprescindible de
partida y meta, al mismo tiempo, de todo paseo en
el pueblo.

Un poco fatigada Andréïda y con el cuerpo resentido todavía por la caminata y emociones experimentadas la noche anterior, tomó asiento en uno
de los duros y simpáticos bancos, que no faltan nunca en nuestras placitas de pueblo, teniendo a su espalda el templo soberbiamente iluminado ahora por
la luna.

Su nuevo amigo permaneció de pie enzarzalado
en una charla incansable, como si hubiese tenido los
minutos contados para vaciar todo lo que andaba
a saltos y brincos por su pensamiento.

Andréïda lo examinaba en silencio, afirmando
su primera impresión de la noche pasada. El hombre era, en verdad, la encarnación vibrante y más
hermosa de Vida. No había en él nada artificial, nada estudiado. Toda su persona era de un encanto
desnudo y sencillo. Fluía la naturaleza de todo su
ser como un manantial inagotable de aguas claras
y poderosas, extrañamente fecundas. Su arrogante
estatura de bien equilibradas proporciones era un
prodigio de viril justeza en los miembros. La hermosa frente rebelde, de lineas imperiosas y como
azotadas por el viento de las grandes alturas, tenía toda la majestad de los fuertes. La boca firme
y carnosa parecía estar hecha para emitir voces de
mando y también para claudicar en una caricia ardorosa. Los hermosos ojos grises, punteados de oro,
tenían encendido, en el fondo, una irresistible llama de alegría de vivir. El ademán elegante y apasionado al que no podía pedirse la sobriedad helada del nórdico, pero que, en cambio, sabía de las
grandes expresiones viriles, cautivada con la amplitud de su gesto que magnificaba la idea. Todo él
irradiaba virilidad magnificente y, ahí estaba, por
último, el mentón inflexible de los grandes voluntariosos a cuyo embiste acerado se doblegan los hombres y los obstáculos. Todo en él era una suprema
explosión de fuerza, de vida y de inteligencia, y Andréïda se dijo que, además, tenía ingenio, una brillante imaginación y una originalidad de pensamiento que le agradaba, sin dejar lugar a dudas.

Y como si él hubiese adivinado el último pensamiento de la joven, calló de pronto.

La suave claridad lunar idealizaba a Andréïda,
prestándole una belleza etérea al acariciar la delicadeza del rostro que el afán de superación de la joven había sellado con una expresión profunda, sin
que se alterase, por ello, su gracia juvenil.

El doctor Raúl sentía la locura de las bellas formas y la contempló como si la viese por primera
vez, constatando con delicia la pureza sensual de
aquellas líneas graciosas, el perfil atrevido del busto, la firme morbidez de los brazos y de las caderas, los finos tobillos, la brillantez gloriosa de la cabellera, la suavidad soñadora y profunda de los bellos
ojos, la sombra misteriosa de las largas y sedosas
pestañas y acabó diciéndose que ninguna mujer, hasta ahora, había satisfecho su ideal de belleza, tan
plenamente, como su nueva amiga. Precisamente a
él que tenía atrás una verdadera cadena insubstancial de mujeres hermosas, cuyo roce le había dado el
convencimiento de que inspiraba, a primera vista,
una predilección en el pensamiento de ellas, cosa que
le había tenido siempre sin cuidado; pero que ahora, por primera vez, le preocupaba, temeroso de
que su ascendiente no tuviera el mismo efecto sobre aquella criatura privilegiada. Temor que en la
comida de al mediodía se había traducido en una
ficticia indiferencia, especie de defensa subconsciente a posibles futuras angustias morales.

La suave garganta desnuda de Andréïda resplandecía bajo la penumbra lunada de los árboles
con una ardorosa dulzura. Los ojos de él se clavaban, de manera irresistible, en el latido delicioso de
las venas que se marcaba, muy ligeramente, bajo
la piel maravillosa de finura y blancura. Y, de
pronto, la hizo suya con una mirada envolvente y
cálida que traspareció, a pesar suyo, el fondo brutal e indudable de su deseo de hombre.

Sintiéndose acariciada por el pensamiento de
su nuevo amigo Andréïda entornó los ojos un momento, incapaz de substraerse a la fuerza vital que
la envolvía; pero no tardó en reaccionar al sentir,
en el misterio profundo de su ser, un choque desconcertante que la obligó a estremecerse. En aquellos instantes tuvo la visión brusca de una sumisión absoluta de sus nervios a aquel conocido de unas horas. Imaginó, en su interior, el derrumbamiento que
su claudicación provocaría en aquella ficción de su
inteligencia, elaborada con tan grande voluntad y
a costa de tan gran esfuerzo y que llenaba la finalidad única de su propia existencia. Con un leve sacudimiento de su preciosa cabecita se dijo riendo
que era arriesgado, muy arriesgado, el interesarse
por alguien de una personalidad tan fuerte y avasalladora como la del doctor Raúl, su nuevo amigo.

Y reaccionando, por completo, al embrujo de la
noche y del hombre se irguió con firmeza, a tiempo que exclamó:

—¡Es ya tarde…! —y la suave garganta moduló la más deliciosa de las risas.

El sonido armonioso de la injustificada risa de
ella desconcertó visiblemente al joven doctor, quien
se limitó a balbucear algo incoherente.

En silencio emprendieron el regreso, no sin que
él hiciera esfuerzos por reanudar nuevamente la
conversación, sin lograr otra cosa que balbucear conceptos vulgares. Sin embargo, aquella inseguridad
desfalleciente de la voz del hombre acarició a Andréïda más seguramente que las propias palabras.
Convencida de su triunfo sobre la masculinidad
de él, fué ahora ella quien llevó el peso de la charla.

En la esquina del hotel, cuadra y media alejado de la placita, se detuvo Andréïda para contemplar una vendedora de fritangas que guarecía su
olorosa mercancía en una especie de cajón acristalado. El rostro de la mujer surgía, de entre las
densas sombras, enrojecido por el fuego del “comal”
y la inexpresiva indiferencia de su actitud inspiró
a Andréïda una pregunta.

—¿Se ha fijado usted en que nuestro verdadero
pueblo, cuando se dedica al comercio, no hace nada
por atraer clientela?

—Es verdad —asintió el doctor Raúl —no sabe
del ofrecimiento rastrero e insistente del judío. Y
es que, indudablemente, nuestro pueblo, es un pueblo de resistencia, de incalculables reservas; pero
no de lucha agresiva, y además, ha olvidado ya el
ímpetu nómada de sus ancestros.

—Es por ello que nos absorbe el extraño —musitó Andréïda.

Un poco más adelante, a la puerta de una taberna, se detuvieron los dos jóvenes, subyugados
por una bellísima melodía anónima que un indígena
arrancaba dulcemente, pasando un rudimentario
arco de hebras de “ixtle” sobre un no menos rudimentario violín de indudable factura casera. Era
aquel canto, en la noche tranquila, como una demostración sonora y magnífica del México creador y divino.

El doctor Raúl reanudó su observación como si
no hubiese mediado entre ella la pausa sonora de la
canción.

—Guay del extraño el día que se decida a vivir
lo nuestro plenamente…

Los dos jóvenes rieron, deseosos de sacudirse su
repentina seriedad que mal sentaba a sus recientes
relaciones de amistad y, sorprendidos de la rapidez
de su paso, se encontraron, de pronto, frente al hotel.

Entonces, Andréïda, tendió al hombre su mano
con una franqueza instintiva y desacostumbrada
en ella. Fué algo así como un sello fraterno entre
los dos. El se limitó a estrecharla en silencio, un
silencio expresivo que parecía estar en espera de
un no se qué definitivo, que autorizara una mayor
intimidad en el futuro.

Ninguno de los dos pronunció más palabras.
Con dulce firmeza Andréïda retiró su mano de entre las manos cálidas del hombre y, volviéndole la
espalda, corrió con ligereza escaleras arriba y fué a
encerrarse en su alcoba.

\* \* \* {.espacio-arriba1 .centrado}

El se quedó ensimismado, contemplando sin ver,
los brillantes escalones encendidos por donde había
ascendido ella. Y pensó que la joven era la obra de
arte más bella y maravillosa que le había sido dado
a él, enamorado impenitente de la belleza y la vida, contemplar. No se ocultó a su aguda percepción
y gran sensibilidad el que precisamente por ser eso
mismo, una obra de arte, producto laborioso, nunca espontáneo, logrado con el concurso de todos los
refinamientos de la cultura tanto espiritual como
física, era algo así como una obra de todos y también y en consecuencia, goce estético para todos. Sus
ojos de hombre habían sabido sorprender en el rostro de Andréïda esa especie de indiferencia anómala de la virgen sin deseo, que se ama generosamente
a sí misma y cuyos labios ignoran el cálido sabor de
la vida. {.espacio-arriba1}

Lejos de que tal convencimiento desanimara el
súbito entusiasmo del joven doctor, se propuso ser
él, el afortunado mortal encargado de mostrarle el
camino de la vida fecunda.

Por primera vez, su espíritu movible, ávido de
prodigarse, tuvo una meta fija, la de conquistar a
Andréïda para sí. Como todos los seres pletóricos
de vida había desbordado sus ímpetus en el cáliz de
todas las flores puestas a su alcance, empujado por
su propia inquietud indomable. Sin embargo, muy
en el fondo de sí mismo, reconocía el que, sin excepción alguna, sus retrocesos le habían dejado siempre un salobre gustillo desagradable en la boca y
un reblandecimiento defraudador en sus súbitos y
frecuentes entusiasmos. Esto había dado lugar a
que sus numerosos amigos lo juzgaran siempre como inconstante veleta de un egoísmo atroz. Y era todo lo contrario.

Desde pequeño la vida había sido extremadamente pródiga con él. La simpatía irresistible que
irradiaba su ser, de la manera más natural, había
comenzado por conquistar a sus propios progenitores, quienes, desde la infancia, lo habían distinguido
y favorecido entre el resto de sus hermanos con un
afecto muy particular. Más tarde, esa misma simpatía natural había obligado a los profesores a facilitar sus estudios y, por lo que respectaba a su carrera, ésta había sido una serie de triunfos brillantísimos logrados sin el menor esfuerzo. Muchas veces, allá en lo íntimo, había sentido vergiuenza de
triunfar sin afanes, allí donde un compañero había
puesto la más ardua y firme de las dedicaciones. El
doctor Raúl desconocía, por completo, la doctrina
de la perfección por el sufrimiento, y no había sido
precisamente el dolor quien le enseñara el magnifico sentido de la vida.

Todo su ser era la más rotunda afirmación del
privilegiado con que, en veces, la Naturaleza gusta
de demostrar al hombre, lo iluso y lo inconsistente
de su soberbio ideal igualitario.

Tras la aparente ligereza cristalina de su actitud para con la vida había una profunda y formidable alegría de vivir arrolladora, cuya firmeza se imponía, rebelde a todo, y era como un soberbio torrente imposible de desafiar.

Hasta hoy había visto en el amor una embriaguez breve y necesaria al equilibrio físico del que
era un orgulloso adorador; pero que siempre había
dejado, en el fondo de su ser, un rastro acre, un humillante disgusto interior y muchas veces se había
llegado a preguntar con una preocupación un tanto risueña, si existirían, en realidad, amores en que
se abismaran dos almas en una fusión completa. Y
esta noche, de pronto, al contemplar hacía poco, en
el banco de la placita, aquella deliciosa figura femenina cuya misteriosa suavidad lo había conquistado
en unas horas, había creído, por fin, en tal posibilidad.

El doctor Raúl se retiró a su habitación y, por
primera vez, su hermoso rostro viril se iluminó, al
dormirse, con una sonrisa de la más franca y esperanzada dicha.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo XII

A la mañana siguiente, el primer pensamiento
que tuvo Andréïda al despertar fué el de huír, e
de arrancarse a aquella incipiente amistad que de
tal manera trastornaba y torcía el sentido todo que
ella se había propuesto imprimir a su vida.

solo unas horas habían bastado para demostrarle la fuerza de aquella influencia que a los dos
había conmovido, con una misma emoción, la noche anterior en la placita, profundamente dulce del
sin par Taxco, y que amenazaba ligar sus vidas no
sabía ella cómo ni quería detener su pensamiento
en imaginarlo.

Por un momento pensó también si todo aquello
no sería una ficción de su sensibilidad exacerbada
por aquellos días de molicie y de soledad. Y le asaltó el recuerdo de la indiferencia de él a la hora de
la comida y se dijo que quizá toda la emoción del
amigo, solo sabía despertar en las horas nocturnas.
Posiblemente, hoy mismo, ocurriría otro tanto al encontrarlo, de nuevo, en el comedor. Se limitaría a
saludarla con un leve movimiento de reconocimiento y a eso se reducirían sus relaciones. Por inconsecuencia, este pensamiento difundió una sutil honda depresora que escalofrió su bello cuerpo.

Su gran temple, su enorme energía parecían distenderse cansados de vibrar continuamente en el vacío y, sin embargo, se imponía a su espíritu la necesidad de defenderse de algo tan vago y tan abstracto como era la seducción de una voz y una imagen.

Con prisa febril comenzó a vestirse y procedió
a empaquetar sus cosas a tiempo que ordenaba a la
muchachita de servicio, que como todas las mañanas había entrado la bandeja del desayuno, el que
suplicara al dueño del Hotel contratase un automóvil para ponerse, cuanto antes, en camino hacia México, costase lo que costase.

Mientras esperaba una respuesta, ya terminados sus breves arreglos de marcha, Andréïda recorría la habitación con paso agitado, reprochándose
en voz baja aquella actitud de su espíritu que reconocía la obligaba a dudar de sí misma. Le anonadó el
solo pensar que pudiera perder su gran fe y su orgullo de mujer superior, con lo que, indudablemente,
se hundiría todo en torno de ella.

Después de haber resuelto su partida se sentía,
en verdad, más animosa; pero esto mismo la hacía
avergonzarse como de una huída que en el fondo no
era otra cosa que una derrota, una tácita confesión
de su miedo a la seducción de un ser, lo que demostraba que su alma no había conquistado, del todo,
aquella fuerza de la cual tanto necesitaba para el
cabal triunfo de su extraño propósito; por lo que
existía un punto débil que la alejaba de ser la verdadera Andréïda, la Mujer Nueva, Pero como tenía
una infinita necesidad de hallar disculpa razonable
a su movimiento de eludición, acabó por decirse que
no podía tacharse a nadie de cobarde cuando huía
de una tempestad y buscaba refugio a un peligro
superior a sus fuerzas. Y aquello de lo que ella huía
no era otra cosa que una tempestad. La tempestad de sus sentidos que se hacían presentes a sus veinticuatro años.

Una llamada discreta la obligó a abandonar sus
razonamientos y fué a abrir. En la puerta estaba el
propio dueño del hotel que le anunciaba la imposibilidad de conseguir un solo coche que deseara emprender la marcha a México inmediatamente.

Aquella no prevista contrariedad exasperó a Andréïda, y observando el hotelero el estado de excitación de la joven, se ofreció a hablarle a un matrimonio americano que se disponía a salir en aquellos instantes, para ver si accedían a cederle un
asiento.

Unos minutos después la joven criadita comunicaba a Andréïda que había tenido éxito la comisión del hotelero.

Con un suspiro de alivio sonrió Andréïda, rechazando lejos el peso de su preocupación y, por ensalmo, renació su admirable confianza en sí misma.

Por otra parte, el matrimonio americano que tan
gentilmente se prestara a llevarla resultó de lo más
simpático y como los dos tenían un gran deseo de
hablar en su idioma con tercera persona, todo el camino fué charlar de sus impresiones sobre el _wonderful old Mexico of romance_, por lo que Andréïda
apenas si dos o tres veces pensó en el joven doctor
con una especie de tristeza vedada que no ofrecía peligro en la añoranza. Y así fué cómo, cuando descendió del automóvil frente al hotel en el cual residía
con Nelly, aproximadamente a las cuatro de la tarde,
se sintió de nuevo en toda la fuerza de su bien afirmada personalidad.


Habían vibrado tanto sus nervios en unos días
que la reacción pasiva no se hizo esperar, y desde
aquel minuto su estancia en Taxco le pareció un mal
sueño que había vivido quizá un ser gemelo al suyo;
pero, por completo, distante a su íntima complejidad anímica. Y acabó por decirse que, en adelante,
estaría al abrigo del peligro desconocido, persuadiéndose que, con aquel alejamiento no la volvería a
asaltar el íntimo fervor que animó, unos minutos,
su existencia.

Todavía había de trastornar su serenidad, recientemente reconquistada, una nueva contrariedad.
Disponíase a subir al departamentito que ocupara
con Nelly antes de sus obligadas vacaciones, cuando
el botones, con un sonoro señorita Andréïda, se le
acercó para comunicarle el que Nelly había liquidado la cuenta, cambiándose muy cerca de allí, y añadió, el chico, oficiosamente, la dirección y exacto
número de la casa.

Aquello provocó en Andréïda una sorda cólera
en contra de Nelly que ni siquiera había tenido la
atención de comunicarle sus planes, y sintió la tentación de tomar para ella sola un nuevo departamento allí mismo en el hotel. Sin embargo, fué más
fuerte su deseo de desahogar su resquemor inmediatamente con la amiga, por lo que optó por pedir
un “libre” y dirigirse a la dirección indicada.

Encontró a Nelly afanada en colgar personalmente alegres cortinajes, y dándole los toques finales a la nueva instalación. La belleza del jardincito
de la casita y los muebles flamantes desarmaron la
cólera de Andréïda, y más que todo ésto la bienvenida dichosa de los ojos de la amiga y sus primeras
palabras.

—¡Dios mío, yo hubiese querido que encontrases todo arreglado, de ahí que no te dijera nada para darte una sorpresa! —y con apresuramiento, añadió: —Esto ya está de acuerdo con tu nueva posición. ¡Por fin, tenemos hogar!

Los ojos de Andréïda adquirieron un brillo húmedo y abrazó en silencio a la admirable Nelly, sin
confesarle la injusticia con que la había juzgado minutos antes.

Del fondo de la encantadora casita surgió la voz
cordial de Torritos dando la bienvenida a Andréïda.
Era éste un viejo periodista que casi sin sentir se
había colado en la vida de las jóvenes, y por su espíritu servicial y deseoso de agradar habíase convertido en compañero inseparable de Nelly. En aquellos momentos, según comunicó a grandes voces, se
ocupaba en armar una “chiva”. Lo que sería exactamente aquello que él denominaba con despreocupación “chiva”, no lo supo Andréïda.

Como todos los solterones, a la zaga tardía de
un imposible hogar, había encontrado en la compañía inteligente de aquellas dos jóvenes, un remedo
de cálido refugio que entibiaba sus miembros ateridos de hombre privado de ternuras. Nelly ejercía
sobre él una suave seducción que nunca se había detenido en analizar, contentándose con la dulce compañía que tenía la virtud de prestar interés a esas
horas vacías que siempre deja el trabajo.

Por su parte, Nelly y Andréïda habíanse ido
acostumbrando insensiblemente a su útil compañía,
confiándole de cuando en cuando pequeñas molestias que recorrían la gama desde cobrar un giro,
echar una carta urgente al correo, hasta encargarle
de la compra de boletos para el teatro. Encontrando
aiempre en Torritos una cómoda aquiesciencia, todo
lo cual implicaba el que sacrificase él sus pequeños
gustos poltrones con una inexplicable alegría de hacerse útil.

A menudo, entablábanse entre Andréïda y Torritos largas discusiones que el segundo calificaba
capciosamente de soberbios choques entre dos potencias, denominando la de la joven potencia del
Norte y la suya, del Sur, Nelly se limitaba a escuchar las inacabables polémicas interviniendo únicamente cuando éstas tomaban rumbos ingratos con
el objeto de suavizar aquellas explosiones y poner
paz entre aquellos formidables adversarios natos.

Andréïda se dejó caer en uno de los amplios sillones del minúsculo _hall_ y fijó acusadoramente
su vista en Nelly, diciendo:

—¡La prudente, previsora y metódica Nelly haciendo locuras! A ver, explícame ésto. ¿De dónde
has sacado dinero para esta instalación?

Intensamente ruborizada la aludida inclinó la
cabeza y con cierta incoherencia y vaguedad enumeró las razones que había tenido para hacer aquello que Andréïda calificaba de locura. Sin embargo,
se reservó entre todas, la principal de ellas, que no
era otra que la de alejar a la amiga de la mortificante curiosidad del hotel.

Convencida Andréïda, en apariencia, de las ventajas que reportaría el tener una casita para ellas
solas, insistió aún en conocer de dónde había obtenido Nelly el dinero necesario. Esta vez fué Torritos el encargado de satisfacer su curiosidad.

—¡Cómo va Andréïda! —Fué su saludo cordial
al entrar en la habitación y nada indicaba que hubiese dejado de ver a la joven un mes largo. —Pregunta usted, —continuó diciendo con parsimonia— de dónde ha salido todo ésto? —Un ademán circular de su delgada mano abarcó la casita. —Pues nada más sencillo, ha surgido al conjuro de dos insoportables parlachines que se llamaban a sí mismos, pomposamente, agentes vendedores, entre paréntesis nueva plaga de nuestro sufrido México, y que convencieron, con extraña facilidad, a nuestra admirable Nelly de las ventajas maravillosas de ese sistema de abonos con que estos nuevos fenicios exprimen la sangre y la vida de nuestros modernos pueblos incautos.

Ante esta salida graciosa y rebuscada de Torritos, Andréïda no pudo hacer otra cosa que reír de
buena gana.

—Ahora es necesario —volvió a la carga Torritos— el que vuecencia se digne dar un vistazo de
aprobación a su palacio.

Andréïda estuvo tentada de rechazar la proposición; pero sorprendió, a tiempo, en los ojos de Nelly,
una lucecita de alegría que la obligó a fingir una
curiosidad y entusiasmo que estaba lejos de sentir.

Recorrieron los tres la casita entre las hipérboles chuscas que soltaba Torritos de cuando en cuando. Constaba la casa de dos piececitas con un baño
en medio, un comedorcito ligado al hall y una cocinita todavía más pequeña. Al pasar Andréïda por
la alcoba que Nelly le había destinado, sus ojos adquirieron un brillo húmedo al observar el esmero y
sencilla elegancia de aquella habitación, cuya evidente superioridad sobresalía entre todo el resto
modesto de la casita. La admirable Nelly recibió un
beso en la mejilla que dejó satisfechos, del todo, sus
afanes y el sacrificio que implicaba aquéllo.

Regresaron a la salita, y Torritos deseando desvirtuar la honda emoción que agitaba a las dos amigas, reanudó sus explicaciones en el punto en que
las dejara.

—Con seguridad que usted, Andréïda, sér superior, ignora la ruín mecánica de este sorprendente
sistema de abonos que atornilla en el potro a los modernos cuyas necesidades y ambiciones sobrepasan
sus medios de cubrirlas, y que nos ha sido importado de su Estados Unidos —el _su_ lo subrayó malévolamente el periodista —con otras no menos funestas importaciones que amenazan destruir nuestro
equilibrio. Pues bien, es mi deber informarla que el
vendedor exige a su víctima un llamado “enganche”,
grillete diría yo, que cubre con celosa exactitud el
valor intrínseco de lo que venden. ¡Oh, admirable
previsión mercachifle!

Torritos sentía una oscura prevención contra el
comercio en general, actividad que tachaba de vergonzosa e indigna. Solía también calificar al comerciante de parásito que chupa la sangre del pueblo extrayendo una utilidad de una inflación artificial, puesto que su tarea es netamente estéril. El
arma favorita del periodista para atacar al comerciante era el referirse a la criminal actividad que
el acaparador desplegara en el ciclo heroico de la Revolución Francesa, afirmando Torritos que así como
entonces había hecho fracasar, en sus efectos, aquel
magno movimiento ideológico creando la funesta
casta burguesa, así también estaba convencido que
se disponía a hacer fracasar, en nuestros días, el
más serio intento emprendido por un hombre, sobre
todas las cosas honesto, en la historia del México
Nuevo. Esto y más se disponía a repetir cuando Nelly interrumpió al periodista diciendo un poco avergonzada:

—Yo te lo diré, Andréïda, ya se a dónde va a
parar Torritos.—Y de un tirón, como un inacostumbrado y friolero bañista que se tira al agua con una
súbita resolución desesperada, terminó Nelly —El
enganche lo he cubierto con tu reserva en Pensiones
y con mis ahorrillos de estos últimos meses. La última parte de la frase apenas si fué percibida por
Andréïda y Torritos, tal fué la suavidad de la muchacha al pronunciarla, casi con el deseo de que no
fuera escuchada.

Fingiendo Andréïda una indignación que desconcertó a Nelly, por el solo gusto de tranquilizarla
con una pequeña caricia, dijo:

—Pero has podido tú hacer eso, sin mi permiso?

Torritos se apresuró a poner en guardia a Nelly
con un guiño de sus pequeños ojos brillantes y un
chasquido peculiar de su lengua sobre los dientes.
Nelly levantó la vista y, al observar la risa que retozaba en los labios de Andréïda, sonrió a su vez y
acto seguido se excusó diciendo que iba a preparar
un poco de té para hacer desaparecer el cansancio
del viaje que se advertía en el rostro de Andréïda.

Andréïda y Torritos quedaron solos. Por un momento la vista del periodista se clavó en la joven
con aquella expresión fija, muy suya, que acababa
por desconcertar siempre a su más sereno interlocutor.

—¿Qué desea saber, insoportable curioso? —Fué
la pregunta con la cual se defendió la joven de aquella mirada inquiridora.

—¿Yo?… Nada —y con un gesto fino y burlón
eludió Torritos el ataque agresivo que veía venir.

Ahora tocó a Andréïda el examinar al periodista. En el fondo y a pesar de sus frecuentes escaramuzas, debido a que sus razonamientos solían partir de puntos de vista encontrados y también a un
afán eterno de contradicción en Torritos, la joven
sentía por él un afecto tierno y la más sincera estimación. Aquilataba, en lo que valía, su fino y cultivado espíritu, su sensibilidad exquisita, la naturalidad elegante de sus ademanes ponderados que no
eran sino reflejo de la elegancia misma de su pensamiento. Le gustaba, asimismo, a Andréïda su innata pulcritud en el vestir, y eso que ella no le había
conocido nunca otro traje que un flux negro, siempre perfectamente planchado y con las rayas del
pantalón impecables, aunque, eso sí, un tanto lustroso en los codos y en la parte de atrás de la americana, por el uso. Y cosa extraña, aquel brillo ominoso no desvirtuaba la primera impresión de elegancia y buen gusto. Por otra parte, la fisonomía
de Torritos era incuestionablemente fea, bien es
cierto que era de aquellas a las que el vulgo se apresura a añadir el consabido “pero simpática”. De
toda su persona desprendíase un sutil cansansio
que no radicaba en la incipiente calvicie que ya despoblaba ambos lados de su frente, dejándola aún
más despejada, sino de algo más hondo, de algo
inexplicable que hacía pensar en que era uno de esos
seres que se adivina debieron nacer ya fatigados.
Lo más extraordinario de todo ésto era que se trataba de un ser en exceso indolente, que procuraba
hacer siempre lo menos posible y que si ocupaba un
puesto mediocre en la vida no era por falta de excelentes cualidades para triunfar, ni porque no se le
hubiese presentado oportunidad de hacerlo, sino sencillamente porque no había querido hacer el esfuerzo necesario y le había faltado la perseverancia indispensable.

Discutiendo una vez Andréïda con él sobre la
única razón decente que podía argüirse como causa
directa de aquella suprema indolencia que paralizaba a menudo su acción, había dicho la joven que
quizá podría encontrarse en la suavidad paradisíaca del clima de México. Andréïda misma, a su llegada a México no había podido menos que extrañar
el deambular de la gente por las calles y de manera
insensible había comparado las muchedumbres que
circulan, por ejemplo, en la esquina de 16 de Septiembre y 5 de Febrero o también las que discurren por San Juan de Letrán, con las que suelen
agitarse en cualquier población americana, en Dallas o Houston, para no citar New York, que es excepción en la propia Unión Americana. Y había
llegado a la conclusión de que la lentitud e indecisión
con que se mueve la gente de la ciudad de México,
como si no tuviese un punto determinado adonde ir
y desconociese del todo la premura de las exigencias
modernas, solo podía describirla gráficamente, el
recién llegado, como un fenómeno visto a través de
la visión retardada de una cámara lenta.

Estas observaciones que un día Andréïda comunicase, llena de extrañeza, a Torritos, las había recibido el periodista con una sonrisa de superioridad
y én seguida había hecho el elogio de la indolencia
como el estado de la felicidad completa sobre la
tierrá. Y había jugueteado peligrosamente con conceptos como el siguiente: Que el trabajo no había
dejado de ser la maldición del hombre desde los
tiempos remotos y oscuros en que unos cuantos más
hábiles habían hecho uso de la superstición para esclavizar a sus hermanos menos despiertos. El mismo, Torritos, sentía un indecible aborrecimiento por
el trabajo y, sin lugar a dudas, había notado, siempre, que nunca se sentía mejor que cuando no hacía
nada. Con grave seriedad afirmaba que era algo
desagradable que había que empujar a un lado, y
juzgaba suprema tontería del Mundo afanarse tanto
y exprimir a las masas en aquella forma. En aquella ocasión se había reído también, de que el proletariado pudiera pensar que había hecho la gran conquista al limitar la jornada de trabajo a ocho horas
diarias, desarrollando Torritos una tesis teóricamente convincente sobre que, a lo sumo, bastarían
unas cuatro horas del trabajo del hombre para satisfacer las necesidades del mundo. A raíz de ésto
rabía edificado en seguida, un ingenioso ensayo de
república, un tanto disolvente, en la que se emplearían las horas de la manera más agradable para el
cuerpo y el espíritu, liberados sus ciudadanos de la
torpe esclavitud de una necesidad falsa que solo
había servido, a su entender, para amontonar un
metal estúpido e inútil como el oro en manos de burgueses gordos con almas de piratas y de Torquemadas. A renglón inmediato, había calificado el buen
Torritos de tendenciosa la creencia que se había
intentado inculcar, a últimas fechas, en el mundo civilizado, acerca de una ficticia sobreproducción, presentando a la máquina como enemiga del hombre,
cosa que el periodista decía no poder creer, mientras los periódicos trasmitiesen noticias de rusos
muriendo de hambre, de chinos, javaneses y otras
razas condenadas a comer tierra a falta de otro alimento decente y apropiado, a más de que, sin ir tan
lejos, ahí estaba la propia población indígena de México con una alimentación básica de frijol y maíz
que desconocía el beneficio del azúcar y del trigo.
Todo ésto, recordaba Andréïda, lo había dicho Torritos a la manera magnífica que él acostumbraba,
dejando fluir las palabras a saltos mordentes, e imprimiendo una elasticidad furtiva a los conceptos
que acababan siempre por colarse en la conciencia
de sus oyentes, con la lisura nueva y exacta de lo
que se elabora viva y agudamente. Después había
hecho el elogio de la máquina, suprema conquista
del hombre, a reserva de que no estuviera en manos
de unos cuantos que la habían hecho instrumento
de esclavización. De ahí que urgía la necesidad inaplazable e ineludible de entregarla a los más, con
lo que de manera automática se pondría, en efecto,
al servicio del hombre, realizando así el progreso
técnico su genuina misión de perfeccionador moral de
la humanidad. Entonces, el periodista se había mostrado un entusiasta teórico de la máquina a la que
estimaba como elemento de progreso material, con 
cuya comodidad podía proporcionarse al cuerpo el
fácil desarrollo y cultura del espíritu. Pero, a espaldas de Torritos, podemos aquí decir impunemente, que no se preocupaba mucho por informarse del
último descubrimiento mecánico, importando más a
su atención el acierto literario de cualquiera de los
últimos libros de los cuales era un feroz devorador.

Rememoraba Andréïda que aquel famoso día había ella acabado por darle toda la razón, subyugada
por la belleza generosa de los razonamientos y también por las imágenes ingeniosas de que se había servido el periodista y cuya plasticidad siempre la maravillaba.

El silencio entre Andréïda y Torritos se había
alargado en demasía y amenazaba convertirse en
descortés por parte de la joven, por lo que, comprendiéndolo así, se apresuró a iniciar una de aquellas conversaciones de que tanto gustaba ella y en
las que acostumbraba fustigar la causticidad de Torritos, provocando un verdadero juego pirotécnico
de conceptos resplandecientes en el que el periodista
solía malgastar su inteligencia y admirable ingenio
al vaciar en bellas palabras toda la actividad de que
era capaz. Andréïda sabía de sobra que, llegado este.
momento, desaparecía del rostro inteligente de Torritos la melancolía que, como niebla habitual, acostumbraba cubrir sus agudas facciones irregulares.

Ningún tópico llenaba de manera tan perfecta
este objeto como el que se iniciaba solo con lanzar
una crítica velada sobre cualquier visible acto del
Gobierno, por lo que Andréïda se dispuso a inclinar la conversación por aquel rumbo, haciéndole una
pregunta equívoca.

—Mi excelente amigo —dijo—; ¿qué le ha parecido
la actitud contradictoria del Senado con respecto a
los líderes obreros?

Inmediatamente Torritos se transformó. La indolencia oxidada de su ademán se convirtió en gesto
violento. Las palabras se desbordaron de sus labios
en torrentes, adquiriendo una vivacidad sorprendente, un colorido mordaz, intensamente ingenioso
y que acabó por hundirse en loz bajos fondos de la
ironía, sin perder, por ello, su habitual plasticidad
elegante.

Era incuestionable el que Torritos sentía una
inquina especial contra el Gobierno y, para no ser
mal comprendidos, diremos que iba contra todo
lo que pudiese llamarse un gobierno. Importaba
bien poco que éste, en el pasado, hubiese tenido a la
cabeza un Porfirio Díaz o cualquier otro jefe más o
menos moderno del período revolucionario y también del ciclo institucional. Como un gran por ciento de ciudadanos mexicanos (¿será demasiado audaz o exagerada esta afirmación?) se creía capaz
de gobernar los destinos de la patria mediante nuevos y relucientes sistemas que no se molestaba siquiera en bosquejar. Esta inquina solía verterse
también sobre los políticos que destacaban en la hora, haciéndolos víctimas de una serie de invectivas
feroces y alusiones personales, dichas con volubilidad; pero profundamente amargas y maliciosas que
tenían, a veces, la sorprendente virtud de dar en lo
vivo.

Su epíritu de crítica, intensamente desarrollado,
se vengaba en palabras sediciosas, hechas chiste ingenioso, de cualquier error político, sarcasmo que
después rodaba por cafés y redacciones con el beneplácito de una docena de eternos insatisfechos y
frustrados como él mismo. Se excusaba de su manera de proceder diciendo que la sátira era el arma
más segura de la razón en contra de la potencia
bruta, la fealdad y la injusticia. Y es deber nuestro
hacer constar que, del espíritu del periodista, estaba
muy lejos la idea convencional y burguesa de la justicia. Todos sus amigos estaban de acuerdo en que
solía llevar demasiado hondo su causticidad, aunque reconocían también que, a los labios móviles de
Torritos, les era necesario el hablar por hablar, como a las plantas la lluvia.

Muchas veces Andréïda había pensado que Torritos era un hombre de oposición, dispuesto a arremeter siempre contra todo y contra todos y al mismo tiempo se había preguntado si no eran en México todos los hombres de oposición…

Había también otra particularidad en Torritos
que Andréïda se empeñaba en creer generalizada, y
era el uso que hacía y la forma como las palabras
viejas adquirían la fuerza de conceptos nuevos, dejando escurrir extrañamente la idea primitiva que
debieron haber expresado para sugerir, en cambio,
una realidad inédita de una fuerza aplastante que
surgía con la lisura de algo nuevo, vivo y mordiente. Ahí estaba, por ejemplo, el sentido singular y
arbitrario que Torritos aplicaba a la palabra revolución. En sus labios revolución era gobierno y gobierno debía ser revolución. La significación, a primera vista incompatible, para el extraño, de los dos
vocablo.s era la más sencilla, clara e indubitable de
las realidades para Torritos que, por encima de todo
y de él mismo, era mexicano y en el mexicano la
revolución se ha convertido en un deber cívico, en
una virtud sólida, hasta el punto de que si alguno
carece de espíritu revolucionario se le juzga inmediatamente antipatriota. Lo que después de todo resume, en el fondo, ese amor temerario y desesperado
por la libertad que también es un odio alerta y latente a los tiranos de toda especie que fustigaran
al pueblo por tanto tiempo, abusando de su credulidad y de su fe.

Torritos gustaba de definir la revolución como
un estado inmanente ai Gobierno, generador de progreso incesante y evolutivo, enemigo de contemplaciones que, en último caso no son más que una claudicación disfrazada de debilidad e indecisión, y que
debía marchar a pasos ftorzados, hacia el radiante
amanecer de los derechos de las masas. En menos
palabras: revolución, un movimiento en principio y
como resultado inmediato, una positiva actividad en
proceso de transformación. Esto era más o menos
lo que el periodista decía acerca de la definición mexicana de tal vocablo, Ahora, en cuanto a la significación de la revolución misma en la historia de
México, la resumía en términos tan claros y enérgicos que hacía olvidar a su interlocutor la magnitud,
humanamente inabarcable, del proceso. Solía verter desconcertantes afirmaciones, demasiado fuertes para ser discutidas y cuya exposición magistral
inutilizaba la más honda argumentación del mejor
intencionado polemista. Acostumbraba también decir, a propósito de la revolución que, en Rusia, se
había esperado todo de los obreros, de los hombres
de tierra baja, y que en México había sido hecha
por los hombres de tierra alta, por los campesinos y
de manera espontánea, ajena a demagogias y dialéctica, obedeciendo únicamente a una necesidad
honda de liberación y justicia, de dignidad humana.
Había todavía más, la nuestra, la mexicana, tenía,
por derecho indiscutible de prioridad, toda la fuerza
de lo original. De ahí que él, Torritos, se indignara
cuando dos o tres empachados de literatura judía
querían imprimirle interpretaciones extrañas. La
exaltación del periodista lo llevaba a decir entonces,
como una muletilla: “solo lo nuestro responde a
nosotros”, palabras de un sentido enorme, ante las
cuales, se inclinaba todo aquél que las oía.

Mas no era solo la palabra revolución la que
sufría un uso tan singular en las elucubraciones
brillantes de Torritos. Era de sorprender la forma en que empleaba vocablos como socialismo, cooperativismo, colectivismo, etc. Andréïda sabía que
cuando Torritos decía colectivismo —esa doctrina que
los Estados Unidos del Norte tienen como utopia
irrealizable —no podía tacharse al periodista de comunismo; pues nada era más contrario a su compleje anímico. Su especial individualismo era refractario, por naturaleza, al rasero igualitario que, como feroz absorbedor de la personalidad, amenaza
tragarse la iniciativa del hombre.

El colectivismo para Torritos era el estado ideal
de la sociedad por su cualidad de medio equilibrador y de defensa del individuo aislado en un mundo
de bajas competencias, que no parece tener otro fin
que destruirse a sí mismo. Por tal razón decía que
México, el más individualista de los pueblos (los
pueblos son individualistas cuando dueños de una
fuerte personalidad, ésta les impide ser un conjunto de moléculas sin cohesión ni enlace, dispuestas a
fundirse en un lodo informe y sin destino), podía
sentir, gustosamente, la fraternidad de unir a sus
hijos en el trabajo, siempre que el producto favoreciese por separado al individuo y conferme a su
particular esfuerzo. Sutil concepto de tal teoría que
Andréïda pensaba concordaba amablemente con la
ideología revolucionaria de Torritos que era demócrata en estilo.

Y como la palabra colectivismo, eran muchas
las que sufrían, en boca de Torritos, una interpretación completamente arbitraria y distante de su
verdadero sentido que asumen, o les corresponde, en
otras latitudes. Por lo que podía decirse que el periodista establecía conceptos nuevos y particulares a
México con palabras idénticas a otras significaciones. Al contrario del occidente europeo que se empeña en forrar conceptos antiguos con palabras nuevas. Tal fenómeno se debía en Torritos a que, en la imposibilidad de crear vocablos que respondieran
con exactitud al nuevo sentido que urgía expresar,
se contentaba con evolucionar y aun variar la definición de los viejos.

El léxico plástico y pintoresco de Torritos solía
también sufrir, por temporadas, el imperio de un
adjetivo cualquiera, siempre que éste fuera sonoro y
ninguno llenaba mejor el objeto que los de acentuación esdrújula. Por ejemplo, en los momentos en
que entramos en conocimiento con él se había aficionado grandemente a la palabra dinámico, decía
de un hombre que era dinámico, de un político que
era dinámico, de un gobernante, y así por el estilo.
Agotado el tema político que enzarzalara a Torritos en una parrafada apasionante, se hizo el silencio, de nuevo, entre los dos. El periodista que no
podía estar sentado cuando hablaba, por lo que tomaba siempre actitudes de “álter magíster” dirigió
sus pasos hacia el librero colonial que albergaba las
últimas compras de Nelly y se entretuvo en hojear
las novedades. Mientras tanto, Andréïda quedó observando la menguada espalda de hombros asimétricos, curvados hacia adelante que hablaban muy
claro de un ser que había labrado su vida inclinado
sobre un escritorio, formando su espíritu impacientemente, de manera autodidacta, por una necesidad
apasionada de elevación hacia formas superiores de
existencia.

Ahí estaba Torritos en su actitud favorita de
ratoncillo aficionado al papel impreso. Torritos, el
hombre apático y perezoso a ratos, violento y revolucionario en otros. Burlón siempre al hablar, hasta el punto de parecer no tomar en serio, ni siquiera, la muerte. Espiritual en ocasiones, incapaz de
respetar ni aceptar las ideas ajenas para las cuales
mostraba una intolerancia sarcástica. Despreocupado, inquieto, turbulento, con una movilidad que
le hacía esclavo de todos sus impulsos. Falto de disciplina interna, pero con un amor desmesurado por
la libertad, siempre que ésta le permitiese verter
toda su inquina sobre aquellos que pensaban de modo distinto al de él, con lo que anulaba, en sí, la
esencia intrínseca de la propia libertad. Amante de
divulgar sorprendentes teorías formuladas en frases virulentas, tenía, sin embargo, aquel espíritu
aparentemente disolvente, dos únicos sentimientos
sagrados: la veneración a su madre y el amor apasionado a la patria.

Su más grande orgullo era el de ser mexicano
de la cabeza a los pies, y no hubiera cambiado su
condición por la del ciudadano más privilegiado de
la tierra. Echaba pestes y centellas contra el español y sus defectos feudales de crueldad, de opresión,
de intransigencia fanática, de ignorancia orgullosa
y despectiva que tan admirablemente habían dejado
injertados en el criollo y en el mestizo, lo que hizo
posible una dictadura y un estancamiento como el
porfirista. Estancamiento para lamentar el cual,
decía Torritos, no había palabras bastantes que concretasen el mal irreparable de aquellos funestos
treinta años que habían detenido el florecimiento de
México, haciendo de él una trágica durmiente del
bosque.

Con una notable inconsecuencia, su odio al español no le impedía el conmoverse hondamente con
la actual tragedia española y mostrarse más partidarista de Valencia que el más exaltado de los madrileños, y si al caso venía, a propósito de lo español, recordaba con una sencillez orgullosa, casi infantil, al abuelo hispánico de luenga barba patriarcal que solía sentarlo, de niño, en sus rodillas con
una ternura brusca. Por lo que bien pudiéramos
decir que su tal odio era un mucho superficial y
más bien hecho de palabras que de sentimiento sincero.

Pero, por quien sí sentía un odio irreconciliable,
presto a desbordarse en hechos y palabras, era contra los rubios primos del Norte y más que nada, contra su insoportable petulancia y engreimiento sublevante de pueblo que se cree, entre todos, superior
y civilizado. En tales ocasiones solía lanzar Torritos una insidiosa pregunta: ¿Quién había hecho
grande a los Estados Unidos?; y él mismo se encargaba de contestarla al punto: La hez de todos los
pueblos del Mundo. Seres desorbitados en su propio medio, dispuestos a luchar en cualquier forma,
puesto que nada exponían y por delante tenían la
probabilidad de triunfar. Con una sorda ironía decía Torritos que a él le había sido más difícil identificar a un americano en New York que a un ciudadano auténtico, de esa nación, comprando un sarape en la esquina de Sanborn's, en México.

De esto deducía Torritos que el pueblo americano
era un conjunto heterogéneo de razas sin principios
de patria, de honor y familia, y con una sola deidad
al frente que al mismo tiempo hacía de único punto
de cohesión: el dollar. Americanos de nombre, cuya
ciudadanía forzada era solo el resultado temerario
de una disposición legal. Y se preguntaba el periodista: ¿ Dónde estaban, en aquella Babel, los primeros pobladores austeros, enérgicos e independientes
de los primeros tiempos? ¿Dónde se escondían los
descendientes de aquellos temerarios que habían
creado civilización aún a costa del exterminio sanguinario de las tribus guerreras y hostiles que estorbaban su expansión agrícola? Sin duda que el
verdadero espíritu americano había sido sofocado
por la avalancha extranjera que portaba el disfraz
legítimo de una falsa ciudadanía. Había algo más
que tampoco podía perdonar Torritos a la nación vecina y era aquella falta de escrúpulo en razones
económicas, muy inglés y muy judío, y que alimentaba su imperialismo hasta el punto de invadir el
derecho de los pueblos de Centroamérica y del Sur.
Y también aquella manera cómo hacían alarde de su
egoísta divisa “cada uno para sí”. Y su burgués
utilitarismo de filisteos. Y su liberalismo culpable,
barnizado de falsa democracia que había hecho posible el arraigamiento capitalista más funesto y mejor organizado de los modernos tiempos, sometiendo
al hombre al ejercicio automático, casi inconsciente
de sus brazos que tenía, como consecuencia natural,
un reflejo anormal en los infelices obreros, marcando su pensamiento con un tic persistente que
anulaba su iniciativa progresivamente. Conjunto
miserable de seres especializados, inutilizados fuera de la fábrica, por solo saber remachar un clavo
de manera admirable, pero estar incapacitados para
construir un banco completo. Seres cuyo albedrío
lo sometía el capitalista a un orden mantenido, con
dificultad y a fuerza de frialdad inhumana.

No eran precisamente, afirmaba Torritos, cualidades brillantes las que habían hecho grandes a
los Estados Unidos, sino más bien fuerza y energía de carácter con una gran congruencia en la forma y medios de extraer el dollar, esa unidad monetaria, de aliento infernal, que abrasa a los pueblos
del orbe. Por lo demás, debía reconocérsele que era
un pueblo ocupado en el que nada se retrasaba, como si se tratase de una gigantesca maquinaria de
reloj suizo.—Los relojes suizos han adquirido fama
de exactos, probablemente es fama, nada más, y se
han dormido en ella.—Un pueblo que había entendi
do la cultura en el sentido preciso del desarrollo de
la energía y resistencia tanto del cuerpo como del
espíritu, factores de éxito que preparaban al individuo para enriquecerse por amor a la vida y no a
vivir por la vida misma, convirtiéndolos, su codicia,
en enemigos de la humanidad.

Nelly entró con la bandeja del té y observando
e! inusitado silencio entre Torritos y Andréïda hizo
patente su extrañamiento en la forma concreta que
le era familiar.

—¡Pero Andréïda, pero Torritos!, ¿qué mutismo es éste?

Andréïda se encargó de explicárselo:

—Torritos parecía estar impaciente por conocer tus nuevos libros y por mi parte me entretuve
en hacer el más cuidadoso de los inventarios sobre
los rasgos sobresalientes que informan el carácter
de nuestro mutuo amigo.

Con una gran turbación el aludido balbuceó:

—Perdón, es verdad, Andréïda me abstraje, por
completo, con la lectura de estos versos admirables
de Lorca y me olvidé de su presencia.

—¡Qué sencillez! —comentó Andréïda—; ¡Se olvidó el señor de mi presencia! Es el primer hombre
que se permite lujo de tal especie! —Y la sonora carcajada con que acompañó la joven sus palabras obligaron al periodista a alzar las cejas fiscalizadoramente. 

—¿Qué ha dicho usted antes, qué hacía el inventario de mi persona?

—Eso he dicho —repitió divertida Andréïda.

—¿Favorable?

—Cuando menos verdadero o muy cerca de la
verdad.

—Entonces no es favorable —afirmó con desenfado el simpático periodista y añadió: —Lo menos
que me figuraba era que su silencio obedecía a un
acto de contrición y que se preparaba a confiar al
viejo amigo todo eso que anda mal colocado en esa
cabecita insensata.

—Pues ya lo ve que no —contestó burlonamente
Andréïda.

—¿Falta de confianza?

—Sobra de orgullo…

Nelly se adelantó con una taza de té y preguntó
a Torritos cuántos terrones de azúcar podía echarle.

—Dos, Nelly.

Y bajo los auspicios de la prudente muchacha
la conversación se encauzó blandamente.

Las tardes de México, en julio, suelen ser oscuras
y lluviosas y aquella no era una excepción. La lluvia azotaba los cristales y en el silencio opaco de la
hora Andréïda musitó una suave rima sobre el tamborileo monótono del agua que acaricia el ensueño.
— Versos?—interrogó Torritos con un brillo malicioso y divertido en la mirada astuta de sus ojos
pequeños, rasgados e intensamente negros.

—No sé cómo han venido a mi memoria —se excusó Andréïda—. Me parece haberlos leído no sé
cuándo ni dónde y estar, sin embargo, singularmente vivos en la mente. Quizá también haya variado yo palabras y ritmo, sin saberlo, no podría decirlo. Pero, ¿ verdad que reflejan el estado por el que
los tres pasamos en este minuto?

—Sí —fué la lacónica respuesta del periodista.

Y ahora tocó a Andréïda el reír con ironía.

—¿Y cuál es su ensueño, si puede saberse…?

—¿Mi ensueño? ¡Bah! es tan vasto que yo mismo, un pobre enfermo de verborrea, me es difíci
reducirlo a palabras.

—¿Reducirlo? ¡Qué optimista! —aseguró Andréïda con el deseo manifiesto de mortificar a Torritos.

Conocida al punto la intención de Andréïda por
Nelly, intervino diciendo:

—Yo conozco el ensueño de Torritos. Un magnífico sueño, el dialéctico sueño generoso y abstracta de un utopista.

—Ah, sí —interrumpió a su vez Andréïda— lo
conozco también. Por cierto que días antes de nuestro intempestivo viaje a Taxco me hizo víctima de
una larga disertación pomposa sobre su ideal de
sociedad futura, bastante quimérico, por cierto. ¿No
es así…?

—Así es cuando usted lo dice —la modulación
galante con que Torritos pronunció estas palabras
vibraba con un reconcentrado deseo de estallar. Por
otra parte, era éste el caldeado ambiente en que solían desarrollarse siempre los poco plácidos altercados entre él y la joven.

Dándose por desentendida, prosiguió Andréïda:

—Este buen Torritos está dispuesto siempre a
construir, sobre la disolución de toda forma arcaica
de sociedad y con el solo elemento de sus palabras
ágiles el formidable edificio de un Estado ejemplar.
Claro es que, también, como buen latino, en todas
sus utopias ocupa un lugar prominente el Estado.
Y añadió la joven ton una fingida inocencia en la
entonación: —De todas maneras, mi excelente Torritos, me pareció aquel día que no hacía usted mucho caso de la nueva forma genial en que ésto debería realizase. He de confesarle, avergonzada de mi
poca comprensión, el que me dejó la ingrata impresión de que se trataba solo de bellas palabras, de
muy bellas palabras, ¿o no fué así…?

Una mirada fulminante de los pequeños ojos
brillantes indicó a Andréïda que había dado en lo
vivo.

Enderezada la conversación por rumbos briosos,
Nelly terció deseosa de retardar, al menos, la tormentosa discusión que veía venir.

—Las bellas palabras expresan una gran valentía espiritual…

—Y también una culpable falta de acción —terminó Andréïda la frase de Nelly con un insoportable tonillo triunfal, y prosiguió diciendo: —sí, el
buen Torritos se hace culpable, sin saberlo, de todas
las bellas palabras vertidas en el pasado, en el presente o por verter y que el pueblo ha escuchado día
tras día porque respondían o responden momentáneamente, a una necesidad que se ha estado larvando desde hace más de veinte años; pero que no han
sabido más que agitarlo de manera peligrosa, ya
que no se ha tenido ni se tiene el valor suficiente
para convertirlas en hechos contundentes. ¡Ah, el
mágico imperio de las palabras de aquéllos que
atienden al sonoro exterior de ellas y les deja indiferente el fondo de las mismas, y que sufre el pueblo! ¡Ah, la indiscutible belleza de esas brillantes
burbujas hinchadas que estallan inútilmente en el
espacio! ¡Lindo espectáculo para competir infantilmente en él, si pudiera uno olvidarse de sus efectos trágicos!

Emocionada Nelly por el hondo significado de
las frases de la bienamada amiga, exclamó:

—Cierto, muy cierto. ¡Cuán terrible es el imperio de las palabras que el pueblo se encarga de tornar en sangre, carne desgarrada y rugidos de dolor
sofocados entre sus dientes macizos de indio estoico!
Porque el pueblo no sabe hacer otra cosa con las bellas palabras que eso: matar y morir sencillamente,
demasiado sencillamente.

—Me alarma usted, Nelly —comentó Torritos con
un gran sarcasmo en el tono armonioso de su voz
grave—; Qué programa tan tétrico, cuánta profecía
catastrófica! Decididamente tendré que defenderme en serio de Andréïda, porque influye mucho en
usted cualquier cosa que ella dice.

—¿Cualquier cosa? —exclamó la aludida—. Le
parece a usted cualquier cosa… A propósito, recuerdo ahora que en alguna ocasión usted me ha
hablado de una alarmante igualdad, es decir, una
igualdad sui-géneris, como a usted le es dable concebirla. O sea, igualdad de oportunidades en la vida.
Se extendió usted sobre el que los hombres debían
enfrentarse a la vida sin privilegios de herencia ni
de cultura. Esta última debía impartirse en la juventud por el estado, sin distingos de ninguna especie y atendiendo únicamente a las disposiciones
particulares de cada individuo. ¿Concibes eso, Nelly? Torritos hablándome de igualdad, él un envidioso impenitente de toda superioridad…

—Está visto —replicó amoscado Torritos— que
esta tarde la tiene usted tomada conmigo… Me
marcho… —Y diciendo y haciendo, el periodista
tomó su sombrero, pero más rápida Nelly lo cogió
del brazo y le dijo, a tiempo que lo obligaba a tomar
asiento de nuevo.

—No haga usted caso, de sobra conoce el espíritu mortificante de Andréïda; pero todo lo hace
por el deseo de desbarrar un poco a costa del prójimo —y dirigiéndose a ésta la interrogó: —¿Es así…? 
—Así es— aceptó Andréïda —y para demostrar que
mi adversión es fingida, voy a exponer inmediatatamente, lo que más admiro en nuestro excelente
amigo. —Después, dirigiéndose a él, añadió con sorna: —espero que ésto no agrave demasiado ese engreimiento insoportable de su persona que lo impele
a querer llenar el mundo con sí mismo.

—¿Otra vez a las andadas…?

—No, Torritos, ahora me dispongo a hablar con
toda sinceridad y honradez.

Y en efecto, Andréïda significó esta vez lo que
decía. Con mentida volubilidad y entre veras y bromas vertió todo aquello que ella más apreciaba en el
periodista y que no era otra cosa que su gran amor
patrio. Difícilmente podría haberse hallado en todo
el orbe, un hombre que amase de manera tan perfecta a su patria y que tuviese tan gran fe en su
magnífico destino. El exaltado amor de Torritos lo
llevaba a decir que México era la antorcha lumínica
destinada a guiar los pasos de los países de Hispano-América por el camino de la cultura y la libertad, países que estimaba él estaban, aun hoy, sumidos en la penumbra de la superstición y del miedo,
en la servidumbre del hombre hacia otro hombre.
Haciendo suyo el sueño de Bolívar, colocaba a México como cabeza directora de aquella futura e impresionante unión de pueblos, afines por la lengua y
los nexos indestructibles de la sangre, contra el enemigo común del Norte. Su espíritu humanista sentía una gran simpatía por aquellos estados menores, lamentando el que no hubiesen sabido todavía
rescatarse a la feudal sombra nefasta heredada y
tradicional, quizá porque no siendo suficientemente
fuertes temían dar el paso certero y radical hacia
el verdadero progreso. Muchas veces, al abarcar
con una gran lucidez de espíritu el futuro de la
América Hispana, involucraba en ella al propio futuro redentor de la Humanidad. Decía que era éste
el único camino transitable que faltaba por andar a
nuestra especie, en el siglo de la gran desesperación
desorbitada, de la disgregación epiléptica, del caos,
el terror y las luchas bestiales. La temeraria especulación de Torritos acerca de estas cosas lo llevaba a pensar, ya en la cúspide de su concepción,
que en la América nuestra, México había sido el
único capaz de saber encontrar el genuino principio
rebelde, motor de la enmienda al mundo y que, por
radicar en él todas las infinitas posibilidades de perfeccionamiento susceptible al hombre, no necesitaba que se imprimiese la huella de lo extraño a su
intrínseca y noble esencia. Madero, el de la exigua
estatura y a quien había tocado en suerte encarnar
el sueño abstracto de las masas al principio del siglo, era quien había puesto en movimiento aquella
soberbia espiral que se elevaba, de perfección en
perfección, y tenía un nombre sencillo: Revolución
Mexicana.

El porvenir del mundo está en la América Hispánica, repetía Torritos y con él estaban de acuerdo Andréïda y Nelly.

La conversación, encauzada de nuevo por rumbos briosos, hacía vibrar el pensamiento de Torritos
y Andréïda como las dos cuerdas tensas de dos arcos
rivales en singular competencia de tiro.

Nelly, apoyadas sus delgadas manos sobre las
rodillas permanecía quieta, aparentemente tranquila y ajena a aquel duelo verbal y solo, de cuando en
cuando, lanzaban un breve relámpago los cristales
oscuros de sus gafas.

Se estableció una pausa que aprovechó Torritos
para servirse una nueva taza de té. Sus largos
dientes amarillos, manchados por la nicotina, se
clavaron con delicia y evidente satisfacción en una
diminuta y crispada galleta. Después, con una calma amenazadora, dijo:

—Agradezco, mi encantadora amiga, el haberme hecho conocer esa admiración, que dice usted
sentir, por mi gran amor a la patria y mi desmesurada, pero bien fincada fe en el futuro de nuestro
México. Sin embargo, me ha parecido percibir en
sus halagadoras palabras un leve tonillo de superioridad complaciente, como de aquél que desciende
a elogiar algo que está muy por debajo de su convicción de belleza, pero que, aún así, lo encuentra
aceptable. ¿O no es así?

Andréïda se limitó a sonreír con frialdad y Torritos prosiguió: —No se lo reprocho, Andréïda, es
más, lo encuentro perfectamente natural. Usted se
ha formado y viene de un país en el que la degradación y el envilecimiento del mundo burgués y capitalista ha alcanzado su máximo desarrollo. De
un país en el que el concepto patria es un concepto
huero y vacuo que no dice nada a las conciencias;
pues sabido es que el dinero no tiene nacionalidad,
acude allí donde hay un hombre que explotar, una
riqueza que absorber, una conciencia que degradar.

Los lentes de Nelly sufrieron un estremecimiento inquietante sobre su nariz. Andréïda elevó los
ojos evidentemente sorprendida. Y Torritos continuó con una sorda emoción en la voz:

—Un país que, lo estamos viendo muy claramente ahora, en el tambaleo tétrico por el que está
pasando, a efecto del primer serio empuje y rugido
de las masas, es capaz de volver sus ojos desorientados hacia la abominable idea de la Unión Universal de los pueblos.

Nelly no pudo contenerse al oír calificar de abominable el soberbio sueño de tantos hombres y abrió
la boca para impugnar el aserto del periodista; mas
éste frustró la intención de la joven con un formidable “usted calle y escuche”.

—Admito la fraternidad —advirtió Torritos— en
las relaciones económicas, no la fusión disparatada
e imposible de los pueblos y de las razas.

—Tengo entendido —lo interrumpió Andréïda—
que Estados Unidos se considera, por encima de
todo, un pueblo demócrata.

—Es cierto—replicó Torritos—su Gobierno es
demócrata en estilo y culpable, por eso mismo, del
languidecimiento que sufre su progreso; además de
que no puede esperarse mucho de los esfuerzos generales que una tal democracia pueda realizar en el
plano internacional, porque estimo que, quienes pretenden sostenerla hipócritamente, no saben desprenderse de esa aceptación burguesa de la vida que, por
ello mismo, los imposibilita para conseguir una realización sincera.

Andréïda y Nelly permanecieron silenciosas.
Las jóvenes miraban a Torritos reconociéndole una
importancia que nunca habían podido sentir antes
por él.

Consciente de su repentino ascendiente, Torritos hizo una pausa para dominar el ligero temblor
que traicionaba su emoción, la gran emoción de encontrar dos personas queridas que lo tomaban en
serio, por primera vez.

—No me negará usted, Andréïda —prosiguió Torritos— que ciertos sectores siguen, en Estados Unidos, con peligrosa atención, el tempestuoso idilio rusiniano, aunque, por lo demás puede afirmarse que
ñnadie en el mundo ha escapado a tal curiosidad, la
que no se ha limitado a ser eso, mera curiosidad,
sino que se ha traducido, en veces, en actitudes imitativas aisladas. Pero, lo que a muchos no se les ha
alcanzado es que, ese gran campo biológico de experimentación que se llamó Rusia, estaba demasiado
infeccionado de gérmenes arcaicos para que pudiera
crecer, con éxito, una planta nueva. No es de extrañar, pues, que imperdonables errores sociales
hayan echado a perder su sueño generoso. Había
demasiada polilla en el Kremlin, no podía surgir
nada bueno ni nuevo. En cuanto a la gran momia
que es la Europa occidental—añadió el periodista
con una severidad que podía atribuirse a la tensión
de su espíritu—esa no puede hacer otra cosa que
esparcir su polvo a los cuatro vientos, aunque el
instinto de conservación la lleve a sacudir trágicamente al mundo con sus estertores. Pero nada impedirá que se la considere liquidada, sin remedio.

La palabra liquidada o liquidado era de una
gran fuerza en labios de Torritos. Muy a menudo
hacía uso de ella y el tono cortante, glacial y categórico con que la pronunciaba la hacía asemejarse
a una terrible catapulta, junto a la cual, la oposición de todas las argumentaciones sobraba.

—La idea de la Patria —afirmó poco después Torritos— es la idea primordial de los pueblos fuertes
que están capacitados para poder hbastarse a sí mismos. La idea nacional es un sentimiento instintivo
en la conciencia de las masas, a quienes subleva todo
lo que es invasión y mutilamiento de la esencia intrínseca de su nación en todos los órdenes. De ahí
que, su espíritu patrio esté dispuesto, a menudo, a
hacer el sacrificio de sus vidas y sus intereses cconómicos, ya que ningún pueblo puede subsistir, como
tal, sin la idea de la patria.

Y el periodista se extendió, con largueza, sobre
el mismo tópico apasionante, llegando hasta decir
que si fuera necesario para la integridad y el triunfo de la nacionalidad un absolutismo de Estado, él,
Torritos, sería el primero en transigir, siempre que
tal Gobierno emanase genuinamente de los de abajo.
Porque consideraba que el origen de todo poder y
de toda potencia está en el pueblo, por lo que la
participación honrada y genuina de éste en sus propios destinos, significaba, de consuno, el indispensable germen de renovación humana y el único real
correctivo posible que podría evitar el servilismo
diabólico a un solo hombre, Preconizó un México
en el que el hombre habría de lograr la suprema
victoria sobre lo económico. La tierra estaba, afirmó el periodista, por primera vez y gracias a una
intención sincera actual, en camino de ser común a
todos y ya habían comenzado a repartirse sus frutos
en forma fraternal y conforme al esfuerzo desplegado por el individuo en la comunidad ejidal. En
Rusia, todo se esperó de los obreros; en México todo
lo hicieron los campesinos que sabían que por la tierra serían salvos y conquistarían su liberación.

Indudablemente, añadió el periodista, el próximo paso que debía dar la gran nación mexicana
consistiría en hacer el uso de la máquina también
común a todos, única forma de enderezar, en debida
forma, su destino redentor y de alivio para la humanidad. Esto no quería decir que él, Torritos, dejase de detestar una moral económica que implicase
el servilismo y la degradación de la personalidad;
pero le era grato pensar que debía colocarse al hombre, en el principio de su lucha por la vida, en igualdad de circunstamcias.

Asimismo, el periodista tuvo expresiones precisas y fuertes al formular todo un programa potente
que favorecía la civilización, el progreso, el trabajo y la vida. Para ello repitió que era necesario
preservar a la patria de las emanaciones mefíticas
del extranjero. Tanto más cuanto, como todos los
pueblos jóvenes, era la desesperación de los estadistas, ya que su propia juventud generosa estaba
dispuesta a someterse a las peores pruebas y sus
ojos niños sufrían el encandilamiento de prestigios
canosos. Había, pues, que acorazarla contra sutilezas fanáticas y una clase de espejismo espiritual.
Torritos consideraba deber sagrado el oponer toda la potencia de la naturaleza virgen de México contra la infección artera de las influencias extranjeras
que amagaban su integridad, o sea lo mejor de la
patria.

Los labios del viejo periodista tuvieron amarguras extrañas cuando, con un orgullo casi doloroso, se refirió a la intromisión lamentable de ciertas
doctrinas que perseguían la desviación del verdadero espíritu mexicano. Su tendencia de eterna censura, combativa y rebelde que era en él una clase
de obsesión se resumía siempre en un hondo patriotismo, en un culto a la dignidad humana y en un
aborrecimiento contra todo lo que empaña y deshonra la idea del hombre. Abandonaba su indolencia de individuo de tierra templada cuando creía
que el concurso de su esfuerzo podía ser empleado
en la causa del progreso, la civilización y el placer
de vivir. Consideraba la transformación radical de
los sistemas económicos de México en la medida del
progreso que libera y de la civilización que afina y
humaniza. Su claro espíritu no concebía lo tortuoso.
Sus especulaciones sociológicas tenían, como finalidad, la utopia del estado perfecto y su pensamiento se mantenía firme, sin peligrosos escarceos en
los medios extraños.

En la profesión de fe de Torritos, les fué dado
a Andréïda y a Nelly, la tarde aquélla, conocer una
nueva fase de la ironía tajante, afilada y corrosiva
del periodista; pero que, a pesar de ello, tenía algo
de constructivo. Flotaba sobre sus palabras una especie de anhelo que intentaba sacudir a la patria
de su estado primitivo de incertidumbre, de miedo,
de apatía resignada, llevándola a un ciclo más activo de su conciencia que, hasta la fecha, no parecía
haber conocido más que de aisladas explosiones bélicas, a las cuales había faltado un horizonte preciso
y una gran voluntad de alcanzarlo.

La ironía de Torritos, en veces, era como un
acierto de dolor elevado sobre la patria y era entonces cuando sus alusiones se tornaban tan sutiles y
abstractas que era preciso una imaginación del trópico para comprenderlas.

Andréïda interrumpió su larga peroración para hacerle una pregunta concreta que cortó de raíz
las abstracciones por las que iba enderezando ya la
conversación.

—Me es permitido preguntarle —dijo la joven—,
¿a qué atribuye usted el malestar de nuestro siglo?

Torritos pareció reunir, con esfuerzo, sus ideas
y contestó lentamente:

—No considero la crisis de los tiempos modernos como un conflicto entre los intereses del hombre y la colectividad…

—¿Entonces…?

—…sino como los intereses de varias docenas
de hombres en contra de la especie humana. Estimo —añadió el hombre con firmeza— que no hay peor
degradación para el individuo que el triunfo de su
egoísmo y de lo económico a costa de sus semejantes. Es verdadero lo que conviene al hombre y conviene al hombre lo que conviene a la colectividad:
ésta es una frase de que me gusta hacer uso seguida
de aquella otra que ustedes conocen: “solo lo nuestro responde a nosotros”. De ahí que no pueda admitir el socialismo como el terror de una dictadura
en la que solo habrá cambiado el nombre de quienes 
la ejerzan; pero cuyos frutos seguirán siendo los
frutos de los hombres de mala fe que sirven para
desviar la esencia de lo que pudo ser bueno para la
humanidad, y que, en tal caso, dan vida solo a una
nueva forma vacía y engañosa.

Después, con expresión de una claridad tal que
penetró la propia médula de las jóvenes, terminó
Torritos de decir:

—La solidaridad es el único valor positivo de
las razas, de los pueblos y de las masas. Un valor
positivo de primerísimo orden sin el cual no es posible hacer, de un país, una nación verdaderamente
grande.

Andréïda sonrió en su interior, le era familiar
aquel dualismo de Torritos que cencordaba mal con
su actitud disociadora y de eterna: censura. Mientras
tanto, la mirada dulce y sombría del periodista cavó sobre Neily con el placer del hombre que sabe
cuenta con un partidario incondicional y fácil de
convencimiento.

Ya en la puerta, para despedirse Torritos, Andréïda tuvo una amable expresión final:

—Mi excelente amigo, pienso en usted como en
un oscuro idealista a quien pesan demasiado sus
propios ensueños.

Torritos estrechó la mano de la mujer y, a despecho de su irreprimible prevención y reserva mental para todo lo que Andréïda estaba empeñada en
representar sobre la tierra, sintió, por primera vez,
el deseo de penetrar un poco su misterio.

—¡Qué admirable contextura moral la de Torritos!—comentó Nelly a tiempo de cerrar la puerta
de la calle tras la sombra del periodista
—Muy admirable—admitió con una sonrisa Andréïda y añadió maliciosamente :—Siempre que se
olvide uno de que muchas veces pide cinco pesos
prestados, con la sana intención de no volverlos, a
aquellos mismos a quienes censurara unos minutos
antes con acritud, a fin de ir a tomar cognac o tequila en compañía vergonzosa y poco edificante.

</section>
<section epub:type="chapter" role="doc-chapter">

# SEGUNDA PARTE {.versalitas .parte}

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo I

Un día no desempeña un papel muy importante
en México, ni una semana, ni quizá un mes.

El mañana en labios de un mexicano tiene una
amplitud cósmica, la misma amplitud eterna e infinita con la que gustamos de explicar el fenómeno
del tiempo y del espacio, a falta de algo mejor y
más concreto.

Ese mañana tiene también toda la pesadez, toda la lentitud, toda la increíble pereza y la fuerza
de inercia de un clima, cuya suavidad muelle envuelve al individuo con el embrujo cálido y feliz de
que debió ser poseedor el mito paradisíaco de los
hebreos.

En compensación, de esta inercia de la materia,
se venga el espíritu, cuya agilidad turbulenta desborda toda proporción, lo que provoca una serie de
cambios de postura, casi angustiosa.

El progreso en otras latitudes es una cuestión
de exigencia material. El progreso, en México, es
una cuestión de elevación espiritual, de anhelo de
perfección en plano cerebral. Asombra el progreso
de las leyes mexicanas en relación con las de países
en los que las realizaciones alcanzan un plano superior. Y es que el México espiritual avanza a pasos
de gigante hacia lo enorme y lo magnífico. En cierto modo, reflejo pueril, pero significativo de este
anhelo de elevación y grandeza, hasta en el orden de
cosas más banal, lo tenemos en los letreros que se
multiplican a través de toda la nación y en sus pueblos más humildes, rubricando una hospedería ínfima con el pomposo nombre de Gran Hotel, un
changarro en que las huellas de las moscas son más
abundantes que la mercancía que albergan las estanterías, clavado en su frente, orgullosamente, el
nombre de Gran Almacén de Novedades, y así por
el estilo. A primera vista y para el observador superficial ésto pudiera parecer un tanto ridículo;
pero, ¿cuál es la cosa o quién es la persona de grandes vuelos y ambiciones que no haya sido motejada,
en sus principios, de ridícula?

Mientras tanto y por ahora, el México positivo
adopta la cómoda postura de la espera.

\* \* \* {.espacio-arriba1 .centrado}

Andréïda había traído del país extraño la actividad febricitante del que sabe que la menor detención en su paso puede ocasionarle el tumbo trágico
bajo las pezuñas de los que corren desatentadamente,
temerosos siempre de llegar tarde, al llamado del
pesebre. {.espacio-arriba1}

Pero como todos los que llegan de fuera y que,
en un principio, desconocen la prodigalidad en el
empleo del tiempo, había acabado por aceptarla, en
parte, con esa curiosa adaptación que hace pensar
que el extraño traga demasiado pronto la simpática aceptación lenta de la vida, no tardando mucho
en habituarse a ella.

Sin embargo, los restos de su actividad brillante debían ayudar a Andréïda a abrirse paso en terreno tan pródigo, en el que basta un arañazo superficial de un arado egipcio para devolver centuplicado el abúlico esfuerzo.

—A pesar de ésto había pasado el tiempo. Para
toda realización es necesario que pase un espacio
más o menos largo de la misteriosa esencia; con
más razón aún sí, como en México, le afecta una
extraña devaluación que le obliga a “dilatarse”. No
en balde el pueblo usa, a menudo, de esta expresión
última, en su parlar deliciosamente plástico y colorido.

¿Habían pasado dos o tres años desde nuestro
último conocimiento con la protagomsta de esta historia? Tal vez. Aquí podríamos eñncajar fácilmente
el filosófico ¡quién sabe!, también muy mexicano,
y que siempre se encuentra muy lejos de ser una
pregunta y, en cambio, asume una clase de cómoda
eludición irresponsable.

Nosotros diremos, desde luego, que ha transcurrido más tiempo del que la propia Andréïda habría
querido que transcurriese en su impaciencia natural por cuajar su desmesurado empeño. Pero también nos es permitido decir que ella era de la clase
de mujeres que no conocen de la salobre vuelta de
cabeza que petrifica y que, por el contrario, sabía
del avance firme, sin vacilaciones funestas, cualidad indispensable de disciplina a toda realizac¡ón.
por pequeña o grande que ésta sea.

Después del terrible abatimiento por el que pasara durante los días de relativa soledad vividos por
ella en Taxco, la ciudad milagrosa, sobrevino a su
espíritu un enfriamiento de la imaginación y un retorno a la sangre fría. Aquella depresión determinó
un como reposo oculto de sus fuerzas anímicas, las
cuales al reaccionar, reflejaron en su cerebro, con
mayor fuerza y singular brillantez, la razón íntima
de su profunda personalidad, tema fundamental y
problema esencial de su vida toda.

Por obra y gracia de su terrible fuerza de voluntad se operó el milagro de convertir una huída,
una derrota inminente —así juzgaba ella la atracción física de dos seres, pues estimaba evasiva toda
otra clasificación que no fuera esta cruda que ella
le daba —en la más completa afirmación de su atrevida concepción de la mujer nueva. Esto había sido
en cierta manera, decía Andréïda, repitiendo, quizá
aiguna frase leída “plantar el pie en la nuca de los
instintos elementales”. Hacer el orden en el caos
interior.

Desde luego que aquéllo no había sido fácil. Todavía ahora, después del tiempo transcurrido, sentía
allá en las profundidades sensibles de su conciencia
una como carcomida angustia secreta que tenía, al
frente, un horizonte extrañamente vacío. Podíamos
aventurar también, el que, debido a ello, se había
filtrado en su empresa cierta animosidad, cierta
prevención de que antes se viera libra, contra el
sexo opuesto, y que ahora se traslucía a veces, cuando su pluma tocaba aquellos temas en artículos periodísticos, en la forma de una gracia amarga, turbadora y excitante que involuntariamente la traicionaba por entero y, en cierto modo, sin saberlo
ella.

Solía hacer, a menudo, examen de lo que ella
consideraba su cristalización.

Había logrado labrarse una situación económica
brillante que le proporcionaba el elemento cardinal
a su triunfo, o sea, una independencia de actos completa.

Consciente de que el mimetismo natural de la
humanidad necesita de ideas que uniformen, clasifiquen y etiqueten su actitud en el tiempo, Andréïda
había comenzado por ejercer su influencia por medio de su elegante originalidad en el vestir. No de
otra manera los dictadores de la época han impuesto sus corrientes ideológicas, hechas cómica puerilidad camiseril.

Como ellos también, y guardando la proporción
debida, podía observarse en las actitudes de Andréïda cierta teatralidad, grata al común humano
y que favorecía directamente esa ansia de proselitismo que hay en el fondo de todo aquél que piensa ha
creado algo nuevo sobre el inevadible círculo vicioso, en marcha, que es el universo.

Es por ésto que se multiplicaban en las calles las
cabecitas de mujer imitando el peinado glorioso de
Andréïda. Y también, por ésto mismo podía observarse cómo las líneas de sus trajes se reproducían, con alarmante persistencia, en cuerpos femeninos. Su color favorito invadía los sitios públicos
y hasta había damiselas que copiasen sus ademanes
servilmente, entre ellos, aquel movimiento orgulloso, muy suyo, de erguir la cabeza con gracia y como
desafiando al mundo.

Alguna vez Nelly se había permitido preguntar
a Andréïda qué había en aquellos detalles banales
que pudiera, realmente, satisfacer un pensamiento
tan serio como el que gustaba ella de hacer alarde.
En respuesta, la singular joven había guardado un
vacuo silencio que Torritos tuvo oportunidad de llegar con una sentencia emitida en voz hinchada:

—Los detalles banales de una época responden
siempre a hondos fenómenos espirituales. Los trajes de pastora de María Antonieta fueron más que
un simple capricho de reina frívola, obedecían a
una necesicad de sencillez latente que el terror sacó,
más tarde, a flote.

Por otra parte, la influencia de Andréïda era
tanto más efectiva cuanto tenía el ascenciente de
ser ultra exquisitamente femenina. No había en ella
nada que indicase el morbo de una competencia ridicula con el hombre, sino, por el contrario, la afirmación de un nuevo ser perfecto que reunía en si
cualidades únicas y se veía libre de todo aquello que
en uno y otro sexo son motivo de debilidades humillantes.

Marchaba por la vida con la seguricad de un
ser que lo espera todo de sí mismo, y nada de los
demás.

El tiempo se había encargado de afirmar en sus
ojos una frialdad inhumana y calculadora que no
tendría piedad, llegado el caso —ni para consigo
misma. La mirada de sus ojos oscuros, escudriñadora, helada y penetrante, parecía poseer un sentido muy seguro y crítico acerca de todas las cosas.
Su pensamiento se había tornado extrañamente certero. Sabía ya lo que quería. Ninguna decepción:
podría conmover su ideal, ninguna emoción, su ser.
La lógica de su razonamiento alcanzaba planos glaciales que hacían temer el que; muy pronto, arribase a la cúspide de su enorme y soberbio propósito.

A su lado Nelly parecía cada día menos dueña
de sí misma. Semejaba un perrillo faldero que no
tuviese otra misión que la de seguir perpetuamente
a su ama, con una mirada de angustia en la suavidad fiel de los ojos. solo de cuando en cuando se
permitía interrogar a la bienamada amiga, a la
hermana pequeña de su corazón, que la aventajaba
em estatura moral y en experiencia, hacia dónde iba
y qué es lo que perseguía. Y la voz con la que solía
haacer las preguntas sencillas se resentía de un temblor trémulo.

En respuesta, Andréïda sonreía, sonreía siempre, segura de su triunfo oculto y ese día acostumbraba escribir algo que desconcertaba conceptos y
afirmaciones sabidas.

…Y llegó un día en el que, por fin, supo Andréïda que el Destino la sometía a una prueba final.
la terrible prueba del fuego de la cual podría surgir cálida, vibrante, verdaderamente triunfal esa
bella y audaz obra que era ella misma, o, por el
contrario, el enorme sueño de su concepción saldría
derretido en informe metal sin valor alguno.

De nuevo encontró al hombre, al único. Para
ella no podía haber más que uno. solo que ahora
ella estaba en la plenitud de su cristalización, y tenía atrás el ambiente esclavizante de la mucha o
poca labor propia que obliga a la persistencia y a
obrar consecuentemente. Fuerza feroz que hace pensar en el fondo de verdad que anima al fatalismo
oriental.

Y lo afrontó sin miedo, segura de ella misma,
con una enorme curiosidad de constatar su verdadero triunfo.

Fué de la siguiente manera:

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo II

_Miss_ Virginia Spleen, antigua profesora del St.
Mary, colegio en el que se educaran Andréïda y
Nelly, había arribado a México entre la avalancha
de “turistas” que han puesto de moda, en Norteamérica, a nuestro maravilloso país, imponiendo a
la capital mexicana restaurantes tipo _Child’s_, centros nocturnos, tiendas de antigiüedades que por lo
mismo expenden, a precio moderado, lo más moderno salido de manos mestizas, “courts”, etc. Todo
ésto amparado con sendos letreros en inglés y con
personal que habla con fluidez el idioma anglosajón;
por lo que el visitante encuentra cordialidad, hospitadlidad exquisita y una sonrisa discreta y sardónica para sus excentricidades, en el fondo de la cual descansa siempre el convencimiento de una superioridad de inteligencia y viva imaginación.

Por un modesto puñado de _dollars_ que se le multiplica maravillosamente en pesos mexicanos, el viajero norteamericano goza de aquéllo que en años
pasados fuera privilegio exclusivo de su potentado
_boss_ en sus visitas a Europa, y encuentra, además, con inenarrable asombro un tipo de nación que
le deslumbra con su sólida alma nacional, bien determinada, que se hace canción, ritmo, verso y atavío para júbilo de propios y extraños.

Miss Virginia Spleen, aparentemente, parecía
verse libre de ese espíritu gregario que, sin excepción, reúne grupos disciplinados y atentos de americanos pendientes de las palabras de un guía. Por
cierto que, es hora ya de hacer ver a nuestros guías
autóctonos la conveniencia de ponerse de acuerdo
entre sí, para dar un tinte uniforme a sus falsedades, las que de esta manera y quizá con el tiempo
podrían adquirir visos de verdad. Díganlo si no los
americanos que han visitado dos veces el castillo
de Chapultepec y, no obstante que se haya dado el
caso de que fuesen atendidos por el mismo guía, con
seguridad que constataron su mala memoria y grave
ignorancia.

Miss Virginia Spleen trajo el propósito de ver a
México con sus propios ojos; pero como del idioma
de Cervantes solo poseía cuatro palabras y parte
de éstas ni siquiera eran genuinamente españolas,
“chili con carne y _hot_ tamales”, pensó que Nelly y
Andréïda le podían ser útiles en sus diez días de
vacaciones, por lo que su primer acto, a su llegada,
fué ir en busca de las jóvenes. El presupuesto de
Miss Spleen para sus vacaciones en México y el cual
no debía sobrepasar con detrimento de su cuenta
corriente, por nada en el mundo, era a razón de cinco dollars diarios, incluyendo en esta mezquina suma
gastos para diversiones, _souvenirs_ mexicanos para las amigas, etc. Además, la precavida y económica Miss Spleen contaba con un asiento libre en un
automóvil de amigos, en el que había hecho el viaje
y efectuaría el retorno.

En compañía de Nelly y Andréïda y con una
kodak al hombro, recorrió Xochimilco, el “Dessert
of Lyons”, Cuernavaca, se empringó los dedos comiendo antojos en “Las Cazuelas”, fué al frontón,
tomó asiento de barrera en “El Toreo”, dirigiendo
miradas furtivas al ruedo durante el _bull-fight_, y
obligó a salir a las jóvenes al tercer toro. Con ésto
creyó haber conocido a fondo México y, por último,
expresó el deseo de dar un vistazo a los murales de
Diego Rivera, pues bien sabía ella, afirmó, que ningún buen turista americano podía decir, en justicia,
que había visitado México sin haber detenido su vista en los famosos muros y en la piedra del calendario azteca. Por cierto, que de este último llevaba
copia labrada en cuero sobre una bolsa de mano, de
enormes proporciones, que se proponía lucir a conciencia, a su regreso, entre sus amistades.

A Nelly le fué imposible ese día acompañar a
Miss Spleen, por ser lunes, y rogó a Andréïda que
la sustituyese en aquella obligación. Y aunque a
Andréïda le iba ya resultando enojosa la compañía
de aquella buena mujer que expresaba sus deseos
como si fuesen órdenes, y que no tenía en la boca,
para sus admiraciones, otro adjetivo que un _so
cute_, aplicado lo mismo a un sarape del Saltillo que
al maravilloso Palacio de Bellas Artes; la joven no
tuvo más remedio que acceder ante el ruego insistente de Nelly, y llevar a Miss Spleen en penosa
peregrinación, primero a la Secretaría de Educación
y más tarde a Palacio.

Allí, recargadas las dos sobre el barandal del
primer rellano, incapacitadas para apreciar el conjunto, pues Miss Spleen deseó acercarse lo más posible a los frescos, vió súbitamente Andréïda descender la escalera al doctor Raúl.

Los ojos punteados de oro del profesionista expresaron la más viva alegría y sorpresa y, con leve
temblor en la voz, exclamó: —Debía encontrarla!
¡Yo sabía que un día la encontraría irremisiblemente! ¡No podía ser de otra manera!

Andréïda había palidecido ligeramente y con forzada naturalidad procedió a hacer la presentación
del profesionista a su antigua profesora.

—Miss Spleen, el Dr. Raúl…

—Torrelavega —añadió el doctor.

Andréïda se disculpó diciendo en español:

——Ni siquiera conocía su apellido…

—Ni yo su nombre —hizo constar el doctor—.
¡Me bastó usted! Después busqué por cielo y tierra
una tal Nelly, de apellido indescifrable, que se había
registrado en el hotel al tomar el cuarto en compañía suya. —Y como si una duda le asaltara de pronto, interrogó: —¿No es ese su nombre, verdad?

—No… —negó Andréïda sonriendo.

Sin saber por qué la alegró el gozo que el doctor
manifestaba abiertamente con su encuentro, diciéndose que aquel entusiasmo masculino se abría hoy
a plena luz del día, pues eran las once de la mañana; y no como ella pensara en Taxco, o sea que solo
podía experimentarlo en las horas nocturnas. La
divertía también la forma en la que él se había arrogado el derecho de interrogarla y reprocharla su
huída a tiempo que afirmaba, con un aplomo gozoso, el que esta vez no había poder humano que le
impidiese el seguir frecuentando su compañía.

Mientras ésto decía el doctor Raúl, Mis Spleen
arriscaba la nariz, de proporciones más que medianas, y hacía esfuerzos supremos por asimilar la
visión colorida que se ofrecía a sus ojos.

Andréïda constató en Raúl idéntica explosión de
vida que prestaba una como luminosidad detonante
a su esbelta figura, y que a ella la sorprendiera,
desde el primer momento, en Taxco, añadida a la
misma ansia fervorosa de desbordamiento interior,
dulce y salvaje, y tan positiva y sencilla que anulaba toda especulación intelectual sobre sus orígenes
y su razón. Era el hombre magnífico, el de la frente azotada por el viento de las grandes alturas, el de
los nervios vibrantes y el mentón rebelde y obstinado como quilla de acero que se abre paso majestuosamente, por entre procelosas ondas, con un solo
destino, el destino único de celebrar sus nupcias con
la vida hasta el límite del abrazo enorme, embriagador, grandioso, completo.

La mirada de Andréïda la sintió el doctor Raúl
como un frío bisturí que ahondara profundidades
ignotas en la carne sangrante, y sintió miedo hasta
ponerse pálido. ¡Qué clase de voluntarismo absurdo
expresaba aquella mirada helada de mujer!, se preguntó y, por los hermosos ojos viriles, pasó la sombra de una peligrosa adquisición, la de la conciencia
de sufrimientos futuros. El, un enamorado de la
belleza, de la libertad de los sentidos y de la felicidad del placer, sintió miedo de la belleza, de los sentidos y su placer.

Después, hombre y mujer, con el pensamiento
ausente, fijaron la vista en el mundo expresional de
Rivera. Sin embargo, Andréïda, con esa facilidad
que poseen las mujeres para observar al hombre sin
fijar necesariamente los ojos en él, captó al punto,
en la manera como el profesionista miró la obra de
arte, una repugnancia implícita que estimuló en la
jeven el desco de interrogarle acerca de sus gustos
en arte y belleza, impulsándolo a hablar con acariciadora brusquedad.

El timbre cálido y velado de la voz de Andréïda
produjo en el doctor Raúl un breve e impetuoso vértigo. El interés repentino de ella por conocer sus
gustos le proporcionó una satisfacción íntima que le
hizo creer que la frialdad con que había sido acogido su encuentro debió ser una clase de coquetería.
Por un momento, reconcentró su mente con el deseo
de sugerir sus impresiones de la manera más exacta, y se expresó así:

—Nunca he sabido exactamente por qué me disgusta la obra pictórica de Rivera, así como tampoco
por qué siento aversión por todos aquellos que tratan de seguir su huella en arte…

Hizo una pausa y titubeó antes de seguir adelante. Percibía la atención un tanto burlona de la
joven, y temió aparecer ante sus ojos como un ser
mediocre. Andréïda aprovechó su silencio para decir: —Reconozco en usted un hombre de sensibilidad, quizá por ello sus ideas son confusas, doctor…

Un breve rubor coloreó la cara viril, y Andréïda
se apresuró a añadir con un tonillo endiabladamente maligno:

—Quizá también usted, como hombre de cultura
burguesa, sus prejuicios le inculquen cierta prevención infundada, no permitiéndole externar un juicio sereno sobre Rivera.

Esta vez la voz del doctor Raúl se tornó bronca
para replicar:

—Creo percibir un velado desafío en sus palabras. ..


—Nada de eso—se apresuró a contestar sonriente Andréïda.

—Lo acepto… Mas debo advertirla que nunca, antes de ahora, detuve mi pensamiento en analizar mi repugnancia. Puede decirse que jamás lo
hago. Ha acertado usted cuando ha dicho, hace poco, que soy un hombre de sensibilidad, un impulsivo. En cuanto a externar un juicio no podría hacerlo, en justicia, ya que me reconozco profano en
la materia, en todo caso mis palabras podrían tomarse como las de un amante de la belleza pura…

—Hágalo así en mi obsequio —suplicó Andréïda
con una repentina seriedad en la voz.

—Pues bien; he aquí lo que pienso: —Había algo
de tajante dureza en la forma como el doctor Raúl
habló—. En primer lugar, estimo que Diego Rivera
parece haber olvidado aquel primer paso trascendental en arte dado por Cimabué, consistente en
“mirar la figura humana para pintar la figura humana”. Ofende a mis ojos esa falta de influencia
anatómica en muchas figuras suyas, esos acortamientos absurdos, esas proporciones arbitrarias.
Observe usted esas piernas humanas que parecen
estar rellenas de aserrín. Probablemente a eso se
deba que nuestro pueblo, tan sensible a la belleza
pura, titule de “monos de Rivera”, a su pintura.
Encuentro en el pincel de Rivera una mala voluntad, un principio profundamente hostil, una infidelidad torva hacia la verdad natural que no puede
emocionarme porque no la determina la impotencia
ni la inhabilidad técnica del artista, sino una complacencia exprofesa en denigrar la belleza humana
a la manera de un Margaritone. Su arte descubre,
a primera vista, el deseo de representar no formas,
sino ideas; no belleza, sino fórmulas abstractas. Es
un arte enfático, demagógico, exaltado. Contemple
usted esa fealdad exagerada, vulgar, repugnante,
deforme, que pone en nuestros indios; esos pliegues
extraños en las bocas carnosas, esas prolongaciones,
esas prominencias, esas irregularidades, esas cavidades y desfiguramientos de su mundo pictórico
desagradable. Esas piernas desviadas de una infidelidad pintoresca que turba. No hay serenidad, ni
salud, ni alegría, solo exaltación, fuerza admirablemente expresada, apelotonamiento de formas, que
después de todo se adivina que no le interesan más
que a medias. Pienso, por otra parte, que la admiración que despierta su obra, es la admiración de un
siglo que gusta de la imperífección y se solaza en lo
absurdo.

El doctor Raúl calló, arrepentido de haberse dado todo entero en sus palabras. Mis Spleen escuchaba sin entender una palabra; pero subyugada
por el bello ardor y apasionamiento que el hombre
había puesto en sus frases.

Andréïda permanecía en silencio, parecía analizar en su interior aquella avalancha de conceptos
que 1e sonaban a nuevos. Comprendía que, en el
fondo, su concepto de belleza no distaba mucho de
aquel del doctor Raúl. Sin embargo, su dualismo interior que constituía, en último análisis, la razón y
principio motor de su existir, la impulsó a rebatir,
eficazmente, la argumentación del profesionista. Y
habló con serenidad y con firmeza:

—Permítame que le exprese, doctor —comenzó
diciendo— mis puntos de vista a este respecto, que
son, por cierto, completamente opuestos a los suyos.
Empezaré por decirle que me parece demasiado literal su interpretación del mundo expresional de Rivera, lo que impide a usted el que pueda ver en la
fealdad atroz de esos indios, que tanto parece chocarle y que, de cierto, no es nódulo exacto de su genuino aspecto racial, la expresión milagrosa de ese
dolor mudo que la dominación de un puñado de
hombres ha puesto en el México atormentado de la
Colonia, de la Reforma y de la Revolución. Sublime dolor que en el pincel de Rivera se torna alarido
soberbio de reivindicación, trazo maestro que lo
mismo ridiculiza al opresor que imprime al oprimido
el gesto rotundo, hondo, lancinante, que no sabe de
los suaves contornos ni de la elegante belleza reblandecida que debilita el espíritu. Exaltación sí,
serenidad, no. Formas dislocadas, figuras taciturnas que turban el alma y trastocan conceptos, más
cerca de la maestría que de la belleza. Color, idea,
expresión que, en fuerza de fealdad, crea una belleza nueva interior, única susceptible de someterse a
un pincel indómito, original, revolucionario en toda
su real verdad.

Andréïda calló a su vez, mientras el doctor Raúl
argüía ineficazmente :

—¡Triste verdad bestial!

Mis Spleen no pudo contenerse y con tres rápidos _what?_, _what?_, _what?_, deseó enterarse de lo
que apasionaba a su antigua discípula y a su nuevo
_handsome dear friend_.

Con una condescendencia sonriente, Andréïda
accedió a traducirle dos o tres superficialidades sobre los méritos de aquellos muros, que no son ciertamente lo mejor del corpulento maestro, y la americana asintió a todo con varios rotundos _Oh_, _yes_, _yes_, _yes_, que dejó satisfecha su deficiente curiosidad de mujer, cuya cultura era la de un país de
progreso técnico. 

Por su parte, el doctor Raúl se sumió en un silencio hostil, hondamente disgustado de sentir a
Andréïda tan independiente en gustos. Su cerebro
hervía en deseos de acorralarla con frases concluyentes, hasta imponerle sus ideas y sus pasiones en arte. Lo irritaba la tranquilidad agresiva de su razonamiento que la colocaba en un plano de adversaria orgullosa, con armas iguales. Le hubiera gustado ver en ella un punto débil, una ligera impotencia, una indecisión en su juicio, una vacilación en
su razonamiento que le hubiera permitido inclinarse
hacia ella, de manera protectora, y hasta sacrificarle, en aras de su capricho femenino, sus propias convicciones.

Un leve ardor empurpuraba sus sienes y permaneció un paso atrás de Andréïda y Miss Spleen
cuando éstas se dirigieron al automóvil que las esperaba en la amplitud palaciega del patio, circundado de esbeltas arcadas. Ya casi para acercarse
ellas se adelantó el doctor a abrir la portezuela,
ayudando a subir a Miss Spleen, la que se felicitó,
una vez más, de ser objeto de la fina cortesía de los
incomparables _gentlemen_ mexicanos. Idéntico
movimiento hizo el doctor Raúl para atender a Andréïda; pero ésta rechazó la cortés ayuda, ya que
juzgaba debilidad en la mujer el aceptarla; cosa que
obligó al profesionista a exclamar con enojo que en
vano intentó templar, recordando el incidente que
dió pretexto en Taxco a su breve conocimiento con
la extraña joven:

—¡Fuí más afortunado en Taxco!

El repentino recuerdo hizo temblar un poco a
Andréïda, por lo que con voz opaca dió una breve
orden al chauffeur. El doctor Raúl que tenía todavía la portezuela abierta, colocó un pie en el estribo
y se acercó a Andréïda diciendo en voz baja y voluntariosa:

—No intentará huír, de nuevo, sin decirme siquiera su nombre…

Al oír tales palabras, Andréïda sintió una ligera
impaciencia; pero no queriendo mostrarse excesivamente brusca y pensando, además, que una negativa le daría base a ella misma para juzgarse como
temerosa de una amistad que ahora sabía, de sobra,
no tenía ya ningún peligro para ella, replicó con
cierta displicencia sonriente:

—Nada de eso, doctor. Puede usted llamarme
Andréïda y si desea un día de estos conocer el círculo de mis amigos, tendré sumo gusto en recibirlo un
miércoles de cualquier semana, a las nueve de la
noche, en Amsterdam 815…

Tendióle la mano y ella misma cerró la portezuela.

El doctor Raúl contempló cómo se alejaba el
coche y, de pronto, le asaltó la duda de que ella no
hubiese sido veraz en la dirección que le había dado
y que había quedado en su memoria de manera imborrable. A grandes zancadas se dirigió a: su coche
y ordenó que siguiese discretamente el de Andréïda
que ya se alejaba raudo por la plaza de la Constitución hacia 16 de Septiembre.

En su interior, el doctor se hizo el propósito de
acudir el miércoles de aquella misma semana a la
casa de Andréïda.

El nombre de Andréïda le había recordado algo.
Una conversación, un tanto ligera, de unos amigos
suyos acerca de una mujer extraordinaria a quien
habían nombrado de la misma extraña manera;
pero temiendo identificar con ella a la amiga, no
quiso ahondar el recuerdo y rechazó el pensamiento
como algo molesto y absurdo. Sin embargo, quedó
aquello en su corazón, a la manera de una angustia
latente y abstracta que le impidió entregarse a su
trabajo en la forma completa que gustaba de hacerlo.

</section>
<section epub:type="chapter" role="doc-chapter">

# Capítulo III

Aquella noche del miércoles tenía Andréïda una
expresión dura e intranquilizadora en los magníficos ojos.

Irritada y sombría hacía los honores a sus habituales invitados con una amabilidad un poco taciturna.

La nueva casa de Andréïda y Nelly, en las calles de Amsterdam, era una verdadera residencia
en la que el gusto refinado de la primera había desplegado toda su delicadeza y originalidad, haciendo
de cada estancia un lugar grato a los ojos y al espíritu. En ella no había un detalle que despegara del
conjunto, ni un color llamativo, ni una arista aguda.
Todo era suavidad firme, grata, acogedora.

El salón principal, en el que ahora se encontraban, poseía un sistema de luz indirecta que se esparcía discretamente, con una claridad tenue y rosada, lo que proporcionaba a los nervios una lasitud sedante y acariciadora.

El círculo de íntimos de Andréïda era bien reducido y heterogéneo. En su salón solían reunirse
las figuras políticas del momento, celebridades intelectuales o simplemente de mérito en concepto de
la propia Andréïda, dos o tres jóvenes fogosos, de
ideas radicales, poetas, periodistas y un gordo burgués —los burgueses tienen que ser forzosamnte rollizos y bien cebados, de lo contrario perderían la
más voluminosa de sus características —introducido
por Torritos y que iba a la zaga de Andréïda con
inocultable codicia. En total, una veintena de personalidades, pues Andréïda que gozaba al contacto
de aglomeraciones, gustaba de bien poca gente en la
intimidad de sus salones.

Torritos, como de costumbre, ocupaba su rincón
favorito en compañía de dos periodistas muy jóvenes que sufrían su ascendiente y hacían las veces
de dóciles satélites, dispuestos a celebrar sus salidas
ingeniosas con graves muestras de la más completa
y profunda comprensión. A decir verdad y en justicia, debemos decir que, las más de las veces, apenas si rozaba sus respectivas epidermis, la aguda y
maligna intención del viejo periodista.

En el rincón opuesto, repudiado de todos, aislada su diminuta y rechoncha figura, se encontraba
también el Mago—el poeta maldito como lo llamaba
graciosamente Andréïda—persiguiendo, como de
costumbre, una imposible rima. El viejo poeta de
los ojos glaucos contemplaba, desde un sillón, el ir
y venir de aquella gente bien ataviada, cuyos afanes
le tenían sin cuidado, sabiéndose en la serena cumbre de quien ha conquistado el filosófico barril diogeniano y le basta el sol para ser feliz. Ahora que,
además del sol, a nuestro poeta le hacían falta, para
su felicidad vegetativa, unas cuantas copas de áspero mezcal. Buena prueba de su ruín debilidad se
podía encontrar en su nariz roja como un exuberante pimiento español y en el movimiento torpe de
sus labios para hablar la prosa diaria; pero milagrosamente hábiles para modular sus versos. Con
su atavío descuidado y grasiento, el desgaire de la
mariposa negra y emblemática al cuello y las monumentales alas negras del chambergo, el Mago revivía al último de los bohemios.

Andréïda fué a sentarse junto a él. Sentía, por
el poeta, una suave predilección. Al punto, él se
apresuró a mostrarle su último soneto, escrito a la
espalda de una boleta de contribución, escamoteada
no se sabía dónde. Andréïda se rehusó a leerlo; pero, en cambio, le suplicó que él mismo se lo recitase.
No gustaba de leer la producción del amigo por temor de encontrar una imperfección que deshiciese
el bello efecto, y, por el contrario, en los gruesos
labios del poeta el verso fluía con la pureza cristalina y espontánea de un fresco venero que brotase
de áspera roca en la montaña. ¡En la palabra escrita se notaba demasiado la imperfección de su
espontaneidad! Y así fué como el poeta susurró al
oído de Andréïda su último canto de corte clásico y
franciscana sencillez.

Andréïda dió un vistazo impaciente al relojito
que circundaba su fina muñeca. Eran las diez. Todo el día había vagado por su pensamiento la ambigua esperanza de que el doctor Raúl acudiese por
la noche, a pesar de que se había repetido a sí misma el que no vendría. Sabía que, en cierta forma,
lo había defraudado.

Se levantó. El Mago esperó en vano la lisonja
acostumbrada y quedó silencioso, ofendido en su
enorme vanidad, esa terrible vanidad de los genios
que de tal manera los acerca a la imperfección común a los mortales. Y Andréïda se dirigió hacia donde se encontraba sentada la señora León, célebre escritora, de grandes y bellísimos ojos castaños, atavío elegantísimo y conversación pausada e inteligente. De ella decían, las personas malignas, que gustaba en exceso de los buenos perfumes y de los
amantes de alta posición política que podían colocar
a su anciano esposo en puestos de importancia.

El elemento masculino predominaba en las reuniones de Andréïda; pues a excepción de la señora
León, de María Campa, dramaturga que peinaba su
oscura y naturalmente rizada cabellera con severidad monjil y cuyos delgados labios se apretaban en
una dura voluntad de triunfar y de excluir a su
paso, con aire de insoportable suficiencia todo juvenil esfuerzo, quizá celosa de una posible rivalidad
futura por ignorar que ella no puede hacer daño
nunca al verdadero mérito, estaban, además, Mónica Rey, escritora de parlar de pájaro, deliciosa
mujercita de ojos azules y cabellera clara, cuya dura lucha por el pan diario la había impedido el desarrollar, en forma, su indiscutible talento, malgastándolo en amables artículos dedicados a la mujer,
y en los que, por su modalidad ligera, pasaban desapercibidos verdaderos destellos de una originalidad maestra. Por último, sentada junto a Mónica
Rey, estaba la tía de Nelly, gorda y vulgar mujer
que se aburría estrepitosamente y contaba en voz
alta, a todo aquel que quería oírla ,las excelencias
de su difunto esposo.

Reinaba en el salón de Andréïda esa grata informalidad que permite se reúnan en grupos personas
afines, abolida la convencional tiesura, grave y molesta que suele establecerse cuando se intenta forzar
una conversación general.

A invitación de Nelly se pasó al comedor, y bien
pronto las conversaciones particulares que ya comenzaban a languidecer, se convirtieron en un murmullo animado y alegre, en el que la risa ligó la
cordialidad de tan diversos y heterogéneos intereses
y gustos individuales.

Con su peculiar gracia chispeante y aguda ironía, Torritos comentó el último panamá descubierto
a la entrada de las nuevas cámaras, y del cual se
hablaba en aquellos días, con gran escándalo, en las
corrillos camerales y en todos los periódicos. Un
diputado presente salió a la defensa de sus compañeros; pero el modo débil e ineficaz como arguyó
no hizo más que agravar el fondo de verdad de las
audaces aserciones del periodista, cuyos sarcasmos
sangrientos provocaron la maligna risa de todos los
presentes. Por otra parte, nadie dió gran importancias a aquéllo, sabiendo, de sobra, que después
de unos días de dimes y diretes quedaría todo, como
siempre, en silencio y archivado. Dió motivo, sin
embargo, para hilvanar las más picantes anécdotas
atribuídas a los poco escrupulosos legisladores, campeando en todas ellas esa falta absoluta de respeto
que, por lo demás, parece caracterizar la actitud de
un sentir general, y al que los propios padres conscriptos se prestan jocundamente, siendo ellos quizá
los más duros para calificarse a sí mismos. 

Andréïda, que había intervenido, de manera accidental, en la conversación, se levantó iniciando
asi el retorno al salón. Ofrecióle el brazo el joven
escritor Farías y soportó de él dos o tres vaciedades, enunciadas con voz lenta y opaca y una grave
y formal intención de hacerlas aparecer hondamente espirituales; pero que en Andréïda no lograron
el efecto que en otras mujeres estaba él soberanamente convencido que producían.

Alguien rogó a Andréïda que tocara al piano y,
con desganada complacencia, se dirigió la joven a
la esquina del salon en donde se encontraba un precioso y minúsculo Baby Grand, sabiendo, de sobra,
que en cuanto ella tocara la primera nota se reanudarían con estrépito las conversaciones. Sentóse
frente al piano y pasó con ligereza sus afilados dedos por el teclado. Al punto sintió a sus espaldas
que se acercaba el señor Ramírez con grave lentitud de grueso paquidermo. El rico industrial jabonero que había logrado de Torritos el ser introducido en el círculo de íntimos de Andréïda, aspiraba,
con terquedad de hombre de negocios acostumbrado
a satistacer sus caprichos bajo la base de “compra-venta”, a lucir una querida tan decorativa como insensatamente pensaba podría convertirse Andréïda.
La mirada torpe de aquellos ojillos redondos y codiciosos produjeron en la joven una sensación de repugnancia que la obligó a estremecerse y tentada
estuvo de levantarse. Se disponía a hacerlo cuando
interrumpió su movimiento un fino chirrido producido por las argollas del pesado cortinaje que incomunicaba al salón con el hall de entrada. Volvió
la cabeza y sus ojos se encontraron, súbitamente,
con los del doctor Raúl. La presencia del profesionista despertó en Andréïda un interés nuevo por
aquella velada que, como ninguna, le había parecido
aburrida e insípida.

Con las manos tendidas y los ojos chispeantes
se: dirigió al recién venido, como a un antiguo y
querido amigo que ha dejado de verse largo tiempo,
y procedió a hacer su presentación. Resultó que el
doctor Raúl era antiguo conocido de la mayor parte
de los ahí reunidos y, además, buen amigo de Torritos, quien se apresuró a inquirir, un tanio indiscretamente, por aquella amistad que él desconocía
tuviese Andréïda con el famoso y brillante joven
profesionista, cuyo último sensacional descubrimiento de un nuevo procedimiento de anestesia total,
era la admiración del México estudioso. Ambos se
excusaron de contestar, cambiando una alegre mirada de connivencia que provocó más de un comentario malicioso entre los presentes, y, acito seguido,
el doctor Raúl rogó a Andréïda que no se abstuviera
de tocar al piano por causa de su intempestivo
arribo.

En el breve trayecto hacia el piano, el doctor
Raúl interrogó a Andréïda:

—¿Me esperaba?

—A decir verdad, no —mintió la joven, e inmediatamente añadió—: pero me ha alegrado el que viniese.

No explicó Andréïda el por qué de esta alegría
y se limitó a sonreírle con gracia.

En la actualidad Andréïda no practicaba mucho
el piano; pero en su niñez había empleado, forzada
por su madre, muchísimas horas sentada sobre el
que en aquélla época y con una animosidad infantil, había llamado el banquillo de los suplicios. Ahora solía hacerlo con una fruición fervorosa y un
placer íntimo que daba singular expresión a todo
lo que tocaba.

Extrañamente satisfecha de tener a su lado al
doctor Raúl, y libre de la molesta sensación que le
había ocasionado la presencia del rico industrial a
sus espaldas, quien, por cierto, a la llegada del profesionista parecía haberse sumido en un amplio sillón, Andréïda se sentó otra vez al piano y palpó
las teclas casi con devoción.

Se inclinó ligeramente hacia adelante y sobre el
raso blanco del traje se dibujó punzante la admirable firmeza del busto como el de una magnífica y
joven diosa griega rediviva. Un arpegio rápido y
nervioso reafirmó su confianza en la distancia conocida de las teclas, y obligó a sus hombros a estremecerse de manera leve. Se reconcentró un momento, deseosa de apurar un breve goce artístico, goce
que algo le decía se vería compartido con aquel sér
extraordinariamente sensible, a toda belleza, que
sabía era el doctor Raúl, y fijando sus admirables
ojos en el enorme jarrón chino, colocado cerca del
piano sobre esbelta columna de ágata y que sustentaba un ramo de gladiolas blancas cuyo resplandor níveo, casi cálido, parecía palpitar en la penumbra del rincón, sus afilados dedos comenzaron a decir, con una juvenil dulzura sonora, el Vals Gracieuse de Dvórak, para después pasar, casi sin transición, a la jocunda alegría reconcentrada de Les
Sylvains de Chaminade. Andréïda dió a los sonidos
toda la intención maliciosa y bucólica de estos faunos de Chaminade, seguramente acostumbrados a
retozar sobre pulcros prados versallescos y a ser sorprendidos por Watteau en atrevido escorzo con preciosas ninfas de cabellos empolvados y de lozanía
un tanto ajada.

En seguida, Andréïda ligó las notas del Pensée
Fugitive de Karganoff. Sus dedos adquirieron una
ligereza alada, invadidos por una profunda y repentina femeneidad. Y todo su ser pareció tenderse en
un vago anhelo sensual.

Raúl la contemplaba fijamente, con una mirada
minuciosa de hombre y un pensamiento de amor en
los ávidos labios. Le pareció haber tenido su imagen grabada dentro del corazón desde el principio
de los tiempos y haberla encontrado, de súbito, en
aquel instante. No solo le gustaba su cuerpo soberbio modelado por el raso blanco del traje, sino el
perfume que de él se desprendía y que de tal manera lo enervaba hasta sentirse naufragar en una
alegría loca, la alegría intensa de experimentar en
el pecho un corazón nuevo, el corazón que ama por
primera y única vez.

Ahora los dedos de Andréïda se estremecían cada vez más lentamente sobre el teclado, como si agotado el placer se distendiese la terrible tensión del
espasmo artístico. La suavidad de las últimas notas se perdió entre el murmullo animado de las conversaciones en el salón, y al doctor Raúl le pareció
que Andréïda había tocado para él solo. Turbado
por la belleza de aquella interpretación delicada y
casi íntima, musitó conmovido, con una voz extrañiamente velada:

—Gracias…

Desde aquel trascendental minuto conoció el
doctor Raúl que Andréïda se le había entraco no
solo en el alma, sino en el cerebro y en sus sentidos
apresándolo entero, trastornando su concepto superficial del amor, empujáncolo inexorablemente
por el camino de una terrible pasión ilimitada hasta apurarla toda, con inacabable fruición. Se sintió
incapaz de reaccionar ante ella, con aquella volubilidad de mariposa que había merecido de sus amigos
el calificativo de incorregible voluble, y experimentó, de pronto, un terror oscuro. Abstraído en tales
pensamiento se dejó llevar por Andréïda, quien se 
había apoyado familiarmente en su brazo y se dirigió al grupo que formaba Torritos, sus dos jóvenes periodistas, el diputado López y un estudiante
de leyes a quien todos llamaban Gomitos y que hacía ostentación de su traje humilde, como de una
pintoresca fachada proletaria que no le impedía gozar de todos los privilegios muelles, ricas comidas,
vinos magníficos, etc., de sus bien acomodadas amistades. En aquellos momentos hacía obsequio a su
auditorio de lo que mal engullera, la noche anterior,
en libros rusos.

Torritos escuchaba con sardónica paciencia, esperando la oportunidad propicia para hacer exhibición, en frases de una acuidad precisa y bellamente
plástica de su gran amor al hombre y de su hondo
nacionalismo.

En el preciso momento en que Andréïda y del
doctor Raúl se acercaron, Torritos afirmó con hueca
entonación de alter magister:

—No es a la razón a la que debe precisamente
la inhumanidad su progreso, sino a las necesidades de
la época, hechas sentimientos generosos en el cerebro de media docena de hombres.

Los jóvenes periodistas apoyaron, con energía,
la redondeada frase, a tiempo que Torritos añadía
en el mismo tono pedagógico:

—Y el sentimiento no puede ser nunca abogado
de la reacción ni del quietismo.

El rico industrial Ramírez intervino débilmente..
desaprobó en frases incoloras ese afán destructivo
de la época. Su criterio mediocre, de burgués enriquecido de prisa, se oponía, con tozudez, a todo la
que significaba un cambio de postura que pudiera
amenazar sus egoístas intereses. Dispuesto estaba,
dijo, a fortalecer el erario del Gobierno con numerosas
contribuciones, siempre que éste guardara incólume
el orden de cosas existente. En cierto modo, él también aceptaba el predominio del estado, pero de un
estado capitalista.

Torritos tuvo una sonrisita exasperada para sus
frases, hasta que no pudiendo contenerse por más
tiempo, lo interrumpió ce esta manera:

—Doloroso, muy doloroso es, señor mío, el que
la parte orgánica, animal, de nosotros mismos, se
oponga al mejoramiento de la humanidad. 

Haciendo grandes muestras de mostrarse hondamente ofendido el Sr. Ramírez arguyó que él no
se oponía al mejoramiento del proletariado, s¡empre
que este mejoramiento estuviese dentro de lo posible. 

—¡Su posible es tan estrecho, señor Ramírez —comentó Andréïda con la deliberada intención de
poner en ridículo al capitalista. Intención que no
pasó desapercibida por los presentes en cuyos rostros se dibujó, en el acto, una sonrisa despectiva.

Gomitos terció con grave énfasis:

—Hay que defendernos contra la ciencia y la
industria convertidas en barbarie…

Y a Torritos no le cupo duda de que el estudiante repetía una frase leída, con seguridad, dos horas
antes, y se dispuso a escucharlo con una atención
risueña y divertida.

La disertación del estudiante versó sobre el punto de vista filosófico del socialismo como reacción
de la colectividad en contra del individuo, y de ahí
pasó a su aspecto económico, el cual se prestó mejor para su análisis. Algo flaqueaba en su exposición. Se notaba, a primera vista, que el joven estaba
imbuído por una vasta y mal digerida cultura teórica, cuyo empacho se hacía patente en el alambicamiento de dos o tres ideas centrales sobre las que
daba incansablemente vueltas, sin decir nada nuevo.

El diputado López escuchaba con gran atención,
constatando, en su cerebro obtuso, semejanzas y uniformidades asombrosas con frases escuchadas en
la tribuna y enunciadas por compañeros suyos, un
poco más despiertos, pero quizá menos bien intencionados que él mismo. El industrial jabonero se
sumía cada minuto más y más en la muelie blandura del monumental sillón, como queriendo escapar a aquel ambiente hostil que desconcertaba todo
el difícil equilibrio de su vida.

Hubo un momento en el que Andréïda quiso que
hablara el doctor Raúl; se empeñó en saber qué era
lo que pensaba el profesionista sobre ese movimiento universal que apasiona y estremece al mundo con
convulsiones de pelele destornillado y que, unas veces lo hace aparecer con ropaje pueril o cómico, y
otras terriblemente trágico. El doctor Raúl se resistió, estaba cohibido. Era hombre de convicciones
puras y de virtudes profundas. Sentía el dolor humano en sus propias entrañas; pero no creía en la
bondad de una fórmula para establecer la dicha universal, Todos los sistemas se le antojaban defectuosos; insuficientes por su misma concreción fija y poco flexible. Creía que las palabras eran el peor enmigo del pueblo, y se abstuvo de hablar.

Mientras tanto, Torritos, dijo:

—Las necesidades del hombre moderno han
marchado demasiado aprisa y se encuentran alejadas de sus precarias posibilidades de satisfacerlas.
Esta es la piedra miliar en la que baila trágicamente nuestro desequilibrio.

Esta vez el viejo periodista sintió el escrúpulo
de que él, a su vez, hubiera repetido un pensamiento ajeno.

—Estatismo, es el único partido político capaz
de solucionar la crisis —enunció, rompiendo su prolongado silencio, el escritor Farías.

—Igualdad, suprema igualdad —vociferó Gomitos, y obligó a Andréïda a saltar a la palestra, con
apasionamiento.

—¡Igualdad, pide usted igualdad! Hombre envidioso de todas la superioridades que desearía ver
a todo el mundo debajo de su importante persona.

El estudiante calló, disimulando su mala impresión con una risita cortés y versátil. Y amante de
cambiar palabras por cambiar palabras, pasó a
echar pestes y centellas contra el capital, llamándolo inmunda hiena que chupa la sangre del obrero,
acumulando y engrandeciendo su riqueza opresora
a medida que la miseria del oprimido es mayor. Preconizó, en frases violentas y corrosivas el advenimiento de la esperada dictadura universal del proletariado, el exterminio de la opresión capitalista,
la destrucción absoluta de la cobarde burguesia. Sé
lamentó en frases jeremíacas de la falta de solidaridad, previsión y sangre fría en el pueblo y su apatía hacia el interés colectivo que había estorbado,
hasta hoy, la hermosa realización de la nueva unión
de estados socialistas soviéticos de México, que perpetuarían el pensamiento y los procedimientos de
ese sublime iluminado que se hacía llamar, por los
rusos, Stalin. De ésto pasó a hablar de la complejidad de los fenómenos sociales y de las necesidades
económicas peculiares al suelo mexicano y, como
parecía no tener fin su flamante saco de extranjera ideología socialista, Torritos lo interrumpió con
parsimonia:

—Reconozco en usted, estudiante amigo, una recomendable erudición en estos achaques; pero también una joven inexperiencia que lo empuja a disparatar. Me parece también —añadió con sorna—
que usted olvida, con demasiada ligereza, dos realidades nuestras, las principales, o sean, primero,
la característica racial con su profundo sentimiento
de nacionalidad que repugna firmemente el injerto
de teorías extrañas, actitud que nuestras masas sintetizan, a menudo, mediante la manifestación clara
de sus intereses colectivos que están muy lejos de
oponerse a la nacionalidad y, que mejor aún, se funden en ella misma como en una alma común. Segundo que, nuestro pueblo ha hecho y sigue haciendo su revolución,, antes de que usted pudiera abrir
bien los ojos, jovenzuelo, y que, por lo tanto, es
precursor, nunca imitador vil. —Torritos hizo una
breve pausa y después prosiguió: —En cuanto a su
anunciada destrucción de la burguesía, en mi concepto, para ello bastará en México el solo cambio
biológico de una generación nueva educada bajo la
generosa idea del bien colectivo, que es el bien mismo del individuo. De la extinción de la vieja generación burguesa se encargará su propio miedo y su
egoísmo, su propia falta de sentido patriota, la desmoralización de sus costumbres, la vejez y la muerte. Y no lo olvide, pequeño, que solo lo nuestro responde a nosotros.

Pareció imposible añadir nada a las palabras de
Torritos que, de esta manera, marcaron ellas la
despedida.

Uno a uno los contertulios se despidieron hasta
que no quedaron en el salón más que Torritos y el
doctor Raúl, los que acabaron por marcharse juntos, invitado el primero por el segundo para subir
en su coche.

Ya en la puerta el doctor Raúl, sobreexcitado
por la fragancia excesiva de Andréïda que ponía
una sorda resonancia fragorosa en sus arterias,
oprimió su mano largamente, en una anticipación
dulce de posesión viril. La joven irguió la cabeza
con un fiero movimiento de rebelión; esculpida en
su bella boca la fría aspereza de una voluntad imperiosa. Y el doctor experimentó en sus nervios de
hombre un pavor físico de aquella fuerte criatura
que parecía discurrir por la vida con una extraordinaria seguridad de doblegar resistencias y esclavizar deseos.

Penetraron los dos amigos en el coche. El cerebro del profesionista se abrasaba en un mar de preguntas. Hubiera querido interrogar a Torritos quién
era Andréïda, qué misterio encerraba, quién era
exactamente aquella mujer maravillosa. Pero, ¡para
qué! Sus observaciones acumuladas en aquellas horas, su propio instinto, le habían empujado a identificar a Andréïda con la mujer temida de quien
había oído relatar más de una extraña historia. Había salido de la casa de ella con un disgusto fiero
en el corazón. Conocía a muchas de aquellas personas reunidas, con desaprensión, en su casa, y le había chocado, en extremo, ciertos giros atrevidos de
lenguaje, ciertas expresiones crudas de que a turno
habían hecho gala, tanto hombres como mujeres,
ciertas indelicadezas para tratar los más escabrosos
asuntos y, sobre todo aquéllo, una como suprema libertad de seres superiores a quienes debe permitírseles todo, por su altá condición, bien de dinero,
bien de inteligencia e ingenio o también por una
falsa pose radical de quien se pone el mundo por
montera. Ahora estaba seguro de que esta Andréïda
suya era la misma de quien repetidas veces le habían llegado ecos absurdos de su extraña existencia
brillante. Y, de nuevo, un frío intenso invadió su
espíritu. 

Y llegó también para él el momento de pensar
en una huída. De pronto había sabido que Andréïda
era la mujer destinada a significarlo todo en su vida. Junto al piano se había dado cuenta de que, ante ella, le abandonaba aquella voluntad suya que había contribuído a hacer de él el hombre fuerte que
era; traducida, en veces, en la aparente ligereza con
que sabía detener su deseo, el tiempo preciso, para
satisfacer superficialmente su gran necesidad de
amor.

Torritos, a su lado, se sumía en la niebla aromada de un cigarrillo, y apenas si el doctor cruzó
con él una frase trivial de cortesía al descender el
periodista frente a su humilde albergue.

El doctor Raúl tomó el volante de su hermoso
Coupé y se lanzó, a toda velocidad, por las calles de
México, desiertas en aquella hora. Poco después se
internaba en la Reforma, camino a las Lomas de
Chapultepec.

Entre los escombros de su temor permanecía
indemne, en toda su soberbia atracción, la figura
de Andréïda, Y el doctor se repitió que era ella su
gran amor, que con ella perdería, para siempre, su
preciosa libertad.

En el silencio profundo de la noche, el carro del
doctor Raúl corría con un sordo rumor de finísimos
caballos mecánicos desbocados con locura. Con el
rostro pálido, demacrado, un agudo surco inciso entre las cejas, el doctor pisaba, hasta el fondo, el sensible acelerador del maravilloso coche. Terriblemente excitado, mordiendo con fiereza el labio inferior, el vértigo de la velocidad parecía poner un paréntesis vacío en su vida. Recurdaba, a saltos, todas las palabras por ella pronunciadas en aquella noche, su sátira y su gracia, llena de malicia y de ingenio. Se hacía presente, a su cerebro, el más insignificante de sus gestos que eran parte de su fuerte personalidad. Sus innatas actitudes elegantes, su sensibilidad artística, su mirada dominante y, sin embargo, tan extrañamente cautivadora, El balanceo lento y casi imperceptible de su cuerpo que parecía sentir la fatiga de una prolongada virginidad, la espléndida fuerza que se desprendía de ella toda entera, el perfume de su carne, intenso y tumultuoso,
de su carne blanca y nada casta y… la adoró. La
adoró en la noche con una exaltación fanática y terrible. La adoró como belleza y como fuerza, desechando todos sus temores, haciendo a un lado todos
sus razonamientos.

La amó como el hombre fuerte que era, con el
delirio de quien se alza sobre el terror fundamental
del alma, siempre medrosa de divinidad infinita. La
amó con el deseo de posesión absoluta que erige al
hombre dueño del mundo.

Embriagado por la fuerza misma de su deseo,
dijo en voz baja:

—Te amo, Andréïda, te amo. Quiero que seas
mía, tendrás que ser mía.

Entonces, con mano firme y una tranquilidad amenazadora tomó la difícil curva que desciende y gozó plenamente del recuerdo de su carne no
casta, del fuego luminoso y profundo de la cabellera magnífica que sabía del goce dulce del cro y del
rojo tono cálido y turbulento de un sol de otoño. Besó con el pensamiento las manos pecadoras que sabían arrancar al piano notas lentas, impuras, insidiosas, cual caricias demasiado voluptuosas y que
martillaban el oído con pensamientos ardorosos y
quemantes.

Desde aquel momento, el hombre no debía tener más que un anhelo, el de hacerla definitivamente suya, ligarla a él por una eternidad.

El obscuro pasado, el amenazador presente, el
misterio del inexplicable hielo en los maravillosos
ojos, la amargura abrumadora y sorda de una ambigua adversión en la hermosa boca, su actitud de
eterna defensa rechazadora, todo huía hacia atrás,
hacia la imprecisa vaguedad de lo que ha dejado
de ser y se hunde en la memoria.

¡La amaba, la amaba! Desde hoy su vida se veía
henchida de una alegría desconocida, de un nuevo
interés apasionante, llenando con su presencia el
mundo.

¡Qué hermoso era el color magnolia de su finísima piel! Qué dulce y tentadora la humedad roja
de sus labios! ¡Qué fascinación tan terrible la de
sus bellísimos ojos! ¡Qué voluptuosidad tan honda
se desprendía de su maravilloso cuerpo modelado
en la suavidad del raso blanco, salpicado de violetas!

El doctor Raúl sintió dentro de si una fuerza
nueva, una potencia violenta y vibrante que le circuló por las venas, y se hizo en sus labios grito anticipado de victoria.

—¡ Mía, mía, mía!

Todo su pecho, fuerte y viril, se expansionó largamente en la maravillosa palabra, en la palabra
brutal, signo del Mundo.

</section>
<section epub:type="chapter" role="doc-chapter">

#Capítulo IV

Para Andréïda y Raúl empezó un período de vida febril y apasionante. Raro era el día en el que
el doctor Raúl no encontraba un pretexto para verse
con Andréïda. Se había convertido en su fiel acompañante y en un asiduo de la casa, tanto como el
propio Torritos. El espacio de tiempo en el que las
exigencias del trabajo de ambos los obligaba a separarse, le parecía, al hombre, eterno.

Mientras tanto, Andréïda, sobre la viril efervescencia que la envolvía en una sublime adoración
gloriosa, pisoteaba, inexorable e indómita, las leyes
fundamentales de la vida, y de entre las cenizas de
su destrucción erigía con sus propias manos su sueño triunfal, su creación de sí misma, esplendente y
soberbia, con una impunidad temeraria y magnífica que se antojaba fuera de la posibilidad humana.

La mujer nueva se preparaba a afirmar su potestad anárquica en el vasto escenario de la comedia
humana, sola y magnífica, sobre los prejuicios que
esclavizan a los mediocres., Deseaba imprimir un
nuevo signo, no importaba que éste fuera mínimo
siempre que perdurara como definitivo.

Aquel juego trágico, en el que el hombre se había abstenido de pronunciar la palabra trascendental, la llenaba de una extraordinaria alegría, proporcionándole el más hermoso de los espectáculos,
el espectáculo soberbio de su propia fuerza en la
más bella de las plenitudes.

Su enorme orgullo diabólico, suprema razón de
su vida, la precipitaba, con una especie de loca embriaguez, a creer en sí misma, como en una inusitada y flamante divinidad, haciendo un sacerdocio
de la belleza perecedera de su materia.

Ella, la mujer libre de milenarios prejuicios, la
valiente apóstata de los huecos dioses erigidos en
oropelescos ídolos por una humanidad, ancestral y
eternamente necesitada de ideal, empuñaba su propia vida mostrándola al mundo como revelación nueva de superioridad, como exaltación divina de la
materia opaca, liberada de la oscura garra de los
sentidos y rescatada, en forma triunfal, de la sima
abyecta que hace imposible toda posible divinidad.

Pero Andréïda, la mujer nueva que había encontrado, de improviso, un significado desconocido
a la vida, alejado de la verdad quizá tanto como el
lírico sueño de un grande poeta, olvidaba, en su temerario vuelo condorino y libérrimo, la única inmortalidad accesible a lo humano, o sea, su perpetuación en el mañana infinito, por medio de su propia especie.

Entre tanto, la impenitente soñadora que era
en el fondo, seguía plasmando en el vacío su fugaz sueño de rebeldía y se dejaba llevar por la impetuosa corriente de la vida, creyendo, la insensata,
que de su hermoso bajel era ella el firme timón. Todavía experimentaba la misma compleja hostilidad
hacia la ilusión pasajera y engañosa de que se sirve
la naturaleza para crear los nuevos eslabones destmados a perpetuar su cadena sobre la tierra.

Por lo demás, ella era la primera en desear la
compañía del hombre. Apreciaba en el doctor Raúl
una fuerza, si no mayor de la que ella se creía dueña, si tan hermosa y relevante como la suya propia, y, por lo tanto, digna de medirse en una posible
y futura heroica batalla.

Con grave inconsecuencia de la que no se había
detenido en buscar explicación, Andréïda se solazaba en la cercanía del doctor Raúl. Le agradaba,
en exceso, sentirlo a su lado, espiar en silencio sus
emociones de hombre, impulsar, con una perversa
ingenuidad, su repentina torpeza y sus desaciertos,
escuchar, de pronto, cómo la bella voz masculina
desfallecía y se tornaba insegura, acariciándola toda entera no sabía ella exactamente en qué sublime
forma.

Pero, como parecía mediar entre ellos un absurdo pacto, no pronunciado, que había hecho posible
aquella íntima amistad, sostenida en los límites de
una camaradería, esto permitía a Andréïda el que
en sus momentos de buen humor, llamara al amigo,
en estricta justicia, camarada Raúl.

Ni una palabra de amor por parte de él que se
abrasaba entero en el violento fuego de su pasión.
Ni una insinuación cobarde, ni el más leve gesto que
denunciara el deseo oculto y contenido de su virilidad. Acaso, en la mirada varonil una como dulzura fogosa y en los labios altivos una avidez insana. Eso era todo.

Por parte de ella, una delectación demasiado ostensible y una imperiosa necesidad de la presencia
de él ; lo que ponía, en aprietos, la sutil dialéctica de
su pretendida superioridad e independencia. Quizá
también, allá en lo hondo de la entraña femenina
una vitalidad excesiva, una rebelión de la materia,
a duras penas reprimida, y que se abría oscuramente paso en un incunfesable deseo de ser oprimida
bajo el peso del homibre, y que trataba, a todo trance, de hacerse presente en un prodigioso instinto
de salvación en contra del pensamiento que pretendía, de manera premeditada, destruir, a fuerza de
inteligencia, la prosecusión infinita de la vida.

Una noche en la que el hombre se había mostrado deliciosamente espiritual, y en apariencia libre
de su obsesionante deseo de posesión, Andréïda acarició, por un momento, la idea de entregarse, abierta a su amor. Claro es que a un amor intrínsecamente cerebral, estéril y divino, lejos del envilecimiento de la carne.

Sobre la maravillosa boca de la mujer vagó una
conrisa ambigua que enloqueció al hombre, obligándolo a cerrar los ojos y a crispar las manos con un
impetu salvaje. Pero el momento había pasado. Una
vez más, Andréïda se sintió aprisionada en su sueño temerario y su enorme voluntad negativa la obligó a proseguir el camino que una mujer andaba,
por primera vez, sobre la tierra. 

Y los días tornaron a deslizarse cada vez más
de prisa.

La mujer, convertida en confidente, conocía del
hombre su más recóndito pensamiento, su vida pasada, sus anhelos de estudiosos, sus antiguos amoríos vacíos, de mujer en mujer, persiguiendo inútilmente el verdadero amor y que aseguraba le
habían dejado siempre sabor de ceniza. en los labios
y en las manos viriles el juguete roto de su ilusión.

Andréïda conocía al hombre y el hombre temía
ahondar en la mujer. Y la vida fluía incansable, como un ir y venir de oleaje tempestuoso. Las confidencias se detenían precisamente allí donde el hombre se sentía impotente para dominar su deseo. Entonces, la mujer entornaba los ojos y se dejaba acariciar por el pensamiento tumultuoso del amigo.

Le enorgullecía su adoración humilde. Recibía,
con agrado, el delicado incienso de sus ojos, sin preocuparse mucho de que, a veces fuese sobradamente
íntimo. Desplegaba para él su atractivo irresistible,
su fecundo ingenio, tejiendo en su exclusivo absequio las más adorables conversaciones con un deseo
delicioso de agradarle y de mostrarse encantadora.

A veces también, sin motivo, el hombre se tornaba brusco, caprichoso, cruel. Ella, en castigo, le
privaba de su compañía con cualquier pretexto, y
al otro día el hombre retornaba humilde, sumiso,
con un grato madrigal en los labios para su figura,
su gracia, el dibujo encantador de sus líneas, el
bien de su hermosura que era grato regalo a los
ojos mortales y solía insistir, con puerilidad un tanto estudiantil, en que el cielo y la tierra eran marco indigno para su real hermosura. Después, se quedaba largo rato en silencio, contemplando los sorprendentes juegos de luz en los cabellos ardientes de la bien amada, observando cómo la bugambilia roja
del jardín hacía descender sobre los ojos de Andréïda una suave sombra. A plena luz del sol, y que
él tanto amaba, le parecía descubrir siempre nuevas
delicadezas en el bellísimo rostro, se sorprendía de
que la claridad resplandeciente favoreciese tanto
sus bellas líneas. Entonces, las pupilas viriles de un
gris azul, punteado de oro, y que ya a ella le llamaran tanto la atención la noche de su primer encuentro, reían con un gozo insensato que contagiaba, en forma irresistible, a la boca, obligando a los
labios a descubrir el brillo ardoroso y dulce de los
dientes voraces. Con una mirada que la envolvía
entera y la hacía suya, el doctor se marchaba bruscamente, llevando en el cerebro la desnudez voluptuosa de las finas manos que no se atrevía a tocar,
por miedo a inclinarse en loca adoración por los dedos finísimos, la frágil muñeca, la deliciosa palma suavísima, maculada de líneas misteriosas. Su
memoria objetiva le recordaba el perfil inteligente
y puro del rostro y todas las bellas formas que le
satisfacían tan plenamente. 

Andréïda lo dejaba marchar, y en la túrgida
garganta retozaba una suave risa, Le gustaba, sobre todas las cosas, turbar su serenidad de hombre.
Su gozo único era sentirlo cerca, oír su voz que
de manera tan perfecta sabía halagar sus vanidades
recónditas. Hilvanar con él largas conversaciones
haciendo intercambio de goces espirituales y alados. Gracias a Raúl había encontrado a su vida un
nuevo sentido grato, interesante, desconocido antes
por ella. Su aliento, el roce accidental de sus manos, la mirada profunda y seria de los ojos viriles,
habían despertado en ella voluptuosidades desconocidas. En un principio, lo había juzgado como un
ser positivista, desconocedor de una impetuosa pasión. Ahora lo encontraba soñador, deliciosamente
espiritual y comprensivo. Andréïda estaba decidida
a conservarlo.

Mas aquel juego trágico, de superficiales perfiles amables, no podía seguir indefinidamente en
aquel estado. Y un día encontró Andréïda a Raúl
en exceso abstraído, indiferente, impenetrable.

Ese día la citó, por la noche, para encontrarse
en el Teatro Alameda. Acudió ella, como de costumbre, deseosa de pasar un rato en su compañía. Antes de pasar al salón de exhibiciones le rogó él que
subiera al primer piso para tomar alguna cosa. Algo intranquilizador en la nueva actitud de él hizo
que Andréïda permaneciese en guardia, Insensiblemente ambos buscaron la penumbra de un extremo
y fueron a sentarse en uno de los amplios y lujosos
divanes.

Desde la mañana, la clara inteligencia de Andréïda le había mostrado un cambio sensible en el
ánimo del amigo, por lo que con manifiesta volubilidad inició una conversación banal que llevaba el
propósito de retardar, lo más posible, cualquier explicación. Pero grandemente desconcertada de verlo
abstraído, imperturbable y en apariencia tan poco
preocupado por ella, aquella situación ambigua
pronto la causó enojo y así hizo sabérselo, con entera franqueza, a tiempo que se dolía de su ausencia mental. La frialdad con que se excusó él pidiéndole mil perdones con una cortesía un poco impaciente, la hizo sentir una especie de angustia y, por
primera vez, se creyó impotente para descifrar la
cruel displicencia y hastío que mostraban los labios
del amigo.

Y tocó a ella ahora el mostrarse inquieta, desconcertada, impaciente y hasta tímida. Pensó dejarlo, pero le pareció, de improviso, que su femenina estratagema no surtiría el efecto acostumbrado. Por primera vez, también, creyó no interesarle y parecerle inoportuna y ridícula.

Una larga pausa se estableció entre ellos, hasta que el hombre rompió el silencio con voz opaca,
sin modulaciones:

—Hoy le pedí que me acompañase, Andréïda,
porque tenía que decirle algo importante.

A pesar suyo, la mujer sintió que el corazón le
latía precipitadamente.

—He decidido —prosiguió el doctor Raúl con voz
incolora— tomarme unas vacaciones en Europa y
pienso abordar el vapor en Veracruz, la semana entrante.

Andréïda permaneció silenciosa. Comprendió, al
punto, que la temía y quería huirla, mas no osó confesárselo.

—No es una repentina decisión —añadió el doctor haciendo una innecesaria aclaración. —La he reflexionado largamente.

Ella no hizo observación alguna. Por un instante, los ojos del doctor Raúl se posaron en Andréïda
con todo el dolor de una suprema renunciación que
traicionó, del todo, su ficticia frialdad. La veía a su
alcance, más deliciosa que nunca y más deseable
también, con un sabor carnal más vivo y un perfume humano más natural. Midió, con un súbito terror, el vacío porvenir que le esperaba, privado de
su cautivadora presencia y, por un instante, sus
párpados se abatieron repetidamente, con un ardor cobarde al que sabía un hombre jamás debe ceder. Y se alejó unos pasos, sin esperar respuesta.

Después, reconquistada su heroica frialdad volvió
a sentarse.

Las primeras frases de Andréïda resultaron premiosas, difíciles, le habló del beneficio que reportaría ese viaje a su profesión. El se rebeló ante la
aparente incomprensión de ella, e, irritado, arguyó
con brusquedad, que sus viajes de estudio los solía
hacer cada dos años y que éste, afirmó, sería exclusivamente de placer.

—¡Qué violencia la suya! —exclamó Andréïda, y
el doctor Raúl se mordió los labios con rabia, En seguida, dejó caer al oído de la mujer y sin intentar
siquiera templar el ardor de sus palabras, lo siguiente:

—Usted sabe, de sobra, el porqué de mi repentina marcha.

Andréïda se estremeció de miedo y de gozo. Al
final había vencido ella y aguardó con impaciencia
el que él pronunciase la palabra trascendental. Mas
el hombre no se humilló a decirla.

Con un enfriamiento de toda su carne y de toda
su alma Andréïda temió, de pronto, el vacío de su
ausencia y no quiso perderlo. ¡No quiso perderlo!
Le gustaba tanto ahora que lo veía mudo y sombrío, dueño de toda su fuerza, que fué ella quien
descendió a decirle:

—¿Y, si no quisiera yo que se marchase?

El doctor Raúl elevó sus claros ojos, punteados
de oro. Primero fué una mirada humilde y gozosa
que conmovió extrañamente a Andréïda; después
el brillo oscuro de una resolución irrevocable que no
podía ser engañada. Aquéllo la atemorizó, perdiendo, for un segundo, su enorme confianza en sí misma.

Con voz de pronto sumisa, impregnada de una
tristeza humilde, ella le dijo que no quería ser la
causa de su partida. Se acusó valientemente de un
imperdonable egoísmo por haber abusado de las horas fáciles y deliciosas de su amistad, las cuales le
habían proporcionado el placer de sentir a su lado
una inteligencia con la que encontraba similitud
de gustos y de ideología. Le confesó, también, que
sentía la necesidad de verlo en todas partes y a todas horas y que quizá, por ello, había procedido con
ligereza al prodigarse a sí misma, inspirando en él
un sentimiento del que el hombre no puede nunca
verse libre al contacto de cualquier mujer, por insignificante que ésta sea.

El doctor Raúl escuchaba, escuchaba. Le hacía
daño el oírla, le faltaba el valor de seguir sufriendo
en silencio, se sentía impotente para sostener aquel
difícil acaliamiento de su deseo. Y su dolor acabó
por estallar con toda la fuerza de una pasión tempestuosa, largamente contenida.

Se quedaría sí, dijo, pero con una sola condición. No quería ya de ella la suave y vaga ternura
de su dulce compañía, no quería su ambigua amistad, la quería toda entera para sí, y de esa manera
se lo hizo saber, con un cruel ardor obstinado.

Andréïda se rebeló, él conocía, le dijo, su manera de pensar. No sacrificaría nunca su ideal a una
abyecta relación carnal. Quería al amigo, solo al
amigo, y de él tenía una necesidad imperiosa de
seguirlo viendo. Le tendió la mano con un gesto que
era a la vez pacto de amistad e inconsciente y desesperada necesidad de aprisionarlo.

Con supremo y brusco orgullo él se negó a tomarla. No quería, repitió, más ficticia amistad, no
deseaba seguir prestándose, por más tiempo, a la
indigna comedia moderna de una falsa camaradería
que él consideraba no podía verdaderamente existir entre hombre y mujer jóvenes. Con una violencia que Andréïda le desconocía barbotó sobre ella
cargos brutales e injustos, la calificó de provocativa y cruel, de mujer de pensamiento monstruoso,
incapaz de amar. Le dijo que la odiaba y que lo
único que pedía al porvenir era borrarla de su corazón, olvidarla del todo en su pensamiento. Con
voz extrañamente desesperada y sorda le describió,
con dureza, todos los deseos ardorosos que había
tenido que sofocar en un supremo delirio por aprisionarla entre sus brazos. Después, con extravío,
enumeró las gracias de ella que más le cautivaban,
su andar elegante, el movimiento atrevido y desafiante de su cabecita, el perfume de sus labios y de
su carne, su orgullo espiritual que con tan grande
tranquilidad sabía decir escuetamente ¡no!, ¡sí!,
y, sobre todo ésto, su hermosa alma femenina que
se revelaba entera y en el menor detalle, a pesar de
lo extraño de su pensamiento.

Cediendo al deseo apremiante de su enorme deseo de hombre, posó repentinamente sus labios violentos sobre los frescos y virginales de Andréïda.
Fué un beso breve, carnal, impetuoso que cedía al
maleficio sensual del fragante olor de ella. Después
y antes de que Andréïda pudiese protestar, besó
con humildad su mano, a tiempo que exclamaba con
voz sorda y emocionante:

—¡Oh, la amo, la amo, cuánto la amo!— y su
pasión floreció esta vez en frases delirantes, atrevidas, con una sencillez salvaje que no conocía la resignación y en la que el instinto primitivo del hombre se desbordaba.

Aquel beso arrebatado a la integridad femínea
de Andréïda pareció calmar, un tanto, su exaltación viril; porque, poco después, pasó a repetirle
lo que había sido para él la vida sin ella. Una sucesión de fáciles deleites, un coger el fruto del
árbol sin fatiga, en lo que si bien no había experimentado un goce intenso tampoco había sufrido
una gran desesperación. Dijo que, hasta conocerla,
había vivido sin saber que vivía; pero sin ser ciertamente desgraciado, como lo era ahora.

Viéndolo ella en más calmado estado de ánimo,
creyó que le sería fácil aplacar su deseo. Erróneamente pensó que el desbordamiento, en palabras,
de su gran dolor lo había serenado y, con una sonrisa subyugadora, volvió a tenderle la mano.

—Sigamos siendo amigos, —le dijo— le perdono
sus palabras, noestoy enfadada.

El doctor Raúl la miró con los ojos enrojecidos
por una desesperación mortal y balbuceó:

—No me comprende, es inútil, adiós. Me iré lejos, adiós… Me engañé cuando le dije que sentía
odiarla, no es cierto, la adoraré siempre, siempre,
por una eternidad… Pero me iré…

Se levantó. Andréïda lo vió marchar, cegado
por el dolor agudo de su pasión no compartida. Tropezó dos o tres veces con varias personas, pidiendo
mecánicamente perdón. Y de pronto, Andréïda recordó otra mirada igual, otra desesperación idéntica que se deshizo en sangre en no muy remoto
rasado, y tuvo miedo, un terrible miedo de perderlo. A su vez se levantó con violencia y lo siguió
llamándolo por su nombre, sin que él pareciese
oírla. Por fin, logró colgarse de su brazo en la
confusión de la muchedumbre que entraba y salía
del teatro. Estaba dispuesta a ofrecerle el sacrificio
de su gran renunciación a cambio de evitar una
nueva tragedia.

Había tal deseperación en los ojos de ella que
él, sensible de pronto a su dolor, la tranquilizó con
dos o tres frases que vibraron a pesar suyo, con
una gran ternura.

Caminaron por el callejón de Coajumulco hacia
las calles más oscuras de México. A medida que andaban el pensamiento de Andréïda reaccionaba.
Otra vez en juego su sutil dialéctica le mostró que
lo que el hombre deseaba bien podía no ser una renunciación, como ella había pensado al principio,
sino una suprema prueba que a ella misma le hacía
falta para someter su personalidad y conocer, en
toda su extensión, la magnitud de su triunfo íntimo. No implicaría, por lo tanto una forzosa claudicación, sino la más temeraria y definitiva de las
afirmaciones.

El hombre se detuvo un instante a media banqueta. La miró largamente en los ojos, avizorando
esperanzado un tenue brillo amoroso en la mirada
amada, y dijo:

—Andréïda la ansío, ¿no comprende a qué horrible tormento me ha sometido todo este tiempo?

Después, de nuevo desesperado, añadió —Déjeme…
yo le suplico rendidamente que me deje.

La voz viril sonaba en la noche con un divino
dolor suicida.

Andréïda lo miró intensamente, luego con voz
extraña, hermética, firme y pausada, musitó:

—Haga usted de mí lo que quiera…

La garganta del profesionista se estremeció en
un sollozo hondo, convulsivo; su mirada tuvo un
brillo de locura. Lentamente se pasó el dorso de la
mano derecha por los ojos deslumbrados, hasta que
pudo balbucear con voz desfallecida:

—Andréïda, no se burle de mí, ¡sería capaz de
matarla si supiese que se burlaba de mí!

La joven sostuvo su mirada con fiereza, mientras en lo más hondo de su ser se libraba una terrible batalla, de la que ella misma no sabía si saldría
triunfante.

De repente él la atrajo contra su pecho, imprimiendo otra vez sus labios y su alma en la boca
amada que con tal sencillez había sabido brindarle
el acceso al paraíso. Ella se desasió con suavidad,
posesionado de sus ojos, nuevamente, el hielo extraño. El dejó caer con desaliento los brazos a lo largo del cuerpo.

—¡Qué se propone usted! —exclamó.

—Se lo he dicho ya, lo que usted quiera— y la
mujer sonrió valerosamente, a tiempo que el hombre
frenético, esclavizado por el deseo brutal, interrogaba:

—¿Cuándo…? —y con bestial premura—
¿Mañana?

Un grito instintivo y angustioso de defensa
brotó de los labios femeninos, mordiendo el aire
tibio de la noche.

—No, no, mañana, no.

—Ve usted— sollozó el hombre, presa de un
terrible vencimiento interno— ¡No me ama, ay,
no me ama!…

—Es demasiado pronto, necesito hacerme a la
idea —replicó la mujer temiendo una nueva explosión de viril desesperación. Y pasó a marcar el
día, cualquiera de la semana entrante, dijo. El
facilitó el camino; rápidamente elaboró un sencillo
plan. Irían, dijo, por la tarde del sábado a Cuernavaca, regresarían al día siguiente diciendo a
Nelly que había sufrido una descompostura el automovil, en fin, cualquier pretexto sería bueno para
la meteórica ausencia.

Se separaron. El hombre tenía un suave agradecimiento en los amantes ojos y una anticipada voluptuosidad estremecía su carne viril, imprimiendo a su morena tez una palidez mate. Y Andréïda
se dijo que la vida era ruin y hacía ruín al mejor
de los hombres, Sin embargo, le había satisfecho, en
el fondo, el que él no se mostrase demasiado gozoso y triunfante.

Aquella noche, la hermosa cabecita femenina se
revolvió en la almohada con impaciencias rebeldes.
Abominó la brutalidad de los hombres que no saben poner un límite a su deseo de posesión. Sin
sombra de vergiienza por lo que había prometido,
se abismó en una inquietud pcr descifrar el misterio
de su alma, haciendo, con una admirable tranquilidad, la disección de su corazón; tranquilidad seguramente parecida a aquélla con la que el doctor
hundía el bisturí, en el silencio de su laboratorio,
desgarrando la carne inocente de sus conejillos.

A pesar suyo, Andréïda, no sentía demasiado la
repentina resolución tamada, es verdad, bajo la
coacción de la violenta y amenazadora desesperación de él. Una y otra vez se repitió a sí misma
que aquéllo no significaría una claudicación irreparable, sino más bien su gloriosa afirmación. Y
con tal pensamiento se quedó profunda y dulcemente dormida. Una sonrisa angelical vagó toda la
noche sobre sus labios jugosos y encendidos, ya
profanados por el hombre…

</section>
<section epub:type="chapter" role="doc-chapter">

#Capitulo V

Los días de aquella semana se le antojaron a
Andréïda que habían pasado aún con más fantástica rapidez. Sobre todas las cosas había temido volver a ver al doctor Raúl; pero éste, con delicada
discreción se había abstenido de ir a la casa y había
guardado un silencio respetuoso hasta la misma
mañana del señalado sábado, en la que, por medio
de Nelly, hizo saber a Andréïda que pasaría por
ella a las cuatro de la tarde en punto.

Y en punto de las cuatro, con inconfesable sobresalto, escuchó Andréïda la llamada discreta y
conocida del claxon del coupé de Raúl. Al fin mujer,
su espíritu sufrió una terrible irresolución. Tentada
estuvo de encerrarse con llave y no acudir al llamado perentorio e insistente. Después, con una
mirada circular, sus ojos abarcaron la preciosa y
casta alcoba de soltera y pareció que en aquella
mirada abarcaba su propia vida pasada.

Con lentitud premiosa, dió los últimos toques
a su sencillo atavío. Y.. a despecho suyo, con todas las fuerzas zozobrantes de su voluntad que flaqueaba, por primera vez, un sentimiento atávico,
un prejuicio ancestral licuó en sus magníficos ojos,
dos lágrimas, dos brillantes lágrimas enormes que
rodaron lentamente, inexorablemente por su pálida
faz. Tuvo un momento de extravío, le pareció que
en el espejo de cuerpo entero se asomaba, en una
niebla vaga, su propio cuerpo envuelto en un níveo
atavío nupcial, y que tras ella se inclinaba sobre
su frente, todavía pura, la sombra protectora y
dulce de su madre, en un innominado movimiento
instintivo de defensa.

La atormentadora visión se borró del espejo y
no tardó en dejarse oír, en la blanca habitación,
una risita exasperada de mujer superior que obligó a las lágrimas a evaporarse con precipitación,
bajo los abrasados párpados. Con un movimiento,
habitual a las bellas manos, derramó unas gotas
de la esencia favorita en la suavidad tibia del pecho, y la mujer marchó, con una fingida despreocupación, a consumar su destino.

Raúl, inquieto, paseaba su impaciencia por la
banqueta, bullendo en su cerebro los más descabellados temores. Toda la semana había vivido como
en sueños, esperando aquel momento, y ahora casi
tamía que hubiese llegado demasiado pronto. Su
impecable traje gris claro, modelaba, con elegancia,
su alta estatura. Sus labios mordisqueaban, con
evidente nerviosidad un cigarrillo apagado. Y al
verla súbitamente venir hacia él, con una recelosa
lentitud, le fué, de pronto, imposible creer que era
su forma carnal la que se acercaba, llevándole en
las manos puras, con una sencillez emocionante, la
suprema donación de sí misma. Casi estuvo tentado
de caer de rodillas allí mismo en la calle, haciendo
partícipe de su emoción a todo aquél que pasaba
Sin embargo, se limitó a recibirla en silencio, con
una reconcentrada alegría que no quería hacerse de
masiado visible; pero que imprimía a su corazón
el cascabaleo triunfal del grito remoto, ancestral,
eterno, ¡Mía, toda mía, para mí solo!

El hombre, quizá por atávica costumbre, marchaba hacia el amor gozosamente, alegremente, despreocupadamente, atento solo a su placer. La mujer,
por el contrario, impregnaba el mismo camino con
una sutilísima y trágica tristeza que se antoja es
actitud exclusiva de ella, ejercitada desde la primera noche del tiempo, con ese espanto vago y emocionante de víctima propiciatoria a quien se obliga
a caer con violencia. La propia Andréïda, la mujer
que aspiraba a una superioridad nueva, no se veía
libre de este complejo femenino. Todavía, en el
momento de subir al coche, tuvo un último movimiento de retroceso, de vacilación, de huída; pero
la mano de él, temblorosa, palpitante, cálida, estrechó con dulzura la suya helada y le fué imposible huir.

El hombre sujetó el volante del coche con una
firmeza grave de dominador. El perfume que se
escapaba del traje cerrado de la amada lo enloquecía de deseo, y se sintió dueño del Mundo, con una
porvenir inagotable de dicha al frente; y apretó el
acelerador.

En cambio, Andréïda, experimentó la molesta
sensación de sentirse prisionera; arrebatada, a pesar suyo, hacia lo desconocido y lo temido, y le
fué dado medir, en esta vez, toda la intensidad
trascendental del paso heroíco que se proponía dar.
¡Iba a ser del hombre! Y sintió tentaciones de llorar por ella y por su ensueño, sí, de llorar, sencillamente, femeninamente, ella a quien desde pequeña le había sido muy difícil entregarse a este
desahogo espiritual.

Raúl se mostraba altamente comprensivo, como
bien pocos hombres en un caso igual. Trató de llevar, con ternura, la conversación por tópicos baladíes, insubstanciales, en un grato empeño de hacer
natural la extraordinaria situación. solo una vez,
al encontrar con su mirada los enormes ojos de
ella, agrandados por una especie de terror indomable, se atrevió a decir en voz baja, inclinado sobre sus labios:

—Perdóname, Andréïda, ¡pero tengo tanta necesidad de ser feliz!

Luego, manejando con un solo brazo, deslizó
el derecho por la espalda de la amada y oprimió
levemente su cintura, gozando el encanto de la
presencia femenina como un efluvio bienhechor,
alado, magnífico que renovó en sus arterias el antiguo ardor. Los ojos viriles se alargaron como bajo un deslumbramiento repentino, y abandonando
el talle de Andréïda guió, de nuevo, con las dos
manos e imprimió una carrera loca, premiosa, voraz, al maravilloso coche. 

Andréïda comprendió aquella súbita impaciencia brutal y se ruborizó intensamente. Era mujer,
a pesar suyo.

Minutos más tarde el coche se deslizó por la
hermosa carretera y no tardó mucho en internarse
en la montaña, El paisaje bellísimo vino a sorprender a Andréïda como si lo contemplase, por primera vez. A cada nueva curva atrevidísima se descubrían nuevas bellezas, y por entre la espesura de
los pinares era dado sorprender, de vez en cuando,
un girón de infinito, un horizonte azul y lechoso
que a lo lejos fingía la ribera azul y aperlada de
un mar en prodigioso quietismo. El aire de la altura azotaba el rostro de la joven y le producía un
placer indecible. Sus oídos comenzaron a zumbar,
insinuando una protesta leve contra la altura. Por
un instante, Andréïda olvidó el terror de su próxima inexorable consumación como mujer.

El automovil doblaba bruscamente las curvas;
pero la seguridad admirable y tranquila de quien
lo manejaba no le permitió a Andréïda sentir el
más ligero temor de un accidente. Los árboles entrelazaban sus ramas, tan cerca del cielo, que la
mirada de la joven se enredaba, por seguirlas, en
la infinita bóveda, intensamente azul.

Hacía unos minutos que Andréïda había divisado Tres Marías y a su corazón volvió toda la angustia primera, increíblemente olvidada por la belleza imponderable del paisaje. El trágico Huitzilac hacía rato que había quedado a sus espaldas y
una vez que el coche salió de la penumbra verde de
la floresta, Andréïda supo que no restaban muchos
minutos para la llegada a la señorial Cuernavaca.

Tentada estuvo de desear un accidente, algo
que detuviera la marcha frenética e inexorable del
coche y del Destino.

Mientras tanto, el doctor tenía un solo pensamiento que daba vueltas y vueltas en bien pocas
palabras gozosas. Andréïda iba a ser suya, nada
más que suya. La había arrebatado a la sociedad
y a su propia voluntad remisa. 

Por bien distinta razón, él también deseaba retardar la llegada, alargar la dicha acrecentándola
con la voluptuosidad de la espera y por tal dulce
anhelo, dejó de apretar con violencia el acelerador
para pisarlo apenas.

Por primera vez le parecía verdaderamente dulce la vida y estaba dispuesto a gozarla en toda su
real plenitud, hasta lo hondo, con un abrazo completo, absoluto, enorme, en el que con gusto dejaría,
en la empresa, la propia existencia.

Andréïda solo sabía lamentar el que el hombre
no hubiese sabido apreciar el simple obsequio de
su compañía, la entrega casta y fervorosa de su alma. Pero comprendía que condición de hombre era
el ser tentado por el cuerpo y se dijo que ellos nunca podrían concebir la superioridad de libertarse
del imperio carnal.

Llegaron. La ciudad presentaba su aspecto acostumbrado de feria; preparada, como siempre, para
recibir al turismo con una sonrisa plácida y un
tanto interesada.

El doctor Raúl dió una vuelta lenta por la
placita atestada de carros que presentaban el aspecto de un soberbio aparador, en el que se mostrase una gran diversidad de marcas y de placas
de todos los Estados de la Unión Americana del
Norte. Dentro de los coches podía apreciarse el habitual espectáculo de mujeres viejas, vestidas juvenilmente con un exceso de colores brillantes y cuya
presencia multiplicada, de manera alarmante, nos
hace pensar en una pesada broma de los rubios primos empenados en enviarnos una colección de mujeres caducas e imperdonablemente feas, acompañadas de media docena de hombres arrugados, de despreocupada indumentaria y grandes pipas aprisionadas entre la perfección asombrosa de las dentaduras postizas.

Una simpática indita de graves, tristes y oscuros ojos, saltó al estribo del coche y ofreció una
linda mercancía; frágiles pulseras de seda, dijo, para la preciosa señorita. El doctor Raúl tuvo un detalle pueril, las compró todas y las engarzó por sí
mismo en los brazos torneados de la amada. Andréïda sonrió complaciente y agitó los brazos con
una falsa alegría infantil.

El roce de las manos femeninas había vuelto a
encender en el hombre su imperioso deseo, y, poniendo en marcha el motor, tomó otra vez por la
carretera.

Andréïda no osó preguntar hacia dónde la llevaba y no fué necesario tampoco, pues escasos tres
minutos después entraba el automóvil por los campos de golf, rumbo al magnífico hotel de Chula
Vista.

Inmediatamente, al descender del coche, comprendió Andréïda que Raúl había arreglado con anticipación su breve estancia en el lugar aquél. Un
camarero discreto—los camareros de cualquier nación del mundo tienen un singular olfato para adivinar a los amantes—los guió a un lujoso apartamento. Ya frente a la puerta, Andréïda tuvo todavía un movimiento de retroceso, y sus hermosos
ojos miraron directamente a la cara del hombre como interrogando si, en realidad, quería él que ella
entrase allí. Una súplica dulce de la viril mirada
la impulsó a obedecer en silencio.

Apreció, en el acto, la delicadeza del amigo que
había transformado la vulgar instalación del hotel
con unos cuantos objetos de arte y de su uso íntimo,
para borrar la penosa impresión de lugar mercenario, privado de personalidad, Las habitaciones
de los hoteles, por lujosas que sean, carecen siempre de personalidad, quizá por haber sido demasiado
holladas al paso de la gente, de la cual reservan
un olor a polvo encerrado y a sobadura oscura y
estúpida con la que el hombre ensucia, a su paso,
todas las cosas de que hace largo uso. Agradeció
también, Andréïda el que la puerta de la alcoba
permaneciese escrupulosamente cerrada, como si su
cercanía no tuviese la virtud de turbar, de todas
maneras, los nervios de los dos.

Una vez que el camarero se retiró, no sin antes
haber recibido en la puerta órdenes que le diera, en
voz baja el doctor Raúl, Andréïda se dirigió al balcón. Desde él se dominaba un barranco que la noche cercana hacía aparecer imponente y temeroso.
Un girón de niebla pretendía borrar, con un blanco
lechoso, el misterio de su sima negra. A lo lejos, la
cercana ciudad, comenzaba a alejarse parcamente
con luces aisladas y de un mortecino brillo amarillento. El perfume de la montaña le llegaba a Andréïda en oleadas enervantes, como un fresco y maligno incienso que la embriagase un poco. También
en el firmamento comenzaban a encenderse las estrellas, con una lentitud muelle de cielo tibio.

Raúl se acercó a Andréïda lentamente, por detrás. Con delicadeza devota la despojó del ligero
abrigo y, después, con exquisita sencillez desprendió la gasa de tres colores trenzados que en la luminosa cabellera hacía las veces de sombrero y de
tocado griego. Su torpeza masculina puso un ligero
desorden en los suaves cabellos femeninos, El perfume conocido de la amada y el rumor un poco anhelante de su respiración turbó hondamente al hombre que con un leve desfallecimiento se vió precisado a cerrar los ojos. Gozó prolongadamente la tentación de tenerla a su alcance y no tocarla. Segundos más tarde, sus labios rozaron la nuca desnuda,
junto al nacimiento del pelo y ya enloquecido de
deseo la estrechó entre sus brazos fuertes, con un
amor convulso que obligó a la joven a echar hacia
atrás la adorable cabecita. Los labios ardientes se
posaron en los frescos labios virginales. Un gemido
grave se escapó de la garganta femenina, como si
aquella primera voluptuosidad desconocida fuera excesivamente intensa para sus sentidos novicios. Pero no tardó mucho, la mujer, en erguirse con energía. No quería caer tan pronto. El hombre comprendió, al punto, y se embriagó de una castidad
no conocida. Acarició la bella cabeza inerte, sin premuras y sin violencias, hundió los dedos viriles en
la suave cabellera observando, con grave dulzura,
cómo se rehacían las amplias ondas en cuanto se
veían libertadas del peso de su mano. Cerró los
ojos amados con besos tan pronto ligeros, frenéticos, prolongados o suavísimos que acabaron por encender en las mejillas de Andréïda el fuego sagrado
de la voluptuosidad.

Ahora la noche triunfaba en una orgía esplendente de estrellas. En la inconmensurable distancia infinita se encendían mundos remotos que se
diluían en una niebla luminosa y fantástica. ¡Estrellas! ¡Estrellas! ¡Más estrellas! Cumbres ignotas, bellas burbujas de luz, sortilegio de nuestra
pupila, tentación de oro para el humano cautivo.
Lumínica invitación insidiosa que empuja el pensamiento hacia lo alto. ¡Estrellas! ¡Estrellas! Más
estrellas! Turbias lunas, Hécates de luz maligna
que saben encender la locura en el útero pravo de
Salomé, la núbil; brujo polvo ardoroso que despierta en la noche a Nuestra Señora Sensualidad sobre
la Tierra.

Atomos del Universo, polvos de Mundos, briznas de infinito, fragmentos luminosos de Tempestad… ¡Qué puede importaros, decid, que una mujer pierda engañada, en la vorágine, su divino sueño de inmortalidad!
En los sueltos cabellos de Andréïda flotaba un
perfume nupcial. La noche entera estaba impregnada de un violento aroma de azahares que embriagaban el alma haciendo de dos cuerpos jóvenes, el
centro del Mundo.

El la cogió dulcemente y la llevó de la mano
hacia el diván. Después, cerró la ventana y se quedó inmóvil. Bajo la mirada violenta de los ojos
viriles, Andréïda pensó enloquecida si no habría
tejido en vano su vida con el frágil andamiaje de
las bellas palabras, y si tendría valor ahora para
vivir aquellos minutos de pasión plena, que en el 
pasado los había ella considerado, en forma superficial, como lucubraciones calenturientas de literatos.

Una discreta llamada a la puerta sobresaltó a
ambos.

Era el camarero que entraba el servicio para
una cena deliciosamente íntima. Andréïda tuvo
vergienza del extraño, del primer testigo de su
flaqueza y volvió la cara en un vano gesto de ocultación.

Ni Raúl ni Andréïda sentían apetito, pero se
dispusieron a picotear aquí y allá como dos chiquillos golosos, cuyos jugueteos no les permiten comer
con formalidad. Templado el recuerdo ardoroso de
las caricias recientes les parecía sentir, sobre ellos,
el imperio de una voluntad desconocida, más fuerte
y de un orden más profundo.

El hombre rodeaba a la mujer de una solicitud
caballeresca y graciosa, con el deseo sutil de adivinar sus menores deseos. Se mostraba delicioso, encantador. Tenía frases que remontaban la vulgaridad común y, sin embargo, repetían, de manera
inconsciente, giros sobados por el transcurso de los
siglos en bocas de amantes. solo que en sus labios
resurgían singularmente flamantes, con una juventud perenne y un sentido nuevo que hace pensar
siempre a los jóvenes en que son ellos quienes los
descubren y los pronuncian por primera vez.

Bajo la mirada pasional del hombre las mejillas
de Andréïda empalidecían o se sonrojaban. Su cerebro vacío no sabía hacer otra cosa que constatar
los latidos tumultuosos de su corazón. En la desnudez del rostro la boca sonreía como una cálida
ofrenda deslumbradora y sangrante y los ojos de la
mujer no se atrevían a ver, cara a cara, el amor.

Bruscamente se puso él en pie. ¡No podía más!
Un momento pareció recogerse, vacilar, después
apartó con violencia la mesita que los separaba.
Quiso decir algo y no pudo, intentó sonreír y la
sonrisa se deformó en los labios con ese crispamiento innoble de la carne cuando triunfa sobre la calidad pensante del hombre y, de pronto, se arrojó a
sus pies.

De cierto que esta actitud sumisa, imploradora
del ser civilizado obedecía al complejo deformador,
falso, desvirtuador de los instintos puros que en el
pasado hicieron del hombre el supremo cazador indómito, el verdacero rey de la creación, y que ahora
se ve convertido en un remedo de cobarde humanidad encorvada bajo el terrible peso de los grilletes
que lo sujetan al convencionalismo.

Las manos del hombre se crisparon sobre la gracilidad de los tobillos femeninos e, inclinándose, besó la seda mate de las medias que trasparecía la
carne pura.

Sorprendida Andréïda lo dejó hacer, sabía que
era el póstumo homenaje a su virginidad. Después,
perdido su sublime ascendiente, no volvería a ser
objeto de aquella adoración fanática. Para el hombre, lo inalcanzado tiene un valor magnífico que
jamás vuelve a otorgar a lo poseído, por mucha necesidad que sienta en conservarlo.

La mirada del hombre reptaba, a pesar suyo,
como una llama roja, implacable, magnífica, acosando la carne, subiendo por entre las rodillas delicadas de la mujer, lamiendo los muslos, envolviéndola entera, en la terrible hoguera visual de un
anticipado abrazo.

La mujer, inmóvil, estática, reprimía la respiración, parecía como si la oprimiese demasiado la
mirada pesada del hombre, la mirada cargada, convulsiva y sublime en el apogeo de toda su fuerza
privativa y salvaje.

Reinó en la habitación un silencio majestuoso y
ritual de templo solitario. Las palabrag habían
huído de los labios. Resultaban ahora inútiles, cumplida ya su misión engañadora y falaz de eternas
deformadoras de la verdad, en anhelo excusable de
un embellecimiento imposible. Celestinas oficiosas
de un sofisma cruel e insostenible, habían huído, como siempre, con la debida oporturridad. Después, de
todo, virtud es ésta esencial a encubridores.

Ahora la mujer intentaba, en la eterna lucha inútil, desprenderse de las manos varoniles, de
aquella boca turbadora que pretendía sorber su vida
y traspasar su carne; pero le faltaron fuerzas y
voluntad. Volvía a ser el ser débil, el de las redondas formas y los largos cabellos.

"Después. .. el grito que se ahoga en la garganta, el eterno grito de la hembra herida en la carne
profunda. Ha cerrado los ojos… No es placer lo
que de tal manera altera las líneas puras de su rostro… No es goce lo que le entreabre los labios en
vna mueca desesperada y le hace entrechocar los
dientes en un temblor nervioso. No es alegría la
que estremece, a intervalos, sus finos miembros…
Es el dolor infinito, es la maldición hebrea, es la
venganza de un Dios imposible…

El hombre ha quedado solo con su gran placer.
Oficiante monstruoso en el monstruoso altar de la
naturaleza es solo carne y a la carne atiende. El
jadeo de su boca vibra, en el gran silencio de su
soledad, con agitado tintinear de incensario ritual.
Una furiosa luz lo circunda como a un haz de átomos en combustión cósmica. Su cuerpo arde en una
apoteosis magnificente de fuego. Su faz se aisla en
la luz de la eternidad sin límites, porque está hecha
de infinito y sabe solo del girar continuo en los
espacios con un solo rumbo, el de perpetuar su molde… Y la estupenda fiebre se desata en un grito:

—¡Mía…!—Palabra y símbolo, voz y signo
bestial del Universo.

Silencio. Más silencio. El escupitajo de la materia sobre la materia herida.

Un anonadamiento que purifica con un sopor
mortal de olvido. Un sueño tan cerca de la muerte,
como que es cadena, advertencia y anticipación a
un tiempo. .

— Dos criaturas hermanadas en la prolongada culpa. Encenagamiento metódico en el gran encenagamiento de la tierra. Seguido de un tardío escalofrío de remordimiento que, en el fondo, no es más
que un eventual hastío. En el cerebro, vacío de pensamiento, una sensación de inutilidad de afanes
renovados en círculo maldito e inevadible. El frío
de una vaga desilusión en el espíritu que ha vuelto
al cuerpo y que huyera a tiempo, con las palabras,
Silencio… Sopor… Frío… Para recomenzar
una vez y otra vez, mientras que por las arterias
pueda correr la púrpura caliente y afiebrada de un
líquído que corre desde que el mundo es mundo y se
propone seguir corriendo indefinidamente, asqueado de presenciar la inútil comedia.

De los ojos de Andréïda fluyen calladamente las
lágrimas. Una combinación química que faltándole
espacio suele inundar terrenos ajenos. Sin embargo,
es una nueva criatura ésta que llora en silencio, al
lado del hombre que ha dejado de ocuparse de ella.
Es una criatura nueva que ha traspasado los límites
vedados del conocimiento y camprende que el mundo está saturado de una ficción engañosa, destinada a perpetuar la materia.

Andréïda, la mujer que había querido salirse
de la convencional prisión de los sentidos, había
sido por ellos ahora humillada y herida; por ellos
había sido dominado su orgullo de dominadora. La
logica humana había tachado de criminal y monstruoso su temerario anhelo y con hostilidad había
espiado su inhibición oscura, fuera de la razón y
de la divinidad. De improviso, la materia universal
había clavado su garra en la rebelde entraña femenina. ¡Para borrar su metafísico fraude a la naturaleza, había bastado un minuto de debilidad!

En medio de su desconcierto y su humillación,
Andréïda recordó, de pronto, que aquel fracaso urgía el metamorfosearlo, cuanto antes, en triunfo
definitivo. Privilegio de fuertes es esta transformación. Su espanto dejó de serlo. La breve vacilación de su caída desapareció del todo. Otra vez
tornó a ser la mujer nueva de ojos helados, la de
cabecita desafiante y temeraria, la de sonrisa inconmovible de superioridad.

Reconoció que había cruzado ilícitamente el camino conocido; pero hasta ésto lo había hecho haciendo a un lado los prejuicios ancestrales que encadenan. Y si había cedido, también el ceder, de vez
en cuando, es privilegio de los fuertes. De ahí que,
hasta la propia y reciente humillación, hubiese implicado, en ella, un acto libertario..

Empezaba para ella, en aquel momento, una fase
trágica de su enorme conquista, para afrontar la
cual necesitaba de todas sus fuerzas y de toda su
inconmensurable fe en sí misma. Por tal razón, no
podía consentir que un hecho accidental se impusiese a su propósito; porque eso implicaría la rendición cobarde de sus convicciones, acariciadas toda
su breve existencia de ser consciente. Aquello no
podía desarmarla, no…

Sintió vibrar en torno suyo la hostilidad del
mundo, más franca y aun más despiadada. Comprendió que sería la suya una guerra sin cuartel de
la que podría salir definitivamente vencida o dueña
soberana de su destino. No vaciló; el solo pensamiento de sostener aquella lucha titánica espoleó
su voluntad, como un animal fogoso que se crece al
castigo.

La sangre le seguía latiendo con violencia en
las arterias; con la diferencia de que ya no era un
temblor desfallecido, sino una poderosa actividad
de la que sentía irradiar, en torno, una violenta
magnificencia. Aspiró el aire de la caldeada habitación con una ansia enorme de libertad, de espacios infinitos. Agitó las finas muñecas de sus manos como quien se desprende de una pesada cadena.
Sintió, de nuevo, su vida purificada, liberada del
todo, hasta de la curiosidad. ..

Ruda, exaltada, hurtó su cuerpo al contacto del
hombre que balbuceó en sueños no supo qué súbita
ternura. Y anárquica, esplendente, con una suprema
tranquilidad, levantó en alto su derecho magnífico
de marchar solitaria por la vida, como una diosa
moderna dueña de su porvenir mortal. Huyó del
amargo paraíso terrenal de la vida que tiene un
rumbo solo, un destino único. Marchó en pos de la
gloria infernal de abrir una vereda nueva sobre la
monotonía esclavizante de lo que, desde un principio, fué hecho dentro de un canon inevadible.

Dos horas después, ya de regreso en México,
penetraba en la alcoba de Nelly como un pálido fantasma que huye de su propia sombra. —No me preguntes nada—dijo con aspereza a la adormilada
amiga que dilataba los miopes ojos en un amplio
gesto de temerosa incomprensión—. Necesito, añadió con firmeza—un mes de absoluta soledad, de
completo silencio. Nadie debe saber dónde me encuentro, ¿lo oyes…? Nadie… Me marcho.

El gran dolor que se reflejaba en el demacrado
rostro de Andréïda, su aspecto febril, la impresión
deprimente estereotipada en la dolorida y bella boca mancillada, inspiró a Nelly un supremo respeto
para aquella terrible lucha que no tenía el pudor
de ocultarse. Ya antes de que hablara, la percepción aguda y casi maternal de la amiga, había adivinado, sin inmutarse, el que Andréïda no había
podido eludir lo que tanto aborreciese en la vida. Y
Nelly nada preguntó, se limitó a saltar de la cama
y a arreglar precipitadamente un baúl con la ropa
más indispensable a la amiga. Y con una senciilez
emocionante, dijo:

— Te acompaño…

Andréïda dudó un momento, echó un rápido
vistazo al relojito que ceñía su muñeca y vió que
eran las tres de la mañana. 

—Bien—aceptó lacónicamente—puedes estar de
regreso aquí mañana a las once. Nadie podrá extrañar tu ausencia. Vamos a Garci-Crespo, me registrarás con un nombre cualquiera.


Mientras hablaba, su orgullo turbaba la lucidez
habitual de su inteligencia. Ahora decía que ignoruba el por qué había sucumbido.

Y en la noche, las dos mujeres, con esa libertad
importada del país vecino, emprendieron el breve
viaje. Andréïda guiaba con velocidad desbocada su
propio carro. No tardaron mucho en encontrar el
camino de Balbuena, internándose, por fin, en la
maravillosa carretera de Puebla…

</section>
<section epub:type="chapter" role="doc-chapter">

#Capitulo VI 

Transcurrieron cuatro semanas en las que Nelly
apenas si recibió de Andréïda dos o tres lacónicas
cartas; pero una mañana, intempestivamente y
cuando se disponía a salir para atender su trabajo,
la vió llegar y se dispuso a recibirla con el cariño
un poco maternal de costumbre, sin demostrarle extrañeza ni hacerle la más mínima pregunta. Y, en
efecto, procuró por todas las formas que Andréïda
reanudase su vida de la manera más natural. No
le comunicó tampoco las incontables veces que el
doctor Raúl había ido a preguntar por ella ni la
desesperación que la propia Nelly sorprendiera en el
demacrado semblante varonil, obligándola a confirmar lo que, a su perspicacia femenina, no se le
ocultara desde la noche aquella en la que Andréïda,
cual un fantasma pálido y desesperado, le comunicase su repentina decisión de marchar a Garci-Crespo.

Andréïda sentía una gran cohibición para mirar a la amiga cara a cara, por lo que permaneció
callada y fué Nelly quien, comprendiendo su turbación, inició la charla de manera banal, a tiempo que
le comunicaba mil y un incidente sin importancia.

Para ocuparse en algo y evitar el estar frente a
frente y que se impusiese un penoso silencio, Nelly
se ofreció a prepararle ella misma un baño caliente, y en el ir y venir le mostró a Andréïda una invitación de la Embajada Colombiana para una recepción que celebraba esa misma noche, instando a la
joven para que no dejara de asistir. Minutos más
tarde y cuando ya estaba listo el baño, Nelly se
acercó a Andréïda y la dijo:

—¿No habrás leído periódicos?

Sobresaltada, Andréïda replicó:

—Ninguno…

—¿Ignoras, entonces, la desgracia de María
Luisa?

— ¿Qué María Luisa?—interrogó con curiosidad
la joven.

—María Luisa, nuestra compañera del St. Mary.

Desce la salida del colegio ni Andréïda ni Nelly
habían tenido ocasión de volver a encontrar a ninguna de sus compañeras. La vida había dispersado
sus existencias a capricho, y no obstante los buenos
propósitos que cada una hiciera al despedirse, no
había sido posible el que se reuniesen, y eso que
buena parte de ellas residía en la capital mexicana.
Por tal razón, Andréïda se dispuso a escuchar con
interés redoblado.

—Pues verás —empezó diciendo Nelly— tu profecía se ha cumplido al pie de la letra. Hace tres
semanas que su tragedia ocupa planas enteras en
los diarios.

—¿Su tragedia…? —preguntó con impaciencia
Andréïda.

—No sé cómo decírtelo. Has de saber que se casó
hace aproximadamente tres años con un hombre
rico y viejo, como tú se lo anunciaste con videncia
extraordinaria; pero se conoce que su vida matrimonial ni fué todo lo feliz que ella esperaba, ni satisfizo su fogosa juventud. Resultado de aquello
que, en un rapto de locura, mató a su amante.

—¿Su amante…?

—Su amante, ya te he dicho que tu profecía se
ha cumplido en todas sus partes.

Las dos amigas quedaron silenciosas. A Nelly
le había temblado la voz al exponer brevemente y
con afectación de superficialidad, la penosa situación de la antigua discípula, después añadió:

—Esta noche tengo un duro deber que cumplir,
el de presenciar la reconstrucción del crimen y hacer una crónica para mi periódico. No he tenido
ocasión de comunicarte que esa es mi nueva ocupación en el diario. Parece ser que el director encuentra atractivo, para el público, el que una mujer escriba las notas rojas. Me ha dicho que mi visión
femenina de ellas es algo nuevo en el periodismo, y
qué he de hacer, yo no tengo esa facilidad tuya para
que se me abran las puertas del éxito… —La sonrisa bondadosa con que acompañó las últimas palabras demostró a Andréïda que no las había dictado
la envidia, pues Nelly era incapaz de sentir, con respecto a su amiga, tal sentimiento odioso, sino que
manifestaban como una aceptación implícita de inferioridad.

—No me atrevo a pedirte que me acompañes —añadió Nelly— pero debo confesarte que tiemblo solo
de imaginar el que me es imposible evitar el ir y he
pensado, además, que quizá tu presencia diera ánimos a María Luisa. ¡En esos casos una mirada
amiga hace tanto bien…!

Andréïda reflexionó un momento y después, con
esa espontaneidad suya que tanto gustaba a Nelly,
dijo simplemente:

—Iré contigo.

\* \* \* {.espacio-arriba1 .centrado}

En todo el día y como por un convenio tácito,
ninguna de las dos amigas volvió a hablar de María
Luisa; sin embargo, Nelly no dejaba de felicitarse
por haber obligado a Andréïda a compartir su carga moral, pues ésto sirvió para que la bien amada
amiga dejara de pensar, un poco, en sí misma. {.espacio-arriba1}

Por la noche acudieron, como habían convenido,
al lugar de la reconstrucción de hechos que era un
ínfimo cabaret de barriada, con todo su aspecto
mezquino de lugar impuro. Torritos, que se encontraba también presente, se apresuró a recibirlas,
reprochando al mismo tiempo a Nelly el que hubiese
llegado tarde, razón por la que se había perdido,
dijo, parte del interrogatorio. Esto no obstó para
que, a pesar de sus frases bruscas, le ofreciera su
block de apuntes con el objeto de que no faltara a
la crónica de Nelly tan interesante detalle.

María Luisa, que se había vuelto a la llegada de
las jóvenes, se dirigió a ellas con una tranquilidad
pasmosa. Se encontraba elegantemente vestida de
negro y hacía gala de la más absoluta frialdad.

—¿Qué tal, Nelly? ¡Hola! Y también tú, Andréïda, ¡qué alegría el verlas! —Hablaba como si todo aquel aparato judicial le fuera ajeno y se encontrase allí solo como elegante espectador curioso.

Andréïda no pudo menos que exclamar:

—¡Qué desgracia!


María Luisa levantó los hombros y con una ligereza un tanto afectada, respondió:

—¿Qué quieres, chica, esta es la vida! —Y recordando de pronto, añadió: —El cachito de dicha…
que me salió… ¡cachito de cuasia…!

Periodistas y funcionarios escuchaban aquella
inusitada conversación con enorme sorpresa, hasta
que el agente del Ministerio Público intervino, con
severidad, para hacer saber a aquellas jóvenes que
no se encontraban precisamente en una reunión social y, al mismo tiempo, hizo constar su protesta
enérgica. Después conminó a María Luisa para que
indicara el lugar exacto en el que su víctima había
caído.

En los precisos momentos en que con gran tranquilidad la joven se preparaba a señalar el trágico
lugar, la entrada de dos nuevas personas provocó otra interrupción, con gran disgusto del mencionado funcionario.

Esta vez se trataba del anciano esposo de María Luisa que venía acompañado del doctor Raúl.

No parecía sino que el destino se propusiese reunir a Andréïda y al doctor Raúl en un sitio trágico. Al verse, ambos palidecieron intensamente.

El esposo de María Luisa se apresuró a presentar sus excusas al Juez por haber llegado tarde,
informándole que se encontraba muy enfermo y que por tal razón, también se había permitido hacerse
acompañar por su buen amigo el doctor Torrelavega. Por su parte, María Luisa, a la llegada de su
esposo, apenas si se había dignado echarle encima una rápida mirada despectiva y había proseguido
la diligencia corrigiendo por sí misma la posición de la persona que hacía de víctima. Con una frialdad que maravilló a los periodistas, empuñó el arma
homicida, dispuesta a hacer una franca demostración visual de cómo había efectuado el crimen.

Uno de los periodistas se atrevió a decir en voz
alta:

—¡Qué cinismo de mujer!

Torritos, que se encontraba a su lado, terció
realmente asombrado: —No, no es cinismo, es…
un caso único, parece como si su conciencia no le
reprochara la muerte de un hombre…

El anciano esposo contemplaba la escena con un
temblor nervioso, parecía allí el único verdadero
acusado. Minutos después pidió permiso al juez para cruzar unas palabras con María Luisa y, al serle
concedido, exclamó con voz tremendamente suplicante:

—¡María Luisa!… No es posible… no puedes
haber rectificado en esa forma tus declaraciones.
¿Te has dado cuenta de lo que has hecho?

María Luisa oyó la interpelación de su esposo
con una sonrisita exasperante, después replicó, señalando al agente del Ministerio:

—El me ha obligado, y, después de todo, me he
convencido de que era lo mejor decir la verdad —y
con su peculiar movimiento despreocupado de hombros, terminó diciendo: —¡Me perjudicará menos!

El hombre viejo se estremeció de cólera, irguió
la desmedrada figura, y los periodistas aprestaron
sus _blocks_ de apuntes. Su admirable olfato les anunciaba que iba a desarrollarse una escena interesantísima. Las miradas de la mujer y del esposo, cargadas de odio insano, decían muy claro el que nada
ni nadie podría esta vez detener el acíbar de que estaban repletos sus corazones.

Don Manuel empezó diciendo con voz entrecortada por violentos sollozos:

—Mujer infame, siempre pensando en ti… ¿No
te basta arrastrar mi reputación por el lodo, escarneciendo mi nombre sin mácula? Y ahora…

Las palabras de don Manuel no tuvieron otro
efecto en María Luisa que acentuar su sonrisa despectiva. La mujer se limitó a repetir: —¡Tu nombre, tu reputación…!

Don Manuel comprendió que no era ese el camino para herir o conmover a la mujer, y recogiéndose en sí mismo le arrojó a la cara:

—¿No concibes que al deshonrarme a mí, deshonras a Graziella, a mi muñequita bonita…?

Esta vez fué María Luisa quien tembló de cólera y dolor.

¡Tu Graziella! —exclamó—. Una niña que no es
hija tuya… y que tú lo sabes… lo has sabido siempre… desde un principio…

Las palabras de María Luisa hicieron correr un
escalofrío de horror en todos los circunstantes. El
anciano se tambaleó aferrándose al brazo del doctor Raúl. Los periodistas dejaron de escribir anonadados ante la fuerza trágica de aquella escena
que sacaba a flote una miseria conyugal inconcebible. Con un esfuerzo supremo, don Manuel se irguió y escupió la cara de María Luisa:

—Mala mujer, calla, insensata…

—Mala mujer, sí, sí —aceptó María Luisa con
una tranquilidad desesperada—. Pero he sido obra
tuya, soy la obra infame de tus manos…

Andréïda se inclinó hacia adelante, no quería
perder una sola palabra de las frases de María
Luisa.

—Tú, el honorable don Manuel… —María Luisa, con una mirada circular, pareció interrogar a
todos los presentes. —¿A qué se llamará honorabilidad en este mundo?… —Después prosiguió con voz
implacable—: honorable porque no has matado,
porque no has robado, porque has llevado, en apariencia, una vida temperante y has hecho siempre
como si trabajases en un puesto público. ¡Honorable! 

Se hizo una pausa en la que pareció quedar en
suspenso la vida de todos los allí presentes.

Cuando María Luisa reanudó su apóstrofe, la
voz femenina tenía inflexiones salvajes:

—¡Honorable! —repitió— ¿Y el crimen de haber
unido una mujer joven a tu vida caduca? ¿El crimen, sin nombre, de haber estrujado entre tus manos sarmentosas un cuerpo lleno de vitalidad? Ese..
¿qué ley lo castiga?

El viejo esposo se hurtó a la pregunta, balbuceando:

—Y, ¿a qué viene ésto ahora?

María Luisa no cejó en su propósito:

—Viene a desenmascarar la hipocresía toda de
tu vida… ¿Qué me dices de tu consentimiento repugnante porque a toda costa me querías tener contigo…? ¡Oh! —añadió con una deseperación real
que llegó al corazón de todos —La bajeza diaria de
una vida en común que era un infierno —¡Tus
ojillos de sádico que me espiaban por encima de las gafas y me observaban con un odio espantoso y un
deseo más espantoso aún…!

Por momentos la desmedrada figurilla del anciano parecía querer esfumarse; pero la voz implacable de la joven esposa lo fijaba en su sitio, irremediablemente.

—Tu, honorable, y encontraste la forma más
cruel de martirizarme… Sabías que, en mis circunstancias, el amor de un hombre joven tendría;
por fuerza, que costarme dinero…, y me lo negabas
o me lo medías con cuenta gotas. Después, lograste saber hasta mis pensamientos; la correspondencia
me fué violada… y ¡qué quieres! me hice cínica
comencé a depravarme. ¡Me comunicaste tu extraño sadismo… —María Luisa rió con cinismo nervioso.

Don Manuel no pudo más, volviéndose a todoa
suplicó:

—Haced callar a esta mujer… ¿Qué no hay
piedad para un anciano? 
Aquello exasperó aún más a María Luisa.

—No he de callar… Tú eres el que debía estar
aquí, aquí… en mi lugar… Tu debías ser el procesado… ¡Si, después de todo, —añadió— tu avaricia y tu sadismo fueron las causas que me obligaron a matar! No niegues, no; tu me veías sufrir
por la falta de dinero y gozabas, y él… el otro canalla, era insaciable… Me amenazaba constantemente con abandonarme… y yo tenía necesidad de
él… Una necesidad espantosa de él… —Y con voz
opaca concluyó de hablar. —Era lo único que después me hacía soportable tus babas asquerosas…

Esta vez el juez intervino. Por dignidad moral se imponía el que aquella mujer callase. Todos lo
comprendían así. Pero la voz de María Luisa era
como una explosión volcánica, para la cual no existía obstáculo capaz de detenerla, y así lo hizo constar ella.

—No hay ser humano que me haga callar ahora… —Y dirigiéndose, de nuevo, a don Manuel,
añadió con menos violencia, como si el peso de su alma se hubiese aligerado —Has invocado el nombre
inocente de mi hija… Una hija que no es tuya, afortunadamente —recalcó con crueldad—. Por ella, por
salvarla a ella lo maté… sí, lo maté, y lo volvería
a matar mil veces si me viese en las mismas circunstancias… Allí cayó… con una baba sanguinolenta en los labios lascivos…

Don Manuel pidió al juez permiso para retirase y le fué concedido. Momentos después salía acompañado del doctor Raúl.

En el sucio salón reinaba un silencio imponente.
Hasta los periodistas se habían abstenido de escribir, acostumbrados como estaban a la abundancia
de crímenes pasionales de que es teatro, a menudo,
la ciudad de México, éste venía a sorprenderles por
sus perfiles oscuros que ponían, de manifiesto, una
depravación espantosa de concebir en seres civilizados, cuya cultura debía haberles inspirado una
dignidad humana que parecían desconocer ambos
por completo.

Mientras tanto, María Luisa, seguía diciendo
con voz monótona:

—Si, lo maté… ¡Lo maté por salvar a Graziella, a mi inocente chiquilla… Me amenazó con la
prensa, con dar a la publicidad no sé qué cartas si
no le daba más dinero; y yo, ¡pobre de mí… quería conservar ante la opinión pública mi reputación… rió nerviosamente— ¡Mi reputación! Quería conservar para la nena, la posición. ¡honorable! del que para todos debía ser su padre…

Las últimas palabras de María Luisa, se perdieron en medio del estruendo producido por una detonación de arma de fuego. Todos, a una, se lanzaron a la puerta, preguntando a voces lo que había
pasado y no tardó mucho en aparecer el doctor Raúl
quien, con voz emocionada, comunicó al juez el
que Don Manuel se había pegado un tiro, en los momentos en que él se había retirado para llamar su María Luisa hizo un comentario cruel:

—¡El hipócrita, ha sabido dar un fin honorable
a su inmoral vida…! 

Nelly intervino; no podía menos que reprochar
a la antigua discípula y compañera su sarcasmo
cruel.

—Piedad, María Luisa… —dijo— ¿Has olvidado tu piedad de mujer?

El juez dispuso que se trasladara al herido a
una habitación, y el doctor Raúl fué a prestar sus
servicios que él mismo hizo constar, anticipadamente, los creía inútiles. Alguien se brindó a llamar una
ambulancia, y el agente del Ministerio Público pidió que se prosiguiera la diligencia.

María Luisa se opuso al observar que los fotógrafos preparaban el magnesio, e hizo saber que
aquéllo le atacaba los nervios. Y no faltó periodista
que hiciera un comentario irónico sobre los repentinos nervios de que hacía gala la joven inculpada.

Sin embargo, su nerviosidad no era nada fingida,
sus bellos ojos rodaban en las órbitas con un miedo
cerval de gacela acorralada. Andréïda, en voz baja,
comentó: 

—¡Desdichada!—

Y al estallar el magnesio, María Luisa se desmayó con un grito de angustia.

Poco después, llegaba la ambulancia por Don
Manuel que falleció. en el breve trayecto y que,
terrible ironía del destino, por última vez lo acompañó, en el mismo coche, el cuerpo desvanecido de
su joven esposa.

Los funcionarios públicos y parte de los periodistas se retiraron. Minutos después no quedaban
en el paupérrimo cabaretucho mas que Andréïda,
Nelly, Torritos y sus adláteres, los dos jóvenes
periodistas. A ellos se había acercado Carrillo, el dueño del establecimiento y les confiaba, entre
grandes aspavientos, la terrible ruina en la que
lo dejaban aquellos sucesos desagradables.

Nelly copiaba de prisa el contenido del _block_
de Torritos y Andréïda esperaba en silencio. A
pesar suyo, más que en la tragedia de María Luisa pensaba en el doctor Raúl. Su inesperada presencia había conmovido aquélla su tranquilidad,
adquirida, con tanto esfuerzo, en el largo mes de
soledad pasado en el elegante balneario.

De súbito fué el propio Raúl quien, de regreso,
apareció en la puerta y dirigiéndose directamente
a Andréïda la dijo: La he encontrado en el lugar
que menos la esperaba. ¡Y mire usted que la he
buscado por cielo y tierra todo este mes…!

Andréïda replicó con sequedad:

—No me encontraba en la ciudad, señor mío…

La extrañeza del doctor Raúl subió de punto al
oirse llamar señor mío. Y Nelly, tratando de disipar la mala impresión, intervino: —Sí, Raúl, como
le informé, Andréïda se encontraba un poco mal
de los nervios y, ahora puedo decírselo, fué a reposar unas semanas a Garci-Crespo.

Raúl pareció tranquilizarse y con ligereza que
quería ser graciosa dijo a Andréïda:

—¿Se fué usted sin consultar a su médico,
señora?

Andréïda repuso un poco molesta:

—¡No era nada de importancia! Estaba cansada del ambiente de México y eso fué todo. —Después, dirigiéndose a Nelly añadió, en tono de reproche. —¿Cómo te atreves a decir nada de mis
nervios? Bien sabes que yo no tengo nervios…

Torritos lanzó al aire una estruendosa carcajada
e hizo saber a su amiga Nelly la imprudencia que
había cometido al hablar de los nervios de Andréïda.

—¡Qué —dijo— acaso Nelly había oividado
que su extraordinaria amiga se resistía a admitir
cualquier cosa en común con la mujer, y con mayor razón si se trataba de debilidades?

Andréïda sonrió, perdonando a Torritos el que
quisiese, a costa de ella, hacer un comentario chispeante.

Nelly había terminado de copiar el interrogatorio y permanecía indecisa, como un ser que tiene
necesidad de suplicar algo muy difícil. Después,
hacienco acopio de voluntad y dirigiéndose a los
periodistas, dijo:

—Señores, yo desearía pedirles un gran favor.

Los tres hombres miraron a la joven compañera con atención risueña.

—Es menester —añadió Nelly con precipitación— que terminemos con esta publicidad que se
le ha hecho a María Luisa, y que no hará más que
perjudicarla, influyendo, desfavorablemente, en el
ánimo del juez.

Torritos saltó al punto. Aquéllo que pedía Nelly era punto menos que imposible de realizar. Estaba de por medio, dijo, la moral periodística que
exigía uniformidad de criterio en el cronista, y
acabó por añadir despectivamente :

—Además, María Luisa no se ayuda nada…

—En efecto —se apresuró a replicar Andréïda,
y su voz vibró con una cólera imponente por lo
contenida— María Luisa no se ayuda nada, no acude a desvanecimientos fingidos ni a toda esa comedia que ablanda el corazón de los hombres…
Ignora el buen Torritos que es una muchacha educada en colegios americanos y que, por tanto, no es
afecta a hacer uso del fingimiento ingénito nuestro …Se muestra tal como es… y eso… en nuestro
medio… ¡es un pecado gravísimo!

—Es verdad, pobre María Luisa! —comentó.
Nelly.

Esta vez el doctor Raúl terció con pasión contenida:

—Es una mujer que no merece la piedad de
nadie…

Andréïda lo miró con frialdad despectiva.

—He aquí un concepto muy masculino —dijo
y preguntó en seguida—. Y, ¿cuáles son vuestras
razones para condenarla irremisiblemente, si pueden saberse…?

—Su amistad íntima con el esposo —irrumpió
Nelly indignada. 

—Mi amistad, no, se equivoca Nelly —replicó.
el doctor—. Mi amistad dejó de serlo en el momento mismo en que ví turbarse su mirada a la primera acusación de su mujer. Mi amistad, no —repitió con calor—. Debo hacer la aclaración que para mí, Don Manuel, fué siempre la imagen perfecta
del hombre probo, el prototipo del hombre honrado, del verdadero hombre… Su único crimen, el
único que yo le reconocía, era el de habérsele olvidado vivir y recordarlo demasiado tarde… 
Como Fausto… En cuanto a María Luisa, pienso,
que es un animal sano que se abrió paso, sin misericordia para nadie, dando rienda suelta al instinto.

Andréïda asintió e hizo saber que, por lo mismo, era injusto el condenarla. En todo caso habría que culpar a la vida, dijo.

Nelly prosiguió en su tarea de convencer a los periodistas para que, desde esa misma noche, modificaran su información en un sentido menos duro
para María Luisa. Torritos seguía sosteniendo, con
firmeza, su primer criterio y alegaba que de nada
serviría el que ellos sacrificaran el sensacionalismo de aquella información, cuando había una docena de periódicos que explotaban la nota roja y eran
muy leídos por el público grueso, influyendo decididamente en su ánimo. Nelly, a su vez, arguyó que
no veía la necesidad de dar a la publicidad las intimidades de aquella desgraciada ni beneficiaba en nada al público.

—Una verdad como un templo— exclamaron,
a una, la pareja de jóvenes periodistas y añadieron—
pero si las empresas periodísticas le hicieran caso, señora Nelly, ya podíamos ir todos buscando
otra profesión.

Sin embargo accedieron, en parte, a la pretensión de Nelly y se despidieron prometiendo el palidecer, en sus respectivas informaciones, todos aquellos incidentes que más pudieran perjudicar a
la acusada.

A la salida de los periodistas, Andréïda recordó a Nelly la necesidad de que se apresurara, si
quería hacerla llegar tarde a la recepción de la
Embajada.

El doctor Raúl, al oir aquéllo, se apresuró a ofrecer su coche; pero Andréïda, declinando su ofrecimiento, se apresuró a pedir a Torritos el que llamara a un “libre”.

—Con todo gusto, Andréïda —respondió el aludido; pero el doctor Raúl lo detuvo, y dirigiéndose
a Andréïda, se expresó en esta forma: —Un momento… Andréïda, necesito hablar con usted, necesito hablar ampliamente con usted…

Andréïda se limitó a responder con visible sequedad:

—Mañana estoy desde las once a la disposición
de usted…

—Bien sabe usted que no… que miente para
alejarme… pero yo no soy, ciertamente, hombre
al que se pueda arrojar a un lado con facilidad…

Torritos y Nelly cambiaron una mirada de indecible asombro y el primero dijo a la segunda en
tono que quería ser gracioso:

—Parece que los señores tienen, efectivamente, mucha precisión de hablar… ¿Nos marchamos,
querida amiga…?

Andréïda que había escuchado se apresuró a
decir: —Yo voy con ustedes…

Pero el doctor Raúl no se dió por vencido, y,
resueltamente expuso: Me opongo, en el caso que
no me permita usted reanudar esta plática, en
cualquier lugar, esta misma noche…

—Claro que no se lo permito —replicó con dureza la joven para añadir inmediatamente. —Doctor Torrelavega, se vuelve usted impertinente…

Raúl se limitó a inclinarse en suave cortesía.

—Andréïda, ¿me permites darte un consejo?
suplicó Nelly.

—Desde luego… ¡si me dejas en libertad de
seguirlo o no!

—Debes hablar con Raúl…

Andréïda quedó unos momentos silenciosa, gravemente pensativa y escuchó a Torritos que decía.

—¡Vaya, vaya, amigo Raúl, ya tiene usted una
partidaria y de fuerza! Que esta dulce Nelly, a pesar de su aspecto que ella se empeña en hacer rudo,
tiene un corazón que no le cabe en el pecho. —Ve
usted —añadió el periodista dirigiéndose a Andréïda
— al amante de las hipérboles no le fué dado encontrar ahora una para el inmenso corazón de Nelly.

—Quizá porque ese corazón le sea muy caro,
amigo Torritos.

—Quizám—asintió el periodista y dirigiéndose a
Raúl, dijo —amigo mío, usted que compone y descompone a diario, cuerpos de hombres y de mujeres me querrá decir, ¿qué órgano posee la mujer para adivinar lo que el mismo interesado ignora? 

El doctor Raúl sonrió con benevolencia.

—Amigo Torritos, no lo sé… Eso es un conocimiento que está por detrás de mi cabeza… como
aquel otro pensamiento de Pascal.

Nelly se levantó y, disponiéndose para irse con
Torritos, dijo a Andréïda con resolución:

—Te dejamos, el doctor Raúl será tan amable
que te acompañe a casa.

Pero Andréïda era difícil de convencer cuando
se había propuesto obrar en cierta forma, por lo
que se apresuró a decir con firmeza:

—No, Nelly, voy con ustedes. No estoy dispuesta, por hoy, a acceder a las pretensiones del doctor
Raúl —y con alegre cortesía, un poco forzada, se
dirigió a la puerta.

—Debes quedarte, Andréïda— insistió Nelly.

El doctor Raúl avanzó un paso. La mirada suplicante de los ojos viriles decidió, de pronto, a
Andréïda y más que nada influyó también, en su
repentina resolución, la sonrisa irónica que danzaba en la boca sarcástica de Torritos como diciéndole:
“Huyes, amiga mía, luego tienes miedo”. Sin embargo, todavía se resistió un poco…

—Ha escogido usted una hora bien inoportuna,
doctor Raúl —dijo—, le repito que no tengo nada
que hablar con usted… que no deseo escuchar esa
conversación tan importante…

—¡Decisiva…! —replicó el profesionista,

——Bien, decisiva… tanto, que le hace olvidar
su caballerosidad innata.

Luego, dirigiéndose a Nelly y a Torritos, añadió: 

—He cambiado de parecer, marcho con el doctor Raúl. Estaré de vuelta en casa, Nelly, dentro
de unos minutos. No deseo ya ir a la embajada.

Poco después, se despedían y tomaban rumbos diferentes Nelly y Torritos. Andréïda y Raúl.

</section>
<section epub:type="chapter" role="doc-chapter">

#Capítulo VII

Una vez que subieron Raúl y Andréïda en el
coche del primero, el doctor se apresuró a incomunicar el departamento del chauffeur, subiendo el
cristal y dándole órdenes para que caminara hasta
nueva disposición. Después, encendió un cigarrillo y
permaneció largo rato en silencio.

—Puesto que es forzoso escucharle, diga usted,
doctor… moduló Andréïda con frialdad.

El doctor Raúl le lanzó una mirada dolorosa.

—Por Dios, Andréïda— suplicó, con pasión. No
me sigas llamando doctor… No me alejes de ti en
esta forma…

Con fría ironía la mujer se limitó a pedirle el
que le dictara la fórmula que desease, con el objeto de complacerio.

—¡Me vuelves loco! —fué la respuesta del doctor a sus frías palabras. Y Andréïda replicó que
aquéllo eran muchos los hombres que se lo habían dicho.

—¡Háblame como aquella noche… en Cuernavaca! —suplicó el hombre.

— ¿Cuál…?

—¡Tan pronto has olvidado…!

La mujer rió.

—Diga usted mejor que no quiero recordar…
y acertará…

Aquéllo colmó la desesperación del joven doctor
quien, pasándose con violencia una mano por la
bien peinada cabellera, exclamó con acento terrible:

—Pero… por qué… por qué… Dios mío…
¿Te soy acaso repugnante?

—¡Indiferente…! afirmó imperturbable Andréïda. 

—Entonces… ¿aquéllo?

—Aquéllo… ¡Nada…!

El profesionista no podía comprender, no quería comprender, y exclamó: 

—¡Nada…! ¿Pero, de qué materia helada está usted hecha…?

Andréïda lanzó al aire una sonora carcajada,
un poco estridente.

—De alguna lo bastante bella para haberlo
atraído… 

——Diabólicamente bella, sí… afirmó el doctor,
Paraliza mi voluntad… soy un guiñapo al lado
tuyo… Yo, el doctor Raúl… el hombre de voluntad
de acero…

En el bello rostro de Andréïda se acentuó, aún
más, la despectiva sonrisa que parecía haberse estereotipado en su boca, desde el principio de la violenta explicación.

—¡Vea usted los funestos resultados de la carne…!

—De sobra sabes que no es la carne la que
me lleva a tí; lo que me hace olvidar de mí mismo
y soportar tu insultante desprecio… —el doctor
Raúl opacó apasionadamente su voz para añadir—

¡Es el Amor… el amor como nunca lo he sentido en
mi vida…!

—¿Amor…? —replicó Andréïda ficticiamente divertida— ¡Extraña cosa que un doctor hable
del amor…! ¡Ha estudiado usted muy mal su profesión!

—¡Basta de ironías— ordenó el doctor con impaciencia, e inmediatamente añadió con voz sorda
y dolorosa— ¡Comprendo ahora, al ingeniero
Alonso!

Excepto la misma Andréïda nadie se había atrevido a acusarla, en aquella forma directa y cruel,
de la muerte del ingeniero Alonso. Con un aire
extraviado e intensamente pálida se quedó contemplando al doctor Raúl, como si la inculpación hubiese tardado en penetrar a lo hondo de su conciencia. Un vasto cataclismo interior había dejado a su
piel exangüe, de la misma manera como cuando
allá en la Montaña se le había acercado el fantasma del ingeniero en plena descomposición orgánica. Los bellos ojos se hundieron en las órbitas, y
un escalofrío de terror recorrió su cuerpo, desde
las raíces de los cabellos hasta la punta de los pies.
Después, reaccionando con brusquedad, dijo:

—¿Para qué me lo recuerda usted…? Yo no
tengo la culpa de inspirar esas pasiones… yo no
las busco… yo no las quiero…

—¿No las buscas… no las quieres…?

—No.

—Y, entonces… ¿yo?

—¿Usted…? El hombre completo que necesitaba…

—¡Que necesitabas…! ¿No fuí más que eso?

—¡Sí!

El doctor Raúl no salía de su asombro. No podía creer lo que estaba oyendo. Maquinalmente se
pasó la mano por los ojos. Por un momento, pensó
que era víctima de una terrible pesadilla. Después,
entre dientes, musitó:

—No comprendo… ¡Yo fuí el primero, yo sé
que fuí el primero!

Andréïda rió suavemente. Había reconquistado
su aplomo.

Abrió su bolso de mano y, extrayendo un lápiz
de los labios, se puso a dibujar el contorno de su
bellísima boca.

—Es usted doctor… debe saberlo —apuntó con
sangrienta ironía maliciosa.

El doctor Raúl ya no escuchaba. El recuerdo
de aquella noche divina se había impuesto a su
cerebro y a sus sentidos. La sangre le golpeaba en
las sienes acelerando su respiración. Se inclinó hacia adelante. Sus ojos devoraban a la mujer con la
misma antigua llama codiciosa. En toda la noche
no le había parecido tan atrayente como en aquel
momento, con aquel gesto provocativo de su mano
dibujando los preciosos labios; con su hermoso
cuerpo hundido frioleramente en el rincón opuesto;
las deliciosas piernas cruzadas una sobre otra, exhibiendo, bajo la transparencia finísima de las medias, sus líneas puras. De toda ella se escapaba
una femenina atracción que turbaba el corazón del
hombre. Raúl olvidó todo. La actitud despectiva e
implacable de ella, su orgullo masculino y, humillándose, con esa fácil condescendencia del hombre
enamorado dispuesto a sacrificar su dignidad, a
cambio de la mujer que lo esclaviza, solicitó humildemente, profundamente:

—Andréïda, vida mía, cásate conmigo!

—¡No quiero…! —La réplica de la mujer había sido rápida, dura, no dejaba lugar a la menor
esperanza. El hombre se sublevó, dispuesto a insistir, a suplicar, a arrastrar su virilidad en pos del
don preciado.

—Pero, ¿por qué, vas a decirme por qué?. No
te soy repugnante desde el momento en que yo…

—Dejemos eso…

Loco de angustia, sabiendo que era superfluo
lo que iba a decir, convencido de que aquéllo no podía conmover a una mujer como Andréïda, todavía
replicó con esfuerzo:

—Le ofrezco a usted una brillante posición…
tendrá en mí todo lo que desee su capricho de mujer… Haré de usted una reina… ¡Tengo fortuna
suficiente para fabricarle un trono…!

Andréïda lanzó una carcajada. Le caía en gracia aquel cambio del tú en usted y aquellas frases
pueriles que no sonaban bien en la boca de un hombre como el doctor Raúl.

—¿En esta época en la que se desploman los tronos? —interrogó irónicamente— ¡Tiene usted
más poder del que yo le atribuía… y también más
imaginación!

El doctor Raúl midió, de pronto, el ridículo de
su posición suplicante; pero, asimismo, supo que
le era imposible retroceder. Estaba irremediablemente apresado en la terrible vorágine de su violenta pasión.

—No me atormentes más… exclamó con dolorosa exasperación— está ya ésto fuera de mis
posibilidades. ¡Me ves humillado…! ¿Quieres humillarme aún más…? ¿Quieres que te lo pida de rodillas?

Andréïda comprendió que aquéllo se alargaba
demasiado, que estaba abusando de su tremendo
ascendiente y que se imponía el definir, una vez
por todas, su situación.

—No es necesario… déjeme usted —suplicó
ella con mayor dulzura en la voz.

—No, no puedo obedecerte —afirmó Raúl— me
haces falta… ¡No puedo vivir sin ti, Andréïda…
amor mío… Raúl se acercó peligrosamente, trató
de sujetarla entre sus brazos; pero Andréïda se
desprendió con brusquedad.

—¡Déjeme usted, se lo he dicho ya!

El hombre se estremeció como si las palabras
de la mujer hubiesen restallado en su rostro, a la
manera de feroz latigazo. E irguiéndose, con cólera imponente, exclamó:

—No, no, mil veces no… Nunca antes de que
me digas qué razón te asiste para haberme entreabierto la puerta del paraíso y después arrojarme de
él ígnominiosamente. ¿Por qué hiciste aquéllo, di…?

De la cara de Andréïda desapareció el disgusto
anterior. Le placía mejor verlo encolerizado. Le
repugnaba luchar con un hombre que suplicaba y
se humillaba indignamente a sus pies.

—Suponed… que a mis conocimientos literarios hacía falta esa experiencia —dijo la mujer con
frialdad.

—¡No, no es posible!— la voz viril vibró dolorosamente.

—Pues sí; no podía hablar de lo que no había
experimentado. Además… quería saber si era efectivo mi triunfo…

—¿Tu triunfo…? Las cejas de Raúl se enarcaron con un asombro inaudito.

—Oh, no se alarme vuestra masculinidad —replicó Andréïda— no sobre usted precisamente…
sino sobre mí misma…

El doctor Raúl no salía de su asombro.

— ¡Comprendo! —dijo— Entonces… no fuiste la virgen loca… no; eres…

Andréïda apercibió el cuerpo como una joven
pantera.

—Dilo, —conminó con violencia.

En el rostro pálido del doctor Raúl se dibujó una
equívoca sonrisa galante.

—Eres el ángel, tal como lo describe la teología,
todo fuego y luz… pero, al mismo tiempo y por incongruencia divina…helado, hermafrodita y estéril…

—Esto último es lo que añade Swedenborg…,
del angel —aceptó Andréïda con fácil ironía— si
la memoria no me es infiel… Pero no es tanta mi
soberbia que me haga forjar la ilusión de creerme
un angel …ni aún siquiera el caído…

Andréïda hizo una pausa y con súbita resolución miró, frente a frente, al doctor Raúl, añadiendo con extraordinaria firmeza: —Muy bien; hemos entrado ya al terreno de las explicaciones. No
me rehuso a dártelas. Más antes quiero que me
digas, ¿qué es lo que me reprochas? ¿He sido para
ti una de esas mujeres defraudadoras de las que
se aleja el hombre con la impresión desencantada
de quien ha dejado las alas rotas…? O acaso, ¿mi
alma ha impreso en la tuya esa disonancia que hace que llegue al espíritu el hastío, mil veces peor
que la muerte…? Sé sincero y dime, ¿No acudiste
a mí destrozado por las otras mujeres…? ¿No por
un momento te rescaté el amor, ese juguete por el
cual llorabas como un niño mimado y que me mostraste roto entre tus manos…? Entonces… ¿qué
reclamas…? ¿De qué te soy deudora?…

—Quiero conservarte… ¡te quiero conmigo!— Raúl habló con los ojos bajos, su boca temblaba
de honda emoción.

—¡Quieres de mí… hacer una esposa — prosiguió Andréïda despectivamente— Es decir… alguien supeditado a ti, un triste ser de quien pretenderás hacerte dueño hasta fiscalizar sus acciones más mínimas. Querrás imprimir a mi espíritu
tus ideas; querrás moldearme a tu antojo; querrás
imponerme tus gustos… ¡Me tomarás cuando lo
desees, y me abandonarás cuando la mariposa de
las nuevas impresiones te lleve lejos de mí! ¡Todo
en una palabra… esposa!

Raúl se estremeció. Vislumbró en las palabras
de Andréïda un levísimo rayo de luz. Abrigó la
esperanza loca de convencerla y habló, habló apasionadamente, convincentemente…

—Te equivocas, no; —dijo— no concibo yo en
esa forma a la esposa. La concibo como la compañera de mi vida… Como el ángel que velará por
mis hijos. La mujer que me inyectará su entusiasmo en los instantes de desfallecimiento… la esposa santa que pasará su blanca mano por mi frente
cansada… cuando llegue agotado de luchar contra
el hombre y contra la muerte…

Andréïda se encogió de hombros. En sus ojos
brillaba la incredulidad.

—Te falta añadir —explicó— el ama de llaves
que se encargará de que no te falte nada… el ser
sufrido que soportará el malhumor que otros te
hayan ocasionado… la criatura indefensa en la
que descargarás tu cólera cuando no hayas podido
desahogarla en tu enemigo… No, eso no… terminó diciendo—. Nunca tendrá eso nada que ver conmigo…
Y con aire fastidiado pidió a Raúl que
diera órdenes al chauffeur, para que la dejara en
su casa.

—No se qué tengamos todavía por dilucidar
—añadió— ¡Sobraba ésto!

Después, viendo que Raúl permanecía en silencio, prosiguió:

—Ignoras que tengo la obsesión de mi independencia, y que no podrás nada contra ella…

—Yo también he estado siempre orgulloso de
mi libertad —replicó Raúl en voz baja—. Sin embargo, no te pido más de lo que yo mismo pienso
ofrecerte… Y con una docilidad repentina al deseo expreso de Andréïda, ordenó al _chauffeur_ que
regresase, trasmitiéndole la dirección de la joven.

Maquinalmente encendió un cigarrillo. Le pareció, de pronto, imposible cruzar nuevas palabras
con la bienamada.

Pero Andréïda no quedó satisfecha con aquel
silencio. Se le antojó que su triunfo había sido incompleto y sintió la necesidad de afirmarlo.

—En nombre del amor quiere usted esposarme,
doctor. En nombre del amor que es naturaleza…
que es el genio de la especie… Insigne doctor,
¿no pretenderá hacerme creer que lo ignora, verdad?—Y rió otra vez con aquella risa irónica y
violenta que le desconocía Raúl, y que tenía la virtud de exasperar sus nervios.

—Pues bien; como guste usted, Andréïda…
Sí, naturaleza, naturaleza sublime; precepto sagrado de perpetuar la especie, y al cual no se renusa el
más ínfimo ser creado por el Señor. ¡Unión sacrosanta de dos seres normalmente sanos; unión perpetua…

Andréïda lo interrumpió, y arrastrando las silabas, con una petulancia condescendiente, replicó:

—Unión perpetua, no! solo momentánea, amigo mío… Recuerde usted aquella observación sutilísima de Villiers…

—¡Siempre Villiers!— reprochó biandamente
Raúl.

—Estoy irremediablemente impregnada de él,
lo sabe usted, doctor. Decía —prosiguió imperturbable la joven— que recordará usted aquella observación que pone en boca de Edison sobre esa ley
física ineludible que hace que dos átomos no puedan tocarse… solo se penetran en la niebla de la
ilusión para encarnar al hijo, pagando así el tributo debido a la naturaleza… Después, amigo
mío… fenecida la ilusión, son dos seres que se
sumen en la soledad más espantosa, en la soledad
aquella a que se refiere el poeta: “¡la soledad de
dos en compañía!” Vea usted aguí las razones por
las que he sido asceta… y quiero seguir siéndolo…

Raúl rompió su indiferencia ficticia de los últimos minutos para argüir:

—No me convencerá usted jamás de la bondad
de su ascetismo… Ascetismo es negación, la peor
de las negaciones, la más terrible de las rebeldías a
las leyes naturales… La ofensa mayor que podamos hacer al Creador… —y en voz baja y extrañamente apasionada, añadió—: Yo siento un santo respeto al amor y al cuerpo…; al amor que es
carne… y que en ella funda su enorme sortilegio…

—Y que es vergüenza íntima que nos enrojece
el cuerpo, e indescriptible espanto y agonía que
agita al cerebro, haciénconos conocer cuanta animalidad hay en nuestro yo… y qué poco hay en
nuestro ser de divina esencia…

—Sí; y es también la gloria adorable de vivir;
es la veneración mística de la forma orgánica; es la
santa maravilla de sentirse, por un momento, Dios,
y palpar la eternidad… Asistir al milagro de extraer de la Nada la imagen del Todo… ¡Oh, la
alegría, el placer infinito de sentir en las venas la
Vida y darla… darla generosamente para conservar la humana cadena sobre la Tierra! ¡Santa potencia creadora…

Andréïda voivió a interrumpirlo. Ahora era
ella quien hablaba con violencia, como si buscase
la afirmación de sus propias convicciones por medio del sonido de las palabras, de las bellas palabras
culpables de haber torcido su hermosa naturaleza.

—Potencia tiránica que es lastre —dijo— y que
es principio de inercia, de voluptuosidad, de descomposición y de vergüenza. Potencia malévola que
es antítesis satánica que se opone al espiritu y paraliza las alas que en raudo y victorioso vuelo nos
llevaban hacia la luz… Mala potencia que hace de
usted, doctor Raúl, en estos momentos, un ser inferior; un ser indefenso, a merced mía, obligándolo a bajar la noble frente en movimiento servil;
haciendo de mi persona el instrumento de vuestra
humillación… ¡La grave humillación de vuestra
virilidad…! ¿Es posible que vuestra razón no tenga poder bastante para reprimir el movimiento
sumiso, cobarde, y acomodaticio que sufre vuestra
inteligencia? ¿Habéis llegado tan bajo que no os
importe abdicar vuestra dignidad en aras de un
mezquino fenómeno físico?… Decidme, entonces…
¿qué os queda del homo sapiens?

Insensiblemente el lenguaje de Andréïda se había hecho dueño de un estilo solemne que no acostumbraba en la vida diaria,

Raúl se removió con inquietud en su asiento,
carecía de palabras para replicar aquella terrible
admonición.

—Oh, es absurdo —se limitó a decir— es sencillamente absurdo todo lo que estáis diciendo…!

—En lo absurdo está mi fuerza y valentía espiritual…

—¿De dónde proviene esa inconcebible hostilidad vuestra a la naturaleza? —interrogó el hombre
desconcertado.

—Es la hostilidad del espíritu orgulloso que,
en noble obstinación, se liberta de las trabas ancestrales…

—¿En qué fuentes perversas habéis bañado
vuestro espíritu escogido, para, de tal suerte, desvincularlo de la materia? —De manera inconsciente
también Raúl había adoptado, al hablar, la misma
forma trascendental de Andréïda.

—En las fuentes de la civilización que es evolución incesante hacia lo nuevo, en anhelo de perfección.

—¡Basta —conminó Raúl—. ¡No intentaréis
ahora hacerme renegar de la obra del hombre…
No me obligaréis a maldecir la nueva torre nuestra,
diciéndome que en la Cumbre está la Confusión…

Andréïda sonrió.

—No seríais el primero, amigo mío. Desgraciadamente no tendríais, siquiera, esa originalidad…

El coche se había detenido frente a la casa de
Andréïda. —Me retiro— dijo la extraordinaria joven, tratando de descender del automóvil.

Raúl la retuvo por las manos. Otra vez la locura del amor extraviaba sus ojos. Se acercó a la
mujer. Una rodilla tocaba casi la alfombrilla del
coche. Su cuerpo, tenso por la emoción, casi se
arrodillaba frente a la amada implacable. 

—¡Andréïda… no quiero analizarte, no quiero saber nada de tí…! —dijo—. Te deseo… te
amo… Te amo… así… llanamente… sin especulaciones… Te amo sencillamente, humanamente…

Trató de sujetarla por la cintura, y esta vez
Andréïda no hizo ningún movimiento de repulsa.
Las manos del hombre resbalaron con lentitud, sin
lograr el objeto.

—¡Andréïda, que es ésto!— las pupilas del hombre se dilataron con un inenarrable espanto—. Tú
no eres de este mundo —afirmó Raúl con locura—.
Tú no perteneces a la tierra… tus ojos… esos ojos… tu carne… tu carne helada… tu carne
muerta… ¡Oh, es horrible! ¡Yo debo salvarte, yo
debo recuperarte a mi amor y a la vida! Dios mío,
¿no habrá una forma… una forma… un camino
para llegar a su corazón?

El hombre desfallecía, presa de una angustia
aguda. Andréïda descendió lentamente y abrió, con
su propio llavín, la puerta de la casa. Raúl, desde
el coche, la contemplaba alejarse con una violenta
desesperación y un terrible anonadamiento que le
impedía hacer el más leve movimiento.

—La pierdo, Señor… se va… se aleja —musitó para sí con un sollozo espantoso en el que agonizaba la suprema rebeldía de su virilidad—. Esto
no es humano, no. Señor, no permitirás que la pierda cuando todas las fibras de mi ser la reclaman,
cuando Tú sabes que nos has hecho el uno para el
otro… señor… piedad… piedad para un hombre que es solo eso… hombre…

Su callada imprecación se perdió en la indiferencia con la que son acogidas esta clase de súplicas por el Alto.

Andréïda, todavía antes de cerrar la puerta, se
detuvo en el dintel e hizo un vago ademán de
adiós… El hombre se quedó contemplando la
puerta cerrada tras de la cual se perdía, para siempre, su razón de vivir.

—Se fué… ¡la he perdido para siempre jamás…! Palabras de eternidad… palabras de
condenación eterna…

</section>
<section epub:type="chapter" role="doc-chapter">

#Capítulo VIII

La vida se reanudó para Andréïda en la misma
forma y con idénticas ocupaciones, distracciones,
etcétera, con que había transcurrido hasta su segundo encuentro con el doctor Raúl. solo que ahora se le antojaba, a la joven, extrañamente incolora
e insípida. Su trabajo carecía del antiguo atractivo
y se imponía a su espíritu como un deber enojoso.
Sus distracciones se veían amargadas por el temor
constante de sufrir un penoso encuentro con el doctor. Desde hacía varias semanas no sabía nada de
él ni quería saber tampoco. En la casa, Nelly se
había abstenido de hacer la más ligera referencia
ni la menor interrogación indiscreta. En cuanto a
su círculo íntimo de amigos parecía también, haberse puesto de acuerdo para no hablarle sobre el
profesionista. Alrededor de ésto se hacía un silencio que la molestaba y que, sin embargo, se consideraba incapaz para poder romperlo. Sabía que no
podría hablar nunca de aquéllo con indiferencia.
Sentía en el corazón como un duelo abstracto. Palpaba en su alma que algo se había roto, que, en
adelante, no podría ser nunca ya la misma. A todo
ésto venía a unirse un mal físico, del cual había
evitado buscar explicación, dejando caer sobre él
una especie de telón obscuro y expectante, en espera de que el tiempo se encargara de descorrerlo.

Por lo demás, hacía todo lo posible por no quedarse a solas con su pensamiento. En el día se dedicaba a trabajar febrilmente; en las noches concurría a selectos centros nocturnos y procuraba, al
regresar a casa en las primeras horas de la mañana, tomar cualquier estupefaciente que, en pocos
minutos la obligara a dormir con un sueño pesado
y fatigoso; pero que cumplía su necesaria misión
de olvido.

Nelly, testigo silencioso de aquella imponente
angustia, no se atrevía a intervenir por miedo a
envenenar la herida que sangraba. Todo lo más
que había hecho era tomar de confidente a Torritos
y suplicarle un sano consejo. El viejo periodista
se había mostrado reservado. A él también le imponía aquel secreto y orgulloso dolor, aquella femenina lucha desesperada por proseguir tercamente
el trazado camino absurdo.

Un tácito propósito hacía que las dos amigas
huyeran de quedarse a solas. Cruzaban las palabras indispensables con igual fraternidad cordial;
pero la confianza había escapado de sus relaciones.

De ahí que, una de tantas noches extrañó Nelly
el que se le acercara Andréïda con un plieguecillo
gris en la mano y mimosamente, como acostumbrara hacerlo en el pasado cuando quería obligar a
la amiga a que no le negase un favor mínimo, le
pidió que la acompañase a la casa de Pita.

Pita, la antigua condiscípula de las jóvenes, hacía tiempo que, por todos los medios, había intentado reanudar sus relaciones con Andréïda y Nelly, haciéndolas objeto de insistentes invitaciones
para que acudieran a sus fiestas y reuniones. 
Andréïda se había negado sistemáticamente, sabiendo
que el repentino interés de la mala compañera
radicaba en un equívoco deseo de exhibir en su casa
a la elegente mujer que atraía la atención del México intelectual. Por su parte, Pita, consideraba a
Andréïda como una hermosa figura decorativa,
digna de discurrir por sus elegantes salones. No
escapaba, tampoco, a la joven ladina, el que Andréïda se había convertido en una positiva atracción de
muy buen gusto. Le concedía, también, que se había abierto paso con la suficiente inteligencia y
discreción para guardar las apariencias y hacer posible su entrada en la mejor sociedad; pero ignoraba que tal fin ruin no era precisamente el perseguido por la singular Andréïda, y que su prestigio
como figura decorativa en los salones, la tenía muy
sin cuidado.

La propia Nelly, muchas veces había insistido
con Andréïda para que no desairara, en aquella
forma, las repetidas invitaciones de Pita, arguyendo que, como compañeras de niñez. Se le debían
ciertas consideraciones. Razón por la cual, aquella
noche, le causó gran sorpresa la petición de Andréïda y no pudo negarse a acompañarla.


\* \* \* {.espacio-arriba1 .centrado}

La residencia de Pita presentaba el animado
aspecto de costumbre. Con la debida anticipación había hecho saber a sus amistades el que esa noche tendría el gusto de presentarles a Andréïda, diciendo,
con elegante displicencia a todo aquel que quería
saberlo, el que no era tan misterioso el origen de su
amiga como las imaginaciones latinas se empeñaban en forjar. La astuta muchacha estaba segura
que surtiría el favorable efecto que se había propuesto aquel plieguecillo gris, dirigido aquella mañana a Andréïda. Y no se equivocaba. Conocedora
de la vida de Andréïda por haberla seguido, con
envidia, de cerca, se proponía vengar, en su reunión los ultrajes de que Andréïda la había hecho
víctima cuando niñas. Había elaborado todo un
plan atrevido para herir a la amiga ruidosamente.

Reinaba en el salón principal una bulliciosa expectación y todas las conversaciones rodaban alrededor de la personalidad desconcertante de Andréïda. En el grupito de caballeros y damas que rodeaban a Pita se comentaban, entre veras y bromas, los atractivos físicos de la extraordinaria
joven.

Uno de los caballeros, refiriéndose a los hermosos ojos de Andréïda, los calificó de profundos, abismales, extraños. Su mirada, dijo, electriza y, al
mismo tiempo, es fría, aceradamene fría. No podemos menos —añadió— que sentir un estremecimiento en nuestra espina dorsal; hay algo fatal en
ella que atemoriza y atrae, a la vez…

Pita rió del entusiasmo del caballero y tachó
su admiración de peligrosa e incendiaria. Despues,
la conversación degeneró por el resbaladizo terreno
de la murmuración, y Pita se apresuró a protestar
diciendo que había empezado demasiado pronto y
que no iban a dejar nada para más tarde, lo que
influiría en hacer un fracaso de su fiesta. Con
picaresca vivacidad hizo el elogio de la murmuración atribuyéndole lo que llamó la sal de la sociedad, pues sin ella, afirmó, las reuniones sociales
resultarían aburridísimas y no habría por qué hacerlas. Además, hizo constar que era una gimnasia
cerebral muy provechosa, y que afinaba el ingenio.
Todos estuvieron ruidosamente de acuerdo, y uno
más entusiasmado, propuso, con grave seriedad,
que se la exaltara en la debida forma.

—Levantémosle una estatua cerca del monumento a la Revolución —dijo—. Podríamos aprovechar el pedestal del Cabaliito…

Una dama insubstancial e ignorante que se encontraba cerca hizo un alarmado aspaviento y conminó al caballero, diciéndole: —Cuidado, que incurre usted en un pecado de lesa patria…

La salida chusca de la señora hizo reír de buena
gana a aquel grupito que necesitaba bien poco para
ésto y que tenía, a la alegría ruidosa y despreocupada, como una cualidad excelsa de independencia.

—¿Patria? —exclamó el caballero aludido, y, con
el ineguívoco deseo de hacer gala de un ingenio
del que estaba terriblemente convencido de poseer,
añadió: ¡Qué concepto y vocablo más anticuado!
Si ya no hay patria, señora mía —Patria significa
posesión, propiedad y no hay que olvidar que el
proloquio de actualidad es: “Toda propiedad es un
robo”. —El hombre había hinchado la voz al decir
esta última frase, lo que provocó una nueva risa
general. Y, deseando afirmar su momentáneo éxito, se dispuso a proseguir su hueca peroración, en
cuanto se acallaron las carcajadas.

—En nuestro siglo ya no hay patria, ya va desapareciendo la familia, ya no hay mío ni tuyo…

—Oh, no lo diga usted muy alto —lo interrumpió Pita entre grandes risotadas— que todavía nos
restan unos cuantos años amables para gozar nuestra situación privilegiada de propietarios…

De pronto, su agudo oído le hizo percibir la parada de un nuevo coche a la entrada, y se apresuró
a decir a sus amigos que probablemente llegaba Andréïda en aquellos instantes.

Sin embargo, el desencanto de todos fué grande
cuando vieron entrar, en lugar de Andréïda, al doctor Raúl.

El doctor Raúl contempló risueño al grupito y
se apresuró a interrogar a Pita.

—¿La he defraudado, señorita, no le esperaba tan pronto?

Pita reaccionó y contestó al punto:

—Nada de eso, doctor, es que creíamos arribaba otra persona a quien estamos esperando con expectación…

Cortésmente, el doctor Raúl interrogó quién era
el afortunado mortal que tanto les interesaba; pero
su pregunta recibió, por parte de Pita, una graciosa
contestación evasiva. El doctor Raúl se inclinó y,
acto seguido, pidió permiso para alejarse, invitando a una señorita, conocida suya, para que lo acompañase a bailar.

El grupo contempló, en silencio, cómo se alejaba el doctor Raúl con su pareja y, apenas se dieron
cuenta de que no podría ya oír sus comentarios,
una dama dijo a Pita:

—¿Por qué no le dijiste a quién esperábamos?

La aludida sonrió con aire de suficiencia.

—Amiguita, porque en ese caso, esta noche, nos
hubiéramos visto privadas de la presencia del ilustre doctor. No creo que ignoren ustedes —dijo enseguida, dirigiéndose al grupo— que Raúl ama apasionadamente a Andréïda y que, de un tiempo a
esta parte, la huye de manera notable para el más
ingenuo…

Todos, a una, hicieron un movimiento de asentimiento.

—Se rumora que el doctor Raúl —dijo uno de
los contertulios en voz baja, como si quisiese hacer
partícipes a sus amigos de un grave secreto de estado— le hizo proposiciones de casamiento y que
ella rehusó.

El caballero que hacía un rato había hecho gala
de ingenio a propósito del concepto de propiedad y
de patria, se apresuró a lanzar un chiste de mal
gusto.

—Está loco el doctor Raúl… ¡Hacer proposiciones de casamiento a una mujer tan bella…! Es
peligroso, peligrosísimo… Estas mujeres debía el
Estado obligarlas a ser propiedad comunal…

Rieron todos la gracejada, y Pita tuvo que intervenir.

—Está visto, amigo mío —dijo— que esta noche
está usted en vena comunista…

De nuevo escuchó ruido en el vestíbulo y todos
volvieron con curiosidad la cabeza. Esta vez no
quedaron defraudados, pues, por la monumental
puerta de cristales, apareció Andréïda exquisitamente vestida y a su lado Nelly, que por dar gusto
a la amiga, había abandonado su burdo traje sastre
de costumbre, cubriéndose con uno de noche, muy
modesto, y que llevaba con poca gracia y muy cohibida.

Pita se dirigió a ellas con los brazos abiertos, y
tuvo un acertado elogio para la toilette elegantísima de la primera. Después, volviéndose a sus amigos y con esa facilidad mundana del que en la vida
no ha tenido otra preocupación que la de moverse
entre gente insubstancial y elegante, hizo la presentación de sus antiguas condiscípulas.

Varios caballeros se apresuraron a pedir a Andréïda el que les concediese bailar con ella, a lo
que la joven accedió con la condición de que la
dejaran reposar un instante; pues dijo tener varios días indispuesta y que, si había acudido a casa
de Pita, solo se debía a un deseo de que la antigua
amiga de la infancia no pensase el que le hacía un
puevo desaire.

Pita se desentendió de la última frase de Andréïda y dijo: —Es cierto, estás un poco pálida y
ojerosa; hay menos firmeza en ti…

Y dirigiéndose a sus amistades, les pidió que la
dejaran sola con sus amigas un instante. Aseguró
que unos minutos de descanso mejorarían la salud
de Andréïda. Con la debida discreción se retiraron
todos, dirigiéndose al salón de juegos unos, y, otros,
al hermoso jardín que estaba feéricamente iluminado, y por el que discurrían numerosas parejas
entregadas al placer de la danza.

Cuando se quedaron las amigas completamente
a solas, Pita trató de iniciar, de la manera más
natural, la charla. Con un poquitín de sorna en el
acento meloso de la voz, interrogó a Nelly sobre su
vida y sobre aquellos famosos proyectos de rehabilitación para la mujer y que ésta gustara de hablar
allá en los tiempos, no muy lejanos, en los que su
prestigio de joven profesora la autorizaba para reunir, extraoficialmente, una docena de dóciles colegialas que escuchaban sus pláticas con un: 
eonato, de bostezo en los burlones labios, al decir de Pita.

Nelly se limitó a responcer a las maliciosas
preguntas, de la manera más sencilla, explicando:
a la mal intencionada Pita el que, por lo que se refería a sus actividades eran las de una periodista:
y en cuanto a sus antiguos proyectos los realizaba,:
en parte, puesto que tenía la satisfacción de impartir conocimientos útiles a varios grupos de muchachas empeñosas que se reunían, con este fin, en
centros culturales nocturnos.

Pita se apresuró a felicitar a Nelly con fingido
entusiasmo y a ofrecerle su colaboración, su granito de arena como hipócritamente tuvo a bien llamarlo, en la forma de un _check_.

Al oír aquello, Nelly se ruborizó intensamente,
por haber sido mal interpretado su celo al hablar,
y, en pocas palabras orgullosas, rehusó el aceptarlo.

Andréïda intervino, secretamente indignada.

—Acéptalo Nelly, —dijo— todo ha sido una pequeña equivocación. Nuestra amiga Pita cree que
el dinero es la palanca de Arquímedes, y con sinceridad te ha ofrecido la oportunidad de que tú
hagas la prueba; ya que ella, de sobra, conoce su
valor y lo emplea a las mil maravillas…

Pita palideció un poco; pero con su acostumbrado aplomo, replicó: —Eso es; solo que al acertado consejo de Andréïda debemos quitar el poquitín de acíbar con el cual siempre amarga todo
lo que proviene de vuestra servidora. Sin embargo, en gracia a su asistencia, esta noche estoy dispuesta
a perdonarle todo…

Andréïda sonrió a la vez que afirmó, con suavidad, el que no había venido precisamente a casa
de Pita para recibir el generoso perdón de ella, sino que había accedido, con toda sencillez, a un deseo de Nelly, quien consideraba que ya eran demasiadas las veces que había desairado sus interesadas invitaciones.

—¿Interesadas…? No comprendo —se apresuró a replicar Pita,

—¿Negarás, queridísima, que esta noche han
asistido a tu reunión connotadas personas solo porque tú anunciaste, con la debida anticipación, mi
presencia…?

Pita rió un poco corrida de que hubiera sido
descub1erto su secreto deseo con franqueza tan desnuda.

—Veo que sigues con la misma presunción de
siempre— dijo.

—Y añade que hoy con más fundamentos.

—Nelly alarmada de los términos violentos que
gmenazab'a tomar aquella conversación, suplicó:

—Pita, Andréïda ¿no irán ustedes a reñir aquí
y por futilezas…?

Andréïda se inclinó para hacer una ligera caricia a Nelly y le aseguró con voz tranquila:

—Descuida, Nelly, no temas; somos demasiado civilizadas para hacer una exhibición de nuestros sentimientos…

Pita suspiró como persona a quien se le quita,
de pronto, un gran peso de encima.

—Es verdad, Andréïda, esta noche no quiero
disentir en nada contigo… Y pasando a otra cosa —añadió— he aquí que se han cumplido tus vaticinios; es decir, los que hiciste respecto a tu persona. ¡Has conseguido aureolarte con un resplandor hermoso de mujer superior!

Pita era uno de esos seres ruines que no pueden hacer otra cosa que morder o modular, rastreramente, elogios con una intención equívoca.

—De mí sé decirte que te admiro —prosiguió
diciendo Pita— y mis convidados bien pronto te
mostrarán el interés que les inspiras y su inocultable entusiasmo… De las damas…

—No me adviertas nada… —dijo Andréïda—
Se el efecto exacto que hago siempre, y en vano tratarías de analizarlo tú, con certeza… Las mujeres me imitan, me estudian y me critican….Es
lo que busco y ojalá hubiese una docena de ellas
que supiera llegar al fondo de mi ser y me imitase
en lo esencial….

Pita se inclinó con ademán burlón.

—Siempre misteriosa… Haces bien… Te favorece —después levantándose, añadió—; ¿Vienen conmigo?

—Donde tú quieras —replicó Andréïda—. Por esta noche accedo a ser la maravilla exótica que tú,
sabio cicerone, mostrarás a tu manera, a los necios
de ahí dentro…

—Jesús, y que desprecio por los pobrecitos mortales —exclamó Pita con fingido asombro y añadió—
Salve Diosa… mas no creo que incluyas entre
esos… necios… al primero de tus admiradorea

—No se a quien te refieras… —contestó Andréïda un poco molesta.

—¡Es imposible! ¡Cuanta ignorancia…! Fingida, por supuesto, un ser superior, como tú, no
puede ignorar nada… ¡Es omnisciente!

Esta vez Andréïda reprochó a Pita, con dureza,
sus ironías y le hizo saber que estaba faltando a su
papel de anfitriona.

—Perdona —se excusó con falsa humildad Pita— Todo fué inocente broma… —y sabiendo e
efecto desconcertante que haría su frase siguiente en Andréïda, añadió abruptamente. —Me refiero
al doctor Raúl.

No se equivocó la mala amiga, Andréïda se estremeció como bajo un inopinado golpe que se está
muy lejos de esperar.

—¿Está aquí el doctor Raúl?

—¿Y qué tiene de extraño?— replicó la maligna joven.

—¿Conoce él, también, mi venida a tu casa…?

—No; temí que si lo sabía, me vería privada
de su presencia… Quizás no ignores que huye de ti…

—Huye, sí —aceptó pensativamente Andréïda—
y tiene razón. —Después, tomando una resolución
súbita, añadió —Lo siento mucho, Pita, pero me retiro

Pita no esperaba aquéllo.

—¿Cómo es eso? ¿Le temes…? —se apresuró a interrogar, y añadió en seguida, sinceramente trastornada. —No, no puedes marcharte, comprometerías mi situación en sociedad…

—Ignoraba ese poder mío— dijo Andréïda con
amarga ironía.

—Al contrario, lo conoces y lo explotas… ¡Ya
has olvidado tu antigia sinceridad ingénita, lo más
noble que había en tí…

Tocó a Nelly intervenir diciendo a Andréïda
que no debía hacer eso. Además, hizo saber a Pita
que ella estaba en espera de un amigo, de un viejo
periodista que se había permitido invitar temiendo
encontrarse un poco sola en aquella reunión. ¡Conoces mi misantropía! acabó por decir Nelly.

—Encantada —se apresuró a afirmar Pita— Tus
amigos son los míos y mi casa la tuya —y como si
recordase algo repentinamente, añadió ¡Ah, me olvidaba! Quizá ésto te haga quedar, Andréïda.

—Di…

— ¿Te acuerdas de María Isabel?

—No he de acordarme —repuso Andréïda, Y Nelly se apresuró a interrogar a su vez:

—¿Está aquí la linda chiquilla? ¡Qué gusto el
poder saludarla…! ¡Feliz idea la de haber venido
a tu casa!

—¿Verdad que sí…? —y ya un poco tranquilizada, prosiguió Pita— La encontrarán dentro de
algunos momentos. Ahora está arriba con su esposo… Un rufián elegante, pero simpático… Jugador poco escrupuloso que usa de Isabel como de una
mampara, por lo que ninguna ama de casa se atreve a expulsarlo… Tienen tres chiquitines, al llegar aquí, me contaba ella que tenía a uno enfermo.
Trabel, naturalmente, hubiese querido quedarse a la
cabecera del chiquillo; pero él la obligó a venir…

Sabía, de antemano, que vendrían ciertos amigos
de papá y que jugarían fuerte…

Nelly no podía creer lo que oía.

—¡Es posible todo eso! —dijo— ¡Pobre chiquilla!

Pita, consciente del interés que sus informaciones despertaban en las dos amigas, añadió:

—Ah, y no vayan ustedes a demostrarle que saben todo lo que les he contado; tiene el orgullo de su
dolor… Le disgusta que alguien penetre y conozca
el abismo de su matrimonio… Es más, creo que de
su boca, el esposo, no ha sufrido el menor reproche.

—¡Qué inexcrutables son los designios de la vida! —comentó Nelly.

—¡Y tanto…! —corroboró Pita— ¿Recuerdan que el último día, allá en el St. Mary? Siempre que me
acuerdo me estremezco con escalofríos de miedo…
Se ha ido cumpliendo, paso a paso, lo que esta loca
de Andréïda nos dijo a todas en un momento de buen
humor…

—¿También lo que te dije a ti…? —interrogó
Andréïda sarcásticamente.

—También —aceptó Pita— contigo es inútil mentir… solo, que ahora tengo la fuerza y aplomo de
mis vicios y he arrojado, lejos de mí, la hipocresía y el miedo que la ignorancia de colegiala me
hacía sentir. Por eso creo que ahora logrraremos ser
amigas —y dirigiéndose directamente a Andréïda,
prosiguió—. Me darás oportunidad de mostrarte el
fonco real de nuestras mujeres de sociedad, que es
más sucio de lo que tú y todos se imaginan….
Señoras, felicitémonos —exclamó la joven con Júbilo
detonante—. Ya contamos en nuestro México con la
perversión de las grandes ciudades del Mundo. ¡Es
un paso hacia la civilización!

—Hacia la civilización, no —protestó Nelly— te
equivocas, Pita, la calumnias…

—¿Entonces…?

—¡Qué se yo…! Sería tan largo de explicar…

Pita no era mujer que gustara de gastar palabras en especulaciones complicadas, por lo que, preguntando a Andréïda si ya se encontraba bien, la
invitó a ir al jardín para presentarla a nuevas amistades. Nelly se rehusó a acompañarlas repitiendo
que quedaba en el salón esperanco a Torritos.

—¡Vamos Andréïda… tu sonrisa mejor y más
enigmática —suplicó Pita, y las dos antiguas condiscípulas salieron del brazo con esa flexibilidad femenina que permite a dos enemigas irreconciliables
marchar juntas y cambiar sonrisas melosas.

\* \* \* {.espacio-arriba1 .centrado}

Como lo imaginaba Nelly, no tardó mucho Torritos en reunírsele. Entró el viejo periodista y sus
primeras frases fueron de reproche para la amiga
por haberlo obligado a pisar aquella casa. Le hizo
saber que se encontraba fuera de órbita en aquellas
reuniones sociales que no eran precisamente su elemento, Nelly, un poco exasperada por la escena anterior con Pita, le replicó con acritud, a su vez, diciéndole que aborrecía a los hombres de costumbres
hechas y que ya sabía que el disgusto de él no provenía, con exactitud, de haber acudido a una reunión
de alta sociedad, sino a que se había visto forzado
a privarse de ir al café y tomar unas cuantas copas de cognac en su peña.
Torritos sonrió. Le causaba extrañeza cómica aquella inusitaca violencia en las expresiones de Nelly, echaba a faltar la ecuanimidad de la amiga y así
se lo hizo saber. Después, en broma, y con una gran
ternura en la voz, la dijo: {.espacio-arriba1}

—La comprendo, Nelly, sé que comienza a cansarla esta vida ostentosa que arrastra por dar gusto a Andréïda. Su lugar está en un hogar con media
docena de chiquillos a quienes educar, moldear, cuidar, etc… Está…

Nelly lo interrumpió. Hacía mucho tiempo, dijo,
que eso se lo había dicho la propia Andréïda; hacía
también mucho tiempo que ella estaba ya íntimamente convencida de que aquello era verdad, era
su verdad…

—Amar… tener hijos míos… tener algo
legítimamente mío…. ¡Que hermoso, Torritos! —dijo con voz soñadora. Ella que se iría de la vida
sin conocer nada de eso, sin dejar huella de su breve tránsito, Sola, siempre sola. Aferrándose el cariño de una amiga como un hambriento a un mendrugo, como un desnudo a un harapo, como un inválido
a un cayado torcido…

Torritos la escuchaba con emoción contenida.
Jamás Nelly se había explayado con tanta franqueza, nunca la amiga le había hecho participe de toda la amargura que había en el fondo de su ser.
Amargura que había pasado desapercibida hasta
para él, observador sutil de la vida y lo humano.

—Mi tragedia es la eterna tragedia vulgar de
la mujer fea —decía en aquellos momentos la voz de Nelly—. Es la tragedia de esas hijastras de la
naturaleza que, desde pequeñas, se acostumbran
a ver en los ojos de las otras mujeres, mejor favorecidas, la compasión torpe o la burla despiadada,
y….en los hombres, la indiferencia. La indiferencia más cruel, aquélla que les hace considerar
a la mujer de esta especie como un mueble útil,
como un ser anónimo, bueno para desempeñar la
más ruin tarea. En quien el jefe de la oficina no se
ve obligado a ver el cansancio que atenacea sus dedos y se los engarrota; una mujer, en fin, a la que
no hay que guardar delicadeza alguna; sus oídos
pueden escucharlo todo; sus ojos contemplarlo todo. La mujer fea no tiene por qué tener pudor, no
es mujer….es algo intermedio….¡Es una desgraciada en la que no se admite que posea alma y cprazón…!

Después de una breve pausa, Nelly reanudó sus
frases amargas. —¿Sabe Torritos? —dijo— A veces pienso que, las mujeres como yo, son las que
debíamos llevar a cabo el ideal de Andréïda, sin
correr el peligro que ella corre a cada paso.

—El ideal de Andréïda— comentó pensativo
Torritos y con repentina violencia, añadió: —No,
nunca… Su amiga es más nociva a la sociedad de
lo que usted cree… ¡Pobre del mundo, pobres de
nosotros, si su semilla arraigase…! Felizmente 
—concluyó— no es mas que una excepción… una
excepción encantadora que distrae a los hombres.

—¿Cree usted que es excepción…? replicó
Nelly con pasión. No lo afirmaría yo tan rotundamente… Vuelva los ojos hacia los Estados Unidos del Norte… Vea mujeres directoras de negociaciones, gerentes de ésto y lo otro, mujeres que
se han labrado una vida independiente del matrimonio y habitan un cuarto de hotel, como cualquier
hombre soltero; mujeres que han sacrificado todo
lo que hubo en ellas de mujer… No lo afirmaría
yo, amigo Torritos; desde que estoy con Andréïda
he dejado de tener ideas hechas…

—Ya lo veo— exclamó Torritos, lastimada una
fibra sensible de su ser, —Tiene mucha ascendencia sobre usted…

Se estableció un silencio entre ellos. Torritos
repentinamente había palidecido y estaba frente a
Nelly como un hombre que se encuentra a sí mismo. Una timidez inusitada le impedía hacer a la
amiga una confesión por largo tiempo guardada
en silencio. Nelly, vuelta de nuevo a su estado pasivo, contemplaba, con indiferencia, el espectáculo
brillante del jardín.

—Yo… hace tiempo que quiero decirle algo…
algo que significa mucho para mí… que significa…
Pero Nelly no atendía al amigo, su voz temblorosa no le había llegado al corazón. Y es que
una angustia súbita se le había clavado en el pecho y no podía despegar los ojos de la silueta de
Andréïda que se dibujaba en la puerta, del brazo
de un caballero.

—Teorritos, vea usted a Andréïda… ¡qué pálida viene!

Torritos cerró los ojos. Comprendió que había
estado a punto de hacer ese ridículo que el hombre
nunca llega a perdonarse a sí mismo.

Mientras tanto, Andréïda, en la puerta del
fondo, daba las gracias a su compañero de baile
y le decía que un poco de reposo, al lado de sus
amigos, sería suficiente para reponerse del malestar que experimentaba. Nelly corrió hacia ella.

—¿Qué te pasa, querida? —interrgó seriamente alarmada la maternal amiga. Hacía días que
insistía con Andréïda para que consultase un doctor; pero había sido en vano.

Torritos también se apresuró a interrogar a
Andréïda sobre si se sentía enferma. A ambos replicó la joven que no era nada. Y dirigiéndose a
Nelly le pidió que fuera al piso de arriba en busca
de Isabel, con quien, dijo, tenía muchas ganas de
charlar, Nelly se resistió un poco. No quería separarse de la amiga y le sugirió el retorno inmediato a la casa. Sin embargo, acabó por obedecer
el deseo de Andréïda y dejó sola a la amiga con el
periodista.

—Andréïda, debe usted obedecer a Nelly y consultar un médico— dijo Torritos en cuanto desapareció Nelly— yo también la noto desmejorada, cambiada… si, (hizo constar observándola detenidamente) hasta sus facciones son otras…

—¿Usted también, Torritos?, mal compañero
he elegido— replicó sonriente Andréïda; después,
mirando directamente y con inequívoca intención a
los ojos del periodista, añadió; —¿Debo felicitarlo?

Torritos se puso en guardia y exclamó de manera evasiva:

—No comprendo…

—Vaya, vaya, ¿acaso cree que estoy ciega?
—y entre veras y bromas habló Andréïda— Niégueme que, en el momento en que entré, iba a confesarle su amor a Nelly.

Mas por ganar tiempo que por negar, interrogó Torritos:

—¿Mi amor…?

—Veo, mi querido amigo, que tendré que desenmascararle… ¡Redomado hipócrita…!

El viejo periodista abandonó su primera actitud
defensiva —Es verdad, Andréïda —aceptó— veo
que es usted más mujer de lo que usted misma
quiere…

—Debía usted haber premiado mi clarividencia con algo mejor que llamarme mujer… reprochó Andréïda con una sonrisa.

Como usted guste, querida chiquilla, replicó el
periodista, y su voz vibró tierna y grave.

Andréïda comentó con volubilidad:

—Chiquilla… A nadie se le ha ocurrido nunca llamarme de ese modo y han tenido razón; yo
siempre he sido vieja, terriblemente vieja.

Torritos contempló un rato a Andréïda con extrañeza.

—¿Qué le pasa, Andréïda? —dijo—. No es usted la misma; me parece que empieza a perder esa
seguridad maravillosa sobre sí misma que es lo que
la hacía distinguirse del resto de las mujeres…

Andréïda se pasó angustiosamente la mano por
los ojos. Por primera vez no le disgustaba que un
extraño fuera testigo de su debilidad.

—No sé yo misma qué es lo que me pasa…
y añadió en seguida—. Tengo un desasosiego interior que me roba la serenidad; ya no soy dueña
de mis actos… Ha acertado usted… Pero dejemos mi persona y volvamos a lo suyo…

—Rehuso el hacerlo… replicó el periodista con
cierta sequedad.

Andréïda vaciló un momento antes de reanudar
la conversación. Luego, con una dulzura desconocida en la voz hizo saber a Torritos que no era simple curiosidad lo que la impulsaba a hablar, sino
el deseo de hacer feliz a dos personas queridas.
Ella había observado con gusto, afirmó, el que Torritos hubiera sabido quitarle las gafas ahumadas
a Nelly, asomándose al lago de imponderable belleza y serenidad que era su alma.

—Si, amigo mío, —repitió— usted ha sabido
ver la belleza de los ojos de Nelly que es la belleza
de su propia alma, y ha quitado las anteojeras que
los afeaban. Pero, sufro por usted —dijo la extraordinaria mujer—. Nelly es de las mujeres que,
aún viejecitas, siguen soñando con el príncipe azul… Ellas, las privadas de belleza, saben amarla mucho más intensamente que las otras… ¡Crueldades de la vida, Torritos !

Torritos asintió. Nelly y él, dijo, eran víctimas
de una tragedia bien común… La fealdad condenaba sus vidas a lo mezquino, a lo nimio, a lo
sin importancia… Eran seres condenados a discurrir por la vida inadvertidos. Sus escasas alegrías hechas de futilezas; sus dolores de pequeñas
cosas… Seres despreciables de quienes nadie intenta saber si sufren o si gozan. En pocas palabras: ¡Seres sin importancia!

Andréïda protestó.

—Usted lo ha dicho, ¡seres sin importancia!,
pero que saben hacer los más grandes sacrificios
calladamente… las acciones más bellas en completo silencio…

—No, no, Andréïda, se equivoca —replicó a su
vez Torritos—. ¡Alguna vez tenía que equivocarse! En nosotros todo es pequeño, todo es nimio;
nuestra medianía nos nulifica… No ocupamos lugar… Vivimos sin ser amados… y jamás nuestro recuerdo nubla unos ojos… Y en mí todavía
hay un agravante más… ¿Recuerda usted la frase del doctor Raúl la noche de la reconstrucción
del crimen de María Luisa?

Andréïda se estremeció con innegable disgusto.

—No se a cuál se referirá usted…

—Aquélla que, refiriéndose al esposo de María Luisa, dijo que habíasele olvidado vivir su vida
como a Fausto… ¡Para mí también es demasiado
tarde! —añadió el hombre—.

Súbitamente tranquilizada Andréïda, arguyó:

—No lo crea, Torritos, no para una mujer como Nelly— en seguida, casi maternalmente prosiguió la joven. —No desespere, verá, todavía meceré yo en su cuna a su primer chiquitín…

Los ojos del viejo periodista se humedecieron.

—¡Qué buena es usted, Andréïda!

—¡Buena!— repitió pensativa la mujer—. Tiene usted el privilegio de encontrar en mí lo que
no existe… lo que no ha existido nunca.

Torritos permaneció silencioso. Observaba el
cambiado semblante de Andréïda con una atención
preocupada. Y, a boca de jarro, espetó a la amiga:

—Y, volviendo al doctor Raúl, ¿No lo ha visto usted…? 

—Aquí no… todavía no…

—¿Está aquí…? No lo hubiese imaginado
—exclamó el periodista y con súbita resolución, añadió—. ¿Sabe usted la vida que lleva ahora el doctor Raúl…?

—No; además no me interesa conocerla…

—¿No se engaña usted en ésto… como en tantas cosas…?

—No, Torritos —la voz de Andréïda se tornó
extrañamente suplicante— no hablemos de él…
¿Quiere usted…?

—¡Por primera vez escucho un ruego de sus
labios! —comentó el periodista enarcando las cejas
con asombro—. ¿Teme usted hablar de él?

Aquéllo era ya demasiado para Andréïda que
replicó con violencia:

—No, ya le he dicho a usted que no…

—Bien, no se enfade —y el periodista prosiguió con dulzura—. Usted ha sido esta noche buena conmigo, y… yo no quiero que alguien la vaya
a herir ahí dentro… No quiero que usted marche
indefensa… que observen en la mujer fuerte que
es usted, un ademán de desfallecimiento, un gesto
de cobardía…

—¿Es que usted cree que lo que se relacione
con el doctor Raúl puede afectarme a tal extremo?

—Lo veremos ahora —replicó Torritos—. Es
tiempo de desengañarse… ¿Sabe usted que Raúl
lleva una vida de crápula…? ¿Sabe que ha perdido grandes cantidades en el juego? ¿Sabe usted
que ha vuelto con la actriz del Politeama y se exhibe
con ella, sin recato alguno…? Ve usted… tengo
yo razón… ¡Ha palidecido, sus manos tiemblan!

Andréïda se removió en el asiento como un
animal acosado, sus ojos tenían un brillo imponente, su voz temblaba de cólera. —¿Qué es lo que
se propone usted…? Tiemblo, sí; me duele ver a
un hombre, del cual tenía un concepto tan alto, caer
en esa forma… Palidezco, sí, de alegría. La alegría de poder confirmar tan claramente una antigua convicción mía y que no es otra que aquélla
sobre que el amor disuelve todo lo que hay de elevado en nosotros… Tiemblo… palidezco… pero
no por él… no por él, Torritos…

Nunca había visto Torritos a Andréïda en un
estado igual de agitación. Sin embargo, creyó que
su deber era, en aquellos momentos, desenmascarar
cruelmente y de manera implacable los sentimientos de Andréïda, con la esperanza de provocar una
benéfica reacción en aquel espíritu femenino empeñado en marchar a contrapelo por la vida.

—Aún hay más, Andréïda— añadió el periodista después de una breve pausa. —¿Sabe usted
que en días pasados llegó en estado inconveniente
a efectuar una delicada operación quirúrgica y que
pronto será expulsado del Sanatorio? ¿Sabe usted,
también, que sus enemigos aprovechan esta oportunidad para derribarlo…?

—Eso no… ¡los cobardes! eso no— La voz
de Andréïda había vibrado con un femenino dolor
que no escapó a la perspicacia del periodista.

—Eso será, Andréïda —insistió Torritos—
Eso es la obra suya… La derrota de Raúl no obedece al amor, en la forma absurda en que usted
lo concibe; ha sido… efecto de la irrazonable actitud de usted frente a un hombre que se inmolaba
entero a sus pies… Ha sido… su orgullo de mujer llevado al extremo… ha sido… una aberración de sus potencias espirituales, un desquiciamiento de su ser por caminos nuevos y nocivos…

Andréïda se levantó con violencia de su asiento, todo su ser se revolvía por la indignación.

—¡Basta!— ordenó secamente—. A nadie he
permitido antes que me califique… Y, usted…
usted es hombre… no comprenderá nunca…

—Tal vez comprenda más de lo que usted cree,
—arguyó Torritos con suavidad—. Pero, aquí está
ya Nelly de regreso, callemos…

En efecto, Nelly entraba en aquellos momentos acompañada de Isabel. La antigua colegiala se
había convertido en una espléndida mujer, de mirar
suavísimo que parecía pedir a la vida permiso para
deslizarse sobre ella. Había también, en la tersa
frente, un como dolor escondido que en vano se
desea ocultar a la curiosidad de la gente. Al ver a
Andréïda corrió hacia ella y sin poder contenerse la
llenó de besos y de lágrimas. Andréïda la abrazó
efusivamente y volviéndose a Torritos hizo la presentación de la amiga.

Poco después, Torritos se excusaba con el pretexto de que seguramente, las antiguas amigas tendrían mucho de que hablar e inclinándose al oído
de Andréïda, la dijo en voz baja:

—Hoy dicta el juez la sentencia de María Luisa. La deliberación estará ya por terminar y necesito conocerla para el periódico. Afortunadamente a Nelly se le ha olvidado, no le diga usted nada,
me temo que sea desfavorable.

Andréïda asintió en silencio.

—¿Y qué conciliábulo es ese?— interrogó Nelly, pero, con habilidad, Andréïda rehuyó una contestación comprometedora,

Apenas retirado Torritos, Isabel comentó:

—Buena persona, ¿verdad?

—Excelente— apuntó Andréïda.

——Y parece ver a Nelly con mucho cariño, hasta su acento cambia cuando se dirige a ella….

—Chutt—, hizo Andréïda— ¡Qué poco discreta eres, Marisabel, la interesada lo ignora todo…

Intensamente ruborizada la pobre Nelly se limitó a decir: —Pero qué locas son ustedes dos,
¡qué va a fijarse Torritos en mí!—

Andréïda e Isabel lanzaron al aire la más sonora de las carcajadas, a tiempo —que la primera
hacía saber: —Nelly mía, a ti es a la única que
falta enterarse de la pasión muy noble del buen
señor …solo que ese afán tuyo de usar gafas,
en lugar de mejorarte la vista, te la nublan…

Nelly refunfuñó algo sobre que no quería que
se hiciesen bromas a costa suya y poco después se
encerró en un silencio hostil que hizo la mar de
gracia a las dos amigas.

De aquéllo pasaron a interrogarse mutuamente
sobre lo que había sido de sus vidas y a recordar,
con ese gusto que tiene para todos el rememorar
cosas de la infancia, un mil incidentes de colegio.
Sin embargo, cuando tocó en turno a Marisabel el
hablar sobre su vida se limitó a decir:

—Me casé… y tengo tres chiquitines más lindos que la aurora… 

Andréïda pareció reflexionar un breve espacio
de tiempo y dijo:

—¡Me casé…! Muy lacónico es eso… ¿ya no
tienes confianza en nosotras…?

—¡Cómo te atreves a pensar eso! —arguyó debílmente Isabel.

—¿Entonces…? interrogó Nelly.

—Pues… nada… eso… me casé. La felicidad no puede expresarse… no hay palabras…

—¡Ni para el dolor tampoco —no pudo menos
que exclamar Andréïda.

—¿Ya has adivinado…? En los grandes ojos
de eterna niña temblaba una chispita supersticiosa.

—No; tú has confesado… fué la lacónica respuesta de Andréïda.

Pues sí, —admitió dócilmente Isabel—, tienes
razón Andréïda, siempre tienes razón…

Nelly miró a Isabel, la joven repetía inconscientemente una frase que ella siempre tenía en
los labios para la amiga.

—Anda, Isabel, desahoga el corazón— fué la
tierna invitación de Andréïda.

—¿Recuerdan mis ilusiones? —preguntó Isabel

—¡No hemos de recordarlas!—

Y puesta en este término la conversación de
las tres amigas, no fué muy difícil para Isabel narrar el calvario de su vida. En su hondo drama convugal, que no se apartaba demasiado de la vulgaridad de otros muchos, había desde la diaria incompresión despiadada del marido hasta la ruin bajeza de golpear a una mujer que tenía, para él, el
gran pecado, primero, de haberlo defrtaudado con
una fortuna que él pensara mayor y, segundo, el
de haberlo llenado de hijos, a su decir, y el de recibir sus despotismos con una eterna sumisión y
dulzura, cuyo dolor no sabía tomar otro camino que
el de las lágrimas, lágrimas que tenían la virtud
de exasperar sus nervios de vicioso.

Todo aquéllo que se podía decir en tan pocas
palabras y que, sin embargo, reflejaba el drama de
una vida rota, lo escucharon Andréïda y Nelly con
silenciosa indignación:

Repentinamente Andréïda, tomó entre sus manos el rostro lloroso de Isabel y con una emoción
profunda en la voz, dijo:

—Isabel, por ellos, por tus hijos, sepárate de
ese hombre…

La joven, niña a pesar de los años transcurridos y de haber llevado tres hijos en su seno fecundo, sollozó con impotencia infantil. —¡No puedo!—

—¿Has probado apartarlo del juego, has probado…? —interrogó Nelly con dulzura y menos
amiga de las soluciones drásticas.

—Lo he probado todo… no hay redención posible— fué la respuesta dolorosamente desarmada.

——¡Sepárate! La voz de Andréïda sonaba extrahamente imperiosa.

—¡No puedo… no puedo … no puedo!— Y
las palabras de Isabel parecían modular una queja
elerna, impotente, fatalista y resignada.

—Nuestro México tiene leyes muy amplias en
cuestión de divorcio— arguyó Nelly,

—No me atormenten más —suplicó Isabel—.
Todo eso lo he pensado en largas noches de insomnio, en largas noches infernales de angustia,

—¿Qué te lo impide… tu religión… tu catolicismo arraigado? Andréïda, la mujer nueva, no
podía concebir aquella resignación fatalista de la
amiga.

—No es tampoco eso… fué la desconcertante
respuesta.

Y Nelly no pudo menos que reprochar:

—Le tienes un temor inerranable, injustificado… ¡Existen leyes que te protejen…!

Isabel levantó los ojos. Había en ellos de todo,
angustia, rebeldía, espanto, indignación, sacrificio
sublime.

—¡Leyes! ¡Y el pan de mis hijos…! ¿quién lo
traerá a la casa, si me separo de él? Un hombre
que no tiene un sueldo, una finca, nada. Que no
tiene más que la bajeza de sus trampas en el juego, consentidas benévolamente por los que lo vieron nacer y tienen piedad de mí y de mis hijos.

Andréïda irguió el cuerpo, todo su rostro condenaba la cobardía de la amiga.

—Ahí está el mal —dijo—. El miedo al hambre… ese terrible miedo al hambre que agarrota
a la mujer en el potro de su martirio. El no saberse ganar la vida… después, volviéndose a Nelly, exclamó. —Tienes razón, no hay redención posible mientras la mujer no conquiste su independencia económica. —Y dirigiéndose de nuevo a Isabel,
prosiguió. —Tú, mujercita radiante, hecha para el
amor, criatura cuyo corazón no sabe abrirse mas
que una vez, pisoteada, envilecida ¿y, por quién?
Por un hombre… ¡Por bien poca cosa…!

La forma despectiva con la que fueron pronunciadas las últimas palabras, hizo estremecer a Nelly, recordando otro drama aún más profundo, el
de la propia Andréïda.

Hubo un nuevo silencio en el que Andréïda y
Nelly trataron de convencerse interiormente cómo
podría existir una mujer resignada a vivir en aquel
infierno moral.

Segundos más tarde, Nelly con voz convincente
y acariciadora intentó convencer a Isabel de que
se libertara de aquel miedo a la vida.

—Nosotras te ayudaremos— dijo generosamente.

—El miedo a la vida… a cuántas mujeres esclaviza— comentó Andréïda. 

Isabel se limitó a decir que era una cobarde, no
lo podía evitar…

—¡Tengo miedo a la vida! —admitió— pero no
por mí, sino por ellos, por los niños, por los inocentes seres que no deben jamás sufrir, que no
deben saber nunca del dolor de vivir…

Nelly se enterneció al escuchar aquéllo.

—Y harás de ellos —dijo— otros impreparados;
mullirás de tal forma su camino que, cuando la
vida los obligue a marchar solos, el primer guijarro hará sangrar sus piecesitos, y si caen, jamás
podrán levantarse…

Isabel protestó sollozando, quería para ellos lo
mejor de la vida, la alegría, la felicidad.

—Pobre Marisabel— comentó con dulzura Andréïda—. Eres cobarde y, sin embargo, tu vida
será un rosario de abnegaciones sin fin… de sacrificios, que la mujer más fuerte temblaría solo de
imaginarlos… como tiemblo yo.

—Levanta la frente, Isabel, afronta la vida…
¡Por ellos!…

Las palabras de Nelly se perdieron entre la algarabía bulliciosa que hizo Pita al entrar.

—Muchachas, ¿saben ya la noticia? —interrogó la recién llegada.

¿Cuál, dínosla? —interrogó Nelly.

—Nada, que la pobre María Luisa ha sido condenada a diecisiete años y pico de prisión.

Al oír aquello, Nelly e Isabel estallaron en sollozos. Andréïda se quedó mirando fijamente a Pita
y comentó con amargura: —Y lo dices así… con tanta naturalidad…

Pita levantó los hombros.

— Y, ¿cómo quieres que lo diga? Eso… todo
el mundo lo esperaba, era inevitable.

——Desventurada María Luisa— escuchó Nelly
que decía Isabel entre sollozos. —Todavía hay seres más desgraciados que yo…

—No, Marisabel, no des abrigo, ni por un momento, a ese pensamiento —la dijo Nelly a su oído
raralizaría tu acción… No vuelvas nunca los ojos
hacia abajo… vuélvelos hacia la luz…

En aquellos instantes entró Torritos, quien no
tuvo otro remedio que confirmar, en pocas palabras, la fatal noticia. Y, contestando a pregunta
que le hiciera Andréïda sobre lo que se podía hacer
para amenguar la pena, dijo que no había otro recurso que el reglamentario, o sea, el apelar de la
sentencia.

—Hay esperanzas —añadió— aunque debo decirles que su amiguita carece de lo más indispensable. Y quizá, por falta de recursos…

—Por falta de fondos no será ella quien sufra
una condena— interrumpió Andréïda al periodista.

Y a su vez terció Pita:

—Tiene razón, Andréïda; después de todo se
trata de una compañera de colegio —y con manifiesta insidia, añadió —Aunque… también hay
que recordar que nos ha deshonrado indirectamente… ¡Caray, una muchacha educada en el St.
Mary!

Andréïda miró duramente a Pita, de cabeza a
pies.

—Del St. Mary ha salido también una Pita
García de León… muy distinguida… muy …

—¡Andréïda! —reprochó Nelly y tomando con
apresuramiento a Pita del brazo y en unión de Isabel la rogaron que las condujese a su alcoba para
arreglar un poco su tocado, Pita accedió y dirigiéndose a Torritos lo instó para que acompañase a
Andréïda a tomar algo al comedor.

Andréïda rechazó la invitación con firmeza y
rogó que la dejasen un rato a solas, diciendo que
no se sentía muy bien. Torritos observó la palidez
de la joven y se dijo, para su interior, que la Andréïda tenía corazón.

Minutos más tarde todos dejaron a Andréïda
sola, como lo había pedido, y Pita, desde la puerta, ofreció el enviarle una copita del algún licor
fuerte para que se reanimara.

Un buen rato quedó Andréïda sola en la habitación; sentía un intenso malestar físico y una especie
de ahogo en la garganta. Hacía días que experimentaba grandes deseos de llorar y ahora le parecía, un
buen pretexto las desgracias íntimas de María Luisa y de Isabel. Sin embargo, no se abandonó a tal
debilidad, e hizo toda clase de esfuerzos para reprimir su extraño estado de ánimo y tornar a ser
dueña de su tranquilidad. Consideraba ahora, que
le era tanto más necesaria cuanto había percibido
con claridad, toda la noche, una reprimida hostilidad entre sus conocidos y una sonrisa desconcertante y triunfal en muchas bocas que, por diversas
razones, las consideraba ella envidiosas y enemigas
de su popularidad.

En tan molestas reflexiones estaba embebida
cuando un leve ruido que hizo la puerta del fondo
al abrirse, la obligó a volverse.

Un momento pareció huir la vida del rostro demacrado de Andréïda.

—¡Raúl!— modularon los labios exangües.

Era, en efecto, el doctor Raúl quien permanecía en el dintel de la puerta con una copa en la
mano. Después, con fría cortesía, dijo: 

—Ah, era para usted… estaba usted aquí…

—¿Le pesa haberla traído?— interrogó Andréïda señalando la copa y haciendo esfuerzos
inauditos por aparecer tranquila.

—No—, musitó Raúl acercándose —Me sorprendí al principio; la señorita Garcta de León
no me dijo para quien era… Me extrañó que
no se sirviera de un criado… La galantería me
obligó a obedecerla…

—Lo han hecho víctima de una mala partida,
amigo mío…

Repentinamente Andréïda volvió a sentirse mal,
a sus pulmones parecía faltar el aire y sobre sus
ojos descendió un velo obscuro. En vano deseó
ocultar su malestar físico, se llevó las manos al pecho y sus ojos rodaron por la vacía estancia, implorando vagamente ayuda.

Raúl se inclinó hacia ella. En su bello rostro
varonil se dibujaba una secreta angustia.

—¿Está usted enferma, Andréïda?— interrogó
en voz baja, con solicitud alarmada.

Andréïda sonrió, le aseguró que no era nada,
que se trataba de un malestar insignificante. De
tiempo acá, afirmó, no podía soportar ningún perfume, ni el más leve humo de tabaco, en el acto le
provocaban náuseas.

El doctor dejó a un lado la copa de licor y
ya con solicitud profesiona] intentó tomarle el pulso. Andréïda se rehusó con firmeza y entonces él
dijo:

—¿No tiene usted confianza en Raúl, médico?

Andréïda hizo un movimiento de cabeza evasivo. Su boca se plegó en un gesto inconfundible de
crueldad femenina.

—Quizá —expresó con malignidad— se me ha
dicho que vuestro sol médico no brilla con igual
esplendor… que marcha hacia el ocaso…

Raúl bajó aun más la cabeza y le tomó el pulso
en silencio. Después, con voz extrañamente sorda,
dijo:

—Es usted cruel, diabólicamente cruel… cuando quiere serlo— y cambiando de tono la interrogó
sobre lo que sentía.

—Le he dicho que no le tengo confianza— repitió con dulzura la joven y se hizo una pausa larga
y pesada entre ellos.

—¡Raúl! —exclamó, por fin, Andréïda. El doctor levantó los ojos, había en ellos una desolación
que emocionaba.

—Ha vuelto usted a las andadas…— añadió la
joven.

—A las andadas, no; —replicó con violencia
Raúl—. Ninguna mujer había tenido nunca poder
para hacerme caer tan bajo. —Y con un gran esfuerzo. de voluntad, añadió débilmente: —¿Sabe
usted que se me trata de expulsar del sanatorio, y…
aún peor… de la academia de Medicina…?

—Lo sé… —fué la lacónica respuesta de Andréïda.

—Soy un guiñapo humano, un desecho de la
vida que pronto arrojará cualquiera a puntapiés —moduló incoloramente la voz del doctor Raúl—.
Vibraba en ellas una clase de pesimista indiferencia que aterró a Andréïda.

—¡Bebe usted mucho!— se atrevió ella a decir.

—¡Y qué… qué le importa a nadie…! ¡qué
le importa a usted!

La súbita violencia de Raúl tenía algo de extravío.

—Nada, tiene usted razón— fué la inusitada réplica humilde de la mujer, que, en seguida, añadió
sordamente: —Me duele porque siempre tuve a usted en otro concepto… Me parecía el único hombre que, de haber sido mujer… hubiese amado…

Raúl la miró detenidamente, curiosamente, en
sus ojos no brillaba el menor rayo de esperanza.

Y escuchó, en silencio, los esfuerzos que ella hizo
para convencerlo de la necesidad de reaccionar.

—No, Andréïda, todo ha acabado para mí…
¡Todo acabó para mí aquella noche! —En las palabras de Raúl había una resignación fatalista que
exasperó a Andréïda.

—¡Lo que hace de un hombre… el amor…!
—comentó despectiva, con la antigua animosidad
en el tono pausado de su voz.

—No, el amor, no;—y la voz del hombre recobró su firmeza y su imponente violencia—. Tú, Andréïda, tú, ¡mujer fatal…! Tu amor me hubiese
dado alas de gigante, hubiera hecho de mí algo
grande… Me sentía con ímpetus suficientes para
dominar el mundo… El amor, tal como tú lo entiendes, lo encarna Marta; ella, la que coloca en mi
mano la copa enloquecedora del ajenjo… la que
enciende en mi cerebro la llama concupiscente…
la que hace de mí un ser vencido… ella, ella…
Y tú, Andréïda, me has arrojado en sus brazos, sin
compasión… —Un sollozo se quebró en la garganta del doctor Raúl.

—¿Yo? Yo no… —se defendió Andréïda—.
¿Para qué le sirve a usted su libre albedrío…?

—¡Para nada…! Ignoras que una mujer puede hacer de un hombre un genio o una bestia…

—¿Tanto es el poder de una mujer…?

—¡Tanto…! Ignoras que cuando una mujer superior enciende el amor en el pecho de un hombre,
éste no puede hacer cosa que no sea consumirsu
en el fuego lento de su pasión…

—¿Eso he hecho yo…?

—Eso… Andréïda, ignoras muchas cosas—.
La voz de Raúl volvió a temblar extrañamente, había algo desnudo en su acento, y que pedía el abrigo de la compasión. —El poder del hombre y de la
mujer —siguió diciendo— se detiene en los límites
en que comienza su gran amor… Allí… dejo yo
de ser el hombre de acero, el doctor Raúl… y allí
también, dejas de ser tú, Andréïda… para convertirnos en algo mejor: un hombre y una mujer
con atributos de creación… ¡No podrás sustraerte
a la ley, no podrás —afirmó agorero—. Para dolor
mío… es posible que otro ser te rescate… y, entonces, yo seré la nada… la nada… ¿Te das cuenta del significado intensamente negativo de la
palabra…?

Esta vez Andréïda no pudo ocultar la impresión que le hacían aquellas dolorosas palabras.

—Deja eso, Raúl —suplicó—. Nos hace daño,
nos hacemos daño…

Otra vez sintió el extraño ahogo en la garganta, la angustia interna de sentir que no llega suficiente aire a los pulmones.

—Raúl, me siento mal… —Se acogía a él, por
primera vez, con una clase de dependencia indefensa. Y para desvirtuar su inmensa necesidad de
ser protegida, añadió con una sonrisa que hacía
más triste la desvastación de su bello rostro: —El
cuerpo me hace sentir su imperio, su perversión
—Y sintiéndose de nuevo mal, suplicó al amigo que
llamara a Nelly, porque quería regresar a casa.

Raúl sufrió un movimiento de indecisión, la
angustia del hombre se imponía a la frialdad del
médico.

—No puedo salir ahora, no puedo dejarte sola…

Con autoridad la obligó a beber la copita de cognac que se había visto obligado a traer. En eso
estaba cuando penetraron Nelly, Pita, Isabel y Torritos que deseaban informarse sobre el estado que
guardaba Andréïda.

La mayoría de los invitados se había despedido
y ahora el jardín presentaba un aspecto brillante y
vacío de lugar abandonado.

—¿Qué tienes, querida? ¡Qué pálida estás!—
Isabel fué la primera en hablar.

—No es nada, Marisabel. No es nada. —Andréïda hablaba con dificultad—. Tú eres la que debías marcharte… Tu hijito te necesita …Ningún
hombre del mundo sería capaz de arrancarme de la
cabecera de mi hijo enfermo. Vete. Pita, dile a ese
hombre que el lugar de Isabel está en la recámara
de su hijo…

—No te exaltes… Te hace daño— intervino
Nelly.

Pita examinaba con maligna curiosidad a Andréïda. Y sus ojos vagaban de ella a Raúl, como
queriendo profundizar el drama de aquellos dos
seres magníficos. Con voz melosa, dijo:

—Sí, querida; ahora mismo voy… Domina tus
nervios… —Y diciendo y haciendo cogió de la cintura a Isabel, la que, desprendiéndose, fué a abrazar a Andréïda, recordando, en su movimiento, una
costumbre habitual del colegio.

—Mi Andréïda, mi pobrecita Andréïda, me espanta tu semblante desencajado… Dios querrá que
no sea nada… Te dejo —y con resolución forzada,
agregó: —Pero mañana iré a verte…

—Gracias, mil gracias, Marisabel… Ve con tu
chiquitín y que se alivie él también… ¡Tengo unos
deseos locos de abrazar a tus chiquillos…! ¡Me parecerán que son algo mío… Estrecharé sus cabecitas en mi regazo con tanto amor…

Esta vez tocó a Torritos el lanzar una inquisitiva mirada a Andréïda.

Una vez que lIsabel y Pita se retiraron, el doctor Raúl pidió a Torritos y a Nelly lo dejasen, a
solas, un instante, con Andréïda para preguntarle,
profesionalmente, por su estado y poder diagnosticar. Nelly y Torritos accedieron a la súplica del
doctor y pasaron a la estancia inmediata, en espera
de su llamado.

—Andréïda, usted necesita aire fresco— dijo en
cuanto quedaron a solas, y fué a abrir la puerta
de cristales. Del salón del primer piso penetraron,
levemente, las notas inolvidables para Raúl, del
_Penseé Fugitive_, de Kiarganoff, y quedó un momento pensativo escuchándolas. Andréïda parecía
recordar también, pues lo llamó a su lado.

—Raúl, va a prometerme algo —dijo—. Va a
jurarme algo que hará descansar mi conciencia de
un gran peso… Va a hacer lo que yo le diga…
Sí, va usted a volver a ser el mismo, el hombre
superior que yo admiré…

Raúl se acercó a ella, había cierta dureza en la
expresión de su semblante cuando replicó:

—¡No es posible! Lo hecho, hecho está… Además… me es grato dejarme arrastrar por la corriente…

Andréïda, que se encontraba ligeramente recostada en el diván, se irguió y con su antiguo tono
imperioso, ordenó:

—Va a hacer lo que yo le diga; no quiero tener que avergonzarme delante de mí misma… Jamás
he hecho nada reprensible… y con usted, con usted… hice algo indebido… algo que yo creí pagar, suficientemente, con el don, por un momento,
de mí misma… Confieso ahora que me equivoqué…

Y tocó a Raúl el rehusarse a hablar de aquéllo,
pasando, con brusquedad a interrogarla, profesionalmente, sobre lo que sentía.

—Fiebre no tienes… —afirmó—. ¿Desde cuándo te encuentras así…

—No sé… ya son muchos los días que sufro… —A toda costa Andréïda quería rehuir el interrogatorio médico. Temía levantar el telón oscuro con el cual se había ocultado, a sí misma, la realidad de su estado, en un torpe empeño de ganar
tiempo y que éste se encargara de revelarle la desnuda y terrible realidad.

—¿Desde cuándo no…?

Raúl se inclinó sobre Andréïda y, tomándole
las manos con pasión, casi junto a los labios de la
amada, pronunció las palabras sublimes:

—Andréïda… Tú vas a ser madre…

Andréïda se incorporó con violencia. Aquello
que había temido todo este tiempo, en el subconsciente, lo oía plasmado crudamente en palabras.

—¡Imposible…! —exclamó—. No… eso no…

Raúl la obligó con suavidad, pero con firmeza
a recostarse. —Sí, eso sí… —replicó.

Luego, sentándose a su lado, le habló como a
una niña inocente a quien hay que explicarle el
misterio de la vida con toda delicadeza. Era otro
hombre. El descendimiento vicioso de las comisuras de sus labios, impreso por la frecuente embriaguez en la que se había sumergido en las últimas
semaras, había desaparecido del todo. Sus bellos
ojos viriles tenían toda la serenidad de un hombre
dispuesto, de nuevo, a conquistar su dicha a cambio de la misma vida. Sus manos habían dejado
de temblar. Su voz estaba impregnada de una honda dulzura. Todo su aspecto era el sublime aspecto
del macho dispuesto a morir en defensa de su hembra y su progenie. —Desde ahora —comenzó diciendo— no vas a ser tú, Andréïda, la que mande en tu
organismo… ¡Ya te decía yo que, ante el amor,
se es hombre o se es mujer… Nada más… Desde
hoy, no serás dueña de ti misma… Tu hermoso
cuerpo se irá deformando… ¡Vana será toda tu
ciencia…! Tu semblante, no será tu semblante;
tus deseos no serán tus deseos… Un pequeñísimo
dictador te obligará a comer lo que él necesite; a
dormir cuando él lo requiera; se apoderará de tu
ser, en tal forma, que sentirás una inmensa tristeza sin causa alguna, y, a veces, también reirás…
porque algo divino te obliga a reír, sin razón…
Así… sin razón…

El rostro del hombre estaba cerca del de la mujer. Irradiaba una sublime alegría dolorosa. Con
voz apagada, repitió:

—¡Andréïda, vas a ser madre…! ¿Te das
cuenta…? vas a ser madre… ¡Llevas, a pesar tuyo, mi amor dentro de ti!

—¡Raúl… piedad! —imploró Andréïda.

Un súbito espanto descompuso las facciones del
hombre.

—Andréïda, no serás tan depravadamente moderna que trates de eliminar… de cometer un
crimen… No, eso no puedes hacerlo tú… Tú has
dicho antes que ningún hombre sería lo suficientemente fuerte para arrebatarte de la cabecera de
tu hijo… Tú has dicho eso… Tú has deseado
estrechar unas cabecitas ajenas en tu seno de mujer… Tu subconsciencia te dictaba ya tu deber…
Andréïda, tú no harás eso —suplicó el hombre—.
Eso te condenaría irremisiblemente… ¡No habría
ya salvación posible para ti…! ¡Divina Andréïda!
¡Amor de mis amores…!

Andréïda parecía no escuchar el largo exordio
de Raúl. Todo su ser estaba empeñado en retrotraerse como en contemplación mística de un milagro interior.

—¡Madre… yo madre!— musitó como si su
cerebro no alcanzase todavía a medir la magnitud
del fenómeno.

—Sí, Andréïda— asintió con dulzura Raúl. Hablaba en voz baja, como en la alcoba de un enfermo
grave o también como en un templo. —Madre de mi hijo… ¿Te das cuenta del milagro? ¿Te das cuenta de la maravilla con la que Dios nos ha honrado?

—¡Mi hijo, un hijo mío… un hijo tuyo!

— Un hijo mío… sí… Te adoro doblemente…
No me atrevo a tocarte…

Raúl la acarició con suma delicadeza, luego comentó para sí mismo:

—¡Eres tan frágil… ahora tengo que protegerte… ahora debo protegerte… ahora me dejarás
que te proteja…!

Andréïda, que había escuchado, musitó:

—Sí, Raúl, sí…

El hombre tembló de gozo, no podía creer lo
que escuchaba, le parecía imposible ventura tanta.

—¿Has dicho que sí…? —interrogó con un temor incrédulo en la voz temblorosa, y una gran risa
gozosa y trémula agitó su garganta sin atreverse a
brotar—. Por fin has dicho que sí… Mi esposa,
mi bienamada… ¿Te casarás conmigo, inmediatamente, es verdad? Dios mío, gracias, gracias.
Ahora tendré ya orientación en mi vida; sabré para qué he nacido…! Un hijo mío, el hijo de mi divina Andréïda!

Andréïda pareció salir de su abstraimiento.
Participaba de la alegría de Raúl. Gozaba, por primera vez, sanamente de su emoción de hombre.
Luego, afirmó:

—Sí, Raúl, sí… Mi grito a Marisabel no fué
más que el resplandor de esa llama dormida que
llevaba en la entraña… La llama de la maternidad… Tienes razón, Raúl…, ¡Me has vencido!

—Vencido, no, Andréïda mía… Te he recuperado a la Vida, te he salvado a mi amor… ¡Serás
algo más grande, mucho más grande de lo que en
tus sueños locos ambicionabas! ¡Serás madre, Andréïda mía!

Un rato quedó la joven soñadoramente pensativa y en silencio musitó aquellos versos del insigne
Martínez Sierra:

>…toda mujer, porque Dios lo ha querido
dentro del corazón lleva un hijo dormido.

Raúl sonrió; femenina es toda mujer que ama a
Martínez Sierra, el delicado cantor de los suaves
placeres de la casa.

—Eres tan frágil, Andréïda, que alfombraré
de rosas tu camino… ¡Te querré tanto…, harás
de mí un hombre nuevo!

Andréïda sonreía, sonreía siempre.

—Haré de ti lo que hubiese querido ser yo…
Y si me olvidas… si más tarde… por ley natural, te desvías de mi camino… entonces… tendré
a mi hijo… ¡Gloria mayor no existe! ¡Recompensa mejor, tampoco!

Y el hombre, porque no podía dejar de serlo,
aseguró lo que todos aseguran apasionadamente en
los albores de un amor.

—Te amaré siempre, Andréïda, siempre… Haré desaparecer de tu bella mirada esa desconfianza
que ponía en tus ojos tan helada expresión. Infundiré a tu carne el calor sacrosanto que los azares
de Ja vida te arrebataron; haré de ti el receptáculo
sublime de mi amor… Andréïda, los poderosos de
la tierra ambicionarán nuestra dicha… ¡No habrá
poder humano que empañe nuestra felicidad…

Andréïda tornó a sonreír, era demasiado humana para creer en la eternidad de los afectos.

—¡Un hijo mío! —musitó dando vueltas a la idea, eje del mundo.

—Un hijo nuestro —corrigió Raúl y añadió puerilmente—. Pero… ya siento celos, qué, ¿no hay
nada para mí?

—Oh, sí, eres el padre de él, de mi hijo…

—No importa— aceptó con humildad Raúl —recibiré de rodillas las migajas de tu afecto. Soy infinitamente feliz solo con que me permitas estar
siempre a tu lado… adorándote… venerándote…

Por el rostro de Andréïda resbalaron dos lágrimas como dos milagrosos diamantes, Raúl, que
lo vió, se arrodilló a sus pies. —¿Lloras? Eres humana… ¡Gracias, Dios mío, por fin es mía… La
recuperé a la Vida… la rescaté a mi amor…

En medio de su éxtasis, Andréïda sonreía, sonreía siempre, aún entre sus lágrimas.

—¡Yo madre, madre!—repetía y no podía
creerlo.

—El título mejor… lo único divino sobre la
miseria y podredumbre de la tierra.

—Mi Raúl, mi amado, ¡existe el paraíso sobre
la tierra…! ¡No es una tradición mítica!

—Existe, Andréïda, existe…

—No todo es miseria, no todo es depravación…

—No todo, amor mío, no todo…

—En el derrumbamiento caótico de nuestra era
queda algo firme… queda algo eterno…

—¡Tu divina maternidad, Andréïda!

—¡Quise ser algo más que mujer y tenía razón…!

—Tenías razón; eres más ahora: eres madre…

—Por fin siento dentro de mi ser un orgullo
legítimo…

—¡El de tener un hijo…!

—¡Qué cosa tan maravillosa, Raúl… Pero tengo miedo… Dudo de mí misma… ¿Tendré el valor necesario…? ¿Triunfaré de la dura prueba…?

—Triunfarás… —aseguró orgullosamente el
hombre—. ¡Yo estaré a tu lado!

—Gracias… ¡Te necesito ahora tanto… Necesito infinitamente tu amor…!

—Por fin, Andréïda… ¡qué feliz me haces, qué feliz soy!… No me importa morir después de
haberte oído…

—Raúl mío… No hables de muerte en el instante preciso en que sabemos que hemos dado al
mundo una nueva vida…

—¡Tienes razón… siempre tienes razón…!

Pensativa, Andréïda replicó:

—No siempre, no… ¡Un hijo mío…! ¡Maravilla, oh maravilla!

</section>
<section epub:type="chapter" role="doc-chapter">

# EPILOGO

El epílogo de nuestra historia —toda historia
debe tener, por imperativo clásico, un epílogo— no
puede ser otro que aquel ingenuo y pueril que en
otros tiempos solía ponerse a las historias felices,
cantadas bizarramente por bellos trovadores imberbes de áureas cabelleras, suavemente ondulosas y
de ojos soñadores y azules. (No recuerdo, con precisión, si sus vestiduras eran todo lo pulcras que
cumplían a lindos trovadores o si, por el contrario,
ofendían el olfato a semejanza ingrata con algunos
de sus insignes descendientes, de cuello ornado con
la emblemática y monumental cuanto pringosa mariposa negra, y amantes infieles de Mimí Pinsón).

“Fueron felices y tuvieron muchos hijos”, era
el hermoso y nunca bien alabado epílogo reglamentario.

"Fueron felices y tuvieron muchos hijos”, para
no desentonar de la tradición diremos también aquí
nosotros.

Mas, a fuer de sinceros y veraces, gustosamente hacemos constar que en la feliz solución a la
magnífica vida de la sin par Andréïda, influyó, no
poco, la cobardía de la mano femenina que la plasmara en letras. Mano cobarde que, espantada de
su audacia, se apresuró a alargarle la rehabilitación, la suprema redención a su heroína, dentro de
una forma suave y rosada, destinada a estremecer
al mundo con un débil vagido de infante.

La protagonista de marras se vió forzada a tomarla. No le quedaba otro camino, so pena de quedarse en el tenebroso, cuanto metafísico depósito
transparente, de una no muy eficiente Parker Vacumatic.

Pero, ¿quién nos dice que en el futuro, en el
pavoroso futuro, esta docilidad de hoy de Andréïda,
ésta su concesión a la farsa, no se ahogue en el lago
letal y sin fondo al que la arroja fatalmente la rebeldía a la injusticia y de la cual nació…?

Amado lector, caído del cielo —alguno deberá
tener Andréïda— óyelo bien: el monstruo más terrible, el supermonstruo de nuestra diabólica era,
es Andréïda… Suele envolverse en la seda preciosa del artificio, tiene pretensiones de perfección…

</section>
