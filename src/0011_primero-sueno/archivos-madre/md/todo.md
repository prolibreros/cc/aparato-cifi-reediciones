<section epub:type="chapter" role="doc-chapter">

# Primero sueño @ignore

Piramidal, funesta, de la tierra {.sangria}

nacida sombra, al cielo encaminaba {.sin-sangria}

de vanos obeliscos punta altiva, {.sin-sangria}

escalar pretendiendo las estrellas, {.sin-sangria}

si bien sus luces bellas,

exentas siempre, siempre rutilantes, {.sin-sangria}

la tenebrosa guerra

que con negros vapores le intimaba {.sin-sangria}

la vaporosa sombra fugitiva {.sin-sangria}

burlaban tan distantes,

que su atezado ceño

al superior convexo aún no llegaba {.sin-sangria}

del orbe de la diosa

que tres veces hermosa

con tres hermosos rostros ser ostenta, {.sin-sangria}

quedando solo dueño

del aire que empañaba

con el aliento denso que exhalaba; {.sin-sangria}

y en la quietud contenta

de impero silencioso,

sumisas solo voces consentía {.sin-sangria}

de las nocturnas aves

tan obscuras, tan graves,

que aun el silencio no se interrumpía. {.sin-sangria}

Con tardo vuelo y canto, del oído {.sin-sangria}

mal, y aun peor del ánimo admitido, {.sin-sangria}

la avergonzada Nictimene acecha {.sin-sangria}

de las sagradas puertas los resquicios, {.sin-sangria}

o de las claraboyas eminentes {.sin-sangria}

los huecos más propicios

que capaz a su intento le abren brecha, {.sin-sangria}

y sacrílega llega a los lucientes {.sin-sangria}

faroles sacros de perenne llama {.sin-sangria}

que extingue, sino infama,

en licor claro, la materia crasa {.sin-sangria}

consumiendo, que el árbol de Minerva {.sin-sangria}

de su fruto, de prensas agravado, {.sin-sangria}

congojoso sudó y rindió forzado. {.sin-sangria}

Y aquellas que su casa

campo vieron volver, sus telas hierba, {.sin-sangria}

a la deidad de Baco inobedientes {.sin-sangria}

---ya no historias contando diferentes, {.sin-sangria}

en forma si afrentosa transformadas---, {.sin-sangria}

segunda forman niebla,

ser vistas, aun temiendo en la tiniebla, {.sin-sangria}

aves sin pluma aladas:

aquellas tres oficiosas, digo, {.sin-sangria}

atrevidas hermanas,

que el tremendo castigo

de desnudas les dio pardas membranas, {.sin-sangria}

alas tan mal dispuestas,

que escarnio son aun de las más funestas: {.sin-sangria}

estos, con el parlero

ministro de Plutón un tiempo, ahora {.sin-sangria}

supersticioso indicio al agorero, {.sin-sangria}

solos la no canora

componían capilla pavorosa, {.sin-sangria}

máximas, negras, longas entonando, {.sin-sangria}

y pausas más que voces, esperando {.sin-sangria}

a la torpe mensura perezosa {.sin-sangria}

de mayor proporción tal vez, que el viento {.sin-sangria}

con flemático echaba movimiento, {.sin-sangria}

de tan tardo compás, tan detenido, {.sin-sangria}

que en medio se quedó tal vez dormido. {.sin-sangria}

Este. pues, triste son intercadente {.sin-sangria}

de la asombrosa turba temerosa, {.sin-sangria}

menos a la atención solicitaba {.sin-sangria}

que al suelo persuadía;

antes sí, lentamente,

su obtusa consonancia espaciosa {.sin-sangria}

al sosiego inducía

y al reposo los miembros convidaba, {.sin-sangria}

el silencio intimando a los vivientes, {.sin-sangria}

(uno y otro sellando labio oscuro {.sin-sangria}

con indicante dedo),

Harpócrates, la noche, silencioso; {.sin-sangria}

a cuyo, aunque no duro,

si bien imperioso

precepto, todos fueron obedientes. {.sin-sangria}

El viento sosegado, el can dormido, {.sin-sangria}

este yace, aquel quedo

los átomos no mueve,

con el susurro hacer temiendo leve, {.sin-sangria}

aunque poco, sacrílego ruido, {.sin-sangria}

violador del silencio sosegado. {.sin-sangria}

El mar, no ya alterado,

ni aún la instable mecía

cerúlea cuna donde el sol dormía; {.sin-sangria}

y los dormidos, siempre mudos, peces, {.sin-sangria}

en los lechos lamosos

de sus oscuros senos cavernosos, {.sin-sangria}

mudos eran dos veces;

y entre ellos, la engañosa encantadora {.sin-sangria}

Almone, a los que antes

en peces transformó, simples amantes, {.sin-sangria}

transformada también, vengaba ahora. {.sin-sangria}

En los del monte senos escondidos, {.sin-sangria}

cóncavos de peñascos mal formados, {.sin-sangria}

de su esperanza menos defendidos {.sin-sangria}

que de su oscuridad asegurados, {.sin-sangria}

cuya mansión sombría

ser puede noche en la mitad del día, {.sin-sangria}

incógnita aún al cierto

montaraz pie del cazador experto, {.sin-sangria}

depuesta la fiereza

de unos, y de otros el temor depuesto, {.sin-sangria}

yacía el vulgo bruto,

a la naturaleza

el de su potestad pagando impuesto, {.sin-sangria}

universal tributo;

y el rey, que vigilancias afectaba, {.sin-sangria}

aun con abiertos ojos no velaba. {.sin-sangria}

El de sus mismos perros acosado, {.sin-sangria}

monarca en otro tiempo esclarecido, {.sin-sangria}

tímido ya venado,

con vigilante oído,

del sosegado ambiente

al menor perceptible movimiento {.sin-sangria}

que los átomos muda,

la oreja alterna aguda

y el leve rumor siente

que aun le altera dormido.

Y en la quietud del nido,

que de brozas y lodo instable hamaca {.sin-sangria}

formó en la más opaca

parte del árbol, duerme recogida {.sin-sangria}

la leve turba, descansando el viento {.sin-sangria}

del que le corta, alado movimiento. {.sin-sangria}

De Júpiter el ave generosa, {.sin-sangria}

como al fin reina, por no darse entera {.sin-sangria}

al descanso, que vicio considera {.sin-sangria}

si de preciso pasa, cuidadosa {.sin-sangria}

de no incurrir de omisa en el exceso, {.sin-sangria}

a un solo pie librada fía el peso, {.sin-sangria}

y en otro guarda el cálculo pequeño {.sin-sangria}

---despertador reloj del leve sueño---, {.sin-sangria}

por que, si necesario fue admitido, {.sin-sangria}

no pueda dilatarse continuado, {.sin-sangria}

antes interrumpido

del regio sea pastoral cuidado. {.sin-sangria}

¡Oh de la majestad pensión gravosa, {.sin-sangria}

que aun el menor descuido no perdona! {.sin-sangria}

Causa, quizá, que ha hecho misteriosa, {.sin-sangria}

circular, denotando, la corona, {.sin-sangria}

en círculo dorado,

que el afán es no menos continuado. {.sin-sangria}

El sueño todo, en fin, lo poseía; {.sin-sangria}

todo, en fin, el silencio lo ocupaba; {.sin-sangria}

aun el ladrón dormía;

aun el amante no se desvelaba. {.sin-sangria}

El conticinio casi ya pasando {.sin-sangria}

iba, y la sombra dimidiaba, cuando {.sin-sangria}

de las diurnas tareas fatigados {.sin-sangria}

---y no solo oprimidos

del afán ponderoso

del corporal trabajo, mas cansados {.sin-sangria}

del deleite también (que también cansa {.sin-sangria}

objeto continuado a los sentidos, {.sin-sangria}

aún siendo deleitoso:

que la naturaleza siempre alterna {.sin-sangria}

ya una, ya otra balanza,

distribuyendo varios ejercicios, {.sin-sangria}

ya al ocio, ya al trabajo destinados, {.sin-sangria}

en el fiel infiel con que gobierna {.sin-sangria}

la aparatosa máquina del mundo)---; {.sin-sangria}

así pues, de profundo

sueño dulce los miembros ocupados, {.sin-sangria}

quedaron los sentidos

del que ejercicio tienen ordinario {.sin-sangria}

(trabajo en fin, pero trabajo amado, {.sin-sangria}

si hay amable trabajo),

si privados no, al menos suspendidos, {.sin-sangria}

y cediendo al retrato del contrario {.sin-sangria}

de la vida, que, lentamente armado, {.sin-sangria}

cobarde embiste y vence perezoso {.sin-sangria}

con armas soñolientas,

desde el cayado humilde al cetro altivo {.sin-sangria}

sin que haya distintivo

que el sayal de la púrpura discierna, {.sin-sangria}

pues su nivel, en todo poderoso, {.sin-sangria}

gradúa por exentas

a ningunas personas,

desde la de a quien tres forman coronas {.sin-sangria}

soberana tiara

hasta la que pajiza vive choza; {.sin-sangria}

desde la que el Danubio undoso dora, {.sin-sangria}

a la que junco humilde, humilde mora; {.sin-sangria}

y con siempre igual vara

(como, en efecto, imagen poderosa {.sin-sangria}

de la muerte) Morfeo

el sayal mide igual con el brocado. {.sin-sangria}

El alma, pues, suspensa

del exterior gobierno ---en que, ocupada {.sin-sangria}

en material empleo,

o bien o mal da el día por gastado---, {.sin-sangria}

solamente dispensa

remota, si del todo separada {.sin-sangria}

no, a los de muerte temporal opresos, {.sin-sangria}

lánguidos miembros, sosegados huesos, {.sin-sangria}

los gajes del calor vegetativo, {.sin-sangria}

el cuerpo siendo, en sosegada calma, {.sin-sangria}

un cadáver con alma,

muerto a la vida y a la muerte vivo, {.sin-sangria}

de lo segundo dando tardas señas {.sin-sangria}

el de reloj humano

vital volante que, sino con mano, {.sin-sangria}

con arterial concierto, unas pequeñas {.sin-sangria}

muestras, pulsando, manifiesta lento {.sin-sangria}

de su bien regulado movimiento. {.sin-sangria}

Este, pues, miembro rey y centro vivo {.sin-sangria}

de espíritus vitales,

con su asociado respirante fuelle {.sin-sangria}

---pulmón, que imán del viento es atractivo, {.sin-sangria}

que en movimientos nunca desiguales, {.sin-sangria}

o comprimiendo ya, o ya dilatando {.sin-sangria}

el musculoso, claro, arcaduz blando, {.sin-sangria}

hace que en él resuelle

el que le circunscribe fresco ambiente {.sin-sangria}

que impele ya caliente,

y él venga su expulsión haciendo, activo, {.sin-sangria}

pequeños robos al calor nativo, {.sin-sangria}

algún tiempo llorados,

nunca recuperados,

si ahora no sentidos de su dueño, {.sin-sangria}

(que, repetido, no hay robo pequeño)---; {.sin-sangria}

estos, pues, de mayor, como ya digo, {.sin-sangria}

excepción, uno y otro fiel testigo, {.sin-sangria}

la vida aseguraban,

mientras con mudas voces impugnaban {.sin-sangria}

la información, callados, los sentidos, {.sin-sangria}

con no replicar solo defendidos; {.sin-sangria}

y la lengua que, torpe, enmudecía, {.sin-sangria}

con no poder hablar los desmentía. {.sin-sangria}

Y aquella del calor más competente {.sin-sangria}

científica oficina,

próvida de los miembros despensera, {.sin-sangria}

que avara nunca y siempre diligente, {.sin-sangria}

ni a la parte prefiere más vecina {.sin-sangria}

ni olvida a la remota,

y en ajustado natural cuadrante, {.sin-sangria}

las cuantidades nota

que a cada cual tocarle considera, {.sin-sangria}

del que alambicó quilo el incesante {.sin-sangria}

calor, en el manjar que, medianero {.sin-sangria}

piadoso, entre él y el húmedo interpuso {.sin-sangria}

su inocente sustancia,

pagando por entero

la que, ya piedad sea, o ya arrogancia, {.sin-sangria}

al contrario voraz, necio, la expuso {.sin-sangria}

(merecido castigo, aunque se excuse, {.sin-sangria}

al que en pendencia ajena se introduce); {.sin-sangria}

esta, pues, si no fragua de Vulcano, {.sin-sangria}

templada hoguera del calor humano, {.sin-sangria}

al cerebro enviaba

húmedos, mas tan claros, los vapores {.sin-sangria}

de los atemperados cuatro humores, {.sin-sangria}

que con ellos no solo empañaba {.sin-sangria}

los simulacros que la estimativa {.sin-sangria}

dio a la imaginativa

y aquesta, por custodia más segura, {.sin-sangria}

en forma ya más pura

entregó a la memoria (que, oficiosa, {.sin-sangria}

grabó tenaz y guarda cuidadosa), {.sin-sangria}

sino que daban a la fantasía {.sin-sangria}

lugar de que formase

imágenes diversas. {.sin-sangria}

Y del modo {.sin-sangria}

que en tersa superficie, que de Faro {.sin-sangria}

cristalino portento, asilo raro {.sin-sangria}

fue, en distancia longísima se vían, {.sin-sangria}

sin que esta le estorbase,

del reino casi de Neptuno todo, {.sin-sangria}

las que distantes le surcaban naves, {.sin-sangria}

viéndose claramente,

en su azogada luna

el número, el tamaño y la fortuna {.sin-sangria}

que en la instable campaña transparente {.sin-sangria}

arresgadas tenían,

mientras aguas y vientos dividían {.sin-sangria}

sus velas leves y sus quillas graves: {.sin-sangria}

así ella, sosegada, iba copiando {.sin-sangria}

las imágenes todas de las cosas, {.sin-sangria}

y el pincel invisible iba formando {.sin-sangria}

de mentales, sin luz, siempre vistosas {.sin-sangria}

colores, las figuras

no solo ya de todas las criaturas {.sin-sangria}

sublunares, mas aun también de aquellas {.sin-sangria}

que intelectuales claras son estrellas, {.sin-sangria}

y en el modo posible

que concebirse puede lo invisible, {.sin-sangria}

en sí, mañosa, las representaba {.sin-sangria}

y al alma las mostraba.

La cual, en tanto, toda convertida {.sin-sangria}

a su inmaterial ser y esencia bella, {.sin-sangria}

aquella contemplaba,

participada de alto Ser, centella {.sin-sangria}

que con similitud en sí gozaba; {.sin-sangria}

y juzgándose casi dividida {.sin-sangria}

de aquella que impedida

siempre la tiene, corporal cadena, {.sin-sangria}

que grosera embaraza y torpe impide {.sin-sangria}

el vuelo intelectual con que ya mide {.sin-sangria}

la cuantidad inmensa de la esfera, {.sin-sangria}

ya el curso considera

regular, con que giran desiguales {.sin-sangria}

los cuerpos celestiales

---culpa, si grave, merecida pena {.sin-sangria}

(torcedor del sosiego, riguroso) {.sin-sangria}

de estudio vanamente judicioso---, {.sin-sangria}

puesta, a su parecer, en la eminente {.sin-sangria}

cumbre de un monte a quien el mismo Atlante, {.sin-sangria}

que preside gigante

a los demás, enano obedecía, {.sin-sangria}

y Olimpo, cuya sosegada frente, {.sin-sangria}

nunca de aura agitada

consintió ser violada,

aun falda suya ser no merecía, {.sin-sangria}

pues las nubes (que opaca son corona {.sin-sangria}

de la más elevada corpulencia, {.sin-sangria}

del volcán más soberbio que en la tierra {.sin-sangria}

gigante erguido intima al cielo guerra), {.sin-sangria}

apenas densa zona

de su altiva eminencia,

o a su vasta cintura

cíngulo tosco son que, mal ceñido, {.sin-sangria}

o el viento lo desata sacudido, {.sin-sangria}

o vecino el calor del sol, lo apura. {.sin-sangria}

A la región primera de su altura {.sin-sangria}

(ínfima parte, digo, dividiendo {.sin-sangria}

en tres su continuado cuerpo horrendo), {.sin-sangria}

el rápido no pudo, el veloz vuelo {.sin-sangria}

del águila ---que puntas hace al cielo {.sin-sangria}

y el sol bebe los rayos (pretendiendo {.sin-sangria}

entre sus luces colocar su nido) {.sin-sangria}

llegar; bien que esforzando

más que nunca el impulso, ya batiendo {.sin-sangria}

las dos plumadas velas, ya peinando {.sin-sangria}

con las garras el aire, ha pretendido, {.sin-sangria}

tejiendo de los átomos escalas, {.sin-sangria}

que su inmunidad rompan sus dos alas. {.sin-sangria}

Las Pirámides dos ---ostentaciones {.sin-sangria}

de Menfis vano, y de la arquitectura {.sin-sangria}

último esmero, si ya no pendones {.sin-sangria}

fijos, no tremolantes---, cuya altura {.sin-sangria}

coronada de bárbaros trofeos {.sin-sangria}

tumba y bandera fue a los Ptolomeos, {.sin-sangria}

que al viento, que a las nubes publicaba {.sin-sangria}

(si ya también el cielo no decía) {.sin-sangria}

de su grande, su siempre vencedora {.sin-sangria}

ciudad ---ya Cairo ahora---

las que, porque a su copia enmudecía {.sin-sangria}

la Fama no cantaba

gitanas glorias, ménficas proezas, {.sin-sangria}

aun en el viento, aun en el cielo impresas; {.sin-sangria}

Eestas, que en nivelada simetría {.sin-sangria}

su estatura crecía

con tal disminución, con arte tanto, {.sin-sangria}

que cuánto más al cielo caminaba, {.sin-sangria}

a la vista, que lince la miraba, {.sin-sangria}

entre los vientos se desaparecía, {.sin-sangria}

sin permitir mirar la sutil punta {.sin-sangria}

que al primer orbe finge que se junta, {.sin-sangria}

hasta que, fatigada del espanto, {.sin-sangria}

no descendida, sino despeñada {.sin-sangria}

se hallaba al pie de la espaciosa basa, {.sin-sangria}

tarde o mal recobrada

del desvanecimiento:

que pena fue no escasa

del visual alado atrevimiento; {.sin-sangria}

cuyos cuerpos opacos

no al sol opuestos, antes avenidos {.sin-sangria}

con sus luces, si no confederados {.sin-sangria}

con él (como, en efecto, confinantes), {.sin-sangria}

tan del todo bañados

de un resplandor eran, que ---lucidos--- {.sin-sangria}

nunca de calurosos caminantes {.sin-sangria}

al fatigado aliento, a los pies flacos, {.sin-sangria}

ofrecieron alfombra

aun de pequeña, aun de señal de sombra; {.sin-sangria}

estas, que glorias ya sean gitanas, {.sin-sangria}

o elaciones profanas,

bárbaros jeroglíficos de ciego {.sin-sangria}

error, según el griego

ciego también, dulcísimo poeta {.sin-sangria}

---si ya, por las que escribe

aquileyas proezas

o marciales de Ulises sutilezas, {.sin-sangria}

la unión no le recibe

de los historiadores, o le acepta, {.sin-sangria}

cuando entre su catálogo le cuente, {.sin-sangria}

que gloria más que número le aumente---, {.sin-sangria}

de cuya dulce serie numerosa {.sin-sangria}

fuera más fácil cosa

al temido Tonante

el rayo fulminante

quitar, o la pesada

a Alcides clava herrada,

que un hemistiquio solo

de los que le dictó propicio Apolo; {.sin-sangria}

según de Homero, digo, la sentencia, {.sin-sangria}

las Pirámides fueron materiales {.sin-sangria}

tipos solo, señales exteriores {.sin-sangria}

de las que, dimensiones interiores, {.sin-sangria}

especies son del alma intencionales: {.sin-sangria}

que como sube en piramidal punta {.sin-sangria}

al cielo la ambiciosa llama ardiente, {.sin-sangria}

así la humana mente

su figura trasunta,

y a la Causa primera siempre aspira, {.sin-sangria}

céntrico punto donde recta tira {.sin-sangria}

la línea, si ya no circunferencia {.sin-sangria}

que contiene, infinita, toda esencia. {.sin-sangria}

Estos, pues, montes dos artificiales {.sin-sangria}

(bien maravillas, bien milagros sean), {.sin-sangria}

y aun aquella blasfema altiva Torre, {.sin-sangria}

de quien hoy dolorosas son señales {.sin-sangria}

---no en piedras, sino en lenguas desiguales, {.sin-sangria}

porque voraz el tiempo no las borre--- {.sin-sangria}

los idiomas diversos que escasean {.sin-sangria}

el sociable trato de las gentes {.sin-sangria}

(haciendo que parezcan diferentes {.sin-sangria}

los que unos hizo la naturaleza, {.sin-sangria}

de la lengua por solo la extrañeza), {.sin-sangria}

si fueran comparados

a la mental pirámide elevada {.sin-sangria}

donde ---sin saber como--- colocada {.sin-sangria}

el alma se miró, tan atrasados {.sin-sangria}

se hallaran, que cualquiera

graduara su cima por esfera, {.sin-sangria}

pues su ambicioso anhelo,

haciendo cumbre de su propio vuelo, {.sin-sangria}

en la más eminente

la encumbró parte de su propia mente, {.sin-sangria}

de sí tan remontada, que creía {.sin-sangria}

que a otra nueva región de sí salía. {.sin-sangria}

En cuya casi elevación inmensa, {.sin-sangria}

gozosa mas suspensa,

suspensa pero ufana,

y atónita aunque ufana, la suprema {.sin-sangria}

de lo sublunar reina soberana {.sin-sangria}

la vista perspicaz, libre de antojos, {.sin-sangria}

de sus intelectuales y bellos ojos, {.sin-sangria}

sin que distancia tema

ni de obstáculo opaco se recele, {.sin-sangria}

de que interpuesto algún objeto cele, {.sin-sangria}

libre tendió por todo lo criado: {.sin-sangria}

cuyo inmenso agregado,

cúmulo incomprehensible,

aunque a la vista quiso manifiesto {.sin-sangria}

dar señas de posible,

a la comprehensión no, que, entorpecida {.sin-sangria}

con la sobra de objetos, y excedida {.sin-sangria}

de la grandeza de ellos su potencia, {.sin-sangria}

retrocedió cobarde.

Tanto no, del osado presupuesto {.sin-sangria}

revocó la intención, arrepentida, {.sin-sangria}

la vista que intentó, descomedida, {.sin-sangria}

en vano hacer alarde

contra objeto que excede en excelencia {.sin-sangria}

las líneas visuales

---contra el sol, digo, cuerpo luminoso, {.sin-sangria}

cuyos rayos castigo son fogoso, {.sin-sangria}

que fuerzas desiguales

despreciando, castigan rayo a rayo {.sin-sangria}

el confiado, antes atrevido {.sin-sangria}

y ya llorado ensayo

(necia experiencia que costosa tanto {.sin-sangria}

fue, que Ícaro ya su propio llanto {.sin-sangria}

lo anegó enternecido)---,

como el entendimiento, aquí vencido {.sin-sangria}

no menos de la inmensa muchedumbre {.sin-sangria}

de tanta maquinosa pesadumbre {.sin-sangria}

(de diversas especies conglobado {.sin-sangria}

esférico compuesto),

que de las cualidades

de cada cual, cedió: tan asombrado {.sin-sangria}

que, entre la copia puesto,

pobre con ella en las neutralidades {.sin-sangria}

de un mar de asombros, la elección confusa, {.sin-sangria}

equívoco las ondas zozobraba; {.sin-sangria}

y por mirarlo todo, nada vía, {.sin-sangria}

ni discernir podía

(bota la facultad intelectiva {.sin-sangria}

en tanta, tan difusa

incomprensible especie que miraba {.sin-sangria}

desde el un eje en que librada estriba {.sin-sangria}

la máquina voluble de la esfera, {.sin-sangria}

el contrapuesto polo)

las partes ya no solo

que al universo todo considera {.sin-sangria}

serle perfeccionantes,

a su ornato, no más, pertenecientes; {.sin-sangria}

mas ni aun las que integrantes

miembros son de su cuerpo dilatado, {.sin-sangria}

proporcionadamente competentes. {.sin-sangria}

Mas como al que ha usurpado

diuturna oscuridad, de los objetos {.sin-sangria}

visibles los colores,

si súbitos le asaltan resplandores, {.sin-sangria}

con la sombra de luz queda más ciego {.sin-sangria}

---que el exceso contrarios hace efectos {.sin-sangria}

en la torpe potencia, que la lumbre {.sin-sangria}

del sol admitir luego

no puede, por la falta de costumbre, {.sin-sangria}

y a la tiniebla misma, que antes era {.sin-sangria}

tenebroso a la vista impedimento, {.sin-sangria}

de los agravios de la luz apela, {.sin-sangria}

y una vez y otra con la mano cela {.sin-sangria}

de los débiles ojos deslumbrados {.sin-sangria}

los rayos vacilantes,

sirviendo ya, piadosa medianera, {.sin-sangria}

la sombra de instrumento

para que recobrados

por grados se habiliten,

por que después constantes

su operación más firme ejerciten. {.sin-sangria}

(Recurso natural, innata ciencia {.sin-sangria}

que confirmada ya de la experiencia, {.sin-sangria}

maestro quizá mudo,

retórico ejemplar, inducir pudo {.sin-sangria}

a uno y otro Galeno

para que del mortífero veneno, {.sin-sangria}

en bien proporcionadas cantidades {.sin-sangria}

escrupulosamente regulando {.sin-sangria}

las ocultas nocivas cualidades, {.sin-sangria}

ya por sobrado exceso

de cálidas o frías,

o ya por ignoradas simpatías {.sin-sangria}

o antipatías con que van obrando {.sin-sangria}

las causas naturales su progreso, {.sin-sangria}

a la admiración dando, suspendida, {.sin-sangria}

efecto cierto en causa no sabida, {.sin-sangria}

con prolijo desvelo y remirada {.sin-sangria}

empírica atención, examinada {.sin-sangria}

en la bruta experiencia,

por menos peligrosa

la confección hicieron provechosa, {.sin-sangria}

último afán de la apolínea ciencia, {.sin-sangria}

de admirable triaca,

que así del mal el bien tal vez se saca). {.sin-sangria}

No de otra suerte el alma, que asombrada {.sin-sangria}

de la vista quedó de objeto tanto, {.sin-sangria}

la atención recogió, que derramada {.sin-sangria}

en diversidad tanta, aun no sabía {.sin-sangria}

recobrarse así misma del espanto {.sin-sangria}

que portentoso había

su discurso calmado,

permitiéndole apenas

de un concepto confuso

el informe embrión que, mal formado, {.sin-sangria}

inordinado caos retrataba {.sin-sangria}

de confusas especies que abrazaba, {.sin-sangria}

sin orden avenidas,

sin orden separadas,

que cuanto más se implican combinadas {.sin-sangria}

tanto más se disuelven desunidas, {.sin-sangria}

de diversidad llenas,

ciñendo con violencia lo difuso {.sin-sangria}

de objeto tanto, a tan pequeño vaso, {.sin-sangria}

aun al más bajo, aun al menor, escaso. {.sin-sangria}

Las velas, en efecto, recogidas {.sin-sangria}

que fió inadvertidas

traidor al mar, al viento ventilante {.sin-sangria}

---buscando, desatento,

al mar fidelidad, constancia al viento---, {.sin-sangria}

mal le hizo de su grado

en la mental orilla

dar fondo, destrozado,

al timón roto, a la quebrada entena, {.sin-sangria}

besando arena a arena

de la playa el bajel, astilla a astilla, {.sin-sangria}

donde, ya recobrado,

el lugar usurpó de la carena {.sin-sangria}

cuerda refleja, reportado aviso {.sin-sangria}

de dictamen remiso:

que en su operación misma reportado, {.sin-sangria}

más juzgó conveniente

a singular asunpto reducirse, {.sin-sangria}

o separadamente

una por discurrir las cosas {.sin-sangria}

que viene a ceñirse

en las que, artificiosas,

dos veces cinco son Categorías: {.sin-sangria}

reducción metafísica que enseña {.sin-sangria}

(los entes concibiendo generales {.sin-sangria}

en solo unas mentales fantasías {.sin-sangria}

donde de la materia se desdeña {.sin-sangria}

el discurso abstraído)

ciencia a formar de los Universales, {.sin-sangria}

reparando, advertido,

con el arte el defecto

de no poder con un intuitivo {.sin-sangria}

conocer acto todo lo creado, {.sin-sangria}

sino que, haciendo escala, de un concepto, {.sin-sangria}

en otro va ascendiendo grado a grado, {.sin-sangria}

y el de compreender orden relativo {.sin-sangria}

sigue, necesitado

del del entendimiento

limitado vigor, que a sucesivo {.sin-sangria}

discurso fía su aprovechamiento: {.sin-sangria}

cuyas débiles fuerzas, la doctrina {.sin-sangria}

con doctos alimentos va esforzando, {.sin-sangria}

y el prolijo, si blando,

continuo curso de la disciplina {.sin-sangria}

robustos le van alientos infundiendo, {.sin-sangria}

con que más animoso

el palio glorioso

del empeño más arduo, altivo aspira, {.sin-sangria}

los altos escalones ascendiendo, {.sin-sangria}

en una ya, ya en otra cultivado {.sin-sangria}

facultad, hasta que insensiblemente {.sin-sangria}

la honrosa cumbre mira,

término dulce de su afán pasado, {.sin-sangria}

de amarga siembra fruto al gusto grato {.sin-sangria}

(que aun a largas fatigas fue barato), {.sin-sangria}

y con planta valiente

la cima huella de su altiva frente. {.sin-sangria}

De esta serie seguir mi entendimiento {.sin-sangria}

el método quería,

o del ínfimo grado

del ser inanimado

(menos favorecido,

sino más desvalido,

de la segunda causa productiva), {.sin-sangria}

pasar a la más noble jerarquía {.sin-sangria}

que, en vegetable aliento,

primogénito es, aunque grosero, {.sin-sangria}

de Temis: el primero,

que a sus fértiles pechos maternales, {.sin-sangria}

con virtud atractiva,

los dulces apoyó manantiales {.sin-sangria}

de humor terrestre, que a su nutrimento {.sin-sangria}

natural es dulcísimo alimento, {.sin-sangria}

y de cuatro adornada operaciones {.sin-sangria}

de contrarias acciones,

ya atrae, ya segrega diligente {.sin-sangria}

lo que no serle juzga conveniente, {.sin-sangria}

ya lo superfluo expele, y de la copia {.sin-sangria}

la sustancia más útil hace propia; {.sin-sangria}

y, esta ya investigada,

forma inculcar más bella,

de sentido adornada,

y aun más que de sentido, de aprehensiva {.sin-sangria}

fuerza imaginativa:

que justa puede ocasionar querella, {.sin-sangria}

cuando afrenta no sea,

de la que más lucida centellea {.sin-sangria}

inanimada estrella,

bien que soberbios brille resplandores {.sin-sangria}

(que hasta a los astros puede superiores {.sin-sangria}

aun la menor criatura, aun la más baja, {.sin-sangria}

ocasionar envidia, hacer ventaja); {.sin-sangria}

y de este corporal conocimiento {.sin-sangria}

haciendo, bien que escaso, fundamento, {.sin-sangria}

el supremo pasar maravilloso {.sin-sangria}

compuesto triplicado,

de tres acordes líneas ordenado {.sin-sangria}

y de las formas todas inferiores {.sin-sangria}

compendio misterioso,

bisagra engazadora

de la que más se eleva entronizada {.sin-sangria}

naturaleza pura

y de la que, criatura

menos noble, se ve más abatida: {.sin-sangria}

no de las cinco solas adornada {.sin-sangria}

sensibles facultades,

mas de las interiores

que tres rectrices son, ennoblecida: {.sin-sangria}

que para ser señora

de las demás, no en vano

la adornó sabia poderosa mano, {.sin-sangria}

fin de sus obras, círculo que cierra {.sin-sangria}

la esfera con la tierra,

última perfección de lo creado {.sin-sangria}

y último de su eterno Autor agrado, {.sin-sangria}

en quien con satisfecha complacencia {.sin-sangria}

su inmensa descansó magnificencia; {.sin-sangria}

fábrica portentosa

que, cuanto más altiva al cielo toca, {.sin-sangria}

sella el polvo la boca

(de quien ser pudo imagen misteriosa {.sin-sangria}

la que Águila evangélica sagrada {.sin-sangria}

visión en Patmos vio, que las estrellas {.sin-sangria}

midió y el suelo con iguales huellas, {.sin-sangria}

o la estatua eminente

que del metal mostraba más preciado {.sin-sangria}

la rica altiva frente,

y en el más desechado

material, flaco fundamento hacia, {.sin-sangria}

con que a leve vaivén se deshacía); {.sin-sangria}

el Hombre, digo, en fin, mayor portento {.sin-sangria}

que discurre el humano entendimiento; {.sin-sangria}

compendio que absoluto

parece al ángel, a la planta, al bruto; {.sin-sangria}

cuya altiva bajeza

toda participó naturaleza. {.sin-sangria}

¿Por qué? Quizá porque, más venturosa {.sin-sangria}

que todas, encumbrada

a merced de amorosa

Unión sería (¡oh, aunque tan repetida, {.sin-sangria}

nunca bastante bien sabida {.sin-sangria}

merced, pues ignorada,

en lo poco apreciada

parece, o en lo mal correspondida!). {.sin-sangria}

Estos, pues, grados discurrir quería {.sin-sangria}

unas veces, pero otras, disentía, {.sin-sangria}

excesivo juzgando atrevimiento {.sin-sangria}

el discurrirlo todo

quien aun la más pequeña,

aun la más fácil parte no entendía {.sin-sangria}

de los más manuales

efectos naturales;

quien de la fuente no alcanzó risueña {.sin-sangria}

el ignorado modo

con que el curso dirige cristalino {.sin-sangria}

deteniendo en ambages su camino, {.sin-sangria}

los horrorosos senos

de Plutón, las cavernas pavorosas {.sin-sangria}

del abismo tremendo,

las campañas hermosas,

los Elíseos amenos,

tálamo ya de su triforme esposa, {.sin-sangria}

clara pesquisidora registrando, {.sin-sangria}

(útil curiosidad, aunque prolija, {.sin-sangria}

que de su no cobrada bella hija {.sin-sangria}

noticia cierta dio a la rubia diosa, {.sin-sangria}

cuando montes y selvas trastornando, {.sin-sangria}

cuando prados y bosques inquiriendo, {.sin-sangria}

su vida iba buscando

y del dolor su vida iba perdiendo); {.sin-sangria}

quien de la breve flor aun no sabía {.sin-sangria}

por qué ebúrnea figura

circunscribe su frágil hermosura; {.sin-sangria}

mixtos, por qué, colores,

confundiendo la grana en los albores, {.sin-sangria}

fragante le son gala;

ámbares por qué exhala,

y el leve, si más bello

ropaje al viento explica,

que en una y otra fresca multiplica {.sin-sangria}

hoja, formando pompa escarolada {.sin-sangria}

de dorados perfiles cairelada, {.sin-sangria}

que, roto del capillo el blanco sello, {.sin-sangria}

de dulce herida de la cipria diosa {.sin-sangria}

los despojos ostenta jactanciosa, {.sin-sangria}

si ya el que la colora,

candor al alba, púrpura a la aurora {.sin-sangria}

no le usurpó y, mezclado,

purpúreo es ampo, rosicler nevado, {.sin-sangria}

tornasol que concita

los que del prado aplausos solicita: {.sin-sangria}

preceptor quizá vano,

si no ejemplo profano,

de industria femenil, que el más activo {.sin-sangria}

veneno hace dos veces ser nocivo {.sin-sangria}

en el velo aparente

de la que finge tez resplandeciente. {.sin-sangria}

Pues si a un objeto solo ---repetía {.sin-sangria}

tímido el pensamiento---

huye el conocimiento

y cobarde el discurso se desvía; {.sin-sangria}

si a especie segregada

---como de las demás independiente, {.sin-sangria}

como sin relación considerada--- {.sin-sangria}

da las espaldas el entendimiento {.sin-sangria}

y asombrado el discurso se espeluza {.sin-sangria}

del difícil certamen que rehúsa {.sin-sangria}

acometer valiente,

porque teme, cobarde,

comprehenderlo o mal, o nunca, o tarde, {.sin-sangria}

¿cómo en tan espantosa

máquina inmensa discurrir pudiera?, {.sin-sangria}

cuyo terrible, incomportable peso, {.sin-sangria}

si ya en su centro mismo no estribara, {.sin-sangria}

de Atlante a las espaldas agobiara, {.sin-sangria}

de Alcídes a las fuerzas excediera; {.sin-sangria}

y el que fue de la esfera

bastante contrapeso,

pesada menos, menos ponderosa {.sin-sangria}

su máquina juzgara, que la empresa {.sin-sangria}

de investigar a la naturaleza. {.sin-sangria}

Otras, más esforzado,

demasiada acusaba cobardía {.sin-sangria}

el laudo antes ceder, que en la lid dura {.sin-sangria}

haber siquiera entrado;

y al ejemplar osado

del claro joven la atención volvía, {.sin-sangria}

auriga altivo del ardiente carro, {.sin-sangria}

y el, si infeliz, bizarro

alto impulso, el espíritu encendía: {.sin-sangria}

donde el ánimo halla,

más que el temor ejemplos de escarmiento, {.sin-sangria}

abiertas sendas al atrevimiento, {.sin-sangria}

que una ya vez trilladas, no hay castigo {.sin-sangria}

que intento baste a remover segundo {.sin-sangria}

(segunda ambición, digo).

Ni el panteón profundo,

cerúlea tumba a su infeliz ceniza, {.sin-sangria}

ni el vengativo rayo fulminante {.sin-sangria}

mueve, por más que avisa,

al ánimo arrogante

que, el vivir despreciando, determina {.sin-sangria}

su nombre eternizar en su ruina. {.sin-sangria}

Tipo es, antes, modelo,

ejemplar pernicioso

que alas engendra a repetido vuelo {.sin-sangria}

del ánimo ambicioso

que, del mismo terror haciendo halago {.sin-sangria}

que el valor lisonjea,

las glorias deletrea

entre los caracteres del estrago. {.sin-sangria}

(O el castigo jamás se publicara, {.sin-sangria}

por que nunca el delito se intentara; {.sin-sangria}

político silencio antes rompiera {.sin-sangria}

los autos del proceso

---circunspecto estadista---;

o en fingida ignorancia simulara {.sin-sangria}

o con secreta pena castigara {.sin-sangria}

el insolente exceso,

sin que a popular vista

el ejemplar nocivo propusiera: {.sin-sangria}

que del mayor delito la malicia {.sin-sangria}

peligra en la noticia,

contagio dilatado trascendiendo; {.sin-sangria}

por que singular culpa solo siendo, {.sin-sangria}

dejara más remota a lo ignorado {.sin-sangria}

su ejecución, que no a lo escarmentado). {.sin-sangria}

Mas mientras entre escollos zozobraba, {.sin-sangria}

confusa la elección, sirtes tocando {.sin-sangria}

de imposibles en cuantos intentaba {.sin-sangria}

rumbos seguir, no hallando

materia en que cebarse

el calor ya, pues su templada llama {.sin-sangria}

(llama al fin, aunque más templada sea, {.sin-sangria}

que si su activa emplea

operación, consume, si no inflama), {.sin-sangria}

sin poder excusarse,

había lentamente

el manjar transformado,

propia sustancia de la ajena haciendo, {.sin-sangria}

y el que hervor resultaba bullicioso {.sin-sangria}

de la unión entre el húmedo y ardiente, {.sin-sangria}

en el maravilloso

natural vaso había ya cesado {.sin-sangria}

faltando el medio y consiguientemente {.sin-sangria}

los que de él ascendiendo

soporíferos, húmedos vapores {.sin-sangria}

el trono racional embarazaban {.sin-sangria}

(desde donde a los miembros derramaban {.sin-sangria}

dulce entorpecimiento),

a los suaves ardores

del calor consumidos,

las cadenas del sueño desataban, {.sin-sangria}

Y, la falta sintiendo de alimento {.sin-sangria}

los miembros extenuados,

del descanso cansados,

ni del todo despiertos ni dormidos, {.sin-sangria}

muestras de apetecer el movimiento {.sin-sangria}

con tardos esperezos

ya daban, extendiendo

los nervios, poco a poco, entumecidos, {.sin-sangria}

y los cansados huesos

aun sin entero arbitrio de su dueño {.sin-sangria}

volviendo al otro lado,

a cobrar empezaron los sentidos, {.sin-sangria}

dulcemente impedidos

del natural beleño,

su operación, los ojos entreabriendo. {.sin-sangria}

Y del cerebro, ya desocupado, {.sin-sangria}

los fantasmas huyeron

y, como de vapor leve formadas, {.sin-sangria}

en fácil humo, en viento convertidas {.sin-sangria}

su forma resolvieron.

(Así, linterna mágica, pintadas {.sin-sangria}

representa fingidas

en la blanca pared varias figuras, {.sin-sangria}

de la sombra no menos ayudadas {.sin-sangria}

que de la luz: que en trémulos reflejos {.sin-sangria}

los competentes lejos

guardando de la docta perspectiva, {.sin-sangria}

en sus ciertas mensuras

de varias experiencias aprobadas, {.sin-sangria}

la sombra fugitiva,

que en el mismo esplendor se desvanece, {.sin-sangria}

cuerpo finge formado,

de todas dimensiones adornado, {.sin-sangria}

cuando a un ser superficie no merece). {.sin-sangria}

En tanto, el padre de la luz ardiente, {.sin-sangria}

de acercarse al Oriente

ya el término prefijo conocía, {.sin-sangria}

y al antípoda opuesto despedía {.sin-sangria}

con trasmontantes rayos:

que de su luz en trémulos desmayos, {.sin-sangria}

en el punto hace mismo su occidente {.sin-sangria}

que nuestro oriente ilustra luminoso. {.sin-sangria}

Pero de Venus, antes, el hermoso {.sin-sangria}

apacible lucero

rompió el albor primero,

y del viejo Titón la bella esposa, {.sin-sangria}

amazona de luces mil vestida, {.sin-sangria}

contra la noche armada,

hermosa si atrevida,

valiente aunque llorosa,

su frente mostró hermosa

de matutinas luces coronada, {.sin-sangria}

aunque tierno preludio, ya animoso {.sin-sangria}

del planeta fogoso,

que venía las tropas reclutando {.sin-sangria}

de bisoñas vislumbres

(las más robustas, veteranas lumbres {.sin-sangria}

para la retaguardia reservando) {.sin-sangria}

contra la que, tirana usurpadora {.sin-sangria}

del imperio del día,

negro laurel de sombras mil ceñía {.sin-sangria}

y con nocturno cetro pavoroso {.sin-sangria}

las sombras gobernaba,

de quien aun ella misma se espantaba. {.sin-sangria}

Pero apenas la bella precursora {.sin-sangria}

signífera del sol el luminoso {.sin-sangria}

en el oriente tremoló estandarte, {.sin-sangria}

tocando alarma todos los suaves {.sin-sangria}

si bélicos clarines de las aves, {.sin-sangria}

diestros, aunque sin arte,

trompetas sonorosos,

cuando ---como tirana al fin, cobarde, {.sin-sangria}

de recelos medrosos

embarazada, bien que hacer alarde {.sin-sangria}

intentó de sus fuerzas, oponiendo {.sin-sangria}

de su funesta capa los reparos, {.sin-sangria}

breves en ella de los tajos claros {.sin-sangria}

heridas recibiendo,

(bien que, mal satisfecho su denuedo, {.sin-sangria}

pretexto mal formado fue del miedo, {.sin-sangria}

su débil resistencia conociendo)---, {.sin-sangria}

a la fuga ya casi cometiendo {.sin-sangria}

más que a la fuerza, el medio de salvarse, {.sin-sangria}

ronca tocó bocina

a recoger los negros escuadrones {.sin-sangria}

para poder en orden retirarse, {.sin-sangria}

cuando de más vecina

plenitud de reflejos fue asaltada, {.sin-sangria}

que la punta rayó más encumbrada {.sin-sangria}

de los del mundo erguidos torreones. {.sin-sangria}

Llegó, en efecto, el sol cerrando el giro {.sin-sangria}

que esculpió de oro sobre azul zafiro. {.sin-sangria}

De mil multiplicados

mil veces puntos, flujos mil dorados, {.sin-sangria}

líneas, digo, de la luz clara, salían {.sin-sangria}

de su circunferencia luminosa, {.sin-sangria}

pautando al cielo la cerúlea plana; {.sin-sangria}

y a la que antes funesta fue tirana {.sin-sangria}

de su imperio, atropadas embestían: {.sin-sangria}

que sin concierto huyendo presurosa, {.sin-sangria}

en sus mismos horrores tropezando, {.sin-sangria}

su sombra iba pisando,

y llegar al ocaso pretendía {.sin-sangria}

con el (sin orden ya) desbaratado {.sin-sangria}

ejército de sombras, acosado {.sin-sangria}

de la luz de la luz que el alcance le seguía. {.sin-sangria}

Consiguió, al fin, la vista del ocaso {.sin-sangria}

el fugitivo paso,

y en su mismo despeño recobrada, {.sin-sangria}

esforzando el aliento de la ruina, {.sin-sangria}

en la mitad del globo que ha dejado {.sin-sangria}

el sol desamparada,

segunda vez rebelde, determina {.sin-sangria}

mirarse coronada,

mientras nuestro hemisferio la dorada {.sin-sangria}

ilustraba del sol madeja hermosa, {.sin-sangria}

que con luz judiciosa

de orden distributivo, repartiendo {.sin-sangria}

a las cosas visibles sus colores {.sin-sangria}

iba, y restituyendo

entera a los sentidos exteriores {.sin-sangria}

su operación, quedando a la luz más cierta {.sin-sangria}

el mundo iluminado, y yo despierta. {.sin-sangria}

</section>
